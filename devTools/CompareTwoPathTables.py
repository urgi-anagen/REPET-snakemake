#!/usr/bin/env python

##@file
# Calculate the sensitivity and specificity of one table (predictions) against the other (references).
# Also return the counts of true positives, false positives, true negatives and false negatives.
# Only consider query coordinates when comparing match boundaries.
# usage: %s [options]
# options:
#      -h: this help
#      -t: name of the table to test (format='path')
#      -r: name of the reference table (format='path')
#      -C: name of the configuration file
#      -l: cumulative length of the queries (used for TN)
#      -b: compare boundaries
#      -v: verbosity level (default=0/1/2)

import os
import sys
import getopt
from commons.core.LoggerFactory import LoggerFactory
from commons.core.sql.DbFactory import DbFactory
from commons.core.sql.TablePathAdaptator import TablePathAdaptator
from commons.core.coord.SetUtils import SetUtils
from commons.core.coord.PathUtils import PathUtils

LOG_DEPTH = "devTools"

class CompareTwoPathTables( object ):
    """
    Calculate the sensitivity and specificity of one table (predictions) against the other (references).
    Also return the counts of true positives, false positives, true negatives and false negatives.
    Only consider query coordinates.
    """
    
    def __init__( self ):
        """
        Constructor.
        """
        self._predData = ""
        self._refData = ""
        self._typeData = ""
        self._configFile = ""
        self._cumulLengthQueries = 0
        self._compareBoundaries = False
        self._mergeData = False
        self._verbose = 0
        self._db = None
        self._tpaPred = None
        self._tpaRef = None
        self._log = LoggerFactory.createLogger("{}.{}".format(LOG_DEPTH, self.__class__.__name__), self._verbose)
        self._name = "CompareTwoPathTables"
        self._lCases = [ "pred_1-to-1", "exact", "near exact", "one-side exact","equivalent", "near equivalent",
                        "similar", "different strand", "pred_1-to-0", "pred_1-to-n" ]
        
        
    def help( self ):
        """
        Display the help on stdout.
        """
        print()
        print("usage: {} [options]".format(sys.argv[0]))
        print("options:")
        print("     -h: this help")
        print("     -p: name of the predicted data (format='path')")
        print("     -r: name of the reference data (format='path')")
        print("     -t: type of the data (file/table)")
        print("     -C: name of the configuration file to access MySQL (e.g. 'TEannot.cfg')")
        print("     -l: cumulative length of the queries (used for TN)")
        print("     -b: compare boundaries (query coordinates)")
        print("     -m: merge data (query coordinates)")
        print("     -v: verbosity level (default=0/1/2)")
        print()

    def setAttributesFromCmdLine( self ):
        """
        Set the attributes from the command-line.
        """
        try:
            opts, args = getopt.getopt(sys.argv[1:],"hp:r:t:C:l:bmv:")
        except getopt.GetoptError as err:
            print(str(err))
            self.help(); sys.exit(1)
        for o,a in opts:
            if o == "-h":
                self.help(); sys.exit(0)
            elif o == "-p":
                self._predData = a
            elif o == "-r":
                self._refData = a
            elif o == "-t":
                self._typeData = a
            elif o == "-C":
                self._configFile = a
            elif o == "-l":
                self._cumulLengthQueries = int(a)
            elif o == "-b":
                self._compareBoundaries = True
            elif o == "-m":
                self._mergeData = True
            elif o == "-v":
                self._verbose = int(a)
                
                
    def checkAttributes( self ):
        """
        Check the attributes are valid before running the algorithm.
        """
        if self._predData == "":
            print("ERROR: missing predicted data (-p)")
            self.help(); sys.exit(1)
        if self._refData == "":
            print("ERROR: missing reference data (-r)")
            self.help(); sys.exit(1)
        if self._typeData == "":
            print("ERROR: missing data type (-t)")
            self.help(); sys.exit(1)
        if self._configFile == "":
            print("ERROR: missing configuration file (-C)")
            self.help(); sys.exit(1)
        if not os.path.exists( self._configFile ):
            print("ERROR: can't find configuration file '{}'".format(self._configFile))
            self.help(); sys.exit(1)
        self._db = DbFactory.createInstance(configFileName=self._configFile)
        if self._cumulLengthQueries == 0:
            print("ERROR: need cumulative length of queries to compute true negatives (-l)")
            self.help(); sys.exit(1)
        if self._typeData == "file":
            if not os.path.exists( self._predData ):
                print("ERROR: can't find file of predicted data '{}'".format(self._predData))
                self.help(); sys.exit(1)
            if not os.path.exists( self._refData ):
                print("ERROR: can't find file of reference data '{}'".format(self._predData))
                self.help(); sys.exit(1)
        elif self._typeData == "table":
            if not self._db.doesTableExist( self._predData ):
                print("ERROR: can't find table of predicted data '{}'".format(self._predData))
                self.help(); sys.exit(1)
            if not self._db.doesTableExist( self._refData ):
                print("ERROR: can't find table of reference data '{}'".format(self._predData))
                self.help(); sys.exit(1)
        else:
            print("ERROR: unrecognized data type '{}'".format(self._typeData))
            self.help(); sys.exit(1)
            
            
    def setAdaptatorsToTablesFromInputData( self ):
        """
        If input data are files, load them into tables. Then set adaptators to path tables.
        """
        if self._typeData == "file":
            pathPredTable = self._predData.replace(".","_")
            self._db.dropTable( pathPredTable )
            self._db.createTable( pathPredTable, "path", self._predData )
            pathRefTable = self._refData.replace(".","_")
            self._db.dropTable( pathRefTable )
            self._db.createTable( pathRefTable, "path", self._refData )
            self._tpaPred = TablePathAdaptator( self._db, pathPredTable )
            self._tpaRef = TablePathAdaptator( self._db, pathRefTable )
        else:
            self._tpaPred = TablePathAdaptator( self._db, self._predData )
            self._tpaRef = TablePathAdaptator( self._db, self._refData )
            
            
    def getCounts( self ):
        """
        Get the true positive, false positive, true negative and false negative:
        TP: found in test as in ref
        FP: found in test but not in ref
        TN: neither found in test nor in ref
        FN: not found in test but found in ref
        """
        tp, fp, fn = 0, 0, 0
        tn = self._cumulLengthQueries
        nbTestMatches = 0
        nbRefMatches = 0
        
        lTestQueries = self._tpaPred.getQueryList()
        lRefQueries = self._tpaRef.getQueryList()
        string = "nb of queries"
        string += "\nin predicted data: {:d}".format(len(lTestQueries))
        string += "\nin reference data: {:d}".format(len(lRefQueries))
        self._log.info( string )
        if self._verbose > 0:
            print(string)
            sys.stdout.flush()
        
        for testQuery in lTestQueries:
            lTest = self._tpaPred.getSetListFromQuery( testQuery )
            nbTestMatches += len(lTest)
            lTest = SetUtils.mergeSetsInList( lTest )
            testLength = SetUtils.getCumulLength( lTest )
            lRef = self._tpaRef.getSetListFromQuery( testQuery )
            nbRefMatches += len(lRef)
            lRef = SetUtils.mergeSetsInList( lRef )
            refLength = SetUtils.getCumulLength( lRef )
            overlapLength = SetUtils.getOverlapLengthBetweenLists( lTest, lRef )
            tp += overlapLength
            fp += testLength - overlapLength
            tn -= testLength + ( refLength - overlapLength )
            fn += refLength - overlapLength
            
        string = "nb of matches"
        string += "\nin predicted data: {:d}".format(nbTestMatches)
        string += "\nin reference data: {:d}".format(nbRefMatches)
        self._log.info( string )
        if self._verbose > 0:
            print(string)
            sys.stdout.flush()
        
        return tp, fp, tn, fn
    
    
    def getPerformanceMeasures( self, tp, fp, tn, fn ):
        """
        Get the sensitivity and specificity.
        """
        sensitivity = tp / float( tp + fn )
        specificity = tn / float( tn + fp )
        return sensitivity, specificity
    
    
    def mergeMatchesInTable( self, table ):
        mergeTable = "{}_merged".format(table)
        self._db.exportDataToFile(table, param = "ORDER BY query_name,query_start,subject_name,subject_start")
        mergeFile = "{}.merged".format(table)
        PathUtils.mergeMatchesOnQueries(table, mergeFile)
        os.remove(table)
        self._db.dropTable(mergeTable)
        self._db.createTable(mergeTable,"path", mergeFile)
        #os.remove( mergeFile )
        return mergeTable
    
    
    def compareBoundariesBetweenPredAndRefMatches( self, predMatch, refMatch ):
        """
        Compare predicted and reference matches in terms of boundaries. Several cases exist:
        distance <= 1 bp in 5' and 3': predicted match is exact
        distance <= 1 bp in 5' (or 3') and 1 < distance <= 10 bp in 3' (or 5'): predicted match is near exact
        distance <= 1 bp in 5' (or 3') and distance > 10 bp in 3' (or 5'): predicted match is one-side exact
        1 < distance <= 10 bp in 5' and 3': predicted match is equivalent
        1 < distance <= 10 bp in 5' (or 3') and d > 10 bp in 3' (or 5'): predicted match is near equivalent
        distance > 10 bp in 5' and 3': predicted match is similar
        """
        distMinCoords = abs( predMatch.range_query.getMin() - refMatch.range_query.getMin() )
        distMaxCoords = abs( predMatch.range_query.getMax() - refMatch.range_query.getMax() )
        
        if distMinCoords <= 1 and distMaxCoords <= 1:
            return "exact"
            
        elif ( distMinCoords <= 1 and distMaxCoords <= 10 ) or ( distMinCoords <= 10 and distMaxCoords <= 1 ):
            return "near exact"
            
        elif ( distMinCoords <= 1 and distMaxCoords > 10 ) or ( distMinCoords > 10 and distMaxCoords <= 1 ):
            return "one-side exact"
            
        elif distMinCoords <= 10 and distMaxCoords <= 10:
            return "equivalent"
            
        elif ( distMinCoords <= 10 and distMaxCoords > 10 ) or ( distMinCoords > 10 and distMaxCoords <= 10 ):
            return "near equivalent"
            
        elif distMinCoords > 10 and distMaxCoords > 10:
            return "similar"
            
        else:
            print("ERROR in compareBoundariesBetweenTestAndRefMatches()")
            predMatch.show()
            refMatch.show()
            sys.exit(1)
            
            
    def compareBoundariesBetweenPredAndRefTables( self ):
        """
        Compare the predicted and reference tables in terms of match boundaries.
        """
        dCase2Count = {}
        for case in self._lCases:
            dCase2Count[ case ] = 0
            
        lQueryNames = self._tpaPred.getQueryList()
        for queryName in lQueryNames:
            
            lPredMatches = self._tpaPred.getPathListSortedByQueryCoordFromQuery( queryName )
            for predMatch in lPredMatches:
                if self._verbose > 1:
                    print("predicted match: {} {:d}->{:d}".format(queryName, predMatch.range_query.start,
                                                                  predMatch.range_query.end))
                    sys.stdout.flush()
                    
                lRefMatches = self._tpaRef.getPathListOverlappingQueryCoord( queryName, predMatch.range_query.start, predMatch.range_query.end )
                
                if len(lRefMatches) == 0:
                    case = "pred_1-to-0"
                    dCase2Count[ case ] += 1
                    if self._verbose > 1:
                        print("case '{}'".format(case))
                    continue
                
                if len(lRefMatches) > 1:
                    case = "pred_1-to-n"
                    dCase2Count[ case ] += 1
                    if self._verbose > 1:
                        print("case '{}' (n={:d})".format(case, len(lRefMatches)))
                    continue
                
                dCase2Count[ "pred_1-to-1" ] += 1
                for refMatch in lRefMatches:
                    if not( predMatch.range_query.getStrand() == refMatch.range_query.getStrand()
                    and predMatch.range_subject.getStrand() == refMatch.range_subject.getStrand() ):
                        dCase2Count[ "different strand" ] += 1
                    case = self.compareBoundariesBetweenPredAndRefMatches( predMatch, refMatch )
                    dCase2Count[ case ] += 1
                    if self._verbose > 1:
                        print("case '{}' with {} {:d}->{:d}".format(case, queryName, refMatch.range_query.start,
                                                                    refMatch.range_query.end))

        return dCase2Count
    
    
    def getResultsAsString( self, dCase2Count, nbMatchesPredTable ):
        string = "results of boundary comparisons"
        
        case = "pred_1-to-1"
        string += "\ncase '{}':".format(case)
        string += " {:d}".format(dCase2Count[case])
        string += " ({:.2f}%)".format(100 * dCase2Count[case] / float(nbMatchesPredTable))
        
        lCasesOneToOne = [ "exact", "near exact", "one-side exact", \
                          "equivalent", "near equivalent", \
                          "similar", "different strand" ]
        for case in lCasesOneToOne:
            string += "\ncase '{}':".format(case)
            string += " {:d}".format(dCase2Count[case])
            string += " ({:.2f}%)".format(100 * dCase2Count[case] / float(dCase2Count["pred_1-to-1"]))
            
        for case in [ "pred_1-to-0", "pred_1-to-n" ]:
            string += "\ncase '{}':".format(case)
            string += " {:d}".format(dCase2Count[case])
            string += " ({:.2f}%)".format(100 * dCase2Count[case] / float(nbMatchesPredTable))
            
        return string
    
    
    def start( self ):
        """
        Useful commands before running the program.
        """
        self.checkAttributes()
        if self._verbose > 0:
            print("START {}.py".format(self._name))
            sys.stdout.flush()
        logFileName = "{}_{}_vs_{}.log".format("CompareTwoPathTables", self._predData, self._refData)
        if os.path.exists( logFileName ):
            os.remove( logFileName )
        self._log = LoggerFactory.createLogger( logFileName )
        self._log.info( "started" )
        
        string = "input data"
        if self._typeData == "file":
            string += "\npredicted file: '{}'".format(self._predData)
            string += "\nreference file: '{}'".format(self._refData)
        elif self._typeData == "table":
            string += "\npredicted table: '{}'".format(self._predData)
            string += "\nreference table: '{}'".format(self._refData)
        self._log.info( string )
        if self._verbose > 0:
            print(string)
            sys.stdout.flush()
        
        
    def end( self ):
        """
        Useful commands before ending the program.
        """
        self._db.close()
        self._log.info( "finished" )
        if self._verbose > 0:
            print("END {}".format(self._name))
            sys.stdout.flush()
            
            
    def run( self ):
        """
        Run the program.
        """
        LoggerFactory.setLevel(self._log, self._verbose)
        self.start()
        
        self.setAdaptatorsToTablesFromInputData()
        
        tp, fp, tn, fn = self.getCounts()
        string = "counts"
        string += "\ntrue positives: {:d}".format(tp)
        string += "\nfalse positives: {:d}".format(fp)
        string += "\ntrue negatives: {:d}".format(tn)
        string += "\nfalse negatives: {:d}".format(fn)
        self._log.info( string )
        if self._verbose > 0:
            print(string)
            sys.stdout.flush()
        sn, sp = self.getPerformanceMeasures( tp, fp, tn, fn )
        string = "performance measures"
        string += "\nsensitivity: {:f}".format(sn)
        string += "\nspecificity: {:f}".format(sp)
        self._log.info( string )
        if self._verbose > 0:
            print(string)
            sys.stdout.flush()
        
        if self._compareBoundaries:
            string = "boundaries comparisons"
            
            if self._mergeData:
                mergePredTable = self.mergeMatchesInTable( self._tpaPred.getTable() )
                mergeRefTable = self.mergeMatchesInTable( self._tpaRef.getTable() )
                self._tpaPred.setTable(mergePredTable)
                self._tpaRef.setTable(mergeRefTable)
                nbMatchesPredTable = self._tpaPred.getSize()
                nbMatchesRefTable = self._tpaRef.getSize()
                string = "nb of merged matches"
                string += "\nin predicted table: {:d}".format(nbMatchesPredTable)
                string += "\nin reference table: {:d}".format(nbMatchesRefTable)
                self._log.info( string )
                if self._verbose > 0:
                    print(string)
                    sys.stdout.flush()
            else:
                nbMatchesPredTable = self._tpaPred.getSize()
                
            dCase2Count = self.compareBoundariesBetweenPredAndRefTables()
            
            string = self.getResultsAsString( dCase2Count, nbMatchesPredTable )
            self._log.info( string )
            if self._verbose > 0:
                print(string)
                sys.stdout.flush()
            if self._mergeData:
                self._db.dropTable( mergePredTable )
                self._db.dropTable( mergeRefTable )
                
        self.end()
        
        
if __name__ == "__main__":
    i = CompareTwoPathTables()
    i.setAttributesFromCmdLine()
    i.run()
