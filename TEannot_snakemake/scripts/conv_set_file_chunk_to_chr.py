#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May  5 11:33:22 2022

@author: mwan
"""

import pandas
import time
import os
import logging
from commons.core.coord.Set import Set
import sys
from annot_pipe.srptAutoPromote import srptAutoPromote
from commons.core.coord.SetUtils import SetUtils


def conv_set_file(map_file, set_file):
    verbose = 0
    df = pandas.read_csv(set_file, sep='\t', delimiter=None, header=None, index_col=None)

    map_data = dict()
    with open(map_file, 'r') as f:
        for line in f.readlines():
            split_line = line.split("\t")
            map_data[split_line[0]] = (split_line[1], int(split_line[2]), int(split_line[3]))

    data = dict()
    vect_df = df.values
    for row in vect_df:
        if row[2] not in data.keys():
            data[row[2]] = list()
        new_set = Set(id=row[0], name=row[1], seqname=row[2], start=row[3], end=row[4])
        data[row[2]].append(new_set)

    tmpFileName = "{}.on_chr".format(set_file)

    with open(tmpFileName, "w") as tmpFile:

        for chunkName in map_data.keys():
            if verbose > 1:
                ("processing {}...".format(chunkName));
                sys.stdout.flush()
            res = data[chunkName]

            for s in res:
                # s.setFromTuple(t)
                i = map_data[chunkName]
                s.seqname = i[0]
                if (i[1] < i[2]):
                    s.start = s.start + i[1] - 1
                    s.end = s.end + i[1] - 1
                else:
                    s.start = i[1] - s.start + 1
                    s.end = i[1] - s.end + 1
                s.write(tmpFile)


def connect_conv_set_file(map_file, set_file, output, verbose=0, log_file=""):
    df = pandas.read_csv(set_file, sep='\t', delimiter=None, header=None, index_col=0)

    if log_file == "":
        log_filename = "connect_conv_set_file.log"
    else:
        log_filename = log_file
    if os.path.exists(log_filename):
        os.remove(log_filename)

    handler = logging.FileHandler(log_filename)
    formatter = logging.Formatter("%(asctime)s %(levelname)s: %(message)s")
    handler.setFormatter(formatter)
    logging.getLogger('').addHandler(handler)
    logging.getLogger('').setLevel(logging.DEBUG)
    logging.info("started connect_conv_set_file")

    # read_map
    map_data = dict()  # chunk
    with open(map_file, 'r') as f:
        for line in f.readlines():
            split_line = line.split("\t")
            map_data[split_line[0]] = (split_line[1], int(split_line[2]), int(split_line[3]))

    temp = []
    vect_df = df.values
    list_id = df.index
    for i in range (len(list_id)) :
        row = vect_df[i]
        index = list_id[i]
        new_set = Set(id=index, name=row[0], seqname=row[1], start=row[2], end=row[3])
        temp.append((index, row[1], new_set))
    data = pandas.DataFrame(temp, columns=("path", "chunk", "set"))

    tmp_file_name = "{}.on_chr.tmp".format(set_file)

    with open(tmp_file_name, "w") as tmpFile:
        for chunk_name in map_data.keys():
            if verbose > 1:
                logging.info("processing {}...".format(chunk_name));

            res = data.loc[(data["chunk"] == chunk_name)]["set"].tolist()
            for s in res:
                i = map_data[chunk_name]
                s.seqname = i[0]
                if (i[1] < i[2]):
                    s.start = s.start + i[1] - 1
                    s.end = s.end + i[1] - 1
                else:
                    s.start = i[1] - s.start + 1
                    s.end = i[1] - s.end + 1
                s.write(tmpFile)

    df_set_chr = pandas.read_csv(tmp_file_name, sep='\t', header=None, index_col=0, low_memory=False)
    df_set_chr_new = df_set_chr.copy()

    nb_chunks = len(map_data.keys())
    for num_chunk in range(1, nb_chunks):

        chunk_name = "chunk{}".format(str(num_chunk).zfill(len(str(nb_chunks))))
        next_chunk_name = "chunk{}".format(str(num_chunk + 1).zfill(len(str(nb_chunks))))

        if next_chunk_name not in map_data.keys():
            break
        if verbose > 1:
            logging.info("try with {} and {}".format(chunk_name, next_chunk_name))
        start = map_data[chunk_name][2]
        end = map_data[next_chunk_name][1]

        if map_data[chunk_name][0] == map_data[next_chunk_name][0]:
            # getChainListOverlappingCoord

            if (start > end):
                start, end = end, start
            sub_df_set_chr_chr = df_set_chr.loc[(df_set_chr[2] == map_data[chunk_name][0])]
            sqlCmd = "(((sub_df_set_chr_chr[3] <= {}) & (sub_df_set_chr_chr[3] >= {}))".format(max(start, end),
                                                                                               min(start, end))
            sqlCmd += " | ((sub_df_set_chr_chr[4] >= {}) & (sub_df_set_chr_chr[4] <= {})))".format(min(start, end),
                                                                                                   max(start, end))
            sqlCmd += "| ((sub_df_set_chr_chr[[3, 4]].min(axis=1) <= {}) & (sub_df_set_chr_chr[[3, 4]].max(axis=1) >= " \
                      "{}))".format(min(start, end), max(start, end))

            sub_df_set_chr_chr_coord = (eval("sub_df_set_chr_chr.loc[{}]".format(sqlCmd)))
            lSets = list()
            for num_set in sorted(set(sub_df_set_chr_chr_coord.index)):
                lSets += data.loc[(data["path"] == num_set)]["set"].tolist()

            list_sorted_sets = SetUtils.getSetListSortedByIncreasingMinThenMax(lSets)

            chg_set_id = {}
            setnum_to_ins = []
            setnum_to_del = []
            dset = []
            rset = []

            for i in list_sorted_sets:
                if i.isOnDirectStrand():
                    dset.append(i)
                else:
                    rset.append(i)
            x = 0
            while x < len(dset) - 1:
                x = x + 1
                if verbose > 1:
                    logging.info("++++")
                    dset[x - 1].show()
                    dset[x].show()

                if dset[x - 1].id != dset[x].id \
                        and dset[x - 1].seqname == dset[x].seqname \
                        and dset[x - 1].name == dset[x].name \
                        and dset[x - 1].isOnDirectStrand() \
                        == dset[x].isOnDirectStrand() \
                        and dset[x - 1].isOverlapping(dset[x]):
                    chg_set_id[dset[x].id] = dset[x - 1].id
                    if dset[x - 1].id not in setnum_to_ins:
                        setnum_to_ins.append(dset[x - 1].id)
                    if dset[x].id not in setnum_to_del:
                        setnum_to_del.append(dset[x].id)
                    dset[x - 1].merge(dset[x])
                    del dset[x]
                    x = x - 1
                    if verbose > 1:
                        logging.info("--> merged")
            if verbose > 1:
                logging.info("...........")
            x = 0
            while x < len(rset) - 1:
                x = x + 1
                if verbose > 1:
                    logging.info("++++")
                    rset[x - 1].show()
                    rset[x].show()

                if rset[x - 1].id != rset[x].id \
                        and rset[x - 1].seqname == rset[x].seqname \
                        and rset[x - 1].name == rset[x].name \
                        and rset[x - 1].isOnDirectStrand() \
                        == rset[x].isOnDirectStrand() \
                        and rset[x - 1].isOverlapping(rset[x]):
                    chg_set_id[rset[x].id] = rset[x - 1].id
                    if rset[x - 1].id not in setnum_to_ins:
                        setnum_to_ins.append(rset[x - 1].id)
                    if rset[x].id not in setnum_to_del:
                        setnum_to_del.append(rset[x].id)
                    rset[x - 1].merge(rset[x])
                    del rset[x]
                    x = x - 1
                    if verbose > 1:
                        logging.info("--> merged")
            if verbose > 1:
                logging.info("...........")
                logging.info(setnum_to_del)

            if verbose > 1:
                # print("setnum to delete", setnum_to_del)
                logging.info("setnum to delete {}".format(setnum_to_del))
                logging.info("setnum to insert {}".format(setnum_to_ins))

            df_set_chr_new = df_set_chr_new.drop(setnum_to_del, errors='ignore')
            df_set_chr_new = df_set_chr_new.drop(setnum_to_ins, errors='ignore')

            tmp_list_dset = list()
            for i in dset:
                # if chg_set_id.has_key(i.id):
                if (i.id) in chg_set_id:
                    i.id = chg_set_id[i.id]
                if verbose > 1:
                    # i.show()
                    logging.info(i)
                if i.id in setnum_to_ins:
                    new_row = pandas.DataFrame({1: [i.getName()],
                                                2: [i.getSeqname()],
                                                3: [i.getStart()],
                                                4: [i.getEnd()]}, index=[i.getId()])

                    tmp_list_dset.append(new_row)

                    if verbose > 1:
                        logging.info("--> inserted!")
            df_set_chr_new = pandas.concat([df_set_chr_new] + tmp_list_dset, axis=0)

            tmp_list_rset = list()
            for i in rset:
                # f chg_set_id.has_key(i.id):
                if (i.id) in chg_set_id:
                    i.id = chg_set_id[i.id]
                if verbose > 1:
                    # i.show()
                    logging.info(i)
                if i.id in setnum_to_ins:
                    new_row = pandas.DataFrame({1: [i.getName()],
                                                2: [i.getSeqname()],
                                                3: [i.getStart()],
                                                4: [i.getEnd()]}, index=[i.getId()])
                    tmp_list_rset.append(new_row)
                    if verbose > 1:
                        logging.info("--> inserted!")
            df_set_chr_new = pandas.concat([df_set_chr_new] + tmp_list_rset, axis=0)

    df_set_chr_new.to_csv(output, sep="\t", index=True, header=None)


"""
#Dmel
#set_file = "/home/mwan/REPET/REPET-snakemake/TEannot_snakemake/test/DmelChr4_chk_allSSRs_set.tsv"
#map_file = "/home/mwan/REPET/REPET-snakemake/results_DmelChr4_repet/TEannot/DmelChr4_db/DmelChr4_chunks.map"
#expected = "/home/mwan/REPET/REPET-snakemake/TEannot_snakemake/test/DmelChr4_chr_allSSRs_set.tsv"

#Atha
#set_file="/home/mwan/REPET/REPET-snakemake/TEannot_snakemake/test/AthaGenome_chk_allSSRs_set.tsv"
#set_file="/home/mwan/REPET/REPET-snakemake/TEannot_snakemake/test/test_AthaGenome_chk_allSSRs_set.txt"
set_file_sm = "/home/mwan/REPET/REPET-snakemake/TEannot_snakemake/test/test_set_to_chr/AthaGenome_chk_allSSRs_set.tsv"
map_file="/home/mwan/REPET/REPET-snakemake/TEannot_snakemake/test/AthaGenome_chunks.map"
expected = "/home/mwan/REPET/REPET-snakemake/TEannot_snakemake/test/test_set_to_chr/AthaGenome_chr_allSSRs_set.tsv"

start_time = time.time()
connect_conv_set_file(map_file, set_file_sm, "test.tsv")
print("--- %s seconds ---" % (time.time() - start_time))
obs = "/home/mwan/REPET/REPET-snakemake/TEannot_snakemake/test/AthaGenome_chk_allSSRs_set.tsv.on_chr.tmp"

#connect_conv_set_file(map_file=snakemake.params.map_chunk, set_file=snakemake.input[0])
"""
connect_conv_set_file(map_file=snakemake.params.map_chunk, 
                      set_file=snakemake.input[0],
                      output=snakemake.output.on_chr_file, 
                      log_file=snakemake.log[0])