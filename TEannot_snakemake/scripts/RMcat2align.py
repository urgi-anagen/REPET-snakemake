#!/usr/bin/env python

import os
import sys
import argparse


def RMcat2align(inFileName="", outFileName="", verbose=0):
    """
    Convert the output file from RepeatMasker ('cat' format) into the 'align' format.
    """
    if not os.path.exists(inFileName):
        msg = "ERROR: can't find input file '{}'".format(inFileName)
        sys.stderr.write("{}\n".format(msg))
        sys.exit(1)

    if outFileName == "":
        outFileName = "{}.align".format(inFileName)

    inFileHandler = open(inFileName, "r")
    outFileHandler = open(outFileName, "w")
    countLines = 0

    while True:
        line = inFileHandler.readline()
        if line == "":
            break
        countLines += 1

        tokens = line.split()
        try:
            test = int(line[0])
        except ValueError:
            continue

        scoreSW = tokens[0]
        percDiv = tokens[1]
        percId = 100.0 - float(percDiv)
        percDel = tokens[2]
        percIns = tokens[3]

        # query coordinates are always direct (start<end)
        qryName = tokens[4]
        qryStart = int(tokens[5])
        qryEnd = int(tokens[6])
        qryAfterMatch = tokens[7]

        # if subject on direct strand
        if len(tokens) == 13:
            sbjName = tokens[8]
            sbjStart = int(tokens[9])
            sbjEnd = int(tokens[10])
            sbjAfterMatch = tokens[11]

        # if subject on reverse strand
        elif tokens[8] == "C":
            sbjName = tokens[9]
            sbjAfterMatch = tokens[10]
            sbjStart = int(tokens[11])
            sbjEnd = int(tokens[12])

        # compute a new score: match length on query times identity
        matchLengthOnQuery = qryEnd - qryStart + 1
        newScore = int(matchLengthOnQuery * float(percId) / 100.0)

        string = "{}\t{}\t{}\t{}\t{}\t{}\t0.0\t{}\t{}\n".format(qryName, qryStart, qryEnd, sbjName, sbjStart, sbjEnd,
                                                                newScore, percId)

        outFileHandler.write(string)

    inFileHandler.close()
    outFileHandler.close()
    if verbose > 0:
        print("nb of lines: {}").format(countLines)

    return 0

RMcat2align(snakemake.input[0], snakemake.output[0])

"""
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Convert the output file from RepeatMasker ('cat' format) into the 'align' format.",
                                     formatter_class=argparse.RawTextHelpFormatter, add_help=True)
    parser.add_argument('-i', '--input', required=True, help="input file name (cat format, output from RepeatMasker)")
    parser.add_argument('-o', '--output', default="",
                        help="output file name (align format, default=inFileName+'.align')")
    parser.add_argument('-v', '--verbose', default=0, type=int, help="verbose (default=0/1)")

    args = parser.parse_args()
    inFileName = args.input
    outFileName = args.output
    verbose = args.verbose

    RMcat2align(inFileName, outFileName, verbose)
"""