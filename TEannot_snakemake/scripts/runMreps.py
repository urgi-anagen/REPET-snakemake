
import os
import contextlib
from os.path import join
from commons.tools.MergeMatchsFiles import MergeMatchsFiles
from commons.launcher.LaunchMreps import main


base = os.getcwd()
os.chdir(snakemake.params.outdir)
if not os.path.exists(os.path.basename(snakemake.input[0])):
    os.symlink(os.path.join(base,snakemake.input[0]),os.path.basename(snakemake.input[0]))
lMreps = main(inFileName=os.path.basename(snakemake.input[0]),singularity_img = snakemake.params.sif)
os.remove(os.path.basename(snakemake.input[0]))
os.chdir(base)
