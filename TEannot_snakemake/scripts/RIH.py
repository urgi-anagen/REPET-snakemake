from commons.tools.RetrieveInitHeadersPandasVersion import RetrieveInitHeadersPandasVersion


RetrieveInitHeadersPandasVersion(snakemake.input.link,
                                snakemake.input.path,
                                snakemake.output[0]).run()
