from annot_pipe.UpdateScoreOfMatches import UpdateScoreOfMatches

UpdateScoreOfMatches(inputData = snakemake.input[0],
                    formatData = "align",
                    outputData = snakemake.output[0],
                    verbose = 0,
                    typeInData = "file").run()
