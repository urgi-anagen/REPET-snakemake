#!/usr/bin/env python

import os
import sys
import getopt
import logging
import pandas
from commons.core.coord.PathUtils import PathUtils
from commons.core.coord.SetUtils import SetUtils
from commons.core.coord.Path import Path
from commons.core.coord.Range import Range
from commons.core.coord.Set import Set


def read_ssr_set_file(ssr_file):
    """ Read SSR file 

    Parameters
    ----------
    ssr_file : str, ssr file (format tsv)

    Returns
    -------
    data_ssr : Dataframe

    """
    df_ssr = pandas.read_csv(ssr_file, sep='\t', header=None, index_col=None)
    sorted_dataframe_start_end = pandas.DataFrame(columns=df_ssr.columns)
    sorted_dataframe_end_start = pandas.DataFrame(columns=df_ssr.columns)

    vect_data_ssr = df_ssr.values
    start_end = list()
    end_start = list()

    for row in vect_data_ssr:
        if row[3] < row[4]:
            start_end.append(pandas.DataFrame(row).T)
            # start_end.append(row.to_frame().T)
        elif row[3] > row[4]:
            end_start.append(pandas.DataFrame(row).T)
            # end_start.append(row.to_frame().T)

    sorted_dataframe_end_start = pandas.concat([sorted_dataframe_end_start] + start_end,
                                               ignore_index=True)
    sorted_dataframe_start_end = pandas.concat([sorted_dataframe_start_end] + end_start,
                                               ignore_index=True)

    sorted_dataframe_start_end.sort_values(by=4)
    sorted_dataframe_end_start.sort_values(by=4)

    sorted_dataframe_start_end.drop_duplicates()
    sorted_dataframe_end_start.drop_duplicates()
    data_ssr_sorted = pandas.concat([sorted_dataframe_start_end, sorted_dataframe_end_start], axis=0, sort=False)

    data_ssr_sorted.drop_duplicates()

    data_ssr = dict()
    vect_data_ssr_sorted = data_ssr_sorted.values
    for row in vect_data_ssr_sorted:
        if row[2] not in data_ssr.keys():
            data_ssr[row[2]] = list()
        new_set = Set(id=row[0], name=row[1], seqname=row[2], start=row[3], end=row[4])
        data_ssr[row[2]].append(new_set)

    return data_ssr


def get_set_list_from_id_list(list_id, list_sets):
    """Get a list of Set objects by their ID

    Parameters
    ----------
    list_id : list(int), list of identifier
    list_sets : list(commons.core.coord.Set), list of "Set" objects

    Returns
    -------
    res_list_set : list(commons.core.coord.Set), list of "Set" objects corresponding to the list of identifier

    """
    res_list_set = []
    if not list_id:
        return res_list_set

    for iD in list_id:
        for o_set in list_sets:
            if o_set.getId() == iD:
                res_list_set.append(o_set)
                break

    return res_list_set


def srptRemoveSpurious(ssr_file, path_file, output="", log_file="", verbose=0, remain_thres_size=20):
    if verbose > 0:
        print("START srptRemoveSpurious")

    if output == "":
        output_filename = "{}_noSSR".format(path_file)
    else:
        output_filename = output

    if log_file == "":
        log_filename = "removeSpurious.log"
    else:
        log_filename = log_file

    if os.path.exists(log_filename):
        os.remove(log_filename)
    handler = logging.FileHandler(log_filename)
    formatter = logging.Formatter("%(asctime)s %(levelname)s: %(message)s")
    handler.setFormatter(formatter)
    logging.getLogger('').addHandler(handler)
    logging.getLogger('').setLevel(logging.DEBUG)
    logging.info("started")

    df_path = pandas.read_csv(path_file, sep='\t', header=None, index_col=0, low_memory=False)
    data_ssr = read_ssr_set_file(ssr_file)
    # Read path file
    data_path = dict()
    vect_df_path = df_path.values
    list_path_id = df_path.index

    for i in range(len(list_path_id)):
        row = vect_df_path[i]
        index = list_path_id[i]
        if index not in data_path.keys():
            data_path[index] = list()
        range_query = Range(seqname=row[0], start=row[1], end=row[2])
        range_subject = Range(seqname=row[3], start=row[4], end=row[5])
        new_path = Path(id=index, range_q=range_query, range_s=range_subject, e_value=row[6], score=row[7],
                        identity=row[8])
        data_path[index].append(new_path)

    # list_path_id = list(df_path.index)
    i_str = "total number of paths: {}".format(len(list_path_id))
    if verbose > 0:
        # print(i_str)
        logging.info(i_str)

    tmp_filename = "{}.tmp".format(path_file)
    with open(tmp_filename, "w") as f:
        for chunk_name in data_ssr.keys():
            list_of_sets = data_ssr[chunk_name]
            for i in list_of_sets:
                set_bin = i.getBin()
                set_max = i.getMax()
                set_min = i.getMin()
                set_strand = i.isOnDirectStrand()
                f.write("%d\t%f\t%s\t%d\t%d\t%d\n" % (i.id, set_bin, i.seqname, set_min, set_max, set_strand))

    list_set_id_to_remove_paths = []
    count = 0
    df_ssr_with_bin = pandas.read_csv(tmp_filename, sep='\t', delimiter=None, header=None, index_col=None)

    for identifier in list_path_id:
        if verbose > 0:
            i_str = "processing path '{}'...".format(identifier)

        list_query_sets = PathUtils.getSetListFromQueries(data_path[identifier])
        sorted(list_query_sets,
               key=lambda i_set: (i_set.getId(), i_set.getSeqname(), i_set.getName(), i_set.getMin(), i_set.getMax()))
        qmin, qmax = SetUtils.getListBoundaries(list_query_sets)
        qmin = qmin - 1
        qmax = qmax + 1
        min_coord = min(qmin, qmax)
        max_coord = max(qmin, qmax)

        sql = "("
        sub_df_chunk = df_ssr_with_bin.loc[(df_ssr_with_bin[2] == list_query_sets[0].seqname.split()[0])]
        sub_df_chunk_coord = sub_df_chunk.loc[((sub_df_chunk[3] <= max_coord) & (sub_df_chunk[4] >= min_coord))]
        for i in range(8, 2, -1):
            bin_lvl = pow(10, i)
            if int(qmin / bin_lvl) == int(qmax / bin_lvl):
                bin = float(bin_lvl + (int(qmin / bin_lvl) / 1e10))
                sql += '(sub_df_chunk_coord[1]=={})'.format(round(bin, 6))
            else:
                bin1 = float(bin_lvl + (int(qmin / bin_lvl) / 1e10))
                bin2 = float(bin_lvl + (int(qmax / bin_lvl) / 1e10))
                sql += '((sub_df_chunk_coord[1]>={}) & (sub_df_chunk_coord[1]<={}))'.format(round(bin1, 6), bin2)

            if bin_lvl != 1000:
                sql += " | "
        sql += ")"
        sub_df_chunk_bins = (eval("sub_df_chunk_coord.loc[{}]".format(sql)))

        list_subject_sets = list()
        if list_query_sets[0].seqname.split()[0] in data_ssr.keys():
            list_subject_sets = get_set_list_from_id_list(list(sub_df_chunk_bins.iloc[:, 0]),
                                                          data_ssr[list_query_sets[0].seqname.split()[0]])

        # list_subject_sets = get_set_list_from_id_list(list(sub_df_chunk_bins.iloc[:, 0]),data_ssr[list_query_sets[0].seqname.split()[0]])

        if verbose > 1:
            print("----------------------------------------")
        if len(list_subject_sets) > 0:
            if verbose > 1:
                print("annot:")
                SetUtils.showList(list_query_sets)
            list_query_sets = SetUtils.mergeSetsInList(list_query_sets)
            qsize = SetUtils.getCumulLength(list_query_sets)
            i_str += "\nannot size: {}".format(qsize)
            if verbose > 1:
                print("annot size=", qsize)
            if verbose > 1:
                print("simple repeats list:")
                SetUtils.showList(list_subject_sets)
            list_subject_sets = SetUtils.mergeSetsInList(list_subject_sets)
            ssize = SetUtils.getCumulLength(list_subject_sets)
            i_str += "\nsimple repeats size: {}".format(ssize)
            osize = SetUtils.getOverlapLengthBetweenLists(list_query_sets, list_subject_sets)
            i_str += "\noverlap size: {}".format(osize)
            i_str += "\nremains {}".format(qsize - osize)

            if verbose > 1:
                print("merged simple repeats list:")
                SetUtils.showList(list_subject_sets)
                print("simple repeats size=", ssize)
                print("overlap size=", osize, "--> remains=", qsize - osize)

            if qsize - osize < remain_thres_size:
                i_str += "\nremove path {}".format(list_query_sets[0].id)
                if verbose > 1:
                    print("remove path {}").format(list_query_sets[0].id)
                list_set_id_to_remove_paths.append(list_query_sets[0].id)
                count = count + 1

        else:
            i_str += "\nno overlapping simple repeats"
            path_length = (qmax - 1) - (qmin + 1) + 1
            if path_length < remain_thres_size:
                i_str += "\nfilter (length={}bp < {}bp)".format(path_length, remain_thres_size)
                list_set_id_to_remove_paths.append(identifier)
                count += 1
                if verbose > 1:
                    print(i_str)

        if verbose > 1:
            logging.info(i_str)

    i_str = "number of path(s) to remove: {}".format(count)

    if verbose > 0:
        logging.info(i_str)
        # print(i_str)
    list_set_id_to_remove_paths.sort()
    if verbose > 1:
        logging.info(list_set_id_to_remove_paths)

    df_path_set_removed = df_path.drop(list_set_id_to_remove_paths)
    if os.path.exists(tmp_filename):
        os.remove(tmp_filename)
    df_path_set_removed.to_csv(output_filename, sep="\t", index=True, header=None)

    logging.info("finished")
    if verbose > 0:
        print("END srptRemoveSpurious")


# ssr_file = "/home/mwan/REPET/REPET-snakemake/TEannot_snakemake/test/AthaGenome_chk_allSSRs_set.on_chr"
# path_file = "/home/mwan/REPET/REPET-snakemake/TEannot_snakemake/test/AthaGenome_TEannot_Matcher_chk_allTEs.path-nr"
# celui de repet
# ssr_file = "/home/mwan/REPET/REPET-snakemake/results_DmelChr4_repet/TEannot/DmelChr4_TEdetect/Comb/Dmel4_chk_allSSR.tsv"
# path_file = "/home/mwan/REPET/REPET-snakemake/results_DmelChr4_repet/TEannot/DmelChr4_TEdetect/Comb/Dmel4_chk_path.tsv"
# [22, 54, 94, 135, 171, 180, 188, 198, 216, 217, 387, 426, 513]
# srptRemoveSpurious(ssr_file, path_file,  verbose =-1)
srptRemoveSpurious(ssr_file=snakemake.input[1], path_file=snakemake.input[0], log_file=snakemake.log[0],
                   remain_thres_size=snakemake.params[0], verbose=snakemake.params[1])
