#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  3 14:32:27 2022

@author: mwan
"""
from commons.core.coord.Path import Path
import sys
import os
import pandas
from commons.core.coord.Path import Path
from commons.core.coord.Range import Range
from commons.core.coord.PathUtils import PathUtils
import logging


def conv_path_file_chunk_to_chr(map_file, path_file, connect=True, verbose=0, log_file=""):
    """
    Convert a 'path' file format.
    """

    map_data = dict()
    output_name = "{}.on_chr".format(path_file)

    if log_file == "":
        log_filename = "conv_path_file_chunk_to_chr.log"
    else:
        log_filename = log_file
    if os.path.exists(log_filename):
        os.remove(log_filename)

    handler = logging.FileHandler(log_filename)
    formatter = logging.Formatter("%(asctime)s %(levelname)s: %(message)s")
    handler.setFormatter(formatter)
    logging.getLogger('').addHandler(handler)
    logging.getLogger('').setLevel(logging.DEBUG)
    logging.info("started conv_path_file_chunk_to_chr")

    with open(map_file, 'r') as f:
        for line in f.readlines():
            split_line = line.split("\t")
            map_data[split_line[0]] = (split_line[1], int(split_line[2]), int(split_line[3]))

    if not connect:
        with open(output_name, "w") as fout, open(path_file, "r") as fin:
            p = Path()
            while True:
                if not p.read(fin):
                    break
                #p.range_query.show()
                logging.info(p.range_query)
                i = map_data.get(p.range_query.seqname.split()[0], None)
                if i == None:
                    # print("*** Error: chunk: '{}' not found".format(p.range_query.seqname))
                    logging.info("*** Error: chunk: '{}' not found".format(p.range_query.seqname))
                    sys.exit(1)
                else:
                    p.range_query.seqname = i[0]
                    if i[1] < i[2]:
                        p.range_query.start = p.range_query.start + i[1] - 1
                        p.range_query.end = p.range_query.end + i[1] - 1
                    else:
                        p.range_query.start = i[1] - p.range_query.start + 1
                        p.range_query.end = i[1] - p.range_query.end + 1
                    p.write(fout)

    else:
        if verbose > 0:
            logging.info("connect chunks...")

        nb_chunks = len(map_data.keys())
        df_path_chk = pandas.read_csv(path_file, sep='\t', header=None, index_col=None, low_memory=False)

        # Read path file and convert chk on chr
        tmp_filename = "{}.on_chr.tmp".format(path_file)
        with open(tmp_filename, "w") as f:
            p = Path()
            # for each chunk
            list_chunks = map_data.keys()
            # list_chunks.sort()
            for chunkName in sorted(list_chunks):
                if verbose > 1:
                    logging.info("processing {}...".format(chunkName))
                sub_df_chunk = df_path_chk.loc[(df_path_chk[1] == chunkName)]
                for i in sub_df_chunk.index:
                    p.setFromTuple(list(sub_df_chunk.loc[i]))
                    # convert the coordinates on the query
                    link = map_data[chunkName]
                    p.range_query.seqname = link[0]
                    if (link[1] < link[2]):
                        p.range_query.start = p.range_query.start + link[1] - 1
                        p.range_query.end = p.range_query.end + link[1] - 1
                    else:
                        p.range_query.start = link[1] - p.range_query.start + 1
                        p.range_query.end = link[1] - p.range_query.end + 1

                    # convert the coordinates on the subject (if necessary)
                    link = map_data.get(p.range_subject.seqname)
                    if link != None:
                        if verbose > 1:
                            logging.info("convert subject: {}".format(p.range_subject.seqname))
                        p.range_subject.seqname = link[0]
                        p.range_subject.start = p.range_subject.start + link[1] - 1
                        p.range_subject.end = p.range_subject.end + link[1] - 1
                    p.write(f)

        df_path_chr = pandas.read_csv(tmp_filename, sep='\t', header=None, index_col=0, low_memory=False)
        df_path_chr_new = df_path_chr.copy()

        if os.path.exists(tmp_filename):
            os.remove(tmp_filename)

        # read new path file
        data_path = dict()
        vect_df = df_path_chr.values
        list_id = df_path_chr.index
        for i in range(len(list_id)):
            row = vect_df[i]
            index = list_id[i]
            # for index, row in df_path_chr.iterrows():
            if index not in data_path.keys():
                data_path[index] = list()
            range_query = Range(seqname=row[0], start=row[1], end=row[2])
            range_subject = Range(seqname=row[3], start=row[4], end=row[5])
            new_path = Path(id=index, range_q=range_query, range_s=range_subject, e_value=row[6], score=row[7],
                            identity=row[8])
            data_path[index].append(new_path)

        for num_chunk in range(1, nb_chunks):
            chunk_name = "chunk{}".format(str(num_chunk).zfill(len(str(nb_chunks))))
            next_chunk_name = "chunk{}".format(str(num_chunk + 1).zfill(len(str(nb_chunks))))

            if next_chunk_name not in map_data.keys():
                break
            if verbose > 1:
                # print("try with {} and {}".format(chunk_name, next_chunk_name))
                logging.info("try with {} and {}".format(chunk_name, next_chunk_name))

            start = map_data[chunk_name][2]
            end = map_data[next_chunk_name][1]

            if map_data[chunk_name][0] == map_data[next_chunk_name][0]:
                if (start > end):
                    start, end = end, start
                sub_df_path_chr_chr = df_path_chr.loc[(df_path_chr[1] == map_data[chunk_name][0])]

                sqlCmd = "(((sub_df_path_chr_chr[2] <= {}) & (sub_df_path_chr_chr[3] >= {}) & (sub_df_path_chr_chr[3] <= {}))".format(
                    start, start, end)
                sqlCmd += " | ((sub_df_path_chr_chr[2] >= {}) & (sub_df_path_chr_chr[3] <= {}))".format(start, end)
                sqlCmd += "| ((sub_df_path_chr_chr[2] >= {}) & (sub_df_path_chr_chr[2] <= {}) & (sub_df_path_chr_chr[" \
                          "3] >= {}))".format(
                    start, end, end)
                sqlCmd += " | ((sub_df_path_chr_chr[2] <= {}) & (sub_df_path_chr_chr[3] >= {})))".format(start, end)

                sub_df_path_chr_chr_coord = (eval("sub_df_path_chr_chr.loc[{}]".format(sqlCmd)))
                lPaths = list()
                for num_path in sorted(set(sub_df_path_chr_chr_coord.index)):
                    lPaths += data_path[num_path]

                if verbose > 1:
                    # print("{} matches on {} ({}->{})".format(len(lPaths), map_data[chunk_name][0], start, end))
                    logging.info("{} matches on {} ({}->{})".format(len(lPaths), map_data[chunk_name][0], start, end))
                list_sorted_paths = PathUtils.getPathListSortedByIncreasingMinQueryThenMaxQueryThenMinSubjectThenMaxSubject(
                    lPaths)
                chg_path_id = {}
                pathnum_to_ins = []
                pathnum_to_del = []
                dpath = []
                rpath = []
                for i in list_sorted_paths:
                    if i.range_query.isOnDirectStrand() and i.range_subject.isOnDirectStrand():
                        dpath.append(i)
                    else:
                        rpath.append(i)
                x = 0
                while x < len(dpath) - 1:
                    x = x + 1
                    if verbose > 1:
                        dpath[x - 1].show()
                        dpath[x].show()
                        logging.info(dpath[x - 1])
                        logging.info(dpath[x])

                    if dpath[x - 1].id != dpath[x].id \
                            and dpath[x - 1].range_query.seqname == dpath[x].range_query.seqname \
                            and dpath[x - 1].range_subject.seqname == dpath[x].range_subject.seqname \
                            and dpath[x - 1].range_query.isOnDirectStrand() == dpath[x].range_query.isOnDirectStrand() \
                            and dpath[x - 1].range_subject.isOnDirectStrand() == dpath[
                        x].range_subject.isOnDirectStrand() \
                            and dpath[x - 1].range_query.isOverlapping(dpath[x].range_query) \
                            and dpath[x - 1].range_subject.isOverlapping(dpath[x].range_subject):
                        chg_path_id[dpath[x].id] = dpath[x - 1].id
                        if dpath[x - 1].id not in pathnum_to_ins:
                            pathnum_to_ins.append(dpath[x - 1].id)
                        if dpath[x].id not in pathnum_to_del:
                            pathnum_to_del.append(dpath[x].id)
                        dpath[x - 1].merge(dpath[x])
                        del dpath[x]
                        x = x - 1
                        if verbose > 1:
                            # print("--> merged")
                            logging.info("--> merged")

                x = 0
                while x < len(rpath) - 1:
                    x = x + 1
                    if verbose > 1:
                        rpath[x - 1].show()
                        rpath[x].show()
                        logging.info(rpath[x - 1])
                        logging.info(rpath[x])
                    if rpath[x - 1].id != rpath[x].id \
                            and rpath[x - 1].range_query.seqname == rpath[x].range_query.seqname \
                            and rpath[x - 1].range_subject.seqname == rpath[x].range_subject.seqname \
                            and rpath[x - 1].range_query.isOnDirectStrand() == rpath[x].range_query.isOnDirectStrand() \
                            and rpath[x - 1].range_subject.isOnDirectStrand() == rpath[
                        x].range_subject.isOnDirectStrand() \
                            and rpath[x - 1].range_query.isOverlapping(rpath[x].range_query) \
                            and rpath[x - 1].range_subject.isOverlapping(rpath[x].range_subject):
                        chg_path_id[rpath[x].id] = rpath[x - 1].id
                        if rpath[x - 1].id not in pathnum_to_ins:
                            pathnum_to_ins.append(rpath[x - 1].id)
                        if rpath[x].id not in pathnum_to_del:
                            pathnum_to_del.append(rpath[x].id)
                        rpath[x - 1].merge(rpath[x])
                        del rpath[x]
                        x = x - 1
                        if verbose > 1:
                            # print("--> merged")
                            logging.info("--> merged")
                if verbose > 1:
                    # print("pathnum to delete", pathnum_to_del)
                    logging.info("pathnum to delete {}".format(pathnum_to_del))

                df_path_chr_new = df_path_chr_new.drop(pathnum_to_del,
                                                       errors='ignore')  # est ce une bonne idée le errors='ignore'
                df_path_chr_new = df_path_chr_new.drop(pathnum_to_ins, errors='ignore')

                for i in dpath:
                    # if chg_path_id.has_key(i.id):
                    if (i.id) in chg_path_id:
                        i.id = chg_path_id[i.id]
                    if verbose > 1:
                        i.show()
                        logging.info(i)
                    if i.id in pathnum_to_ins:
                        new_row = pandas.DataFrame({1: [i.getQueryName()],
                                                    2: [i.getQueryStart()],
                                                    3: [i.getQueryEnd()],
                                                    4: [i.getSubjectName()],
                                                    5: [i.getSubjectStart()],
                                                    6: [i.getSubjectEnd()],
                                                    7: [i.getEvalue()],
                                                    8: [i.getScore()],
                                                    9: [i.getIdentity()]}, index=[i.getIdentifier()])
                        df_path_chr_new = pandas.concat([df_path_chr_new, new_row], axis=0)
                        if verbose > 1:
                            # print("--> inserted!")
                            logging.info("--> inserted!")
                for i in rpath:
                    # f chg_path_id.has_key(i.id):
                    if (i.id) in chg_path_id:
                        i.id = chg_path_id[i.id]
                    if verbose > 1:
                        i.show()
                        logging.info(i)
                    if i.id in pathnum_to_ins:
                        new_row = pandas.DataFrame({1: [i.getQueryName()],
                                                    2: [i.getQueryStart()],
                                                    3: [i.getQueryEnd()],
                                                    4: [i.getSubjectName()],
                                                    5: [i.getSubjectStart()],
                                                    6: [i.getSubjectEnd()],
                                                    7: [i.getEvalue()],
                                                    8: [i.getScore()],
                                                    9: [i.getIdentity()]}, index=[i.getIdentifier()])
                        df_path_chr_new = pandas.concat([df_path_chr_new, new_row], axis=0)
                        if verbose > 1:
                            # print("--> inserted!")
                            logging.info("--> inserted!")

        df_path_chr_new.to_csv(output_name, sep="\t", index=True, header=None)
    if verbose > 0:
        # print("END Conv path chunk to chr")
        logging.info("END conv_path_file_chunk_to_chr")

    return 0


# conv_path_file_chunk_to_chr("/home/mwan/REPET/REPET-snakemake/TEannot_snakemake/test/srptConv/MSYD_TEannotGr_chunks.map","/home/mwan/REPET/REPET-snakemake/TEannot_snakemake/test/srptConv/MSYD_TEannotGr_chk_allTEs_nr_noSSR_path.tsv")

conv_path_file_chunk_to_chr(map_file=snakemake.params.map_chunk, path_file=snakemake.input[0],
                            connect=snakemake.params.connect, verbose=snakemake.params.verbose,
                            log_file=snakemake.log[0])