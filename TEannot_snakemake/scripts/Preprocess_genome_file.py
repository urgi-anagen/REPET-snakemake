
from commons.tools.PreProcess import PreProcess
import os
from os.path import join


PreProcess(fastaFileName=snakemake.input[0],
            outputFasta=snakemake.output[0],
            step=1,
            verbosity=3,
            singularity_img = snakemake.params.sif,
            log_name =snakemake.log[0]
            ).run(dirStep = os.path.join(os.getcwd(),snakemake.params.dir))
