import os
from commons.tools.ChangeSequenceHeaders import ChangeSequenceHeaders

genome_name = os.path.basename(snakemake.config["genome_file"]).strip(".fa")
results_dir = "results_TEannot_{}".format(genome_name)
chunk_dir = "{}/chunk_dir".format(results_dir)


base = os.getcwd()
os.chdir(chunk_dir)

if not os.path.exists(snakemake.params.init_name):
    os.symlink(snakemake.input[0],snakemake.params.init_name)
os.chdir(base)

ChangeSequenceHeaders(inFile=snakemake.params.init_file_path,
                    format="fasta",
                    step=1,
                    prefix="refTE_",
                    outFile = snakemake.output.fasta).run()
