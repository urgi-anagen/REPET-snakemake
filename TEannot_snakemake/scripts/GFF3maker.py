from commons.tools.GFF3Maker import GFF3Maker

GFF3Maker(inFastaName=snakemake.input.genome_fasta_file,
        joinPathFile=snakemake.input.join_path_file,
        classifFile=snakemake.params.classif_file,
        isChado=snakemake.params.is_chado,
        isGFF3WithoutAnnotation=snakemake.params.is_GFF3_without_annotation,
        isWithSequence=snakemake.params.is_with_sequence,
        areMatchPartsCompulsory= snakemake.params.match_parts_compulsory,
        verbose=snakemake.params.verbosity,
        doMergeIdenticalMatches=snakemake.params.do_merge_identical_matches,
        doSplit=snakemake.params.do_split,
        alignInformationsFile=snakemake.input.align_tab_file,
        logName = snakemake.log[0],
        projectDir = snakemake.params.project_dir).run()
