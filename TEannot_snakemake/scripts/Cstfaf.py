from annot_pipe.ComputeScoreThresholdFromAlignFiles import ComputeScoreThresholdFromAlignFiles

if snakemake.params.no_CENSOR:
    ComputeScoreThresholdFromAlignFiles(
                                    list_files_rm =snakemake.input.rm_align_files,
                                    list_files_blr =snakemake.input.blaster_align_files ,
                                    log_name=snakemake.log[0],
                                    output_name = snakemake.output[0],
                                    verbosity = 3).run()
else :
    ComputeScoreThresholdFromAlignFiles(list_files_cen=snakemake.input.censor_align_files,
                                    list_files_rm =snakemake.input.rm_align_files,
                                    list_files_blr =snakemake.input.blaster_align_files ,
                                    log_name=snakemake.log[0],
                                    output_name = snakemake.output[0],
                                    verbosity = 3).run()
