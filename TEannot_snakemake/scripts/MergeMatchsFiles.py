
import os
import contextlib
from commons.tools.MergeMatchsFiles import MergeMatchsFiles
from os.path import join


if snakemake.params.allByAll:
    base_path = os.getcwd()

    MergeMatchsFiles(path = snakemake.params.path_dir,
                    fileType=snakemake.params.filetype,
                    outFileBaseName=snakemake.params.outFileBaseName,
                    allByAll=snakemake.params.allByAll,
                    clean=snakemake.params.clean,
                    singularity_img = snakemake.params.sif).run()
                    
    os.chdir(base_path)

else:
    base_path = os.getcwd()

    MergeMatchsFiles(path = snakemake.params.path_dir,
                    fileType=snakemake.params.filetype,
                    outFileBaseName=snakemake.params.outFileBaseName,
                    clean=snakemake.params.clean,
                    singularity_img = snakemake.params.sif).run()

    os.chdir(base_path)
