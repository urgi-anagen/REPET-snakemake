from commons.tools.GetSpecificTELibAccordingToAnnotation import GetSpecificTELibAccordingToAnnotation

GetSpecificTELibAccordingToAnnotation(fasta_file=snakemake.input.consensus_fasta_file,
                                    table_name=snakemake.input.stat_tab,
                                    verbose=snakemake.params.verbose,
                                    work_dir=snakemake.params.work_dir,
                                    log_name=snakemake.log[0]).run()
