from commons.tools.AlignTEOnGenomeAccordingToAnnotation import AlignTEOnGenomeAccordingToAnnotation

AlignTEOnGenomeAccordingToAnnotation(join_path_file=snakemake.input.path_file[0],
                                    genome_fasta=snakemake.input.genome,
                                    consensus_fasta=snakemake.input.consensus_ref,
                                    merge_same_path_id=snakemake.params.mergeSamePathId,
                                    out_file_name=snakemake.output[0],
                                    match_penalty=snakemake.params.matchPenalty,
                                    mismatch=snakemake.params.mismatch,
                                    gap_opening=snakemake.params.gapOpening,
                                    gap_extend=snakemake.params.gapExtend,
                                    gap_length=snakemake.params.gapLength,
                                    verbosity=snakemake.params.verbose,
                                    log_name = snakemake.log[0],
                                    singularity_img = snakemake.params.sif).run()
