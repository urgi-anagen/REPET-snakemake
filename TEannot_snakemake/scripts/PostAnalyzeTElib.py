
from commons.tools.AnnotationStats import AnnotationStats
from commons.core.seq.FastaUtils import FastaUtils

genome_fasta = FastaUtils.read_fasta(snakemake.input.genome_fasta_file)
genomeSize = sum(list(map(len,genome_fasta.values())))

AnnotationStats(analysisName="TE",
                seqFileName=snakemake.input.consensus_fasta_file,
                pathFileName=snakemake.input.join_path_file[0],
                genomeLength=genomeSize,
                statsFileName=snakemake.output.stat_per_TE,
                globalStatsFileName=snakemake.output.global_stat_per_TE,
                verbosity=snakemake.params.verbosity - 1,
                singularityImg=snakemake.params.sif,
                logName = snakemake.log[0]).run()
