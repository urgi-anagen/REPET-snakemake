from commons.tools.FilterAlign import FilterAlign
import pandas

df_threshold = pandas.read_csv(snakemake.input.threshold_file[0],sep='\t',header=None,index_col=0)
score = df_threshold.loc[snakemake.params.origin, 1]

FilterAlign(inFileName=snakemake.input.align_file,
            resetEvalues=snakemake.params.reset_evalues,
            minScore=score,
            verbosity=snakemake.params.verbose,
            log_name = snakemake.log[0]).run()
