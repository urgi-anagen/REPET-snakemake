from annot_pipe.LongJoinsForTEs import LongJoinsForTEs

LongJoinsForTEs(input_path_file=snakemake.input[0],
                output_file=snakemake.output[0],
                log_name=snakemake.log[0],
                verbose_var=snakemake.params[0]).run()
