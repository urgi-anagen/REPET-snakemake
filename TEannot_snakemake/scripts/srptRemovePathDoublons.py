#!/usr/bin/env python

##@file
# Remove duplicates within a path table.

import os
import sys
import getopt
import string

try:
    import ConfigParser
except:
    import configparser
import logging
from commons.core.coord.Path import Path
from commons.core.coord.PathUtils import PathUtils
from commons.core.coord.Range import Range
import pandas


def srptRemovePathDoublons(path_file, log_file="", verbose=0):
    df = pandas.read_csv(path_file, sep='\t', delimiter=None, header=None, index_col=None)
    data = dict()
    tmpFileName = "{}-nr".format(path_file)
    if log_file == "":
        log_filename = "removePathDoublons.log"
    else:
        log_filename = log_file

    if os.path.exists(log_filename):
        os.remove(log_filename)
    handler = logging.FileHandler(log_filename)
    formatter = logging.Formatter("%(asctime)s %(levelname)s: %(message)s")
    handler.setFormatter(formatter)
    logging.getLogger('').addHandler(handler)
    logging.getLogger('').setLevel(logging.DEBUG)
    logging.info("started")

    for index, row in df.iterrows():
        if row[1] not in data.keys():
            data[row[1]] = list()
        range_query = Range(seqname=row[1], start=row[2], end=row[3])
        range_subject = Range(seqname=row[4], start=row[5], end=row[6])
        new_path = Path(id=row[0], range_q=range_query, range_s=range_subject, e_value=row[7], score=row[8],
                        identity=row[9])
        data[row[1]].append(new_path)

    for c in data.keys():
        string = "processing query '{}'...".format(c)
        clist = data[c]
        string += "\ninitial nb of paths: {}".format(len(clist))

        clist = PathUtils.getPathListWithoutDuplicates(clist, True)
        string += "\nnb of paths without duplicates: {}".format(len(clist))


        if verbose > 1:
            logging.info(string)


        # write the non-redundant paths into the temporary file
        PathUtils.writeListInFile(clist, tmpFileName, "a")

    if verbose > 0:
        logging.info("finished")
        # print("END srptRemovePathDoublons")
        # sys.stdout.flush()

    return 0


srptRemovePathDoublons(snakemake.input[0], snakemake.log[0])
"""

if __name__ == "__main__":
    path_file = "/home/mwan/REPET/TEannot_sm/results_TEannot_DmelChr4/DmelChr4_TEdetect/Comb/DmelChr4_TEannot_Matcher_chk_allTEs.path"
    srptRemovePathDoublons(path_file)
"""
