#!/usr/bin/env python

import os
import sys
import getopt
import argparse


def CENmap2align(inFileName="", outFileName="", verbose=0):
    """
    Convert the output file from Censor ('map' format) into the 'align' format.
    """

    if inFileName == "":
        msg = "ERROR: missing input file (-i)"
        sys.stderr.write("{}\n".format(msg))
        help()
        sys.exit(1)
    if not os.path.exists(inFileName):
        msg = "ERROR: can't find input file '{}'".format(inFileName)
        sys.stderr.write("{}\n".format(msg))
        help()
        sys.exit(1)
    if outFileName == "":
        outFileName = "{}.align".format(inFileName)

    inFileHandler = open(inFileName, "r")
    outFileHandler = open(outFileName, "w")
    countLines = 0

    while True:
        line = inFileHandler.readline()
        if line == "":
            break
        countLines += 1

        tokens = line.split()
        qryName = tokens[0]
        qryStart = int(tokens[1])
        qryEnd = int(tokens[2])
        sbjName = tokens[3]
        strand = tokens[6]
        if strand == "d":  # if match with direct subject
            sbjStart = int(tokens[4])
            sbjEnd = int(tokens[5])
        elif strand == "c":  # if match with complement subject
            sbjStart = int(tokens[5])
            sbjEnd = int(tokens[4])

        similarity = float(tokens[7])
        BLASTscore = int(tokens[9])
        newScore = int(BLASTscore * similarity)
        percId = 100 * similarity

        string = "{}\t{}\t{}\t{}\t{}\t{}\t0.0\t{}\t{}\n".format(qryName, qryStart, qryEnd, sbjName, sbjStart, sbjEnd,
                                                                newScore, percId)

        outFileHandler.write(string)

    inFileHandler.close()
    outFileHandler.close()
    if verbose > 0:
        print("nb of lines: {}").format(countLines)

    return 0


CENmap2align(snakemake.input[0], snakemake.output[0])
"""
if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Convert the output file from Censor ('map' format) into the 'align' format.",
        formatter_class=argparse.RawTextHelpFormatter, add_help=True)
    parser.add_argument('-i', '--input', required=True, help="input file name (map format, output from Censor)")
    parser.add_argument('-o', '--output', default="",
                        help="output file name (align format, default=inFileName+'.align')")
    parser.add_argument('-v', '--verbose', default=0, type=int, help="verbose (default=0/1)")

    args = parser.parse_args()
    inFileName = args.input
    outFileName = args.output
    verbose = args.verbose
    main(inFileName, outFileName, verbose)
"""