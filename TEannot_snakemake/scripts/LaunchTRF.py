
import os
import contextlib
import shutil
from os.path import join
from commons.launcher.LaunchTRF import LaunchTRF



base = os.getcwd()
os.chdir(snakemake.params.outdir)

if not os.path.exists(os.path.basename(snakemake.input[0])):
    os.symlink(os.path.join(base,snakemake.input[0]),os.path.basename(snakemake.input[0]))

LaunchTRF(inFileName=os.path.basename(snakemake.input[0]),
            maxPeriod=snakemake.params.max_period,
            doClean=snakemake.params.clean,
            verbosity=3,
            singularity_img = snakemake.params.sif,
            log_name=snakemake.params.log
            ).run()



os.remove(os.path.basename(snakemake.input[0]))
os.chdir(base)
shutil.move(os.path.join(snakemake.params.outdir,snakemake.params.log),os.path.join(snakemake.params.log_dir,snakemake.params.log))
