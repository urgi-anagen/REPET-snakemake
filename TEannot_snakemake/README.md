# TEannot-snakemake
TEannnot Pipeline in snakemake based on REPET v3.1.
This pipeline annotates repeats in eukaryotic genomes.

## Directory tree
```
├── config
│   └── config.yaml
├── README.md
├── run_Snakemake.sh
├── scripts
│   ├── ATOGATA.py
│   ├── CENmap2align.py
│   ├── ChangeSequenceHeader.py
│   ├── conv_path_file_chunk_to_chr.py
│   ├── conv_set_file_chunk_to_chr.py
│   ├── Create_batch.py
│   ├── Cstfaf.py
│   ├── db_shuffle.py
│   ├── FilterAlign.py
│   ├── getTElib.py
│   ├── GFF3maker.py
│   ├── LaunchTRF.py
│   ├── LJFTE.py
│   ├── MergeMatchsFiles.py
│   ├── PostAnalyzeTElib.py
│   ├── Preprocess_genome_file.py
│   ├── RIH.py
│   ├── RMc2p.py
│   ├── RMcat2align.py
│   ├── runMreps.py
│   ├── srptRemovePathDoublons.py
│   ├── srptRemoveSpurious.py
│   └── USOM.py
├── Snakefile
├── Snakefile_conda
└── Snakefile_Singularity_CENSOR

```

### Scripts
python scripts used in the Snakefile

### config
The directory contains snakemake config file

```yaml
#Input files
# Path to genome file, must be a fasta
genome_file: /home/etbardet/Documents/REPET-snakemake/data/DmelChr4.fa

# Path to Apptainer images
container :
  TE_finder: /home/etbardet/REPET/REPET-snakemake/Singularity/REPET.sif
  CENSOR: /home/etbardet/REPET/REPET-snakemake/Singularity/CENSOR.sif

# for fasta checking
preprocess : True

conda_envs:
  TE_finder: "TE_finder"
  RepeatMasker: "RepeatMasker"
  Recon: "Recon"
  Easel: "Hmmer"
  MREP: "MREPS"
  TRF: "TRF"
  Piler: "Piler"
  MCL: "MCL"

# Clustering Methods
clustering_methods :
  grouper : True
  recon : True
  piler : True

params_batch :
  chunk_length : 200000
  chunk_overlap : 10000
  min_nb_seq_per_batch :  5

params_blaster:
  sensitivity : 0
  e_value : 1e-300
  fragment_cutter_length : 2000000

params_run_RmvPairAlignInChunkOverlaps :
  chunkLength: 200000
  chunkOverlap: 10000
  margin: 10
```

### run_Snakemake.sh
bash script to execute the snakemake.  The script will activate the snakemake environment and run the pipeline, it is assumed that
there is already a snakemake environment.

If the environment doesn't exist, you can create one, using this command line (it is assumed that conda already exists) :
```
conda create -c bioconda -c conda-forge --name snakemake snakemake snakedeploy python=3.9
```
You can also install the environment snakemake in other way, take a look to this documentation :
https://snakemake.readthedocs.io/en/stable/getting_started/installation.html

If you don't want to use the bash script to launch the snakemake pipeline, you can execute it with the command line below :
```
> conda activate snakemake
## if scheduler slurm
> snakemake --use-conda --use-singularity -p --configfile config/config.yaml -j 64 -s Snakefile -R all --cluster sbatch --max-jobs-per-second 5 --max-status-checks-per-second 5
## if no scheduler
> snakemake --use-conda --use-singularity -p --configfile config/config.yaml -c 30 -s Snakefile -R all --max-jobs-per-second 5 --max-status-checks-per-second 5

```

### Snakefile
**![](dag_TEannot.svg)**
To obtain the graph, launch the command below :
```
snakemake --use-conda --use-singularity -p --configfile config/config.yaml -c 30 -s Snakefile -R all --max-jobs-per-second 5 --max-status-checks-per-second 5 --latency-wait 3600 --rulegraph | dot -Tsvg > dag_TEannot.svg
```

## Launch TEannot snakemake
1. Fill the config file in the directory config/

2. Then launch [run_Snakemake.sh](run_Snakemake.sh) :
```
./run_Snakemake.sh
```
or you can launch directly the command line :
```
snakemake --use-conda --use-singularity -p --configfile config/config.yaml -c 30 -s Snakefile -R all --max-jobs-per-second 5 --max-status-checks-per-second 5 --latency-wait 3600
```

3. The results will be in the directory :
```
REPET-snakemake/TEannot_snakemake/results_TEannot_<fasta_name>
```

## Results
The main files in the results directory :
- **<fasta_name>_GFF3chr/*.gff3** : it's the annotation file in gff3 format that contains the annotated copies on the genome. There are one annotations files per sequences (chromosome or chunk).
- **<fasta_name>.globalAnnotStatsPerTE.txt** : It's a statistic file that give you some informations about your annotation
