# FROM biolabs/snakemake
FROM centos:centos7.9.2009

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

#RUN sudo apt-get update && sudo apt-get install -y build-essential libssl-dev uuid-dev libgpgme11-dev squashfs-tools libseccomp-dev pkg-config

#RUN conda install -c conda-forge singularity

RUN yum install -y debootstrap.noarch squashfs-tools make openssl-devel libuuid-devel gpgme-devel libseccomp-devel cryptsetup-luks wget epel-release git gcc gcc-c++ unzip PyYAML.x86_64
RUN yum -y groupinstall "Development Tools"
WORKDIR /usr/local
RUN wget https://go.dev/dl/go1.19.6.linux-amd64.tar.gz \
    && tar -xzvf go1.19.6.linux-amd64.tar.gz
    
#COPY packages/singularity-3.8.7.tar.gz /usr/local/singularity-3.8.7.tar.gz 
#RUN tar -xzvf singularity-3.8.7.tar.gz

COPY packages/apptainer-v1.2.0-rc.2.tar.gz /usr/local/apptainer-v1.2.0-rc.2.tar.gz
RUN tar -xzvf apptainer-v1.2.0-rc.2.tar.gz

WORKDIR /etc/profile.d
RUN touch go.sh \
    && echo "export PATH=$PATH:/usr/local/go/bin" >go.sh \
    && chmod 755 go.sh
RUN export PATH=$PATH:/usr/local/go/bin

WORKDIR /usr/local/apptainer
RUN echo $PATH
RUN export PATH=$PATH:/usr/local/go/bin && ./mconfig --with-suid
WORKDIR builddir
RUN make \
    && make install

WORKDIR /usr/local
RUN curl -O https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh && sh ./Miniconda3-latest-Linux-x86_64.sh -p /usr/local/anaconda -b
#RUN wget https://repo.anaconda.com/archive/Anaconda3-2022.10-Linux-x86_64.sh && bash Anaconda3-2022.10-Linux-x86_64.sh -b -p /usr/local/anaconda/
#RUN /usr/local/anaconda/bin/
RUN export PATH=/usr/local/anaconda/bin:$PATH \
    && conda config --append channels bioconda \
    && conda config --append channels conda-forge \
    && conda install -n base -c conda-forge mamba -y 
#conda install -n base -c conda-forge mamba
RUN ln -s /usr/local/anaconda/etc/profile.d/conda.sh /etc/profile.d/conda.sh && ln -s /usr/local/anaconda/etc/profile.d/mamba.sh /etc/profile.d/mamba.sh
#RUN export PATH=/usr/local/anaconda/bin:$PATH && mamba create -y -c conda-forge -c bioconda -n snakemake snakemake 
RUN export PATH=/usr/local/anaconda/bin:$PATH && conda create -c bioconda -c conda-forge --name snakemake snakemake snakedeploy build python=3.9
    
RUN /usr/local/anaconda/bin/conda init bash
WORKDIR /
#ENTRYPOINT ["source","/usr/local/anaconda/bin/activate","snakemake"]
#CMD ["/usr/local/anaconda/bin/conda","activate","snakemake"]
RUN echo "source activate snakemake" > ~/.bashrc
ENV PATH /usr/local/anaconda/envs/snakemake/bin:${PATH} 
