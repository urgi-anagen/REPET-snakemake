This file is an .md file containing instruction as to how to setup the TE_finder conda env

# BUILDING TE_FINDER

## 1) Setup

### Configure the defaults Conda channels

Conda-forge and bioconda need to be setup as default channels. If not the case, use the following command

``` bash
conda config --add channels conda-forge
conda config --add channels bioconda
 ```
### Uninstall libcppunit

If you have an already installed version of libcppunit, the package build will fail. Uninstall libcppunit, build the package, and then reinstall it later

```bash
sudo apt-get purge libcppunit
```

## 2) Build the local TE_finder package

Go to REPET-snakemake/packages/conda_build

```bash
cd REPET-snakemake/packages/conda_build
```

and then build the package using:

```bash
conda build recipe
#or if using boa
conda mambabuild recipe
```
WARNING: Building the package can take a LONG time (several hours in fact) if you use base conda.
It is advised to use boa whit conda mambabuild instead of base conda to get an acceptable build time

link to boa [here](https://boa-build.readthedocs.io/en/latest/mambabuild.html)

## 3) Install the built package  

```bash
conda create -n TE_finder -c local te_finder
```
Also make sure the TE_finder dependency are correctly installed

```bash
conda deactivate && conda activate TE_finder
conda install -c bioconda blast=2.12.0
conda install -c bioconda blast-legacy
```
check that the installation completed succefully by calling matcherthreads2.32 in your shell


#### if you cannot clone TE_finder from gitlab

go to the _packages_ directory and uncompress TE_finder.

```bash
cd ../
tar -xvzf te_finder.tar.gz
```

 Then open the meta.yaml file in _packages/conda_build/Recipe_ and replace the following lines

```yaml
source:
  git_url: https://forgemia.inra.fr/urgi-anagen/te_finder.git
  git_rev: {{ branch }}
```
by these lines (don't forget to put a valid path insetad of the {})

```yaml
source:
  path: {absolute path to your REPET-snakemake Folder}/Packages/te_finder
```
You should now be able to build

## 4) Verify the installation

Call matcherThreads2.32

```
matcherThreads2.32 -h
```

If you get the matcherThreads help, you are good to go

TE_finder's conda package will hopefully be published on bioconda in the near future, so you won't have to build the package localy anymore 

# Setting up the REPETRepbase

This section is here as a reminder in case you did not follow the steps to setup the conda version of RepeatMasker. i you already did this you can skip a head  

You need to replace the default RepeatMasker library with the REPET repbase for the pipelines to work

## Get the REPET repbase

It is located in the REPET-snakemake/packages dir

```bash
cd REPET-snakemake/packages
```

Locate the *RepBaseRepeatMaskerEdition-20170127.tar.gz* file and untar it, you should get a file called *RMRBSeqs.embl*

```bash
tar -xvf ./RepBaseRepeatMaskerEdition-20170127.tar.gz
```

###### Locate your RepeatMasker environnement directory

```bash
cd /{Your_conda_or_anaconda3_dir}/env/{name_of_your_RM_env}/share/Repeatmasker/library
#i.e ~/anaconda3/envs/RepeatMasker/share/RepeatMasker/library
```

###### Remove/Rename the old library

``` bash
mv RepeatMaskerLib.embl RepeatMaskerLib_old.embl
#or
rm RepeatMaskerLib.embl
```

###### Copy the RMRBSeqs.embl extracted in the RepeatMasker library dir

```bash
cp /{REPET_path}/REPET-snakemake/packages/RMRBSeqs.embl /{Your_conda_or_anaconda3_dir}/env/{name_of_your_RM_env}/share/Repeatmasker/library/RMRBSeqs.embl
```

##### Configuring RepeatMasker

You need to configure your installation of RepeatMasker

```bash
cd ../
perl ./configure
#if you don't have a perl interpretor, the Recon environnment has one. Activate the env to use it
```

Once prompted, enter the following
```
{Your_conda_or_anaconda3_dir}/env/{name_of_your_RM_env}/bin/perl
{Your_conda_or_anaconda3_dir}/env/{name_of_your_RM_env}
{Your_conda_or_anaconda3_dir}/env/{name_of_your_RM_env}/bin
2
{your_conda/anaconda3_dir}/env/{name_of_your_RM_env}/bin
Y
5
```
RepeatMasker is now operationnal
