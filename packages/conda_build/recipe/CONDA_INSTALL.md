# Description

This directory (the Recipe directory) contains all the files that you need to build a working TE_finder conda packages

# Dependencies

Have conda or anaconda3 installed

Have the conda-forge and bioconda channels setup as default channels. if it is not the case, use the following command

    conda config --add channels conda-forge
    conda config --add channels bioconda

# Build instruction

Use the "Conda build" command on the Recipe directory

    conda build Recipe

The local package thus created will be named "te_finder"

Wait until the build is finished, then install the local package that you created on one of your conda environnement or create a brand new one for te_finder:

    conda install -c local "te_finder"

or

    conda create -n TE_finder -c local "te_finder"

**NB:** Note that the recipe will instruct conda to pull te_finder from the repository on the [ForgeMIA](https://forgemia.inra.fr/urgi-anagen/te_finder.git). If you do not have permission clone this project, then you will have to build from the te_finder.tar.gz archive

First, uncompress TE_finder

```bash
cd ../
tar -xvzf te_finder.tar.gz
```

 Then open the meta.yaml file in _conda_build/Recipe_ and replace the following lines

```yaml
source:
  git_url: https://forgemia.inra.fr/urgi-anagen/te_finder.git
  git_rev: {{ branch }}
```
by these lines (don't forget to put a valid path instead of the {})

```yaml
source:
  path: {absolute path to your REPET-snakemake Folder}/Packages/te_finder
```
You should now be able to build
