# REPET Snakemake

REPET Snakemake repository contains TEdenovo and TEannot pipelines to detect and annotate repeats in eukaryotic genomes.


## Disclaimer

This version is a beta version. while I did my best to provide a reliable version of TEdenovo and TEannot, using these pipelines will probalbly not be a bug free experience.
If you encounter an error, do not hesitate to contact us to signal the problem

Only The first 4 steps of TEdenovo are available in this version of REPET, to run PASTEC, you will need to use the [docker image](https://forgemia.inra.fr/urgi-anagen/wiki-repet/-/wikis/tutorial-with-Docker-image-of-REPET) for now. PASTEC will be properly added in a future update (as of writing, the reimplementation is mostly finished, but still need some adjustements before release)
TEannot is fully implemented
For reference, information on the pipelines differents steps can be found [here](https://urgi.versailles.inra.fr/Tools/REPET/)

NCBI blast is the only Blast type supported for now

You need to install and setup [git-lfs](https://git-lfs.com/), or crucial library will be missing when you clone this project

[The Singularity images hosted on our git's container registry](https://forgemia.inra.fr/urgi-anagen/REPET-snakemake/container_registry) are known to have some segfault issues if you use them on debian
If you encounter these segfaults, do not hesitate to describe your problem to us and send you logs.
Building the Singularity images yourself has been known as a potential fix. All the required packages are present in this release.
Alternatively, you may choose to go around the problem by running the pipelines whit conda environements. Instruction as to how to do that are contained in this README (feedback on this method in particullar would be appreciated)

## Installations

### Apptainer
To install Apptainer, please check the [doc](https://github.com/apptainer/apptainer/blob/main/INSTALL.md)
NB: The official singularity images are stored on our gitlab in the container registry. To pull them, you need a version of singularity that can push and pull into ORAS registry. You thus need Singularity 3.5.0 or the latest version

### Snakemake
You need snakemake version > 7.8.2.\
You can install snakemake using mamba : Check the [doc](https://snakemake.readthedocs.io/en/stable/getting_started/installation.html).
Or you can install snakemake using conda env, example :
```bash
conda create -c bioconda -c conda-forge --name snakemake snakemake snakedeploy python=3.9
```

## Directory tree
```
.
├── data
├── devTools
│   ├── config_bench
│   ├── data_bench
│   └── __pycache__
├── packages
│   └── conda_build
│       └── recipe
├── script_repet
│   ├── __pycache__
│   └── repet_tools
│       ├── annot_pipe
│       ├── commons
│       │   ├── core
│       │   │   ├── checker
│       │   │   ├── coord
│       │   │   ├── Hmm
│       │   │   ├── parsing
│       │   │   ├── seq
│       │   │   ├── stat
│       │   │   └── utils
│       │   ├── launcher
│       │   └── tools
│       ├── denovo_pipe
│       ├── dist
│       ├── __pycache__
│       ├── repet_tools.egg-info
│       └── SMART
├── Singularity
├── TEannot_snakemake
│   ├── config
│   └── scripts
└── TEdenovo_snakemake
    ├── config
    └── scripts

```

### Data
Species genomes (fasta files) and consensus files for test
* DmelChr4.fa : _Drosophila melanogaster_ chromosome 4 fasta file
* all_consensus_repet.fa  : _Drosophila melanogaster_ consensus fasta file. This fasta comes from a TEdenovo (REPET v3.0) on chromosome 4 of Drosophila.

### DevTools
This directory is dedicated to benchmarking.
I've done my best to gather together the elements that are important for benchmarking, based on the REPET v 3.0
documentation and on what I think is worth doing.
There are two directories :
* config_bench : this directory contains the config files for launching snakemake  
* data_bench : data files for benchmarks

Before running the benchmarks, please read the [Benchmarks_tuto.txt](devTools%2FBenchmarks_tuto.txt) first.

### Script repet
Repet_tools : python scripts from REPET v3.1 used in the pipeline TEdenovo, TEannot and PASTEC pipelines
Scripts have been modified to adapt to Snakemake and remove the link to MySQL.

### Packages
All the packages used for creating the Apptainer image:

* Bash script to install Bioperl and to configure RepeatMasker (one for Debian and the other one for CentOS)
  - bioperl.sh                             
  - RepBaseRepeatMaskerDebian.sh
  - RepBaseRepeatMasker.sh

* RPM files
  - mreps-2017_03_08-stable.x86_64.rpm  
  - piler-2017_03_08-stable.x86_64.rpm
  - trf-2017_03_08-stable.x86_64.rpm

* Tar.gz packages
  - blast-2.2.26-ia32-linux.tar.gz
  - blast2.linux26-i686.tar.gz             
  - RECON-1.08.tar.gz
  - censor-norb-4.2.29.tar.gz              
  - RepBaseRepeatMaskerEdition-20170127.tar.gz
  - hmmer-3.1b2-linux-intel-x86_64.tar.gz  
  - RepeatMasker-open-4-0-6.tar.gz
  - rmblast-2.10.0+-x64-linux.tar.gz
  - ncbi-blast-2.10.0+-x64-linux.tar.gz
  - te_finder.tar.gz

* conda_build directory that contains the recipe used to build a conda environement for TE_finder
  - Recipe

### Singularity
The directory contains the Apptainer recipe.
This image is used by the Snakefile to execute :
* Recon
* Piler
* CENSOR
* Blaster
* Grouper
* Matcher
* Repeat Masker
* NCBI Blast

There are 3 main recipes :
- REPET.def : The REPET Apptainer image recipe
- REPET_localbuild.def: REPET Apptainer image recipe, it install TE_finder from a tarfile instead of cloning it form the git. Use it if you cannot clone TE_finder
- CENSOR.def : Apptainer image used to run CENSOR


### TEdenovo_snakemake
Pipeline TEdenovo

### TEannot_snakemake
Pipeline TEannot

## Usage
### Prerequisite
First, activate your snakemake environment :
```bash
conda activate snakemake
```

Then install repet_tools :
```bash
cd REPET-snakemake/script_repet/repet_tools
# if the package build is not install, please install it : pip install build --user
./install_package.sh
```

### Running the pipelines using Singularity images

#### REPET.sif

This is the main image

WARNING: as of now, this image is known to segfaults if used on Debian.  

The image should be hosted on [REPET-Snakemak's gitlab](https://forgemia.inra.fr/urgi-anagen/REPET-snakemake), in the container registry.

To pull it, do

```bash
> singularity pull Singularity/REPET.sif oras://registry.forgemia.inra.fr/urgi-anagen/repet-snakemake:REPET_image_dev
```
If you cannot pull it, you still can build the images localy:
NB: check first if you can clone [TE_finder from the ForgeMIA](https://forgemia.inra.fr/urgi-anagen/te_finder), if unable to, use the REPET_alt recipe to build

```bash
> cd REPET-snakemake/
> sudo singularity build Singularity/REPET.sif Singularity/REPET.def
#or
> sudo singularity build Singularity/REPET.sif Singularity/REPET_alt.def
#if you do not have admin right, use the --fakeroot option to build
#singularity build --fakeroot Singularity/REPET.sif Singularity/REPET.def
```

#### CENSOR.sif

TEannot used to use the CENSOR package. As of REPET_V4, we chose to remove it's use from the defaults options. You can still enable CENSOR use if you whish to still use it in TEannot by following these steps

If you want to run TEannot whit CENSOR, you will also have to pull/build the corresponding images

```bash
#to pulll from the ORAS registry
> singularity pull Singularity/CENSOR.sif oras://registry.forgemia.inra.fr/urgi-anagen/repet-snakemake:CENSOR_image_dev
#to build locally
> cd REPET-snakemake/
> sudo singularity build Singularity/CENSOR.sif Singularity/CENSOR.def
```
You also need to swap the TEannot Snakefile:

```bash
> cd REPET-snakemake/TEannot_snakemake
> mv ./Snakefile ./Snakefile_Singularity_noCENSOR
> mv ./Snakefile_Singularity_CENSOR ./Snakefile
```

### Running the pipelines using conda environment

As an alternative to the use of singularity images, you can use conda environnement to handle all the dependency.

WARNING:
- The RepeatMasker found on conda is not an official version maintained by the ISB. You can run the pipelines
using the conda version of RepeatMasker, but know it is not an official version, and will require some fidling to get it to work as intended
- CENSOR has to be mlaunched trought an Singularity image

##### Setup of the conda environnements

You can create the required conda environnement with the following commands

```bash
conda create -n RepeatMasker -c bioconda repeatmasker=4.0.6 &&
conda create -n Recon -c bioconda recon &&
conda create -n Hmmer -c bioconda easel=0.45 hmmer=3.1b2  &&
conda create -n MREPS -c bioconda mreps=2.6.01 &&
conda create -n TRF -c bioconda TRF=4.09.1 &&
conda create -n Piler -c bioconda piler &&
conda create -n MCL -c bioconda mcl
```

##### Build the TE_finder package

For now, the conda TE_finder conda package is not publicly available, you need to build it locally

You may want to install [boa](https://boa-build.readthedocs.io/en/latest/getting_started.html) to get access to mambabuild. Building the TE_finder recipe whit base conda is possible, but it can take a LONG time (hours in fact)
Using mambabuild can shorten the build time MASSIVELY  

```bash
cd REPET-snakemake/packages/conda_build
conda build recipe
#or if using boa
conda mambabuild recipe
```
Wait until the build is finished, then install the package from your local channel  

```bash
conda create -n TE_finder -c local te_finder
```
Get into your TE_finder environnement and make sure _blast_ version 2.14 and _Blast-legacy_ are installed
```bash
conda deactivate && conda activate TE_finder
conda install -c bioconda blast=2.14.0
conda install -c bioconda blast-legacy
```  
##### Setting up the REPETRepbase

You then need to replace the default RepeatMasker library with the REPET repbase for the pipeline to work

###### Get the REPET repbase

 REPET repbase is located in the REPET-snakemake/packages directory

```bash
cd REPET-snakemake/packages
```

Locate the *RepBaseRepeatMaskerEdition-20170127.tar.gz* file and untar it, you should get a file called *RMRBSeqs.embl*

```bash
tar -xvf ./RepBaseRepeatMaskerEdition-20170127.tar.gz
```

###### Locate your RepeatMasker environnement directory

It is locate in you conda/anaconda/minconda environnement dir, which should be located somewhere in your home directory

```bash
> home/user/{Your_conda_or_anaconda3_dir}/env/{name_of_your_RM_env}/share/Repeatmasker/library
#in my case /home/etbardet/anaconda3/envs/RepeatMasker/
```

###### Remove/Rename the old library

``` bash
mv RepeatMaskerLib.embl RepeatMaskerLib_old.embl
#or
rm RepeatMaskerLib.embl
```

###### Copy the RMRBSeqs.embl previously extracted in the RepeatMasker library directory

```bash
cp /{REPET_path}/REPET-snakemake/packages/RMRBSeqs.embl /{Your_conda_or_anaconda3_dir}/env/{name_of_your_RM_env}/share/Repeatmasker/library/RMRBSeqs.embl
```

##### Configuring RepeatMasker

You then need to configure your installation of RepeatMasker

WARNING: You will have to setup RepeatMasker again each time you install/remove a package or change the environnement's name   

You also need a perl interpretor. If you do not have one, the Recon environnment has a perl interpretor available

```bash
cd ../
perl ./configure
```

Once prompted, enter the following
```
{Your_conda_or_anaconda3_dir}/env/{name_of_your_RM_env}/bin/perl
{Your_conda_or_anaconda3_dir}/env/{name_of_your_RM_env}
{Your_conda_or_anaconda3_dir}/env/{name_of_your_RM_env}/bin
2
{your_conda/anaconda3_dir}/env/{name_of_your_RM_env}/bin
Y
5
```
RepeatMasker should now be operationnal

### Using TEdenovo
Go to the directory TEdenovo_snakemake

```bash
cd REPET-snakemake/TEdenovo_snakemake/
```

Fill the config file in the directory config/


```yaml
# config/config.yaml

#Input files
# Path to genome file, must be a fasta
genome_file: /home/etbardet/REPET/REPET-snakemake/data/DmelChr4.fa

# Path to Apptainer images
container :
  REPET_image: /home/etbardet/REPET/REPET-snakemake/Singularity/REPET.sif

# for fasta checking
preprocess : True

#Put the name of the conda environement you will use to call TEdenovo's dependency
conda_envs:
  TE_finder: "TE_finder"
  RepeatMasker: "RepeatMasker"
  Recon: "Recon"
  Hmmer: "Hmmer"
  MREP: "MREPS"
  TRF: "TRF"
  Piler: "Piler"
  MCL: "MCL"

# Clustering Methods
clustering_methods :
  grouper : True
  recon : True
  piler : True

params_batch :
  chunk_length : 200000
  chunk_overlap : 10000
  min_nb_seq_per_batch :  5

params_blaster:
  sensitivity : 0
  e_value : 1e-300
  fragment_cutter_length : 2000000

params_run_RmvPairAlignInChunkOverlaps :
  chunkLength: 200000
  chunkOverlap: 10000
  margin: 10

```

If running the pipeline throught conda, swap the snakefiles

```bash
mv Snakefile Snakefile_singularity
mv Snakefile_conda Snakefile
```

Then launch [run_Snakemake.sh](TEdenovo_snakemake%2Frun_Snakemake.sh):
```bash
./run_Snakemake.sh
```
or you can launch directly the command line :
```bash
snakemake --use-conda --use-singularity -p --configfile config/config.yaml -c 30 -s Snakefile -R all --max-jobs-per-second 5 --max-status-checks-per-second 5 --latency-wait 240
```

The results will be in the directory :
```
REPET-snakemake/TEdenovo_snakemake/results_TEdenovo_<fasta_name>
```

NB: This version of TEdenovo only run on the first four steps of TEdenovo REPET (if you want more informations about [TEdenovo REPET](https://urgi.versailles.inrae.fr/Tools/REPET/TEdenovo-tuto)).
The last 2 steps are in the process of being reimplemented.


### TEannot
Go to the directory TEannot_snakemake
```bash
cd REPET-snakemake/TEannot_snakemake/
```

Fill the config file in the directory config/
```yaml
# config/config.yaml

#Input files
# Path to genome file, must be a fasta
genome_file: /home/etbardet/REPET/REPET-snakemake/data/DmelChr4.fa
# Path to TE librairy, must be a fasta
consensus_lib: /home/etbardet/REPET/REPET-snakemake/data/all_consensus_repet.fa

# Path to Apptainer images
container :
  REPET_image: /home/etbardet/REPET/REPET-snakemake/Singularity/REPET.sif
  CENSOR_image : /home/etbardet/REPET/REPET-snakemake/Singularity/CENSOR.sif

program_align_sequences:
  RepeatMasker: True
  Blaster: True
  Censor: False


conda_envs:
  TE_finder: "TE_finder"
  RepeatMasker: "RepeatMasker"
  Recon: "Recon"
  Easel: "Hmmer"
  MREP: "MREPS"
  TRF: "TRF"
  Piler: "Piler"
  MCL: "MCL"


params_batch :
  chunk_length : 200000
  chunk_overlap : 10000
  min_nb_seq_per_batch :  5

params_blaster:
  sensitivity : 0
  e_value : 1e-300
  fragment_cutter_length : 2000000

params_run_RmvPairAlignInChunkOverlaps :
  chunkLength: 200000
  chunkOverlap: 10000
  margin: 10

```

If running the pipeline throught conda, swap the snakefiles

```bash
mv Snakefile Snakefile_singularity
mv Snakefile_conda Snakefile
```

Then launch [run_Snakemake.sh](TEannot_snakemake%2Frun_Snakemake.sh):
```bash
./run_Snakemake.sh
```
or you can launch directly the command line :
```bash
snakemake --use-conda --use-singularity -p --configfile config/config.yaml -c 30 -s Snakefile -R all --max-jobs-per-second 5 --max-status-checks-per-second 5 --latency-wait 240
```
