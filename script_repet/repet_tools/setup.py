from setuptools import setup, find_packages
import os

PACKAGE_NAME = "repet_tools"


def read_package_variable(key):
    """Read the value of a variable from the package without importing."""
    module_path = os.path.join(PACKAGE_NAME, '__init__.py')
    with open(module_path) as module:
        for line in module:
            parts = line.strip().split(' ')
            if parts and parts[0] == key:
                return parts[-1].strip("'")
    assert 0, "'{0}' not found in '{1}'".format(key, module_path)



setup(
    name="repet_tools",
    version='0.0.1',
    license='CeCILL',
    author='URGI',
    #install_requires=['numpy<1.21,>=1.17' ],
    author_email='urgi-repet@inrae.fr',
    description='REPET tools',
    packages=find_packages()
)
