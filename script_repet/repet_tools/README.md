Create the package :
python3 -m build
If you want to test your application on your local machine, you can install the .whl file using pip:
python3 -m pip install dist/repet_tools-0.0.1-py3-none-any.whl
To re-install :
python3 -m pip install dist/repet_tools-0.0.1-py3-none-any.whl --force-reinstall
