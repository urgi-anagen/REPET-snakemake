import subprocess
class DetectPolyA(object):

    def __init__(self, inFile, logFileName, identity=0.9, minlen=10, match=10, mismatch=16, gapopen=32, gapextend=32, extlen=0.5, minsize=10):
        self.inFile = inFile
        self.logFileName = logFileName
        self.identity = identity
        self.minlen = minlen
        self.match = match
        self.mismatch = mismatch
        self.gapopen = gapopen
        self.gapextend = gapextend
        self.extlen = extlen
        self.minsize = minsize

    def run(self):
        print ("Detecting PolyA tails")
        cmd = "polyAtail "
        cmd += self.inFile
        cmd += " > {} ".format(self.logFileName)
        process = subprocess.run(cmd, shell = True)
        if process.returncode != 0:
            print("ERROR when launching ",cmd)
