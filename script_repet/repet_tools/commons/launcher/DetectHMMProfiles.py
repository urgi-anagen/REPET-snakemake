import subprocess
import os
from commons.core.seq.BioseqUtils import BioseqUtils
from commons.core.utils.FileUtils import FileUtils
from commons.core.Hmm.HmmOutput2alignAndTransformCoordInNtAndFilterScores import HmmOutput2alignAndTransformCoordInNtAndFilterScores

class DetectHMMProfiles(object):


    def __init__(self, fastaFileName, profilesBank, doClean=True, maxProfilesEvalue=10, logFileName="HMMfinder.log", verbose=1):
        self.fastaFileName = fastaFileName
        self.profilesBank = profilesBank
        self.doClean = doClean
        self.maxProfilesEvalue = maxProfilesEvalue
        self.logFileName = logFileName
        self.verbose = 1

    def SetArgumentsFromCommmandLine(self):
        ## TODO: implement this, not critical, but nice to have
        print ("unimplemented")

    def prepare_banks(self):
        cmd = "hmmpress"
        cmd += " -f"
        cmd += " " + self.profilesBank #add a blank space
        cmd += " >> {}".format(self.logFileName+"_Bank_preperation.log")

        process = subprocess.run(cmd, shell = True)
        if process.returncode != 0:
            raise Exception("ERROR when launching", cmd)
        print ("banks prepared")

    def run(self):
        if self.verbose > 0:
            print("Detecting hmm profiles...")


        if not (os.path.exists("{}.h3m".format(self.profilesBank)) \
            and os.path.exists("{}.h3i".format(self.profilesBank)) \
            and os.path.exists("{}.h3f".format(self.profilesBank)) \
            and os.path.exists("{}.h3p".format(self.profilesBank))) :
            if self.verbose > 0:
                print("Hmm banks not prepared, launching preperation steps")
            self.prepare_banks()
        else:
            print("banks already prepared, skipping banks prep")

        IDK = self.fastaFileName
        lBioseqs = BioseqUtils.extractBioseqListFromFastaFile(IDK)
        lTranslatedBioseqs = BioseqUtils.translateBioseqListInAllFrames(lBioseqs)
        lTranslatedBioseqsWithoutStop = BioseqUtils.replaceStopCodonsByXInBioseqList(lTranslatedBioseqs)
        BioseqUtils.writeBioseqListIntoFastaFile(lTranslatedBioseqsWithoutStop, "{}_translated".format(self.fastaFileName))
        #i don't know what is happening here

        cmd = "hmmscan"
        cmd += " -o {}_tr.hmmScanOut".format(self.fastaFileName)
        cmd += " --domtblout {}_tr.hmmScanOutTab".format(self.fastaFileName)
        cmd += " --noali -E {}".format(self.maxProfilesEvalue)
        cmd += " --cpu 1"
        cmd += " {}".format(self.profilesBank)
        cmd += " {}_translated".format(self.fastaFileName)

        #maybe put this in a function in BioseqUtils or FileUtils ? I get reused a lot
        print ("launching hmmscan")
        process = subprocess.run(cmd, shell = True)
        if process.returncode != 0:
            raise Exception("ERROR when launching", cmd)
        print ("hmmscan done")

        profilesBankName = os.path.basename(self.profilesBank)
        if self.verbose > 0:
            print("checking if correction works, profilesBankName is:", profilesBankName)

        print("Launching HmmOutput2alignAndTransformCoordInNtAndFilterScores")

        HmmOutput2alignAndTransformCoordInNtAndFilterScores(
        inFileName="{}_tr.hmmScanOutTab".format(self.fastaFileName),
        outFileName="{}_Profiles_{}.align".format(self.fastaFileName, profilesBankName),
        consensusFileName=self.fastaFileName,
        program= "hmmscan",
        verbose=2).run()

        """
        cmd = "python3 scripts/HMMfinder/HmmOutput2alignAndTransformCoordInNtAndFilterScores_script.py" #need to redo file perms and get the python interpreter path setup
        #cmd = "HmmOutput2alignAndTransformCoordInNtAndFilterScores_script.py"
        cmd += " -i {}_tr.hmmScanOutTab".format(self.fastaFileName)
        cmd += " -o {}_Profiles_{}.align".format(self.fastaFileName, profilesBankName)
        cmd += " -T {}".format(self.fastaFileName)
        cmd += " -p hmmscan"
        cmd += " -c"


        print("Launching HmmOutput2align")
        process = subprocess.run(cmd, shell = True)
        if process.returncode != 0:
            raise Exception("ERROR when launching", cmd)
        print("HmmOutput2align done")
        """
        """
        cmd = "matcherThreads2.31"
        cmd += " -m {}_Profiles_{}.align".format(self.fastaFileName, profilesBankName)
        cmd += " -j"
        cmd += " -E 10"
        cmd += " -L 0"
        cmd += " -v 1 >> {}".format(self.logFileName)

        print("Launching matcherThreads")
        process = subprocess.run(cmd, shell = True)
        if process.returncode != 0:
            raise Exception("ERROR when launching '%s'" % cmd)
        print("matcherThreads done")

        """

        if self.doClean:
            print ("cleaning")
            #os.remove(logFileName)
            os.remove("{}_translated".format(self.fastaFileName))
            os.remove("{}_tr.hmmScanOut".format(self.fastaFileName))
            #os.remove("{}_Profiles_{}.align".format(self.fastaFileName, profilesBankName))
            #os.remove("{}_Profiles_{}.align.clean_match.param".format(self.fastaFileName, profilesBankName))
            #os.remove("{}_Profiles_{}.align.clean_match.bed".format(self.fastaFileName, profilesBankName))
            #os.remove("{}_Profiles_{}.align.clean_match.gff3".format(self.fastaFileName, profilesBankName))
            #FileUtils.removeFilesByPattern("*.hmm.h3*")
