import subprocess
from commons.core.utils.FileUtils import FileUtils

class PrepareBanks(object):
    """
    Is supposed to prepare Banks for blaster
    Was not tested, does not work, and will probably get scrapped
    Use at your own risks
    """

    def __init__(self, inFile="", outFile="", blast="blastn", logFileName="", blastType="ncbi", verbosity=4, doClean=True, TEversion="2.32"):
        self.inFile = inFile
        self.outFile = outFile
        self.blast = blast
        self.blastType = blastType
        self.logFileName = logFileName
        self.verbosity = verbosity
        self.doClean = doClean
        self.TEversion = TEversion

    def check_args(self):

        if self.logFileName=="":
            self.logFileName = "{}_bank_creation.log".format(self.inFile)
            if self.verbosity > 3:
                print("Log Filename not specified, defaulting to " + self.logFileName)

        if not FileUtils.isRessourceExists(self.inFile):
            raise Exception("ERROR: Input file does not exist" )
        elif FileUtils.isEmpty(self.inFile) :
            raise Exception("ERROR: empty input file")

    def run(self):
        self.check_args()
        if self.verbosity > 0:
            print("Creating bank for {}".format(self.inFile))

        cmd = "blaster"+self.TEversion
        cmd += " -q {}".format(self.inFile) #query bank
        cmd += " -B {}".format(self.outFile) #output prefix
        cmd += " -r" #force re-run
        cmd += " -P" #prepare banks only
        cmd += " -v {} >> {}".format(self.verbosity - 2, self.logFileName)

        if self.verbosity > 3:
            print("bank creation command check:", cmd)

        process = subprocess.run(cmd, shell = True)
        if process.returncode != 0:
            print("ERROR when launching ",cmd)

        if self.doClean:
            FileUtils.removeFilesByPattern(self.inFile+"_cut.*")
            FileUtils.removeFilesByPattern(self.inFile+"Nstretch.map") #don't know what an Nstretch.map is or does.
