import subprocess




class DetectTRSearch(object):

    def __init__(self, fastaFileName, logFileName, identity=0.8, minlen=10, match=10, mismatch=16, gapopen=32, gapextend=32, extlen=0.5, minsize=10, verbose=0, singularity=""):
        self.fastaFileName = fastaFileName
        self.logFileName = logFileName
        self.identity = identity
        self.minlen = minlen
        self.match = match
        self.mismatch = mismatch
        self.gapextend = gapextend
        self.gapopen = gapopen
        self.extlen = extlen
        self.minsize = minsize
        self.verbose = verbose
        self.singularity = singularity

    def run(self):
        print ("Detect terminal repeats...")

        if self.singularity=="":
            cmd = "TRsearch"
        else:
            cmd = "singularity exec {} TRsearch".format(self.singularity)
        #cmd += " -i {}".format(identity)#Float is causing trouble and provoking crash
        cmd += " -l {}".format(self.minlen)
        cmd += " -m {}".format(self.match)
        cmd += " -d {}".format(self.mismatch)
        cmd += " -g {}".format(self.gapopen)
        cmd += " -e {}".format(self.gapextend)
        cmd += " -x {}".format(self.extlen)
        cmd += " -s {}".format(self.minsize)
        cmd += " " + self.fastaFileName
        #cmd += " > " + logFileName #if in the snakemake, will be completely useless, logfile can be handled by snakemake itself
        if self.verbose > 0:
            print ("DetectTRSearchw will run this command: \n", cmd, "\n")
        process = subprocess.run(cmd, shell = True)
        if process.returncode != 0:
            print("ERROR when launching DetectTRsearch whit the following command: ", cmd)
