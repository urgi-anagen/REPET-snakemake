import os
import subprocess
from pathlib import Path
from commons.core.utils.FileUtils import FileUtils

class LaunchMatcherThreads(object):
    """
    Script that launches MatcherThreads, is used as a followupto LaunchBlaster in PASTEC
    """

    def __init__(self, hmm=False, inFile="", outFile="", logFile="", version="2.32", bankFile ="", doClean=True, singularity=""):
        self.hmm = hmm
        self.inFile = inFile
        self.outFile = outFile
        self.logFile = logFile
        self.version = version
        self.bankFile = bankFile
        self.doClean = doClean
        self.singularity = singularity

    def BuildCommandLine(self):

        if self.hmm:
            if self.singularity and self.version=="2.25":
                cmd="singularity exec {} matcherThreads". format(self.singularity)
            elif self.singularity:
                cmd="singularity exec {} matcherThreads{}". format(self.singularity, self.version)
            else:
                cmd = "matcherThreads" + self.version
            cmd += " -m "+ self.inFile
            cmd += " -j"
            cmd += " -E 10"
            cmd += " -L 0"
            cmd += " -v 1 >> {}".format(self.logFile)
            return cmd

        else:
            cmd = "matcherThreads"+self.version
            cmd += " -m {}".format(self.inFile)
            cmd += " -j"
            cmd += " -v 1 >> {}".format(self.logFile)
            return cmd

    def Cleanup(self):
        if self.doClean and self.hmm:
            print ("cleaning")
            #os.remove(logFileName)
            #os.remove("{}_translated".format(self.inFile))
            #os.remove("{}_tr.hmmScanOut".format(fself.inFile))
            #os.remove("{}_Profiles_{}.align".format(self.inFile, profilesBankName))
            os.remove(self.inFile+".clean_match.param")
            os.remove(self.inFile+".clean_match.bed")
            os.remove(self.inFile+".clean_match.gff3")
            FileUtils.removeFilesByPattern("*.hmm.h3*")
        elif self.doClean:
            os.remove(self.inFile+".param")
            os.remove(self.inFile+".align")
            os.remove(self.inFile+".align.clean_match.param")
            os.remove(self.inFile+".align.clean_match.bed")
            os.remove(self.inFile+".align.clean_match.gff3")

    def run(self):

        if FileUtils.isEmpty(self.inFile) and not self.hmm: #in case the file is empty, can happen if no pertinent match found by blaster
            print ("WARNING: .align file is empty, \n Matcherthreads will create an empty file to make the programm continue")
            open (self.inFile+".clean_match.path",'w').close

        else:
            cmd = self.BuildCommandLine()
            print("Launching matcherThreads")
            process = subprocess.run(cmd, shell = True)
            if process.returncode != 0:
                raise Exception("ERROR when launching matcherThreadswhit following commmand: \n", cmd, "\n")
            print("matcherThreads done")
            self.Cleanup()
