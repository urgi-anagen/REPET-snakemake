import os
import subprocess
from pathlib import Path
from commons.core.utils.FileUtils import FileUtils
from commons.launcher.LaunchMatcherThreads import LaunchMatcherThreads


class LaunchBlaster(object):
    """
    Script that launches blaster, is mostly used in PASTEC
    """

    def __init__(self, TEfile, bank, logFileName, blast, acronym, blastType, TEversion="2.31", doClean=True ,MoveResults=True, verbose=1, matcherfollow=False, singularity=""):

        self.singularity = singularity
        self.TEfile = TEfile
        self.bank = bank
        self.logFileName = logFileName
        self.blast = blast
        self.acronym = acronym
        self.blastType = blastType
        self.TEversion = TEversion
        self.doClean = doClean
        self.MoveResults = MoveResults
        self.verbose = verbose
        self.matcherfollow = matcherfollow
        self.singularity = singularity

    def blaster(self):
        """
        TODO: Get rid of self.TEversion, tefinder2.32 is stable now
        """

        bankname=os.path.basename(self.bank)
        TEfilename=os.path.basename(self.TEfile)
        TE_filename_noext=os.path.splitext(TEfilename)[0]
        #align_file="{}/{}/{}_{}_{}".format(os.path.dirname(self.TEfile), TE_filename_noext, TE_filename_noext, self.acronym, bankname)
        align_file="{}_{}_{}".format(TE_filename_noext, self.acronym, bankname)

        if self.singularity=="":
            cmd = "blaster"+self.TEversion
        else:
            cmd = "singularity exec {} blaster{}".format (self.singularity, self.TEversion) #asume that if you provide image, you want to run the old 2.25 whitout the low-complexity filter


        cmd += " -q {}".format(self.TEfile)
        cmd += " -s {}".format(self.bank)
        cmd += " -B {}".format(align_file)
        cmd += " -n {}".format(self.blast)
        if self.blastType == "ncbi":
            cmd += " -N"
            #option += " -p '-a 1'" #Multithreading option MAKE ALL THE TE_FINDER TOOLS CRASH IF CORE > 1
        elif self.blastType == "wu": #Wu blast is deprecated, don't use
            cmd += " -W"
            #cmd += " -p -cpus=1"
        elif self.blastType == "blastplus":
            cmd += " -X"
            #cmd += " -p '-num_threads 1'"
        cmd += " -r"
        #cmd += " -c"
        cmd += " -v 1 >> {}".format(self.logFileName) #switch verbose back to 1

        print("blaster command check:", cmd)
        process = subprocess.run(cmd, shell = True)
        if process.returncode != 0:
            print("ERROR when launching", cmd)

        if self.doClean:
            os.remove(align_file+".param")
            FileUtils.removeFilesByPattern(self.bank+"_cut.*")

    def matcher(self):
        print(os.getcwd())
        LaunchMatcherThreads(hmm=False,
                            inFile="{}_{}_{}.align".format(os.path.splitext(os.path.basename(self.TEfile))[0], self.acronym, os.path.basename(self.bank)),
                            logFile=self.logFileName+"matcher_followup.log",
                            version= self.TEversion,
                            singularity= self.singularity,
                            doClean=False).run()

    def run(self):

        self.blaster()
        if self.matcherfollow:
            self.matcher()
