#!/usr/bin/env python

from commons.core.seq.FastaUtils import FastaUtils
from commons.core.seq.BioseqDB import BioseqDB
from commons.core.seq.BioseqUtils import BioseqUtils
from commons.core.parsing.MrepsToSet import MrepsToSet
import subprocess
import os
import sys
import getopt
import argparse


def main(inFileName="", outFileName="", errorFilter=1.0, clean=False, verbose=0, singularity_img=""):
    """
    Launch Mreps.
    """

    if inFileName == "":
        print("ERROR: missing compulsory options")
        sys.exit(1)

    if verbose > 0:
        print("Beginning of LaunchMreps")
        sys.stdout.flush()

    # Mreps 2.5 doesn't fully support IUPAC nomenclature and Ns % has to be less than 5% of the sequence
    if verbose > 0:
        print("* check IUPAC symbols")
        sys.stdout.flush()
    tmpInFileName = "%s.tmp%i" % (inFileName, os.getpid())
    if os.path.exists(tmpInFileName):
        os.system("rm -f %s" % (tmpInFileName))
    bsDB = BioseqDB(inFileName)
    for bs in bsDB.db:
        if verbose > 0:
            print(bs.header)
            sys.stdout.flush()
        bs.partialIUPAC()
        onlyN = True
        for nt in ["A", "T", "G", "C"]:
            if nt in bs.sequence:
                onlyN = False
        if onlyN == True:
            if verbose > 0:
                print("** Warning: only Ns")
                sys.stdout.flush()
        else:
            bsDB.save(tmpInFileName)

    if not os.path.exists(tmpInFileName):
        sys.exit(0)

    # Ns % verification
    dSeqName2Tuple = {}
    dSeqName2Tuple = FastaUtils.giveTab_seq2Nts(tmpInFileName)
    lSeqKept = []
    for seq, tupleInformation in dSeqName2Tuple.items():
        if verbose > 0:
            print(seq, tupleInformation)
        length = tupleInformation[0]
        tabInfo = tupleInformation[1]
        pct = float(tabInfo["N"]) / float(length) * 100
        if pct <= 5.0:  # mreps limits the pct Nts in sequence to 5%
            lSeqKept.append(seq)
    if verbose > 0:
        print(lSeqKept)
    newBioSeqDB = bsDB.fetchList(lSeqKept)
    BioseqUtils.writeBioseqListIntoFastaFile(newBioSeqDB, tmpInFileName)

    # launch Mreps on the input file
    MrepsOutFileName = "{}.Mreps.xml".format(tmpInFileName)
    if singularity_img == "":
        prg = "mreps"
    else:
        prg = "singularity exec {} mreps".format(singularity_img)
    cmd = prg
    cmd += " -res 3"
    cmd += " -exp 3.0"
    cmd += " -maxsize 50"
    cmd += " -xmloutput {}".format(MrepsOutFileName)
    cmd += " -fasta {}".format(tmpInFileName)
    process = subprocess.Popen(cmd, shell=True)
    process.communicate()
    if process.returncode != 0:
        raise Exception("ERROR when launching '{}'".format(cmd))

    if outFileName == "":
        outFileName = "{}.Mreps.set".format(inFileName)

    # parse Mreps results in xml format
    iMrepsToSet = MrepsToSet(inFileName, MrepsOutFileName, outFileName, errorFilter)
    iMrepsToSet.run()
    if clean:
        iMrepsToSet.clean()

    # remove temporary input filename
    os.remove(tmpInFileName)

    if verbose > 0:
        print("LaunchMreps finished successfully\n")
        sys.stdout.flush()

    return 0


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='usage: LaunchMreps.py [ options ]\n', formatter_class=argparse.RawTextHelpFormatter, add_help=True)
    parser.add_argument('-i', '--input', required=True,
                        help="name of the input file (format='fasta')")
    parser.add_argument('-o', '--output', default="",
                        help="name of the output file (default=inFileName+'.Mreps.set')")
    parser.add_argument('-f', '--filter_error', default=1.0, type=float,
                        help="error filter (default=1.0)")
    parser.add_argument('-c', '--clean', action='store_true', help="clean")
    parser.add_argument('-v', '--verbose', default=0, type=int, help="verbosity level  (default=0/1)")

    args = parser.parse_args()
    input = args.input
    output = args.output
    filter_error = args.filter_error
    clean = args.clean
    verbose = args.verbose
    main(inFileName=input, outFileName=output, errorFilter=filter_error, clean=clean, verbose=verbose)

