#! /usr/bin/env python

# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

#this goes in launcher

from commons.core.utils.FileUtils import FileUtils
from commons.core.utils.RepetOptionParser import RepetOptionParser
import os
import re

class LaunchtRNAscanSE(object):

    def __init__(self, inputFileName = "" , outputFileName = "", logFileName="", override = False, doClean = False, verbosity = 0):
        self.setInputFileName(inputFileName)
        self.setOutputFileName(outputFileName)
        self.setOverride(override)
        self._clean = doClean
        self.setVerbosity(verbosity)
        self.setLogFileName(logFileName)

    def setAttributesFromCmdLine(self):
        usage = "LaunchtRNAscanSE.py [options]"
        description = "LaunchtRNAscanSE can run the tRNAscan-SE program.\n"
        parser = RepetOptionParser(description = description, usage = usage)
        parser.add_option("-i", "--input", dest="inputFileName", action="store", type="string", default="", help="input file (fasta type)")
        parser.add_option("-o", "--output", dest="outputFileName", action="store", type="string", default="", help="output file (map type, default = inputFileName.tRNA.map)")
        parser.add_option("-Q", "--override", action="store_true", default=False, help="override previous output file (default = don't override)")
        parser.add_option("-c", "--clean", action="store_true", default=False, help="remove temporary files (results from tRNAscan-SE) (default = don't clean)")
        parser.add_option("-v", "--verbose", action="store", type="int", default = 0, help = "verbose (default = 0, 1)")
        options = parser.parse_args()[0]
        self._setAttributesFromOptions(options)

    def _setAttributesFromOptions(self, options):
        self.setInputFileName(options.inputFileName)
        self.setOutputFileName(options.outputFileName)
        self.setOverride(options.override)
        self.setClean(options.clean)
        self.setVerbosity(options.verbose)

    def checkOptions(self):
        if self._inputFileName != "":
            if not FileUtils.isRessourceExists(self._inputFileName):
                raise Exception("ERROR: Input file does not exist!")

    def setOutputFileName(self, outputFileName):
        if outputFileName == "":
            self._outputMapFileName = "{}.tRNA.map".format(self._inputFileName)
        else:
            self._outputMapFileName = outputFileName

    def setLogFileName(self, logFileName):
        if logFileName=="":
            logFileName = "tRNAscan-SE.log"
        else:
            self._logFileName=logFileName

    def setInputFileName(self, inputFileName):
        self._inputFileName = inputFileName
        self._outputTabFileName = "{}_tRNAscanSE.tab".format(os.path.splitext(self._inputFileName)[0])

    def setOverride(self, override):
        if override:
            self._override = "-Q"

    def setClean(self, clean):
        self._clean = clean

    def setVerbosity(self, verbose):
        if verbose == 0:
            self._verbose = "-q"
        else:
            self._verbose = ""

    def formatedLine(self,line):
        result = re.match("(?P<name>.[^ ]+)(\s+)(?P<num>\d+)(\s+)(?P<start>\d+)(\s+)(?P<end>\d+)(\s+)(?P<type>[A-Za-z]+)(\s+)(?P<cod>[ATGC\?]+)(\s+)(?P<ibeg>\d+)(\s+)(?P<iend>\d+)(\s+)(?P<cov>\d+.\d+)", line)
        formatedLine = result.group('type')+"|"+result.group('cod')+"|intron="+result.group('ibeg')+"-"+result.group('iend')+"|"+result.group('cov')+"\t"+result.group('name')+"\t"+result.group('start')+"\t"+result.group('end')
        return formatedLine

    def tab2Map(self):
        outputFile = open(self._outputTabFileName, "r")
        outputFileMap = open(self._outputMapFileName, "w")
        if not self._verbose:
            #3 lines of tRNAscan-SE file header
            outputFile.readline()
            outputFile.readline()
            outputFile.readline()
        line = outputFile.readline()
        while line:
            outputFileMap.write(self.formatedLine(line) + "\n")
            line = outputFile.readline()
        outputFile.close()
        outputFileMap.close()

    def run(self):
        self.checkOptions()
        cmd = "tRNAscan-SE {} {} -o {} {} 2> {}".format(self._override, self._verbose, self._outputTabFileName, self._inputFileName, self._logFileName)
        print ("command check :", cmd)
        #cmd = "tRNAscan-SE %s %s -o %s %s 2> %s" % (self._override, self._verbose, self._outputTabFileName, self._inputFileName, logFileName) #change
        #cmd += self._override
        #cmd += self._verbose
        #cmd += "-o "+ self._outputTabFileName
        #cmd += self._inputFileName
        #cmd += "2> " + logFileName

        os.system(cmd)
        self.tab2Map()
        if self._clean:
            os.remove(self._outputTabFileName)
            os.remove(logFileName)

if __name__ == "__main__":
    iLaunchtRNAscanSE = LaunchtRNAscanSE()
    iLaunchtRNAscanSE.setAttributesFromCmdLine()
    iLaunchtRNAscanSE.run()
