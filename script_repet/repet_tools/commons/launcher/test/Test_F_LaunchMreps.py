import os
import time
import random
import unittest
import subprocess

from shutil import rmtree
from commons.core.utils.FileUtils import FileUtils
from commons.launcher.LaunchMreps import main

REPET_DATA = os.path.join(os.getcwd(),'data')
class Test_F_LaunchMreps(unittest.TestCase):
    
    def setUp(self):
        self._curTestDir = os.getcwd()
        self._testPrefix = 'test_F_LM_{}_{}'.format(time.strftime("%H%M%S"), random.randint(0, 1000))
        self._singularity = "../../../../../../Singularity/repet_TEannot.sif"
        try:
            os.makedirs(self._testPrefix)
        except:pass
        os.chdir(self._testPrefix)

        self._inputFastaFileName = "input.fa"
        os.symlink("%s/batch_1.fa" % REPET_DATA, "batch_1.fa")
        os.symlink("%s/batch_2.fa" % REPET_DATA, "batch_2.fa")
        os.system("cat batch_1.fa >> %s" % self._inputFastaFileName)
        os.system("cat batch_2.fa >> %s" % self._inputFastaFileName)

        self._obsMrepsOutputFileName = "obsMreps.Mreps.set"
        self._expMrepsOutputFileName = "expMreps.Mreps.set"
        os.symlink("%s/DmelChr4_TEannot_Mreps.set" % REPET_DATA, self._expMrepsOutputFileName)
        
    def tearDown(self):
        os.chdir(self._curTestDir)
        try:
            rmtree(self._testPrefix)
        except:pass
    
    def test_run_as_script(self):
        #cmd = "LaunchMreps.py -i %s -o %s -v 0 -c -s %s" % (self._inputFastaFileName, self._obsMrepsOutputFileName, self._singularity)
        #process = subprocess.Popen(cmd, shell = True)
        #process.communicate()
        lMreps = main(inFileName=self._inputFastaFileName, outFileName= self._obsMrepsOutputFileName, singularity_img = self._singularity)
        cmd = "sort -k3,3 -k4,4n -k5,5n %s | cut -f2,3,4,5 > expResult.txt" % self._expMrepsOutputFileName
        subprocess.call(cmd, shell = True)
        cmd = "sort -k3,3 -k4,4n -k5,5n %s | cut -f2,3,4,5 > obsResult.txt" % self._obsMrepsOutputFileName
        subprocess.call(cmd, shell = True)

        self.assertTrue(FileUtils.are2FilesIdentical("expResult.txt", "obsResult.txt"))
        
    def test_run_as_script_seqWithNsStrechOver5pct(self):
        self._writeNewFasta()
        #cmd = "launchMreps.py -i %s -o %s -v 0 -c -s %s" % (self._inputFastaFileName, self._obsMrepsOutputFileName, self._singularity)
        #process = subprocess.Popen(cmd, shell = True)
        #process.communicate()
        lMreps = main(inFileName=self._inputFastaFileName, outFileName= self._obsMrepsOutputFileName, singularity_img = self._singularity)
        cmd = "sort -k3,3 -k4,4n -k5,5n %s | cut -f2,3,4,5 > expResult.txt" % self._expMrepsOutputFileName
        subprocess.call(cmd, shell = True)
        cmd = "sort -k3,3 -k4,4n -k5,5n %s | cut -f2,3,4,5 > obsResult.txt" % self._obsMrepsOutputFileName
        subprocess.call(cmd, shell = True)
        self.assertTrue(FileUtils.are2FilesIdentical("expResult.txt", "obsResult.txt"))
        
    def _writeNewFasta(self):
        f = open("batch_3.fa", "w")
        f.write(">INE-1\n")
        f.write("TATACCCNNNNNNNNNNNNNNNAAATGAAT\n")
        f.write(">INT-1\n")
        f.write("TATACCCGTTACTAGATTNNNNNNNNNNNNN\n")
        f.close()
        os.system("cat batch_3.fa >> %s" % self._inputFastaFileName)



if __name__ == "__main__":
    unittest.main()
