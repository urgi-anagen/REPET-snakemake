import os
import subprocess
import unittest
from commons.launcher.LaunchTRF import LaunchTRF
from commons.core.utils.FileUtils import FileUtils
from commons.core.checker.RepetException import RepetException
import time
import random
import shutil

class Test_F_LaunchTRF(unittest.TestCase):

    def setUp(self):
        self._curTestDir = os.getcwd()
        self._testPrefix = 'test_F_LM_{}_{}'.format(time.strftime("%H%M%S"), random.randint(0, 1000))
        try:
            os.makedirs(self._testPrefix)
        except:pass
        os.chdir(self._testPrefix)
        
        self._inputFastaFileName = "input.fa"
        self._obsTRFOutputFileName = "obsTRF.TRF.set"
        self._expTRFOutputFileName = "expTRF.TRF.set"
        self._writeInputFile()
        self._writeExpTRFOutputFile()
        self._singularity = "../../../../../../Singularity/repet_TEannot.sif"

        self._verbosity = 3

    def tearDown(self):
        os.chdir(self._curTestDir)
        try:
            shutil.rmtree(self._testPrefix)
        except:pass
        
        # lFilesToRemove = [self._inputFastaFileName, self._expTRFOutputFileName, self._obsTRFOutputFileName]
        # lFilesToRemove.extend(FileUtils.getFileNamesList(os.getcwd(), "input.fa.*.dat"))
        # for f in lFilesToRemove:
        #     try:
        #         os.remove(f)
        #     except:
        #         pass

    def test_run(self):
        iLTRF = LaunchTRF(self._inputFastaFileName, self._obsTRFOutputFileName, maxPeriod=15.5,
                          verbosity=self._verbosity,singularity_img = self._singularity)
        iLTRF.run()
        self.assertTrue(FileUtils.are2FilesIdentical(self._expTRFOutputFileName, self._obsTRFOutputFileName))

    def test_run_inFileNameTooLarge(self):
        newName = "inputaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa.fa"
        os.rename(self._inputFastaFileName, newName)
        iLTRF = LaunchTRF(newName, self._obsTRFOutputFileName, maxPeriod=15.5, verbosity=self._verbosity,singularity_img = self._singularity)
        iLTRF.run()
        self.assertTrue(FileUtils.are2FilesIdentical(self._expTRFOutputFileName, self._obsTRFOutputFileName))
        os.remove(newName)
        FileUtils.removeFilesByPattern('*.dat')

    def test_run_emptyConstructor_NoOutputGiven_changedMismatch_indel_minScore_outputMask(self):
        obsMaskedFasta = "input.fa.mask"
        expMaskedFasta = "expInput.fa.mask"

        iLTRF = LaunchTRF()
        iLTRF.setInFileName(self._inputFastaFileName)
        iLTRF.setOutFileName("")
        iLTRF.setMismatch(2)
        iLTRF.setIndelPenalty(4)
        iLTRF.setMinScore(0)
        iLTRF.setMaxPeriod(10)
        iLTRF.setOutMaskedFileName(obsMaskedFasta)
        iLTRF.setDoClean(True)
        iLTRF.setVerbosity(self._verbosity)
        iLTRF.setSingularityImage(self._singularity)
        iLTRF.run()

        self._obsTRFOutputFileName = "input.fa.TRF.set"
        self._writeExpTRFOutputFile_maxPeriod10_mismatch2_indelPen4_minScore0()
        self._writeExpMaskedFasta_maxPeriod10_mismatch2_indelPen4_minScore0(expMaskedFasta)
        self.assertTrue(FileUtils.are2FilesIdentical(expMaskedFasta, obsMaskedFasta))
        os.remove(obsMaskedFasta)
        os.remove(expMaskedFasta)
        self.assertTrue(FileUtils.are2FilesIdentical(self._expTRFOutputFileName, self._obsTRFOutputFileName))

    # def test_run_as_script(self):
    #     print(os.getcwd())
    #     cmd = 'LaunchTRF.py -i {} -o {} -v {}'.format(self._inputFastaFileName, self._obsTRFOutputFileName,
    #                                                      self._verbosity)
    #     subprocess.call(cmd, shell=True)
    #     self.assertTrue(FileUtils.are2FilesIdentical(self._expTRFOutputFileName, self._obsTRFOutputFileName))

    # def test_run_as_scriptNoOutputGiven_changedPeriod(self):
    #     cmd = 'LaunchTRF.py -i {} -m 10 -v {}'.format(self._inputFastaFileName, self._verbosity)
    #     subprocess.call(cmd, shell=True)
    #     self._obsTRFOutputFileName = "input.fa.TRF.set"
    #     self._writeExpTRFOutputFile_maxPeriod10()
    #     self.assertTrue(FileUtils.are2FilesIdentical(self._expTRFOutputFileName, self._obsTRFOutputFileName))

    # def test_run_as_scriptNoOutputGiven_changedMismatch_indel_minScore_outputMask(self):
    #     obsMaskedFasta = "input.fa.mask"
    #     expMaskedFasta = "expInput.fa.mask"
    #     cmd = 'LaunchTRF.py -i {} --outMaskedFile {} -M 2 -p 4 -s 0 -m 10 -v {}'.format(self._inputFastaFileName,
    #                                                                                        obsMaskedFasta,
    #                                                                                        self._verbosity)
    #     subprocess.call(cmd, shell=True)
    #     self._obsTRFOutputFileName = "input.fa.TRF.set"
    #     self._writeExpTRFOutputFile_maxPeriod10_mismatch2_indelPen4_minScore0()
    #     self._writeExpMaskedFasta_maxPeriod10_mismatch2_indelPen4_minScore0(expMaskedFasta)
    #     self.assertTrue(FileUtils.are2FilesIdentical(expMaskedFasta, obsMaskedFasta))
    #     os.remove(obsMaskedFasta)
    #     os.remove(expMaskedFasta)
    #     self.assertTrue(FileUtils.are2FilesIdentical(self._expTRFOutputFileName, self._obsTRFOutputFileName))

    # def test_run_as_scriptNoOutputGivenDoClean(self):
    #     cmd = 'LaunchTRF.py -i {} -c -v {}'.format(self._inputFastaFileName, self._verbosity)
    #     subprocess.call(cmd, shell=True)
    #     self._obsTRFOutputFileName = "input.fa.TRF.set"
    #     datFilePath = os.path.join(os.getcwd(), "{}.2.3.5.80.10.20.15.dat".format(self._inputFastaFileName))
    #     self.assertFalse(FileUtils.isRessourceExists(datFilePath))

    def test_run_maxPeriodError(self):
        iLTRF = LaunchTRF(self._inputFastaFileName, self._obsTRFOutputFileName, maxPeriod=-10,
                          verbosity=self._verbosity,singularity_img = self._singularity)

        isExceptionRaised = False
        obsMessage = ""
        try:
            iLTRF.run()
        except RepetException as e:
            isExceptionRaised = True
            obsMessage = e.getMessage()

        expMessage = "maxPeriod ('-m' option) must be a strictly positive integer"

        self.assertTrue(isExceptionRaised)
        self.assertEqual(obsMessage, expMessage)

    def _writeInputFile(self):
        with open(self._inputFastaFileName, 'w') as inputFile:
            inputFile.write('>sequence\n')
            inputFile.write('GGGCGGCACGGTGGTGTGGTGGTTAGCACTGTTGCCTCACAGCAAGAAGGCCCCGGGTTC\n')
            inputFile.write('GATCCCCGGTTGGGACTGAGGCTGGGGACTTTCTGTGTGGAGTTTGCATGTTCTCCCTGT\n')
            inputFile.write('GCCTGCGTGGGTTCTCTCCGGGTACTCCGGCTTCCTCCCACAGTCCAAAGACATGCATGA\n')
            inputFile.write('TTGGGGATTAGGCTAATTGGAAACTCTAAAATTGCCCGTAGGTGTGAGTGTGAGAGAGAA\n')
            inputFile.write('TGGTTGTTTGTCTATATGTGTTAGCCCTGCGATTGACTGGCGTCCAGTCCAGGGTGTACC\n')
            inputFile.write('CTGCCTCCGCCCATTGTGCTGGGATAGGCTCCAGTCCCCCCG\n')
            inputFile.write('CAAGCGGTAGAAAGTGAGTGAGTGAGTGA\n')
            inputFile.write('>sequence2\n')
            inputFile.write('GGGCAGCCTGGGTGGCTCAGCGGTTTAGCGCCTGCCTTTGGCCCAGGGCGTGATCCTGGA\n')
            inputFile.write('GACCCGGGATCGAGTCCCACATCGGGCTCCCTGCATGGAGCCTGCTTCTCCCTCTGCCTG\n')
            inputFile.write('TGTCTCTGCCTCTCTCTCTCTCTGTGTCTCTCATGAATAAA\n')

    def _writeExpTRFOutputFile(self):
        with open(self._expTRFOutputFileName, 'w') as expTRFOutputFile:
            expTRFOutputFile.write("1\t(GTGGT)2\tsequence\t11\t21\n")
            expTRFOutputFile.write("2\t(GGT)5\tsequence\t10\t23\n")
            expTRFOutputFile.write("3\t(GTGTGA)4\tsequence\t222\t242\n")
            expTRFOutputFile.write("4\t(GTCCA)2\tsequence\t282\t292\n")
            expTRFOutputFile.write("5\t(AGTG)4\tsequence\t355\t371\n")
            expTRFOutputFile.write("6\t(GCCTGTCTCTCCTCT)4\tsequence2\t100\t152\n")
            expTRFOutputFile.write("7\t(CTCTGCCTGTGT)3\tsequence2\t112\t151\n")
            expTRFOutputFile.write("8\t(TC)23\tsequence2\t107\t152\n")

    def _writeExpTRFOutputFile_maxPeriod10(self):
        with open(self._expTRFOutputFileName, 'w') as expTRFOutputFile:
            expTRFOutputFile.write("1\t(GTGGT)2\tsequence\t11\t21\n")
            expTRFOutputFile.write("2\t(GGT)5\tsequence\t10\t23\n")
            expTRFOutputFile.write("3\t(GTGTGA)4\tsequence\t222\t242\n")
            expTRFOutputFile.write("4\t(GTCCA)2\tsequence\t282\t292\n")
            expTRFOutputFile.write("5\t(AGTG)4\tsequence\t355\t371\n")
            expTRFOutputFile.write("6\t(TC)23\tsequence2\t107\t152\n")

    def _writeExpTRFOutputFile_maxPeriod10_mismatch2_indelPen4_minScore0(self):
        with open(self._expTRFOutputFileName, 'w') as expTRFOutputFile:
            expTRFOutputFile.write("1\t(GTGGT)2\tsequence\t11\t21\n")
            expTRFOutputFile.write("2\t(GGT)5\tsequence\t10\t23\n")
            expTRFOutputFile.write("3\t(CTCCGGCGTA)2\tsequence\t136\t158\n")
            expTRFOutputFile.write("4\t(GA)7\tsequence\t226\t239\n")
            expTRFOutputFile.write("5\t(TGTT)5\tsequence\t241\t259\n")
            expTRFOutputFile.write("6\t(GTCCA)2\tsequence\t282\t292\n")
            expTRFOutputFile.write("7\t(AGTG)4\tsequence\t355\t371\n")
            expTRFOutputFile.write("8\t(TC)23\tsequence2\t107\t152\n")

    def _writeExpMaskedFasta_maxPeriod10_mismatch2_indelPen4_minScore0(self, fileName):
        with open(fileName, 'w') as fH:
            fH.write('>sequence\n')
            fH.write('GGGCGGCACNNNNNNNNNNNNNNTAGCACTGTTGCCTCACAGCAAGAAGGCCCCGGGTTC\n')
            fH.write('GATCCCCGGTTGGGACTGAGGCTGGGGACTTTCTGTGTGGAGTTTGCATGTTCTCCCTGT\n')
            fH.write('GCCTGCGTGGGTTCTNNNNNNNNNNNNNNNNNNNNNNNCACAGTCCAAAGACATGCATGA\n')
            fH.write('TTGGGGATTAGGCTAATTGGAAACTCTAAAATTGCCCGTAGGTGTNNNNNNNNNNNNNNA\n')
            fH.write('NNNNNNNNNNNNNNNNNNNGTTAGCCCTGCGATTGACTGGCNNNNNNNNNNNGGTGTACC\n')
            fH.write('CTGCCTCCGCCCATTGTGCTGGGATAGGCTCCAGTCCCCCCGCAAGCGGTAGAANNNNNN\n')
            fH.write('NNNNNNNNNNN\n')
            fH.write('\n')
            fH.write('>sequence2\n')
            fH.write('GGGCAGCCTGGGTGGCTCAGCGGTTTAGCGCCTGCCTTTGGCCCAGGGCGTGATCCTGGA\n')
            fH.write('GACCCGGGATCGAGTCCCACATCGGGCTCCCTGCATGGAGCCTGCTNNNNNNNNNNNNNN\n')
            fH.write('NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNATGAATAAA\n')
            fH.write('\n')


if __name__ == "__main__":
    unittest.main()
