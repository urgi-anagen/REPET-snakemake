#!/usr/bin/env python

# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import os
import shutil
import subprocess
from commons.core.LoggerFactory import LoggerFactory
from commons.core.checker.CheckerUtils import CheckerUtils
from commons.core.checker.RepetException import RepetException
from commons.core.utils.FileUtils import FileUtils
from commons.core.utils.RepetOptionParser import RepetOptionParser

LOG_DEPTH = "commons.launcher"


##Launch TRF
#
class LaunchTRF(object):

    def __init__(self, inFileName="", outFileName="", maxPeriod=15, doClean=False, verbosity=3, singularity_img = "" ,
                 log_name="LaunchTRF.log"):
        self._inFileName = inFileName
        self.setOutFileName(outFileName)
        self._outMaskedFileName = ""
        self._mismatch = 3
        self._indelPenalty = 5
        self._minScore = 20
        self._maxPeriod = maxPeriod

        self._doClean = doClean
        self._verbosity = verbosity
        self._singularity_img = singularity_img
        #self._log = LoggerFactory.createLogger("{}.{}".format(LOG_DEPTH, self.__class__.__name__), self._verbosity)
        self._log = LoggerFactory.createLogger("{}.{}".format((LOG_DEPTH, self.__class__.__name__), self._verbosity),
                                              log_name)

    def setAttributesFromCmdLine(self):
        usage = "LaunchTRF.py [options]"
        description = "Launch TRF to detect micro-satellites in sequences.\n"
        epilog = "\nExample: launch with a maximum period size of 10, keep temporary files and display only warnings.\n"
        epilog += "\t$ LaunchTRF.py -i genome.fa -m 10 -v 2\n"
        parser = RepetOptionParser(description=description, epilog=epilog, usage=usage)
        parser.add_option("-i", "--in", dest="inFileName", action="store", type="string",
                          help="input file name [compulsory] [format: fasta]", default="")
        parser.add_option("-o", "--out", dest="outFileName", action="store", type="string",
                          help="output file name [default: <input>.TRF.set]", default="")
        parser.add_option("--outMaskedFile", dest="outMaskedFile", action="store", type="string",
                          help="output masked FASTA file name [optional] [default: no output]", default="")
        parser.add_option("-M", "--mismatch", dest="mismatch", action="store", type="int",
                          help="mismatching penalty [default: 3]", default=3)
        parser.add_option("-p", "--penaltyIn", dest="indelPen", action="store", type="int",
                          help="indel penalty [default: 5]", default=5)
        parser.add_option("-s", "--scoreMin", dest="minScore", action="store", type="int",
                          help="minimum alignment score to report [default: 20]", default=20)
        parser.add_option("-m", "--maxPeriod", dest="maxPeriod", action="store", type="int",
                          help="maximum period size to report [default: 15]", default=15)
        parser.add_option("-c", "--clean", dest="doClean", action="store_true",
                          help="clean temporary files [optional] [default: False]", default=False)
        parser.add_option("-v", "--verbosity", dest="verbosity", action="store", type="int",
                          help="verbosity [optional] [default: 3]", default=3)
        options = parser.parse_args()[0]
        self._setAttributesFromOptions(options)

    def _setAttributesFromOptions(self, options):
        self.setInFileName(options.inFileName)
        self.setOutFileName(options.outFileName)
        self.setMismatch(options.mismatch)
        self.setIndelPenalty(options.indelPen)
        self.setMinScore(options.minScore)
        self.setMaxPeriod(options.maxPeriod)
        self.setOutMaskedFileName(options.outMaskedFile)
        self.setDoClean(options.doClean)
        self.setVerbosity(options.verbosity)
    
    def setSingularityImage(self,singularity_img) : 
        self._singularity_img = singularity_img

    def setInFileName(self, inFileName):
        self._inFileName = inFileName

    def setOutFileName(self, outFileName):
        if outFileName == "":
            self._outFileName = "{}.TRF.set".format(self._inFileName)
        else:
            self._outFileName = outFileName

    def setMismatch(self, mismatch):
        self._mismatch = mismatch

    def setIndelPenalty(self, indelPenalty):
        self._indelPenalty = indelPenalty

    def setMinScore(self, minScore):
        self._minScore = minScore

    def setMaxPeriod(self, maxPeriod):
        self._maxPeriod = maxPeriod

    def setOutMaskedFileName(self, outMaskedFile):
        self._outMaskedFileName = outMaskedFile

    def setDoClean(self, doClean):
        self._doClean = doClean

    def setVerbosity(self, verbosity):
        self._verbosity = verbosity

    def _checkOptions(self):
        if self._inFileName == "":
            self._logAndRaise("Missing input fasta file name")
        else:
            if not FileUtils.isRessourceExists(self._inFileName):
                self._logAndRaise("Input fasta file '{}' does not exist!".format(self._inFileName))

        self._log.debug("Forcing integer type for maxPeriod, mismatch, penaltyIn and scoreMin parameters")
        self._maxPeriod = int(self._maxPeriod)
        self._mismatch = int(self._mismatch)
        self._indelPenalty = int(self._indelPenalty)
        self._minScore = int(self._minScore)
        if self._maxPeriod < 1:
            self._logAndRaise("maxPeriod ('-m' option) must be a strictly positive integer")
        if self._mismatch < 0:
            self._logAndRaise("mismatch ('-M' option) must be a positive integer")
        if self._indelPenalty < 0:
            self._logAndRaise("penaltyIn ('-p' option) must be a positive integer")
        if self._minScore < 0:
            self._logAndRaise("scoreMin ('-s' option) must be a positive integer")

    def _logAndRaise(self, errorMsg):
        self._log.error(errorMsg)
        raise RepetException(errorMsg)

    def _launchTRF(self):
        if self._singularity_img == "":
            cmd = "trf {} 2 {} {} 80 10 {} {} -h -d".format(self._inFileName, self._mismatch, self._indelPenalty,
                                                        self._minScore, self._maxPeriod)
        else :
            cmd = "singularity exec {} trf {} 2 {} {} 80 10 {} {} -h -d".format(self._singularity_img, self._inFileName, self._mismatch, self._indelPenalty,
                                                            self._minScore, self._maxPeriod)
        if self._outMaskedFileName:
            cmd += " -m"
        self._log.debug("Running : {}".format(cmd))
        process = subprocess.Popen(cmd.split(' '), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output = process.communicate()
        self._log.debug("Output:\n{}".format(output[0]))

    def _parseTRF(self):
        self._log.debug("Parsing TRF output")
        with open("{}.2.{}.{}.80.10.{}.{}.dat".format(self._inFileName, self._mismatch, self._indelPenalty,
                                                      self._minScore, self._maxPeriod), 'r') as inFile:
            with open(self._outFileName, 'w') as outFile:
                nbPatterns = 0
                nbInSeq = 0
                for line in inFile:
                    data = line.split(" ")
                    if len(data) > 1 and "Sequence:" in data[0]:
                        nbInSeq += 1
                        seqName = data[1][:-1]
                    if len(data) >= 14:
                        nbPatterns += 1
                        consensus = data[13]
                        copyNb = int(float(data[3]) + 0.5)
                        start = data[0]
                        end = data[1]
                        outFile.write(
                            "{}\t({}){}\t{}\t{}\t{}\n".format(nbPatterns, consensus, copyNb, seqName, start, end))
        self._log.debug("Finished Parsing TRF output")

    def run(self):
        """
        Launch TRF to detect micro-satellites in sequences.
        """
        LoggerFactory.setLevel(self._log, self._verbosity)
        self._log.info("START LaunchTRF")
        if self._singularity_img == "" and (not CheckerUtils.isExecutableInUserPath("trf")) :
            self._logAndRaise("ERROR: 'trf' must be in your path")
        self._checkOptions()
        self._log.debug("Input file name: {}".format(self._inFileName))

        self._launchTRF()
        self._parseTRF()

        if self._outMaskedFileName:
            shutil.move("{}.2.{}.{}.80.10.{}.{}.mask".format(self._inFileName, self._mismatch, self._indelPenalty,
                                                             self._minScore, self._maxPeriod), self._outMaskedFileName)

        if self._doClean:
            self._log.warning("Temporary files will be cleaned")
            try:
                os.remove("{}.2.{}.{}.80.10.{}.{}.dat".format(self._inFileName, self._mismatch, self._indelPenalty,
                                                              self._minScore, self._maxPeriod))
            except:
                pass

        self._log.info("END LaunchTRF")


if __name__ == "__main__":
    iLaunchTRF = LaunchTRF()
    iLaunchTRF.setAttributesFromCmdLine()
    iLaunchTRF.run()
