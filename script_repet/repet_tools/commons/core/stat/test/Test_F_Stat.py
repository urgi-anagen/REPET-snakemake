import unittest
from commons.core.stat.Stat import Stat


class Test_F_Stat(unittest.TestCase):


    def test_output(self):
        lValues = [0, -1, -5, 112, 10.2, 0.5, 4, -0.5]
        iStat = Stat(lValues)
        expString = "n=8 mean=15.025 var=1554.934 sd=39.433 min=-5.000 med=0.250 max=112.000"
        self.assertEqual(expString, iStat.string())

    def test_statObject( self ):
        min = 0.8112781244591328#0.811278124459
        max = 1.6500224216483543#1.65002242165
        sum = 5.046263046828643#5.04626304683
        sumOfSquares = 6.892852315860488#6.89285231586
        en = 4
        lValues = [1.584962500721156, 1.0, 1.6500224216483543, 0.8112781244591328]#[1.5849625007211561, 1.0, 1.6500224216483541, 0.81127812445913283]
        obsStat = Stat(lValues)
        self.assertEqual(min, obsStat.getMin())
        self.assertEqual(max, obsStat.getMax())
        self.assertEqual(sum, obsStat.getSum())
        self.assertEqual(sumOfSquares, obsStat.getSumOfSquares())
        self.assertEqual(en, obsStat.getValuesNumber())
        self.assertEqual(lValues, obsStat.getValuesList())

    def test_outputQuantile(self):
        lValues = [0, -1, -5, 112, 10.2, 0.5, 4, -0.5]
        iStat = Stat(lValues)
        expString = "n=8 min=-5.000 Q1=-0.500 median=0.500 Q3=10.200 max=112.000"
        self.assertEqual(expString, iStat.stringQuantiles())
        
        
if __name__ == "__main__":
    unittest.main()