from commons.core.stat.Stat import Stat
import unittest

class Test_Stat(unittest.TestCase):
    
    def test__eq__true(self):
        iStat1 = Stat([1, 2, 3, 46])
        iStat2 = Stat([1, 2, 3, 46])
        self.assertTrue(iStat1 == iStat2)
  
    def test__eq__false(self):
        iStat1 = Stat([1, 2, 3, 4])
        iStat2 = Stat([1, 2, 3, 46])
        self.assertFalse(iStat1 == iStat2)
  
    def test__eq__disordered_list(self):
        iStat1 = Stat([3, 2, 1, 46])
        iStat2 = Stat([1, 2, 3, 46])
        self.assertTrue(iStat1 == iStat2)
  
    def test_reset(self):
        lValues = [1, 2, 5, 9, 12, 46]
        iStat = Stat(lValues)
        iStat.reset()
        expValuesList = []
        expSum = 0
        expSum2 = 0
        expN = 0
        expMin = 0
        expMax = 0
        obsValuesList = iStat.getValuesList()
        obsSum = iStat.getSum()
        obsSum2 = iStat.getSumOfSquares()
        obsN = iStat.getValuesNumber()
        obsMin = iStat.getMin()
        obsMax = iStat.getMax()
        self.assertEqual(expValuesList, obsValuesList)
        self.assertEqual(expSum, obsSum)
        self.assertEqual(expSum2, obsSum2)
        self.assertEqual(expN, obsN)
        self.assertEqual(expMin, obsMin)
        self.assertEqual(expMax, obsMax)
  
    def test_add_EmptyList(self):
        lValues = []
        iStat = Stat(lValues)
        iStat.add(5)
        expValuesList = [5]
        expSum = 5
        expSum2 = 25
        expN = 1
        expMin = 5
        expMax = 5
        obsValuesList = iStat.getValuesList()
        obsSum = iStat.getSum()
        obsSum2 = iStat.getSumOfSquares()
        obsN = iStat.getValuesNumber()
        obsMin = iStat.getMin()
        obsMax = iStat.getMax()
        self.assertEqual(expValuesList, obsValuesList)
        self.assertEqual(expSum, obsSum)
        self.assertEqual(expSum2, obsSum2)
        self.assertEqual(expN, obsN)
        self.assertEqual(expMin, obsMin)
        self.assertEqual(expMax, obsMax)
         
    def test_add_Max(self):
        lValues = [0,1,1]
        iStat = Stat(lValues)
        iStat.add(2)
        expValuesList = [0,1,1,2]
        expSum = 4
        expSum2 = 6
        expN = 4
        expMin = 0
        expMax = 2
        obsValuesList = iStat.getValuesList()
        obsSum = iStat.getSum()
        obsSum2 = iStat.getSumOfSquares()
        obsN = iStat.getValuesNumber()
        obsMin = iStat.getMin()
        obsMax = iStat.getMax()
        self.assertEqual(expValuesList, obsValuesList)
        self.assertEqual(expSum, obsSum)
        self.assertEqual(expSum2, obsSum2)
        self.assertEqual(expN, obsN)
        self.assertEqual(expMin, obsMin)
        self.assertEqual(expMax, obsMax)
         
    def test_add_Min(self):
        lValues = [2,1,1]
        iStat = Stat(lValues)
        iStat.add(0)
        expValuesList = [2,1,1,0]
        expSum = 4
        expSum2 = 6
        expN = 4
        expMin = 0
        expMax = 2
        obsValuesList = iStat.getValuesList()
        obsSum = iStat.getSum()
        obsSum2 = iStat.getSumOfSquares()
        obsN = iStat.getValuesNumber()
        obsMin = iStat.getMin()
        obsMax = iStat.getMax()
        self.assertEqual(expValuesList, obsValuesList)
        self.assertEqual(expSum, obsSum)
        self.assertEqual(expSum2, obsSum2)
        self.assertEqual(expN, obsN)
        self.assertEqual(expMin, obsMin)
        self.assertEqual(expMax, obsMax)
         
    def test_fill_emptyList(self):
        lValues = [2,1,1]
        iStat = Stat(lValues)
        iStat.fill([])
        expValuesList = [2,1,1]
        expSum = 4
        expSum2 = 6
        expN = 3
        expMin = 1
        expMax = 2
        obsValuesList = iStat.getValuesList()
        obsSum = iStat.getSum()
        obsSum2 = iStat.getSumOfSquares()
        obsN = iStat.getValuesNumber()
        obsMin = iStat.getMin()
        obsMax = iStat.getMax()
        self.assertEqual(expValuesList, obsValuesList)
        self.assertEqual(expSum, obsSum)
        self.assertEqual(expSum2, obsSum2)
        self.assertEqual(expN, obsN)
        self.assertEqual(expMin, obsMin)
        self.assertEqual(expMax, obsMax)
         
    def test_fill(self):
        lValues = [2, 1, 1]
        iStat = Stat(lValues)
        iStat.fill([4, 0])
        expValuesList = [2, 1, 1, 4, 0]
        expSum = 8
        expSum2 = 22
        expN = 5
        expMin = 0
        expMax = 4
        obsValuesList = iStat.getValuesList()
        obsSum = iStat.getSum()
        obsSum2 = iStat.getSumOfSquares()
        obsN = iStat.getValuesNumber()
        obsMin = iStat.getMin()
        obsMax = iStat.getMax()
        self.assertEqual(expValuesList, obsValuesList)
        self.assertEqual(expSum, obsSum)
        self.assertEqual(expSum2, obsSum2)
        self.assertEqual(expN, obsN)
        self.assertEqual(expMin, obsMin)
        self.assertEqual(expMax, obsMax)
          
    def test_mean_emptyList(self):
        lValues = []
        iStat = Stat(lValues)
        expMean = 0
        obsMean = iStat.mean()
        self.assertEqual(expMean, obsMean)
          
    def test_mean(self):
        lValues = [0, 1]
        iStat = Stat(lValues)
        expMean = 0.5
        obsMean = iStat.mean()
        self.assertEqual(expMean, obsMean)
          
    def test_var_emptyList(self):
        lValues = []
        iStat = Stat(lValues)
        expVar = 0
        obsVar = iStat.var()
        self.assertEqual(expVar, obsVar)
         
    def test_var(self):
        lValues = [2.75, 1.75, 1.25, 0.25, 0.5, 1.25, 3.5]
        iStat = Stat(lValues)
        expVar = 1.37202
        obsVar = round(iStat.var(), 5)
        self.assertEqual(expVar, obsVar)
         
         
    def test_var2(self):
        lValues = [1,6,3,4,9,6]
        iStat = Stat(lValues)
        expVar = 7.76667
        obsVar = round(iStat.var(), 5)
        self.assertEqual(expVar, obsVar)
          
    def test_var_null_very_small_rounded_to_null(self):
        lValues = [0.32830643654e-9, 0.328e-9]
        iStat = Stat(lValues)
        # real variance is around 4.69516765204e-26
        expVar = 0
        obsVar = iStat.var()
        self.assertEqual(expVar, obsVar)
          
    def test_var_null_negative(self):
        lValues = [1096.0, 1096.0, 1096.0, 1096.0, 1096.0, 1096.0, 1096.0, 1096.0, 1096.0, 1096.0, 1096.0, 1096.0, 1096.0, 1096.0, 1096.0, 1096.0, 1096.0, 1096.0, 1096.0, 1096.0]
        iStat = Stat(lValues)
        expVar = 0
        obsVar = iStat.var()
        self.assertEqual(expVar, obsVar)
          
    def test_sd(self):
        lValues = [1, 1, 2]
        iStat = Stat(lValues)
        expSd = round(0.577350269, 5)
        obsSd = round(iStat.sd(), 5)
        self.assertEqual(expSd, obsSd)
          
    def test_sd_null(self):
        lValues = [87.340000000000003, 87.340000000000003, 87.340000000000003, 87.340000000000003, 87.340000000000003, 87.340000000000003, 87.340000000000003]
        iStat = Stat(lValues)
        expSd = 0
        obsSd = round(iStat.sd(), 5)
        self.assertEqual(expSd, obsSd)
  
    def test_cv(self):
        lValues = [1, 1, 2]
        iStat = Stat(lValues)
        expSd = round(0.433012702, 5)
        obsSd = round(iStat.cv(), 5)
        self.assertEqual(expSd, obsSd)
  
    def test_cv_null(self):
        lValues = [1096, 1096, 1096, 1096, 1096, 1096, 1096, 1096, 1096]
        iStat = Stat(lValues)
        expSd = 0
        obsSd = iStat.cv()
        self.assertEqual(expSd, obsSd)
  
    def test_cv_mean_is_nul(self):
        lValues = [1, -1]
        iStat = Stat(lValues)
        expSd = 0
        obsSd = iStat.cv()
        self.assertEqual(expSd, obsSd)
          
    def test_median_emptyList(self):
        lValues = []
        iStat = Stat(lValues)
        expMedian = "NA"
        obsMedian = iStat.median()
        self.assertEqual(expMedian, obsMedian)
          
    def test_median_even(self):
        lValues = [1, 2, 3, 4, 1, 2, 54, 6, 7]
        iStat = Stat(lValues)
        expMedian = 3
        obsMedian = iStat.median()
        self.assertEqual(expMedian, obsMedian)
          
    def test_median_odd(self):
        lValues = [1, 2, 3, 4, 2, 54, 6, 7]
        iStat = Stat(lValues)
        expMedian = 3.5
        obsMedian = iStat.median()
        self.assertEqual(expMedian, obsMedian)
          
    def test_kurtosis_flat(self):
        lValues = [1, 1, 1]
        iStat = Stat(lValues)
        expKurtosis = 0
        obsKurtosis = iStat.kurtosis()
        self.assertEqual(expKurtosis, obsKurtosis)
          
    def test_kurtosis_peak(self):
        lValues = [1, 100, -5]
        iStat = Stat(lValues)
        expKurtosis = round(712872278.6609683, 2)
        obsKurtosis = round(iStat.kurtosis(), 2)
        self.assertEqual(expKurtosis, obsKurtosis)
   
    def test_kurtosis_normal(self):
        lValues = [-1, 0, 1.64, 1.64, 0, -1]
        iStat = Stat(lValues)
        expKurtosis = 3.0
        obsKurtosis = round(iStat.kurtosis(), 1)
        self.assertEqual(expKurtosis, obsKurtosis)
          
    def test_sort(self):
        lValues = [-1, 0, 1.64, 1.64, 0, -1]
        iStat = Stat(lValues)
        expSort = [-1, -1, 0, 0, 1.64, 1.64]
        obsSort = iStat.sort()
        self.assertEqual(expSort, obsSort)
          
    def test_sort_reverse(self):
        lValues = [-1, 0, 1.64, 1.64, 0, -1]
        iStat = Stat(lValues)
        expSort = [1.64, 1.64, 0, 0, -1, -1]
        obsSort = iStat.sort(True)
        self.assertEqual(expSort, obsSort)
          
    def test_sort_emptyList(self):
        lValues = []
        iStat = Stat(lValues)
        expSort = []
        obsSort = iStat.sort()
        self.assertEqual(expSort, obsSort)
          
    def test_quantile_emptyList(self):
        lValues = []
        iStat = Stat(lValues)
        expQuantile = 0
        obsQuantile = iStat.quantile(0.25)
        self.assertEqual(expQuantile, obsQuantile)
          
    def test_quantile_0perc(self):
        lValues = [0, 2.64, 1.64, -1, 5]
        iStat = Stat(lValues)
        expQuantile = -1
        obsQuantile = iStat.quantile(0)
        self.assertEqual(expQuantile, obsQuantile)
         
    def test_quantile_25perc(self):
        lValues = [144, 33, 111, 79, 292, 306]
        iStat = Stat(lValues)
        expQuantile = 79
        obsQuantile = iStat.quantile(0.25)
        self.assertEqual(expQuantile, obsQuantile)
         
    def test_quantile_41perc(self):
        lValues = [0, 2.64, 1.64, -1, 5]
        iStat = Stat(lValues)
        expQuantile = 1.64
        obsQuantile = iStat.quantile(0.41)
        self.assertEqual(expQuantile, obsQuantile)
          
    def test_quantile_75perc(self):
        lValues = [144, 33, 111, 79, 292, 306]
        iStat = Stat(lValues)
        expQuantile = 292
        obsQuantile = iStat.quantile(0.75)
        self.assertEqual(expQuantile, obsQuantile)
          
    def test_quantile_81perc(self):
        lValues = [0, 2.64, 1.64, -1, 5]
        iStat = Stat(lValues)
        expQuantile = 5
        obsQuantile = iStat.quantile(0.81)
        self.assertEqual(expQuantile, obsQuantile)
          
    def test_quantile_100perc(self):
        lValues = [0, 2.64, 1.64, -1, 5]
        iStat = Stat(lValues)
        expQuantile = 5
        obsQuantile = iStat.quantile(1)
        self.assertEqual(expQuantile, obsQuantile)
          
    def test_NandLperc_50(self):
        lValues = [144, 33, 111, 79, 292, 306]
        iStat = Stat(lValues)
        expL50 = 292
        expN50 = 2
        obsN50, obsL50 = iStat.NandLperc(0.5)
        self.assertEqual(expN50, obsN50)
        self.assertEqual(expL50, obsL50)
  
    def test_NandLperc_90(self):
        lValues = [144, 33, 111, 79, 292, 306]
        iStat = Stat(lValues)
        expL90 = 79
        expN90 = 5
        obsN90, obsL90 = iStat.NandLperc(0.9)
        self.assertEqual(expN90, obsN90)
        self.assertEqual(expL90, obsL90)
 
    def test_NandLperc_emptyListe(self):
        lValues = []
        iStat = Stat(lValues)
        expL90 = 0
        expN90 = 0
        obsN90, obsL90 = iStat.NandLperc(0.9)
        self.assertEqual(expN90, obsN90)
        self.assertEqual(expL90, obsL90)
 
   
    def test_NandLperc_90_onlyOneValue(self):
        lValues = [15]
        iStat = Stat(lValues)
        expL90 = 0
        expN90 = 0
        obsN90, obsL90 = iStat.NandLperc(0.9)
        self.assertEqual(expN90, obsN90)
        self.assertEqual(expL90, obsL90)
        
if __name__ == "__main__":
    unittest.main()