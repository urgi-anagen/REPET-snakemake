# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

from commons.core.coord.Path import Path
from commons.core.LoggerFactory import LoggerFactory

LOG_DEPTH = "repet.commons.core.coord"


## Merge Path objects with the same ID in a single object
#
class MergedPath(object):
    __slots__ = ("_lPaths", "_length", "_ID", "_nbPaths", "_log")

    ## Constructor
    #
    def __init__(self, lPaths=None, verbosity=0):
        self._log = LoggerFactory.createLogger("{}.{}".format(LOG_DEPTH, self.__class__.__name__), verbosity=verbosity)
        self._lPaths = []
        self._length = 0
        self._ID = None
        self._nbPaths = 0

        if lPaths:
            for p in lPaths:
                self.add(p)

    ## repr
    #
    def __repr__(self):
        return self.toString()

    ## Equal operator
    #
    def __eq__(self, o):
        equal = True
        if type(o) is not type(self):
            equal = False
        elif self.getID() != o.getID() or self.getLength() != o.getLength() or self._nbPaths != o._nbPaths:
            equal = False
        elif self.getID() == o.getID() and self.getLength() == o.getLength() and self._nbPaths == o._nbPaths:
            lPath = self.getPathsList()
            lOtherPath = o.getPathsList()
            lPathAsString = []
            for path in lPath:
                lPathAsString.append(path.toString())
            lOtherPathAsString = []
            for path in lOtherPath:
                lOtherPathAsString.append(path.toString())
            if sorted(lPathAsString) != sorted(lOtherPathAsString):
                equal = False
        return equal

    ## Not equal operator
    #
    def __ne__(self, o):
        return not self.__eq__(o)

    ## Try to add a Path instance to this MergedPath instance
    #
    # @param path a Path instance
    #
    def add(self, path):
        if not isinstance(path, Path):
            msg = "Error when adding to MergedPath : Not a Path"
            self._log.warning(msg)
        elif self._ID and self._ID != path.getIdentifier():
            msg = "Couldn't add Path ({}) to MergedPath ({}) : Different IDs".format(path.getIdentifier(), self._ID)
            self._log.warning(msg)
        else:
            self._ID = path.getIdentifier()
            self._lPaths.append(path)
            self._length += path.getAlignInstance().getLengthOnQuery()
            self._nbPaths = len(self._lPaths)

    ## Get the total length of the Paths in this MergedPath instance
    #
    def getLength(self):
        return self._length

    ## Get the list of Paths objects in this MergedPath instance
    # 
    def getPathsList(self):
        return self._lPaths

    ## Get the ID of the Path
    #
    def getID(self):
        return self._ID

    ## Get the number of paths (fragments)
    #
    def getNbPaths(self):
        return self._nbPaths

    ## Get the weighted identity
    #
    def getWeightedIdentity(self):
        res = 0
        if self._nbPaths > 0:
            sumIdentity = sum(
                (abs(path.range_query.end - path.range_query.start) + 1) * path.identity for path in self._lPaths)
            sumSizes = sum(abs(path.range_query.end - path.range_query.start) + 1 for path in self._lPaths)
            res = sumIdentity / sumSizes
            res = float("{0:.2f}".format(res))  # Truncate to 2 decimals
        return res

    ## Return the attributes as a formatted string
    #
    def toString(self):
        string = "Empty MergedPath()"
        if self._ID:
            string = "ID: {}".format(self._ID)
            string += "\tLength: {}".format(self._length)
            string += "\tNbPaths: {}".format(self._nbPaths)
            string += "\tPaths:\n"
            string += "\n".join([p.toString() for p in self._lPaths])
        return string
