from commons.core.coord.Path import Path


class MatchingSubject(Path):

    ## Constructor
    #
    # @param id identifier
    # @param range_q a Range instance for the query
    # @param range_s a Range instance for the subject
    # @param e_value float E-value of the match
    # @param score integer score of the match
    # @param identity float identity percentage of the match
    # @param length integer length of the subject
    #
    def __init__(self, path=Path(), length=-1):
        self._length = length
        Path.__init__(self, path.id, path.range_query, path.range_subject, path.e_value, path.score, path.identity)

    ## Equal operator
    #
    def __eq__(self, o):
        if type(o) is not type(self) or self._length != o._length:
            return False
        return Path.__eq__(self, o)

    ## Not equal operator
    #
    def __ne__(self, o):
        return not self.__eq__(o)

    def getProteinDomainLength(self):
        return self._length

    def setProteinDomainLength(self, length):
        self._length = length