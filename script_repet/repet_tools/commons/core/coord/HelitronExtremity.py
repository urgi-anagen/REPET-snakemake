class HelitronExtremity(object):

    def __init__(self, name = "", eValue = -1, start = -1, end = -1):
        self._name = name
        self._eValue = float(eValue)
        self._start = start
        self._end = end
        
    def __eq__(self, o):
        if type(o) is type(self):
            return self._name == o._name and self._eValue == o._eValue \
                and self._start == o._start and self._end == o._end
        return False
            
    def __ne__(self, o):
        return not self.__eq__(o)
        
    def setName(self, name):
        self._name = name
        
    def setEValue(self, eValue):
        self._eValue = float(eValue)
        
    def setStart(self, start):
        self._start = start
        
    def setEnd(self, end):
        self._end = end
        
    def getStart(self):
        return self._start
    
    def getEnd(self):
        return self._end
    
    def getEValue(self):
        return self._eValue
    
    def getName(self):
        return self._name 
    
    def __str__(self):
        return "%s: (%s, %s, %s)" % (self._name, self._eValue, self._start, self._end)