## This class contains result of classifier agent, that have to fill a black board result
#
class ClassifierResult(object):

    def __init__ (self, demand, message):
        self.demand = demand
        self.message = message

    def __eq__(self, o):
        if type(o) is type(self):
            return self.__class__ == o.__class__ and o.demand == self.demand and o.message == self.message
        return False
    
    def __ne__(self, o):
        return not self.__eq__(o)
    
    def __str__(self):
        return "demand=%s, message=%s" % (self.demand, self.message)