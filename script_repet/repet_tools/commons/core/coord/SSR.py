class SSR(object):

    def __init__(self, pattern = "", position = ""):
        self._pattern = pattern
        self._position = position
        
    def __eq__(self, o):
        if type(o) is type(self):
            return self._pattern == o._pattern and self._position == o._position
        return False
    
    def __ne__(self, o):
        return not self.__eq__(o)
    
    def __str__(self):
        return "{}_{}".format(self._pattern, self._position)