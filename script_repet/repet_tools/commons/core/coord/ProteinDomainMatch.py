## This class represents a protein domain match on a sequence
#
class ProteinDomainMatch(object):
    # TODO: replace "coverageOnSubject" by "coverageOnSubjectWithoutOverlaps" or "coverageWithoutOverlaps" etc... "coverage" and "coverageOnSubject" are both on subjects!
    """
    @param name: string Profile name : (subject name)
    @param coverage: float Cumulative profile coverage of all profile's matches (percentage)
    @param start: integer Position on query sequence corresponding to the beginning of the first profile's match
    @param strand: string Value can be "+", "-", "."
    @param frame: integer Value can be 1, 2, 3, -1, -2, -3
    @param end: integer Position on query sequence corresponding to the end of the last profile's match
    @param index: float weighted coverage
    @param coverageOnSubject: float Profile coverage of all non-overlapping profile's matches (percentage)
    """

    def __init__(self, name, coverage=0.0, start=0, strand="", frame=0, end=0, index=0, coverageOnSubject=0.0):
        self._name = name
        self._coverage = coverage
        self._start = start
        self._strand = strand
        self._frame = frame
        self._end = end
        self._index = index
        self._coverageOnSubject = coverageOnSubject

    def __eq__(self, o):
        if type(o) is type(self):
            return (self._name == o._name and round(self._coverage, 3) == round(o._coverage, 3) \
                    and self._start == o._start and self._strand == o._strand \
                    and self._frame == o._frame and self._end == o._end and round(self._index, 2) == round(o._index,
                                                                                                           2) and round(
                        self._coverageOnSubject, 3) == round(o._coverageOnSubject, 3))
        return False

    def __ne__(self, o):
        return not self.__eq__(o)

    def __str__(self):
        return "name=%s, coverage=%0.2f, start=%d, strand='%s', frame=%s, end=%d, index=%0.2f, coverageOnSubject=%0.2f" % (
        self._name, self._coverage, self._start, self._strand, self._frame, self._end, self._index,
        self._coverageOnSubject)

    def __repr__(self):
        return self.__str__()

    def getType(self):
        return self._name.split("_")[-2]

    def getName(self):
        return self._name

    def getStart(self):
        return self._start

    def getCoverage(self):
        return float(self._coverage)

    def getCoverageOnSubject(self):
        return float(self._coverageOnSubject)

    def getFrame(self):
        return self._frame

    def getStrand(self):
        return self._strand

    def setEnd(self, end):
        self._end = end

    def setStrand(self, strand):
        self._strand = strand

    def increaseCoverage(self, coverage):
        self._coverage += coverage

    def computeIndex(self, scoreWeight):
        self._index = round(self._coverage * scoreWeight, 2)