# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

## Record a region on multiple sequence using Path ID information
#
class MergedRange(object):
    __slots__ = ("_lId", "_start", "_end")

    ## Constructor
    #
    # @param lId list of Path ID
    # @param start the start coordinate
    # @param end the end coordinate
    #
    def __init__(self, lId=None, start=-1, end=-1):
        self._lId = lId or []
        self._start = start
        self._end = end

        ## Equal operator

    #
    # @param o a MergedRange instance
    #        
    def __eq__(self, o):
        if type(o) is type(self):
            return o._lId == self._lId and o._start == self._start and o._end == self._end
        else:
            return False

    ## Not equal operator
    #
    def __ne__(self, o):
        return not self.__eq__(o)

    ## Return True if the MergedRange instance overlaps with another MergedRange instance, False otherwise 
    #
    # @param o a MergedRange instance
    # @return boolean False or True
    #
    def isOverlapping(self, o):
        if o._start <= self._start and o._end >= self._end:
            return True
        if o._start >= self._start and o._start <= self._end or o._end >= self._start and o._end <= self._end:
            return True
        return False

    ## Merge coordinates and ID of two Merged Range     
    #
    # @param o a MergedRange instance
    #
    def merge(self, o):
        self._start = min(self._start, o._start)
        self._end = max(self._end, o._end)
        self._lId.extend(o._lId)
        self._lId.sort()

    ## Set a Merged Range instance using a Match instance
    #
    # @param iMatch instance Match instance 
    # 
    def setFromMatch(self, iMatch):
        self._lId = [iMatch.id]
        self._start = iMatch.range_query.start
        self._end = iMatch.range_query.end

    ## Get a Merged Range instance list using a Match instance list
    #
    # @param lIMatch list Match instance list
    # @return lMergedRange list MergedRange instance list
    #
    @staticmethod
    def getMergedRangeListFromMatchList(lIMatch):
        lMergedRange = []
        for iMatch in lIMatch:
            mr = MergedRange()
            mr.setFromMatch(iMatch)
            lMergedRange.append(mr)
        return lMergedRange
