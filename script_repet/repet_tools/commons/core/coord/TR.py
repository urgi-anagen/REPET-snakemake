class TR(object):
    
    ## Constructor
    #
    # @param type string value in termLTR, non-termLTR, termTIR, non-termTIR
    # @param length int length of TR
    #
    def __init__(self, type = "", length = -1):
        self._type = type
        self._length = length

    def __eq__(self, o):
        if type(o) is type(self):
            return o._type == self._type and o._length == self._length
        return False
    
    def __ne__(self, o):
        return not self.__eq__(o)
    
    def setType(self, type):
        self._type = type
    
    def getType(self):
        return self._type
    
    def getLength(self):
        return self._length
    
    def isEmpty(self):
        return self._type == ""
    
    def __str__(self):
        return "%s: %i" % (self._type, self._length)