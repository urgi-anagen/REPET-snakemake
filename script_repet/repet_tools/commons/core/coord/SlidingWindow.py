# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

class SlidingWindow(object):

    def __init__(self, length=1, overlap=1):
        self._length = length
        self._overlap = overlap
        self._start = 1
        self._end = length
        self._step = length - overlap

    def slideWindowOnce(self):
        self._start = self._start + self._step
        self._end = self._end + self._step

    def getStart(self):
        return self._start

    def getEnd(self):
        return self._end

    def getLength(self):
        return self._length

    def getOverlap(self):
        return self._overlap

    def getMiddle(self):
        return int(self._start + ((self._end - self._start - 1) / 2))

    """
    def setStart(self, start):
        self._start = start

    def setEnd(self, end):
        self._end = end

    def setLength(self, length):
        self._length = length

    def setOverlap(self, overlap):
        self._overlap = overlap
    """

    def getSlidingMsg(self):
        return "Window is sliding : {} {}".format(self._start, self._end)


class SlidingWindowToCountMatchingBases(SlidingWindow):

    def getSetLengthOnWindow(self, iSet):
        if self._isSetIncludedInTheWindow(iSet):
            return iSet.getLength()
        if self._isWindowIncludedInTheSet(iSet):
            return self._length
        elif self._isSetOverlapTheRightSideOfTheWindow(iSet):
            return self._end - iSet.getMin() + 1
        elif self._isSetOverlapTheLeftSideOfTheWindow(iSet):
            return iSet.getMax() - self._start + 1

    def getCoordSetOnWindow(self, iSet):
        if self._isSetIncludedInTheWindow(iSet):
            return iSet.getStart(), iSet.getEnd()
        if self._isWindowIncludedInTheSet(iSet):
            return self.getStart(), self.getEnd()
        elif self._isSetOverlapTheRightSideOfTheWindow(iSet):
            return iSet.getStart(), self.getEnd()
        elif self._isSetOverlapTheLeftSideOfTheWindow(iSet):
            return self.getStart(), iSet.getEnd()

    def _isSetIncludedInTheWindow(self, feature):
        return feature.getMin() >= self._start and feature.getMax() <= self._end

    def _isWindowIncludedInTheSet(self, feature):
        return self._start >= feature.getMin() and self._end <= feature.getMax()

    def _isSetOverlapTheRightSideOfTheWindow(self, feature):
        return feature.getMin() <= self._end and feature.getMin() >= self._start

    def _isSetOverlapTheLeftSideOfTheWindow(self, feature):
        return feature.getMax() <= self._end and feature.getMax() >= self._start
