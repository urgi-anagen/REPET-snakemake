LABEL = "SSR"

class SSRCollection(object):

    def __init__(self, coverage=""):
        self._coverage = coverage
        self._lSSR = []

    def __eq__(self, o):
        if type(o) is type(self):
            return self._coverage == o._coverage and self._lSSR == o._lSSR
        return False

    def __ne__(self, o):
        return not self.__eq__(o)

    # TODO: review with SuperAgent (OutputWriter)
    def __str__(self):
        if self._lSSR == []:
            return ""
        str = "{}: {}".format(LABEL, self._lSSR[0])
        for iSSR in self._lSSR[:1]:
            str += ", {}".format(iSSR)
        return str

    def strCoverage(self):
        return "SSRCoverage: {}%".format(self._coverage)

    ############################################

    def append(self, iSSR):
        self._lSSR.append(iSSR)

    def setCoverage(self, coverage):
        self._coverage = coverage

    def isEmpty(self):
        return self._lSSR == []

    def getSSRList(self):
        return self._lSSR

    def getSSRCoverage(self):
        return self._coverage