class BlasterMatch(object):

    def __init__(self, bestMatchName = "", coverage = 0.0, strand = "+", identity = 0.0):
        self._name = bestMatchName
        self._coverage = round(coverage, 2)
        self._strand = strand
        self._identity = identity

    def __eq__(self, o):
        # if type(o) is type(self):
        #     return o._name == self._name and o._coverage == self._coverage \
        #         and self._strand == o._strand and self._identity == o._identity
        if type(o) is type(self):
            return o._name == self.getName() and o._coverage == self.getCoverage() and o._strand == self.getStrand()  and o._identity == self.getIdentity()

        return False
            
    def __ne__(self, o):
        return not self.__eq__(o)
    
    def __str__(self):
        return "{}: {:.2f}%".format(self._name, self._coverage)
    
    def isEmpty(self):
        return self._name == ""
    
    def getName(self):
        return self._name
    
    def getCoverage(self):
        return float(self._coverage)
    
    # For BLRtx_x_Agents
    def getStrand(self):
        return self._strand
    
    # For HG_BLRn_Agent
    def getIdentity(self):
        return float(self._identity)