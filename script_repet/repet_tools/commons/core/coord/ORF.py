class ORF(object):

    def __init__(self, sizeCategory = "", lProfileTypes = []):
        self._sizeCategory = sizeCategory
        self._lProfileTypes = lProfileTypes
        
    def __eq__(self, o):
        if type(o) is type(self):
            return self._sizeCategory == o._sizeCategory and self._lProfileTypes == o._lProfileTypes
        return False
    
    def __ne__(self, o):
        return not self.__eq__(o)
        
    def setSizeCategory(self, sizeCategory):
        self._sizeCategory = sizeCategory
        
    def setProfileTypesList(self, lProfileTypes):
        self._lProfileTypes = lProfileTypes
    
    def getProfileTypesList(self):
        return self._lProfileTypes
    
    def isEmpty(self):
        return self._sizeCategory == ""
        
    def __str__(self):
        orfToString = self._sizeCategory
        if self._lProfileTypes != []:
            orfToString += " "
            orfToString += " ".join(self._lProfileTypes)
        return orfToString