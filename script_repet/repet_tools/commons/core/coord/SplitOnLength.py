# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

from commons.core.LoggerFactory import LoggerFactory

LOG_DEPTH = "repet.commons.core.coord"


## Splits a list of objects implementing getLength() based on a list of length thresholds
#
class SplitOnLength(object):

    def __init__(self, lObjects, lThresholds, verbosity=0):
        self._log = LoggerFactory.createLogger("{}.{}".format(LOG_DEPTH, self.__class__.__name__), verbosity=verbosity)
        self._lObjects = lObjects
        self._lThresholds = lThresholds

    ## Splits the list of objects over the list of thresholds.
    #
    # @return a list of lists (groups) of objects
    #
    def split(self):
        lSplit = [self._lObjects]
        doObjectsImplementGetLength = False not in set([hasattr(o, "getLength") for o in self._lObjects])

        if not self._lObjects:
            self._log.warning("Empty input objects list, no split.")
        elif not doObjectsImplementGetLength:
            self._log.warning("At least one object in the list does not implement getLength(), no split.")
        elif not self._lThresholds:
            self._log.warning("Empty input thresholds list, no split.")
        elif not self._lThresholds == sorted(self._lThresholds):
            self._log.warning("Input thresholds list isn't sorted, no split. ({})".format(self._lThresholds))
        else:
            lSplit = [[] for i in range(len(self._lThresholds) + 1)]

            for obj in self._lObjects:
                if obj.getLength() <= self._lThresholds[0]:
                    lSplit[0].append(obj)
                elif self._lThresholds[-1] < obj.getLength():
                    lSplit[-1].append(obj)
                else:
                    for i in range(0, len(self._lThresholds) - 1):
                        if self._lThresholds[i] < obj.getLength() <= self._lThresholds[i + 1]:
                            lSplit[i + 1].append(obj)
                            break
        return lSplit
