LABEL = "polyAtail"

class PolyATail(object):
    
    def __init__(self, isTailPresent = False):
        self._isTailPresent = isTailPresent
        
    def __eq__(self, o):
        if type(o) is type(self):
            return self._isTailPresent == o._isTailPresent
        return False
    
    def __ne__(self, o):
        return not self.__eq__(o)
    
    def __str__(self):
        return LABEL
    
    def setIsTailPresent(self, isTail ):
        self._isTailPresent = isTail
        
    def isTailPresent(self):
        return self._isTailPresent

    