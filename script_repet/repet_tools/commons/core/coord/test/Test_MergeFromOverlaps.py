import unittest
from commons.core.coord.MergeFromOverlaps import MergeFromOverlaps
from commons.core.coord.MergedRange import MergedRange
from commons.core.coord.Range import Range

class Test_MergeFromOverlaps(unittest.TestCase):
        
    def test_clusterize_One_Merged_Range(self):
        lUnsorted = []
        lUnsorted.append(MergedRange([1], 1, 10))
        
        mfo = MergeFromOverlaps(lUnsorted)
        lObsSorted = mfo.clusterize()
        
        lExpSorted = [MergedRange([1], 1, 10)]
        
        self.assertEqual(lExpSorted, lObsSorted)
        
    def test_clusterize_Two_Merged_Range_not_overlapping(self):
        lUnsorted = []
        lUnsorted.append(MergedRange([1], 1, 10))
        lUnsorted.append(MergedRange([2], 11, 20))
        
        mfo = MergeFromOverlaps(lUnsorted)
        lObsSorted = mfo.clusterize()
        
        lExpSorted = [MergedRange([1], 1, 10), MergedRange([2], 11, 20)]
        
        self.assertEqual(lExpSorted, lObsSorted)
        
    def test_clusterize_Two_Merged_Range_overlapping(self):
        lUnsorted = []
        lUnsorted.append(MergedRange([1], 1, 10))
        lUnsorted.append(MergedRange([2], 5, 15))
        
        mfo = MergeFromOverlaps(lUnsorted)
        lObsSorted = mfo.clusterize()
        
        lExpSorted = [MergedRange([1, 2], 1, 15)]
        
        self.assertEqual(lExpSorted, lObsSorted)
        
    def test_clusterize_Three_Merged_Range_overlapping(self):
        lUnsorted = []
        lUnsorted.append(MergedRange([1], 1, 10))
        lUnsorted.append(MergedRange([2], 10, 20))
        lUnsorted.append(MergedRange([3], 5, 15))
        
        mfo = MergeFromOverlaps(lUnsorted)
        lObsSorted = mfo.clusterize()
        
        lExpSorted = [MergedRange([1, 2, 3], 1, 20)]
        
        self.assertEqual(lExpSorted, lObsSorted)
        
    def test_clusterize_Three_Merged_Range_with_different_overlapping(self):
        lUnsorted = []
        lUnsorted.append(MergedRange([1], 1, 10))
        lUnsorted.append(MergedRange([2], 14, 20))
        lUnsorted.append(MergedRange([3], 5, 15))
        
        mfo = MergeFromOverlaps(lUnsorted)
        lObsSorted = mfo.clusterize()
        
        lExpSorted = [MergedRange([1, 2, 3], 1, 20)]
        
        self.assertEqual(lExpSorted, lObsSorted)
        
    def test_clusterize_five_Ranges_with_different_sequences(self):
        lFeaturesToMerge = []
        lFeaturesToMerge.append(Range("seq1", 1, 10))
        lFeaturesToMerge.append(Range("seq1", 8, 20))
        lFeaturesToMerge.append(Range("seq1", 5, 15))
        lFeaturesToMerge.append(Range("seq2", 1, 10))
        lFeaturesToMerge.append(Range("seq2", 5, 15))
         
        mfo = MergeFromOverlaps(lFeaturesToMerge)
        lObsMerged = mfo.clusterize()
         
        lExpMerged = [Range("seq1", 1, 20), Range("seq2", 1, 15)]

        self.assertEqual(lExpMerged, lObsMerged)
        
if __name__ == "__main__":
    unittest.main()