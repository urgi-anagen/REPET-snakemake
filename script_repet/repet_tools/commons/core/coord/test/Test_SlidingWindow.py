import unittest
from commons.core.coord.SlidingWindow import SlidingWindow
from commons.core.coord.SlidingWindow import SlidingWindowToCountMatchingBases
from commons.core.coord.Set import Set

class Test_SlidingWindow( unittest.TestCase ):
        
    def test_slideWindowOnce( self ):
        expStart = 91 
        expEnd = 190
        self.sw = SlidingWindow(100, 10)
        self.sw.slideWindowOnce()
        obsStart = self.sw._start
        obsEnd = self.sw._end
        
        self.assertEqual(expStart, obsStart)
        self.assertEqual(expEnd, obsEnd)
        
    def test_slideWindowOnceFourTime( self ):
        expStart = 201 
        expEnd = 300
        self.sw = SlidingWindow(100, 50)
        i = 0
        for i in range(4):
            self.sw.slideWindowOnce()
            i += 1
        obsStart = self.sw._start
        obsEnd = self.sw._end
        
        self.assertEqual(expStart, obsStart)
        self.assertEqual(expEnd, obsEnd)
        
    def test_getMiddle_even( self ):
        expMiddle1 = 50
        expMiddle2 = 100
        self.sw = SlidingWindow(100, 50)
        
        obsMiddle1 = self.sw.getMiddle()
        self.sw.slideWindowOnce()
        obsMiddle2 = self.sw.getMiddle()
        
        self.assertEqual(expMiddle1, obsMiddle1)
        self.assertEqual(expMiddle2, obsMiddle2)
        
    def test_getMiddle_odd( self ):
        expMiddle1 = 49
        expMiddle2 = 98
        self.sw = SlidingWindow(99, 50)
        
        obsMiddle1 = self.sw.getMiddle()
        self.sw.slideWindowOnce()
        obsMiddle2 = self.sw.getMiddle()
        
        self.assertEqual(expMiddle1, obsMiddle1)
        self.assertEqual(expMiddle2, obsMiddle2)
        
        
class Test_SlidingWindowToCountMatchingBases(unittest.TestCase):
        
    def test_getSetLengthOnWindow_featureIncluded( self ):
        self.sw = SlidingWindowToCountMatchingBases(100, 1)
        iSet = Set( 1, "TE3", "chr1", 21, 30 )
        exp = 10
        obs = self.sw.getSetLengthOnWindow( iSet)
        self.assertEqual( exp, obs )
        
    def test_getSetLengthOnWindow_windowIncluded( self ):
        self.sw = SlidingWindowToCountMatchingBases(100, 10)
        self.sw.slideWindowOnce()
        iSet = Set( 1, "TE3", "chr1", 21, 530 )
        exp = 100
        obs = self.sw.getSetLengthOnWindow( iSet)
        self.assertEqual( exp, obs )
        
    def test_getSetLengthOnWindow_featureOverlapLeft( self ):
        self.sw = SlidingWindowToCountMatchingBases(100, 10)
        self.sw.slideWindowOnce()
        iSet = Set( 1, "TE3", "chr1", 21, 130 )
        exp = 40
        obs = self.sw.getSetLengthOnWindow( iSet)
        self.assertEqual( exp, obs )
        
    def test_getSetLengthOnWindow_featureOverlapRight( self ):
        self.sw = SlidingWindowToCountMatchingBases(100, 10)
        self.sw.slideWindowOnce()
        iSet = Set( 1, "TE3", "chr1", 121, 230 )
        exp = 70
        obs = self.sw.getSetLengthOnWindow( iSet)
        self.assertEqual( exp, obs )
        
    def test_getCoordSetOnWindow_featureIncluded( self ):
        self.sw = SlidingWindowToCountMatchingBases(100, 1)
        iSet = Set( 1, "TE3", "chr1", 21, 30 )
        expStart = 21
        expEnd = 30
        obsStart,obsEnd = self.sw.getCoordSetOnWindow( iSet)
        self.assertEqual( expStart, obsStart )
        self.assertEqual( expEnd, obsEnd )
        
    def test_getCoordSetOnWindow_windowIncluded(self):
        self.sw = SlidingWindowToCountMatchingBases(100, 10)
        self.sw.slideWindowOnce()
        iSet = Set(1, "TE3", "chr1", 21, 530)
        expStart = 91
        expEnd = 190
        obsStart,obsEnd = self.sw.getCoordSetOnWindow( iSet)
        self.assertEqual(expStart, obsStart)
        self.assertEqual(expEnd, obsEnd)
        
    def test_getCoordSetOnWindow_featureOverlapLeft(self):
        self.sw = SlidingWindowToCountMatchingBases(100, 10)
        self.sw.slideWindowOnce()
        iSet = Set(1, "TE3", "chr1", 21, 130)
        expStart = 91
        expEnd = 130
        obsStart,obsEnd = self.sw.getCoordSetOnWindow(iSet)
        self.assertEqual(expStart, obsStart)
        self.assertEqual(expEnd, obsEnd)
        
    def test_getCoordSetOnWindow_featureOverlapRight( self ):
        self.sw = SlidingWindowToCountMatchingBases(100, 10)
        self.sw.slideWindowOnce()
        iSet = Set(1, "TE3", "chr1", 121, 230)
        expStart = 121
        expEnd = 190
        obsStart,obsEnd = self.sw.getCoordSetOnWindow(iSet)
        self.assertEqual(expStart, obsStart)
        self.assertEqual(expEnd, obsEnd)


if __name__ == "__main__":
    unittest.main()