# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.


import os
import sys
import time
import unittest
from commons.core.coord.Map import Map
from commons.core.coord.Set import Set
from commons.core.coord.MapUtils import MapUtils
from commons.core.utils.FileUtils import FileUtils

# if not 'REPET_PATH' in os.environ:
#     print("ERROR: no environment variable REPET_PATH")
#     sys.exit(1)

class Test_MapUtils(unittest.TestCase):
    
    def test_getMapListFromFile(self):
        mapFile = "dummyFile_{}_{}" .format (time.strftime("%Y%m%d%H%M%S"), os.getpid())
        with open(mapFile, "w") as fileHandler:
            fileHandler.write("A\tchr4\t151\t250\n")
            fileHandler.write("B\tchr4\t301\t500\n")
        p1 = Map()
        p1.setFromTuple(("A", "chr4", "151", "250"))
        p2 = Map()
        p2.setFromTuple(("B", "chr4", "301", "500"))
        expLMap = [p1, p2]
        obsLMap = MapUtils.getMapListFromFile(mapFile)
        os.remove(mapFile)
        self.assertEqual(expLMap, obsLMap)
        
    def test_getMapListFromFile_empty_file(self):
        mapFile = "dummyFile_{}_{}" .format (time.strftime("%Y%m%d%H%M%S"), os.getpid())
        fileHandler = open(mapFile, "w")
        fileHandler.close()
        expLMap = []
        obsLMap = MapUtils.getMapListFromFile(mapFile)
        os.remove(mapFile)
        self.assertEqual(expLMap, obsLMap)
    
    def test_getMapListSortedByIncreasingMinThenMax( self ):
        iMap1 = Map("name1", "chr1", 1, 350)
        iMap2 = Map("name2", "chr1", 1, 100)
        iMap3 = Map("name3", "chr1", 50, 350)
        iMap4 = Map("name4", "chr1", 5, 450)
        lMaps = [ iMap1, iMap2, iMap3, iMap4 ]
        
        expLMaps = [ iMap2, iMap1, iMap4, iMap3 ]
        
        obsLMaps = MapUtils.getMapListSortedByIncreasingMinThenMax( lMaps )
        
        self.assertEqual( expLMaps, obsLMaps )
        
        
    def test_getMapListSortedByIncreasingMinThenMax_ordered( self ):
        iMap1 = Map("name1", "chr1", 1, 100)
        iMap2 = Map("name2", "chr1", 1, 350)
        
        lMaps = [ iMap1, iMap2 ]
        expLMaps = [ iMap1, iMap2 ]
        
        obsLMaps = MapUtils.getMapListSortedByIncreasingMinThenMax( lMaps )
        
        self.assertEqual( expLMaps, obsLMaps )
        
        
    def test_getMapListSortedByIncreasingMinThenMax_unordered( self ):
        iMap1 = Map("name1", "chr1", 1, 350)
        iMap2 = Map("name2", "chr1", 1, 100)
        
        lMaps = [ iMap1, iMap2 ]
        expLMaps = [ iMap2, iMap1 ]
        
        obsLMaps = MapUtils.getMapListSortedByIncreasingMinThenMax( lMaps )
        
        self.assertEqual(expLMaps, obsLMaps )
        
        
    def test_getMapListSortedByIncreasingMinThenMax_nonOverlapping( self ):
        iMap1 = Map("name1", "chr1", 1, 350)
        iMap2 = Map("name2", "chr1", 400, 600)
        
        lMaps = [ iMap2, iMap1 ]
        expLMaps = [ iMap1, iMap2 ]
        
        obsLMaps = MapUtils.getMapListSortedByIncreasingMinThenMax( lMaps )
        
        self.assertEqual( expLMaps, obsLMaps )
        
        
    def test_getMapListSortedByIncreasingMinThenMax_sameMinThreeMaps( self ):
        iMap1 = Map("name1", "chr1", 350, 1)
        iMap2 = Map("name2", "chr1", 400, 1)
        iMap3 = Map("name3", "chr1", 500, 1)
        
        lMaps = [ iMap2, iMap1, iMap3 ]
        expLMaps = [ iMap1, iMap2, iMap3 ]
        
        obsLMaps = MapUtils.getMapListSortedByIncreasingMinThenMax( lMaps )
        
        self.assertEqual( expLMaps, obsLMaps )
        
        
    def test_getMapListSortedByIncreasingMinThenMax_included( self ):
        iMap1 = Map("name1", "chr1", 350, 1)
        iMap2 = Map("name2", "chr1", 300, 10)
        
        lMaps = [ iMap2, iMap1 ]
        expLMaps = [ iMap1, iMap2]
        
        obsLMaps = MapUtils.getMapListSortedByIncreasingMinThenMax( lMaps )
        
        self.assertEqual( expLMaps, obsLMaps )
        
        
    def test_getMapListSortedByIncreasingMinThenMax_equal( self ):
        iMap1 = Map("name1", "chr1", 350, 1)
        iMap2 = Map("name2", "chr1", 350, 1)
        
        lMaps = [ iMap2, iMap1 ]
        expLMaps = [ iMap2, iMap1 ]
        notExpLMaps = [ iMap1, iMap2 ]
        
        obsLMaps = MapUtils.getMapListSortedByIncreasingMinThenMax( lMaps )
        
        self.assertEqual(expLMaps, obsLMaps)
        self.assertNotEqual(notExpLMaps, obsLMaps)
        
        
    def test_getMapListSortedByIncreasingNameThenSeqnameThenMinThenMax( self ):
        iMap1 = Map("name1", "chr1", 1, 350)
        iMap2 = Map("name4", "chr2", 5, 450)
        iMap3 = Map("name4", "chr1", 10, 450)
        iMap4 = Map("name4", "chr1", 10, 500)
        iMap5 = Map("name4", "chr1", 5, 450)
        iMap6 = Map("name3", "chr1", 50, 350)
        iMap7 = Map("name2", "chr1", 1, 100)

        lMaps = [ iMap1, iMap2, iMap3, iMap4, iMap5, iMap6, iMap7 ]
        
        expLMaps = [ iMap1, iMap7, iMap6, iMap5, iMap3, iMap4, iMap2 ]
        
        obsLMaps = MapUtils.getMapListSortedByIncreasingNameThenSeqnameThenMinThenMax( lMaps )
        
        self.assertEqual( expLMaps, obsLMaps )
        
        
    def test_getMapListSortedByIncreasingNameThenSeqnameThenMinThenMax_ordered( self ):
        iMap1 = Map("name1", "chr1", 1, 100)
        iMap2 = Map("name1", "chr2", 1, 350)
        
        lMaps = [ iMap1, iMap2 ]
        expLMaps = [ iMap1, iMap2 ]
        
        obsLMaps = MapUtils.getMapListSortedByIncreasingNameThenSeqnameThenMinThenMax( lMaps )
        
        self.assertEqual( expLMaps, obsLMaps )
        
        
    def test_getMapListSortedByIncreasingNameThenSeqnameThenMinThenMax_unordered( self ):
        iMap1 = Map("name1", "chr2", 1, 350)
        iMap2 = Map("name1", "chr1", 1, 100)
        
        lMaps = [ iMap1, iMap2 ]
        expLMaps = [ iMap2, iMap1 ]
        
        obsLMaps = MapUtils.getMapListSortedByIncreasingNameThenSeqnameThenMinThenMax( lMaps )
        
        self.assertEqual( expLMaps, obsLMaps )
        
        
    def test_getMapListSortedByIncreasingNameThenSeqnameThenMinThenMax_nonOverlapping( self ):
        iMap1 = Map("name1", "chr1", 1, 350)
        iMap2 = Map("name2", "chr1", 400, 600)
        
        lMaps = [ iMap2, iMap1 ]
        expLMaps = [ iMap1, iMap2 ]
        
        obsLMaps = MapUtils.getMapListSortedByIncreasingNameThenSeqnameThenMinThenMax( lMaps )
        
        self.assertEqual( expLMaps, obsLMaps )
        
        
    def test_getMapListSortedByIncreasingNameThenSeqnameThenMinThenMax_sameMinThreeMaps( self ):
        iMap1 = Map("name2", "chr1", 150, 10)
        iMap2 = Map("name2", "chr1", 1, 100)
        iMap3 = Map("name2", "chr1", 100, 1)
        
        lMaps = [ iMap1, iMap2, iMap3 ]
        expLMaps = [ iMap2, iMap3, iMap1 ]
        
        obsLMaps = MapUtils.getMapListSortedByIncreasingNameThenSeqnameThenMinThenMax( lMaps )
        
        self.assertEqual( expLMaps, obsLMaps )
        

    def test_getDictPerNameFromMapFile( self ):
        iMap1 = Map( "chunk1", "chromosome1", 1, 100 )
        iMap2 = Map( "chunk2", "chromosome1", 91, 190 )
        iMap3 = Map( "chunk3", "chromosome2", 1, 100 )
        iMap4 = Map( "chunk1", "chromosome1", 1, 100 )  # redundant with iMap1
        
        mapFile = "dummyFile.map"
        with open( mapFile, "w" ) as mapFileHandler:
            for iMap in [ iMap1, iMap2, iMap3, iMap4 ]:
                iMap.write( mapFileHandler )
        
        dExp = { "chunk1": iMap1, "chunk2": iMap2, "chunk3": iMap3 }
        
        dObs = MapUtils.getDictPerNameFromMapFile( mapFile )
        
        self.assertEqual( dExp, dObs )
        
        os.remove( mapFile )
        
        
    def test_mapList2SetList(self):
        map1 = Map( "name1", "desc1", 1, 120 )
        map2 = Map( "name2", "desc2", 1, 20 )
        lMap = [ map1, map2 ]
        set1 = Set( 1, "name1", "desc1", 1, 120 )
        set2 = Set( 2, "name2", "desc2", 1, 20 )
        explMapSet = [ set1, set2 ]
        obslMapSet = MapUtils.mapList2SetList( lMap )
        
        self.assertEqual( explMapSet, obslMapSet )
        
        
    # def test_mergeCoordsInFile( self ):

    #     if os.path.exists(os.path.join(os.environ['REPET_PATH'],'bin')):
    #         os.environ["PATH"] += os.pathsep + os.path.join(os.environ['REPET_PATH'],'bin')
    #     else:
    #         print("The directory {} does not exist. Please verify the binaries directory exists.".format(os.path.join(os.environ['REPET_PATH'],'bin')))
    #         sys.exit(1)
    #     if ( "commons.core.checker.CheckerUtils" ) in sys.modules:
    #         inFile = "dummyInFile"
    #         with open( inFile, "w" ) as inFileHandler:
    #             inFileHandler.write( "TE1\tchr1\t1501\t2500\n" )
    #             inFileHandler.write( "TE3\tchr1\t4000\t3401\n" )
    #             inFileHandler.write( "TE4\tchr1\t3800\t3601\n" )
    #             inFileHandler.write( "TE5\tchr1\t2600\t2700\n" )
    #             inFileHandler.write( "TE6\tchr1\t2650\t2800\n" )

    #         expFile = "dummyExpFile"
    #         with open( expFile, "w" ) as expFileHandler:
    #             expFileHandler.write( "TE1\tchr1\t1501\t2500\n" )
    #             expFileHandler.write( "TE5\tchr1\t2600\t2800\n" )
    #             expFileHandler.write( "TE3\tchr1\t4000\t3401\n" )

    #         obsFile = "dummyObsFile"
    #         MapUtils.mergeCoordsInFile(inFile, obsFile)
    #         self.assertTrue( FileUtils.are2FilesIdentical( expFile, obsFile ) )
    #         for f in [inFile, expFile, obsFile]:
    #             os.remove(f)
                
    def test_getDictPerSeqNameFromMapFile(self):
        inFile = "dummyInFile"
        with open(inFile, "w") as inFileHandler:
            inFileHandler.write("TE1\tchr1\t1\t10\n")
            inFileHandler.write("TE2\tchr1\t60\t41\n")
            inFileHandler.write("TE3\tchr2\t5\t36\n")

        dExp = { "chr1": [ Map("TE1", "chr1", 1, 10), Map("TE2", "chr1", 60, 41) ],
                "chr2": [ Map("TE3", "chr2", 5, 36) ] }
        dObs = MapUtils.getDictPerSeqNameFromMapFile(inFile)
        self.assertEqual(dExp, dObs)
        os.remove(inFile)
                
    def test_getDictPerSeqNameFromMapFile_empty_file(self):
        inFile = "dummyInFile"
        inFileHandler = open( inFile, "w" )
        inFileHandler.close()
        dExp = {}
        dObs = MapUtils.getDictPerSeqNameFromMapFile( inFile )
        self.assertEqual( dExp, dObs )
        os.remove( inFile )

    def test_convertMapFileIntoSetFile(self):
        mapInputFile = "dummyExpFile"
        with open( mapInputFile, "w" ) as mapFileHandler:
            mapFileHandler.write( "seq31\tchr1\t151\t250\n" )
            mapFileHandler.write( "seq27\tchr2\t301\t500\n" )
            mapFileHandler.write( "seq40\tchr2\t600\t700\n" )
            mapFileHandler.write( "seq2\tchr3\t301\t500\n" )

        expSetFile = "dummyexpSetFile"
        with open( expSetFile, "w" ) as expSetFileHandler :
            expSetFileHandler.write( "1\tseq31\tchr1\t151\t250\n" )
            expSetFileHandler.write( "2\tseq27\tchr2\t301\t500\n" )
            expSetFileHandler.write( "3\tseq40\tchr2\t600\t700\n" )
            expSetFileHandler.write( "4\tseq2\tchr3\t301\t500\n" )

        
        obsFile = "dummyObsFile"
        
        MapUtils.convertMapFileIntoSetFile( mapInputFile, obsFile )
        
        self.assertTrue( FileUtils.are2FilesIdentical( expSetFile, obsFile ) )
        
        for f in [ expSetFile, mapInputFile, obsFile ]:
            os.remove( f )

    def test_convertMapFileIntoSetFile_one_line(self):
        mapInputFile = "dummyExpFile"
        with open( mapInputFile, "w" ) as mapFileHandler:
            mapFileHandler.write( "seq31\tchr1\t151\t250\n" )

        expSetFile = "dummyexpSetFile"
        with open( expSetFile, "w" ) as expSetFileHandler:
            expSetFileHandler.write( "1\tseq31\tchr1\t151\t250\n" )
        
        obsFile = "dummyObsFile"
        
        MapUtils.convertMapFileIntoSetFile( mapInputFile, obsFile )
        
        self.assertTrue( FileUtils.are2FilesIdentical( expSetFile, obsFile ) )
        
        for f in [ expSetFile, mapInputFile, obsFile ]:
            os.remove( f )

    def test_convertMapFileIntoSetFile_empty_file(self):
        mapInputFile = "dummyFile.map"
        mapFileHandler = open( mapInputFile, "w" )
        mapFileHandler.close()
        
        expFile = "dummyExpFile.map.set"
        expFileHandler = open( expFile, "w" )
        expFileHandler.close()
        
        obsFile = "dummyFile.map.set"
        
        MapUtils.convertMapFileIntoSetFile( mapInputFile )
        
        self.assertTrue( FileUtils.are2FilesIdentical( expFile, obsFile ) )
        
        for f in [ expFile, mapInputFile, obsFile ]:
            os.remove( f )
            
    def test_writeListInFile_empty_list(self):
        lMaps = [ ]
        expFileName = "expFileName"
        fileHandle = open(expFileName, "w")
        fileHandle.close()
 
        obsFileName = "obsFileName"
        with open(obsFileName, "w") as fileHandle:
            MapUtils.writeListInFile(lMaps, obsFileName, "w")
         
        self.assertTrue(FileUtils.are2FilesIdentical(expFileName, obsFileName))
        
        os.remove(obsFileName)
        os.remove(expFileName)
        
    def test_writeListInFile_list_one_set(self):
        lMaps = [ Map( "map1", "map1seq", 1, 10 ) ]
        line =  "map1\tmap1seq\t1\t10\n"
       
        expFileName = "expFileName"
 
        with open(expFileName, "w") as fileHandle:
            fileHandle.write(line)
 
        obsFileName = "obsFileName"
        with open(obsFileName, "w") as fileHandle:
            MapUtils.writeListInFile(lMaps, obsFileName, "w")

        self.assertTrue(FileUtils.are2FilesIdentical( expFileName, obsFileName))
        
        os.remove(obsFileName)
        os.remove(expFileName)

    # def test_getMinLengthOfMapFile(self):
    #     mapFileName = "{}/Gnome_tools/Vein_v4_scaffold_00001.fa.Nstretch.map" .format(os.environ["REPET_DATA"])
    #     expMinLengthofMapFile = 20
    #     obsMinLengthofMapFile = MapUtils.getMinLengthOfMapFile(mapFileName)
    #     self.assertEqual(expMinLengthofMapFile, obsMinLengthofMapFile)
       
    # def test_getMaxLengthOfMapFile(self):
    #     mapFileName = "{}/Gnome_tools/Vein_v4_scaffold_00001.fa.Nstretch.map" .format(os.environ["REPET_DATA"])
    #     expMinLengthofMapFile = 6344
    #     obsMinLengthofMapFile = MapUtils.getMaxLengthOfMapFile(mapFileName)
    #     self.assertEqual(expMinLengthofMapFile, obsMinLengthofMapFile)


if __name__ == "__main__":
    unittest.main()