#!/usr/bin/env python

# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.


import unittest
import sys
import Test_Align
import Test_AlignUtils
import Test_Map
import Test_MapUtils
import Test_Match
import Test_MatchUtils
import Test_Path
import Test_PathUtils
import Test_Range
import Test_Set
import Test_SetUtils


def main():
    
    TestSuite_coord = unittest.TestSuite() 
    
    TestSuite_coord.addTest( unittest.makeSuite( Test_Align.Test_Align, "test" ) )
    TestSuite_coord.addTest( unittest.makeSuite( Test_AlignUtils.Test_AlignUtils, "test" ) )
    TestSuite_coord.addTest( unittest.makeSuite( Test_Map.Test_Map, "test" ) )
    TestSuite_coord.addTest( unittest.makeSuite( Test_MapUtils.Test_MapUtils, "test" ) )
    TestSuite_coord.addTest( unittest.makeSuite( Test_Match.Test_Match, "test" ) )
    TestSuite_coord.addTest( unittest.makeSuite( Test_MatchUtils.Test_MatchUtils, "test" ) )
    TestSuite_coord.addTest( unittest.makeSuite( Test_Path.Test_Path, "test" ) )
    TestSuite_coord.addTest( unittest.makeSuite( Test_PathUtils.Test_PathUtils, "test" ) )
    TestSuite_coord.addTest( unittest.makeSuite( Test_Range.Test_Range, "test" ) )
    TestSuite_coord.addTest( unittest.makeSuite( Test_Set.Test_Set, "test" ) )
    TestSuite_coord.addTest( unittest.makeSuite( Test_SetUtils.Test_SetUtils, "test" ) )
    
    runner = unittest.TextTestRunner( sys.stderr, 2, 2 )
    runner.run( TestSuite_coord )
    
    
if __name__ == "__main__":
    main()
