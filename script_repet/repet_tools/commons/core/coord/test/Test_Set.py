# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.


import unittest
import os
from commons.core.coord.Set import Set
from commons.core.coord.Map import Map
import subprocess

class Test_Set( unittest.TestCase ):
    
    def test__eq__sets_equals(self):
        set1 = Set( 1, "set1", "seq1", 1, 2 )
        set2 = Set( 1, "set1", "seq1", 1 ,2 )   
        self.assertEqual( set1, set2 )
        
    def test__eq__sets_not_equals_ids(self):
        set1 = Set( 1, "set1", "seq1", 1, 2 )
        set2 = Set( 2, "set1", "seq1", 1 ,2 )   
        self.assertNotEqual( set1, set2 )
        
    def test__eq__sets_not_equals_name(self):
        set1 = Set( 1, "set1", "seq1", 1, 2 )
        set2 = Set( 1, "set2", "seq1", 1 ,2 )   
        self.assertNotEqual( set1, set2 )
    
    def test__eq__on_empty_set(self):
        set1 = Set()
        set2 = Set()
        self.assertEqual( set1, set2 )
        
    def test_setFromTuple_equals_instances(self):
        tuple = ( 1, "set1", "seq1", 1, 2 )
        obsSet = Set()
        obsSet.setFromTuple(tuple)
        expSet = Set( 1, "set1", "seq1", 1, 2 )
        self.assertEqual( expSet, obsSet )
   
    def test_setFromTuple_not_equals_instances(self):
        tuple = ( 1, "set1", "seq1", 1, 2 )
        obsSet = Set()
        obsSet.setFromTuple(tuple)
        expSet = Set( 2, "set1", "seq1", 1, 2 )
        self.assertNotEqual( expSet, obsSet )
        
    def test_read_empty_line_file(self):
        fileName = "dummyFile"
        
        subprocess.call(" touch {}".format(fileName), shell=True)
        
        with open(fileName, "r") as fileHandler:
            obsSet = Set()

            obsRes = obsSet.read( fileHandler )
            expRes = 0

        os.remove(fileName)
        
        self.assertEqual( expRes, obsRes )

    def test_read_one_line_file(self):
        line = ( "1\tset1\tseq1\t1\t2" )
        fileName = "dummyFile"
        
        with open( fileName, "w" ) as fileHandler:
            fileHandler.write( line )

        with open(fileName, "r") as fileHandler:
            tuple = line.split("\t")
            expSet = Set()
            expSet.setFromTuple(tuple)

            obsSet = Set()

            expRes = 1
            obsRes = obsSet.read(fileHandler)

        os.remove(fileName)
        self.assertEqual( expRes, obsRes )
        self.assertEqual( expSet, obsSet ) 
        
    def test_read_two_line_file(self):
        line1 = ( "1\tset1\tseq1\t1\t2\n" )
        line2 = ( "2\tset2\tseq2\t3\t4" )
        fileName = "dummyFile"
        
        with open( fileName, "w" ) as fileHandler:
            fileHandler.write( line1 )
            fileHandler.write( line2 )
        
        tuple1 = line1.split("\t")
        tuple2 = line2.split("\t")
        
        expSet1 = Set()
        expSet2 = Set()
        
        expSet1.setFromTuple(tuple1)
        expSet2.setFromTuple(tuple2)
        
        expSetList = [ expSet1, expSet2 ]
        expResList = [ 1, 1 ]
        
        with open( fileName, "r" ) as fileHandler:
            obsSet1 = Set()
            obsSet2 = Set()

            obsRes1 = obsSet1.read(fileHandler)
            obsRes2 = obsSet2.read(fileHandler)

            obsSetList= [ obsSet1, obsSet2 ]
            obsResList = [ obsRes1, obsRes2 ]

        os.remove(fileName)
        
        self.assertEqual( expSetList, obsSetList )
        self.assertEqual( expResList, obsResList )
        
    def test_merge_first_id_greater_than_second_id(self):
        firstSet = Set( 2, "set1", "seq1", 10, 40 )
        secondSet = Set( 1, "set2", "seq1", 20, 60 )
        
        firstSet.merge( secondSet )
        
        expSet = Set( 1, "set1", "seq1", 10, 60)
        obsSet = firstSet
        
        self.assertEqual( expSet, obsSet)
        
    def test_merge_first_id_smaller_than_second_id(self):
        firstSet = Set( 1, "set1", "seq1", 10, 40 )
        secondSet = Set( 2, "set2", "seq1", 20, 60 )
        
        firstSet.merge( secondSet )
        
        expSet = Set( 1, "set1", "seq1", 10, 60)
        obsSet = firstSet
        
        self.assertEqual( expSet, obsSet)

    def test_merge_first_id_equals_second_id(self):
        firstSet = Set( 1, "set1", "seq1", 10, 40 )
        secondSet = Set( 1, "set2", "seq1", 20, 60 )
        
        firstSet.merge( secondSet )
        
        expSet = Set( 1, "set1", "seq1", 10, 60)
        obsSet = firstSet
        
        self.assertEqual( expSet, obsSet)
    
    def test_merge_different_seqnames(self):
        firstSet = Set( 2, "set1", "seq1", 10, 40 )
        secondSet = Set( 1, "set1", "seq2", 20, 60 )
        expSet = Set( 2, "set1", "seq1", 10, 40 )
        firstSet.merge( secondSet )
        obsSet = firstSet
        self.assertEqual( expSet, obsSet )
        
    def test_diff_on_empty_sets(self):
        firstSet = Set()
        firstSet.seqname = "seq1"
        secondSet = Set()
        secondSet.seqname = "seq2"
        
        obsSet = firstSet.diff( secondSet )
        expSet = Set()
        
        self.assertEqual( expSet, obsSet )
    
    def test_diff(self):
        firstSet = Set( 2, "set1", "seq1", 10, 80 )
        secondSet = Set( 1, "set2", "seq1", 20, 60 )
        
        expSet1 = Set( 2, "set1", "seq1", 10, 19 )
        expSet2 = Set( 2, "set1", "seq1", 61, 80 )        
        
        obsSet2 = firstSet.diff( secondSet )
        obsSet1 = firstSet
                
        self.assertEqual( expSet1, obsSet1 ) 
        self.assertEqual( expSet2, obsSet2 )
        
    def test_diff_reverse(self):
        firstSet = Set( 2, "set1", "seq1", 20, 60 )
        secondSet = Set( 1, "set2", "seq1", 10, 80 )
        
        expSet1 = Set( 2, "set1", "seq1", 0, 0 )
        expSet2 = Set( )        
        
        obsSet2 = firstSet.diff( secondSet )
        obsSet1 = firstSet
                
        self.assertEqual( expSet1, obsSet1 ) 
        self.assertEqual( expSet2, obsSet2 )
        
    def test_diff_list1_overlap_end_list2(self):
        firstSet = Set( 2, "set1", "seq1", 20, 100 )
        secondSet = Set( 1, "set2", "seq1", 10, 80 )
        
        expSet1 = Set( 2, "set1", "seq1", 81, 100 )  
        expSet2 = Set( )             
        
        obsSet2 = firstSet.diff( secondSet )
        obsSet1 = firstSet
                
        self.assertEqual( expSet1, obsSet1 ) 
        self.assertEqual( expSet2, obsSet2 )
        
    def test_diff_with_empty_set1(self):
        set2 = Set( 1, "set1", "seq1", 2, 45 )
        set1 = Set( )
        
        expSet1 = Set( )
        expSet2 = Set( )
        
        obsSet2 = set1.diff( set2 )
        obsSet1 = set1
        
        self.assertEqual( expSet1, obsSet1 ) 
        self.assertEqual( expSet2, obsSet2 )
        
    def test_diff_list2_overlap_end_list1(self):
        firstSet = Set( 2, "set1", "seq1", 10, 70 )
        secondSet = Set( 1, "set2", "seq1", 40, 100 )
        
        expSet1 = Set( 2, "set1", "seq1", 10, 39 )
        expSet2 = Set( )        
        
        obsSet2 = firstSet.diff( secondSet )
        obsSet1 = firstSet
                
        self.assertEqual( expSet1, obsSet1 ) 
        self.assertEqual( expSet2, obsSet2 )
        
    def test_set2map(self):
        set = Set( 1, "set", "seq", 1, 2 )
        
        expMap = Map( "set::1", "seq", 1, 2 )
        obsMap = set.set2map()
        
        self.assertEqual( expMap, obsMap )
    

if __name__ == "__main__":
    test_suite = unittest.TestSuite()
    test_suite.addTest(unittest.makeSuite(Test_Set))
    unittest.TextTestRunner(verbosity=2).run( test_suite )
