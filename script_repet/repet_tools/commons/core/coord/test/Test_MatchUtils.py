# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.


import unittest
import os
from commons.core.utils.FileUtils import FileUtils
from commons.core.coord.MatchUtils import MatchUtils
from commons.core.coord.Match import Match
from commons.core.seq.BioseqDB import BioseqDB


class Test_MatchUtils( unittest.TestCase ):
    
    def test_getMatchListFromFile(self):
        inFile = "dummyInFile"
        inFileHandler = open(inFile, "w")
        inFileHandler.write("query.name\tquery.start\tquery.end\tquery.length\tquery.length.%\tmatch.length.%\tsubject.name\tsubject.start\tsubject.end\tsubject.length\tsubject.length.%\tE.value\tScore\tIdentity\tpath\n")
        m1 = Match()
        m1.setFromTuple(("QName", 1, 5, 5, 0.1, 0.2, "SName1", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1))
        m1.write(inFileHandler)
        m2 = Match()
        m2.setFromTuple(("QName", 1, 5, 5, 0.1, 0.2, "SName2", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1))
        m2.write(inFileHandler)
        inFileHandler.close()

        lExp = [ m1, m2 ]

        lObs = MatchUtils.getMatchListFromFile(inFile)

        self.assertEqual(lExp, lObs)

        os.remove(inFile)

    def test_getDictOfListsWithSubjectAsKey(self):
        m1 = Match()
        m1.setFromTuple(("QName", 1, 5, 5, 0.1, 0.2, "SName1", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1))
        m2 = Match()
        m2.setFromTuple(("QName", 1, 5, 5, 0.1, 0.2, "SName2", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1))
        lMatch = [ m1, m2 ]

        dExp = { "SName1": [ m1 ], "SName2": [ m2 ] }

        dObs = MatchUtils.getDictOfListsWithSubjectAsKey(lMatch)

        self.assertEqual(dExp, dObs)

    def test_getDictOfListsWithQueryAsKey(self):
        m1 = Match()
        m1.setFromTuple(("QName1", 1, 5, 5, 0.1, 0.2, "SName1", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1))
        m2 = Match()
        m2.setFromTuple(("QName2", 1, 5, 5, 0.1, 0.2, "SName2", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1))
        m3 = Match()
        m3.setFromTuple(("QName1", 1, 5, 5, 0.1, 0.2, "SName3", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1))
        lMatch = [ m1, m2, m3 ]

        dExp = { "QName1": [ m1, m3 ], "QName2": [ m2 ] }

        dObs = MatchUtils.getDictOfListsWithQueryAsKey(lMatch)

        self.assertEqual(dExp, dObs)

    def test_getIdListFromMatchList(self):
        m1 = Match()
        m1.setFromTuple(("QName", 1, 5, 5, 0.1, 0.2, "SName1", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1))
        m2 = Match()
        m2.setFromTuple(("QName", 1, 5, 5, 0.1, 0.2, "SName2", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 10))
        lMatch = [ m1, m2 ]

        lExp = [1, 10]

        lObs = MatchUtils.getIdListFromMatchList(lMatch)

        self.assertEqual(lExp, lObs)

    def test_getIdListFromMatchList_empty_list(self):
        lMatch = []
        lExp = []

        lObs = MatchUtils.getIdListFromMatchList(lMatch)

        self.assertEqual(lExp, lObs)

    def test_writeListInFile_without_header(self):
        m1 = Match()
        m1.setFromTuple(("QName", 1, 5, 5, 0.1, 0.2, "SName1", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1))
        m2 = Match()
        m2.setFromTuple(("QName", 1, 5, 5, 0.1, 0.2, "SName2", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 10))
        lMatch = [ m1, m2 ]

        line1 = "QName\t1\t5\t5\t0.1\t0.2\tSName1\t5\t25\t20\t0.15\t1e-20\t15.0\t87.2\t1\n"
        line2 = "QName\t1\t5\t5\t0.1\t0.2\tSName2\t5\t25\t20\t0.15\t1e-20\t15.0\t87.2\t10\n"

        expFileName = "expFileName.match"
        expFileHandle = open (expFileName, 'w')
        expFileHandle.write(line1)
        expFileHandle.write(line2)
        expFileHandle.close()

        obsFileName = "obsFileName.match"

        MatchUtils.writeListInFile(lMatch, obsFileName)

        self.assertTrue(FileUtils.are2FilesIdentical(expFileName, obsFileName))

        os.remove(obsFileName)
        os.remove(expFileName)

    def test_writeListInFile_with_header(self):
        m1 = Match()
        m1.setFromTuple(("QName", 1, 5, 5, 0.1, 0.2, "SName1", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1))
        m2 = Match()
        m2.setFromTuple(("QName", 1, 5, 5, 0.1, 0.2, "SName2", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 10))
        lMatch = [ m1, m2 ]

        headerLine = "query.name\tquery.start\tquery.end\tquery.length\tquery.length.%\tmatch.length.%\tsubject.name\tsubject.start\tsubject.end\tsubject.length\tsubject.length.%\tE.value\tScore\tIdentity\tpath\n"

        line1 = headerLine
        line2 = "QName\t1\t5\t5\t0.1\t0.2\tSName1\t5\t25\t20\t0.15\t1e-20\t15.0\t87.2\t1\n"
        line3 = "QName\t1\t5\t5\t0.1\t0.2\tSName2\t5\t25\t20\t0.15\t1e-20\t15.0\t87.2\t10\n"

        expFileName = "expFileName.match"
        expFileHandle = open(expFileName, 'w')
        expFileHandle.write(line1)
        expFileHandle.write(line2)
        expFileHandle.write(line3)
        expFileHandle.close()

        obsFileName = "obsFileName.match"

        MatchUtils.writeListInFile(lMatch, obsFileName, header = headerLine)

        self.assertTrue(FileUtils.are2FilesIdentical(expFileName, obsFileName))

        os.remove(obsFileName)
        os.remove(expFileName)

    def test_writeListInFile_with_append_mode(self):
        m1 = Match()
        m1.setFromTuple(("QName", 1, 5, 5, 0.1, 0.2, "SName1", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1))
        m2 = Match()
        m2.setFromTuple(("QName", 1, 5, 5, 0.1, 0.2, "SName2", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 10))
        lMatch = [ m1, m2 ]

        line1 = "QName\t1\t5\t5\t0.1\t0.2\tSName1\t5\t25\t20\t0.15\t1e-20\t15.0\t87.2\t1\n"
        line2 = "QName\t1\t5\t5\t0.1\t0.2\tSName2\t5\t25\t20\t0.15\t1e-20\t15.0\t87.2\t10\n"

        expFileName = "expFileName.match"
        expFileHandle = open (expFileName, 'w')
        expFileHandle.write(line1)
        expFileHandle.write(line1)
        expFileHandle.write(line2)
        expFileHandle.close()

        obsFileName = "obsFileName.match"
        obsFileHandle = open (obsFileName, 'w')
        obsFileHandle.write(line1)
        obsFileHandle.close()

        MatchUtils.writeListInFile(lMatch, obsFileName, 'a')

        self.assertTrue(FileUtils.are2FilesIdentical(expFileName, obsFileName))

        os.remove(obsFileName)
        os.remove(expFileName)

    def test_rmvDuplicateMatches(self):
        m1 = Match()
        m1.setFromTuple(("QName", 1, 5, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1))
        m2 = Match()
        m2.setFromTuple(("QName", 1, 5, 5, 0.1, 0.2, "SName2", 5, 25, 20, 0.15, 1e-20, 15, 86.2, 1))
        m3 = Match()
        m3.setFromTuple(("QName", 1, 5, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1))
        lMatch = [ m1, m3, m2 ]

        lExp = [m1, m2]
        lObs = MatchUtils.rmvDuplicateMatches(lMatch)

        self.assertEqual(lExp, lObs)

    def test_filterDiffQrySbj_same_seq(self):
        fastaFileName = "file.fa"
        self._writeFastaFile(fastaFileName)
        qryDB = BioseqDB(fastaFileName)
        tabFileName = "file.tab"
        self._writeMatchFile(tabFileName)

        expListToKeep = ["HELITRON2"]
        obsListToKeep = MatchUtils.filterDiffQrySbj(qryDB, tabFileName, 0.95, 0.98, 2)
        self.assertEqual(expListToKeep, obsListToKeep)
        os.remove(fastaFileName)
        os.remove(tabFileName)

    def test_filterDiffQrySbj_TE_included_in_67percent_in_other_TE(self):
        fastaFileName = "file.fa"
        self._writeFastaFile2(fastaFileName)
        qryDB = BioseqDB(fastaFileName)
        tabFileName = "file.tab"
        self._writeMatchFile2(tabFileName)
        expListToKeep = []
        obsListToKeep = MatchUtils.filterDiffQrySbj(qryDB, tabFileName, 0.95, 0.98, 2)
        self.assertEqual(expListToKeep, obsListToKeep)
        os.remove(fastaFileName)
        os.remove(tabFileName)

    def test_getNbDistinctSequencesInsideMatchesWithThresh_query(self):
        tabFileName = "file.tab"
        self._writeMatchFile3(tabFileName)
        lMatches = MatchUtils.getMatchListFromFile(tabFileName)
        expNbDistinctMatches = 1
        obsNbDistinctMatches = MatchUtils.getNbDistinctSequencesInsideMatchesWithThresh(lMatches, 0.95, 0.98, "query")
        self.assertEqual(expNbDistinctMatches, obsNbDistinctMatches)
        os.remove(tabFileName)

    def test_getNbDistinctSequencesInsideMatchesWithThresh_subject(self):
        tabFileName = "file.tab"
        self._writeMatchFile3(tabFileName)
        lMatches = MatchUtils.getMatchListFromFile(tabFileName)
        expNbDistinctMatches = 1
        obsNbDistinctMatches = MatchUtils.getNbDistinctSequencesInsideMatchesWithThresh(lMatches, 0.95, 0.98, "subject")
        self.assertEqual(expNbDistinctMatches, obsNbDistinctMatches)
        os.remove(tabFileName)

    def test_convertMatchFileToAlignFile(self):
        inputMatchFileName = "file.tab"
        expAlignFileName = "expected.align"
        obsAlignFileName = "file.align"

        self._writeExpAlignFile(expAlignFileName)
        self._writeMatchFile4(inputMatchFileName)
        MatchUtils.convertMatchFileToAlignFile(inputMatchFileName)

        self.assertTrue(FileUtils.are2FilesIdentical(expAlignFileName, obsAlignFileName))

        os.remove(inputMatchFileName)
        os.remove(expAlignFileName)
        os.remove(obsAlignFileName)

    def test_convertMatchFileToAlignFile_empty_file(self):
        inputMatchFileName = "file.tab"
        expAlignFileName = "expected.align"
        obsAlignFileName = "file.align"

        f = open(expAlignFileName, "w")
        f.close()
        f = open(inputMatchFileName, "w")
        f.close()
        MatchUtils.convertMatchFileToAlignFile(inputMatchFileName)

        self.assertTrue(FileUtils.are2FilesIdentical(expAlignFileName, obsAlignFileName))

        os.remove(inputMatchFileName)
        os.remove(expAlignFileName)
        os.remove(obsAlignFileName)

    def test_generateMatchFileWithNewPathId(self):
        inputMatchFileName = "file.tab"
        expMatchFileName = "expected.tab"
        obsMatchFileName = "obsFile.tab"

        self._writeMatchFile5(inputMatchFileName)
        self._writeExpMatchFile(expMatchFileName)
        MatchUtils.generateMatchFileWithNewPathId(inputMatchFileName, obsMatchFileName)

        self.assertTrue(FileUtils.are2FilesIdentical(expMatchFileName, obsMatchFileName))

        os.remove(inputMatchFileName)
        os.remove(expMatchFileName)
        os.remove(obsMatchFileName)

    def test_generateMatchFileWithNewPathId_empty_file(self):
        inputMatchFileName = "file.tab"
        expMatchFileName = "expected.tab"
        obsMatchFileName = "obsFile.tab"

        f = open(expMatchFileName, "w")
        f.write("query.name\tquery.start\tquery.end\tquery.length\tquery.length.%\tmatch.length.%\tsubject.name\tsubject.start\tsubject.end\tsubject.length\tsubject.length.%\tE.value\tScore\tIdentity\tpath\n")
        f.close()
        f = open(inputMatchFileName, "w")
        f.close()
        MatchUtils.generateMatchFileWithNewPathId(inputMatchFileName, obsMatchFileName)

        self.assertTrue(FileUtils.are2FilesIdentical(expMatchFileName, obsMatchFileName))

        os.remove(inputMatchFileName)
        os.remove(expMatchFileName)
        os.remove(obsMatchFileName)
         
    def test_convertMatchFileIntoABCFileWithCoverageComputeOnSmallestSeq(self):
        matchFileName = "dummy.tab"
        with open(matchFileName, "w") as f:
            f.write("query.name\tquery.start\tquery.end\tquery.length\tquery.length.%\tmatch.length.%\tsubject.name\tsubject.start\tsubject.end\tsubject.length\tsubject.length.%\tE.value\tScore\tIdentity\tpath\n")
            f.write("Idefix\t6816\t7411\t596\t0.080421\t0.940063\tseq50\t18\t616\t599\t0.944795\t0\t1033\t97.84\t2\n")# min(|7411-6816+1|,|616-18+1|)/min(7411,634)==>0.94
            f.write("Idefix\t812\t7411\t6600\t0.890568\t0.881881\tseq458\t879\t7468\t6590\t0.880545\t0\t12430\t98.77\t3\n")# min(|7411-812+1|,|7468-879+1|)/min(7411,7484)==>0.89
            f.write("FB\t1\t794\t794\t0.717902\t0.488615\tseq458\t1624\t830\t795\t0.489231\t0\t1033\t91.97\t783\n")# min(|794-1+1|,|830-1624+1|)/min(1106,1625)==>0.72
            f.write("FB\t1\t973\t973\t0.879747\t0.821097\tseq469\t1185\t209\t977\t0.824473\t0\t1394\t93.57\t786\n")# min(|973-1+1|,|209-1185+1|)/min(1106,1185)==>0.88
            f.write("412\t7054\t7567\t514\t0.0679265\t0.0659905\tseq21\t6\t525\t520\t0.0667608\t0\t932\t98.27\t414\n")# min(|7567-7054+1|,|525-6+1|)/min(7567,7789)==>0.07
            f.write("412\t1091\t7567\t6477\t0.855953\t0.862335\tseq31\t1006\t7505\t6500\t0.865397\t0\t12610\t99.51\t415\n")# min(|7567-1091+1|,|7505-1006+1|)/min(7567,7511)==>0.86
            f.write("412\t1091\t7567\t6477\t0.855953\t0.862335\t412\t1006\t7505\t6500\t0.865397\t0\t12610\t99.51\t415\n")# Qname=SName
            f.write("412\t1091\t7567\t6477\t0.855953\t0.862335\t412\t1006\t7505\t6500\t0.865397\t0\t12610\t99.51\t415\n")# Qname=SName
            f.write("Mersenne\t1\t794\t794\t0.717902\t0.488615\tseq458\t1624\t830\t795\t0.489231\t0\t1033\t91.97\t783\n")# min(|794-1+1|,|830-1624+1|)/min(1106,1625)==>0.72
            f.write("Mersenne\t900\t940\t41\t0.48\t0.488615\tIdefix\t160\t100\t61\t0.15\t0\t1033\t91.97\t783\n")# min(|940-900+1|,|100-160+1|)/min(41/0.48,61/0.15)==>0.48
            f.write("seq469\t1\t973\t973\t0.879747\t0.821097\tseq469\t1185\t209\t977\t0.824473\t0\t1394\t93.57\t786\n")# Qname=SName
        expFileName = "exp.abc"
        with open(expFileName, "w") as f:
            f.write("Idefix\tseq50\t0.94\n")
            f.write("Idefix\tseq458\t0.89\n")
            f.write("FB\tseq469\t0.88\n")
            f.write("412\tseq31\t0.86\n")
        expFileName_unused = "exp.abc.unused"
        with open(expFileName_unused, "w") as f:
            f.write("Mersenne\n")
            f.write("seq21\n")
        obsFileName = "obs.abc"
        obsFileName_unused = "obs.abc.unused"
        MatchUtils.convertMatchFileIntoABCFileWithCoverageComputeOnSmallestSeq(matchFileName, obsFileName, coverageThreshold = 0.85)

        self.assertTrue(FileUtils.are2FilesIdentical(expFileName, obsFileName))
        self.assertTrue(FileUtils.are2FilesIdentical(expFileName_unused, obsFileName_unused))

        os.remove(matchFileName)
        os.remove(expFileName)
        os.remove(obsFileName)
        os.remove(expFileName_unused)
        os.remove(obsFileName_unused)

    def test_convertMatchFileIntoABCFileWithCoverageComputeOnSmallestSeq_coverage_threshold_86(self):
        matchFileName = "dummy.tab"
        with open(matchFileName, "w") as f:
            f.write("query.name\tquery.start\tquery.end\tquery.length\tquery.length.%\tmatch.length.%\tsubject.name\tsubject.start\tsubject.end\tsubject.length\tsubject.length.%\tE.value\tScore\tIdentity\tpath\n")
            f.write("Idefix\t6816\t7411\t596\t0.080421\t0.940063\tseq50\t18\t616\t599\t0.944795\t0\t1033\t97.84\t2\n")  # min(|7411-6816+1|,|616-18+1|)/min(7411,634)==>0.94
            f.write("Idefix\t812\t7411\t6600\t0.890568\t0.881881\tseq185\t879\t7468\t6590\t0.880545\t0\t12430\t98.77\t3\n")  # min(|7411-812+1|,|7468-879+1|)/min(7411,7484)==>0.89
            f.write("FB\t1\t794\t794\t0.717902\t0.488615\tseq458\t1624\t830\t795\t0.489231\t0\t1033\t91.97\t783\n")  # min(|794-1+1|,|830-1624+1|)/min(1106,1625)==>0.72
            f.write("FB\t1\t973\t973\t0.879747\t0.821097\tseq469\t1185\t209\t977\t0.824473\t0\t1394\t93.57\t786\n")  # min(|973-1+1|,|209-1185+1|)/min(1106,1185)==>0.88
            f.write("412\t7054\t7567\t514\t0.0679265\t0.0659905\tseq21\t6\t525\t520\t0.0667608\t0\t932\t98.27\t414\n")  # min(|7567-7054+1|,|525-6+1|)/min(7567,7789)==>0.07
            f.write("412\t1091\t7567\t6477\t0.855953\t0.862335\tseq31\t1006\t7505\t6500\t0.865397\t0\t12610\t99.51\t415\n")  # min(|7567-1091+1|,|7505-1006+1|)/min(7567,7511)==>0.86
            f.write("412\t1091\t7567\t6477\t0.855953\t0.862335\t412\t1006\t7505\t6500\t0.865397\t0\t12610\t99.51\t415\n")# Qname=SName
            f.write("412\t1091\t7567\t6477\t0.855953\t0.862335\t412\t1006\t7505\t6500\t0.865397\t0\t12610\t99.51\t415\n")# Qname=SName
            f.write("Mersenne\t1\t794\t794\t0.717902\t0.488615\tseq458\t1624\t830\t795\t0.489231\t0\t1033\t91.97\t783\n")# min(|794-1+1|,|830-1624+1|)/min(1106,1625)==>0.72
        expFileName = "exp.abc"
        with open(expFileName, "w") as f:
            f.write("Idefix\tseq50\t0.94\n")
            f.write("Idefix\tseq185\t0.89\n")
            f.write("FB\tseq469\t0.88\n")
            f.write("412\tseq31\t0.86\n")
        expFileName_unused = "exp.abc.unused"
        with open(expFileName_unused, "w") as f:
            f.write("Mersenne\n")
            f.write("seq21\n")
            f.write("seq458\n")
        obsFileName = "obs.abc"
        obsFileName_unused = "obs.abc.unused"

        MatchUtils.convertMatchFileIntoABCFileWithCoverageComputeOnSmallestSeq(matchFileName, obsFileName, coverageThreshold = 0.86)

        self.assertTrue(FileUtils.are2FilesIdentical(expFileName, obsFileName))
        self.assertTrue(FileUtils.are2FilesIdentical(expFileName_unused, obsFileName_unused))

        os.remove(matchFileName)
        os.remove(expFileName)
        os.remove(obsFileName)
        os.remove(expFileName_unused)
        os.remove(obsFileName_unused)

    def test_convertMatchFileIntoABCFileWithCoverageComputeOnSmallestSeq_EmptyABCFile(self):
        matchFileName = "dummy.tab"
        with open(matchFileName, "w") as f:
            f.write("query.name\tquery.start\tquery.end\tquery.length\tquery.length.%\tmatch.length.%\tsubject.name\tsubject.start\tsubject.end\tsubject.length\tsubject.length.%\tE.value\tScore\tIdentity\tpath\n")
            f.write("Idefix\t6816\t7411\t596\t0.080421\t0.940063\tseq50\t18\t616\t599\t0.944795\t0\t1033\t97.84\t2\n")  # min(|7411-6816+1|,|616-18+1|)/min(7411,634)==>0.94
            f.write("Idefix\t812\t7411\t6600\t0.890568\t0.881881\tseq185\t879\t7468\t6590\t0.880545\t0\t12430\t98.77\t3\n")  # min(|7411-812+1|,|7468-879+1|)/min(7411,7484)==>0.89
            f.write("FB\t1\t794\t794\t0.717902\t0.488615\tseq458\t1624\t830\t795\t0.489231\t0\t1033\t91.97\t783\n")  # min(|794-1+1|,|830-1624+1|)/min(1106,1625)==>0.72
            f.write("FB\t1\t973\t973\t0.879747\t0.821097\tseq469\t1185\t209\t977\t0.824473\t0\t1394\t93.57\t786\n")  # min(|973-1+1|,|209-1185+1|)/min(1106,1185)==>0.88
            f.write("412\t7054\t7567\t514\t0.0679265\t0.0659905\tseq21\t6\t525\t520\t0.0667608\t0\t932\t98.27\t414\n")  # min(|7567-7054+1|,|525-6+1|)/min(7567,7789)==>0.07
            f.write("412\t1091\t7567\t6477\t0.855953\t0.862335\tseq31\t1006\t7505\t6500\t0.865397\t0\t12610\t99.51\t415\n")  # min(|7567-1091+1|,|7505-1006+1|)/min(7567,7511)==>0.86
            f.write("412\t1091\t7567\t6477\t0.855953\t0.862335\t412\t1006\t7505\t6500\t0.865397\t0\t12610\t99.51\t415\n")# Qname=SName
            f.write("412\t1091\t7567\t6477\t0.855953\t0.862335\t412\t1006\t7505\t6500\t0.865397\t0\t12610\t99.51\t415\n")# Qname=SName
            f.write("Mersenne\t1\t794\t794\t0.717902\t0.488615\tseq458\t1624\t830\t795\t0.489231\t0\t1033\t91.97\t783\n")# min(|794-1+1|,|830-1624+1|)/min(1106,1625)==>0.72
        expFileName = "exp.abc"
        f = open(expFileName, "w")
        f.close()
        expFileName_unused = "exp.abc.unused"
        with open(expFileName_unused, "w") as f:
            f.write("412\n")
            f.write("FB\n")
            f.write("Idefix\n")
            f.write("Mersenne\n")
            f.write("seq185\n")
            f.write("seq21\n")
            f.write("seq31\n")
            f.write("seq458\n")
            f.write("seq469\n")
            f.write("seq50\n")

        obsFileName = "obs.abc"
        obsFileName_unused = "obs.abc.unused"

        MatchUtils.convertMatchFileIntoABCFileWithCoverageComputeOnSmallestSeq(matchFileName, obsFileName, coverageThreshold = 0.941)

        self.assertTrue(FileUtils.are2FilesIdentical(expFileName, obsFileName))
        self.assertTrue(FileUtils.are2FilesIdentical(expFileName_unused, obsFileName_unused))

        os.remove(matchFileName)
        os.remove(expFileName)
        os.remove(obsFileName)
        os.remove(expFileName_unused)
        os.remove(obsFileName_unused)

    def test_convertMatchFileIntoABCFileOnQueryCoverage(self):
        matchFileName = "dummy.tab"
        with open(matchFileName, "w") as f:
            f.write("query.name\tquery.start\tquery.end\tquery.length\tquery.length.%\tmatch.length.%\tsubject.name\tsubject.start\tsubject.end\tsubject.length\tsubject.length.%\tE.value\tScore\tIdentity\tpath\n")
            f.write("chr3\t1\t100\t100\t0.98\t0.95\tchr5\t11\t110\t100\t0.95\t1e-52\t133\t87.200000\n")
            f.write("chr7\t1\t200\t200\t0.98\t0.95\tchr2\t11\t210\t200\t0.95\t1e-78\t235\t98.900000\n")
            f.write("chr5\t1\t100\t100\t0.95\t0.95\tchr3\t11\t110\t100\t0.98\t1e-52\t133\t87.200000\n")
            f.write("chr2\t1\t200\t200\t0.95\t0.95\tchr7\t11\t210\t200\t0.98\t1e-78\t235\t98.900000\n")
        expFileName = "exp.abc"
        with open(expFileName, "w") as f:
            f.write("chr3\tchr5\t0.98\n")
            f.write("chr7\tchr2\t0.98\n")
            f.write("chr5\tchr3\t0.95\n")
            f.write("chr2\tchr7\t0.95\n")
        obsFileName = "obs.abc"

        MatchUtils.convertMatchFileIntoABCFileOnQueryCoverage(matchFileName, obsFileName)

        self.assertTrue(FileUtils.are2FilesIdentical(expFileName, obsFileName))

        os.remove(matchFileName)
        os.remove(expFileName)
        os.remove(obsFileName)

    def test_convertMatchFileIntoABCFileOnQueryCoverage_coverage_threshold_85(self):
        matchFileName = "dummy.tab"
        with open(matchFileName, "w") as f:
            f.write("query.name\tquery.start\tquery.end\tquery.length\tquery.length.%\tmatch.length.%\tsubject.name\tsubject.start\tsubject.end\tsubject.length\tsubject.length.%\tE.value\tScore\tIdentity\tpath\n")
            f.write("chr3\t1\t100\t100\t0.98\t0.95\tchr5\t11\t110\t100\t0.95\t1e-52\t133\t87.200000\n")
            f.write("chr7\t1\t200\t200\t0.98\t0.95\tchr2\t11\t210\t200\t0.95\t1e-78\t235\t98.900000\n")
            f.write("chr5\t1\t100\t100\t0.85\t0.95\tchr3\t11\t110\t100\t0.98\t1e-52\t133\t87.200000\n")
            f.write("chr2\t1\t200\t200\t0.80\t0.95\tchr7\t11\t210\t200\t0.98\t1e-78\t235\t98.900000\n")
        expFileName = "exp.abc"
        with open(expFileName, "w") as f:
            f.write("chr3\tchr5\t0.98\n")
            f.write("chr7\tchr2\t0.98\n")
            f.write("chr5\tchr3\t0.85\n")
        obsFileName = "obs.abc"

        MatchUtils.convertMatchFileIntoABCFileOnQueryCoverage(matchFileName, obsFileName, coverage = 0.85)

        self.assertTrue(FileUtils.are2FilesIdentical(expFileName, obsFileName))

        os.remove(matchFileName)
        os.remove(expFileName)
        os.remove(obsFileName)

    def test_convertMatchFileIntoABCFileOnQueryCoverage_EmptyABCFile(self):
        matchFileName = "dummy.tab"
        with open(matchFileName, "w") as f:
            f.write("query.name\tquery.start\tquery.end\tquery.length\tquery.length.%\tmatch.length.%\tsubject.name\tsubject.start\tsubject.end\tsubject.length\tsubject.length.%\tE.value\tScore\tIdentity\tpath\n")
            f.write("chr3\t1\t100\t100\t0.48\t0.95\tchr5\t11\t110\t100\t0.95\t1e-52\t133\t87.200000\n")
            f.write("chr7\t1\t200\t200\t0.48\t0.95\tchr2\t11\t210\t200\t0.95\t1e-78\t235\t98.900000\n")
            f.write("chr5\t1\t100\t100\t0.15\t0.95\tchr3\t11\t110\t100\t0.98\t1e-52\t133\t87.200000\n")
            f.write("chr2\t1\t200\t200\t0.50\t0.95\tchr7\t11\t210\t200\t0.98\t1e-78\t235\t98.900000\n")
        expFileName = "exp.abc"
        obsFileName = "obs.abc"
        with open(expFileName, "w"):
            MatchUtils.convertMatchFileIntoABCFileOnQueryCoverage(matchFileName, obsFileName, coverage = 0.85)
        self.assertTrue(FileUtils.are2FilesIdentical(expFileName, obsFileName))

        os.remove(matchFileName)
        os.remove(expFileName)
        os.remove(obsFileName)

    def _writeFastaFile(self, fileName):
        with open(fileName, "w") as f:
            f.write(">HELITRON3\n")
            f.write("GGCCAGTCACAATGGGGGTTTCACTGGTGTGTCATGCACATTTAATAGGGGTAAGACTGA\n")
            f.write("ATAAAAAATGATTATTTGCATGAAATGGGGATGAGAGAGAAGGAAAGAGTTTCATCCTGG\n")
            f.write("GATTCGTTTCATTCACCGGATCTCTTGCGTCCGCCTCCGCCGTGCGACCTCCGCATTCTC\n")
            f.write(">HELITRON2\n")
            f.write("GGCCAGTCACAATGGGGGTTTCACTGGTGTGTCATGCACATTTAATAGGGGTAAGACTGA\n")
            f.write("ATAAAAAATGATTATTTGCATGAAATGGGGATGAGAGAGAAGGAAAGAGTTTCATCCTGG\n")
            f.write("GATTCGTTTCATTCACCGGATCTCTTGCGTCCGCCTCCGCCGTGCGACCTCCGCATTCTC\n")

    def _writeMatchFile(self, fileName):
        with open(fileName, "w") as f:
            f.write("query.name\tquery.start\tquery.end\tquery.length\tquery.length.%\tmatch.length.%\tsubject.name\tsubject.start\tsubject.end\tsubject.length\tsubject.length.%\tE.value\tScore\tIdentity\tpath\n")
            f.write("HELITRON3\t1\t180\t180\t1\t1\tHELITRON2\t1\t180\t180\t1\t2e-103\t357\t100\t1\n")

    def _writeFastaFile2(self, fileName):
        with open(fileName, "w") as f:
            f.write(">header2\n")
            f.write("TTTCACTGGTGTGTCATGCACATTTAATAGGGGTAAGACTGAATAAAAAATGATTATTTG\n")
            f.write("CATGAAATGGGGATGAGAGAGAAGGAAAGAGTTTCATCCTGGGATTCGTTTCATTCACCG\n")

    def _writeMatchFile2(self, fileName):
        with open(fileName, "w") as f:
            f.write("query.name\tquery.start\tquery.end\tquery.length\tquery.length.%\tmatch.length.%\tsubject.name\tsubject.start\tsubject.end\tsubject.length\tsubject.length.%\tE.value\tScore\tIdentity\tpath\n")
            f.write("header2\t1\t120\t120\t1\t0.674157\tBS31790\t19\t138\t120\t0.674157\t3e-68\t238\t100\t1\n")

    def _writeMatchFile3(self, fileName):
        with open(fileName, "w") as f:
            f.write("query.name\tquery.start\tquery.end\tquery.length\tquery.length.%\tmatch.length.%\tsubject.name\tsubject.start\tsubject.end\tsubject.length\tsubject.length.%\tE.value\tScore\tIdentity\tpath\n")
            f.write("header2\t1\t120\t120\t0.674157\t0.674157\tBS31790\t19\t138\t120\t0.674157\t3e-68\t238\t100\t1\n")
            f.write("header3\t1\t120\t120\t0.99\t0.994157\tBS31790\t19\t138\t120\t0.994157\t3e-68\t238\t100\t1\n")
            f.write("header4\t1\t120\t120\t1\t0.94157\tBS31790\t19\t138\t120\t0.674157\t3e-68\t238\t67\t1\n")

    def _writeMatchFile4(self, fileName):
        with open(fileName, "w") as f:
            f.write("query.name\tquery.start\tquery.end\tquery.length\tquery.length.%\tmatch.length.%\tsubject.name\tsubject.start\tsubject.end\tsubject.length\tsubject.length.%\tE.value\tScore\tIdentity\tpath\n")
            f.write("header2\t1\t120\t120\t0.674157\t0.674157\tBS31790\t19\t138\t120\t0.674157\t3e-68\t238\t100\t1\n")
            f.write("header3\t120\t220\t120\t0.99\t0.994157\tBS31790\t19\t138\t120\t0.994157\t3e-65\t238\t100\t1\n")
            f.write("header4\t1\t120\t120\t1\t0.94157\tBS31790\t19\t138\t120\t0.674157\t3e-67\t244\t90\t1\n")


    def _writeExpAlignFile(self, fileName):
        with open(fileName, "w") as f:
            f.write("header2\t1\t120\tBS31790\t19\t138\t3e-68\t238.0\t100.0\n")
            f.write("header3\t120\t220\tBS31790\t19\t138\t3e-65\t238.0\t100.0\n")
            f.write("header4\t1\t120\tBS31790\t19\t138\t3e-67\t244.0\t90.0\n")

    def _writeMatchFile5(self, fileName):
        with open(fileName, "w") as f:
            f.write("query.name\tquery.start\tquery.end\tquery.length\tquery.length.%\tmatch.length.%\tsubject.name\tsubject.start\tsubject.end\tsubject.length\tsubject.length.%\tE.value\tScore\tIdentity\tpath\n")
            f.write("header2\t1\t120\t120\t0.674157\t0.674157\tBS31790\t19\t138\t120\t0.674157\t3e-68\t238\t100\t1\n")
            f.write("header2\t124\t144\t120\t0.674157\t0.674157\tBS31790\t19\t138\t120\t0.674157\t3e-68\t238\t100\t1\n")
            f.write("header3\t120\t220\t120\t0.99\t0.994157\tBS31790\t19\t138\t120\t0.994157\t3e-65\t238\t100\t1\n")
            f.write("header4\t1\t120\t120\t1\t0.94157\tBS31790\t19\t138\t120\t0.674157\t3e-67\t244\t90\t1\n")


    def _writeExpMatchFile(self, fileName):
        with open(fileName, "w") as f:
            f.write("query.name\tquery.start\tquery.end\tquery.length\tquery.length.%\tmatch.length.%\tsubject.name\tsubject.start\tsubject.end\tsubject.length\tsubject.length.%\tE.value\tScore\tIdentity\tpath\n")
            f.write("header2\t1\t120\t120\t0.674157\t0.674157\tBS31790\t19\t138\t120\t0.674157\t3e-68\t238.0\t100.0\t1\n")
            f.write("header2\t124\t144\t120\t0.674157\t0.674157\tBS31790\t19\t138\t120\t0.674157\t3e-68\t238.0\t100.0\t1\n")
            f.write("header3\t120\t220\t120\t0.99\t0.994157\tBS31790\t19\t138\t120\t0.994157\t3e-65\t238.0\t100.0\t2\n")
            f.write("header4\t1\t120\t120\t1.0\t0.94157\tBS31790\t19\t138\t120\t0.674157\t3e-67\t244.0\t90.0\t3\n")


if __name__ == "__main__":
    test_suite = unittest.TestSuite()
    test_suite.addTest(unittest.makeSuite(Test_MatchUtils))
    unittest.TextTestRunner(verbosity=2).run(test_suite)
