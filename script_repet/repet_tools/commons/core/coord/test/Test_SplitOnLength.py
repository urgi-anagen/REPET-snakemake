import unittest
from commons.core.coord.Path import Path
from commons.core.coord.Range import Range
from commons.core.coord.MergedPath import MergedPath
from commons.core.coord.SplitOnLength import SplitOnLength

class Test_SplitOnLength(unittest.TestCase):

    def test_split_allObjectsUnderThreshold(self):
        iPath1 = Path()
        iPath1.setFromString("1\tquery\t1\t6\tsubject\t11\t16\t0\t10\t12.34\n")
        iPath2 = Path()
        iPath2.setFromString("2\tquery\t1\t15\tsubject\t21\t35\t0\t10\t12.34\n")

        iMP1 = MergedPath([iPath1])
        iMP2 = MergedPath([iPath2])

        lMP = [iMP1, iMP2]

        exp = []
        exp.append([iMP1, iMP2])
        exp.append([])
        iSOL = SplitOnLength(lMP, [1000])
        obs = iSOL.split()

        self.assertEqual(exp, obs)

    def test_split_threeMergedPaths_oneThreshold(self):
        iPath1 = Path()
        iPath1.setFromString("1\tquery\t1\t10\tsubject\t11\t16\t0\t10\t12.34\n")
        iPath2 = Path()
        iPath2.setFromString("2\tquery\t1\t11\tsubject\t21\t35\t0\t10\t12.34\n")
        iPath3 = Path()
        iPath3.setFromString("3\tquery\t1\t5\tsubject\t11\t15\t0\t10\t12.34\n")

        iMP1 = MergedPath([iPath1])
        iMP2 = MergedPath([iPath2])
        iMP3 = MergedPath([iPath3])

        lMP = [iMP1, iMP2, iMP3]

        exp = []
        exp.append([iMP1, iMP3])
        exp.append([iMP2])
        iSOL = SplitOnLength(lMP, [10])
        obs = iSOL.split()

        self.assertEqual(exp, obs)

    def test_split_threeMergedPaths_twoThresholds(self):
        iPath1 = Path()
        iPath1.setFromString("1\tquery\t1\t10\tsubject\t11\t16\t0\t10\t12.34\n")
        iPath2 = Path()
        iPath2.setFromString("2\tquery\t1\t20\tsubject\t21\t35\t0\t10\t12.34\n")
        iPath3 = Path()
        iPath3.setFromString("3\tquery\t1\t25\tsubject\t31\t55\t0\t10\t12.34\n")

        iMP1 = MergedPath([iPath1])
        iMP2 = MergedPath([iPath2])
        iMP3 = MergedPath([iPath3])

        lMP = [iMP1, iMP2, iMP3]

        exp = []
        exp.append([iMP1])
        exp.append([iMP2])
        exp.append([iMP3])
        iSOL = SplitOnLength(lMP, [10, 20])
        obs = iSOL.split()

        self.assertEqual(exp, obs)

    def test_split_twoRanges_oneThreshold(self):
        iRange1 = Range()
        iRange1.setFromString("chunk1\t1\t10")
        iRange2 = Range()
        iRange2.setFromString("chunk2\t1\t30")
        lRange = [iRange1, iRange2]

        exp = []
        exp.append([iRange1])
        exp.append([iRange2])
        iSOL = SplitOnLength(lRange, [20])
        obs = iSOL.split()

        self.assertEqual(exp, obs)

    def test_split_heterogeneousObjects(self):
        iRange1 = Range()
        iRange1.setFromString("chunk1\t1\t5")
        iRange2 = Range()
        iRange2.setFromString("chunk2\t1\t15")

        iPath1 = Path()
        iPath1.setFromString("1\tquery\t1\t10\tsubject\t11\t16\t0\t10\t12.34\n")
        iPath2 = Path()
        iPath2.setFromString("2\tquery\t1\t20\tsubject\t21\t35\t0\t10\t12.34\n")
        iPath3 = Path()
        iPath3.setFromString("3\tquery\t1\t25\tsubject\t31\t55\t0\t10\t12.34\n")

        iMP1 = MergedPath([iPath1])
        iMP2 = MergedPath([iPath2])
        iMP3 = MergedPath([iPath3])

        lObjects = [iRange2, iMP1, iMP2, iRange1, iMP3]

        exp = []
        exp.append([iMP1, iRange1])
        exp.append([iRange2, iMP2])
        exp.append([iMP3])
        iSOL = SplitOnLength(lObjects, [10, 20])
        obs = iSOL.split()

        self.assertEqual(exp, obs)

    def test_split_listOfEmptyObjects(self):
        iRange = Range()
        iMP = MergedPath()

        exp = []
        exp.append([Range(), MergedPath()])
        exp.append([])

        iSOL = SplitOnLength([iRange, iMP], [1], verbosity = 2)
        obs = iSOL.split()

        self.assertEqual(exp, obs)

    def test_split_emptyObjectsList(self):
        exp = []
        exp.append([])
        iSOL = SplitOnLength([], [10], verbosity = 2)
        obs = iSOL.split()

        self.assertEqual(exp, obs)

    def test_split_emptyThresholdsList(self):
        iRange1 = Range()
        iRange1.setFromString("chunk1\t1\t10")

        exp = []
        exp.append([iRange1])
        iSOL = SplitOnLength([iRange1], [], verbosity = 2)
        obs = iSOL.split()

        self.assertEqual(exp, obs)

    def test_split_moreThresholdsThanObjects(self):
        iRange1 = Range()
        iRange1.setFromString("chunk1\t1\t20")
        thresholds = [5, 10, 20, 25]

        exp = []
        exp.append([]) # <= 5
        exp.append([]) # ]5;10]
        exp.append([iRange1]) # ]10;20]
        exp.append([]) # ]20;25]
        exp.append([]) # > 25

        iSOL = SplitOnLength([iRange1], thresholds)
        obs = iSOL.split()

        self.assertEqual(exp, obs)

    def test_split_sameThreshold(self):
        iRange1 = Range()
        iRange1.setFromString("chunk1\t1\t10")
        iRange2 = Range()
        iRange2.setFromString("chunk2\t1\t30")
        thresholds = [20, 20]

        exp = []
        exp.append([iRange1])
        exp.append([])
        exp.append([iRange2])

        iSOL = SplitOnLength([iRange1, iRange2], thresholds)
        obs = iSOL.split()

        self.assertEqual(exp, obs)
        
    def test_split_unsortedThresholds(self):
        iRange1 = Range()
        iRange1.setFromString("chunk1\t1\t10")
        iRange2 = Range()
        iRange2.setFromString("chunk2\t1\t40")
        thresholds = [50, 20]
        
        exp = []
        exp.append([iRange1, iRange2])
        iSOL = SplitOnLength([iRange1, iRange2], thresholds, verbosity = 2)
        obs = iSOL.split()

        self.assertEqual(exp, obs)

    def test_split_noGetLength(self):
        objWithoutGetLength1 = "I don't implement getLength()"
        objWithoutGetLength2 = "Me neither"
        iRange = Range()
        iRange.setFromString("chunk1\t190000\t290000")
        lObjects = [objWithoutGetLength1, iRange, objWithoutGetLength2]

        exp = [lObjects]
        iSOL = SplitOnLength(lObjects, [5], verbosity = 2)
        obs = iSOL.split()

        self.assertEqual(exp, obs)

if __name__ == "__main__":
    unittest.main()