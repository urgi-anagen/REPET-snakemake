# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.


import unittest
from commons.core.coord.Range import Range
from commons.core.coord.Range import getBin, getIdx


class Test_Range( unittest.TestCase ):
    
    def setUp(self):
        self._range = Range()
        
    def test_setFromString(self):
        line = "chunk1\t190000\t390000"
        self._range.setFromString( line )
        self.assertEqual( self._range.seqname, "chunk1" )
        self.assertEqual( self._range.start, 190000 )
        self.assertEqual( self._range.end, 390000 )
        
        line = "chunk1\t190000\t390000\n"
        self._range.setFromString( line )
        self.assertEqual( self._range.seqname, "chunk1" )
        self.assertEqual( self._range.start, 190000 )
        self.assertEqual( self._range.end, 390000 )
        
        line = "chunk1;190000;390000"
        self._range.setFromString( line, ";" )
        self.assertEqual( self._range.seqname, "chunk1" )
        self.assertEqual( self._range.start, 190000 )
        self.assertEqual( self._range.end, 390000 )
        
    def test_setFromTuple(self):
        tuple = ("chunk1","190000","390000")
        self._range.setFromTuple( tuple)
        
        self.assertEqual( self._range.seqname, "chunk1" )
        self.assertEqual( self._range.start, 190000 )
        self.assertEqual( self._range.end, 390000 )
        
    def test___eq__(self):
        self._range.setFromString( "chunk1\t190000\t390000\n" )
        o = Range()
        o.setFromString( "chunk1\t190000\t390000\n" )
        self.assertEqual( self._range, o )
        
        o.setFromString( "chunk1\t190000\t39" )
        self.assertNotEqual( self._range, o )
        
        o.setFromString( "chromosome1\t190000\t390000" )
        self.assertNotEqual( self._range, o )
        
        o.setFromString( "chunk1\t390000\t190000" )
        self.assertNotEqual( self._range, o )
        
        o.setFromString( "chromosome1\t390000\t190000" )
        self.assertNotEqual( self._range, o )
        
    def test_getMin(self):
        self._range.setFromTuple( ("chunk1", 190000, 390000) )
        expMin = 190000
        obsMin = self._range.getMin() 
        self.assertTrue(expMin, obsMin)
        
    def test_getMax(self):
        self._range.setFromTuple( ("chunk1", 190000, 390000) )
        expMax = 390000
        obsMax = self._range.getMax() 
        self.assertTrue(expMax, obsMax)
        
    def test_isOnDirectStrand_true(self):
        self._range.setFromTuple( ("chunk1", 190000, 390000) )
        self.assertTrue(self._range.isOnDirectStrand())
        
    def test_isOnDirectStrand_false(self):
        self._range.setFromTuple( ("chunk1", 390000, 190000) )
        self.assertFalse(self._range.isOnDirectStrand())
        
    def test_isOnReverseStrand_true(self):
        self._range.setFromTuple( ("chunk1", 390000, 190000) )
        self.assertTrue(self._range.isOnReverseStrand())
        
    def test_isOnReverseStrand_false(self):
        self._range.setFromTuple( ("chunk1", 190000, 390000) )
        self.assertFalse(self._range.isOnReverseStrand())
        
    def test_getStrand_direct(self):
        self._range.setFromTuple( ("chunk1", 190000, 390000) )
        expStrand = '+'
        obsStrand = self._range.getStrand()
        self.assertEqual(expStrand, obsStrand)
        
    def test_getStrand_reverse(self):
        self._range.setFromTuple( ("chunk1", 390000, 190000) )
        expStrand = '-'
        obsStrand = self._range.getStrand()
        self.assertEqual(expStrand, obsStrand)
        
    def test_reverse(self):
        obsRange = Range("chunk1", 190000, 390000)
        expRange = Range("chunk1", 390000, 190000)
        obsRange.reverse()
        self.assertEqual(expRange, obsRange)
        
    def test_getLength(self):
        self._range.setFromTuple( ("chunk1", 190000, 390000) )
        expLength = 200001 
        obsLength = self._range.getLength()
        self.assertEqual(expLength, obsLength)
        
    def test_isEmpty_true( self ):
        exp = True
        obs = self._range.isEmpty()
        self.assertEqual( exp, obs )
        
    def test_isEmpty_false( self ):
        self._range.setFromTuple( ( "seq1", 1, 10 ) )
        exp = False
        obs = self._range.isEmpty()
        self.assertEqual( exp, obs )
        
    def test_merge_diff_sequences( self ):
        self._range.setFromTuple( ( "seq1", 6, 10 ) )
        range2 = Range( "seq2", 6, 12 )
        expReturn = None
        expRange = Range( "seq1", 6, 10 )
        obsReturn = self._range.merge( range2 )
        obsRange = self._range
        self.assertEqual( expReturn, obsReturn )
        self.assertEqual( expRange, obsRange )
        
    def test_merge_no_overlapping_ranges( self ):
        self._range.setFromTuple( ( "seq1", 6, 10 ) )
        range2 = Range( "seq1", 12, 20 )
        expReturn = None
        expRange = Range( "seq1", 6, 20 )
        obsReturn = self._range.merge( range2 )
        obsRange = self._range
        self.assertEqual( expReturn, obsReturn )
        self.assertEqual( expRange, obsRange )
        
    def test_merge_no_overlapping_ranges_range1_on_neg_strand( self ):
        self._range.setFromTuple( ( "seq1", 10, 6 ) )
        range2 = Range( "seq1", 12, 20 )
        expReturn = None
        expRange = Range( "seq1", 20, 6 )
        obsReturn = self._range.merge( range2 )
        obsRange = self._range
        self.assertEqual( expReturn, obsReturn )
        self.assertEqual( expRange, obsRange )
        
    def test_merge_overlapping_ranges_range1_and_range2_on_neg_strand( self ):
        self._range.setFromTuple( ( "seq1", 10, 6 ) )
        range2 = Range( "seq1", 20, 12 )
        expReturn = None
        expRange = Range( "seq1", 20, 6 )
        obsReturn = self._range.merge( range2 )
        obsRange = self._range
        self.assertEqual( expReturn, obsReturn )
        self.assertEqual( expRange, obsRange )
        
    def test_merge_on_overlapping_ranges_range1_and_range2( self ):
        self._range.setFromTuple( ( "seq1", 6, 10 ) )
        range2 = Range( "seq1", 8, 20 )
        expReturn = None
        expRange = Range( "seq1", 6, 20 )
        obsReturn = self._range.merge( range2 )
        obsRange = self._range
        self.assertEqual( expReturn, obsReturn )
        self.assertEqual( expRange, obsRange )
        
    def test_isOverlapping_diff_sequences( self ):
        self._range.setFromTuple( ( "seq1", 6, 10 ) )
        r2 = Range( "seq2", 6, 10 )
        exp = False
        obs = self._range.isOverlapping( r2 )
        self.assertEqual( exp, obs )
        
    def test_isOverlapping_no( self ):
        self._range.setFromTuple( ( "seq1", 6, 10 ) )
        r2 = Range( "seq1", 16, 20 )
        exp = False
        obs = self._range.isOverlapping( r2 )
        self.assertEqual( exp, obs )
        
    def test_isOverlapping_yes( self ):
        self._range.setFromTuple( ( "seq1", 6, 10 ) )
        r2 = Range( "seq1", 1, 15 )
        exp = True
        obs = self._range.isOverlapping( r2 )
        self.assertEqual( exp, obs )
        
    def test_isOverlapping_yes_range2_on_neg_strand( self ):
        self._range.setFromTuple( ( "seq1", 6, 10 ) )
        r2 = Range( "seq1", 15, 1 )
        exp = True
        obs = self._range.isOverlapping( r2 )
        self.assertEqual( exp, obs )
        
    def test_isOverlapping_range1_before_range2( self ):
        self._range.setFromTuple( ( "seq1", 6, 10 ) )
        r2 = Range( "seq1", 8, 15 )
        exp = True
        obs = self._range.isOverlapping( r2 )
        self.assertEqual( exp, obs )
        
    def test_isOverlapping_range1_after_range2( self ):
        self._range.setFromTuple( ( "seq1", 6, 10 ) )
        r2 = Range( "seq1", 1, 8 )
        exp = True
        obs = self._range.isOverlapping( r2 )
        self.assertEqual( exp, obs )
        
    def test_isOverlapping_range1_equal_range2( self ):
        self._range.setFromTuple( ( "seq1", 6, 10 ) )
        r2 = Range( "seq1", 6, 10 )
        exp = True
        obs = self._range.isOverlapping( r2 )
        self.assertEqual( exp, obs )
        
    def test_isOverlapping_yes_edges_left( self ):
        self._range.setFromTuple( ( "seq1", 6, 10 ) )
        r2 = Range( "seq1", 1, 6 )
        exp = True
        obs = self._range.isOverlapping( r2 )
        self.assertEqual( exp, obs )
        
    def test_isOverlapping_yes_edges_right( self ):
        self._range.setFromTuple( ( "seq1", 6, 10 ) )
        r2 = Range( "seq1", 10, 20 )
        exp = True
        obs = self._range.isOverlapping( r2 )
        self.assertEqual( exp, obs )
        
    def test_isOverlapping_yes_one_nt( self ):
        self._range.setFromTuple( ( "seq1", 10, 10 ) )
        r2 = Range( "seq1", 10, 20 )
        exp = True
        obs = self._range.isOverlapping( r2 )
        self.assertEqual( exp, obs )
        
    def test_isOverlapping_yes_span_1( self ):
        self._range.setFromTuple( ( "seq1", 10, 100 ) )
        r2 = Range( "seq1", 5, 110 )
        exp = True
        obs = self._range.isOverlapping( r2 )
        self.assertEqual( exp, obs )
        
    def test_isOverlapping_yes_span_2( self ):
        self._range.setFromTuple( ( "seq1", 5, 110 ) )
        r2 = Range( "seq1", 10, 100 )
        exp = True
        obs = self._range.isOverlapping( r2 )
        self.assertEqual( exp, obs )
        
    def test_isOverlapping_yes_include_1( self ):
        self._range.setFromTuple( ( "seq1", 10, 100 ) )
        r2 = Range( "seq1", 15, 90 )
        exp = True
        obs = self._range.isOverlapping( r2 )
        self.assertEqual( exp, obs )
        
    def test_isOverlapping_yes_include_2( self ):
        self._range.setFromTuple( ( "seq1", 15, 90 ) )
        r2 = Range( "seq1", 10, 100 )
        exp = True
        obs = self._range.isOverlapping( r2 )
        self.assertEqual( exp, obs )
        
    def test_getOverlapLength_on_different_range( self ):
        range1 = Range( "chunk1", 190000, 390000 )
        range2 = Range( "chunk2", 290000, 590000 )
        expSize = 0
        obsSize = range1.getOverlapLength( range2 )
        self.assertEqual( expSize, obsSize )
        
        
    def test_getOverlapLength_on_no_overalping_range( self ):
        range1 =Range( "chunk1", 190000, 390000 )
        range2 =Range( "chunk1", 490000, 590000 )
        expSize = 0
        obsSize = range1.getOverlapLength( range2 )
        self.assertEqual( expSize, obsSize )
        
        
    def test_getOverlapLength_range2_included_in_range1( self ):
        range1 = Range( "chunk1", 19, 39 )
        range2 = Range( "chunk1", 22, 25 )
        expSize = 4
        obsSize = range1.getOverlapLength( range2 )
        self.assertEqual( expSize, obsSize )
        obsSize = range2.getOverlapLength( range1 )
        self.assertEqual( expSize, obsSize )
        
        
    def test_getOverlapLength_range1_included_in_range2( self ):
        range1 = Range( "chunk1", 220000, 250000 )
        range2 = Range( "chunk1", 190000, 390000 )
        expSize = 30001
        obsSize = range1.getOverlapLength( range2 )
        self.assertEqual( expSize, obsSize )
        
        
    def test_getOverlapLength_range1_before_range2( self ):
        range1 = Range( "chunk1", 190000, 390000 )
        range2 = Range( "chunk1", 290000, 590000 )
        expSize = 100001
        obsSize = range1.getOverlapLength( range2 )
        self.assertEqual( expSize, obsSize )
        
        
    def test_getOverlapLength_range1_after_range2( self ):
        range1 = Range( "chunk1", 390000, 790000 )
        range2 = Range( "chunk1", 290000, 590000 )
        expSize = 200001
        obsSize = range1.getOverlapLength( range2 )
        self.assertEqual( expSize, obsSize )
        
        
    def test_isIncludedIn( self ):
        iRange1 = Range( "chunk1", 21, 30 )
        iRange2 = Range( "chunk1", 11, 40 )
        self.assertTrue( iRange1.isIncludedIn( iRange2 ) )
        
        
    def test_isIncludedIn_diffStrand( self ):
        iRange1 = Range( "chunk1", 21, 30 )
        iRange2 = Range( "chunk1", 40, 11 )
        self.assertTrue( iRange1.isIncludedIn( iRange2 ) )
        
        
    def test_isIncludedIn_onlyOverlap( self ):
        iRange1 = Range( "chunk1", 21, 50 )
        iRange2 = Range( "chunk1", 11, 40 )
        self.assertFalse( iRange1.isIncludedIn( iRange2 ) )
        
        
    def test_isIncludedIn_diffSeqname( self ):
        iRange1 = Range( "chunk1", 21, 30 )
        iRange2 = Range( "chunk2", 11, 40 )
        self.assertFalse( iRange1.isIncludedIn( iRange2 ) )
        
        
    def test_getDistance_on_overlapping_range(self):
        overlapTuple1 = ("chunk1", 220000, 250000)
        overlapRange1 = Range()
        overlapRange1.setFromTuple(overlapTuple1)
        
        overlapTuple2 = ("chunk1", 190000, 390000)
        overlapRange2 = Range()
        overlapRange2.setFromTuple(overlapTuple2)
        
        expDistance = 0
        obsDistance = overlapRange1.getDistance(overlapRange2)
        
        self.assertEqual(expDistance, obsDistance)
        
    def test_getDistance_on_range1_on_plus_strand_included_in_range2_on_neg_strand(self):
        tuple1 = ("chunk1", 220000, 250000)
        range1 = Range()
        range1.setFromTuple(tuple1)
        
        tuple2 = ("chunk1", 390000, 190000)
        range2 = Range()
        range1.setFromTuple(tuple2)
        
        expDistance = -1
        obsDistance = range1.getDistance(range2)
        
        self.assertEqual(expDistance, obsDistance)
        
    def test_getDistance_range1_after_range2(self):
        tuple1 = ("chunk1", 390000, 590000)
        range1 = Range()
        range1.setFromTuple(tuple1)
        
        tuple2 = ("chunk1", 190000, 290000)
        range2 = Range()
        range2.setFromTuple(tuple2)
        
        expDistance = 100000
        obsDistance = range1.getDistance(range2)
        
        self.assertEqual(expDistance, obsDistance)
        
    def test_getDistance_range1_before_range2(self):
        tuple1 = ("chunk1", 190000, 290000)
        range1 = Range()
        range1.setFromTuple(tuple1)
        
        tuple2 = ("chunk1", 390000, 590000)
        range2 = Range()
        range2.setFromTuple(tuple2)
        
        expDistance = 100000
        obsDistance = range1.getDistance(range2)
        
        self.assertEqual(expDistance, obsDistance)
        
    def test_getDistance_range1_after_range2_both_on_neg_strand(self):   
        tuple1 = ("chunk1", 590000, 390000)
        range1 = Range()
        range1.setFromTuple(tuple1)
        
        tuple2 = ("chunk1", 290000, 190000)
        range2 = Range()
        range2.setFromTuple(tuple2)
        
        expDistance = 100000
        obsDistance = range1.getDistance(range2)
        
        self.assertEqual(expDistance, obsDistance)
        
    def test_getDistance_range1_before_range2_both_on_neg_strand(self):   
        tuple1 = ("chunk1", 290000, 190000)
        range1 = Range()
        range1.setFromTuple(tuple1)
        
        tuple2 = ("chunk1", 590000, 390000)
        range2 = Range()
        range2.setFromTuple(tuple2)
        
        expDistance = 100000
        obsDistance = range1.getDistance(range2)
        
        self.assertEqual(expDistance, obsDistance)
        
    def test_diff_on_no_overlapping_range(self):
        range1 = Range("chunk1", 190000, 390000)
        range2 = Range("chunk1", 490000, 590000)
        
        expRange1 = Range("chunk1",190000,390000)
        expReturnedRange = Range("chunk1")
        
        obsReturnedRange = range1.diff(range2)
        obsRange1 = range1
        
        self.assertEqual(expRange1, obsRange1)
        self.assertEqual(expReturnedRange, obsReturnedRange)
        
    def test_diff_on_different_chunks(self):
        range1 = Range("chunk1", 190000, 390000)
        range2 = Range("chunk2", 290000, 590000)
        
        expRange1 = Range("chunk1", 190000, 390000)
        expReturnedRange = Range("chunk1")
        
        obsReturnedRange = range1.diff(range2)
        obsRange1 = range1
        
        self.assertEqual(expRange1, obsRange1)
        self.assertEqual(expReturnedRange, obsReturnedRange)
        
    def test_diff_range1_before_range2(self):
        range1 = Range("chunk1", 190000, 390000)
        range2 = Range("chunk1", 290000, 590000)
        
        expRange1 = Range("chunk1", 190000, 289999)
        expReturnedRange = Range("chunk1")
        
        obsReturnedRange = range1.diff(range2)
        obsRange1 = range1
        
        self.assertEqual(expRange1, obsRange1)
        self.assertEqual(expReturnedRange, obsReturnedRange)
        
    def test_diff_range1_before_range2_range1_on_neg_strand(self):
        range1 = Range("chunk1", 390000, 190000)
        range2 = Range("chunk1", 290000, 590000)
        
        expRange1 = Range("chunk1", 289999, 190000)
        expReturnedRange = Range("chunk1")
        
        obsReturnedRange = range1.diff(range2)
        obsRange1 = range1
        
        self.assertEqual(expRange1, obsRange1)
        self.assertEqual(expReturnedRange, obsReturnedRange)
        
    def test_diff_range2_included_in_range1(self):
        range1 = Range("chunk1", 190000, 590000)
        range2 = Range("chunk1", 290000, 390000)
        
        expRange1 = Range("chunk1", 190000, 289999)
        expReturnedRange = Range("chunk1", 390001, 590000)
        
        obsReturnedRange = range1.diff(range2)
        obsRange1 = range1
        
        self.assertEqual(expRange1, obsRange1)
        self.assertEqual(expReturnedRange, obsReturnedRange)
        
    def test_diff_range2_included_in_range1_range1_on_reverse_strand(self):
        range1 = Range("chunk1", 590000, 190000)
        range2 = Range("chunk1", 290000, 390000)
        
        expRange1 = Range("chunk1", 289999, 190000)
        expReturnedRange = Range("chunk1", 590000, 390001)
        
        obsReturnedRange = range1.diff(range2)
        obsRange1 = range1
        
        self.assertEqual(expRange1, obsRange1)
        self.assertEqual(expReturnedRange, obsReturnedRange)
        
    def test_diff_range1_included_in_range2(self):
        range1 = Range("chunk1", 390000, 490000)
        range2 = Range("chunk1", 290000, 590000)
        
        expRange1 = Range("chunk1",0,0)
        expReturnedRange = Range("chunk1")
        
        obsReturnedRange = range1.diff(range2)
        obsRange1 = range1
        
        self.assertEqual(expRange1, obsRange1)
        self.assertEqual(expReturnedRange, obsReturnedRange)
        
    def test_diff_range1_after_range2(self):
        range1 = Range("chunk1", 390000, 590000)
        range2 = Range("chunk1", 290000, 490000)
        
        expRange1 = Range("chunk1", 490001, 590000)
        expReturnedRange = Range("chunk1")
        
        obsReturnedRange = range1.diff(range2)
        obsRange1 = range1
        
        self.assertEqual(expRange1, obsRange1)
        self.assertEqual(expReturnedRange, obsReturnedRange)
        
    def test_diff_range1_after_range2_range1_on_neg_strand(self):
        range1 = Range("chunk1", 590000, 390000)
        range2 = Range("chunk1", 290000, 490000)
        
        expRange1 = Range("chunk1", 590000, 490001)
        expReturnedRange = Range("chunk1")
        
        obsReturnedRange = range1.diff(range2)
        obsRange1 = range1
        
        self.assertEqual(expRange1, obsRange1)
        self.assertEqual(expReturnedRange, obsReturnedRange)
        
    def test_getIdx(self):
        self.assertEqual(getIdx(1000,3),1000001)
        self.assertEqual(getIdx(999,3),1000000)
        self.assertEqual(getIdx(2000,3),1000002)
        self.assertEqual(getIdx(2000,4),2000000)
        self.assertEqual(getIdx(2000,5),3000000)
        self.assertEqual(getIdx(20000000,6),4000000)
        self.assertEqual(getIdx(20000000,5),3000200)
        self.assertEqual(getIdx(20000000,4),2002000)
        self.assertEqual(getIdx(20000000,3),1020000)
        
    def test_getBin_bin_level_9(self):
        tuple1 = ("chunk1", 190000000, 390000000)
        range1 = Range()
        range1.setFromTuple(tuple1)
        
        expRes = 100000000.0
        obsRes = range1.getBin()
        
        self.assertEqual(expRes, obsRes)
        
    def test_getBin_bin_level_8(self):
        tuple1 = ("chunk1", 19000000, 39000000)
        range1 = Range()
        range1.setFromTuple(tuple1)
        
        expRes = 100000000.0
        obsRes = range1.getBin()
        
        self.assertEqual(expRes, obsRes)
        
    def test_getBin_bin_level_7(self):
        tuple1 = ("chunk1", 1900000, 3900000)
        range1 = Range()
        range1.setFromTuple(tuple1)
        
        expRes = 10000000.0
        obsRes = range1.getBin()
        
        self.assertEqual(expRes, obsRes)
        
    def test_getBin_bin_level_6(self):
        tuple1 = ("chunk1", 190000, 390000)
        range1 =Range()
        range1.setFromTuple(tuple1)
        
        expRes = 1000000.0
        obsRes = range1.getBin()
        
        self.assertEqual(expRes, obsRes)
        
    def test_getBin_bin_level_5(self):
        tuple = ("chunk1", 19000, 39000)
        range = Range()
        range.setFromTuple(tuple)
        expRes = 100000.0
        obsRes = range.getBin()
        
        self.assertEqual(expRes, obsRes)
        
    def test_getBin_bin_level_4(self):
        tuple = ("chunk1", 1900, 3900)
        range = Range()
        range.setFromTuple(tuple)
        
        expRes = 10000.0
        obsRes = range.getBin()
        
        self.assertEqual(expRes, obsRes)
        
    def test_getBin_bin_level_3(self):
        tuple = ("chunk1", 190, 390)
        range = Range()
        range.setFromTuple(tuple)
        
        expRes = 1000.0
        obsRes = range.getBin()
        
        self.assertEqual(expRes, obsRes)
        
    def test_getBin_bin_level_2(self):
        tuple = ("chunk1", 19, 39)
        range = Range()
        range.setFromTuple(tuple)
        
        expRes = 1000.0
        obsRes = range.getBin()
        
        self.assertEqual(expRes, obsRes)
        
    def test_getBin_bin_level_1(self):
        tuple = ("chunk1", 1, 3)
        range = Range()
        range.setFromTuple(tuple)
        
        expRes = 1000.0
        obsRes = range.getBin()
        
        self.assertEqual(expRes, obsRes)
        
        
    def test_getBin_function(self):
        expBin = 2
        obsBin = getBin(200, 2)
        
        self.assertEqual(expBin, obsBin)
        
    def test_findIdx(self):
        o = Range()
        o.setFromString( "chunk1\t1000\t2000\n" )
        self.assertEqual(o.findIdx(),2000000)
        
        o.setFromString( "chunk1\t2000\t1000\n" )       
        self.assertEqual(o.findIdx(),2000000)
        
        o.setFromString( "chunk1\t200\t999\n" )       
        self.assertEqual(o.findIdx(),1000000)
        
        o.setFromString( "chunk1\t1\t20000000\n" )       
        self.assertEqual(o.findIdx(),4000000)
        
        

if __name__ == "__main__":
    test_suite = unittest.TestSuite()
    test_suite.addTest(unittest.makeSuite(Test_Range))
    unittest.TextTestRunner(verbosity=2).run( test_suite )
