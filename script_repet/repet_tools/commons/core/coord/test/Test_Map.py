# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.


import unittest
import os
import subprocess
from commons.core.coord.Map import Map
from commons.core.utils.FileUtils import FileUtils


class Test_Map( unittest.TestCase):
    
    def setUp(self):
        self._map = Map()
        
    def test_setFromString(self):
        line = "MbQ12Gr2Cl2\tconsensus1\t51\t1230\n"   # test with '\t' separator
        self._map.setFromString(line)
        self.assertEqual( self._map.name, "MbQ12Gr2Cl2" )
        self.assertEqual( self._map.seqname, "consensus1" )
        self.assertEqual( self._map.start, 51 )
        self.assertEqual( self._map.end, 1230 )
        line = "MbQ12Gr2Cl2;consensus1;51;1230"   # test with ';' separator
        self._map.setFromString(line,";")
        self.assertEqual( self._map.name, "MbQ12Gr2Cl2" )
        self.assertEqual( self._map.seqname, "consensus1" )
        self.assertEqual( self._map.start, 51 )
        self.assertEqual( self._map.end, 1230 )
    
    def test___eq__(self):
        self._map.setFromString( "MbQ12Gr2Cl2\tconsensus1\t51\t1230\n" )
        o = Map()
        o.setFromString( "MbQ12Gr2Cl2\tconsensus1\t51\t1230\n" )
        self.assertEqual( self._map, o )   # same data
        o.setFromString( "MbQ12Gr2Cl1\tconsensus1\t51\t1230\n" )
        self.assertNotEqual( self._map, o )   # different name
        o.setFromString( "MbQ12Gr2Cl2\tconsensus2\t51\t1230\n" )
        self.assertNotEqual( self._map, o )   # different seqname
        o.setFromString( "MbQ12Gr2Cl2\tconsensus1\t15\t1230\n" )
        self.assertNotEqual( self._map, o )   # different start
        o.setFromString( "MbQ12Gr2Cl2\tconsensus1\t51\t123000\n" )
        self.assertNotEqual( self._map, o )   # different end
        o.setFromString( "MbQ12Gr2Cl2\tconsensus1\t1230\t51\n" )
        self.assertNotEqual( self._map, o )   # same start/end but in different order
        
    def test_setFromTuple(self):
        tuple = ("MbQ12Gr2Cl2", "consensus1","51","1230")
        self._map.setFromTuple(tuple)

        expMap = Map("MbQ12Gr2Cl2", "consensus1",51,1230)
        obsMap = self._map
        
        self.assertEqual(expMap, obsMap)

    def test_reset(self):
        self._map.setFromTuple(("MbQ12Gr2Cl2", "consensus1", "51", "1230"))
        self._map.reset()
        self.assertEqual(Map(), self._map)
    
    def test_read_empty_file(self):
        
        fileName = "dummyFile"
        subprocess.call("touch {}".format(fileName), shell=True)
        fileHandle = open(fileName, "r")
        
        obsResult = self._map.read(fileHandle)
        expResult = 0
         
        fileHandle.close()
        os.remove(fileName) 
        
        self.assertEqual(expResult, obsResult)
    
    def test_read_uncompleted_line( self):
        uncompletedLine = "MbQ12Gr2Cl2\tconsensus1\t51"
        fileName = "dummyFile"

        fileHandle = open(fileName, "w")
        fileHandle.write(uncompletedLine)
        fileHandle.close()

        fileHandle = open(fileName, "r")
       
        obsResult = self._map.read(fileHandle)
        expResult = 0

        fileHandle.close()
        os.remove(fileName)

        self.assertEqual(obsResult, expResult)

    def test_read(self):
        line =  "MbQ12Gr2Cl2\tconsensus1\t51\t1230\n"
        fileName = "dummyFile"

        fileHandle = open(fileName, "w")
        fileHandle.write(line)
        fileHandle.close()

        fileHandle = open(fileName, "r")
        self._map.read(fileHandle)
        obsResult = self._map
        
        expResult = Map()
        expResult.setFromString(line) 

        fileHandle.close()
        os.remove(fileName)

        self.assertEqual(obsResult, expResult) 
     
    def test_write(self):
        line =  "MbQ12Gr2Cl2\tconsensus1\t51\t1230\n"
        expFileName = "expFileName"

        fileHandle = open(expFileName, "w")
        fileHandle.write(line)
        fileHandle.close()
        
        obsFileName = "obsFileName"
        fileHandle = open(obsFileName, "w")
        self._map.setFromString(line)
        self._map.write(fileHandle)
        fileHandle.close()
        
        self.assertTrue( FileUtils.are2FilesIdentical( expFileName, obsFileName ) )
        
        os.remove(obsFileName)
        os.remove(expFileName)
        
    def test_diff1(self):
        map1 = Map("seq1","DmelChr4", 190000, 390000)
        map2 = Map("seq2","DmelChr4", 290000, 590000)
        
        expMap1 = Map("seq1", "DmelChr4", 190000, 289999)
        expReturnedMap = Map()
        
        obsReturnedMap = map1.diff(map2)
        obsMap1 = map1
        
        self.assertEqual(expMap1, obsMap1)
        self.assertEqual(expReturnedMap, obsReturnedMap)
        
    def test_diff2(self):
        map1 = Map("seq1","DmelChr4", 190000, 590000)
        map2 = Map("seq2","DmelChr4", 290000, 390000)

        expMap1 = Map("seq1", "DmelChr4", 190000, 289999)
        expReturnedMap = Map("seq1", "DmelChr4", 390001, 590000)
        
        obsReturnedMap = map1.diff(map2)
        obsMap1 = map1
        
        self.assertEqual(expMap1, obsMap1)
        self.assertEqual(expReturnedMap, obsReturnedMap)
        
if __name__ == "__main__":
    unittest.main()