# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.


import unittest
import os
import time
from commons.core.coord.Set import Set
from commons.core.coord.Map import Map
from commons.core.coord.SetUtils import SetUtils
from commons.core.utils.FileUtils import FileUtils


class Test_SetUtils( unittest.TestCase ):
    
    def test_changeIdInList_on_empty_list(self):
        lSets = []
        SetUtils.changeIdInList( lSets , 1 )
        obsLSets = lSets
        expLSets = []
        self.assertEqual( expLSets , obsLSets )
        
    def test_changeIdInList_on_list_size_one(self):
        set1 = Set( 1, "set1", "seq1", 1, 2 )
        lSets = [ set1 ]
        SetUtils.changeIdInList( lSets , 9 )
        obsLSets = lSets
        set1 = Set( 9, "set1", "seq1", 1, 2 )
        expLSets = [ set1 ]
        self.assertEqual( expLSets , obsLSets )
        
    def test_changeIdInList(self):
        set1 = Set( 1, "set1", "seq1", 1, 2 )
        set2 = Set( 2, "set2", "seq2", 2, 3 )
        lSets = [ set1, set2 ]
        SetUtils.changeIdInList( lSets , 9 )
        obsLSets = lSets
        set1 = Set( 9, "set1", "seq1", 1, 2 )
        set2 = Set( 9, "set2", "seq2", 2, 3 )
        expLSets = [ set1, set2 ]
        
        self.assertEqual( expLSets , obsLSets )
        
    def test_getOverlapLengthBetweenLists_all_list_are_empty (self):
        lSets1 = []
        lSets2 = []
        
        expOverlapSize = 0
        obsOverlapSize = SetUtils.getOverlapLengthBetweenLists( lSets1, lSets2 )
        
        self.assertEqual( expOverlapSize, obsOverlapSize )
        
    def test_getOverlapLengthBetweenLists_list1_empty_list2_size_one (self):
        lSets1 = []
        lSets2 = [ Set( 9, "set1", "seq1", 1, 2 ) ]
        
        expOverlapSize = 0
        obsOverlapSize = SetUtils.getOverlapLengthBetweenLists( lSets1, lSets2 )
        
        self.assertEqual( expOverlapSize, obsOverlapSize )
        
    def test_getOverlapLengthBetweenLists_list1_empty_list2_size_two (self):
        lSets1 = []
        lSets2 = [ Set( 9, "set1", "seq1", 1, 2 ), Set( 9, "set2", "seq2", 2, 3 ) ]
        
        expOverlapSize = 0
        obsOverlapSize = SetUtils.getOverlapLengthBetweenLists( lSets1, lSets2 )
        
        self.assertEqual( expOverlapSize, obsOverlapSize )
        
    def test_getOverlapLengthBetweenLists_list1_size_one_list2_empty (self):
        lSets1 = [ Set( 9, "set1", "seq1", 1, 2 ) ]
        lSets2 = []
        
        expOverlapSize = 0
        obsOverlapSize = SetUtils.getOverlapLengthBetweenLists( lSets1, lSets2 )
        
        self.assertEqual( expOverlapSize, obsOverlapSize )
        
    def test_getOverlapLengthBetweenLists_list1_size_one_list2_size_one_without_overlapp (self):
        lSets1 = [ Set( 1, "set1", "seq1", 1, 2 ) ]
        lSets2 = [ Set( 2, "set2", "seq2", 2, 3 ) ]
        
        expOverlapSize = 0
        obsOverlapSize = SetUtils.getOverlapLengthBetweenLists( lSets1, lSets2 )
        
        self.assertEqual( expOverlapSize, obsOverlapSize )
        
    def test_getOverlapLengthBetweenLists_list1_size_one_list2_size_one_with_overlap_size_1 (self):
        lSets1 = [ Set( 1, "set1", "seq1", 1, 2 ) ]
        lSets2 = [ Set( 2, "set1", "seq1", 2, 3 ) ]
        
        expOverlapSize = 1
        obsOverlapSize = SetUtils.getOverlapLengthBetweenLists( lSets1, lSets2 )
        
        self.assertEqual( expOverlapSize, obsOverlapSize )
        
    def test_getOverlapLengthBetweenLists_list1_size_one_list2_size_one_with_overlap_size_2 (self):
        lSets1 = [ Set( 1, "set1", "seq1", 1, 2 ) ]
        lSets2 = [ Set( 2, "set1", "seq1", 1, 3 ) ]
        
        expOverlapSize = 2
        obsOverlapSize = SetUtils.getOverlapLengthBetweenLists( lSets1, lSets2 )
        
        self.assertEqual( expOverlapSize, obsOverlapSize )
        
    def test_getOverlapLengthBetweenLists_list1_size_one_list2_size_two_without_overlap_sets_in_list1_greater_than_sets_in_list2 (self):
        lSets1 = [ Set( 1, "set1", "seq1", 4, 6 ) ]
        lSets2 = [ Set( 2, "set2", "seq1", 2, 3 ), Set( 3, "set3", "seq3", 1, 2 ) ]
        
        expOverlapSize = 0
        obsOverlapSize = SetUtils.getOverlapLengthBetweenLists( lSets1, lSets2 )
        
        self.assertEqual( expOverlapSize, obsOverlapSize )
        
    def test_getOverlapLengthBetweenLists_list1_size_one_list2_size_two_with_overlap_sets_in_list1_greater_than_sets_in_list2 (self):
        lSets1 = [ Set( 1, "set1", "seq1", 4, 6 ) ]
        lSets2 = [ Set( 2, "set2", "seq2", 2, 3 ), Set( 3, "set3", "seq1", 4, 5 ) ]
        
        expOverlapSize = 2
        obsOverlapSize = SetUtils.getOverlapLengthBetweenLists( lSets1, lSets2 )
        
        self.assertEqual( expOverlapSize, obsOverlapSize )
        
    def test_getOverlapLengthBetweenLists_list1_size_one_list2_size_two_with_overlap_on_redundant_position (self):
        lSets1 = [ Set( 1, "set1", "seq1", 1, 2 ) ]
        lSets2 = [ Set( 2, "set2", "seq1", 2, 3 ), Set( 3, "set1", "seq1", 1, 2 ) ]
        
        expOverlapSize = 3
        obsOverlapSize = SetUtils.getOverlapLengthBetweenLists( lSets1, lSets2 )
        
        self.assertEqual( expOverlapSize, obsOverlapSize )
        
    def test_getOverlapLengthBetweenLists_list1_size_two_list2_empty (self):
        lSets1 = [ Set( 1, "set1", "seq1", 1, 2 ), Set( 2, "set2", "seq2", 2, 3 ) ]
        lSets2 = []
        
        expOverlapSize = 0
        obsOverlapSize = SetUtils.getOverlapLengthBetweenLists( lSets1, lSets2 )
        
        self.assertEqual( expOverlapSize, obsOverlapSize )
        
    def test_getOverlapLengthBetweenLists_on_different_seq_names (self):
        lSets1 = [ Set( 1, "set1", "seq1", 1, 2 ), Set( 2, "set2", "seq2", 2, 3 ) ]
        lSets2 = [ Set( 3, "set1", "seq2", 4, 5 ), Set( 5, "set2", "seq4", 2, 3 ) ]
        
        expOverlapSize = 0
        obsOverlapSize = SetUtils.getOverlapLengthBetweenLists( lSets1, lSets2 )
        
        self.assertEqual( expOverlapSize, obsOverlapSize )
        
    def test_getOverlapLengthBetweenLists_list1_size_two_list2_size_two_with_overlap (self):
        lSets1 = [ Set( 1, "set1", "seq1", 1, 6 ), Set( 2, "set2", "seq2", 2, 3 ) ]
        lSets2 = [ Set( 3, "set1", "seq1", 1, 2 ), Set( 2, "set2", "seq3", 2, 3 ) ]
        
        expOverlapSize = 2
        obsOverlapSize = SetUtils.getOverlapLengthBetweenLists( lSets1, lSets2 )
        
        self.assertEqual( expOverlapSize, obsOverlapSize )
        
    def test_areSetsOverlappingBetweenLists_list2_empty(self):
        tuple1 = ("1","chr1","seq1", "100","110")
        tuple2 = ("1","chr1","seq1", "200","220")
        tuple3 = ("1","chr1","seq1", "300","330")
        setList1 = self._makeSetListFromTupleList([tuple1, tuple2, tuple3])
        
        setList2 = []
        
        expRes = False
        obsRes = SetUtils.areSetsOverlappingBetweenLists( setList1, setList2 )
        
        self.assertEqual( expRes, obsRes )
        
    def test_areSetsOverlappingBetweenLists_list2_size1(self):
        tuple1 = ("1","chr1","seq1", "9","11")
        tuple2 = ("1","chr1","seq1", "20","22")
        tuple3 = ("1","chr1","seq1", "30","33")
        setList1 = self._makeSetListFromTupleList( [ tuple1, tuple2, tuple3 ] )
        
        tuple11 = ("1","chr1", "seq1", "8","11")
        setList2 = self._makeSetListFromTupleList( [ tuple11 ] )
        
        expRes = True
        obsRes = SetUtils.areSetsOverlappingBetweenLists( setList1, setList2 )
        
        self.assertEqual( expRes, obsRes )
        
    def test_areSetsOverlappingBetweenLists_list1_greater_list2(self):
        tuple1 = ("1","chr1","seq1","100","110")
        tuple2 = ("1","chr1","seq1","200","220")
        tuple3 = ("1","chr1","seq1","300","330")
        setList1 = self._makeSetListFromTupleList( [ tuple1, tuple2, tuple3 ] )
        
        tuple11 = ("1","chr1","seq1", "10","11")
        tuple22 = ("1","chr1","seq1", "20","22")
        tuple33 = ("1","chr1","seq1", "30","33")
        setList2 = self._makeSetListFromTupleList( [ tuple11, tuple22, tuple33 ] )
        
        expRes = False
        obsRes = SetUtils.areSetsOverlappingBetweenLists( setList1, setList2 )
        
        self.assertEqual( expRes, obsRes )
    
    def test_areSetsOverlappingBetweenLists_unordered_first_item_of_list1_greater_second_item_smaller(self):
        tuple1 = ("1","chr1","seq1","400","440")
        tuple2 = ("1","chr1","seq1","1","11")
        setList1 = self._makeSetListFromTupleList( [ tuple1, tuple2 ] )
        
        tuple11 = ("1","chr1","seq1","15","17")
        tuple22 = ("1","chr1","seq1", "20","22")
        tuple33 = ("1","chr1","seq1","30","33")
        setList2 = self._makeSetListFromTupleList( [ tuple11, tuple22, tuple33 ] )
        
        expRes = False
        obsRes = SetUtils.areSetsOverlappingBetweenLists( setList1, setList2 )
        
        self.assertEqual( expRes, obsRes )
 
    def test_areSetsOverlappingBetweenLists_unorderd_second_item_of_list1_overlap_first_item(self):
        tuple1 = ("1","chr1","seq1","400","440")
        tuple2 = ("1","chr1","seq1", "1","18")
        setList1 = self._makeSetListFromTupleList( [ tuple1, tuple2 ] )
        
        tuple11 = ("1","chr1","seq1","15","17")
        tuple22 = ("1","chr1","seq1","20","22")
        tuple33 = ("1","chr1","seq1","30","33")
        setList2 = self._makeSetListFromTupleList( [ tuple11, tuple22, tuple33 ] )
        
        expRes = True
        obsRes = SetUtils.areSetsOverlappingBetweenLists( setList1, setList2 )
        
        self.assertEqual( expRes, obsRes )
    
    def test_areSetsOverlappingBetweenLists_last_item_list1_overlap_last_item_list2(self):
        tuple1 = ("1","chr1","seq1","400","440")
        tuple2 = ("1","chr1","seq1","320","340")
        pathList1 = self._makeSetListFromTupleList( [ tuple1, tuple2 ] )
        
        tuple11 = ("1","chr1","seq1","100","110")
        tuple22 = ("1","chr1","seq1","200","220")
        tuple33 = ("1","chr1","seq1","300","330")
        pathList2 = self._makeSetListFromTupleList( [ tuple11, tuple22, tuple33 ] )
        
        expRes = True
        obsRes = SetUtils.areSetsOverlappingBetweenLists( pathList1, pathList2 )
        
        self.assertEqual( expRes, obsRes )
                
    def test_getCumulLength_empty_list(self):
        lSets = []
        
        expSize = 0
        obsSize = SetUtils.getCumulLength( lSets )
        
        self.assertEqual( expSize, obsSize )
        
    def test_getCumulLength_1_item(self):
        lSets = [ Set( 1, "set1", "seq1", 1, 6 ) ]
        
        expSize = 6
        obsSize = SetUtils.getCumulLength( lSets )
        
        self.assertEqual( expSize, obsSize )
        
    def test_getCumulLength_2_items(self):
        lSets = [ Set( 1, "set1", "seq1", 1, 6 ), Set( 1, "set1", "seq1", 1, 2 ) ]
        
        expSize = 8
        obsSize = SetUtils.getCumulLength( lSets )
        
        self.assertEqual( expSize, obsSize )
        
    def test_getCumulLength_complex(self):
        set8 = Set(498, "(TACATA)25", "Mela_Blaster_Grouper_611_MAP_20", 17, 348)
        set5 = Set(502, "(GGAGA)2", "Mela_Blaster_Grouper_611_MAP_20", 356, 366)
        set6 = Set(503, "(TTTAAAGAACCC)2", "Mela_Blaster_Grouper_611_MAP_20", 433, 457)
        set7 = Set(504, "(TATAA)2", "Mela_Blaster_Grouper_611_MAP_20", 484, 495) 
        lSets = [ set8, set5, set6, set7 ] 
        expSize = 380
        obsSize = SetUtils.getCumulLength( lSets )
        
        self.assertEqual( expSize, obsSize )
        
    def test_getListBoundaries_one_set(self):
        lSets = [ Set( 1, "set1", "seq1", 3, 8 ) ]
        
        expTuple = ( 3, 8 )
        obsTuple = SetUtils.getListBoundaries( lSets )
        
        self.assertEqual( expTuple, obsTuple )
        
    def test_getListBoundaries_two_sets(self):
        lSets = [ Set( 1, "set1", "seq1", 3, 8 ), Set( 2, "set2", "seq2", 5, 10 ) ]
        
        expTuple = ( 3, 10 )
        obsTuple = SetUtils.getListBoundaries( lSets )
        
        self.assertEqual( expTuple, obsTuple )
 
    def test_writeListInFile_empty_list(self):
        lSets = [ ]
        expFileName = "expFileName"
        fileHandle = open(expFileName, "w")
        fileHandle.close()
 
        obsFileName = "obsFileName"
        with open(obsFileName, "w") as fileHandle:
            SetUtils.writeListInFile(lSets, obsFileName, "w")

         
        self.assertTrue( FileUtils.are2FilesIdentical( expFileName, obsFileName ) )
        
        os.remove(obsFileName)
        os.remove(expFileName)
        
    def test_writeListInFile_list_one_set(self):
        lSets = [ Set( 1, "set1", "seq1", 3, 8 ) ]
        line =  "1\tset1\tseq1\t3\t8\n"
        expFileName = "expFileName"

        with open(expFileName, "w") as fileHandle:
            fileHandle.write(line)
 
        obsFileName = "obsFileName"
        with open(obsFileName, "w") as fileHandle:
            SetUtils.writeListInFile(lSets, obsFileName, "w")
         
        self.assertTrue( FileUtils.are2FilesIdentical( expFileName, obsFileName ) )
        
        os.remove(obsFileName)
        os.remove(expFileName)
        
        
    def test_getDictOfListsWithIdAsKey_empty_list( self ):
        lSets = []
        expDic = {}
        obsDic = SetUtils.getDictOfListsWithIdAsKey( lSets )
        self.assertEqual( expDic, obsDic )
        
        
    def test_getDictOfListsWithIdAsKey_one_set( self ):
        lSets = [ Set( 2, "set2", "seq2", 3, 8 ) ]
        expDic = { 2: [ Set( 2, "set2", "seq2", 3, 8 ) ] }
        obsDic = SetUtils.getDictOfListsWithIdAsKey( lSets )
        self.assertEqual( expDic, obsDic )
        
        
    def test_getDictOfListsWithIdAsKey_two_sets_with_same_id( self ):
        lSets = [ Set( 2, "set2", "seq2", 10, 50 ), Set( 2, "set2", "seq2", 3, 8 )]
        expDic = { 2: [Set( 2, "set2", "seq2", 10, 50 ), Set( 2, "set2", "seq2", 3, 8 )] }
        obsDic = SetUtils.getDictOfListsWithIdAsKey( lSets )
        self.assertEqual( expDic, obsDic )
        
        
    def test_getDictOfListsWithIdAsKey_sets_with_3_different_id( self ):
        set1 = Set( 1, "set2", "seq2", 10, 50 )
        set2 = Set( 1, "set2", "seq2", 20, 50 )
        set3 = Set( 2, "set2", "seq2", 30, 50 )
        set4 = Set( 2, "set2", "seq2", 40, 50 )
        set5 = Set( 3, "set2", "seq2", 1, 2 )
        lSets = [ set1, set2, set3, set4, set5 ]
        expDic = { 1: [set1, set2],
                   2: [set3, set4],
                   3: [set5] }
        obsDic = SetUtils.getDictOfListsWithIdAsKey( lSets )
        self.assertEqual( expDic, obsDic )
        
        
    def test_getDictOfListsWithIdAsKeyFromFile(self):
        setFile = "dummySetFile"
        setFileHandler = open( setFile, "w" )
        setFileHandler.write( "1\tseq31\tchr1\t151\t250\n" )
        setFileHandler.write( "2\tseq27\tchr2\t301\t500\n" )
        setFileHandler.write( "2\tseq27\tchr2\t601\t650\n" )
        setFileHandler.close()
        
        dExp = { 1: [ Set( 1, "seq31", "chr1", 151, 250 ) ],
                2: [ Set( 2, "seq27", "chr2", 301, 500 ),
                    Set( 2, "seq27", "chr2", 601, 650 ) ] }
        dObs = SetUtils.getDictOfListsWithIdAsKeyFromFile( setFile )
        
        self.assertEqual( dExp, dObs )
        
        os.remove( setFile )
        
        
    def test_getMapListFromSetList_empty_list(self):
        lSets = []
        
        expLMap = []
        obsLMap = SetUtils.getMapListFromSetList(lSets)
        
        self.assertEqual(expLMap, obsLMap)
        
    def test_getMapListFromSetList_one_set(self):
        lSets = [ Set( 1, "set", "seq1", 1, 6 )]
        map1 = Map("set::1","seq1",1,6)
        
        expLMap = [ map1 ]
        obsLMap = SetUtils.getMapListFromSetList(lSets)
        
        self.assertEqual(expLMap, obsLMap)
                    
    def test_getMapListFromSetList_two_sets(self):
        lSets = [ Set( 1, "set", "seq1", 1, 6 ), Set( 2, "set", "seq2", 3, 8 ) ]
        map1 = Map("set::1","seq1",1,6)
        map2 = Map("set::2","seq2",3,8)
        
        expLMap = [ map1, map2 ]
        obsLMap = SetUtils.getMapListFromSetList(lSets)
        
        self.assertEqual(expLMap, obsLMap)
        
    def test_getSetListFromMapList_empty_list(self):
        lMap =[]
        expLSets = []
        obsLSets = SetUtils.getSetListFromMapList(lMap)
        
        self.assertEqual(expLSets, obsLSets)
        
    def test_getSetListFromMapList_one_map(self):
        map1 = Map( "map1","seq1",1,6)
        lMap =[ map1 ]
        expLSets = [ Set( 1, "map1", "seq1", 1, 6 ) ]
        obsLSets = SetUtils.getSetListFromMapList(lMap)
                            
        self.assertEqual(expLSets, obsLSets)
        
    def test_getSetListFromMapList_two_maps(self):
        map1 = Map( "map1","seq1",1,6)
        map2 = Map( "map2","seq2",3,8)
        lMap =[ map1,map2 ]
        expLSets = [ Set( 1, "map1", "seq1", 1, 6 ), Set( 2, "map2", "seq2", 3, 8 ) ]
        obsLSets = SetUtils.getSetListFromMapList(lMap)
        
        self.assertEqual(expLSets, obsLSets)
        
    def test_getSetListFromMapList_list_with_one_empty_map(self):
        map1 = Map( "map1","seq1",1,6)
        map2 = Map()
        lMap =[ map1,map2 ]
        expLSets = [ Set( 1, "map1", "seq1", 1, 6 ), Set(2, ) ]
        obsLSets = SetUtils.getSetListFromMapList(lMap)
        
        self.assertEqual(expLSets, obsLSets)
        
    def test_mergeSetsInList(self):
        set1 = Set( 1, "set1", "seq1", 1, 6 )
        set2 = Set( 2, "set2", "seq1", 3, 6 )
        set3 = Set( 3, "set3", "seq1", 3, 5 )
        set4 = Set( 4, "set4", "seq1", 1, 2 )
        lSets = [ set1, set2, set3, set4 ]
        
        obsList = SetUtils.mergeSetsInList( lSets )
        expList = [ Set( 1, "set1", "seq1", 1, 6 ) ]
        
        self.assertEqual( expList, obsList )
        
        
    def test_mergeSetsInList_with_reverse_only(self):
        set1 = Set( 1, "set1", "seq1", 6, 1 )
        set2 = Set( 2, "set2", "seq1", 6, 3 )
        set3 = Set( 3, "set3", "seq1", 5, 3 )
        set4 = Set( 4, "set4", "seq1", 2, 1 )
        lSets = [ set1, set2, set3, set4 ]
        
        obsList = SetUtils.mergeSetsInList( lSets )
        expList = [ Set( 1, "set1", "seq1", 6, 1 ) ]
        
        self.assertEqual( expList, obsList )
        
        
    def test_mergeSetsInList_with_direct_and_reverse(self):
        set1 = Set( 1, "set1", "seq1", 6, 1 )
        set2 = Set( 2, "set2", "seq1", 6, 3 )
        set3 = Set( 3, "set3", "seq1", 3, 5 )
        set4 = Set( 4, "set4", "seq1", 2, 1 )
        lSets = [ set1, set2, set3, set4 ]
        
        obsList = SetUtils.mergeSetsInList( lSets )
        expList = [ set1 ]
        
        self.assertEqual( expList, obsList )
        
        
    def test_mergeSetsInList_with_empty_set(self):
        set1 = Set( 1, "set1", "seq1", 1, 6 )
        set2 = Set()
        set3 = Set( 3, "set3", "seq1", 3, 5 )
        set4 = Set( 4, "set4", "seq1", 1, 2 )
        lSets = [ set1, set2, set3, set4 ]
        
        obsList = SetUtils.mergeSetsInList( lSets )
        expList = [ set2, set1 ]
        
        self.assertEqual( expList, obsList )
        
        
    def test_mergeSetsInList_empty_list(self):
        lSets = []
        
        obsList = SetUtils.mergeSetsInList( lSets )
        expList = []
         
        self.assertEqual( expList, obsList )
        
        
    def test_mergeSetsInList_one_set (self):
        set1 = Set( 1, "set1", "seq1", 1, 6 )
        lSets = [ set1 ]
        
        obsLSets = SetUtils.mergeSetsInList( lSets )
        expLSets = [ set1 ]
         
        self.assertEqual( expLSets, obsLSets )
        
        
    def test_mergeSetsInList_diffSeqnames_without_overlapping(self):
        set1 = Set( 1, "set1", "seq1", 1, 6 )
        set2 = Set( 1, "set2", "seq2", 11, 16 )
        lSets = [ set1, set2 ]
        
        obsLSets = SetUtils.mergeSetsInList( lSets )
        expLSets = [ set1, set2 ]
        
        self.assertEqual( expLSets, obsLSets )
        
 
    def test_mergeSetsInList_diffSeqnames_overlapping(self):
        set1 = Set( 1, "set1", "seq1", 1, 6 )
        set2 = Set( 1, "set2", "seq2", 2, 5 )
        lSets = [ set1, set2 ]
        
        obsLSets = SetUtils.mergeSetsInList( lSets )
        expLSets = [ set1, set2 ]
        
        self.assertEqual( expLSets, obsLSets )
        
 
    def test_mergeSetsInList_sameSeqnames_without_overlapping(self):
        set1 = Set( 1, "set1", "seq1", 1, 6 )
        set2 = Set( 1, "set2", "seq1", 11, 16 )
        lSets = [ set1, set2 ]
        
        obsLSets = SetUtils.mergeSetsInList( lSets )
        expLSets = [ set1, set2 ]
        
        self.assertEqual( expLSets, obsLSets )
        
    def test_mergeSetsInList_complex(self):
        set1 = Set(498, "(ACATATAATAA)10", "Mela_Blaster_Grouper_611_MAP_20", 77, 181)
        set2 = Set(499, "(TACATA)25", "Mela_Blaster_Grouper_611_MAP_20", 17, 166)
        set3 = Set(500, "(ATAAAATAC)26", "Mela_Blaster_Grouper_611_MAP_20", 94, 348)
        set4 = Set(501, "(TAA)15", "Mela_Blaster_Grouper_611_MAP_20", 257, 303)
        set5 = Set(502, "(GGAGA)2", "Mela_Blaster_Grouper_611_MAP_20", 356, 366)
        set6 = Set(503, "(TTTAAAGAACCC)2", "Mela_Blaster_Grouper_611_MAP_20", 433, 457)
        set7 = Set(504, "(TATAA)2", "Mela_Blaster_Grouper_611_MAP_20", 484, 495)    
        lSets = [ set1, set2, set3, set4, set5, set6, set7 ]  
        obsLSets = SetUtils.mergeSetsInList( lSets )
        
        set8 = Set(498, "(TACATA)25", "Mela_Blaster_Grouper_611_MAP_20", 17, 348)
        
        expLSets = [ set8, set5, set6, set7 ]
        
        self.assertEqual( expLSets, obsLSets )
        
    def test_getSetListUnjoined_listToKeep_empty_listToUnjoin_empty (self):
        setListToKeep = []
        setListToUnjoin = []
 
        obsLSets = SetUtils.getSetListUnjoined(setListToKeep, setListToUnjoin)
        expLSets = []
 
        self.assertEqual(expLSets, obsLSets)
        
    def test_getSetListUnjoined_listToKeep_empty (self):
        setListToKeep = []
        
        setUnjoin1 = Set( 1, "set", "seq", 1, 3 )
        setUnjoin2 = Set( 1, "set", "seq", 2, 3 )
        setUnjoin3 = Set( 1, "set", "seq", 5, 7 )
        setListToUnjoin = [ setUnjoin1, setUnjoin2, setUnjoin3 ]
 
        obsLSets = SetUtils.getSetListUnjoined(setListToKeep, setListToUnjoin)
        expLSets = [ setListToUnjoin ]
 
        self.assertEqual(expLSets, obsLSets)       
        
    def test_getSetListUnjoined_listToKeep_empty_listToUnjoin_size1 (self):
        setListToKeep = []
        
        setUnjoin = Set( 1, "set", "seq", 1, 3 )
        setListToUnjoin = [ setUnjoin ]
 
        obsLSets = SetUtils.getSetListUnjoined(setListToKeep, setListToUnjoin)
        expLSets = [ setListToUnjoin ]
 
        self.assertEqual(expLSets, obsLSets)     
        
    def test_getSetListUnjoined_listToKeep_size1_listToUnjoin_empty (self):
        setKeep = Set( 1, "set", "seq", 1, 3 )
        setListToKeep = [ setKeep ]
        
        setListToUnjoin = []
 
        obsLSets = SetUtils.getSetListUnjoined(setListToKeep, setListToUnjoin)
        expLSets = []
 
        self.assertEqual(expLSets, obsLSets)  
       
    def test_getSetListUnjoined_listToKeep_size1_listToUnjoin_size1 (self):
        setKeep = Set( 1, "set", "seq", 1, 3 )
        setListToKeep = [ setKeep ]
        
        setUnjoin = Set( 1, "set", "seq", 5, 9 )
        setListToUnjoin = [ setUnjoin ]
 
        obsLSets = SetUtils.getSetListUnjoined(setListToKeep, setListToUnjoin)
        expLSets = [ setListToUnjoin ]
 
        self.assertEqual(expLSets, obsLSets)
 
    def test_getSetListUnjoined_listToKeep_size1_listToUnjoin_size2_noSplit_item_in_listToKeep_smaller_item_in_listToUnjoin (self):
        setKeep = Set( 1, "set", "seq", 1, 3 )
        setListToKeep = [ setKeep ]
        
        setUnjoin1 = Set( 1, "set", "seq", 5, 9 )
        setUnjoin2 = Set( 1, "set", "seq", 11, 16 )
        setListToUnjoin = [ setUnjoin1, setUnjoin2 ]
 
        obsLSets = SetUtils.getSetListUnjoined(setListToKeep, setListToUnjoin)
        expLSets = [ setListToUnjoin ]
 
        self.assertEqual(expLSets, obsLSets)  
 
    def test_getSetListUnjoined_listToKeep_size1_listToUnjoin_size3_noSplit_item_in_listToKeep_smaller_item_in_listToUnjoin (self):
        setKeep = Set( 1, "set", "seq", 1, 3 )
        setListToKeep = [ setKeep ]
        
        setUnjoin1 = Set( 1, "set", "seq", 5, 9 )
        setUnjoin2 = Set( 1, "set", "seq", 11, 16 )
        setUnjoin3 = Set( 1, "set", "seq", 21, 26 )
        setListToUnjoin = [ setUnjoin1, setUnjoin2, setUnjoin3 ]
 
        obsLSets = SetUtils.getSetListUnjoined(setListToKeep, setListToUnjoin)
        expLSets = [ setListToUnjoin ]
 
        self.assertEqual(expLSets, obsLSets)  
 
    def test_getSetListUnjoined_listToKeep_size1_listToUnjoin_size2_noSplit_item_in_listToUnjoin_smaller_item_in_listToKeep (self):
        setKeep = Set( 1, "set", "seq", 10, 13 )
        setListToKeep = [ setKeep ]
        
        setUnjoin1 = Set( 1, "set", "seq", 5, 9 )
        setUnjoin2 = Set( 1, "set", "seq", 1, 3 )
        setListToUnjoin = [ setUnjoin1, setUnjoin2 ]
 
        obsLSets = SetUtils.getSetListUnjoined(setListToKeep, setListToUnjoin)
        expLSets = [ SetUtils.getSetListSortedByIncreasingMinThenMax( setListToUnjoin ) ]
 
        self.assertEqual(expLSets, obsLSets)   
 
    def test_getSetListUnjoined_listToKeep_size3_listToUnjoin_size2_oneSplit_item_in_listToKeep_smaller_item_in_listToUnjoin (self):
        setKeep1 = Set( 1, "set", "seq", 1, 1 )
        setKeep2 = Set( 1, "set", "seq", 21, 30 )
        setKeep3 = Set( 1, "set", "seq", 61, 70 )
        setListToKeep = [ setKeep1, setKeep2, setKeep3 ]
        
        setUnjoin1 = Set( 1, "set", "seq", 41, 50 )
        setUnjoin2 = Set( 2, "set", "seq", 81, 90 )
        setListToUnjoin = [ setUnjoin1, setUnjoin2 ]
 
        expLSets = [ [ setUnjoin1 ], [ setUnjoin2 ] ]
        
        obsLSets = SetUtils.getSetListUnjoined(setListToKeep, setListToUnjoin)
 
        self.assertEqual(expLSets, obsLSets) 
        
    def test_getSetListUnjoined_listToKeep_size3_listToUnjoin_size3_twoSplits_item_in_listToUnjoin_smaller_item_in_listToKeep(self):
        setKeep1 = Set( 1, "set", "seq", 21, 30 )
        setKeep2 = Set( 1, "set", "seq", 41, 50 )
        setKeep3 = Set( 1, "set", "seq", 81, 90 )
        setListToKeep = [ setKeep1, setKeep2, setKeep3 ]
        
        setUnjoin1 = Set( 1, "set", "seq", 1, 10 )
        setUnjoin2 = Set( 2, "set", "seq", 61, 70 )
        setUnjoin3 = Set( 2, "set", "seq", 101, 110 )
        setListToUnjoin = [ setUnjoin1, setUnjoin2, setUnjoin3 ]
        
        expLSets = [ [ setUnjoin1 ], [ setUnjoin2 ], [ setUnjoin3 ] ]
        
        obsLSets = SetUtils.getSetListUnjoined( setListToKeep, setListToUnjoin )
        
        self.assertEqual(expLSets, obsLSets) 
        
    def test_getSetListUnjoined_listToKeep_size1_listToUnjoin_size2_split(self):
        setKeep1 = Set( 1, "set", "seq", 51, 80 )
        setListToKeep = [ setKeep1 ]        
        
        setUnjoin1 = Set( 1, "set", "seq", 21, 40 )
        setUnjoin2 = Set( 1, "set", "seq", 101, 150 )
        setListToUnjoin = [ setUnjoin1, setUnjoin2 ]
        
        expLSets = [ [setUnjoin1] , [setUnjoin2] ]
               
        obsLSets = SetUtils.getSetListUnjoined( setListToKeep, setListToUnjoin )
        
        self.assertEqual( expLSets, obsLSets )
 
    def test_getSetListUnjoined_listToKeep_size2_listToUnjoin_size2_split(self):
        setKeep1 = Set( 1, "set", "seq", 1, 15 )
        setKeep2 = Set( 1, "set", "seq", 81, 130 )
        setListToKeep = [ setKeep1, setKeep2 ]
        
        setUnjoin1 = Set( 1, "set", "seq", 21, 40 )
        setUnjoin2 = Set( 1, "set", "seq", 201, 250 )
        setListToUnjoin = [ setUnjoin1, setUnjoin2 ]
        
        expLSets = [ [setUnjoin1] , [setUnjoin2] ] 
        
        obsLSets = SetUtils.getSetListUnjoined( setListToKeep, setListToUnjoin )
        
        self.assertEqual( expLSets, obsLSets )
        
    def test_getSetListUnjoined_listToKeep_size1_listToUnjoin_ordered_OneSplit(self):
        setKeep = Set( 1, "set", "seq", 120, 180 )
        setListToKeep = [ setKeep ]
        
        setUnjoin1 = Set( 1, "set", "seq", 100, 110 )
        setUnjoin2 = Set( 1, "set", "seq", 200, 210 )
        setUnjoin3 = Set( 1, "set", "seq", 250, 280 )
        setListToUnjoin = [ setUnjoin1, setUnjoin2, setUnjoin3 ]
 
        obsLSets = SetUtils.getSetListUnjoined(setListToKeep, setListToUnjoin)
        expLSets = [ [setUnjoin1], [setUnjoin2, setUnjoin3] ]
 
        self.assertEqual(expLSets, obsLSets)
        
    def test_getSetListUnjoined_listToKeep_size1_listToUnjoin_unordered_OneSplit(self):
        setKeep = Set( 1, "set", "seq", 120, 180 )
        setListToKeep = [ setKeep ]
        
        setUnjoin1 = Set( 1, "set", "seq", 200, 210 )
        setUnjoin2 = Set( 1, "set", "seq", 250, 280 )
        setUnjoin3 = Set( 1, "set", "seq", 100, 110 )
        setListToUnjoin = [ setUnjoin1, setUnjoin2, setUnjoin3 ]
 
        obsLSets = SetUtils.getSetListUnjoined(setListToKeep, setListToUnjoin)
        expLSets = [ [setUnjoin3], [setUnjoin1, setUnjoin2] ]
 
        self.assertEqual(expLSets, obsLSets)
 
    def test_getSetListUnjoined_listToKeep_size2_listToUnjoin_size4_twoSplits(self):
        setKeep1 = Set( 1, "set", "seq", 21, 30 )
        setKeep2 = Set( 1, "set", "seq", 81, 90 )
        setListToKeep = [ setKeep1, setKeep2 ]
                
        setUnjoin1 = Set( 1, "set", "seq", 1, 10 )
        setUnjoin2 = Set( 1, "set", "seq", 41, 50 )
        setUnjoin3 = Set( 1, "set", "seq", 61, 70 )
        setUnjoin4 = Set( 1, "set", "seq", 101, 110 )
        setListToUnjoin = [ setUnjoin1, setUnjoin2, setUnjoin3, setUnjoin4 ]
        
        expLSets = [ [setUnjoin1], [ setUnjoin2, setUnjoin3 ], [setUnjoin4] ]
        
        obsLSets = SetUtils.getSetListUnjoined(setListToKeep, setListToUnjoin)
        
        self.assertEqual( expLSets, obsLSets )
        
    def test_getSetListWithoutDuplicates_empty_list(self):
        lSets = []
        obsLSets = SetUtils.getSetListWithoutDuplicates( lSets )        
        expLSets = []
        self.assertEqual( expLSets, obsLSets )
        
    def test_getSetListWithoutDuplicates_list_size1(self):
        set = Set( 1, "set", "seq", 100, 110 )
        lSets = [ set ]
        obsLSets = SetUtils.getSetListWithoutDuplicates( lSets )
        expLSets = lSets
        self.assertEqual( expLSets, obsLSets )
        
    def test_getSetListWithoutDuplicates_list_with_only_doublons(self):
        set1 = Set( 1, "set", "seq", 100, 110 )
        set2 = Set( 1, "set", "seq", 100, 110 )
        set3 = Set( 1, "set", "seq", 100, 110 )
        set4 = Set( 1, "set", "seq", 100, 110 )
        lSets = [set1, set2, set3, set4]
        
        obsLSets = SetUtils.getSetListWithoutDuplicates( lSets )
        
        expLSets = [ set1 ]
        
        self.assertEqual( expLSets, obsLSets )
        
    def test_getSetListWithoutDuplicates_list_with_doublons_at_start_and_at_end(self):
        set1 = Set( 1, "set", "seq", 200, 210 )
        set2 = Set( 1, "set", "seq", 100, 110 )
        set3 = Set( 1, "set", "seq", 300, 310 )
        set4 = Set( 1, "set", "seq", 100, 110 )
        lSets = [set1, set2, set3, set4]
        
        obsLSets = SetUtils.getSetListWithoutDuplicates( lSets )
        
        expLSets = [ set1, set2, set3 ]
        expLSets = SetUtils.getSetListSortedByIncreasingMinThenMax(expLSets)
        
        self.assertEqual( expLSets, obsLSets )
        
    def test_getSetListWithoutDuplicates_list_with_contiguous_doublons(self):
        set1 = Set( 1, "set", "seq", 200, 210 )
        set2 = Set( 1, "set", "seq", 100, 110 )
        set3 = Set( 1, "set", "seq", 100, 110 )
        set4 = Set( 1, "set", "seq", 300, 310 )
        lSets = [set1, set2, set3, set4]
        
        obsLSets = SetUtils.getSetListWithoutDuplicates( lSets )
        
        expLSets = [ set1, set2, set4 ]
        expLSets = SetUtils.getSetListSortedByIncreasingMinThenMax(expLSets)
        
        self.assertEqual( expLSets, obsLSets )
        
    def test_getSetListWithoutDuplicates_list_with_one_doublon(self):
        set1 = Set( 1, "set", "seq", 200, 210 )
        set2 = Set( 1, "set", "seq", 100, 110 )
        set3 = Set( 1, "set", "seq", 210, 250 )
        set4 = Set( 1, "set", "seq", 100, 110 )
        set5 = Set( 1, "set", "seq", 300, 310 )
        lSets = [set1, set2, set3, set4, set5]
        
        obsLSets = SetUtils.getSetListWithoutDuplicates( lSets )
        
        expLSets = [ set1, set2, set3, set5 ]
        expLSets = SetUtils.getSetListSortedByIncreasingMinThenMax(expLSets)
        
        self.assertEqual( expLSets, obsLSets )
        
    def test_getSetListWithoutDuplicates_list_with_two_doublons(self):
        set1 = Set( 1, "set", "seq", 200, 210 )
        set2 = Set( 1, "set", "seq", 100, 110 )
        set3 = Set( 1, "set", "seq", 210, 250 )
        set4 = Set( 1, "set", "seq", 230, 250 )
        set5 = Set( 1, "set", "seq", 210, 250 )
        set6 = Set( 1, "set", "seq", 100, 110 )
        set7 = Set( 1, "set", "seq", 300, 310 )
        lSets = [set1, set2, set3, set4, set5, set6, set7]
        
        obsLSets = SetUtils.getSetListWithoutDuplicates( lSets )
        
        expLSets = [ set1, set2, set3, set4, set7 ]
        expLSets = SetUtils.getSetListSortedByIncreasingMinThenMax(expLSets)
        
        self.assertEqual( expLSets, obsLSets )
       
    def test_getSetListSortedByIncreasingMinThenMax_alreadyOrdered_diffIdentifier(self):
        set1 = Set( 1, "set1", "seq1", 1, 10 )
        set2 = Set( 2, "set1", "seq1", 21, 30 )
        lSets = [ set1, set2 ]
        
        expList = [ set1, set2 ]
        
        obsList = SetUtils.getSetListSortedByIncreasingMinThenMax( lSets )
        
        self.assertEqual( expList, obsList )
        
        
    def test_getSetListSortedByIncreasingMinThenMax_unordered_diffIdentifier(self):
        set1 = Set( 1, "set1", "seq1", 21, 30 )
        set2 = Set( 2, "set1", "seq1", 1, 10 )
        lSets = [ set1, set2 ]
        
        expList = [ set2, set1 ]
        
        obsList = SetUtils.getSetListSortedByIncreasingMinThenMax( lSets )
        
        self.assertEqual( expList, obsList )
        
        
    def test_getSetListSortedByIncreasingMinThenMax_unordered_sameIdentifier(self):
        set1 = Set( 1, "set1", "seq1", 21, 30 )
        set2 = Set( 1, "set1", "seq1", 1, 10 )
        lSets = [ set1, set2 ]
        
        expList = [ set2, set1 ]
        
        obsList = SetUtils.getSetListSortedByIncreasingMinThenMax( lSets )
        
        self.assertEqual( expList, obsList )
        
        
    def test_getSetListSortedByIncreasingMinThenMax_unordered_overlapping(self):
        set1 = Set( 2, "set1", "seq1", 6, 15 )
        set2 = Set( 1, "set1", "seq1", 1, 10 )
        lSets = [ set1, set2 ]
        
        expList = [ set2, set1 ]
        
        obsList = SetUtils.getSetListSortedByIncreasingMinThenMax( lSets )
        
        self.assertEqual( expList, obsList )
        
    def test_getSetListSortedByIncreasingMinThenMax_unordered_sameMin_threeSets(self):
        set1 = Set( 1, "set1", "seq1", 1, 15 )
        set2 = Set( 2, "set2", "seq1", 1, 10 )
        set3 = Set( 3, "set3", "seq1", 1, 12 )
        lSets = [ set1, set2, set3 ]
        
        expList = [ set2, set3, set1 ]
        
        obsList = SetUtils.getSetListSortedByIncreasingMinThenMax( lSets )
        
        self.assertEqual( expList, obsList )
        
        
    def test_getSetListSortedByIncreasingMinThenMax_unordered_included(self):
        set1 = Set( 1, "set1", "seq1", 2, 4 )
        set2 = Set( 2, "set2", "seq1", 1, 5 )
        lSets = [ set1, set2 ]
        
        expList = [ set2, set1 ]
        
        obsList = SetUtils.getSetListSortedByIncreasingMinThenMax( lSets )
        
        self.assertEqual( expList, obsList )
        
        
    def test_getSetListSortedByIncreasingMinThenInvLength_alreadyOrdered_diffIdentifier(self):
        set1 = Set( 1, "set1", "seq1", 1, 10 )
        set2 = Set( 2, "set1", "seq1", 21, 30 )
        lSets = [ set1, set2 ]
        
        expList = [ set1, set2 ]
        
        obsList = SetUtils.getSetListSortedByIncreasingMinThenInvLength( lSets )
        
        self.assertEqual( expList, obsList )
        
        
    def test_getSetListSortedByIncreasingMinThenInvLength_unordered_diffIdentifier(self):
        set1 = Set( 1, "set1", "seq1", 21, 30 )
        set2 = Set( 2, "set1", "seq1", 1, 10 )
        lSets = [ set1, set2 ]
        
        expList = [ set2, set1 ]
        
        obsList = SetUtils.getSetListSortedByIncreasingMinThenInvLength( lSets )
        
        self.assertEqual( expList, obsList )
        
        
    def test_getSetListSortedByIncreasingMinThenInvLength_unordered_sameIdentifier(self):
        set1 = Set( 1, "set1", "seq1", 21, 30 )
        set2 = Set( 1, "set1", "seq1", 1, 10 )
        lSets = [ set1, set2 ]
        
        expList = [ set2, set1 ]
        
        obsList = SetUtils.getSetListSortedByIncreasingMinThenInvLength( lSets )
        
        self.assertEqual( expList, obsList )
        
        
    def test_getSetListSortedByIncreasingMinThenInvLength_unordered_overlapping(self):
        set1 = Set( 2, "set1", "seq1", 6, 15 )
        set2 = Set( 1, "set1", "seq1", 1, 10 )
        lSets = [ set1, set2 ]
        
        expList = [ set2, set1 ]
        
        obsList = SetUtils.getSetListSortedByIncreasingMinThenInvLength( lSets )
        
        self.assertEqual( expList, obsList )
        
        
    def test_getSetListSortedByIncreasingMinThenInvLength_unordered_sameMin_threeSets(self):
        set1 = Set( 1, "set1", "seq1", 1, 10 )
        set2 = Set( 2, "set2", "seq1", 3, 8 )
        set3 = Set( 3, "set3", "seq1", 3, 17 )
        lSets = [ set1, set2, set3 ]
        
        expList = [ set1, set3, set2 ]
        
        obsList = SetUtils.getSetListSortedByIncreasingMinThenInvLength( lSets )
        
        self.assertEqual( expList, obsList )
        
    def test_getSetListSortedByIncreasingMinThenInvLength_unordered_included(self):
        set1 = Set( 1, "set1", "seq1", 2, 4 )
        set2 = Set( 2, "set2", "seq1", 1, 5 )
        lSets = [ set1, set2 ]
        
        expList = [ set2, set1 ]
        
        obsList = SetUtils.getSetListSortedByIncreasingMinThenInvLength( lSets )
        
        self.assertEqual( expList, obsList )
        
    def test_getSetListSortedBySeqThenRegionThenMinThenMax_already_sorted(self):
        set1 = Set(1, "set1", "seq1", 2, 4)
        set2 = Set(2, "set2", "seq2", 1, 5)
        set3 = Set(3, "set3", "seq2", 8, 10)
        lSets = [set1, set2, set3]
        
        expList = [set1, set2, set3]
        
        obsList = SetUtils.getSetListSortedBySeqThenRegionThenMinThenMax(lSets)
        
        self.assertEqual(expList, obsList)
        
    def test_getSetListSortedBySeqThenRegionThenMinThenMax_not_sorted_by_seqname(self):
        set1 = Set(1, "set1", "seq1", 2, 4)
        set2 = Set(2, "set2", "seq2", 1, 5)
        set3 = Set(3, "set3", "seq2", 8, 10)
        lSets = [set2, set1, set3]
        
        expList = [set1, set2, set3]
        
        obsList = SetUtils.getSetListSortedBySeqThenRegionThenMinThenMax(lSets)
        
        self.assertEqual(expList, obsList)
         
    def test_getSetListSortedBySeqThenRegionThenMinThenMax_not_sorted_by_region_and_start(self):
        set1 = Set(1, "set1", "seq1", 2, 4)
        set2 = Set(2, "set2", "seq2", 1, 5)
        set3 = Set(3, "set3", "seq2", 8, 10)
        lSets = [set3, set2, set1]
        
        expList = [set1, set2, set3]
        
        obsList = SetUtils.getSetListSortedBySeqThenRegionThenMinThenMax(lSets)
        
        self.assertEqual(expList, obsList)
         
    def test_getSetListSortedBySeqThenRegionThenMinThenMax_not_sorted_with_overlap(self):
        set1 = Set(1, "set1", "seq1", 2, 4)
        set2 = Set(2, "set2", "seq2", 1, 5)
        set3 = Set(3, "set2", "seq2", 8, 10)
        set4 = Set(4, "set2", "seq3", 7, 9)
        set5 = Set(5, "set3", "seq3", 8, 12)
        lSets = [set3, set2, set1, set5, set4]
        
        expList = [set1, set2, set3, set4, set5]
        
        obsList = SetUtils.getSetListSortedBySeqThenRegionThenMinThenMax(lSets)
        
        self.assertEqual(expList, obsList)

    def test_getSetListSortedBySeqThenRegionThenMinThenMax_not_sorted_with_reverse(self):
        set1 = Set(1, "set1", "seq1", 2, 4)
        set2 = Set(2, "set2", "seq2", 1, 5)
        set3 = Set(3, "set2", "seq2", 8, 10)
        set4 = Set(4, "set3", "seq3", 7, 9)
        set5 = Set(5, "set3", "seq3", 12, 5)
        lSets = [set3, set2, set5, set1, set4]
        
        expList = [set1, set2, set3, set5, set4]
        
        obsList = SetUtils.getSetListSortedBySeqThenRegionThenMinThenMax(lSets)
        
        self.assertEqual(expList, obsList)

    def test_getListOfIdListOfOverlappingSets_2_item_in_reference_list_overlaps_with_one_item_in_subject_list(self):
        set1 = Set( 1, "set1", "seq1", 10, 30 )
        set2 = Set( 2, "set2", "seq1", 40, 60 )
        
        lRef = [set1, set2]

        set3 = Set( 3, "set2", "seq1", 25, 45 )
        lSubj = [set3]
        
        lExp = [[1,-3,2]]
        lObs = SetUtils.getListOfIdListOfOverlappingSets(lRef, lSubj)
        
        self.assertEqual(lExp, lObs)
        
    def test_getListOfIdListOfOverlappingSets_one_item_in_reference_list_overlaps_with_2_item_in_subject_list(self):
        set1 = Set( 1, "set2", "seq1", 25, 45 )
        lRef = [set1]
        
        set2 = Set( 2, "set1", "seq1", 10, 30 )
        set3 = Set( 3, "set2", "seq1", 40, 60 )
        
        lSubj = [set2, set3]
        
        lExp = [[1,-2,-3]]
        lObs = SetUtils.getListOfIdListOfOverlappingSets(lRef, lSubj)
        
        self.assertEqual(lExp, lObs)
        
    def test_getListOfIdListOfOverlappingSets_all_item_in_reference_list_overlaps_with_items_in_subject_list(self):
        set1 = Set( 1, "set1", "seq1", 10, 30 )
        set2 = Set( 2, "set2", "seq1", 40, 60 )
        set3 = Set( 3, "set2", "seq1", 70, 90 )
        
        lRef = [set1, set2, set3]
        
        set4 = Set( 4, "set1", "seq1", 5, 9 )
        set5 = Set( 5, "set1", "seq1", 15, 29 )
        set6 = Set( 6, "set2", "seq1", 45, 55 )
        set7 = Set( 7, "set2", "seq1", 57, 68 )
        set8 = Set( 8, "set2", "seq1", 73, 85 )
        set9 = Set( 9, "set2", "seq1", 100, 115 )
        
        lSubj = [set4, set5, set6, set7, set8, set9]
        
        lExp = [[1,-5], [2,-6,-7], [3,-8]]
        lObs = SetUtils.getListOfIdListOfOverlappingSets(lRef, lSubj)
        
        self.assertEqual(lObs, lExp)
            
    def test_getListOfIdListOfOverlappingSets_all_item_in_subject_list_overlaps_with_items_in_reference_list(self):
        set1 = Set( 1, "set1", "seq1", 5, 9 )
        set2 = Set( 2, "set1", "seq1", 15, 29 )
        set3 = Set( 3, "set2", "seq1", 45, 55 )
        set4 = Set( 4, "set2", "seq1", 57, 68 )
        set5 = Set( 5, "set2", "seq1", 73, 85 )
        set6 = Set( 6, "set2", "seq1", 100, 115 )
        
        lRef = [set1, set2, set3, set4, set5, set6]
        
        set7 = Set( 7, "set1", "seq1", 10, 30 )
        set8 = Set( 8, "set2", "seq1", 40, 60 )
        set9 = Set( 9, "set2", "seq1", 70, 90 )
        
        lSubj = [set7, set8, set9]
              
        lExp = [[2, -7], [3, -8, 4], [5, -9]]
        lObs = SetUtils.getListOfIdListOfOverlappingSets(lRef, lSubj)
        
        self.assertEqual(lObs, lExp) 
               
    def test_getListOfIdListOfOverlappingSets_all_item_overlaps_one_by_one(self):
        set1 = Set( 1, "set1", "seq1", 10, 20 )
        set2 = Set( 2, "set1", "seq1", 30, 40 )
        set3 = Set( 3, "set2", "seq1", 50, 60 )
        
        lRef = [set1, set2, set3]
        
        lSubj = [set1, set2, set3]
              
        lExp = [[1, -1], [2, -2], [3, -3]]
        lObs = SetUtils.getListOfIdListOfOverlappingSets(lRef, lSubj)
        
        self.assertEqual(lObs, lExp) 
        
    def test_getListOfIdListOfOverlappingSets_continuus_overlap_between_item_in_reference_list_and_item_in_subject_list(self):
        set1 = Set( 1, "set1", "seq1", 10, 20 )
        set2 = Set( 2, "set1", "seq1", 30, 40 )
        set3 = Set( 3, "set2", "seq1", 50, 60 )
        set4 = Set( 4, "set2", "seq1", 70, 80 )
        
        lRef = [set1, set2, set3, set4]
        
        set5 = Set( 5, "set1", "seq1", 15, 32 )
        set6 = Set( 6, "set2", "seq1", 35, 52 )
        set7 = Set( 7, "set2", "seq1", 55, 75 )
        
        lSubj = [set5, set6, set7]
              
        lExp = [[ 1, -5, 2, -6, 3, -7, 4 ]]
        lObs = SetUtils.getListOfIdListOfOverlappingSets(lRef, lSubj)
        
        self.assertEqual(lObs, lExp)   
        
        
    def test_getListOfIdListOfOverlappingSets_first_item_in_reference_overlap_with_one_item_in_subject_list(self):
        set1 = Set( 1, "set1", "seq1", 10, 20 )
        set2 = Set( 2, "set1", "seq1", 30, 40 )
        
        lRef = [set1, set2]
        
        set3 = Set( 3, "set1", "seq1", 5, 9 )
        set4 = Set( 4, "set2", "seq1", 12, 25 )
        set5 = Set( 5, "set2", "seq1", 55, 75 )
        
        lSubj = [set3, set4, set5]
              
        lExp = [[1, -4]]
        lObs = SetUtils.getListOfIdListOfOverlappingSets(lRef, lSubj)
        
        self.assertEqual(lObs, lExp)
        
    def test_getListOfIdListOfOverlappingSets_last_item_in_reference_overlap_with_one_item_in_subject_list(self):
        set1 = Set( 1, "set1", "seq1", 10, 20 )
        set2 = Set( 2, "set1", "seq1", 30, 40 )
        
        lRef = [set1, set2]
        
        set3 = Set( 3, "set1", "seq1", 5, 9 )
        set4 = Set( 4, "set2", "seq1", 32, 45 )
        set5 = Set( 5, "set2", "seq1", 55, 75 )
        
        lSubj = [set3, set4, set5]
              
        lExp = [[2, -4]]
        lObs = SetUtils.getListOfIdListOfOverlappingSets(lRef, lSubj)
        
        self.assertEqual(lObs, lExp)       
                
    def test_getListOfIdListOfOverlappingSets_one_item_in_reference_in_the_middle_overlap_with_one_item_in_subject_list(self):
        set1 = Set( 1, "set1", "seq1", 10, 20 )
        set2 = Set( 2, "set1", "seq1", 30, 40 )
        set3 = Set( 3, "set1", "seq1", 50, 60 )
        set4 = Set( 4, "set1", "seq1", 70, 80 )
        
        lRef = [set1, set2, set3, set4]
        
        set5 = Set( 5, "set1", "seq1", 2, 9 )
        set6 = Set( 6, "set2", "seq1", 55, 65 )
        set7 = Set( 7, "set2", "seq1", 90, 100 )
        
        lSubj = [set5, set6, set7]
              
        lExp = [[3, -6]]
        lObs = SetUtils.getListOfIdListOfOverlappingSets(lRef, lSubj)
        
        self.assertEqual(lObs, lExp)
        
    def test_getListOfIdListOfOverlappingSets_first_item_in_subject_overlap_with_one_item_in_reference_list(self):
        set1 = Set( 1, "set1", "seq1", 10, 20 )
        set2 = Set( 2, "set1", "seq1", 30, 40 )
        
        lRef = [set1, set2]
        
        set3 = Set( 3, "set1", "seq1", 15, 25 )
        set4 = Set( 4, "set2", "seq1", 45, 50 )
        set5 = Set( 5, "set2", "seq1", 55, 75 )
        
        lSubj = [set3, set4, set5]
              
        lExp = [[1, -3]]
        lObs = SetUtils.getListOfIdListOfOverlappingSets(lRef, lSubj)
        
        self.assertEqual(lObs, lExp)
                
    def test_getListOfIdListOfOverlappingSets_last_item_in_subject_overlap_with_one_item_in_reference_list(self):
        set1 = Set( 1, "set1", "seq1", 10, 20 )
        set2 = Set( 2, "set1", "seq1", 30, 40 )
        
        lRef = [set1, set2]
        
        set3 = Set( 3, "set1", "seq1", 1, 9 )
        set4 = Set( 4, "set2", "seq1", 21, 29 )
        set5 = Set( 5, "set2", "seq1", 35, 50 )
        
        lSubj = [set3, set4, set5]
              
        lExp = [[2, -5]]
        lObs = SetUtils.getListOfIdListOfOverlappingSets(lRef, lSubj)
        
        self.assertEqual(lObs, lExp) 
        
    def test_getListOfIdListOfOverlappingSets_one_item_in_subject_in_the_middle_overlap_with_one_item_in_reference_list(self):
        set1 = Set( 1, "set1", "seq1", 10, 20 )
        set2 = Set( 2, "set1", "seq1", 30, 40 )
        
        lRef = [set1, set2]
        
        set3 = Set( 3, "set1", "seq1", 1, 9 )
        set4 = Set( 4, "set2", "seq1", 21, 29 )
        set5 = Set( 5, "set2", "seq1", 35, 50 )
        set6 = Set( 6, "set2", "seq1", 60, 70 )
        
        lSubj = [set3, set4, set5, set6]
              
        lExp = [[2, -5]]
        lObs = SetUtils.getListOfIdListOfOverlappingSets(lRef, lSubj)
        
        self.assertEqual(lObs, lExp)
        
    def test_getListOfMergedSetsAndNextId_each_item_in_reference_list_overlap_with_one_different_item_in_subject_list(self):
        setRef1 = Set( 1, "set1", "seq1", 10, 20 )
        setRef2 = Set( 2, "set1", "seq1", 30, 40 )
        setRef3 = Set( 3, "set1", "seq1", 50, 60 )
        
        lRef = [setRef1, setRef2, setRef3]
        
        setSubj1 = Set( 1, "set1", "seq1", 12, 25 )
        setSubj2 = Set( 2, "set1", "seq1", 31, 45 )
        setSubj3 = Set( 3, "set1", "seq1", 55, 80 )
        
        lSubj = [setSubj1, setSubj2, setSubj3]
        
        setRef1merged = Set( 1, "set1", "seq1", 10, 25 )
        setRef2merged = Set( 2, "set1", "seq1", 30, 45 )
        setRef3merged = Set( 3, "set1", "seq1", 50, 80 )
        
        nextId = 4
        
        lExp = ([setRef1merged, setRef2merged, setRef3merged], nextId)
        lObs = SetUtils.getListOfMergedSetsAndNextId(lRef, lSubj)
        
        self.assertEqual(lObs, lExp)
        
    def test_getListOfMergedSetsAndNextId_all_items_in_reference_list_overlap_with_the_same_item_in_subject_list(self):
        setRef1 = Set( 1, "set1", "seq1", 10, 20 )
        setRef2 = Set( 2, "set1", "seq1", 15, 25 )
        setRef3 = Set( 3, "set1", "seq1", 20, 30 )
        
        lRef = [setRef1, setRef2, setRef3]
        
        setSubj1 = Set( 1, "set1", "seq1", 12, 35 )
        setSubj2 = Set( 2, "set1", "seq1", 40, 45 )
        setSubj3 = Set( 3, "set1", "seq1", 55, 80 )
        
        lSubj = [setSubj1, setSubj2, setSubj3]
        
        setRef1merged = Set( 1, "set1", "seq1", 10, 35 )
        setSubj2Exp = Set( 4, "set1", "seq1", 40, 45 )
        setSubj3Exp = Set( 5, "set1", "seq1", 55, 80 )
        
        nextId = 6
        
        lExp = ([setRef1merged, setSubj2Exp, setSubj3Exp], nextId)
        lObs = SetUtils.getListOfMergedSetsAndNextId(lRef, lSubj)
        
        self.assertEqual(lObs, lExp)
                
    def test_getListOfMergedSetsAndNextId_two_last_ref_items_overlap_with_last_subject_item(self):
        setRef1 = Set( 1, "set1", "seq1", 10, 20 )
        setRef2 = Set( 2, "set1", "seq1", 30, 40 )
        setRef3 = Set( 3, "set1", "seq1", 50, 60 )
        
        lRef = [setRef1, setRef2, setRef3]
        
        setSubj1 = Set( 1, "set1", "seq1", 1, 5 )
        setSubj2 = Set( 2, "set1", "seq1", 6, 9 )
        setSubj3 = Set( 3, "set1", "seq1", 35, 55 )
        
        lSubj = [setSubj1, setSubj2, setSubj3]
        
        setRef1Exp = Set( 1, "set1", "seq1", 10, 20 )
        setRef2merged = Set( 2, "set1", "seq1", 30, 60 )
        setSubj1Exp = Set( 4, "set1", "seq1", 1, 5 )
        setSubj2Exp = Set( 5, "set1", "seq1", 6, 9 )
        
        nextId = 6
        
        lExp = ([setRef2merged, setRef1Exp, setSubj1Exp, setSubj2Exp], nextId)
        lObs = SetUtils.getListOfMergedSetsAndNextId(lRef, lSubj)
        
        self.assertEqual(lObs, lExp)
        
    def test_getListOfMergedSetsAndNextId_all_items_in_reference_list_overlap_with_all_items_in_subject_list(self):
        setRef1 = Set( 1, "set1", "seq1", 10, 20 )
        setRef2 = Set( 2, "set1", "seq1", 30, 40 )
        
        lRef = [setRef1, setRef2]
        
        setSubj1 = Set( 1, "set1", "seq1", 10, 20 )
        setSubj2 = Set( 2, "set1", "seq1", 30, 40 )
        
        lSubj = [setSubj1, setSubj2]
        
        setRef1merged = Set( 1, "set1", "seq1", 10, 20 )
        setRef2merged = Set( 2, "set1", "seq1", 30, 40 )
        
        nextId = 3
        
        lExp = ([setRef1merged, setRef2merged], nextId)
        lObs = SetUtils.getListOfMergedSetsAndNextId(lRef, lSubj)
        
        self.assertEqual(lObs, lExp)
        
    def test_getListOfMergedSetsAndNextId_one_item_in_reference_list_overlap_with_all_items_in_subject_list(self):
        setRef1 = Set( 1, "set1", "seq1", 10, 20 )
        setRef2 = Set( 2, "set1", "seq1", 30, 40 )
        setRef3 = Set( 3, "set1", "seq1", 50, 60 )
        
        lRef = [setRef1, setRef2, setRef3]
        
        setSubj1 = Set( 1, "set1", "seq1", 11, 13 )
        setSubj2 = Set( 2, "set1", "seq1", 14, 16 )
        setSubj3 = Set( 3, "set1", "seq1", 17, 25 )
        
        lSubj = [setSubj1, setSubj2, setSubj3]
        
        setRef1merged = Set( 1, "set1", "seq1", 10, 25 )
        setRef2Exp = Set( 2, "set1", "seq1", 30, 40 )
        setRef3Exp = Set( 3, "set1", "seq1", 50, 60 )
        
        nextId = 4
        
        lExp = ([setRef1merged, setRef2Exp, setRef3Exp], nextId)
        lObs = SetUtils.getListOfMergedSetsAndNextId(lRef, lSubj)
        
        self.assertEqual(lObs, lExp)
        
    def test_getListOfMergedSetsAndNextId_one_item_in_reference_list_overlap_with_one_item_in_subject_list(self):
        setRef1 = Set( 1, "set1", "seq1", 10, 20 )
        setRef2 = Set( 2, "set1", "seq1", 30, 40 )
        setRef3 = Set( 3, "set1", "seq1", 50, 60 )
        
        lRef = [setRef1, setRef2, setRef3]
        
        setSubj1 = Set( 1, "set1", "seq1", 1, 9 )
        setSubj2 = Set( 2, "set1", "seq1", 31, 45 )
        setSubj3 = Set( 3, "set1", "seq1", 70, 80 )
        
        lSubj = [setSubj1, setSubj2, setSubj3]
        
        setRef1Exp = Set( 1, "set1", "seq1", 10, 20 )
        setRef2merged = Set( 2, "set1", "seq1", 30, 45 )
        setRef3Exp = Set( 3, "set1", "seq1", 50, 60 )
        setSubj1Exp = Set( 4, "set1", "seq1", 1, 9 )
        setSubj3Exp = Set( 5, "set1", "seq1", 70, 80 )
        
        nextId = 6
        
        lExp = ([setRef2merged, setRef1Exp, setRef3Exp, setSubj1Exp, setSubj3Exp], nextId)
        
        lObs = SetUtils.getListOfMergedSetsAndNextId(lRef, lSubj)
        
        self.assertEqual(lObs, lExp)
        
    def test_getListOfMergedSetsAndNextId_one_item_in_reference_list_overlap_with_one_item_in_subject_list_with_strange_output_order(self):
        setRef1 = Set( 5, "set1", "seq1", 10, 20 )
        setRef2 = Set( 6, "set1", "seq1", 30, 40 )
        setRef3 = Set( 7, "set1", "seq1", 50, 60 )
        setRef4 = Set( 8, "set1", "seq1", 90, 92 )
        
        lRef = [setRef1, setRef2, setRef3, setRef4]
        
        setSubj1 = Set( 1, "set1", "seq1", 1, 9 )
        setSubj2 = Set( 2, "set1", "seq1", 35, 45 )
        setSubj3 = Set( 3, "set1", "seq1", 70, 80 )
        setSubj4 = Set( 4, "set1", "seq1", 130, 140 )
        
        lSubj = [setSubj1, setSubj2, setSubj3, setSubj4]
        
        setRef1Exp = Set( 5, "set1", "seq1", 10, 20 )
        setRef2merged = Set( 6, "set1", "seq1", 30, 45 )
        setRef3Exp = Set( 7, "set1", "seq1", 50, 60 )
        setRef4Exp = Set( 8, "set1", "seq1", 90, 92 )
        setSubj1Exp = Set( 9, "set1", "seq1", 1, 9 )
        setSubj3Exp = Set( 10, "set1", "seq1", 70, 80 )
        setSubj4Exp = Set( 11, "set1", "seq1", 130, 140 )
        
        nextId = 12
        
        lExp = ([setRef2merged, setRef1Exp, setRef3Exp, setRef4Exp, setSubj1Exp, setSubj3Exp, setSubj4Exp], nextId)
        
        lObs = SetUtils.getListOfMergedSetsAndNextId(lRef, lSubj)
        
        self.assertEqual(lObs, lExp)
        
    def test_getListOfMergedSetsAndNextId_all_items_overlap_between_lists_with_same_length(self):
        setRef1 = Set( 1, "set1", "seq1", 10, 20 )
        setRef2 = Set( 2, "set1", "seq1", 30, 40 )
        setRef3 = Set( 3 , "set1", "seq1", 50, 60 )
        
        lRef = [setRef1, setRef2, setRef3]
        
        setSubj1 = Set( 1, "set1", "seq1", 15, 35 )
        setSubj2 = Set( 2, "set1", "seq1", 36, 55 )
        setSubj3 = Set( 3, "set1", "seq1", 56, 65 )
        
        lSubj = [setSubj1, setSubj2, setSubj3]
        
        setRef1merged = Set( 1, "set1", "seq1", 10, 65 )
        
        nextId = 4
        
        lExp = ([setRef1merged], nextId)
        lObs = SetUtils.getListOfMergedSetsAndNextId(lRef, lSubj)
        
        self.assertEqual(lObs, lExp)
        
    def test_getListOfMergedSetsAndNextId_all_items_overlap_between_lists_with_different_length(self):
        setRef1 = Set( 1, "set1", "seq1", 10, 20 )
        setRef2 = Set( 2, "set1", "seq1", 30, 40 )
        setRef3 = Set( 3 , "set1", "seq1", 50, 60 )
        
        lRef = [setRef1, setRef2, setRef3]
        
        setSubj1 = Set( 1, "set1", "seq1", 15, 35 )
        setSubj2 = Set( 2, "set1", "seq1", 36, 55 )
        
        lSubj = [setSubj1, setSubj2]
        
        setRef1merged = Set( 1, "set1", "seq1", 10, 60 )
        
        nextId = 4
        
        lExp = ([setRef1merged], nextId)
        lObs = SetUtils.getListOfMergedSetsAndNextId(lRef, lSubj)
        
        self.assertEqual(lObs, lExp)
        
    def test_getListOfSetWithoutOverlappingBetweenTwoListOfSet(self):
        set1 = Set( 1, "set1", "seq1", 10, 20 )
        set2 = Set( 2, "set1", "seq1", 30, 40 )
        set3 = Set( 3, "set1", "seq1", 50, 60 )
        
        lSet1 = [set1, set2, set3]
        
        set4 = Set( 1, "set1", "seq1", 15, 35 )
        set5 = Set( 2, "set1", "seq1", 36, 55 )
        set6 = Set( 3, "set1", "seq1", 66, 95 )
        
        lSet2 = [set4, set5, set6]
        
        expLSet = [set6]
        obsLSet = SetUtils.getListOfSetWithoutOverlappingBetweenTwoListOfSet(lSet1, lSet2)
        
        self.assertEqual(expLSet, obsLSet)
        
    def test_getListOfSetWithoutOverlappingBetweenTwoListOfSet_Empty_Result(self):
        set1 = Set(1, "set1", "seq1", 10, 20)
        set2 = Set(2, "set1", "seq1", 30, 40)
        set3 = Set(3, "set1", "seq1", 50, 60)
        
        lSet1 = [set1, set2, set3]
        
        set4 = Set(1, "set1", "seq1", 15, 35 )
        set5 = Set(2, "set1", "seq1", 36, 55 )
        
        lSet2 = [set4, set5]
        
        expLSet = []
        obsLSet = SetUtils.getListOfSetWithoutOverlappingBetweenTwoListOfSet(lSet1, lSet2)
        
        self.assertEqual(expLSet, obsLSet)
        
    def test_getListOfSetWithoutOverlappingBetweenTwoListOfSet_Empty_Result_Length_Condition(self):
        set1 = Set(1, "set1", "seq1", 10, 20)
        set2 = Set(2, "set1", "seq1", 30, 40)
        set3 = Set(3, "set1", "seq1", 50, 60)
        
        lSet1 = [set1, set2, set3]
        
        set4 = Set(1, "set1", "seq1", 15, 35)
        set5 = Set(2, "set1", "seq1", 36, 55)
        set6 = Set(3, "set1", "seq1", 66, 68)
        
        lSet2 = [set4, set5, set6]
        
        expLSet = []
        obsLSet = SetUtils.getListOfSetWithoutOverlappingBetweenTwoListOfSet(lSet1, lSet2)
        
        self.assertEqual(expLSet, obsLSet)
        
        
    def test_getSetListFromFile(self):
        file = "dummyFile_{}_{}" .format(time.strftime("%Y%m%d%H%M%S"), os.getpid())
        with open(file, "w") as fileHandler:
            fileHandler.write("1\tseq1\tchr1\t151\t250\n")
            fileHandler.write("2\tseq2\tchr2\t301\t500\n")
        s1 = Set()
        s1.setFromTuple(("1", "seq1", "chr1", "151", "250"))
        s2 = Set()
        s2.setFromTuple(("2", "seq2", "chr2", "301", "500"))
        expLSet = [s1, s2]
        obsLSet = SetUtils.getSetListFromFile(file)
        os.remove(file)
        self.assertEqual(expLSet, obsLSet)
        
        
    def test_convertSetFileIntoMapFile( self ):
        setFile = "dummySetFile"
        with open( setFile, "w") as setFileHandler:
            setFileHandler.write("1\tseq31\tchr1\t151\t250\n")
            setFileHandler.write("2\tseq27\tchr2\t301\t500\n")

        
        expFile = "dummyExpFile"
        with open( expFile, "w") as expFileHandler:
            expFileHandler.write("seq31\tchr1\t151\t250\n")
            expFileHandler.write("seq27\tchr2\t301\t500\n")

        obsFile = "dummyObsFile"
        
        SetUtils.convertSetFileIntoMapFile(setFile, obsFile)
        
        self.assertTrue(FileUtils.are2FilesIdentical(expFile, obsFile))
        
        for f in [setFile, expFile, obsFile]:
            os.remove(f)
            
            
    def test_getDictOfListsWithSeqnameAsKey_empty( self ):
        lSets = []
        dExp = {}
        dObs = SetUtils.getDictOfListsWithSeqnameAsKey( lSets )
        self.assertEqual( dExp, dObs )
            
            
    def test_getDictOfListsWithSeqnameAsKey( self ):
        lSets = [ Set( 1, "TE3", "chr2", 10, 50 ),
                 Set( 2, "gene74", "chr1", 31, 800 ),
                 Set( 3, "TE1", "chr1", 1, 30 ) ]
        dExp = { "chr1": [ Set( 2, "gene74", "chr1", 31, 800 ),
                          Set( 3, "TE1", "chr1", 1, 30 ) ],
                "chr2": [ Set( 1, "TE3", "chr2", 10, 50 ) ] }
        dObs = SetUtils.getDictOfListsWithSeqnameAsKey( lSets )
        self.assertEqual( dExp, dObs )
        
        
    def test_filterOnLength( self ):
        lSets = [ Set( 1, "TE3", "chr2", 10, 50 ),
                 Set( 2, "gene74", "chr1", 31, 800 ),
                 Set( 3, "TE1", "chr1", 1, 30 ) ]
        lExp = [ Set( 2, "gene74", "chr1", 31, 800 ) ]
        lObs = SetUtils.filterOnLength( lSets, 100 )
        self.assertEqual( lExp, lObs )
        
        
    def test_getListOfNames( self ):
        setFile = "dummySetFile"
        with open( setFile, "w" ) as setFileHandler:
            setFileHandler.write( "1\tseq31\tchr1\t151\t250\n" )
            setFileHandler.write( "2\tseq27\tchr2\t301\t500\n" )
        
        lExp = [ "seq31", "seq27" ]
        lObs = SetUtils.getListOfNames( setFile )
        
        self.assertEqual( lExp, lObs )
        
        os.remove( setFile )
        
        
    def test_getDictOfDictsWithNamesThenIdAsKeyFromFile( self ):
        setFile = "dummySetFile"
        with open( setFile, "w" ) as setFileHandler:
            setFileHandler.write( "1\tseq31\tchr1\t151\t250\n" )
            setFileHandler.write( "3\tseq27\tchr3\t1\t100\n" )
            setFileHandler.write( "2\tseq27\tchr2\t301\t500\n" )
            setFileHandler.write( "2\tseq27\tchr2\t601\t650\n" )
        
        dExp = { "seq31": { 1: [ Set( 1, "seq31", "chr1", 151, 250 ) ] },
                "seq27": { 2: [ Set( 2, "seq27", "chr2", 301, 500 ),
                               Set( 2, "seq27", "chr2", 601, 650 ) ],
                               3: [ Set( 3, "seq27", "chr3", 1, 100 ) ]
                               }
                }
        dObs = SetUtils.getDictOfDictsWithNamesThenIdAsKeyFromFile( setFile )
        
        self.assertEqual( dExp, dObs )
        
        os.remove( setFile )
        
        
    def _makeSetListFromTupleList (self, tupleList):
        setList = []
        for tuple in tupleList:
            set = Set()
            set.setFromTuple(tuple)
            setList.append(set)
        return setList
        
        

if __name__ == "__main__":
    test_suite = unittest.TestSuite()
    test_suite.addTest(unittest.makeSuite(Test_SetUtils))
    unittest.TextTestRunner(verbosity=2).run( test_suite )
