# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.


import unittest
from commons.core.coord.Path import Path
from commons.core.coord.Align import Align
from commons.core.coord.Set import Set


class Test_Path( unittest.TestCase ):
    
    def setUp( self ):
        self._path = Path()
        
    def test_setFromTuple( self ):
        line = "1\tchr1\t1\t10\tTE2\t11\t17\t1e-20\t30\t90.2"
        self._path.setFromTuple( line.split("\t") )
        self.assertEqual( self._path.id, 1 )
        self.assertEqual( self._path.range_query.seqname, "chr1" )
        self.assertEqual( self._path.range_query.start, 1 )
        self.assertEqual( self._path.range_query.end, 10 )
        self.assertEqual( self._path.range_subject.seqname, "TE2" )
        self.assertEqual( self._path.range_subject.start, 11 )
        self.assertEqual( self._path.range_subject.end, 17 )
        self.assertEqual( self._path.e_value, float("1e-20") )
        self.assertEqual( self._path.score, float("30") )
        self.assertEqual( self._path.identity, float("90.2") )
        
    def test___eq__( self ):
        self._path.setFromString( "1\tchr1\t1\t6\tTE2\t11\t16\t1e-20\t30\t90.2\n" )
        o = Path()
        o.setFromString( "1\tchr1\t1\t6\tTE2\t11\t16\t1e-20\t30\t90.2\n" )
        self.assertEqual( self._path,  o )
        o.setFromString( "2\tchr1\t1\t6\tTE2\t11\t16\t1e-20\t30\t90.2\n" )
        self.assertNotEqual( self._path,  o )
        o.setFromString( "1\tchr1\t1\t6\tTE2\t11\t16\t1e-20\t3000000\t90.2\n" )
        self.assertNotEqual( self._path,  o )
        
    def test_canMerge( self ):
        tuple = ("1", "chr1","1", "10","TE2","11","17","1e-20","30","90.2")
        self._path.setFromTuple(tuple)
        tuple = ("2", "chr1","2", "9","TE2","10","13","1e-20","30","90.2")
        o = Path()
        o.setFromTuple(tuple)
        self.assertTrue(self._path.canMerge(o))
        
    def test_canMerge_on_same_id ( self ): 
        tuple = ("1", "chr1","1", "10","TE2","11","17","1e-20","30","90.2")
        self._path.setFromTuple(tuple)
        tuple = ("1", "chr1","2", "9","TE2","10","13","1e-20","30","90.2")
        o = Path()
        o.setFromTuple(tuple)
        self.assertFalse(self._path.canMerge(o))
        
    def test_canMerge_on_same_chr( self ):     
        tuple = ("1", "chr1","1", "10","TE2","11","17","1e-20","30","90.2")
        self._path.setFromTuple(tuple)
        tuple = ("2", "chr2","2", "9","TE2","10","13","1e-20","30","90.2")
        o = Path()
        o.setFromTuple(tuple)
        self.assertFalse(self._path.canMerge(o))
        
    def test_canMerge_on_diff_subj( self ):      
        tuple = ("1", "chr1","1", "10","TE2","11","17","1e-20","30","90.2")
        self._path.setFromTuple(tuple)
        tuple = ("2", "chr1","2", "9","TE3","10","13","1e-20","30","90.2")
        o = Path()
        o.setFromTuple(tuple)
        self.assertFalse(self._path.canMerge(o)) 
        
    def test_canMerge_on_queries_that_do_not_overlap( self ):
        tuple = ("1", "chr1","5", "11","TE2","11","17","1e-20","30","90.2")
        self._path.setFromTuple(tuple)
        tuple = ("2", "chr1","1", "4","TE2","10","13","1e-20","30","90.2")
        o = Path()
        o.setFromTuple(tuple)
        self.assertFalse(self._path.canMerge(o)) 
        
    def test_canMerge_on_subjects_that_do_not_overlap( self ):    
        tuple = ("1", "chr1","1", "10","TE2","11","17","1e-20","30","90.2")
        self._path.setFromTuple(tuple)
        tuple = ("2", "chr1","2", "9","TE2","1","10","1e-20","30","90.2")
        o = Path()
        o.setFromTuple(tuple)
        self.assertFalse(self._path.canMerge(o))
        
    def test_getSubjectAsSetOfQuery( self ):
        tuple = ("1","chr1","1","10","TE2","11","17","1e-20","30","90.2")
        self._path.setFromTuple(tuple)
        exp = Set(1,"TE2","chr1",1,10)
        obs = self._path.getSubjectAsSetOfQuery()
        self.assertEqual( exp, obs )
        
    def test_getSubjectAsSetOfQuery_on_neg_strand( self ):
        tuple = ("1","chr1","10","1","TE2","11","17","1e-20","30","90.2")
        self._path.setFromTuple(tuple)
        exp = Set(1,"TE2","chr1",10,1)
        obs = self._path.getSubjectAsSetOfQuery()
        self.assertEqual( exp, obs )
        
    def test_toString( self ):
        self._path.setFromString( "1\tchr1\t1\t10\tTE3\t11\t17\t1e-20\t30\t85.2\n" )
        exp = "1\tchr1\t1\t10\tTE3\t11\t17\t{}\t30\t{}".format(1e-20, 85.2)
        obs = self._path.toString()
        self.assertEqual( obs, exp )
        
    def test_getAlignInstance( self ):
        self._path.setFromTuple( ( "2", "chr3", "250", "151", "seq5", "1", "100", "1e-32", "147", "87.9" ) )
        expAlign = Align()
        expAlign.setFromTuple( ( "chr3", "151", "250", "seq5", "100", "1", "1e-32", "147", "87.9" ) )
        obsAlign = self._path.getAlignInstance()
        self.assertEqual( expAlign, obsAlign )
        
        

if __name__ == "__main__":
    test_suite = unittest.TestSuite()
    test_suite.addTest(unittest.makeSuite(Test_Path))
    unittest.TextTestRunner(verbosity=2).run( test_suite )
