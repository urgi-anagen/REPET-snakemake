import unittest
from commons.core.coord.MergedRange import MergedRange
from commons.core.coord.Match import Match

class Test_MergedRange(unittest.TestCase):
    
    def test_eq_True(self):
        mr1 = MergedRange([1], 6, 10)
        mr2 = MergedRange([1], 6, 10)
        self.assertEqual(mr1, mr2)
    
    def test_eq_different_list(self):
        mr1 = MergedRange([1], 6, 10)
        mr2 = MergedRange([1, 2], 6, 10)
        self.assertNotEqual(mr1, mr2)
    
    def test_eq_different_start(self):
        mr1 = MergedRange([1], 5, 10)
        mr2 = MergedRange([1], 6, 10)
        self.assertNotEqual(mr1, mr2)
    
    def test_eq_different_end(self):
        mr1 = MergedRange([1], 6, 10)
        mr2 = MergedRange([1], 6, 11)
        self.assertNotEqual(mr1, mr2)

    def test_isOverlapping_no( self ):
        mr1 = MergedRange([1], 6, 10)
        mr2 = MergedRange([2], 16, 20)
        exp = False
        obs = mr1.isOverlapping( mr2 )
        self.assertEqual( exp, obs )
        
    def test_isOverlapping_yes( self ):
        mr1 = MergedRange([1], 6, 10)
        mr2 = MergedRange([2], 5, 20)
        exp = True
        obs = mr1.isOverlapping( mr2 )
        self.assertEqual( exp, obs )

    def test_isOverlapping_range1_before_range2( self ):
        mr1 = MergedRange([1], 6, 10)
        mr2 = MergedRange([2], 8, 15)
        exp = True
        obs = mr1.isOverlapping( mr2 )
        self.assertEqual( exp, obs )
        
    def test_isOverlapping_range1_after_range2( self ):
        mr1 = MergedRange([1], 6, 10)
        mr2 = MergedRange([2], 1, 8)
        exp = True
        obs = mr1.isOverlapping( mr2 )
        self.assertEqual( exp, obs )
        
    def test_isOverlapping_range1_equal_range2( self ):
        mr1 = MergedRange([1], 6, 10)
        mr2 = MergedRange([2], 6, 10)
        exp = True
        obs = mr1.isOverlapping( mr2 )
        self.assertEqual( exp, obs )
    
    def test_merge_mr1_with_mr2(self):
        otherMergedRange = MergedRange()
        otherMergedRange._lId.append(3)
        otherMergedRange._start = 1
        otherMergedRange._end = 10
        
        mr1 = MergedRange()
        mr1._lId.append(1)
        mr1._start = 6
        mr1._end = 10
        
        mr2 = MergedRange([2], 1, 15)
        mr1.merge(mr2)
        
        exp = MergedRange([1, 2], 1, 15)
        self.assertEqual(exp, mr1)
        
    def test_merge_mr2_with_mr1(self):
        mr1 = MergedRange([1], 6, 10)
        mr2 = MergedRange([2], 1, 15)
        mr2.merge(mr1)
        exp = MergedRange([1, 2], 1, 15)
        self.assertEqual(exp, mr2)
        
    def test_setFromMatch(self):
        tuple = ("QName", 1, 5, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1)
        iMatch = Match()
        iMatch.setFromTuple(tuple)
        
        expMergedRange = MergedRange([1], 1, 5)
        obsMergedRange = MergedRange()
        obsMergedRange.setFromMatch(iMatch)
        
        self.assertEqual(expMergedRange, obsMergedRange)
    
    def test_getMergedRangeListFromMatchList(self):
        tuple1 = ("QName", 1, 5, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1)
        iMatch1 = Match()
        iMatch1.setFromTuple(tuple1)
        tuple2 = ("QName", 10, 15, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 2)
        iMatch2 = Match()
        iMatch2.setFromTuple(tuple2)
        lMatch = [iMatch1, iMatch2]
        
        explMergedRange = [MergedRange([1], 1, 5), MergedRange([2], 10, 15)]
        obslMergedRange = MergedRange.getMergedRangeListFromMatchList(lMatch)

        self.assertEqual(explMergedRange, obslMergedRange)
    
    def test_getMergedRangeListFromMatchList_empty_list(self):
        lMatch = []
        explMergedRange = []
        obslMergedRange = MergedRange.getMergedRangeListFromMatchList(lMatch)

        self.assertEqual(explMergedRange, obslMergedRange)
        
if __name__ == "__main__":
    unittest.main()