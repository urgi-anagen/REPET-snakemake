# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.


import unittest
from commons.core.coord.Match import Match
from commons.core.coord.Path import Path


class Test_Match( unittest.TestCase ):
    
    def test_eq_match_equals( self ):
        tuple1 = ("QName", 1, 5, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1)
        match1 = Match()
        match1.setFromTuple(tuple1)
        tuple2 = ("QName", 1, 5, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1)
        match2 = Match()
        match2.setFromTuple(tuple2)
        self.assertEqual( match1, match2 )
        
    def test_eq_match_not_equals_query_name( self ):
        tuple1 = ("Name", 1, 5, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1)
        match1 = Match()
        match1.setFromTuple(tuple1)
        tuple2 = ("QName", 1, 5, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1)
        match2 = Match()
        match2.setFromTuple(tuple2)
        self.assertNotEqual( match1, match2 )
        
    def test_eq_match_not_equals_query_start( self ):
        tuple1 = ("QName", 2, 5, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1)
        match1 = Match()
        match1.setFromTuple(tuple1)
        tuple2 = ("QName", 1, 5, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1)
        match2 = Match()
        match2.setFromTuple(tuple2)
        self.assertNotEqual( match1, match2 )
        
    def test_eq_match_not_equals_query_end( self ):
        tuple1 = ("QName", 1, 6, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1)
        match1 = Match()
        match1.setFromTuple(tuple1)
        tuple2 = ("QName", 1, 5, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1)
        match2 = Match()
        match2.setFromTuple(tuple2)
        self.assertNotEqual( match1, match2 )
        
    def test_eq_match_not_equals_query_length( self ):
        tuple1 = ("QName", 1, 5, 6, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1)
        match1 = Match()
        match1.setFromTuple(tuple1)
        tuple2 = ("QName", 1, 5, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1)
        match2 = Match()
        match2.setFromTuple(tuple2)
        self.assertNotEqual( match1, match2 )
        
    def test_eq_match_not_equals_query_length_perc( self ):
        tuple1 = ("QName", 1, 5, 5, 0.15, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1)
        match1 = Match()
        match1.setFromTuple(tuple1)
        tuple2 = ("QName", 1, 5, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1)
        match2 = Match()
        match2.setFromTuple(tuple2)
        self.assertNotEqual( match1, match2 )
        
    def test_eq_match_not_equals_match_length_perc( self ):
        tuple1 = ("QName", 1, 5, 5, 0.1, 0.25, "SName", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1)
        match1 = Match()
        match1.setFromTuple(tuple1)
        tuple2 = ("QName", 1, 5, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1)
        match2 = Match()
        match2.setFromTuple(tuple2)
        self.assertNotEqual( match1, match2 )
        
    def test_eq_match_not_equals_subject_name( self ):
        tuple1 = ("QName", 1, 5, 5, 0.1, 0.2, "Name", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1)
        match1 = Match()
        match1.setFromTuple(tuple1)
        tuple2 = ("QName", 1, 5, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1)
        match2 = Match()
        match2.setFromTuple(tuple2)
        self.assertNotEqual( match1, match2 )
        
    def test_eq_match_not_equals_subject_start( self ):
        tuple1 = ("QName", 1, 5, 5, 0.1, 0.2, "SName", 6, 25, 20, 0.15, 1e-20, 15, 87.2, 1)
        match1 = Match()
        match1.setFromTuple(tuple1)
        tuple2 = ("QName", 1, 5, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1)
        match2 = Match()
        match2.setFromTuple(tuple2)
        self.assertNotEqual( match1, match2 )
        
    def test_eq_match_not_equals_subject_end( self ):
        tuple1 = ("QName", 1, 5, 5, 0.1, 0.2, "SName", 5, 26, 20, 0.15, 1e-20, 15, 87.2, 1)
        match1 = Match()
        match1.setFromTuple(tuple1)
        tuple2 = ("QName", 1, 5, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1)
        match2 = Match()
        match2.setFromTuple(tuple2)
        self.assertNotEqual( match1, match2 )
        
    def test_eq_match_not_equals_subject_length( self ):
        tuple1 = ("QName", 1, 5, 5, 0.1, 0.2, "SName", 5, 25, 21, 0.15, 1e-20, 15, 87.2, 1)
        match1 = Match()
        match1.setFromTuple(tuple1)
        tuple2 = ("QName", 1, 5, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1)
        match2 = Match()
        match2.setFromTuple(tuple2)
        self.assertNotEqual( match1, match2 )
        
    def test_eq_match_not_equals_subject_length_perc( self ):
        tuple1 = ("QName", 1, 5, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.16, 1e-20, 15, 87.2, 1)
        match1 = Match()
        match1.setFromTuple(tuple1)
        tuple2 = ("QName", 1, 5, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1)
        match2 = Match()
        match2.setFromTuple(tuple2)
        self.assertNotEqual( match1, match2 )
        
    def test_eq_match_not_equals_subject_e_value( self ):
        tuple1 = ("QName", 1, 5, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-21, 15, 87.2, 1)
        match1 = Match()
        match1.setFromTuple(tuple1)
        tuple2 = ("QName", 1, 5, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1)
        match2 = Match()
        match2.setFromTuple(tuple2)
        self.assertNotEqual( match1, match2 )
        
    def test_eq_match_not_equals_subject_score( self ):
        tuple1 = ("QName", 1, 5, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 16, 87.2, 1)
        match1 = Match()
        match1.setFromTuple(tuple1)
        tuple2 = ("QName", 1, 5, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1)
        match2 = Match()
        match2.setFromTuple(tuple2)
        self.assertNotEqual( match1, match2 )
        
    def test_eq_match_not_equals_subject_identity( self ):
        tuple1 = ("QName", 1, 5, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 15, 85.2, 1)
        match1 = Match()
        match1.setFromTuple(tuple1)
        tuple2 = ("QName", 1, 5, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1)
        match2 = Match()
        match2.setFromTuple(tuple2)
        self.assertNotEqual( match1, match2 )
        
    def test_eq_match_not_equals_subject_id( self ):
        tuple1 = ("QName", 1, 5, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 2)
        match1 = Match()
        match1.setFromTuple(tuple1)
        tuple2 = ("QName", 1, 5, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1)
        match2 = Match()
        match2.setFromTuple(tuple2)
        self.assertNotEqual( match1, match2 )
        
    def test_setFromTuple_direct_strand( self ):
        tuple = ("QName", 1, 5, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1)
        obsMatch = Match()
        obsMatch.setFromTuple(tuple)
        expMatch = Match()
        expMatch.range_query.seqname = "QName"
        expMatch.range_query.start = 1
        expMatch.range_query.end = 5
        expMatch.query_length = 5
        expMatch.query_length_perc = 0.1
        expMatch.match_length_perc = 0.2
        expMatch.range_subject.seqname = "SName"
        expMatch.range_subject.start = 5
        expMatch.range_subject.end = 25
        expMatch.subject_length = 20
        expMatch.subject_length_perc = 0.15
        expMatch.e_value = 1e-20
        expMatch.score = 15
        expMatch.identity = 87.2
        expMatch.id = 1
        expMatch.subject_seqlength = int( expMatch.subject_length / expMatch.subject_length_perc )
        expMatch.query_seqlength = int( expMatch.query_length / expMatch.query_length_perc )
        self.assertEqual( expMatch, obsMatch )
   
    def test_setFromTuple_reverse_strand_on_subject( self ):
        tuple = ("QName", 1, 5, 5, 0.1, 0.2, "SName", 25, 5, 20, 0.15, 1e-20, 15, 87.2, 1)
        obsMatch = Match()
        obsMatch.setFromTuple(tuple)
        expMatch = Match()
        expMatch.range_query.seqname = "QName"
        expMatch.range_query.start = 1
        expMatch.range_query.end = 5
        expMatch.query_length = 5
        expMatch.query_length_perc = 0.1
        expMatch.match_length_perc = 0.2
        expMatch.range_subject.seqname = "SName"
        expMatch.range_subject.start = 25
        expMatch.range_subject.end = 5
        expMatch.subject_length = 20
        expMatch.subject_length_perc = 0.15
        expMatch.e_value = 1e-20
        expMatch.score = 15
        expMatch.identity = 87.2
        expMatch.id = 1
        expMatch.subject_seqlength = int( expMatch.subject_length / expMatch.subject_length_perc )
        expMatch.query_seqlength = int( expMatch.query_length / expMatch.query_length_perc )
        self.assertEqual( expMatch, obsMatch )
        
    def test_setFromTuple_reverse_strand_on_query( self ):
        tuple = ("QName", 5, 1, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1)
        obsMatch = Match()
        obsMatch.setFromTuple(tuple)
        expMatch = Match()
        expMatch.range_query.seqname = "QName"
        expMatch.range_query.start = 1
        expMatch.range_query.end = 5
        expMatch.query_length = 5
        expMatch.query_length_perc = 0.1
        expMatch.match_length_perc = 0.2
        expMatch.range_subject.seqname = "SName"
        expMatch.range_subject.start = 25
        expMatch.range_subject.end = 5
        expMatch.subject_length = 20
        expMatch.subject_length_perc = 0.15
        expMatch.e_value = 1e-20
        expMatch.score = 15
        expMatch.identity = 87.2
        expMatch.id = 1
        expMatch.subject_seqlength = int( expMatch.subject_length / expMatch.subject_length_perc )
        expMatch.query_seqlength = int( expMatch.query_length / expMatch.query_length_perc )
        self.assertEqual( expMatch, obsMatch )
        
    def test_setFromTuple_reverse_strand_on_query_and_subject( self ):
        tuple = ("QName", 5, 1, 5, 0.1, 0.2, "SName", 25, 5, 20, 0.15, 1e-20, 15, 87.2, 1)
        obsMatch = Match()
        obsMatch.setFromTuple(tuple)
        expMatch = Match()
        expMatch.range_query.seqname = "QName"
        expMatch.range_query.start = 1
        expMatch.range_query.end = 5
        expMatch.query_length = 5
        expMatch.query_length_perc = 0.1
        expMatch.match_length_perc = 0.2
        expMatch.range_subject.seqname = "SName"
        expMatch.range_subject.start = 5
        expMatch.range_subject.end = 25
        expMatch.subject_length = 20
        expMatch.subject_length_perc = 0.15
        expMatch.e_value = 1e-20
        expMatch.score = 15
        expMatch.identity = 87.2
        expMatch.id = 1
        expMatch.subject_seqlength = int( expMatch.subject_length / expMatch.subject_length_perc )
        expMatch.query_seqlength = int( expMatch.query_length / expMatch.query_length_perc )
        self.assertEqual( expMatch, obsMatch )
        
    def test_toString( self ):        
        tuple = ("QName", 1, 5, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1)
        match = Match()
        match.setFromTuple(tuple)
        expString = "QName\t1\t5\t5\t{}\t{}\tSName\t5\t25\t20\t{}\t{}\t15.0\t{}\t1".format(0.1,0.2,0.15,1e-20, 87.2)
        obsString = match.toString()
        self.assertEqual(expString, obsString)
        
    def test_getPathInstance( self ):
        tuple = ( "QName", 1, 5, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1 )
        match = Match()
        match.setFromTuple( tuple )
        tuple = ( 1, "QName", 1, 5, "SName", 5, 25, 1e-20, 15, 87.2 )
        exp = Path()
        exp.setFromTuple( tuple )
        obs = match.getPathInstance()
        self.assertEqual( exp, obs )
        
    def test_getQryIsIncluded(self):
        tuple = ("QName", 1, 5, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1)
        match = Match()
        match.setFromTuple(tuple)
        expString = "query QName (50 bp: 1-5) is contained in subject SName (133 bp: 5-25): id=87.20 - 0.100 - 0.200 - 0.150"
        obsString = match.getQryIsIncluded()
        self.assertEqual(expString, obsString)
        
    def test_isDoublonWith_Matchs_equals(self):
        tuple1 = ("QName", 1, 5, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1)
        match1 = Match()
        match1.setFromTuple(tuple1)
        tuple2 = ("QName", 1, 5, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1)
        match2 = Match()
        match2.setFromTuple(tuple2)
        self.assertTrue(match1.isDoublonWith(match2))
        
    def test_isDoublonWith_Matchs_unequals_on_MatchNumbers(self):
        tuple1 = ("QName", 1, 5, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1)
        match1 = Match()
        match1.setFromTuple(tuple1)
        tuple2 = ("QName", 1, 5, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 15, 86.2, 1)
        match2 = Match()
        match2.setFromTuple(tuple2)
        self.assertFalse(match1.isDoublonWith(match2))
        
    def test_isDoublonWith_Matchs_unequals_on_SeqNames(self):
        tuple1 = ("QName", 1, 5, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1)
        match1 = Match()
        match1.setFromTuple(tuple1)
        tuple2 = ("QName", 1, 5, 5, 0.1, 0.2, "Name", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1)
        match2 = Match()
        match2.setFromTuple(tuple2)
        self.assertFalse(match1.isDoublonWith(match2))
        
    def test_isDoublonWith_Matchs_unequals_on_Coordinates(self):
        tuple1 = ("QName", 1, 5, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1)
        match1 = Match()
        match1.setFromTuple(tuple1)
        tuple2 = ("QName", 1, 6, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1)
        match2 = Match()
        match2.setFromTuple(tuple2)
        self.assertFalse(match1.isDoublonWith(match2))
        
    def test_isDoublonWith_Reversed_Matchs_equals(self):
        tuple1 = ("QName", 1, 5, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1)
        match1 = Match()
        match1.setFromTuple(tuple1)
        tuple2 = ("SName", 5, 25, 20, 0.15, 0.2, "QName", 1, 5, 5, 0.1, 1e-20, 15, 87.2, 1)
        match2 = Match()
        match2.setFromTuple(tuple2)
        self.assertTrue(match1.isDoublonWith(match2))
        
    def test_isDoublonWith_Reversed_Matchs_unequals(self):
        tuple1 = ("QName", 1, 5, 5, 0.1, 0.2, "SName", 5, 25, 20, 0.15, 1e-20, 15, 87.2, 1)
        match1 = Match()
        match1.setFromTuple(tuple1)
        tuple2 = ("SName", 5, 25, 20, 0.15, 0.2, "QName", 1, 6, 5, 0.1, 1e-20, 15, 87.2, 1)
        match2 = Match()
        match2.setFromTuple(tuple2)
        self.assertFalse(match1.isDoublonWith(match2))
        
        

if __name__ == "__main__":
    test_suite = unittest.TestSuite()
    test_suite.addTest(unittest.makeSuite(Test_Match))
    unittest.TextTestRunner(verbosity=2).run( test_suite )
