import unittest
from commons.core.coord.Path import Path
from commons.core.coord.MergedPath import MergedPath

class Test_MergedPath(unittest.TestCase):
        
#    def test_eq_True(self):
#        iPath1 = Path()
#        iPath1.setFromString("1\tquery\t1\t6\tsubject\t11\t16\t0\t10\t12.34\n")
#        mp1 = MergedPath()
#        mp1.add(iPath1)
#
#        iPath2 = Path()
#        iPath2.setFromString("1\tquery\t1\t6\tsubject\t11\t16\t0\t10\t12.34\n")
#        mp2 = MergedPath()
#        mp2.add(iPath2)
#
#        self.assertEquals(mp1, mp2)
#        self.assertTrue(mp1 == mp2)
#        self.assertFalse(mp1 != mp2)
#        
#    def test_eq_False(self):
#        iPath1 = Path()
#        iPath1.setFromString("1\tquery\t1\t6\tsubject\t11\t16\t0\t10\t12.34\n")
#        mp1 = MergedPath()
#        mp1.add(iPath1)
#  
#        iPath2 = Path()
#        iPath2.setFromString("2\tquery\t7\t10\tsubject\t17\t20\t0\t20\t23.45\n")
#        mp2 = MergedPath()
#        mp2.add(iPath2)
#          
#        self.assertNotEquals(mp1, mp2)
#        self.assertTrue(mp1 != mp2)
#        self.assertFalse(mp1 == mp2)
         
    def test_eq_True_unSorted(self):
        tuplea = ("1","query","1","6","subject","11","16","0","10","12.34")
        tupleb = ("1","query","1","7","subject","17","20","0","20","23.45")
        iPath1 = Path()
        iPath1.setFromTuple(tuplea)
        iPath2 = Path()
        iPath2.setFromTuple(tupleb)
         
        iPath3 = Path()
        iPath3.setFromTuple(tupleb)
        iPath4 = Path()
        iPath4.setFromTuple(tuplea)
        
        mp1 = MergedPath()
        mp1.add(iPath1)
        mp1.add(iPath2)
        mp2 = MergedPath()
        mp2.add(iPath3)
        mp2.add(iPath4)
 
        self.assertEqual(mp1, mp2)
         
    def test_eq_False_multi(self):
        iPath1 = Path()
        iPath1.setFromString("1\tquery\t1\t6\tsubject\t11\t16\t0\t10\t12.34\n")
        iPath2 = Path()
        iPath2.setFromString("1\tquery\t7\t10\tsubject\t17\t20\t0\t20\t23.45\n")
         
        iPath3 = Path()
        iPath3.setFromString("1\tquery\t1\t6\tsubject\t11\t16\t0\t10\t12.34\n")
        iPath4 = Path()
        iPath4.setFromString("1\tquery\t7\t10\tsubject\t17\t20\t0\t20\t23.45\n")
        
        import copy
        mp1 = MergedPath()
        mp1.add(copy.deepcopy(iPath2))
        mp1.add(iPath1)
        mp1.add(iPath2)
        mp2 = MergedPath()
        mp2.add(iPath4)
        mp2.add(iPath3)
 
        self.assertNotEqual(mp1, mp2)
        
    def test_eq_True_empty_MergedPaths(self):
        mp1 = MergedPath()
        mp2 = MergedPath()
 
        self.assertEqual(mp1, mp2)
        self.assertTrue(mp1 == mp2)
        self.assertFalse(mp1 != mp2)
         
    def test_eq_False_compare_to_None(self):
        mp1 = MergedPath()
        mp2 = None
 
        self.assertNotEqual(mp1, mp2)
        self.assertTrue(mp1 != mp2)
        self.assertFalse(mp1 == mp2)
         
    def test_eq_False_compare_to_String(self):
        mp1 = MergedPath()
        mp2 = "M57885161 is PRIME!"
 
        self.assertNotEqual(mp1, mp2)
        self.assertTrue(mp1 != mp2)
         
    def test_eq_False_compare_to_int(self):
        mp1 = MergedPath()
        mp2 = 42
 
        self.assertNotEqual(mp1, mp2)
        self.assertTrue(mp1 != mp2)
 
    def test_init_withlPaths(self):
        iPath1 = Path()
        iPath1.setFromString("1\tquery\t1\t6\tsubject\t11\t16\t0\t10\t12.34\n")
        iPath2 = Path()
        iPath2.setFromString("1\tquery\t7\t10\tsubject\t17\t20\t0\t20\t23.45\n")
        lPaths = [iPath1, iPath2]
 
        mp = MergedPath(lPaths)
 
        self.assertTrue(mp.getID() == 1 and mp.getNbPaths() == 2 and mp.getLength() == 10)
 
    def test_add_pathsWithSameID(self):
        iPath1 = Path()
        iPath1.setFromString("1\tquery\t1\t6\tsubject\t11\t16\t0\t10\t12.34\n")
        iPath2 = Path()
        iPath2.setFromString("1\tquery\t7\t10\tsubject\t17\t20\t0\t20\t23.45\n")
        lPaths = [iPath1, iPath2]
        mp = MergedPath()
 
        mp.add(iPath1)
        mp.add(iPath2)
 
        self.assertEqual(mp.getPathsList(), lPaths)
 
    def test_add_pathsWithDiffID(self):
        iPath1 = Path()
        iPath1.setFromString("1\tquery\t1\t6\tsubject\t11\t16\t0\t10\t12.34\n")
        iPath2 = Path()
        iPath2.setFromString("2\tquery\t7\t10\tsubject\t17\t20\t0\t20\t23.45\n")
        iPath3 = Path()
        iPath3.setFromString("1\tquery\t1\t6\tsubject\t11\t16\t0\t10\t12.34\n")
        lPaths = [iPath1, iPath2, iPath3]
 
        mp = MergedPath(verbosity = 2)
        for p in lPaths:
            mp.add(p)
 
        self.assertTrue(mp.getID() == 1 and mp.getNbPaths() == 2)
 
    def test_add_notAPathObject(self):
        notAPath = "This is not a Path object."
        mp = MergedPath(verbosity = 2)
        mp.add(notAPath)
 
        self.assertEqual(mp, MergedPath())
 
    def test_getLength(self):
        iPath1 = Path()
        iPath1.setFromString("1\tquery\t1\t6\tsubject\t11\t16\t0\t10\t12.34\n")
        iPath2 = Path()
        iPath2.setFromString("1\tquery\t7\t10\tsubject\t17\t20\t0\t20\t23.45\n")
        mp = MergedPath()
 
        mp.add(iPath1)
        self.assertEqual(mp.getLength(), 6)
 
        mp.add(iPath2)
        self.assertEqual(mp.getLength(), 10)
         
    def test_getWeightedIdentity(self):
        iPath1 = Path()
        iPath1.setFromString("1\tquery\t1\t200\tsubject\t11\t16\t0\t10\t100\n")
        iPath2 = Path()
        iPath2.setFromString("1\tquery\t201\t300\tsubject\t17\t20\t0\t20\t70\n")
        mp = MergedPath([iPath1, iPath2])
         
        exp = 90.0 # (200 * 100 + 100 * 70) / (200 + 100)
        obs = mp.getWeightedIdentity()
        self.assertEqual(obs, exp)
         
    def test_getWeightedIdentity_reverse(self):
        iPath1 = Path()
        iPath1.setFromString("1\tquery\t200\t1\tsubject\t11\t16\t0\t10\t100\n")
        iPath2 = Path()
        iPath2.setFromString("1\tquery\t300\t201\tsubject\t17\t20\t0\t20\t70\n")
        mp = MergedPath([iPath1, iPath2])
         
        exp = 90.0 # (200 * 100 + 100 * 70) / (200 + 100)
        obs = mp.getWeightedIdentity()
        self.assertEqual(obs, exp)
 
    def test_toString(self):
        iPath1 = Path()
        iPath1.setFromString("1\tquery\t1\t6\tsubject\t11\t16\t0\t10\t12.34\n")
        iPath2 = Path()
        iPath2.setFromString("1\tquery\t21\t26\tsubject\t31\t36\t0\t10\t12.34\n")
        mp = MergedPath()
 
        exp = "Empty MergedPath()"
        obs = mp.toString()
        self.assertEqual(obs, exp)
 
        mp.add(iPath1)
        exp = "ID: 1\tLength: 6\tNbPaths: 1\tPaths:\n1\tquery\t1\t6\tsubject\t11\t16\t{}\t10\t{}".format(0, 12.34)
        obs = mp.toString()
        self.assertEqual(obs, exp)
 
        mp.add(iPath2)
        exp = "ID: 1\tLength: 12\tNbPaths: 2\tPaths:\n1\tquery\t1\t6\tsubject\t11\t16\t{}\t10\t{}\n1\tquery\t21\t26\tsubject\t31\t36\t{}\t10\t{}".format(0, 12.34, 0, 12.34)
        obs = mp.toString()
        self.assertEqual(obs, exp)
        
if __name__ == "__main__":
    unittest.main()