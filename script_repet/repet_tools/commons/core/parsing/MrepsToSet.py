from commons.core.parsing.FindRep import FindRep
from xml.sax import make_parser
from xml.sax.handler import feature_namespaces
import os


class MrepsToSet(object):

    def __init__(self, mrepsInputFileName="", mrepsOuputFileName="", outputFileName=None, errorFilter=0):
        self._mrepsInputFileName = mrepsInputFileName
        self._mrepsOuputFileName = mrepsOuputFileName
        self._outputFileName = outputFileName or "{}.Mreps.set".format(mrepsOuputFileName )
        self._errorFilter = errorFilter
        
    def run(self):
        xmlParser = make_parser()
        xmlParser.setFeature( feature_namespaces, 0 )
        xmlParser.setContentHandler( FindRep( self._outputFileName, self._errorFilter, 0 ) )
        xmlParser.parse( self._mrepsOuputFileName )

    def clean(self):
        """
        Remove the output file (xml) from Mreps to keep only the 'set' file.
        """
        if os.path.exists(self._mrepsOuputFileName):
            os.remove(self._mrepsOuputFileName)