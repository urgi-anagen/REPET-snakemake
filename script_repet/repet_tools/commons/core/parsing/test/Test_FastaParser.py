import os
import unittest
from commons.core.parsing.FastaParser import FastaParser
from SMART.Java.Python.structure.Sequence import Sequence

class Test_FastaParser(unittest.TestCase):
    
    def test_getSubsequence(self):
        fastaFile = "myFastaInput.fasta"
        self._writeInputFastaFile(fastaFile)
        parser = FastaParser(fastaFile)
        chromosome = "1"
        expSeq = Sequence("1:1-20 (1)", "CCTAAGCCATTGCTTGGTGA")
        obsSeq = parser.getSubSequence(chromosome, 1, 20, 1)
        try:
            os.remove(fastaFile)
        except:
            pass
        self.assertEqual(expSeq, obsSeq)

    def test_getSubsequence_long_sequence(self):
        fastaFile = "myFastaInput.fasta"
        self._writeInputFastaFile(fastaFile)
        parser = FastaParser(fastaFile)
        chromosome = "2"
        expSeq = Sequence("subsequence", "TGAAGA")
        obsSeq = parser.getSubSequence(chromosome, 55, 60, 1, "subsequence")
        try:
            os.remove(fastaFile)
        except:
            pass
        self.assertEqual(expSeq, obsSeq)

    def test_getSubsequence_long_sequence_inside_and_outside(self):
        fastaFile = "myFastaInput.fasta"
        self._writeInputFastaFile(fastaFile)
        parser = FastaParser(fastaFile)
        chromosome = "2"
        expSeq = Sequence("subsequence", "TTA")
        obsSeq = parser.getSubSequence(chromosome, 137, 151, 1, "subsequence")
        try:
            os.remove(fastaFile)
        except:
            pass
        self.assertEqual(expSeq, obsSeq)

    def test_getSubsequence_long_sequence_last_letter(self):
        fastaFile = "myFastaInput.fasta"
        self._writeInputFastaFile(fastaFile)
        parser = FastaParser(fastaFile)
        chromosome = "2"
        expSeq = Sequence("subsequence", "A")
        obsSeq = parser.getSubSequence(chromosome, 139, 151, 1, "subsequence")
        try:
            os.remove(fastaFile)
        except:
            pass
        self.assertEqual(expSeq, obsSeq)

    def test_getSubsequence_long_sequence_totally_outside(self):
        fastaFile = "myFastaInput.fasta"
        self._writeInputFastaFile(fastaFile)
        parser = FastaParser(fastaFile)
        chromosome = "2"
        isSysExit = False
        try:
            parser.getSubSequence(chromosome, 140, 151, 1, "subsequence")
        except:
            isSysExit = True
        try:
            os.remove(fastaFile)
        except:
            pass
        self.assertTrue(isSysExit)

    def test_parseOne_empty_file(self):
        fastaFile = "myFastaInput.fasta"
        open(fastaFile, "w").close()
        parser = FastaParser(fastaFile)
        obsSeq = parser.parseOne()
        try:
            os.remove(fastaFile)
        except:
            pass
        self.assertTrue(obsSeq.name is None)

    def test_setTags(self):
        fastaFile = "myFastaInput.fasta"
        self._writeInputFastaFile(fastaFile)
        parser = FastaParser(fastaFile)
        parser.setTags()
        expTags = {"1" : 0,
                   "2" : 54}
        obsTags = parser.getTags()
        try:
            os.remove(fastaFile)
        except:
            pass
        self.assertEqual(expTags, obsTags)

    def _writeInputFastaFile(self, fastaFile):
        with open(fastaFile, 'w') as myHandler:
            myHandler.write(">1\n")
            myHandler.write("CCTAAGCCATTGCTTGGTGATTATGAAGGCAGTAGTCAAACCTCCACAAT\n")
            myHandler.write(">2\n")
            myHandler.write("TATGAGATCAACAGGGGCTTTGCTAGCCTGAGGGCGATTGGTCAAGGCCG\n")
            myHandler.write("GACCTGAAGAAATTCCTGATTGTACGTTCTGGTTACTCTTCAATTTGGGC\n")
            myHandler.write("TGCTTAATTATCTCCTCAATTTCAATTTGGCCATGCTTA\n")

if __name__ == "__main__":
    unittest.main()
