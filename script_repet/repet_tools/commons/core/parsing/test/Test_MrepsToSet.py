import unittest
import os
from commons.core.utils.FileUtils import FileUtils
from commons.core.parsing.MrepsToSet import MrepsToSet

class Test_MrepsToSet(unittest.TestCase):
    def setUp(self):
        self._mrepsInputFileName = "mrepsInput.fa"
        self._mrepsOuputFileName = "mrepsOutput.xml"
        self._obsSetFileName = "obsOuput.set"
        self._expSetFileName = "expOuput.set"
        
        self._writeExpSet(self._expSetFileName)
        self._writeMrepsOutput(self._mrepsOuputFileName)
 
    def tearDown(self):
        os.remove(self._expSetFileName)
        os.remove(self._obsSetFileName)
        os.remove(self._mrepsOuputFileName)
    
    def test_convert(self):
        iMrepsToSet = MrepsToSet(self._mrepsInputFileName, self._mrepsOuputFileName, self._obsSetFileName)
        iMrepsToSet.run()
        self.assertTrue(FileUtils.are2FilesIdentical(self._obsSetFileName, self._expSetFileName))  
    
    def _writeExpSet(self, fileName):
        with open(fileName, "w") as f:
            f.write("1\t(tatt)3\tseq1\t4\t16\n")
            f.write("2\t(tatt)3\tseq1\t23\t35\n")
            f.write("3\t(tatt)3\tseq1\t42\t54\n")

        
    def _writeMrepsOutput(self, fileName):
        with open(fileName, "w") as f:
            f.write("<?xml version='1.0' encoding='UTF-8' ?>\n")
            f.write("<mreps>\n")
            f.write("<time>Thu Dec  1 17:25:54 2011\n")
            f.write("</time>\n")
            f.write("<parameters>\n")
            f.write("    <type-of-input>file in fasta format</type-of-input>\n")
            f.write("    <err>3</err>\n")
            f.write("    <from>1</from>\n")
            f.write("    <to>-1</to>\n")
            f.write("    <win>-1</win>\n")
            f.write("    <minsize>1</minsize>\n")
            f.write("    <maxsize>-1</maxsize>\n")
            f.write("    <minperiod>1</minperiod>\n")
            f.write("   <maxperiod>-1</maxperiod>\n")
            f.write("   <minexponent>3.00</minexponent>\n")
            f.write("</parameters>\n")
            f.write("<results>\n")
            f.write("<sequence-name>seq1</sequence-name>\n")
            f.write("<repetitions>\n")
            f.write("<window>\n")
            f.write("<windowstart>1</windowstart>\n")
            f.write("<windowend>60</windowend>\n")
            f.write("    <repeat>\n")
            f.write("        <start>4</start>\n")
            f.write("        <end>16</end>\n")
            f.write("        <length>13</length>\n")
            f.write("       <period>4</period>\n")
            f.write("       <exponent>3.25</exponent>\n")
            f.write("        <score>0.000</score>\n")
            f.write("        <sequence>\n")
            f.write("            <unit>tatt</unit>\n")
            f.write("            <unit>tatt</unit>\n")
            f.write("            <unit>tatt</unit>\n")
            f.write("            <unit>t</unit>\n")
            f.write("        </sequence>\n")
            f.write("    </repeat>\n")
            f.write("    <repeat>\n")
            f.write("        <start>23</start>\n")
            f.write("        <end>35</end>\n")
            f.write("        <length>13</length>\n")
            f.write("        <period>4</period>\n")
            f.write("        <exponent>3.25</exponent>\n")
            f.write("        <score>0.000</score>\n")
            f.write("        <sequence>\n")
            f.write("            <unit>tatt</unit>\n")
            f.write("            <unit>tatt</unit>\n")
            f.write("            <unit>tatt</unit>\n")
            f.write("            <unit>t</unit>\n")
            f.write("        </sequence>\n")
            f.write("    </repeat>\n")
            f.write("    <repeat>\n")
            f.write("        <start>42</start>\n")
            f.write("       <end>54</end>\n")
            f.write("        <length>13</length>\n")
            f.write("        <period>4</period>\n")
            f.write("        <exponent>3.25</exponent>\n")
            f.write("        <score>0.000</score>\n")
            f.write("        <sequence>\n")
            f.write("            <unit>tatt</unit>\n")
            f.write("            <unit>tatt</unit>\n")
            f.write("            <unit>tatt</unit>\n")
            f.write("            <unit>t</unit>\n")
            f.write("        </sequence>\n")
            f.write("    </repeat>\n")
            f.write("<nbofreps>3</nbofreps>\n")
            f.write("</window>\n")
            f.write("</repetitions>\n")
            f.write("</results>\n")
            f.write("<errorcode>0</errorcode>\n")
            f.write("</mreps>\n")


if __name__ == "__main__":
    unittest.main()