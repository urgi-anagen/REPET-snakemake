#!/usr/bin/env python

"""
Class overriding optparse.OptionParser default epilog formatter.
The resulting epilog display format is the same as if the corresponding string was printed. 
"""

# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

from optparse import OptionParser
from optparse import BadOptionError
from optparse import OptionValueError
SUPPRESS_USAGE = "SUPPRESS"+"USAGE"

class RepetOptionParser(OptionParser):

    def parse_args(self, args=None, values=None):
        rargs = self._get_args(args)
        if not rargs:
            rargs = ["-h"]
        if values is None:
            values = self.get_default_values()
        self.rargs = rargs
        self.largs = largs = [] 
        self.values = values
        try: 
            self._process_args(largs, rargs, values)
        except (BadOptionError, OptionValueError) as err:
            self.error(str(err))
        args = largs + rargs
        return self.check_values(values, args)

    def set_usage(self, usage):
        if not usage or usage is SUPPRESS_USAGE:
            self.usage = None
        elif usage.lower().startswith("usage: "):
            self.usage = usage[7:]
        else:
            self.usage = usage
    
    def format_epilog(self, formatter):
        if self.epilog != None:
            return self.epilog
        else:
            return ""
    
    def format_description(self, formatter):
        if self.description != None:
            return "Description: {}".format(self.description)
        else:
            return ""
