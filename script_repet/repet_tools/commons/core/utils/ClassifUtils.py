# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import os
import json
from collections import OrderedDict
from commons.core.utils.Classif import Classif
from commons.core.utils.FileUtils import FileUtils


DWICKERCODE = {
        "I" : "RXX", 
        "II" : "DXX", 
        "LTR" : "RLX", 
        "DIRS" : "RYX", 
        "PLE" : "RPX", 
        "LINE" : "RIX", 
        "SINE" : "RSX",
        "TIR" : "DTX", 
        "Crypton" : "DYX", 
        "Helitron" : "DHX", 
        "Maverick" : "DMX",
        "LARD" : "RXX-LARD",
        "MITE" : "DXX-MITE",
        "TRIM": "RXX-TRIM"
       }
DORDER2CLASS = {
            "LTR": "I",
            "DIRS": "I",
            "PLE": "I",
            "LINE": "I",
            "SINE": "I",
            "TIR": "II",
            "Crypton": "II",
            "Maverick": "II",
            "Helitron": "II",
            "LARD" : "I",
            "MITE": "II",
            "TRIM": "I",
            "NA": "NA"
            }
    
class ClassifUtils(object):

    @staticmethod
    def formatProfilesResultsAsDict(lProfilesResults):
        if len(lProfilesResults) == 0:
            return OrderedDict()
        dResults = OrderedDict()
        for refNameAndCoverage in lProfilesResults:
            refName, coverage = refNameAndCoverage.split(": ")
            profilesResult = OrderedDict()
            if coverage.find("%(") != -1:
                coverage, coverageOnSubject = coverage.split("%(")
                profilesResult["cov"] = coverage
                profilesResult["covOnSubject"] = coverageOnSubject.replace("%)", "")
            else:
                profilesResult["cov"] = coverage.replace("%","")
                profilesResult["covOnSubject"] = ""
            dResults[refName] = profilesResult
        return dResults
    
    
    # @staticmethod
    # def _formatCodingFeaturesAsDict(lineOfEvidence, dCoding):
    #     codingEvidences = lineOfEvidence.split("; ")
    #     for codingTypeData in codingEvidences:
    #         codingTypeData = codingTypeData.split(": ")
    #         codingType = codingTypeData.pop(0)
    #
    #         codingTypeData = ": ".join(codingTypeData)
    #         codingTypeData = codingTypeData.split(", ")
    #
    #         if codingType == "TE_BLRtx":
    #             if not "TE_BLRtx" in dCoding.keys():
    #                 dCoding["TE_BLRtx"] = OrderedDict()
    #             for refNameAndCoverage in codingTypeData:
    #                 blrtxResult = OrderedDict()
    #                 refName, coverage = refNameAndCoverage.rsplit(": ", 1)
    #                 blrtxResult["cov"] = coverage.replace("%", "")
    #                 dCoding["TE_BLRtx"][refName] = blrtxResult
    #
    #         if codingType == "TE_BLRx":
    #             if not "TE_BLRx" in dCoding.keys():
    #                 dCoding["TE_BLRx"] = OrderedDict()
    #             for refNameAndCoverage in codingTypeData:
    #                 blrxResult = OrderedDict()
    #                 refName, coverage = refNameAndCoverage.rsplit(": ", 1)
    #                 blrxResult["cov"] = coverage.replace("%", "")
    #                 dCoding["TE_BLRx"][refName] = blrxResult
    #
    #         if codingType == "profiles":
    #             dCoding["profiles"] = ClassifUtils.formatProfilesResultsAsDict(codingTypeData)
    #
    #         if codingType == "Other_profiles":
    #             dCoding["Other_profiles"] = ClassifUtils.formatProfilesResultsAsDict(codingTypeData)
    #
    #         if codingType == "rDNA_BLRn":
    #             dCoding["rDNA_BLRn"] = OrderedDict()
    #             codingTypeData = ", ".join(codingTypeData)
    #             try:
    #                 refName, coverage = codingTypeData.rsplit(": ", 1)
    #             except ValueError:
    #                 refName = codingTypeData
    #                 coverage = -1.0
    #
    #             dCoding["rDNA_BLRn"]["name"] = refName
    #             dCoding["rDNA_BLRn"]["cov"] = coverage.replace("%", "")
    #
    #         if codingType == "HG_BLRn":
    #             dCoding["HG_BLRn"] = OrderedDict()
    #             refName, coverage = codingTypeData[0].rsplit(": ", 1)
    #
    #             dCoding["HG_BLRn"]["name"] = refName
    #             dCoding["HG_BLRn"]["cov"] = coverage.replace("%", "")
    
    # @staticmethod
    # def _formatStructFeaturesAsDict(lineOfEvidence, dStruct):
    #     structEvidences = lineOfEvidence.split("; ")
    #     for structTypeData in structEvidences:
    #         structTypeData = structTypeData.split(": ")
    #         structType = structTypeData.pop(0)
    #
    #         structTypeData = ": ".join(structTypeData)
    #         structTypeData = structTypeData.split(", ")
    #
    #         if structType == "TElength":
    #             dStruct["TElength"] = structTypeData.pop()
    #
    #         if structType == "TermRepeats":
    #             dStruct["TermRepeats"] = OrderedDict()
    #             for refNameAndLength in structTypeData:
    #                 refName, length = refNameAndLength.rsplit(": ", 1)
    #                 dStruct["TermRepeats"][refName] = int(length)
    #
    #         if structType == "ORF":
    #             if not "ORF" in dStruct.keys():
    #                 dStruct["ORF"] = structTypeData
    #
    #         if structType in ["SSR", "SSRtrf"]:
    #             if not structType in dStruct.keys():
    #                 dStruct[structType] = structTypeData
    #
    #         if "SSRCoverage" in structType :
    #             dummy, cov = structType.split("=")
    #             dStruct["SSRCoverage"] = cov
    #
    #         if structType == "polyAtail":
    #             dStruct["polyAtail"] = True
    #
    #         if structType == "helitronExtremities":
    #             structTypeData = ", ".join(structTypeData)
    #             structTypeData = structTypeData.split("), ")
    #             dStruct["helitronExtremities"] = OrderedDict()
    #             for helitronData in structTypeData:
    #                 helName, helData = helitronData.split(": (")
    #                 helData = helData.replace(")", "")
    #                 eValue, start, end = helData.split(", ")
    #
    #                 helitronExtResult = OrderedDict()
    #                 helitronExtResult["start"] = int(start)
    #                 helitronExtResult["end"] = int(end)
    #                 helitronExtResult["eValue"] = eValue
    #                 dStruct["helitronExtremities"][helName] = helitronExtResult
    #
    # @staticmethod
    # def _formatOtherFeaturesAsDict(lineOfEvidence, dOther):
    #     if lineOfEvidence != "":
    #         ClassifUtils._formatCodingFeaturesAsDict(lineOfEvidence, dOther)
    #         ClassifUtils._formatStructFeaturesAsDict(lineOfEvidence, dOther)
    
#     @staticmethod
#     def getClassifLineAsDict(line):
#         dClassif = OrderedDict()
#         iClassif = Classif()
#         lOldClassifItem = line.split("\t")
#         if len(lOldClassifItem) != 8:
#             msg = "Can't parse line: \"{}\"\n" .format (line.strip())
#             print("WARNING - ClassifUtils - {}" .format (msg))
#             return dClassif
#
#         teClass = lOldClassifItem[4]
#         teOrder = lOldClassifItem[5]
#         # TODO: recompute wicker code like this or force the user to provide a classif file as input with the wicker code already added
#         wCode = iClassif._decisionRuleForWickerCode(teClass, teOrder)
#
#         dClassif["name"] = lOldClassifItem[0]
#         dClassif["wCode"] = wCode
#         dClassif["length"] = int(lOldClassifItem[1])
#         dClassif["strand"] = lOldClassifItem[2]
# #TODO change 'chimeric' by 'confused'
#         dClassif["chimeric"] = False if lOldClassifItem[3] == "ok" else True
#
#         dClassif["class"] = teClass
#         dClassif["order"] = teOrder
#
#         if lOldClassifItem[6] == "complete":
#             dClassif["complete"] = True
#         elif lOldClassifItem[6] == "incomplete":
#             dClassif["complete"] = False
#         else:
#             dClassif["complete"] = None
#
#         allFields = lOldClassifItem[7].split("; ")
#
#         CI = allFields.pop(0)
#         CI = CI.split("=")[-1]
#         if CI != "NA":
#             try:
#                 CI = int(CI)
#             except ValueError as e:
#                 print("Couldn't convert {} to int : {}".format(CI, e))
#         dClassif["CI"] = CI
#
#         dClassif["coding"] = OrderedDict()
#         dClassif["struct"] = OrderedDict()
#         dClassif["other"] = OrderedDict()
#
#         allFields = "; ".join(allFields)
#         codingField = ""
#         structField = ""
#         otherField = ""
#
#         codingStart = allFields.find("coding=(")
#         if codingStart != -1:
#             pCount = 1
#             trueStart = codingStart + len("coding=(")
#             end = trueStart
#             for char in allFields[trueStart:]:
#                 if char == "(":
#                     pCount += 1
#                 if char == ")":
#                     pCount -= 1
#                 if pCount == 0:
#                     break
#                 end += 1
#             if pCount == 0:
#                 codingField = allFields[trueStart:end]
#
#         structStart = allFields.find("struct=(")
#         if structStart != -1:
#             pCount = 1
#             trueStart = structStart + len("struct=(")
#             end = trueStart
#             for char in allFields[trueStart:]:
#                 if char == "(":
#                     pCount += 1
#                 if char == ")":
#                     pCount -= 1
#                 if pCount == 0:
#                     break
#                 end += 1
#             structField = allFields[trueStart:end]
#
#         otherStart = allFields.find("other=(")
#         if otherStart != -1:
#             pCount = 1
#             trueStart = otherStart + len("other=(")
#             end = trueStart
#             for char in allFields[trueStart:]:
#                 if char == "(":
#                     pCount += 1
#                 if char == ")":
#                     pCount -= 1
#                 if pCount == 0:
#                     break
#                 end += 1
#             otherField = allFields[trueStart:end]
#         if codingField != "":
#             ClassifUtils._formatCodingFeaturesAsDict(codingField, dClassif["coding"])
#         if structField != "":
#             ClassifUtils._formatStructFeaturesAsDict(structField, dClassif["struct"])
#         if otherField != "":
#             ClassifUtils._formatOtherFeaturesAsDict(otherField, dClassif["other"])
#
#         return dClassif

    @staticmethod
    def getNewClassifLineAsDict(line):
        dClassif = OrderedDict()
        lOldClassifItem = line.split("\t")
        if len(lOldClassifItem) != 12:
            msg = "Can't parse line: \"{}\"\n" .format (line.strip())
            print("WARNING - ClassifUtils - {}" .format (msg))
            return dClassif
        
        dClassif["name"] = lOldClassifItem[0]
        dClassif["length"] = int(lOldClassifItem[1])
        dClassif["strand"] = lOldClassifItem[2]
        if lOldClassifItem[3] == "True":
            dClassif["confused"] = True
        else:
            dClassif["confused"] = False
        dClassif["class"] = lOldClassifItem[4]
        dClassif["order"] = lOldClassifItem[5]
        dClassif["wCode"] = lOldClassifItem[6]
        dClassif["sFamily"] = lOldClassifItem[7]
        CI = lOldClassifItem[8]
        dClassif["CI"] = CI
        
        dClassif["coding"] = ClassifUtils.formatNewCodingFeaturesAsDict(lOldClassifItem[9])
        # a = lOldClassifItem[9].strip().lstrip("coding=(")
        # if a.endswith("))"):
        #     b = a[:len(a)-1]
        # else: b = a.rstrip(")")
        # ClassifUtils._formatNewCodingFeaturesAsDict(b, dClassif["coding"])
        dClassif["struct"] = ClassifUtils.formatNewStructFeaturesAsDict(lOldClassifItem[10])
        # a = lOldClassifItem[10].strip().lstrip("struct=(")
        # if a.endswith("))"):
        #     b = a[:len(a)-1]
        # else: b = a.rstrip(")")
        # ClassifUtils._formatNewStructFeaturesAsDict(b, dClassif["struct"])
        dClassif["other"] = ClassifUtils.formatNewOtherFeaturesAsDict(lOldClassifItem[11])
        # a = lOldClassifItem[11].strip().lstrip("other=(")
        # if a.endswith("))"):
        #     b = a[:len(a)-1]
        # else: b = a.rstrip(")")
        # ClassifUtils._formatNewOtherFeaturesAsDict(b, dClassif["other"])

        return dClassif
    
    ## Retrieve the classification informations from a classif file
    # 
    # @param fileName Name of the classif file
    # @return A dict containing the classification infos
    #
    @staticmethod
    def getClassifInfosAsDict(fileName):
        dConsensusInfo = OrderedDict()
        if FileUtils.isEmpty(fileName):
            raise SystemExit("ERROR - ClassifUtils - Input file is empty\n")
            
        ext = os.path.splitext(fileName)[1]
        if  ext != (".classif" or ".classifnew"):
            raise SystemExit("ERROR - ClassifUtils - Input file must be a classif file from TEdenovo\n")

        with open(fileName, "r") as classifFile:
#First line, verify if there are headers or not
            line = classifFile.readline()
            lFields = line.split("\t")
            nbFields = len(lFields)
            if lFields[0] not in ["Name", "Seq_name"]:
                seqName = lFields[0]
                if nbFields == 12:
                    dConsensusInfo[seqName] = ClassifUtils.getNewClassifLineAsDict(line)
                else:
                    print("{} : classif line for this consensus is not available, it's ignored." .format(seqName))
#Other lines
            for line in classifFile:
                lFields = line.split("\t")
                seqName = lFields[0]
                nbFields = len(lFields)
                if nbFields == 12:
                    dConsensusInfo[seqName] = ClassifUtils.getNewClassifLineAsDict(line)
                else:
                    print("{} : classif line for this consensus is not available, it's ignored." .format(seqName))

        return dConsensusInfo

    ## Retrieve the classification informations from a classif file
    #
    # @param fileName Name of the classif file (new format with 12 colomns)
    # @return a dict of Classif objects
    #
    @staticmethod
    def createClassifInstanceDictFromClassifFile( fileName ):
        if FileUtils.isEmpty(fileName):
            raise SystemExit("ERROR - ClassifUtils - Input file is empty\n")

        ext = os.path.splitext(fileName)[1]
        if ext != (".classif" or ".classifnew"):
            raise SystemExit("ERROR - ClassifUtils - Input file must be a classif file from TEdenovo\n")
        dConsensus2Classif = OrderedDict()
        with open(fileName, "r") as classifFile:
                # First line, verify if there are headers or not
                line = classifFile.readline()
                lFields = line.split("\t")
                nbFields = len(lFields)
                if lFields[0] not in ["Name", "Seq_name"] or nbFields != 12:
                    raise SystemExit("ERROR - ClassifUtils - Input file must be a new classif with 12 colomns\n")
                # Other lines
                for line in classifFile:
                    iClassif = Classif()
                    iClassif.createClassifInstanceFromClassifLine(line)
                    consName = iClassif.getConsensusName()
                    dConsensus2Classif[consName] = iClassif
        return dConsensus2Classif

    ## Convert a classif file to JSON format
    # 
    # @param fileName Name of the classif file
    # @param outFileName Name of the output JSON file (optional)
    #  
    @staticmethod
    def convertClassifToJson(fileName, outFileName = ""):
        dConsensusInfo = ClassifUtils.getClassifInfosAsDict(fileName)
        if outFileName == "":
            outFileName = "{}_classif.json" .format (os.path.basename(fileName).rsplit(".", 1)[0])
        with open(outFileName, 'w') as outFile:
            json.dump(dConsensusInfo, outFile)

    @staticmethod
    def formatNewCodingFeaturesAsDict(codingInfo):
        lCodingInfo = codingInfo[:-1].lstrip("coding=(").split("; ")
        if lCodingInfo[0] != "" and lCodingInfo[0] != "NA":
            dcodingTypeToData = OrderedDict()
            for codingTypeData in lCodingInfo:
                codingTypeData = codingTypeData.split(": ")
                codingType = codingTypeData.pop(0)
                codingTypeData = ": ".join(codingTypeData)
                codingTypeData = codingTypeData.split(", ")
                if codingType == "TE_BLRtx":
                    if not "TE_BLRtx" in dcodingTypeToData.keys():
                        dcodingTypeToData["TE_BLRtx"] = OrderedDict()
                    for refNameAndCoverage in codingTypeData:
                        refName, coverage = refNameAndCoverage.rsplit(": ", 1)
                        coverage = coverage
                        dcodingTypeToData["TE_BLRtx"][refName] = coverage

                if codingType == "TE_BLRx":
                    if not "TE_BLRx" in dcodingTypeToData.keys():
                        dcodingTypeToData["TE_BLRx"] = OrderedDict()
                    for refNameAndCoverage in codingTypeData:
                        refName, coverage = refNameAndCoverage.rsplit(": ", 1)
                        coverage = coverage
                        dcodingTypeToData["TE_BLRx"][refName] = coverage

                if codingType == "profiles":
                    if not "profiles" in dcodingTypeToData.keys():
                        dcodingTypeToData["profiles"] = OrderedDict()
                        dcodingTypeToData["profiles"] = ClassifUtils.formatProfilesResultsAsDict(codingTypeData)

                if codingType == "Other_profiles":
                    if not "Other_profiles" in dcodingTypeToData.keys():
                        dcodingTypeToData["Other_profiles"] = ClassifUtils.formatProfilesResultsAsDict(codingTypeData)
                if codingType == "rDNA_BLRn":
                    dcodingTypeToData["rDNA_BLRn"] = OrderedDict()
                    codingTypeData = ", ".join(codingTypeData)
                    try:
                        refName, coverage = codingTypeData.rsplit(": ", 1)
                    except ValueError:
                        refName = codingTypeData
                        coverage = -1.0

                    dcodingTypeToData["rDNA_BLRn"]["name"] = refName
                    dcodingTypeToData["rDNA_BLRn"]["cov"] = coverage.replace("%", "")

                if codingType == "HG_BLRn":
                    dcodingTypeToData["HG_BLRn"] = OrderedDict()
                    refName, coverage = codingTypeData[0].rsplit(": ", 1)

                    dcodingTypeToData["HG_BLRn"]["name"] = refName
                    dcodingTypeToData["HG_BLRn"]["cov"] = coverage.replace("%", "")
        else:
            dcodingTypeToData = "NA"

        return dcodingTypeToData
    
    @staticmethod
    def _formatNewProfilesFeaturesAsDict(lProfilesResults):
        if len(lProfilesResults) == 0:
            return OrderedDict()
        dProfileNameToCov = OrderedDict()
        
        for refNameAndCoverage in lProfilesResults:
            refName, coverage = refNameAndCoverage.split(": ")
            coverage = coverage.split("%(")[1].replace("%))","")
            coverage = float(coverage.replace(";",""))
            dProfileNameToCov[refName] = coverage
        return dProfileNameToCov

    @staticmethod
    def formatNewStructFeaturesAsDict(structInfos):
        lStructInfo = structInfos[:-1].lstrip("struct=(").split("; ")
        if lStructInfo[0] != "" and lStructInfo[0] != "NA":
            dStructTypeToData = OrderedDict()
            for structTypeData in lStructInfo:
                structTypeData = structTypeData.split(": ")
                structType = structTypeData.pop(0)

                structTypeData = ": ".join(structTypeData)
                structTypeData = structTypeData.split(", ")

                if structType == "TElength":
                    dStructTypeToData["TElength"] = structTypeData.pop()

                if structType == "TermRepeats":
                    dStructTypeToData["TermRepeats"] = OrderedDict()
                    for refNameAndLength in structTypeData:
                        refName, length = refNameAndLength.rsplit(": ", 1)
                        dStructTypeToData["TermRepeats"][refName] = int(length)

                if structType == "ORF":
                    if not "ORF" in dStructTypeToData.keys():
                        dStructTypeToData["ORF"] = []
                        for orfLengthAndInfo in structTypeData:
                            orfLengthAndInfo = orfLengthAndInfo.split(' ')
                            orfLength = orfLengthAndInfo[0]
                            if len(orfLengthAndInfo) == 1:
                                orfInfo = ""
                            else:
                                orfInfo = " ".join(orfLengthAndInfo[1:]).replace(")","")
                            dStructTypeToData["ORF"].append((orfLength,orfInfo))

                if structType in ["SSR", "SSRtrf"]:
                    if not structType in dStructTypeToData.keys():
                        lSsrData = []
                        for ssr in structTypeData:
                            lSsrData.append(ssr)
                            dStructTypeToData[structType] = lSsrData

                if "SSRCoverage" in structType:
                    dummy, cov = structType.split("=")
                    cov = cov.replace(')','')
                    cov = cov.replace('%','')
                    dStructTypeToData["SSRCoverage"] = float(cov)

                if structType == "polyAtail":
                    dStructTypeToData["polyAtail"] = True

                if structType == "helitronExtremities":
                    structTypeData = ", ".join(structTypeData)
                    structTypeData = structTypeData.split("), ")
                    dStructTypeToData["helitronExtremities"] = OrderedDict()
                    for helitronData in structTypeData:
                        helName, helData = helitronData.split(": (")
                        eValue, start, end = helData.split(", ")

                        helitronExtResult = OrderedDict()
                        helitronExtResult["start"] = int(start)
                        helitronExtResult["end"] = int(end.replace(")","").replace(";",""))
                        helitronExtResult["eValue"] = float(eValue)
                        dStructTypeToData["helitronExtremities"][helName] = helitronExtResult
        else:
            dStructTypeToData = 'NA'

        return dStructTypeToData
            
    @staticmethod
    def formatNewOtherFeaturesAsDict(otherInfo):
        lOtherInfo = otherInfo[:-1].lstrip("other=(").split("; ")
        if lOtherInfo[0] != "" and lOtherInfo[0] != "NA":
            dOtherTypeToData = OrderedDict()
            for otherTypeData in lOtherInfo:
                otherTypeData = otherTypeData.split(": ")
                otherType = otherTypeData.pop(0)
                otherTypeData = ": ".join(otherTypeData)
                otherTypeData = otherTypeData.split(", ")
                if otherType == "TElength":
                    dOtherTypeToData["TElength"] = otherTypeData.pop()
                if otherType == "TermRepeats":
                    dOtherTypeToData["TermRepeats"] = OrderedDict()
                    for refNameAndLength in otherTypeData:
                        refName, length = refNameAndLength.rsplit(": ", 1)
                        dOtherTypeToData["TermRepeats"][refName] = int(length)
                if "SSRCoverage" in otherType:
                    dummy, cov = otherType.split("=")
                    cov = cov.replace(')','')
                    cov = cov.replace('%','')
                    dOtherTypeToData["SSRCoverage"] = float(cov)
                if otherType in ["SSR", "SSRtrf"]:
                    if not otherType in dOtherTypeToData.keys():
                        lSsrData = []
                        for ssr in otherTypeData:
                            lSsrData.append(ssr)
                            dOtherTypeToData[otherType] = lSsrData
                if otherType == "Other_profiles":
                    if not "Other_profiles" in dOtherTypeToData.keys():
                        dOtherTypeToData["Other_profiles"] = ClassifUtils.formatProfilesResultsAsDict(otherTypeData)
                if otherType == "polyAtail":
                    dOtherTypeToData["polyAtail"] = True
                if otherType == "helitronExtremities":
                    otherTypeData = ", ".join(otherTypeData)
                    otherTypeData = otherTypeData.split("), ")
                    dOtherTypeToData["helitronExtremities"] = OrderedDict()
                    for helitronData in otherTypeData:
                        helName, helData = helitronData.split(": (")
                        eValue, start, end = helData.split(", ")
                        helitronExtResult = OrderedDict()
                        helitronExtResult["start"] = int(start)
                        helitronExtResult["end"] = int(end.replace(")","").replace(";",""))
                        helitronExtResult["eValue"] = float(eValue)
                        dOtherTypeToData["helitronExtremities"][helName] = helitronExtResult
                if otherType == "TermRepeats":
                    dOtherTypeToData["TermRepeats"] = OrderedDict()
                    for refNameAndLength in otherTypeData:
                        refName, length = refNameAndLength.rsplit(": ", 1)
                        dOtherTypeToData["TermRepeats"][refName] = int(length)
                if otherType == "ORF":
                    if not "ORF" in dOtherTypeToData.keys():
                        dOtherTypeToData["ORF"] = []
                        for orfLengthAndInfo in otherTypeData:
                            orfLengthAndInfo = orfLengthAndInfo.split(' ')
                            orfLength = orfLengthAndInfo[0]
                            if len(orfLengthAndInfo) == 1:
                                orfInfo = ""
                            else:
                                orfInfo = " ".join(orfLengthAndInfo[1:]).replace(")","")
                            dOtherTypeToData["ORF"].append((orfLength,orfInfo))
                if otherType == "rDNA_BLRn":
                    dOtherTypeToData["rDNA_BLRn"] = OrderedDict()
                    otherTypeData = ", ".join(otherTypeData)
                    try:
                        refName, coverage = otherTypeData.rsplit(": ", 1)
                    except ValueError:
                        refName = otherTypeData
                        coverage = -1.0
                    dOtherTypeToData["rDNA_BLRn"]["name"] = refName
                    dOtherTypeToData["rDNA_BLRn"]["cov"] = coverage.replace("%", "")
                if otherType == "HG_BLRn":
                    dOtherTypeToData["HG_BLRn"] = OrderedDict()
                    refName, coverage = otherTypeData[0].rsplit(": ", 1)
                    dOtherTypeToData["HG_BLRn"]["name"] = refName
                    dOtherTypeToData["HG_BLRn"]["cov"] = coverage.replace("%", "")
                if otherType == "TE_BLRtx":
                    if not "TE_BLRtx" in dOtherTypeToData.keys():
                        dOtherTypeToData["TE_BLRtx"] = OrderedDict()
                    for refNameAndCoverage in otherTypeData:
                        refName, coverage = refNameAndCoverage.rsplit(": ", 1)
                        coverage = coverage
                        dOtherTypeToData["TE_BLRtx"][refName] = coverage
                if otherType == "TE_BLRx":
                    if not "TE_BLRx" in dOtherTypeToData.keys():
                        dOtherTypeToData["TE_BLRx"] = OrderedDict()
                    for refNameAndCoverage in otherTypeData:
                        refName, coverage = refNameAndCoverage.rsplit(": ", 1)
                        coverage = coverage
                        dOtherTypeToData["TE_BLRx"][refName] = coverage
                if otherType == "profiles":
                    if not "profiles" in dOtherTypeToData.keys():
                        dOtherTypeToData["profiles"] = OrderedDict()
                        dOtherTypeToData["profiles"] = ClassifUtils.formatProfilesResultsAsDict(otherTypeData)

        else:
            dOtherTypeToData = 'NA'
        return dOtherTypeToData

    @staticmethod
    def parseClassifToNewClassif(line):
        newLine = ""
        lOldClassifItem = line.split("\t")
        if len(lOldClassifItem) != 8:
            msg = "Can't parse line: \"{}\"\n" .format (line.strip())
            print("WARNING - ClassifUtils - {}" .format (msg))
        else:
            iClassif = Classif()
            iClassif.createClassifInstanceFromClassifLine(line)
            newConsName = iClassif.getConsensusName()
            newConsLength = iClassif.getConsensusLength()
            newConsStrand = iClassif.getConsensusStrand()
            newConsConfused = iClassif.getConfusness()
            orders = iClassif.getConsensusOrder().replace("noCat", "Unclassified")
            lOrders = orders.split('|')
            classes = iClassif.getConsensusClass().replace("noCat", "Unclassified")
            lClass = classes.split('|')
            if newConsConfused:
                if len(lOrders) == 1 and len(lClass) == 1:
                    newConsClass = "{}|NA".format(classes)
                    newConsOrder = "{}|NA".format(orders)
                else:
                    if len(lClass) == 1:
                        newConsOrder = orders
                        lNewClass = []
                        for order in lOrders:
                            if order == "Unclassified" or order == "noCat":
                                itemClass = lClass[0]
                            elif order == "NA":
                                itemClass = "NA"
                            else:
                                itemClass = DORDER2CLASS.get(order)
                            lNewClass.append(itemClass)
                        newConsClass = "|".join(lNewClass)
                    else:
                        newConsClass = classes
                        newConsOrder = orders
            else:
                newConsClass = classes
                newConsOrder = orders
            newConsWCode = iClassif.getCode()
            newConsSuperFamily = iClassif.getConsensusSuperFamily()
            newConsCI = iClassif.getConsensusCI()
            newConsCoding = iClassif.writeCodingFeaturesLine()
            newConsStruct = iClassif.writeStructFeaturesLine()
            newConsOther = iClassif.writeOtherFeaturesLine()
            
            newLine = "{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\tcoding=({})\tstruct=({})\tother=({})".format(newConsName,
                                                                                                        newConsLength,
                                                                                                        newConsStrand,
                                                                                                        newConsConfused,
                                                                                                        newConsClass,
                                                                                                        newConsOrder,
                                                                                                        newConsWCode,
                                                                                                        newConsSuperFamily,
                                                                                                        newConsCI,
                                                                                                        newConsCoding,
                                                                                                        newConsStruct,
                                                                                                        newConsOther)
        return newLine

    @staticmethod
    def classifRLX2SupFamily(inputClassifFile,outputClassifWithOrder):

        with open(inputClassifFile, "r") as inF, open(outputClassifWithOrder, "w") as outF:
            headersLine = inF.readline()
            outF.write(headersLine)
            for inLine in inF:
                copia = gypsy = 0
                iClassif = Classif()
                iClassif.createClassifInstanceFromClassifLine(inLine)
                if iClassif.getCode() == "RLX" and not iClassif.getConfusness():
                    #codingField converted in upper case
                    codingField = iClassif.getConsensusCoding().upper()
                    copia = codingField.find("CLASSI:LTR:COPIA")
                    gypsy = codingField.find("CLASSI:LTR:GYPSY")
                    if copia >= 0 and gypsy == -1:
                        iClassif.setCode("RLC")
                        iClassif.setConsensusSuperFamily("Copia")
                    elif gypsy >= 0 and copia == -1:
                        iClassif.setCode("RLG")
                        iClassif.setConsensusSuperFamily("Gypsy")
                    else:
                        pass
                    outF.write(iClassif.createClassifLine())
                else:
                    outF.write(inLine)

    @staticmethod
    def renameConsensus(inputClassifFile,outputClassifWithOrder, tabFileName):
        dInit2new = {}
        with open(tabFileName, "r") as tabF:
            for line in tabF:
                lNames = line.rstrip("\n").split("\t")
                dInit2new[lNames[0]] = lNames[1]
        with open(inputClassifFile, "r") as inF, open(outputClassifWithOrder, "w") as outF:
            headersLine = inF.readline()
            outF.write(headersLine)
            for inLine in inF:
                iClassif = Classif()
                iClassif.createClassifInstanceFromClassifLine(inLine)
                initConsensusName = iClassif.getConsensusName()
                if initConsensusName in dInit2new:
                    newName = dInit2new[initConsensusName]
                iClassif.setConsensusName(newName)
                outF.write(iClassif.createClassifLine())


