#!/usr/bin/env python

# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import re
from commons.core.LoggerFactory import LoggerFactory

LOG_DEPTH = "repet.commons.utils"

DWICKERCODE = {
               "NA":"NA",
               "ClassI":"RXX",
               "ClassII":"DXX",
               "I":"RXX",
               "II":"DXX",
               "LTR":"RLX",
               "DIRS":"RYX",
               "PLE":"RPX",
               "LINE":"RIX",
               "SINE":"RSX",
               "TIR":"DTX",
               "Crypton":"DYX",
               "Helitron":"DHX",
               "Maverick":"DMX",
#Can be change if we don't want use a trigram for non-autonomus elements
                "LARD" : "RXX-LARD",
                "LTR-LARD": "RXX-LARD",
                "TRIM": "RXX-TRIM",
                "LTR-TRIM": "RXX-TRIM",
                "MITE": "DXX-MITE",
                "TIR-MITE":"DXX-MITE",
                "PotentialrDNA": "PrDNA",
                "rDNA": "rDNA",
                "PotentialHostGene": "PHG",
                "SSR": "SSR",
#                "Unclassified": "NA"
                }
DORDER2CLASS = {
            "LTR": "I",
            "DIRS": "I",
            "PLE": "I",
            "LINE": "I",
            "SINE": "I",
            "TIR": "II",
            "Crypton": "II",
            "Maverick": "II",
            "Helitron": "II",
            "LARD" : "I",
            "MITE": "II",
            "TRIM": "I",
            "NA": "NA"
            }
        
class Classif(object):
    """ The class Classif is a object what determine a line in classif file.
    """

    def __init__(self, consensusName = "", code = "NA", outConfuseness = False, outCompleteness = "", projectName = "", isShorten = False, consensusLength = 0,
                 consensusStrand = "NA", consensusClass = "NA", consensusOrder = "NA", consensusSuperFam = "NA", consensusCI = "NA",
                 consensusCoding = "", consensusStruct = "", consensusOther = ""):
        self._consensusName = consensusName
        self._consensusLength = int(consensusLength)
        self._consensusStrand = consensusStrand
        self._confusness = outConfuseness
        self._consensusClass = consensusClass
        self._consensusOrder = consensusOrder
        self._code = code 
        self._consensusSuperFam = consensusSuperFam
        self._consensusCI = consensusCI
        self._consensusCoding = consensusCoding
        self._consensusStruct = consensusStruct
        self._consensusOther = consensusOther
        self._completeness = str(outCompleteness)
        self._projectName = projectName
        self._isShorten = isShorten
        self._isNoChim = ""
        self._hasCodingPart = False
        self._hasStructPart = False
        self._hasOtherPart = False
        self._evidence = {}
        self._newFormat = False       
        self._log = LoggerFactory.createLogger(name="{}.{}".format(LOG_DEPTH, self.__class__.__name__), verbosity=4)

    def __eq__(self, o):
        compare = False
        if self._newFormat == o.isConsensusFormatNew():
            if self._newFormat:
                compare = (self._consensusName == o.getConsensusName() and self._consensusLength == o.getConsensusLength()
                    and self._consensusStrand == o.getConsensusStrand() and self._confusness == o.getConfusness()
                    and self._consensusClass == o.getConsensusClass() and self._consensusOrder == o.getConsensusOrder()
                    and self._code == o.getCode() and self._consensusSuperFam == o.getConsensusSuperFamily()
                    and self._consensusCI == o.getConsensusCI() and self._consensusCoding== o.getConsensusCoding()
                    and self._consensusStruct == o.getConsensusStruct() and self._consensusOther == o.getConsensusOther())
            else:
                compare = (self._consensusName == o.getConsensusName() and self._code == o.getCode()
                    and self._confusness == o.getConfusness() and self._completeness == o.getCompleteness())

        return compare

    def __ne__(self, o):
        return not self.__eq__(o)

    def __str__(self):
        if not self._newFormat:
            self._log.info("Classif object having old format (with 9 elements)")
        else:
            self._log.info("Classif object having new format (with 12 elements)")
        return "{}\n{}\n{}\n{}\n{}\n{}\n{}\n{}\n{}\n{}\n{}\n{}\n{}".format(self._consensusName, self._consensusLength, self._consensusStrand, self._confusness,
                                                       self._consensusClass, self._consensusOrder, self._code, self._consensusSuperFam, self._consensusCI,
                                                       self._consensusCoding, self._consensusStruct, self._consensusOther, self._completeness)
            
    def setConsensusName(self, consensusName):
        if not self._newFormat:
            #remove -comp, -incomp, -chim
            consensusName = consensusName.replace("-comp","")
            consensusName = consensusName.replace("-incomp","")
            consensusName = consensusName.replace("-chim","")
        self._consensusName = consensusName

    def setInfoEvidence(self, evidence):
        for key in evidence.keys():
            self._evidence.update({key:evidence[key]})
        
    def setInfoEvidenceFromEvidencesDict(self, message):
        if message:
            if 'dictOfEvidence' in message:
                self.setInfoEvidence(message["dictOfEvidence"])
            else:
                self.setInfoEvidence(message)
                
            coding = self.writeCodingFeaturesLine()
            if coding != "":
                self.setConsensusCoding(coding)
            
            struct = self.writeStructFeaturesLine()
            if struct != "":
                self.setConsensusStructure(struct)
            
            other = self.writeOtherFeaturesLine()
            if other != "":
                self.setConsensusOther(other)
        else:
            self.setConsensusCoding('NA')
            self.setConsensusStructure('NA')
            self.setConsensusOther('NA')

    def setCode(self, wCode = "NA"):
        if not '|' in wCode and self._confusness == True:
            self._code = "{}|NA".format(wCode)
        else:
            self._code = wCode

    def setConfusness(self, Confusness):
        if Confusness == "ok" or Confusness == "Ok" or Confusness == "False" or Confusness == False:
            self._confusness = False
        if Confusness == "PotentialChimeric" or Confusness == "True" or Confusness == True:
            self._confusness = True

    def setCompleteness(self, completeness):
        self._completeness = completeness

    def setProjectName(self, projectName):
        self._projectName = projectName

    def setConsensusLength(self, cLength):
        self._consensusLength = int(cLength)

    def setConsensusStrand(self, cStrand):
        self._consensusStrand = cStrand

    def setConsensusClass( self, cClass ):
        self._consensusClass = cClass

    def setConsensusOrder(self, cOrder):
        self._consensusOrder =  cOrder

    def setConsensusSuperFamily(self, cSuperFamily):
        if self._confusness and cSuperFamily.find("|") == -1:
            self._consensusSuperFam = "{}|NA".format(cSuperFamily)
        else:
            self._consensusSuperFam = cSuperFamily

    def setConsensusCI(self, CI):
        if not '|' in CI and self._confusness == True:
            self._consensusCI = "{}|NA".format(CI)
        else :
            self._consensusCI = CI

    def setConsensusCoding(self, coding):
        self._consensusCoding = coding

    def setConsensusStructure(self, structure):
        self._consensusStruct = structure

    def setConsensusOther(self, other):
        self._consensusOther = other
    
    def setNewFormat(self, newFormat):
        self._newFormat = newFormat
    
    def setConsensusWCodeFromOrderAndClass(self):
        lWCode = []
        orders = self.getConsensusOrder()
        lOrders = orders.split('|')
        classes = self.getConsensusClass()
        lClass = classes.split('|')
        if len(lOrders) == 1 and len(lClass) == 1:
            if orders == "Unclassified" or orders == "noCat":
                if classes == "Unclassified" or classes == "noCat":
                    WickerCode = "NA"
                else:
                    WickerCode = DWICKERCODE.get(classes)
            elif orders == "NA":
                WickerCode = "NA"
            else:
                WickerCode = DWICKERCODE.get(orders)
            self.setCode(WickerCode)
        else:
            i = 0
            for order in lOrders:
                if order == "Unclassified" or order == "noCat":
                    WickerCode = DWICKERCODE.get(lClass[i])
                elif order == "NA":
                    WickerCode = "NA"
                else:
                    WickerCode = DWICKERCODE.get(order)
                lWCode.append(WickerCode)
                i += 1
            self.setCode("|".join(lWCode))

    def getConsensusName(self):
        return self._consensusName

    def getNewFormat(self):
        return self._newFormat

    def getCode(self):
        return self._code

    def getConfusness(self):
        return self._confusness

    def getCompleteness(self):
        return self._completeness

    def getprojectName(self):
        return self._projectName
    
    def getConsensusLength(self):
        return self._consensusLength
    
    def getConsensusStrand(self):
        return self._consensusStrand
    
    def getConsensusClass(self):
        return self._consensusClass
    
    def getConsensusOrder(self):
        return self._consensusOrder
    
    def getConsensusSuperFamily(self):
        return self._consensusSuperFam
    
    def getConsensusCI(self):
        return str(self._consensusCI)

    def isConsensusFormatNew( self ):
        return self._newFormat

    def getInfoEvidence(self):
        return self._evidence
    
    def getConsensusCoding(self):
        return self._consensusCoding
    
    def getConsensusStruct(self):
        return self._consensusStruct
    
    def getConsensusOther(self):
        return self._consensusOther
    
    def completeName( self , suffixe):
        self._consensusName += suffixe

    def writeCodingFeaturesLine(self):
        coding = self.getConsensusCoding()
        lResults = []
        if coding == "":
            coding = "NA"                
            dFeature2Infos = self.getInfoEvidence()
            if 'coding' in dFeature2Infos:
                    coding = dFeature2Infos['coding']
            else:
                self.formatCodingFeatures(dFeature2Infos, lResults)
                if lResults :
                    coding = "; ".join(lResults)
                    self._hasCodingPart = True
        return coding

    def writeStructFeaturesLine(self):
        struct = self.getConsensusStruct()
        lResults = []
        if struct == "":
            struct="NA"
            dFeature2Infos = self.getInfoEvidence()
            if "struct" in dFeature2Infos:
                struct = dFeature2Infos['struct']
            else:
                self.formatStructFeatures(dFeature2Infos, lResults)
                if lResults:
                    struct = "; ".join(lResults)
                    self._hasStructPart = True
        return struct

    def writeOtherFeaturesLine(self):
        other = self.getConsensusOther()        
        lResultsWithCoding = []
        lResultsWithStruct = []
        if other =="":
            dFeature2Infos = self.getInfoEvidence()
            if "other" in dFeature2Infos:
                self.formatCodingFeatures(dFeature2Infos['other'], lResultsWithCoding)
                self.formatStructFeatures(dFeature2Infos['other'], lResultsWithStruct)
                if lResultsWithCoding:
                    other = "; ".join(lResultsWithCoding)
                if lResultsWithStruct:
                    other = "{}; {}".format(other,"; ".join(lResultsWithStruct))
                other = other.lstrip(";").lstrip(" ")
            else:
                other = "NA"
        return other

    def formatCodingFeatures(self, dEvidence, lResults):
        if 'Repbase_tbx' in dEvidence and dEvidence['Repbase_tbx']:
            lResults.append("TE_BLRtx: {}".format(", ".join(map(str, dEvidence['Repbase_tbx']))))
        
        if 'Repbase_bx' in dEvidence and dEvidence['Repbase_bx']:
            lResults.append("TE_BLRx: {}".format(", ".join(map(str, dEvidence['Repbase_bx']))))
            
        if ('te_hmmer' in dEvidence) and (dEvidence['te_hmmer']):
            lResults.append('profiles: {}'.format(self.formatProfilesResults(dEvidence['te_hmmer'])))
            
        if 'Other_profiles' in dEvidence:
            lResults.append('Other_profiles: {}'.format(self.formatProfilesResults(dEvidence['Other_profiles'])))
        
        if "rDNA" in dEvidence and dEvidence["rDNA"]:
            lResults.append("rDNA_BLRn: {}".format(dEvidence["rDNA"]))
        
        if "HG" in dEvidence and dEvidence["HG"]:
            lResults.append("HG_BLRn: {}".format(dEvidence["HG"]))

    def formatProfilesResults(self, dProfilesResults):
        if len(dProfilesResults.keys()) == 0:
            return ""
        lResults = []
        for key in (sorted(dProfilesResults.keys())) :#dProfilesResults.keys():
            iPDM = dProfilesResults[key]
            cov = "{0:.2f}%".format(iPDM.getCoverageOnSubject())
            profilesResult = '{}: {}'.format(key, cov)
            lResults.append(profilesResult)
        return ", ".join(lResults)
    
    def formatStructFeatures(self, dEvidence, lResults):
        if 'length' in dEvidence and dEvidence['length']:
            lResults.append('TElength: {}'.format(dEvidence['length']))

        if 'TR' in dEvidence and dEvidence['TR']:
            lResults.append('TermRepeats: {}'.format(", ".join(map(str, dEvidence['TR']))))
            
        if 'ORF' in dEvidence and dEvidence['ORF']:
            lResults.append('ORF: {}'.format(", ".join(dEvidence['ORF'])))

        if 'SSR' in dEvidence and dEvidence['SSR']:
            lResults.append('SSR: {}'.format(", ".join(dEvidence['SSR'])))
        
        if 'SSRCoverage' in dEvidence and dEvidence['SSRCoverage']:
            lResults.append('SSRCoverage={}%'.format(dEvidence['SSRCoverage']))
                                        
        if 'polyAtail' in dEvidence:
            lResults.append('polyAtail')   
             
        if 'helitronExtremities' in dEvidence and dEvidence['helitronExtremities']:
            lResults.append('helitronExtremities: {}'.format(", ".join(map(str, dEvidence['helitronExtremities']))))
        return lResults

                    
    #  deprecated for repet v3.0 because consensus code, when consensus is confused, is elaborate with all trigrams
    def _decisionRuleForWickerCode(self, teClass, order):
        code = 'NA'
        if order in DWICKERCODE.keys():
            code = DWICKERCODE[order]
        elif teClass in DWICKERCODE.keys():
            code = DWICKERCODE[teClass]
        elif order == "Unclassified" and teClass == "Unclassified":
            code = "NA"
        elif re.search("\|", order) and teClass == "Unclassified":
            code = "XXX"
        elif re.search("\|", order) and re.search("\|",teClass):
            lClass = teClass.split("|")
            for iC in lClass[1:]:
                if lClass[0] != iC:
                    code = "XXX"
                    return code
            code = DWICKERCODE[lClass[0]]
        return code

    def renameLARDTRIMAndMITE(self):
        order = self.getConsensusOrder()
        order = order.replace("MITE", "TIR-MITE")
        order = order.replace("LARD", "LTR-LARD")
        order = order.replace("TRIM", "LTR-TRIM")
        self.setConsensusOrder(order)
        dEvidence = self.getInfoEvidence()
        if 'LARD' in dEvidence.keys():
            dEvidence["LTR-LARD"] = dEvidence["LARD"]
            del dEvidence["LARD"]
        if 'TRIM' in dEvidence.keys():
            dEvidence["LTR-TRIM"] = dEvidence["TRIM"]
            del dEvidence["TRIM"]
        if 'MITE' in dEvidence.keys():
            dEvidence["TIR-MITE"] = dEvidence["MITE"]
            del dEvidence["MITE"]
        self.setInfoEvidence(dEvidence)
            
    def separateFeatures(self, featureLine):
        lFeaturesSplited = []
        if featureLine.find("CI=") != -1:
            lParts = featureLine.split("; ")
            CIPart = lParts[0].lstrip("CI=")
            featureLine = "; ".join(lParts[1:])
            featureLine = featureLine.rpartition(")")[0]
            lFeatures = featureLine.split("); ")
            dType2Value = {"coding": "", "struct": "", "other": ""}
            for feature in lFeatures:
                if feature.find("=(") != -1:
                    typeFeature, valueFeature = feature.split("=(")
                    dType2Value[typeFeature] = valueFeature
                else:
                    dType2Value["other"] += "); " + feature
            lFeaturesSplited = [CIPart, dType2Value["coding"], dType2Value["struct"], dType2Value["other"]]
        else:
            self._log.warning("'{}' is not a feature line in old format".format(featureLine))
        return lFeaturesSplited

    def createClassifInstanceFromClassifLine(self, classifFileLine):
        lClassifItem = classifFileLine.split("\t")
#        self.setConsensusName(lClassifItem[0])
        self.setConsensusLength(lClassifItem[1])
        self.setConsensusStrand(lClassifItem[2])
        self.setConfusness(lClassifItem[3])
        if len(lClassifItem) == 8:## old format of classif line, with 8 colomns
            if self._confusness and self._consensusOrder.find("|") >= 0 and self._consensusClass.find("|") < 0:
                lOrders = self._consensusOrder.split("|")
                teClass = "{}|{}".format(DORDER2CLASS.get(lOrders[0]), DORDER2CLASS.get(lOrders[1]))
                self.setConsensusClass(teClass)
            if lClassifItem[5] == "PHG" or lClassifItem[5] == "rDNA" or lClassifItem[
                5] == "PotentialrDNA" or lClassifItem[5] == "SSR":
                self.setConsensusOrder("NA")
                self.setConsensusClass("NA")
                if lClassifItem[5] == "PHG":
                    self.setCode("PHG")
                elif lClassifItem[5] == "rDNA" or lClassifItem[5] == "PotentialrDNA":
                    self.setCode("rDNA")
                else:
                    self.setCode("SSR")
            else:
                if lClassifItem[5]=="Unclassified":
                    self.setConsensusOrder("Unclassified")
                else:
                    self.setConsensusOrder(lClassifItem[5])
                if lClassifItem[4]=="Unclassified":
                    self.setConsensusClass("Unclassified")
                else:
                    self.setConsensusClass(lClassifItem[4])
                self.setConsensusWCodeFromOrderAndClass()

            self.setConsensusSuperFamily("NA")
            if self._confusness:
                if self._consensusOrder.find("|") == -1: # no |
                    self._consensusOrder = "{}|NA".format(self._consensusOrder)
                if self._consensusClass.find("|") == -1: # no |
                    self._consensusClass = "{}|NA".format(self._consensusClass)
            self.setCompleteness(lClassifItem[6])
            lFeatures = self.separateFeatures(lClassifItem[7])
            if len(lFeatures) > 0:
                self.setConsensusCI(lFeatures[0])
                self.setConsensusCoding(lFeatures[1])
                self.setConsensusStructure(lFeatures[2])
                self.setConsensusOther(lFeatures[3])
            else:
                self._log.warning("The current Classif object will have no feature information")                
        else:## new format of classif line, with 12 colomns
            self.setNewFormat(True)
            self.setConsensusOrder(lClassifItem[5])
            self.setConsensusClass(lClassifItem[4])
            self.setCode(lClassifItem[6])
            self.setConsensusSuperFamily(lClassifItem[7])
            self.setConsensusCI(lClassifItem[8].lstrip("CI=(").rstrip(")"))
            # use of rpartition(")")[0] to remove the last ')' in stead of rstrip(")") because if there several ')' rstrip() remove all not only the last!
            self.setConsensusCoding(lClassifItem[9].lstrip("coding=(").rpartition(")")[0])
            self.setConsensusStructure(lClassifItem[10].lstrip("struct=(").rpartition(")")[0])
            self.setConsensusOther(lClassifItem[11].lstrip("other=(").rpartition(")")[0])                    
        self.setConsensusName(lClassifItem[0])

    def createNewConsensusName(self):
        pastecClassif = self._code
        if self._completeness == "comp" or self._completeness == "complete":
            pastecClassif += "-comp"
        if self._completeness == "incomp" or self._completeness == "incomplete":
            pastecClassif += "-incomp"

        if self._confusness == "confused" or self._confusness == "chim" or self._confusness == "PotentialChimeric":
            pastecClassif += "-chim"
        if self._isShorten:
            pattern = "{}_[a-zA-Z0-9]+_[a-zA-Z0-9]+_[a-zA-Z0-9_]+".format(self._projectName)
            if re.match(pattern, self._consensusName) and not "{}_RS_".format(self._projectName in self._consensusName):
                header = self.shortenConsensusName()
                header = "{}_{}".format(pastecClassif, header)
            else:
                header = "{}_{}".format(pastecClassif, self._consensusName)
        else:
            header = "{}_{}".format(pastecClassif, self._consensusName)
        if self._consensusStrand == "-":
            header += "_reversed"

        return header

    def shortenConsensusName(self):
        consensusShorten = "{}".format(self._projectName)
        desc = self._consensusName.split(self._projectName)[1]
        palignMeth = desc.split("_")[1]
        clustMeth = desc.split("_")[2]
        clustID = desc.split("_")[3]
        lmalignMeth = desc.split("_")[4:]
        if len(lmalignMeth) > 2:
            malignMeth = "{}{}_{}".format(lmalignMeth[0], lmalignMeth[1], lmalignMeth[2])
        else:
            malignMeth = "".join(lmalignMeth)
        consensusShorten = "{}-{}-{}{}-{}".format(self._projectName, palignMeth[0], clustMeth[0], clustID, malignMeth)

        return consensusShorten

    ## Write Classif line from classif object in new format
    #
    def createClassifLine( self):
        if self.getNewFormat():
            line = "{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\tcoding=({})\tstruct=({})\tother=({})\n".format(self._consensusName, self._consensusLength, self._consensusStrand, self._confusness,
                                                       self._consensusClass, self._consensusOrder, self._code, self._consensusSuperFam, self._consensusCI,
                                                       self._consensusCoding, self._consensusStruct, self._consensusOther)
        else:
            line = "{}\t{}\t{}\t{}\t{}\t{}\t{}\tCI={}; coding=({}); struct=({}); other=({})\n".format(self._consensusName, self._consensusLength, self._consensusStrand, self._confusness,
                                                       self._consensusClass, self._consensusOrder, self._completeness, self._consensusCI,
                                                       self._consensusCoding, self._consensusStruct, self._consensusOther)
        return line
#        pass

    ## Write Classif instance into a classif file handler
    #
    # @param classifFileHandler file handler of a classif file
    #
    def writeInNewClassifFormat( self, classifFileHandler ):
        line = "{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\tcoding=({})\tstruct=({})\tother=({})\n".format(self._consensusName, self._consensusLength, self._consensusStrand, self._confusness,
                                                       self._consensusClass, self._consensusOrder, self._code, self._consensusSuperFam, self._consensusCI,
                                                       self._consensusCoding, self._consensusStruct, self._consensusOther)
        classifFileHandler.write(line)

    ## Write Classif instance into a classif file handler
    #
    # @param classifFileHandler file handler of a classif file
    #
    def writeInOldClassifFormat( self, classifFileHandler ):
        line = "{}\t{}\t{}\t{}\t{}\t{}\t{}\tCI={}; coding=({}); struct=({}); other=({})\n".format(self._consensusName, self._consensusLength, self._consensusStrand, self._confusness,
                                                       self._consensusClass, self._consensusOrder, self._completeness, self._consensusCI,
                                                       self._consensusCoding, self._consensusStruct, self._consensusOther)
        classifFileHandler.write(line)

