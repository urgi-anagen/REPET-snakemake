import unittest
from collections import OrderedDict
from commons.core.utils.Classif import Classif
from commons.core.utils.FileUtils import FileUtils
from PASTEC.BlasterMatch import BlasterMatch
from PASTEC.TR import TR
from PASTEC.HelitronExtremity import HelitronExtremity
from PASTEC.ProteinDomainMatch import ProteinDomainMatch

class Test_Classif(unittest.TestCase):

    def setUp(self):
        pass
    
    def tearDown(self):
        pass

    def test_eq(self):
        iClassif1 = Classif()
        iClassif2 = Classif()
        self.assertEqual(iClassif1, iClassif2)

    def test_shortenConsensusName( self ):
        projectName = "Vigne_1_3_12_1"
        iClassif = Classif("Vigne_1_3_12_1_Blaster_Grouper_3678_Map_14", "RE", "chim", "comp")
        iClassif.setProjectName(projectName)
        obsConsensusName = iClassif.shortenConsensusName()
        expConsensusName = "Vigne_1_3_12_1-B-G3678-Map14"
        self.assertEqual(expConsensusName, obsConsensusName)

    def test_shortenConsensusName_MCL( self ):
        projectName = "Vigne_1_3_12_1"
        iClassif = Classif("Vigne_1_3_12_1_LTRharvest_MCL_6101988_Map_14", "RE", "chim", "comp")
        iClassif.setProjectName(projectName)
        obsConsensusName = iClassif.shortenConsensusName()
        expConsensusName = "Vigne_1_3_12_1-L-M6101988-Map14"
        self.assertEqual(expConsensusName, obsConsensusName)

    def test_shortenConsensusName_reversed( self ):
        projectName = "Vigne_1_3_12_1"
        iClassif = Classif("Vigne_1_3_12_1_Blaster_Grouper_3678_Map_14_reversed", "RE", "chim", "comp")
        iClassif.setProjectName(projectName)
        obsConsensusName = iClassif.shortenConsensusName()
        expConsensusName = "Vigne_1_3_12_1-B-G3678-Map14_reversed"
        self.assertEqual(expConsensusName, obsConsensusName)

# The 3 following tests are designed to test RepeatScout's headers shortening.
# By now, RS consensus' headers are kept as it (cf. Classif.createNewConsensusName method).
# If you want to shorten them, uncomment these tests and make them pass, baby.
#
# /!\ Be careful! Modify functional tests too, and check TEdenovo's following steps and tests for resulting errors!

    # def test_shortenConsensusName_RS(self):
    #    projectName = "Vigne_1_3_12_1"
    #    iClassif = Classif("Vigne_1_3_12_1_RS_2", "RE", "chim", "comp")
    #    iClassif.setProjectName(projectName)
    #    obsConsensusName = iClassif.shortenConsensusName()
    #    expConsensusName = "Vigne_1_3_12_1-RS2"
    #    self.assertEqual(expConsensusName, obsConsensusName)
    #
    # def test_shortenConsensusName_RS_reversed(self):
    #    projectName = "Vigne_1_3_12_1"
    #    iClassif = Classif("Vigne_1_3_12_1_RS_2_reversed", "RE", "chim", "comp")
    #    iClassif.setProjectName(projectName)
    #    obsConsensusName = iClassif.shortenConsensusName()
    #    expConsensusName = "Vigne_1_3_12_1-RS2_reversed"
    #    self.assertEqual(expConsensusName, obsConsensusName)
    #
    # def test_shortenConsensusName_RS_reversed_simpleProjectName(self):
    #    projectName = "test"
    #    iClassif = Classif("test_RS_2_reversed", "RE", "chim", "comp")
    #    iClassif.setProjectName(projectName)
    #    obsConsensusName = iClassif.shortenConsensusName()
    #    expConsensusName = "test-RS2_reversed"
    #    self.assertEqual(expConsensusName, obsConsensusName)

    def test_createNewConsensusName( self ):
        iClassif = Classif("gypsy5", "DM", "confused", "")
        obsHeader = iClassif.createNewConsensusName()
        expHeader = "DM-chim_gypsy5"
        self.assertEqual(expHeader, obsHeader)
        iClassif = Classif("ALU","RSX", False, "comp",consensusStrand = "-")
        obsHeader = iClassif.createNewConsensusName()
        expHeader = "RSX-comp_ALU_reversed"
        self.assertEqual(expHeader, obsHeader)
        iClassif = Classif("DIRS_Nvi:classI:LTR_retrotransposon","RXX", "PotentialChimeric", "comp",consensusStrand = "+")
        obsHeader = iClassif.createNewConsensusName()
        expHeader = "RXX-comp-chim_DIRS_Nvi:classI:LTR_retrotransposon"
        self.assertEqual(expHeader, obsHeader)
        iClassif = Classif("Unknown-B-R21164-Map3_NoCat","DXX", "NA", "comp",consensusStrand = "+")
        obsHeader = iClassif.createNewConsensusName()
        expHeader = "DXX-comp_Unknown-B-R21164-Map3_NoCat"
        self.assertEqual(expHeader, obsHeader)

    def test_createClassifInstanceFromClassifLine_lineInNewFormat(self):
        obsClassif = Classif(projectName = "LStag_chk16k")
        obsClassif.setNewFormat(True)
        line = "LStag_chk16k-B-G3361-Map20\t1118\t.\tFalse\tUnclassified\tUnclassified\tXXX\tNA\tNA\tcoding=(NA)\tstruct=(TermRepeats: non-termLTR: 378; SSRCoverage=33.00%)\tother=(NA)"
        obsClassif.createClassifInstanceFromClassifLine(line)
        expClassif = Classif("LStag_chk16k-B-G3361-Map20", "XXX", False, "", "LStag_chk16k", True, 1118, ".", "Unclassified", "Unclassified", "NA", "NA", "NA", "TermRepeats: non-termLTR: 378; SSRCoverage=33.00%", "NA" )
        expClassif.setNewFormat(True)
        self.assertEqual(expClassif, obsClassif)

        line = "G3\t4605\t+\tTrue\tI|Unclassified\tLINE|Unclassified\tRIX|NA\tNA|NA\t63|NA"\
                "\tcoding=(TE_BLRtx: G3_DM:classI:LINE: 100.00%; TE_BLRx: Jockey-1_DYa_2p:classI:LINE: 92.33%, TART_DV_2p:classI:LINE: 27.29%; profiles: PF00078.22_RVT_1_RT_20.7: 98.60%)"\
                "\tstruct=(TElength: >1000bps; ORF: >1000bps, >1000bps RT; SSR: (A)16_end)"\
                "\tother=(TE_BLRx: BEL2-I_Dmoj_1p:classI:LTR_retrotransposon: 8.16%; Other_profiles: PF07530.6_PRE_C2HC_OTHER_22.1: 92.65%; SSRCoverage=8.00%)"
        obsClassif = Classif()
        obsClassif.setNewFormat(True)
        obsClassif.createClassifInstanceFromClassifLine(line)
        expClassif = Classif("G3", "RIX|NA", True, "","", True, 4605, "+", "I|Unclassified", "LINE|Unclassified", "NA|NA", "63|NA",
                             "TE_BLRtx: G3_DM:classI:LINE: 100.00%; TE_BLRx: Jockey-1_DYa_2p:classI:LINE: 92.33%, TART_DV_2p:classI:LINE: 27.29%; profiles: PF00078.22_RVT_1_RT_20.7: 98.60%",
                             "TElength: >1000bps; ORF: >1000bps, >1000bps RT; SSR: (A)16_end",
                             "TE_BLRx: BEL2-I_Dmoj_1p:classI:LTR_retrotransposon: 8.16%; Other_profiles: PF07530.6_PRE_C2HC_OTHER_22.1: 92.65%; SSRCoverage=8.00%" )
        expClassif.setNewFormat(True)
        self.assertEqual(expClassif, obsClassif)
        line= "PotentialHostGene_LStag_chk16k-B-G334-Map8\t2401\t.\tFalse\tNA\tNA\tPHG\tNA\t100\tcoding=(Other_profiles: PF07645.10_EGF_CA_NA_OTHER_21.0: 80.95%)\tstruct=(NA)\tother=(SSRCoverage=16.00%)"
        obsClassif = Classif(projectName = "LStag_chk16k")
        obsClassif.createClassifInstanceFromClassifLine(line)
        expClassif = Classif("PotentialHostGene_LStag_chk16k-B-G334-Map8", "PHG", False, "","LStag_chk16k", True, 2401, ".", "NA", "NA", "NA","100", "Other_profiles: PF07645.10_EGF_CA_NA_OTHER_21.0: 80.95%", "NA", "SSRCoverage=16.00%" )
        expClassif.setNewFormat(True)
        self.assertEqual(expClassif, obsClassif)


    def test_createClassifInstanceFromClassifLine_oldFormat(self):
        line = "RXX-incomp_LStag_chk16k-B-G3361-Map20\t1118\t.\tOk\tClassI\tnoCat\tNA\tCI=NA; coding=(NA); struct=(TermRepeats: non-termLTR: 378; SSRCoverage=0.33); other=(NA)"        
        obsClassif = Classif(projectName = "LStag_chk16k")
        obsClassif.createClassifInstanceFromClassifLine(line)
        expClassif = Classif("RXX_LStag_chk16k-B-G3361-Map20", "RXX", False, "NA","LStag_chk16k", True, 1118, ".", "ClassI", "unclassified","NA", "NA", "NA", "TermRepeats: non-termLTR: 378; SSRCoverage=0.33", "NA" )
        self.assertEqual(expClassif, obsClassif)
        line= "PotentialHostGene_LStag_chk16k-B-G334-Map8\t2401\t.\tok\tNA\tPotentialHostGene\tNA\tCI=100; coding=(Other_profiles: PF07645.10_EGF_CA_NA_OTHER_21.0: 80.95%(80.95%)); other=(SSRCoverage=0.16)"
        obsClassif = Classif(projectName = "LStag_chk16k")
        obsClassif.createClassifInstanceFromClassifLine(line)
        expClassif = Classif("PotentialHostGene_LStag_chk16k-B-G334-Map8", "PHG", False, "NA","LStag_chk16k", True, 2401, ".", "NA", "NA", "NA",100, "Other_profiles: PF07645.10_EGF_CA_NA_OTHER_21.0: 80.95%(80.95%)", "", "SSRCoverage=0.16" )
        self.assertEqual(expClassif, obsClassif)

    def test_separateFeatures(self):
        iClassif = Classif()
        featureLine = "CI=92; coding=(TE_BLRtx: ROOA_I:classI:LTR_retrotransposon: 100.00%, ROOA_LTR:classI:LTR_retrotransposon: 100.00%; TE_BLRx: BEL13_AGp:classI:LTR_retrotransposon: 9.65%, BEL2-I_Dpse_1p:classI:LTR_retrotransposon: 10.82%, MAXp:classI:LTR_retrotransposon: 77.47%; profiles: _RT_bel_RT_NA: 99.66%(99.66%), _GAG_bel_GAG_NA: 98.62%(98.62%), PF05380.8_Peptidase_A17_AP_20.6: 30.19%(30.19%), _AP_bel_AP_NA: 100.00%(100.00%), _RNaseH_bel_RH_NA: 100.00%(100.00%), _INT_bel_INT_NA: 94.02%(94.02%)); "\
                        "struct=(TElength: >4000bps; TermRepeats: termLTR: 386; ORF: >3000bps AP RT AP RH INT); "\
                        "other=(SSRCoverage=0.04)"
        obsLFeatures = iClassif.separateFeatures(featureLine)
        expLFeatures = ["92", "TE_BLRtx: ROOA_I:classI:LTR_retrotransposon: 100.00%, ROOA_LTR:classI:LTR_retrotransposon: 100.00%; TE_BLRx: BEL13_AGp:classI:LTR_retrotransposon: 9.65%, BEL2-I_Dpse_1p:classI:LTR_retrotransposon: 10.82%, MAXp:classI:LTR_retrotransposon: 77.47%; profiles: _RT_bel_RT_NA: 99.66%(99.66%), _GAG_bel_GAG_NA: 98.62%(98.62%), PF05380.8_Peptidase_A17_AP_20.6: 30.19%(30.19%), _AP_bel_AP_NA: 100.00%(100.00%), _RNaseH_bel_RH_NA: 100.00%(100.00%), _INT_bel_INT_NA: 94.02%(94.02%)",
                        "TElength: >4000bps; TermRepeats: termLTR: 386; ORF: >3000bps AP RT AP RH INT",
                        "SSRCoverage=0.04"]
        self.assertEqual(expLFeatures, obsLFeatures)
        featureLine = "CI=20; other=(TermRepeats: non-termLTR: 378; SSRCoverage=0.33)"
        obsLFeatures = iClassif.separateFeatures(featureLine)
        expLFeatures = ["20", "", "", "TermRepeats: non-termLTR: 378; SSRCoverage=0.33"]
        self.assertEqual(expLFeatures, obsLFeatures)
        featureLine = "CI=20; other=(SSRCoverage=0.19); struct=(TElength: <700bps; TermRepeats: termTIR: 20)"
        obsLFeatures = iClassif.separateFeatures(featureLine)
        expLFeatures = ["20", "", "TElength: <700bps; TermRepeats: termTIR: 20", "SSRCoverage=0.19"]
        self.assertEqual(expLFeatures, obsLFeatures)
        featureLine = "CI=25; coding=(profiles: PF00098.18_zf-CCHC_GAG_16.7: 94.44%(94.44%)); struct=(TElength: >4000bps; TermRepeats: termLTR: 3315, non-termLTR: 166; ORF: >1000bps GAG); other=(Other_profiles: PF05754.9_DUF834_OTHER_23.0: 120.00%(98.57%); SSRCoverage=0.22)"
        obsLFeatures = iClassif.separateFeatures(featureLine)
        expLFeatures = ["25", "profiles: PF00098.18_zf-CCHC_GAG_16.7: 94.44%(94.44%)", "TElength: >4000bps; TermRepeats: termLTR: 3315, non-termLTR: 166; ORF: >1000bps GAG", "Other_profiles: PF05754.9_DUF834_OTHER_23.0: 120.00%(98.57%); SSRCoverage=0.22"]
        self.assertEqual(expLFeatures, obsLFeatures)

                
    def test_separateFeatures_withWrongLine(self):
        featureLine = "45; coding=(NA); struct=(TermRepeats: non-termLTR: 378; SSRCoverage=0.33); other=(NA)"
        iClassif = Classif()
        obsLFeatures = iClassif.separateFeatures(featureLine)
        expLFeatures = []
        self.assertEqual(expLFeatures, obsLFeatures)

    def test_writeOtherFeaturesLine(self):
        iBlasterMatch1 = BlasterMatch("Copia-14_AA-I_1p:ClassI:LTR:Copia", 39.24, identity = 0.0)
        iBlasterMatch2 = BlasterMatch("Copia-1_DPer-I_1p:ClassI:LTR:Copia", 22.22, identity = 0.0)
        iBlasterMatch3 = BlasterMatch("Copia-5_DWil-I_1p:ClassI:LTR:Copia", 100.0, identity = 0.0)
        iBlasterMatch4 = BlasterMatch("COPIA_DM_I:ClassI:LTR:Copia", 100.0, identity = 0.0)
        iBlasterMatch5 = BlasterMatch("COPIA_DM_LTR:ClassI:LTR:Copia", 100.0, identity = 0.0)
        iC = Classif()
        iTR1 = TR("termTIR", 12)
        iTR2 = TR("termLTR", 275)
        message = {'length': '>4000bps', 
                   'other': {
                             'SSRCoverage': '9.00',
                             'TR': [iTR1], 
                             'Other_profiles': {
                                                "PF13961.1_DUF4219_OTHER_27.0": ProteinDomainMatch("PF13961.1_DUF4219_OTHER_27.0", start=462, strand='+', frame=0, end=0, index=0.00, coverageOnSubject=100.00)
                                                }
                             },   
                   'te_hmmer': {
                              "PF07727.9_RVT_2_RT_22.0": ProteinDomainMatch("PF07727.9_RVT_2_RT_22.0", coverage=99.19, start=3189, strand='+', frame=3, end=3920, index=99.19, coverageOnSubject=99.19), 
                              'PF00665.21_rve_INT_30.0': ProteinDomainMatch("PF00665.21_rve_INT_30.0", coverage=97.50, start=1863, strand='+', frame=3, end=2216, index=97.50, coverageOnSubject=97.50), 
                              'PF13976.1_gag_pre-integrs_GAG_27.0': ProteinDomainMatch("PF13976.1_gag_pre-integrs_GAG_27.0", coverage=80.60, start=1641, strand='+', frame=3, end=1820, index=80.60, coverageOnSubject=80.60), 
                              'PF14227.1_UBN2_2_GAG_27.0': ProteinDomainMatch("PF14227.1_UBN2_2_GAG_27.0", coverage=96.67, start=657, strand='+', frame=3, end=1001, index=96.67, coverageOnSubject=96.67)
                              }, 
                   'ORF': ['>3000bps GAG GAG INT RT'], 
                   'Repbase_bx': [iBlasterMatch1, iBlasterMatch2, iBlasterMatch3], 
                   'TR': [iTR2], 
                   'Repbase_tbx': [iBlasterMatch4, iBlasterMatch5]
                   }
        iC.setInfoEvidence(message)
        obs = iC.writeOtherFeaturesLine()
        exp = 'Other_profiles: PF13961.1_DUF4219_OTHER_27.0: 100.00%; TermRepeats: termTIR: 12; SSRCoverage=9.00%'
        self.assertEqual(obs, exp)    

    def test_writeOtherFeaturesLine_withNoOtherAttribut(self):
        iC = Classif()
        iTR1 = TR("termTIR", 12)
        message = {'length': '>4000bps', 
                   'ORF': ['>3000bps GAG GAG INT RT'],
                   'TR': [iTR1], 
                   }
        iC.setInfoEvidence(message)
        obs = iC.writeOtherFeaturesLine()
        exp = 'NA'
        self.assertEqual(obs, exp)    

    def test_writeOtherFeaturesLine_withNoOtherProfileAttribut(self):
        iC = Classif()
        iTR1 = TR("termTIR", 12)
        message = {
            'other': {  'SSRCoverage': '9.00',
                        'TR': [iTR1],
                        'te_hmmer': {
                            "PF07727.9_RVT_2_RT_22.0": ProteinDomainMatch("PF07727.9_RVT_2_RT_22.0", coverage=99.19,
                                                                          start=3189, strand='+', frame=3, end=3920,
                                                                          index=99.19, coverageOnSubject=99.19),
                            'PF00665.21_rve_INT_30.0': ProteinDomainMatch("PF00665.21_rve_INT_30.0", coverage=97.50,
                                                                          start=1863, strand='+', frame=3, end=2216,
                                                                          index=97.50, coverageOnSubject=97.50),
                            'PF13976.1_gag_pre-integrs_GAG_27.0': ProteinDomainMatch(
                                "PF13976.1_gag_pre-integrs_GAG_27.0", coverage=80.60, start=1641, strand='+', frame=3,
                                end=1820, index=80.60, coverageOnSubject=80.60),
                                    }
                        }
                }
        iC.setInfoEvidence(message)
        obs = iC.writeOtherFeaturesLine()
        #exp = 'profiles: PF07727.9_RVT_2_RT_22.0: 99.19%, PF00665.21_rve_INT_30.0: 97.50%, PF13976.1_gag_pre-integrs_GAG_27.0: 80.60%; TermRepeats: termTIR: 12; SSRCoverage=9.00%'
        exp = 'profiles: PF00665.21_rve_INT_30.0: 97.50%, PF07727.9_RVT_2_RT_22.0: 99.19%, PF13976.1_gag_pre-integrs_GAG_27.0: 80.60%; TermRepeats: termTIR: 12; SSRCoverage=9.00%'
        self.assertEqual(obs, exp)                

        
    def test_writeOtherFeaturesLine_withNoOtherFeatures(self):
        iBlasterMatch1 = BlasterMatch("Copia-14_AA-I_1p:ClassI:LTR:Copia", 39.24, identity = 0.0)
        iBlasterMatch2 = BlasterMatch("Copia-1_DPer-I_1p:ClassI:LTR:Copia", 22.22, identity = 0.0)
        iBlasterMatch3 = BlasterMatch("Copia-5_DWil-I_1p:ClassI:LTR:Copia", 100.0, identity = 0.0)
        iBlasterMatch4 = BlasterMatch("COPIA_DM_I:ClassI:LTR:Copia", 100.0, identity = 0.0)
        iBlasterMatch5 = BlasterMatch("COPIA_DM_LTR:ClassI:LTR:Copia", 100.0, identity = 0.0)
        iC = Classif()
        iTR2 = TR("termLTR", 275)
        message = {'length': '>4000bps',   
                   'te_hmmer': {
                              "PF07727.9_RVT_2_RT_22.0": ProteinDomainMatch("PF07727.9_RVT_2_RT_22.0", coverage=99.19, start=3189, strand='+', frame=3, end=3920, index=99.19, coverageOnSubject=99.19), 
                              'PF00665.21_rve_INT_30.0': ProteinDomainMatch("PF00665.21_rve_INT_30.0", coverage=97.50, start=1863, strand='+', frame=3, end=2216, index=97.50, coverageOnSubject=97.50), 
                              'PF13976.1_gag_pre-integrs_GAG_27.0': ProteinDomainMatch("PF13976.1_gag_pre-integrs_GAG_27.0", coverage=80.60, start=1641, strand='+', frame=3, end=1820, index=80.60, coverageOnSubject=80.60), 
                              'PF14227.1_UBN2_2_GAG_27.0': ProteinDomainMatch("PF14227.1_UBN2_2_GAG_27.0", coverage=96.67, start=657, strand='+', frame=3, end=1001, index=96.67, coverageOnSubject=96.67)
                              }, 
                   'ORF': ['>3000bps GAG GAG INT RT'], 
                   'Repbase_bx': [iBlasterMatch1, iBlasterMatch2, iBlasterMatch3], 
                   'TR': [iTR2], 
                   'Repbase_tbx': [iBlasterMatch4, iBlasterMatch5]
                   }
        iC.setInfoEvidence(message)
        obs = iC.writeOtherFeaturesLine()
        exp = 'NA'
        self.assertEqual(obs, exp)

    def test_writeCodingFeaturesLine(self):
        #  dans cet exemple, Ce qui va etre utilise pour le coding c'est le contenu des cles Repbase_tbx et Repbase_bx.
        #  les autres cles  length, SSRCoverage, TR et ORF seraient utilisees pour constituer le struct
        #  Attention les % sont toujours avec 2 decimales dans l'expected meme si au depart il n'y en a qu'une
        iBlasterMatch1 = BlasterMatch("Copia-14_AA-I_1p:ClassI:LTR:Copia", 39.24)
        iBlasterMatch2 = BlasterMatch("Copia-1_DPer-I_1p:ClassI:LTR:Copia", 22.22)
        iBlasterMatch3 = BlasterMatch("Copia-5_DWil-I_1p:ClassI:LTR:Copia", 100.0)
        iBlasterMatch4 = BlasterMatch("COPIA_DM_I:ClassI:LTR:Copia", 100.0)
        iBlasterMatch5 = BlasterMatch("COPIA_DM_LTR:ClassI:LTR:Copia", 100.0)
        iC = Classif()
        iTR1 = TR("termTIR", 12)
        message = {'length': '>4000bps',
                   'SSRCoverage': '0.09', 
                   'TR': [iTR1], 
                   'ORF': ['>3000bps GAG GAG INT RT'], 
                   'Repbase_bx': [iBlasterMatch1, iBlasterMatch2, iBlasterMatch3], 
                   'Repbase_tbx': [iBlasterMatch4, iBlasterMatch5]
                   }
        iC.setInfoEvidence(message)
        obs = iC.writeCodingFeaturesLine()
        exp = 'TE_BLRtx: COPIA_DM_I:ClassI:LTR:Copia: 100.00%, COPIA_DM_LTR:ClassI:LTR:Copia: 100.00%; TE_BLRx: Copia-14_AA-I_1p:ClassI:LTR:Copia: 39.24%, Copia-1_DPer-I_1p:ClassI:LTR:Copia: 22.22%, Copia-5_DWil-I_1p:ClassI:LTR:Copia: 100.00%'
        self.assertEqual(obs, exp)

    def test_writeCodingFeaturesLine_withCodingAttribut(self):
        iC = Classif()
        iC.setConsensusCoding("TE_BLRtx: COPIA_DM_I:ClassI:LTR:Copia: 100.00%, COPIA_DM_LTR:ClassI:LTR:Copia: 100.00%; TE_BLRx: Copia-14_AA-I_1p:ClassI:LTR:Copia: 39.24%, Copia-1_DPer-I_1p:ClassI:LTR:Copia: 22.22%, Copia-5_DWil-I_1p:ClassI:LTR:Copia: 100.00%")
        obs = iC.writeCodingFeaturesLine()
        exp = 'TE_BLRtx: COPIA_DM_I:ClassI:LTR:Copia: 100.00%, COPIA_DM_LTR:ClassI:LTR:Copia: 100.00%; TE_BLRx: Copia-14_AA-I_1p:ClassI:LTR:Copia: 39.24%, Copia-1_DPer-I_1p:ClassI:LTR:Copia: 22.22%, Copia-5_DWil-I_1p:ClassI:LTR:Copia: 100.00%'
        self.assertEqual(obs, exp)                

    def test_writeCodingFeaturesLine_withOtherProfileAttribut_PHGcase(self):
        iBlasterMatch1 = BlasterMatch("Copia-14_AA-I_1p:ClassI:LTR:Copia", 39.24, identity = 0.0)
        iBlasterMatch4 = BlasterMatch("COPIA_DM_I:ClassI:LTR:Copia", 100.0, identity = 0.0)
        iBlasterMatch5 = BlasterMatch("COPIA_DM_LTR:ClassI:LTR:Copia", 100.0, identity = 0.0)
        iC = Classif()
        message = {'length': '>4000bps',
                   'Other_profiles': {
                       "PF13961.1_DUF4219_OTHER_27.0": ProteinDomainMatch("PF13961.1_DUF4219_OTHER_27.0", start=462,
                                                                          strand='+', frame=0, end=0, index=0.00,
                                                                          coverageOnSubject=100.00)
                                    },
                   'Repbase_bx': [iBlasterMatch1],
                   'Repbase_tbx': [iBlasterMatch4, iBlasterMatch5]

                   }
        iC.setInfoEvidence(message)
        obs = iC.writeCodingFeaturesLine()
        exp = 'TE_BLRtx: COPIA_DM_I:ClassI:LTR:Copia: 100.00%, COPIA_DM_LTR:ClassI:LTR:Copia: 100.00%; TE_BLRx: Copia-14_AA-I_1p:ClassI:LTR:Copia: 39.24%; Other_profiles: PF13961.1_DUF4219_OTHER_27.0: 100.00%'
        self.assertEqual(obs, exp)

    def test_writeCodingFeaturesLine_withEvidenceAttribut(self):
        #  dans cet exemple il n'y a que de l'information qui correspond a la cle 'other', donc il n'y aura rien correspondant a coding
        iBlasterMatch1 = BlasterMatch("Copia-14_AA-I_1p:ClassI:LTR:Copia", 39.24, identity = 0.0)
        iBlasterMatch2 = BlasterMatch("Copia-1_DPer-I_1p:ClassI:LTR:Copia", 22.22, identity = 0.0)
        iBlasterMatch3 = BlasterMatch("Copia-5_DWil-I_1p:ClassI:LTR:Copia", 100.0, identity = 0.0)
        iBlasterMatch4 = BlasterMatch("COPIA_DM_I:ClassI:LTR:Copia", 100.0, identity = 0.0)
        iBlasterMatch5 = BlasterMatch("COPIA_DM_LTR:ClassI:LTR:Copia", 100.0, identity = 0.0)
        iC = Classif()
        iTR1 = TR("termTIR", 12)
        message = {'length': '>4000bps',
                   'other': {
                             'SSRCoverage': '0.09', 
                             'TR': [iTR1], 
                             'Other_profiles': {
                                                "PF13961.1_DUF4219_OTHER_27.0": ProteinDomainMatch("PF13961.1_DUF4219_OTHER_27.0", start=462, strand='+', frame=0, end=0, index=0.00, coverageOnSubject=100.00)
                                                }, 
                             'te_hmmer': {
                                          "PF07727.9_RVT_2_RT_22.0": ProteinDomainMatch("PF07727.9_RVT_2_RT_22.0", coverage=99.19, start=3189, strand='+', frame=3, end=3920, index=99.19, coverageOnSubject=99.19), 
                                          'PF00665.21_rve_INT_30.0': ProteinDomainMatch("PF00665.21_rve_INT_30.0", coverage=97.50, start=1863, strand='+', frame=3, end=2216, index=97.50, coverageOnSubject=97.50), 
                                          'PF13976.1_gag_pre-integrs_GAG_27.0': ProteinDomainMatch("PF13976.1_gag_pre-integrs_GAG_27.0", coverage=80.60, start=1641, strand='+', frame=3, end=1820, index=80.60, coverageOnSubject=80.60), 
                                          'PF14227.1_UBN2_2_GAG_27.0': ProteinDomainMatch("PF14227.1_UBN2_2_GAG_27.0", coverage=96.67, start=657, strand='+', frame=3, end=1001, index=96.67, coverageOnSubject=96.67)
                                          }, 
                             'ORF': ['>3000bps GAG GAG INT RT'], 
                             'Repbase_bx': [iBlasterMatch1, iBlasterMatch2, iBlasterMatch3], 
                             'Repbase_tbx': [iBlasterMatch4, iBlasterMatch5]
                             }   
                   }
        iC.setInfoEvidence(message)
        obs = iC.writeCodingFeaturesLine()
        exp = 'NA'
        self.assertEqual(obs, exp)

    def test_writeStructFeaturesLine(self):
        #  dans cet exemple il n'y a que de l'information qui correspond a la cle 'other', donc il n'y aura que length et ORF qui correspondant a de l'info structurale
        iC = Classif()
        iTR1 = TR("termTIR", 12)
        message = {'length': '>4000bps',
                   'ORF': ['>3000bps GAG GAG INT RT'], 
                   'other': {
                             'SSRCoverage': '0.09', 
                             'TR': [iTR1], 
                             'Other_profiles': {
                                                "PF13961.1_DUF4219_OTHER_27.0": ProteinDomainMatch("PF13961.1_DUF4219_OTHER_27.0", start=462, strand='+', frame=0, end=0, index=0.00, coverageOnSubject=100.00)
                                                }, 
                             'ORF': ['>3000bps GAG GAG INT RT']
                             }
                   }
        iC.setInfoEvidence(message)
        obs = iC.writeStructFeaturesLine()
        exp = 'TElength: >4000bps; ORF: >3000bps GAG GAG INT RT'
        self.assertEqual(obs, exp)        

    def test_writeStructFeaturesLine_withStructAttribut(self):
        iC = Classif()
        iC.setConsensusStructure("TElength: >4000bps; ORF: >3000bps GAG GAG INT RT")
        obs = iC.writeStructFeaturesLine()
        exp = 'TElength: >4000bps; ORF: >3000bps GAG GAG INT RT'
        self.assertEqual(obs, exp)                
        
    def test_writeStructFeaturesLine_withEvidenceAttribut(self):
        iC = Classif()
        iTR1 = TR("termTIR", 12)
        message = {
                   'other': {
                             'SSRCoverage': '0.09', 
                             'TR': [iTR1], 
                             'Other_profiles': {
                                                "PF13961.1_DUF4219_OTHER_27.0": ProteinDomainMatch("PF13961.1_DUF4219_OTHER_27.0", start=462, strand='+', frame=0, end=0, index=0.00, coverageOnSubject=100.00)
                                                }, 
                             'te_hmmer': {
                                          "PF07727.9_RVT_2_RT_22.0": ProteinDomainMatch("PF07727.9_RVT_2_RT_22.0", coverage=99.19, start=3189, strand='+', frame=3, end=3920, index=99.19, coverageOnSubject=99.19), 
                                          'PF00665.21_rve_INT_30.0': ProteinDomainMatch("PF00665.21_rve_INT_30.0", coverage=97.50, start=1863, strand='+', frame=3, end=2216, index=97.50, coverageOnSubject=97.50), 
                                          'PF13976.1_gag_pre-integrs_GAG_27.0': ProteinDomainMatch("PF13976.1_gag_pre-integrs_GAG_27.0", coverage=80.60, start=1641, strand='+', frame=3, end=1820, index=80.60, coverageOnSubject=80.60), 
                                          'PF14227.1_UBN2_2_GAG_27.0': ProteinDomainMatch("PF14227.1_UBN2_2_GAG_27.0", coverage=96.67, start=657, strand='+', frame=3, end=1001, index=96.67, coverageOnSubject=96.67)
                                          }, 
                             'ORF': ['>3000bps GAG GAG INT RT']
                             }
                   }
 
        iC.setInfoEvidence(message)
        obs = iC.writeStructFeaturesLine()
        exp = 'NA'
        self.assertEqual(obs, exp)

        
    def test_formatCodingFeatures(self):
        iBlasterMatch = BlasterMatch("embl|GQ167354|GQ167354 Uncultured eukaryote isolate DGGE gel band B2 18S ribosomal RNA, partial sequence", 98.33, identity = 97.55)
        iBlasterMatch1 = BlasterMatch("Copia-14_AA-I_1p:ClassI:LTR:Copia", 39.24, identity = 0.0)
        iBlasterMatch2 = BlasterMatch("Copia-1_DPer-I_1p:ClassI:LTR:Copia", 22.22, identity = 0.0)
        iBlasterMatch6 = BlasterMatch("Dummy_match", 100.0, identity = 94.0)
        iC = Classif()
        obsList = []
        message = {
                   'rDNA' : iBlasterMatch,
                   'Other_profiles': {
                                      "PF13961.1_DUF4219_OTHER_27.0": ProteinDomainMatch("PF13961.1_DUF4219_OTHER_27.0", start=462, strand='+', frame=0, end=0, index=0.00, coverageOnSubject=100.00)
                                      }, 
                   'te_hmmer': {
                                "PF07727.9_RVT_2_RT_22.0": ProteinDomainMatch("PF07727.9_RVT_2_RT_22.0", coverage=99.19, start=3189, strand='+', frame=3, end=3920, index=99.19, coverageOnSubject=99.19)
                                }, 
                   'ORF': ['>3000bps GAG GAG INT RT'], 
                   'Repbase_bx': [iBlasterMatch1],
                   'Repbase_tbx': [iBlasterMatch2],
                   'HG' : iBlasterMatch6 
                   }
        iC.formatCodingFeatures(message, obsList)
        expList = [
                   "TE_BLRtx: Copia-1_DPer-I_1p:ClassI:LTR:Copia: 22.22%",
                   "TE_BLRx: Copia-14_AA-I_1p:ClassI:LTR:Copia: 39.24%",
                   "profiles: PF07727.9_RVT_2_RT_22.0: 99.19%",
                   "Other_profiles: PF13961.1_DUF4219_OTHER_27.0: 100.00%",
                   "rDNA_BLRn: embl|GQ167354|GQ167354 Uncultured eukaryote isolate DGGE gel band B2 18S ribosomal RNA, partial sequence: 98.33%",
                   "HG_BLRn: Dummy_match: 100.00%"]
        
        self.assertEqual(obsList, expList)

    def test_formatCodingFeatures_withOneFeature(self):
        iBlasterMatch1 = BlasterMatch("Copia-14_AA-I_1p:ClassI:LTR:Copia", 39.24, identity = 0.0)
        iC = Classif()
        obsList = []
        message = { 
                   'ORF': ['>3000bps GAG GAG INT RT'], 
                   'Repbase_bx': [iBlasterMatch1]
                   }
        iC.formatCodingFeatures(message, obsList)
        expList = [
                   "TE_BLRx: Copia-14_AA-I_1p:ClassI:LTR:Copia: 39.24%"
                   ]
        
        self.assertEqual(obsList, expList)

    def test_formatCodingFeatures_withoutFeature(self):
        iC = Classif()
        obsList = []
        message = { 
                   'ORF': ['>3000bps GAG GAG INT RT']
                   }
        iC.formatCodingFeatures(message, obsList)
        expList = []
        
        self.assertEqual(obsList, expList)

    def test_formatStructFeatures(self):
        iHelitronExtremity = HelitronExtremity("HELITRON1_DM:classII:Helitron", 0.0, 1, 564)
        iBlasterMatch4 = BlasterMatch("COPIA_DM_I:ClassI:LTR:Copia", 100.0, identity = 0.0)
        iBlasterMatch5 = BlasterMatch("COPIA_DM_LTR:ClassI:LTR:Copia", 100.0, identity = 0.0)
        iC = Classif()
        obsList = []
        iTR2 = TR("termLTR", 275)
        message = {'length': '>4000bps',
                   'ORF': ['>3000bps GAG GAG INT RT'],
                   'TR': [iTR2], 
                   'Repbase_tbx': [iBlasterMatch4, iBlasterMatch5],
                   'SSRCoverage': '9.00',
                   'SSR' : ['(TAA)3_end'],
                   'polyAtail' : 'yes',
                   'helitronExtremities': [iHelitronExtremity]
                   }
        
        iC.formatStructFeatures(message, obsList)
        expList = [
                   'TElength: >4000bps',
                   'TermRepeats: termLTR: 275',
                   'ORF: >3000bps GAG GAG INT RT',
                   'SSR: (TAA)3_end',
                   'SSRCoverage=9.00%',
                   'polyAtail',
                   'helitronExtremities: HELITRON1_DM:classII:Helitron: (0.0, 1, 564)'
                   ]
        self.assertEqual(obsList, expList)

    def test_formatStructFeatures_withOneFeature(self):
        iBlasterMatch4 = BlasterMatch("COPIA_DM_I:ClassI:LTR:Copia", 100.0, identity = 0.0)
        iBlasterMatch5 = BlasterMatch("COPIA_DM_LTR:ClassI:LTR:Copia", 100.0, identity = 0.0)
        iC = Classif()
        obsList = []
        message = {'length': '>4000bps',
                   'Repbase_tbx': [iBlasterMatch4, iBlasterMatch5]
                   }
        
        iC.formatStructFeatures(message, obsList)
        expList = [
                   'TElength: >4000bps'
                   ]
        self.assertEqual(obsList, expList)

    def test_formatStructFeatures_withoutFeature(self):
        iBlasterMatch4 = BlasterMatch("COPIA_DM_I:ClassI:LTR:Copia", 100.0, identity = 0.0)
        iBlasterMatch5 = BlasterMatch("COPIA_DM_LTR:ClassI:LTR:Copia", 100.0, identity = 0.0)
        iC = Classif()
        obsList = []
        message = {
                   'Repbase_tbx': [iBlasterMatch4, iBlasterMatch5]
                   }
        
        iC.formatStructFeatures(message, obsList)
        expList = []
        self.assertEqual(obsList, expList)

    def test_formatProfilesResults_inHmmerCase(self):
        iC = Classif()
        message = {
                   'PF07727.9_RVT_2_RT_22.0': ProteinDomainMatch("PF07727.9_RVT_2_RT_22.0", coverage=99.19, start=3189, strand='+', frame=3, end=3920, index=99.19, coverageOnSubject=99.19), 
                   'PF00665.21_rve_INT_30.0': ProteinDomainMatch("PF00665.21_rve_INT_30.0", coverage=97.50, start=1863, strand='+', frame=3, end=2216, index=97.50, coverageOnSubject=97.50), 
                   'PF13976.1_gag_pre-integrs_GAG_27.0': ProteinDomainMatch("PF13976.1_gag_pre-integrs_GAG_27.0", coverage=80.60, start=1641, strand='+', frame=3, end=1820, index=80.60, coverageOnSubject=80.60), 
                   }
        
        obsList = iC.formatProfilesResults(message)
        expList = "PF00665.21_rve_INT_30.0: 97.50%, PF07727.9_RVT_2_RT_22.0: 99.19%, PF13976.1_gag_pre-integrs_GAG_27.0: 80.60%"
        self.assertEqual(obsList, expList)
        
    def test_formatProfilesResults_inHmmerCase_withOneFeature(self):
        iC = Classif()
        message = {
                   'PF07727.9_RVT_2_RT_22.0': ProteinDomainMatch("PF07727.9_RVT_2_RT_22.0", coverage=99.19, start=3189, strand='+', frame=3, end=3920, index=99.19, coverageOnSubject=99.19)
                   }
        
        obsList = iC.formatProfilesResults(message)
        expList = "PF07727.9_RVT_2_RT_22.0: 99.19%"
        self.assertEqual(obsList, expList)
        
    def test_formatProfilesResults_inOtherProfilesCase(self):
        iC = Classif()
        message = {
                   "PF13961.1_DUF4219_OTHER_27.0": ProteinDomainMatch("PF13961.1_DUF4219_OTHER_27.0", start=462, strand='+', frame=0, end=0, index=0.00, coverageOnSubject=100.00)
                   }
        
        obsList = iC.formatProfilesResults(message)
        expList = "PF13961.1_DUF4219_OTHER_27.0: 100.00%"
        self.assertEqual(obsList, expList)
        
    def test_formatProfilesResults_withoutFeature(self):
        iC = Classif()
        message = {}        
        obsList = iC.formatProfilesResults(message)
        expList = ""
        self.assertEqual(obsList, expList)
        
    def test_renameLARDTRIMAndMITE(self):
        iC = Classif(consensusOrder = "LTR")
        iC.setInfoEvidence({'LTR':{}})
        iC.renameLARDTRIMAndMITE()
        self.assertEqual(iC.getConsensusOrder(), "LTR")
        self.assertEqual(list(iC.getInfoEvidence().keys()), ["LTR"])
                
        iC = Classif(consensusOrder = "LARD")
        iC.setInfoEvidence({'LARD':{}})
        iC.renameLARDTRIMAndMITE()
        self.assertEqual(iC.getConsensusOrder(), "LTR-LARD")
        self.assertEqual(list(iC.getInfoEvidence().keys()), ["LTR-LARD"])
        
        iC = Classif(consensusOrder = "TRIM")
        iC.setInfoEvidence({'TRIM':{}})
        iC.renameLARDTRIMAndMITE()
        self.assertEqual(iC.getConsensusOrder(), "LTR-TRIM")
        self.assertEqual(list(iC.getInfoEvidence().keys()), ["LTR-TRIM"])
        
        iC = Classif(consensusOrder = "MITE")
        iC.setInfoEvidence({'MITE':{}})
        iC.renameLARDTRIMAndMITE()
        self.assertEqual(iC.getConsensusOrder(), "TIR-MITE")
        self.assertEqual(list(iC.getInfoEvidence().keys()), ["TIR-MITE"])

    def test_setConsensusWCodeFromOrderAndClass(self):
        iClassif = Classif(consensusClass = "I", consensusOrder = "LTR")
        iClassif.setConsensusWCodeFromOrderAndClass()
        obsCode = iClassif.getCode()
        expCode = "RLX"
        self.assertEqual(obsCode, expCode)

        iClassif = Classif(consensusClass = "Unclassified", consensusOrder = "Unclassified")
        iClassif.setConsensusWCodeFromOrderAndClass()
        obsCode = iClassif.getCode()
        expCode = "NA"
        self.assertEqual(obsCode, expCode)

        iClassif = Classif(consensusClass = "ClassII", consensusOrder = "Unclassified")
        iClassif.setConsensusWCodeFromOrderAndClass()
        obsCode = iClassif.getCode()
        expCode = "DXX"
        self.assertEqual(obsCode, expCode)

        iClassif = Classif(consensusClass = "SSR", consensusOrder = "NA")
        iClassif.setConsensusWCodeFromOrderAndClass()
        obsCode = iClassif.getCode()
        expCode = "NA"
        self.assertEqual(obsCode, expCode)

        iClassif = Classif(consensusClass = "NA", consensusOrder = "NA")
        iClassif.setConsensusWCodeFromOrderAndClass()
        obsCode = iClassif.getCode()
        expCode = "NA"
        self.assertEqual(obsCode, expCode)

        iClassif = Classif(outConfuseness = True, consensusClass = "I|NA", consensusOrder = "Unclassified|NA")
        iClassif.setConsensusWCodeFromOrderAndClass()
        obsCode = iClassif.getCode()
        expCode = "RXX|NA"
        self.assertEqual(obsCode, expCode)

        iClassif = Classif(outConfuseness = True, consensusClass = "II|II", consensusOrder = "TIR|Maverick")
        iClassif.setConsensusWCodeFromOrderAndClass()
        obsCode = iClassif.getCode()
        expCode = "DTX|DMX"
        self.assertEqual(obsCode, expCode)

        iClassif = Classif(outConfuseness = True, consensusClass = "II|I", consensusOrder = "TIR|LTR-TRIM")
        iClassif.setConsensusWCodeFromOrderAndClass()
        obsCode = iClassif.getCode()
        expCode = "DTX|RXX-TRIM"
        self.assertEqual(obsCode, expCode)

        iClassif = Classif(outConfuseness = True, consensusClass = "NA|I", consensusOrder = "NA|LTR-TRIM")
        iClassif.setConsensusWCodeFromOrderAndClass()
        obsCode = iClassif.getCode()
        expCode = "NA|RXX-TRIM"
        self.assertEqual(obsCode, expCode)

    def test_setInfoEvidence(self):
        expEvidencesDict = self._writeEvidencesDictNewFormat()
        obsClassif = Classif()
        obsClassif.setInfoEvidence(self._writeEvidencesDictNewFormat())
        self.assertEqual(expEvidencesDict, obsClassif.getInfoEvidence())
    
    def test_setInfoEvidence_useFeatureByFeatures(self):
        expEvidencesDict = self._writeEvidencesDictNewFormat()
        obsClassif = Classif()
        obsClassif.setInfoEvidence(self._writeCodingEvidencesDictNewFormat())
        obsClassif.setInfoEvidence(self._writeStructEvidencesDictNewFormat())
        obsClassif.setInfoEvidence(self._writeOtherEvidencesDictNewFormat())    
        self.assertEqual(expEvidencesDict, obsClassif.getInfoEvidence())
    
    def test_setInfoEvidence_useEmptyFeatures(self):
        expEvidencesDict = {"coding":OrderedDict([]), "struct":OrderedDict([]), "other":OrderedDict([])}
        obsClassif = Classif()
        obsClassif.setInfoEvidence(self._writeEmptyEvidencesDictNewFormat())
        self.assertEqual(expEvidencesDict, obsClassif.getInfoEvidence())

    def test_setInfoEvidenceFromEvidencesDict_withKey_dictOfEvidence(self):
        message = self._writeEvidencesDict()
        iC = Classif()
        iC.setInfoEvidenceFromEvidencesDict(message)
        expCoding = "TE_BLRx: DUMMY:classII:TIR: 100.00%"
        obsCoding = iC.getConsensusCoding()
        self.assertTrue(expCoding, obsCoding)
        expStruct = "TElength: >700bps; helitronExtremities: HELITRON1_DM:classII:Helitron: (0.0, 1, 564)"
        obsStruct = iC.getConsensusStruct()
        self.assertTrue(expStruct, obsStruct)
        expOther = "Other_profiles: PF13961.1_DUF4219_OTHER_27.0: 100.00%; TermRepeats: termTIR: 12; SSRCoverage=0.09"
        obsOther = iC.getConsensusOther()
        self.assertTrue(expOther, obsOther)
    
    def test_setInfoEvidenceFromEvidencesDict_withoutKey_dictOfEvidence(self):
        iBlasterMatch1 = BlasterMatch("Copia-14_AA-I_1p:ClassI:LTR:Copia", 39.24)
        iBlasterMatch4 = BlasterMatch("COPIA_DM_I:ClassI:LTR:Copia", 100.0)
        iBlasterMatch5 = BlasterMatch("COPIA_DM_LTR:ClassI:LTR:Copia", 100.0)
        iC = Classif()
        message = {'length': '>4000bps', 
                   'SSRCoverage': '0.09', 
                   'ORF': ['>3000bps GAG GAG INT RT'], 
                   'Repbase_bx': [iBlasterMatch1], 
                   'Repbase_tbx': [iBlasterMatch4, iBlasterMatch5]
                   }
        iC.setInfoEvidenceFromEvidencesDict(message)
        expCoding = "TE_BLRtx: COPIA_DM_I:ClassI:LTR:Copia: 100.00%, COPIA_DM_LTR:ClassI:LTR:Copia: 100.00%; TE_BLRx: Copia-14_AA-I_1p:ClassI:LTR:Copia: 39.24%"
        obsCoding = iC.getConsensusCoding()
        self.assertTrue(expCoding, obsCoding)
        expStruct = "TElength: >4000bps; SSRCoverage: 0.09; ORF: >3000bps GAG GAG INT RT"
        obsStruct = iC.getConsensusStruct()
        self.assertTrue(expStruct, obsStruct)
        expOther = "NA"
        obsOther = iC.getConsensusOther()
        self.assertTrue(expOther, obsOther)

    def test_createClassifLine( self ):
        #in new format
        expLine = "RLX_denovoMDO_kr-B-G1981-Map5_reversed\t9864\t+\tFalse\tI\tLTR\tRLX\tNA\t35\t" \
                  "coding=(TE_BLRtx: Gypsy-16_Mad-I_#2:ClassI:LTR:Gypsy:?: 12.92%, Gypsy-36_Mad-I:ClassI:LTR:Gypsy:?: 7.39%, Gypsy-44_Mad-LTR:ClassI:LTR:Gypsy:?: 46.07%, Gypsy-48_Mad-I:ClassI:LTR:Gypsy:?: 46.68%, Gypsy-4_PX-I:ClassI:LTR:Gypsy:?: 16.81%, LTR-2_Mad:ClassI:LTR:Gypsy:?: 7.15%, Copia-7_Mad-I:ClassI:LTR:Copia:?: 5.93%; TE_BLRx: Gypsy-4_PX-I_1p:ClassI:LTR:Gypsy:?: 18.89%, Gypsy-46_Mad-I_4p:ClassI:LTR:Gypsy:?: 27.23%)\t" \
                  "struct=(TElength: >4000bps; TermRepeats: termLTR: 2214; ORF: >1000bps)\tother=(TE_BLRtx: L1-3_VC:ClassI:LINE:L1:L1: 5.01%; TElength: >1000bps; profiles: PF00075.19_RNase_H_NA_RH_21.3: 31.06%)\n"
        iClassif = Classif()
        iClassif.createClassifInstanceFromClassifLine(expLine)
        obsLine = iClassif.createClassifLine()
        self.assertEqual(expLine, obsLine)
        #in old format
        line= "PotentialHostGene_LStag_chk16k-B-G334-Map8\t2401\t.\tok\tNA\tPotentialHostGene\tNA\tCI=100; coding=(Other_profiles: PF07645.10_EGF_CA_NA_OTHER_21.0: 80.95%(80.95%)); other=(SSRCoverage=0.16)\n"
        iClassif = Classif()
        iClassif.createClassifInstanceFromClassifLine(line)
        obsLine = iClassif.createClassifLine()
        expLine = "PotentialHostGene_LStag_chk16k-B-G334-Map8\t2401\t.\tFalse\tNA\tNA\tNA\tCI=100; coding=(Other_profiles: PF07645.10_EGF_CA_NA_OTHER_21.0: 80.95%(80.95%)); struct=(); other=(SSRCoverage=0.16)\n"
        self.assertEqual(expLine, obsLine)
    
    def _writeEvidencesDict(self):
        iHelitronExtremity = HelitronExtremity("HELITRON1_DM:classII:Helitron", 0.0, 1, 564)
        iBlasterMatch1 = BlasterMatch('DUMMY:classII:TIR', 100.0)
        iTR1 = TR("termLTR", 1116)
        message = {
                  'consensusName' : 'LStag_chk16k-B-G3361-Map20',
                  'consensusLength': '2989',
                  'consensusStrand': "NA",
                  'chimeric' : "PotentialChimeric",
                  'consensusClass': 'II',
                  'consensusOrder': 'TIR', 
                  'completeness': 'complete',
                  'confidenceIndex' : 2,
                  'dictOfEvidence':
                                {
                                'Repbase_bx' : [iBlasterMatch1],
                                'other': {
                                          'SSRCoverage': '0.09', 
                                          'TR': [iTR1], 
                                          'Other_profiles': {
                                                "PF13961.1_DUF4219_OTHER_27.0": ProteinDomainMatch("PF13961.1_DUF4219_OTHER_27.0", start=462, strand='+', frame=0, end=0, index=0.00, coverageOnSubject=100.00)
                                                }
                                          },   
                               'length' : '>700bps',
                                'helitronExtremities': [iHelitronExtremity],

                                }
                }
        return message
    
    def _writeClassifDictNewFormat(self):              
        return OrderedDict([( "LStag_chk16k-B-G3361-Map20", OrderedDict([
                        ("name", "LStag_chk16k-B-G3361-Map20"), 
                        ("length", 1118), 
                        ("strand", "."), 
                        ("confused", False), 
                        ("class", "Unclassified"), 
                        ("order", "NA"), 
                        ("wCode", "NA"), 
                        ("sFamily", "NA"), 
                        ("CI", "NA"), 
                        ("coding", OrderedDict([])),
                        ("struct", OrderedDict([("TermRepeats", OrderedDict([("non-termLTR", 378), ("SSRCoverage", 0.33)]))])), 
                        ("other", OrderedDict([]))
                        ]))])


    def _writeEvidencesDictNewFormat(self):              
        return OrderedDict([ 
                        ("coding", OrderedDict([
                            ("TE_BLRtx", OrderedDict([
                                ("Crypton-2_TCa:ClassII:Crypton:Crypton", "100.00%")
                                                  ])), 
                                    ("TE_BLRx", OrderedDict([("Crypton-1_TC_1p:ClassII:Crypton:Crypton", "5.26%"),
                                                             ("Crypton-2_TCa_1p:ClassII:Crypton:Crypton", "99.65%")
                                                 ])), 
                                    ("profiles", OrderedDict([("PF00589.17_Phage_integrase_INT_21.6", OrderedDict([('cov', '84.39'), ('covOnSubject', '84.39')]))
                                                  ]))
                        ])),
                        ("struct", OrderedDict([ 
                              ("TElength", ">4000bps"), 
                              ("TermRepeats", OrderedDict([ 
                                  ("non-termLTR", 1188), 
                                  ])), 
                              ("ORF", [
                                  (">1000bps", ""), #if it was a dictionnary, it may get several identical keys
                                  (">3000bps", "INT"), 
                                  (">1000bps", "RT RH") 
                                  ]), 
                              ("SSR", ["(ATAATAAAAAC)3_end", 
                                       "(A)31_end"
                                       ]), 
                              ('helitronExtremities', OrderedDict([
                                  ("ATREP3:ClassII:Helitron:Helitron",OrderedDict([
                                      ("start", 1), 
                                      ("end", 2097), 
                                      ("eValue", 0.0)
                                      ]))
                                   ]))
                          ])), 
                          ("other", OrderedDict([
                                ("Other_profiles", OrderedDict([
                                    ("PF13384.1_HTH_23_OTHER_24.6", OrderedDict([('cov', '70'), ('covOnSubject', '70')]))
                                    ])), 
                                                   ("HG_BLRn", OrderedDict([
                                                       ("name", "FBtr0073959_Dmel_r4.3"), 
                                                       ("cov","21.90")
                                                       ])), 
                                                   ("TermRepeats", OrderedDict([
                                                       ("termLTR", 390), 
                                                       ("non-termLTR", 388), 
                                                       ("termTIR", 390)
                                                       ])), 
                                                   ("SSRCoverage", 0.09)
                                                   ]))
                            ])

    def _writeCodingEvidencesDictNewFormat(self):              
        return OrderedDict([ 
                        ("coding", OrderedDict([
                            ("TE_BLRtx", OrderedDict([
                                ("Crypton-2_TCa:ClassII:Crypton:Crypton", "100.00%")
                                                  ])), 
                                    ("TE_BLRx", OrderedDict([("Crypton-1_TC_1p:ClassII:Crypton:Crypton", "5.26%"),
                                                             ("Crypton-2_TCa_1p:ClassII:Crypton:Crypton", "99.65%")
                                                 ])), 
                                    ("profiles", OrderedDict([("PF00589.17_Phage_integrase_INT_21.6", OrderedDict([('cov', '84.39'), ('covOnSubject', '84.39')]))
                                                  ]))
                        ]))])
    def _writeStructEvidencesDictNewFormat(self):              
        return OrderedDict([ 
                        ("struct", OrderedDict([ 
                              ("TElength", ">4000bps"), 
                              ("TermRepeats", OrderedDict([ 
                                  ("non-termLTR", 1188), 
                                  ])), 
                              ("ORF", [
                                  (">1000bps", ""), #if it was a dictionnary, it may get several identical keys
                                  (">3000bps", "INT"), 
                                  (">1000bps", "RT RH") 
                                  ]), 
                              ("SSR", ["(ATAATAAAAAC)3_end", 
                                       "(A)31_end"
                                       ]), 
                              ('helitronExtremities', OrderedDict([
                                  ("ATREP3:ClassII:Helitron:Helitron",OrderedDict([
                                      ("start", 1), 
                                      ("end", 2097), 
                                      ("eValue", 0.0)
                                      ]))
                                   ]))
                          ]))])

    def _writeOtherEvidencesDictNewFormat(self):              
        return OrderedDict([ 
                          ("other", OrderedDict([
                                ("Other_profiles", OrderedDict([
                                    ("PF13384.1_HTH_23_OTHER_24.6", OrderedDict([('cov', '70'), ('covOnSubject', '70')]))
                                    ])), 
                               ("HG_BLRn", OrderedDict([
                                   ("name", "FBtr0073959_Dmel_r4.3"), 
                                   ("cov","21.90")
                                   ])), 
                               ("TermRepeats", OrderedDict([
                                   ("termLTR", 390), 
                                   ("non-termLTR", 388), 
                                   ("termTIR", 390)
                                   ])), 
                               ("SSRCoverage", 0.09)
                          ]))])

    def _writeEmptyEvidencesDictNewFormat(self):              
        return OrderedDict([ 
                        ("coding", OrderedDict([])),
                        ("struct", OrderedDict([])),
                        ("other", OrderedDict([]))
                        ])

    def test_writeInNewClassifFormat( self ):
        iClassif = Classif()
        line = "blumeria_Grouper_28830_3\t5742\t-\tFalse\tI\tLTR\tRLX\tNA\t50\tcoding=(TE_BLRtx: GYPSY10_I:classI:LTR_retrotransposon: 9.61%; TE_BLRx: ORF1p_TCN4#2:classI:LTR_retrotransposon: 35.45%; profiles: PF00078.19_RVT_1_RT_32.0: 90.91%, PF00078.19_RVT_1_RT_35.2: 90.91%, PF00665.18_rve_INT_32.0: 97.63%, PF00098.15_zf-CCHC_GAG_19.8: 83.33%, PF00665.18_rve_INT_18.8: 97.63%, PF00098.15_zf-CCHC_GAG_17.9: 83.33%)\tstruct=(TElength: >4000bps; TermRepeats: termLTR: 224; ORF: >1000bps, >1000bps)\tother=(NA)"
        iClassif.createClassifInstanceFromClassifLine(line)
        with open("classif", "w") as classifFile:
            iClassif.writeInNewClassifFormat(classifFile)

        with open("expClassif", "w") as expClassif:
            expClassif.write("{}\n".format(line))

        self.assertTrue(FileUtils.are2FilesIdentical("expClassif", "classif"))
        lFiles = ["expClassif", "classif"]
        FileUtils.removeFilesFromList(lFiles)


    def test_writeInOldClassifFormat( self ):
        iClassif = Classif()
        line = "blumeria_Grouper_28830_3\t5742\t-\tFalse\tI\tLTR\tcomplete\tCI=50; coding=(TE_BLRtx: GYPSY10_I:classI:LTR_retrotransposon: 9.61%; TE_BLRx: ORF1p_TCN4#2:classI:LTR_retrotransposon: 35.45%; profiles: PF00078.19_RVT_1_RT_32.0: 90.91%, PF00078.19_RVT_1_RT_35.2: 90.91%, PF00665.18_rve_INT_32.0: 97.63%, PF00098.15_zf-CCHC_GAG_19.8: 83.33%, PF00665.18_rve_INT_18.8: 97.63%, PF00098.15_zf-CCHC_GAG_17.9: 83.33%); struct=(TElength: >4000bps; TermRepeats: termLTR: 224; ORF: >1000bps, >1000bps); other=(NA)"
        iClassif.createClassifInstanceFromClassifLine(line)
        with open("classif", "w") as classifFile:
            iClassif.writeInOldClassifFormat(classifFile)

        with open("expClassif", "w") as expClassif:
            expClassif.write("{}\n".format(line))

        self.assertTrue(FileUtils.are2FilesIdentical("expClassif", "classif"))
        lFiles = ["expClassif", "classif"]
        FileUtils.removeFilesFromList(lFiles)


if __name__ == "__main__":
    unittest.main()