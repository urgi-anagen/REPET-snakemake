import unittest
import time
import os
import subprocess
import shutil
from commons.core.utils.FileUtils import FileUtils
from commons.core.utils.ConvertClassif9ColTo12Col import ConvertClassif9ColTo12Col


class Test_F_ConvertClassif9ColTO12Col( unittest.TestCase ):
    
    def setUp( self ):
        self._uniqId = "{}_{}".format( time.strftime("%Y%m%d%H%M%S") , os.getpid() )
        self._curDir = os.getcwd()
        self._workdir = "Conversion_{}".format(self._uniqId)
        os.makedirs(self._workdir)
        os.chdir(self._workdir)
        self._inClassifFile = "ClassifWith9Col.classif"
        
    def tearDown( self ):
        self._uniqId = None
        os.chdir(self._curDir)
        shutil.rmtree(self._workdir)
        
    def test_run(self):
        self._writeInputClassifFile(self._inClassifFile)
        iCC = ConvertClassif9ColTo12Col(self._inClassifFile)
        iCC.run()
        expFileName = "expConvertClassifFile.classif"
        self._writeExpClassifFile(expFileName)
        obsFileName = "ClassifWith9Col.classif_new.classif"        
        self.assertTrue(FileUtils.are2FilesIdentical(expFileName, obsFileName))
        expStatsFileName = "exp.classif_new.classif_stats.txt"
        self._writeExpStatsFile(expStatsFileName)
        obsStatsFileName = "ClassifWith9Col.classif_new.classif_stats.txt"        
        self.assertTrue(FileUtils.isRessourceExists(obsStatsFileName))
        self.assertTrue(FileUtils.are2FilesIdentical(expStatsFileName, obsStatsFileName))

    def test_run_withOutputFile(self):
        self._writeInputClassifFile(self._inClassifFile)
        obsFileName = "ClassifWith12Col.classif"
        iCC = ConvertClassif9ColTo12Col(self._inClassifFile,obsFileName)
        iCC.run()
        expFileName = "expConvertClassifFile.classif"
        self._writeExpClassifFile(expFileName)        
        self.assertTrue(FileUtils.are2FilesIdentical(expFileName, obsFileName))

    def test_run_asScript(self):
        self._writeInputClassifFile(self._inClassifFile)
        obsFileName = "ClassifWith12Col.classif"
        cmd = "python3 ../../ConvertClassif9ColTo12Col.py -i {} -o {} -v 3 ".format(self._inClassifFile, obsFileName)
        subprocess.call(cmd, shell=True)
        expFileName = "expConvertClassifFile.classif"
        self._writeExpClassifFile(expFileName)        
        self.assertTrue(FileUtils.are2FilesIdentical(expFileName, obsFileName))

    def test_run_asScript_withVerbosity_withoutStats(self):
        self._writeInputClassifFile(self._inClassifFile)
        obsFileName = "ClassifWith12Col.classif"
        cmd = "python3 ../../ConvertClassif9ColTo12Col.py -i {} -o {} -s -v 3 ".format(self._inClassifFile, obsFileName)
        subprocess.call(cmd, shell=True)
        expFileName = "expConvertClassifFile.classif"
        self._writeExpClassifFile(expFileName)
        statsFileName = "ClassifWith12Col.classif_stats.txt"        
        self.assertFalse(FileUtils.isRessourceExists(statsFileName))

    def test_run_wrongInputFile(self):
        self._writeInputClassifFile("truc")
        obsFileName = "ClassifWith12Col.classif"
        iCC = ConvertClassif9ColTo12Col(self._inClassifFile, obsFileName)
        self.assertRaises(SystemExit, iCC.run)

    def test_run_wrongInputFileExtension(self):
        self._inClassifFile = "truc.fa"
        self._writeInputClassifFile(self._inClassifFile)
        obsFileName = "ClassifWith12Col.classif"
        iCC = ConvertClassif9ColTo12Col(self._inClassifFile, obsFileName)

        self.assertRaises(SystemExit, iCC.run)

    def test_run_wrongOutputFileExtension(self):
        self._writeInputClassifFile(self._inClassifFile)
        obsFileName = "ClassifWith12Col.fa"
        iCC = ConvertClassif9ColTo12Col(self._inClassifFile, obsFileName)

        self.assertRaises(SystemExit, iCC.run)

    def test_run_withoutInputFile(self):
        iCC = ConvertClassif9ColTo12Col()
        self.assertRaises(SystemExit, iCC.run)

    def _writeInputClassifFile(self, fileName):
        with open(fileName, "w") as f:
            f.write("RLC-comp_denovoMDO_kr-B-G6143-Map8\t2239\t+\tok\tI\tLTR\tcomplete\tCI=7; coding=(TE_BLRtx: Copia-98_Mad-I:ClassI:LTR:Copia:?: 69.02%, Copia-98_Mad-LTR:ClassI:LTR:Copia:?: 55.78%); struct=(TElength: >700bps; TermRepeats: termLTR: 651); other=(SSRCoverage=0.12)\n")
            f.write("RLG-incomp_denovoMDO_kr-B-R2493-Map10_reversed\t2004\t+\tok\tI\tLTR\tincomplete\tCI=14; coding=(TE_BLRtx: Gypsy-30_Mad-LTR:ClassI:LTR:Gypsy:?: 9.02%, Gypsy-32_Mad-I:ClassI:LTR:Gypsy:?: 23.17%, Gypsy-33_Mad-I:ClassI:LTR:Gypsy:?: 29.59%, Gypsy-33_Mad-LTR:ClassI:LTR:Gypsy:?: 86.76%); struct=(TElength: >700bps); other=(SSRCoverage=0.27)\n")
            f.write("RLX_accord\t7404\t+\tok\tI\tLTR\tcomplete\tCI=71; coding=(TE_BLRtx: ACCORD_I:classI:LTR_retrotransposon: 100.00%, ACCORD_LTR:classI:LTR_retrotransposon: 100.00%; TE_BLRx: ENV#2:classI:LTR_retrotransposon: 19.66%, GAG#2:classI:LTR_retrotransposon: 73.13%, GYPSY5_I_1p:classI:LTR_retrotransposon: 10.91%, GYPSY5_I_2p:classI:LTR_retrotransposon: 99.48%, Gypsy5-NVi_I_1p:classI:LTR_retrotransposon: 7.83%; profiles: rve_INT_32.0: 100.00%(100.00%), RVT_1_RT_32.0: 100.00%(100.00%), RVP_AP_30.9: 84.82%(84.82%)); struct=(TElength: >4000bps; TermRepeats: termLTR: 558; ORF: >1000bps, >3000bps AP RT INT); other=(SSRCoverage=0.00)\n")
            f.write("RIX_baggins\t5453\t+\tok\tI\tLINE\tincomplete\tCI=72; coding=(TE_BLRx: RandI-3p:classI:LINE: 5.12%; profiles: Exo_endo_phos_EN_11.0: 57.29%(57.29%), RnaseH_RH_7.5: 100.00%(100.00%), RVT_1_RT_32.0: 100.00%(100.00%), Exo_endo_phos_EN_19.6: 42.21%(42.21%)); struct=(TElength: >1000bps; ORF: >1000bps, >3000bps EN EN RT RH; SSR: (TAA)3_end); other=(TE_BLRtx: BAGGINS1:classI:?: 100.00%; TE_BLRx: Baggins-1_NVi_1p:classI:?: 96.83%; SSRCoverage=0.01)\n")
            f.write("rDNA_GQ167353\t200\t.\tok\tNA\trDNA\tNA\tCI=100; coding=(rDNA_BLRn: embl|GQ167353|GQ167353 Uncultured eukaryote isolate DGGE gel band B1 18S ribosomal RNA, partial sequence.: 100.00%); other=(SSRCoverage=0.00)\n")
            f.write("RIX-comp_PENELOPE:classI:LINE\t2780\t+\tok\tI\tLINE\tcomplete\tCI=45; coding=(TE_BLRtx: PENELOPE:classI:LINE: 100.00%; TE_BLRx: PENELOPE_1p:classI:LINE: 100.00%; profiles: PF00078.22_RVT_1_RT_20.7: 80.37%(80.37%)); struct=(TElength: >1000bps; SSR: (AAATATGG)2_end); other=(SSRCoverage=0.19)\n")
            f.write("RSX-incomp_SINE3-2_AO\t3294\t+\tok\tI\tSINE\tincomplete\tCI=20; coding=(TE_BLRtx: SINE3-2_AO:classI:SINE: 100.00%); struct=(TElength: >1000bps); other=(Other_profiles: PF11917.3_DUF3435_OTHER_25.8: 54.31%(54.31%); rDNA_BLRn: embl|J01890|J01890 Thermomyces lanuginosus 5S ribosomal RNA.: 1.88%; SSRCoverage=0.03)\n")
            f.write("noCat_UCON3\t172\t.\tok\tnoCat\tnoCat\tNA\tCI=NA; struct=(SSRCoverage=0.00)\n")
            f.write("SSR_Short_Consensus\t16\t.\tok\tNA\tSSR\tNA\tCI=100; struct=(TElength: <100bps; SSRCoverage=0.88); other=(SSR: (CAGATGA)2_end)\n")
            f.write("RLX-incomp-chim_DIRS-1_CB:classI:LTR_retrotransposon\t4357\t+\tPotentialChimeric\tI\tLTR\tincomplete\tCI=42; coding=(TE_BLRtx: DIRS-1_CB:classI:LTR_retrotransposon: 99.95%; TE_BLRx: DIRS1_CB_ORF1:classI:LTR_retrotransposon: 84.19%, DIRS1_CB_ORF2:classI:LTR_retrotransposon: 100.00%; profiles: PF00078.22_RVT_1_RT_20.7: 89.25%(89.25%), PF13456.1_RVT_3_RT_21.6: 68.18%(68.18%)); struct=(TElength: >4000bps; ORF: >1000bps RT RT); other=(TermRepeats: non-termLTR: 148; SSRCoverage=0.09)\n")
            f.write("DXX-chim_POLINTN1_SM:classII:Polinton_reversed\t3153\t-\tPotentialChimeric\tII\tTIR\tNA\tCI=12; coding=(TE_BLRtx: POLINTN1_SM:classII:Polinton: 34.22%, POLINTN1_SM:classII:Polinton: 64.57%); struct=(TermRepeats: termTIR: 1216; SSRCoverage=0.44)\n")
            f.write("DMX-incomp-chim_Polinton3_SM:classII:Polinton\t2836\t+\tPotentialChimeric\tII\tMaverick\tincomplete\tCI=37; coding=(TE_BLRtx: Polinton3_SM:classII:Polinton: 94.08%; TE_BLRx: POLB-2_SM:classII:Polinton: 10.60%, Polinton3_SM_1p:classII:Polinton: 100.00%; profiles: PF02945.10_Endonuclease_7_EN_21.0: 46.34%(46.34%)); struct=(TElength: <10000bps; TermRepeats: termTIR: 105); other=(SSRCoverage=0.21)\n")


    def _writeExpClassifFile(self, fileName):
        with open(fileName, "w") as f:
            f.write("Seq_name\tlength\tstrand\tconfused\tclass\torder\tWcode\tsFamily\tCI\tcoding\tstruct\tother\n")
            f.write("RLC_denovoMDO_kr-B-G6143-Map8\t2239\t+\tFalse\tI\tLTR\tRLX\tNA\t7\tcoding=(TE_BLRtx: Copia-98_Mad-I:ClassI:LTR:Copia:?: 69.02%, Copia-98_Mad-LTR:ClassI:LTR:Copia:?: 55.78%)\tstruct=(TElength: >700bps; TermRepeats: termLTR: 651)\tother=(SSRCoverage=0.12)\n")
            f.write("RLG_denovoMDO_kr-B-R2493-Map10_reversed\t2004\t+\tFalse\tI\tLTR\tRLX\tNA\t14\tcoding=(TE_BLRtx: Gypsy-30_Mad-LTR:ClassI:LTR:Gypsy:?: 9.02%, Gypsy-32_Mad-I:ClassI:LTR:Gypsy:?: 23.17%, Gypsy-33_Mad-I:ClassI:LTR:Gypsy:?: 29.59%, Gypsy-33_Mad-LTR:ClassI:LTR:Gypsy:?: 86.76%)\tstruct=(TElength: >700bps)\tother=(SSRCoverage=0.27)\n")
            f.write("RLX_accord\t7404\t+\tFalse\tI\tLTR\tRLX\tNA\t71\tcoding=(TE_BLRtx: ACCORD_I:classI:LTR_retrotransposon: 100.00%, ACCORD_LTR:classI:LTR_retrotransposon: 100.00%; TE_BLRx: ENV#2:classI:LTR_retrotransposon: 19.66%, GAG#2:classI:LTR_retrotransposon: 73.13%, GYPSY5_I_1p:classI:LTR_retrotransposon: 10.91%, GYPSY5_I_2p:classI:LTR_retrotransposon: 99.48%, Gypsy5-NVi_I_1p:classI:LTR_retrotransposon: 7.83%; profiles: rve_INT_32.0: 100.00%(100.00%), RVT_1_RT_32.0: 100.00%(100.00%), RVP_AP_30.9: 84.82%(84.82%))\tstruct=(TElength: >4000bps; TermRepeats: termLTR: 558; ORF: >1000bps, >3000bps AP RT INT)\tother=(SSRCoverage=0.00)\n")
            f.write("RIX_baggins\t5453\t+\tFalse\tI\tLINE\tRIX\tNA\t72\tcoding=(TE_BLRx: RandI-3p:classI:LINE: 5.12%; profiles: Exo_endo_phos_EN_11.0: 57.29%(57.29%), RnaseH_RH_7.5: 100.00%(100.00%), RVT_1_RT_32.0: 100.00%(100.00%), Exo_endo_phos_EN_19.6: 42.21%(42.21%))\tstruct=(TElength: >1000bps; ORF: >1000bps, >3000bps EN EN RT RH; SSR: (TAA)3_end)\tother=(TE_BLRtx: BAGGINS1:classI:?: 100.00%; TE_BLRx: Baggins-1_NVi_1p:classI:?: 96.83%; SSRCoverage=0.01)\n")
            f.write("rDNA_GQ167353\t200\t.\tFalse\tNA\tNA\tPrDNA\tNA\t100\tcoding=(rDNA_BLRn: embl|GQ167353|GQ167353 Uncultured eukaryote isolate DGGE gel band B1 18S ribosomal RNA, partial sequence.: 100.00%)\tstruct=(NA)\tother=(SSRCoverage=0.00)\n")
            f.write("RIX_PENELOPE:classI:LINE\t2780\t+\tFalse\tI\tLINE\tRIX\tNA\t45\tcoding=(TE_BLRtx: PENELOPE:classI:LINE: 100.00%; TE_BLRx: PENELOPE_1p:classI:LINE: 100.00%; profiles: PF00078.22_RVT_1_RT_20.7: 80.37%(80.37%))\tstruct=(TElength: >1000bps; SSR: (AAATATGG)2_end)\tother=(SSRCoverage=0.19)\n")
            f.write("RSX_SINE3-2_AO\t3294\t+\tFalse\tI\tSINE\tRSX\tNA\t20\tcoding=(TE_BLRtx: SINE3-2_AO:classI:SINE: 100.00%)\tstruct=(TElength: >1000bps)\tother=(Other_profiles: PF11917.3_DUF3435_OTHER_25.8: 54.31%(54.31%); rDNA_BLRn: embl|J01890|J01890 Thermomyces lanuginosus 5S ribosomal RNA.: 1.88%; SSRCoverage=0.03)\n")
            f.write("noCat_UCON3\t172\t.\tFalse\tUnclassified\tUnclassified\tXXX\tNA\tNA\tcoding=(NA)\tstruct=(SSRCoverage=0.00)\tother=(NA)\n")
            f.write("SSR_Short_Consensus\t16\t.\tFalse\tNA\tNA\tSSR\tNA\t100\tcoding=(NA)\tstruct=(TElength: <100bps; SSRCoverage=0.88)\tother=(SSR: (CAGATGA)2_end)\n")
            f.write("RLX_DIRS-1_CB:classI:LTR_retrotransposon\t4357\t+\tTrue\tI|NA\tLTR|NA\tRLX|NA\tNA|NA\t42|NA\tcoding=(TE_BLRtx: DIRS-1_CB:classI:LTR_retrotransposon: 99.95%; TE_BLRx: DIRS1_CB_ORF1:classI:LTR_retrotransposon: 84.19%, DIRS1_CB_ORF2:classI:LTR_retrotransposon: 100.00%; profiles: PF00078.22_RVT_1_RT_20.7: 89.25%(89.25%), PF13456.1_RVT_3_RT_21.6: 68.18%(68.18%))\tstruct=(TElength: >4000bps; ORF: >1000bps RT RT)\tother=(TermRepeats: non-termLTR: 148; SSRCoverage=0.09)\n")
            f.write("DXX_POLINTN1_SM:classII:Polinton_reversed\t3153\t-\tTrue\tII|NA\tTIR|NA\tDTX|NA\tNA|NA\t12|NA\tcoding=(TE_BLRtx: POLINTN1_SM:classII:Polinton: 34.22%, POLINTN1_SM:classII:Polinton: 64.57%)\tstruct=(TermRepeats: termTIR: 1216; SSRCoverage=0.44)\tother=(NA)\n")
            f.write("DMX_Polinton3_SM:classII:Polinton\t2836\t+\tTrue\tII|NA\tMaverick|NA\tDMX|NA\tNA|NA\t37|NA\tcoding=(TE_BLRtx: Polinton3_SM:classII:Polinton: 94.08%; TE_BLRx: POLB-2_SM:classII:Polinton: 10.60%, Polinton3_SM_1p:classII:Polinton: 100.00%; profiles: PF02945.10_Endonuclease_7_EN_21.0: 46.34%(46.34%))\tstruct=(TElength: <10000bps; TermRepeats: termTIR: 105)\tother=(SSRCoverage=0.21)\n")

    def _writeExpStatsFile(self, fileName):
        with open(fileName, "w") as f:
            f.write("Nb total consensus: 12\n")
            f.write("\n")
            f.write("Not confused consensus\n")
            f.write("\n")
            f.write("\t\tFrom ClassI:\n")
            f.write("\t\t\tLINE total (RIX): 2 (16.67%)\n")
            f.write("\t\t\tLTR total (RLX): 3 (25.00%)\n")
            f.write("\t\t\tSINE total (RSX): 1 (8.33%)\n")
            f.write("\t\tClassI total: 6 (50.00%)\n")
            f.write("\n")
            f.write("\tUnclassified at class and order: 1 (8.33%)\n")
            f.write("\n")
            f.write("\tFrom not TEs:\n")
            f.write("\t\tPrDNA total: 1 (8.33%)\n")
            f.write("\t\tSSR total: 1 (8.33%)\n")
            f.write("\tNot TEs total: 2 (16.67%)\n")
            f.write("\n")
            f.write("Not confused consensus total: 9 (75.00%)\n")
            f.write("\n")
            f.write("Confused consensus\n")
            f.write("\n")
            f.write("\tConfused at Class and order: 3 (25.00%)\n")
            f.write("\n")
            f.write("Confused consensus total: 3 (25.00%)\n")
            f.write("\n")
            f.write("#############NOTES#############\n")
            f.write("\n")
            f.write("Read  'A unified classification system for eukaryotic transposable elements'. Wicker T. et al 2007, Nat Rev Genet. 8:973-982\n")
            f.write("1) Consensus with several orders or several class and orders are confused, the proposed classifications are presented order by decreased confidence index\n")
            f.write("\tseparated by |. All features from major classification appeared in 'coding' end 'struct' colomns. If the features from the other classifications are different\n")
            f.write("\tthey are displayed in 'others' colomns.\n")
            f.write("\tIf there is an None TE classification with TE classifcation, this consensus is not confused but the features from none TE classification are displayed in 'others' colomns.\n")
            f.write("2) All percentages are calculated with total number of consensus\n")
            f.write("3) Following all classifications that can be found, with Wicker's code:\n")
            f.write("\n")
            f.write("Transposable elements (TEs):\n")
            f.write("\tClass I (RXX)\n")
            f.write("\t\tDIRS (RYX)\n")
            f.write("\t\tLARD (RXX-LARD)\n")
            f.write("\t\tLINE (RIX)\n")
            f.write("\t\tLTR Copia (RLC)\n")
            f.write("\t\tLTR Gypsy (RLG)\n")
            f.write("\t\tLTR (RLX)\n")
            f.write("\t\tPLE (RPX)\n")
            f.write("\t\tSINE (RSX)\n")
            f.write("\t\tTRIM (RXX-TRIM)\n")
            f.write("\n")
            f.write("\tClass II (DXX)\n")
            f.write("\t\tCrypton (DYX)\n")
            f.write("\t\tHelitron (DHX)\n")
            f.write("\t\tMITE (DXX-MITE)\n")
            f.write("\t\tMaverick (DMX)\n")
            f.write("\t\tTIR (DTX)\n")
            f.write("\n")
            f.write("\tUnclassified (sequence not classified at class AND order levels) (XXX)\n")
            f.write("\n")
            f.write("Not transposable elements :\n")
            f.write("\tPHG for PotentialHostGene\n")
            f.write("\tPrDNA for PotentialrDNA\n")
            f.write("\tSSR for Small Simple Repeat\n")
            f.write("\n")
            f.write("If a classification is not in the statistical results, it means that none of the analyzed consensus has been classified in this way.\n")


if __name__ == "__main__":
    unittest.main()