import random

import shutil
import unittest
import os
import time
from collections import OrderedDict

from commons.core.utils.Classif import Classif
from commons.core.utils.FileUtils import FileUtils
from commons.core.utils.ClassifUtils import ClassifUtils

class Test_ClassifUtils(unittest.TestCase):
    
    def setUp(self):
        self._uniqId = "{}_{}".format(time.strftime("%Y%m%d%H%M%S"), os.getpid())
        self._curTestDir = os.getcwd()
        self._testPrefix = 'test_CU_{}_{}'.format(time.strftime("%H%M%S"), random.randint(0, 1000))
        try:
            os.makedirs(self._testPrefix)
        except:pass
        os.chdir(self._testPrefix)

    def tearDown( self ):
        os.chdir(self._curTestDir)
        try:
            shutil.rmtree(self._testPrefix)
        except:pass

    def test_getNewClassifLineAsDict_miscellaneous(self):
        line = "DHX-comp_Azer\t1234\t.\tFalse\tII\tHelitron\tDHX\tNA\t99\tcoding=(TE_BLRtx: Foo:Helitron: 86.84%, " \
               "Bar:Helitron: 80.12%; TE_BLRx: Foo:Gypsy: 72.87%; profiles: _Foo_Profile: 94.23%(94.23%))\tstruct=(" \
               "TElength: >4000bps; TermRepeats: termLTR: 290; ORF: >1000bps GAG, >1000bps AP; helitronExtremities: " \
               "Foo:classII:Helitron: (2e-39, 1, 82))\tother=(Other_profiles: Foo.Bar_OTHER_P: 188.12%(99.81%); " \
               "SSRCoverage=18.00%)"
              
        preExpDict = [("name", "DHX-comp_Azer"),
                         ("length", 1234),
                         ("strand", "."),
                         ("confused", False),
                         ("class", "II"),
                         ("order", "Helitron"),
                         ("wCode", "DHX"),
                         ("sFamily", "NA"),
                         ("CI", '99'),
                         ("coding", OrderedDict([
                             ("TE_BLRtx", OrderedDict([
                                 ("Foo:Helitron", "86.84%"),
                                 ("Bar:Helitron", "80.12%")
                                 ])),
                             ("TE_BLRx", OrderedDict([
                                 ("Foo:Gypsy", "72.87%")
                                 ])),
                             ("profiles", OrderedDict([
                                 ("_Foo_Profile", OrderedDict([
                                     ("cov", "94.23"),
                                     ("covOnSubject", "94.23")
                                 ]))
                             ]))
                         ])),
                         ("struct", OrderedDict([
                             ("TElength", ">4000bps"),
                             ("TermRepeats", OrderedDict([
                                 ("termLTR", 290)
                             ])),
                             ("ORF", [('>1000bps', 'GAG'), ('>1000bps', 'AP')]),
                             ("helitronExtremities", OrderedDict([
                                 ("Foo:classII:Helitron", OrderedDict([
                                     ("start", 1),
                                     ("end", 82),
                                     ("eValue", 2e-39)
                                 ]))
                             ]))
                         ])),
                         ("other", OrderedDict([
                             ("Other_profiles", OrderedDict([
                                 ("Foo.Bar_OTHER_P", OrderedDict([
                                     ("cov", "188.12"),
                                     ("covOnSubject", "99.81")
                                 ]))
                             ])),
                             ("SSRCoverage", 18.00)
                         ]))
                     ]
          
        expDict = OrderedDict(preExpDict)
        obsDict = ClassifUtils.getNewClassifLineAsDict(line)
          
        self.assertEqual(expDict, obsDict)
 
    def test_getNewClassifLineAsDict_coding(self):
        line = "RLX-incomp_Azer\t1234\t+\tFalse\tI\tLTR\tRLX\tNA\t99\t" \
                "coding=(TE_BLRtx: Foo:Helitron: 86.84%, Bar:Helitron: 80.12%; TE_BLRx: Foo:Gypsy: 72.87%; " \
                "profiles: _Foo_Profile: 94.23%(94.23%); Other_profiles: PF01__OTHER_25.0: 100.00%(100.00%); " \
                "rDNA_BLRn: embl|GQ167354|GQ167354 Uncultured eukaryote isolate DGGE gel band B2 18S ribosomal RNA, partial sequence: 100.00%; " \
                "HG_BLRn: Dummy_match: 98.04%)\t" \
                "struct=(NA)\tother=(NA)"
          
        preExpDict = [("name", "RLX-incomp_Azer"),
                         ("length", 1234),
                         ("strand", "+"),
                         ("confused", False),
                         ("class", "I"),
                         ("order", "LTR"),
                         ("wCode", "RLX"),
                         ("sFamily", "NA"),
                         ("CI", '99'),
                         ("coding", OrderedDict([
                             ("TE_BLRtx", OrderedDict([
                                 ('Foo:Helitron', '86.84%'),
                                 ('Bar:Helitron', '80.12%')
                                 ])),
                             ("TE_BLRx", OrderedDict([
                                 ('Foo:Gypsy', '72.87%')
                                 ])),
                             ("profiles", OrderedDict([
                                 ("_Foo_Profile", OrderedDict([
                                     ("cov", "94.23"),
                                     ("covOnSubject", "94.23")
                                 ]))
                             ])),
                             ("Other_profiles", OrderedDict([
                                 ("PF01__OTHER_25.0", OrderedDict([
                                     ("cov", "100.00"),
                                     ("covOnSubject", "100.00")
                                 ]))
                             ])),
                             ("rDNA_BLRn", OrderedDict([
                                 ("name", "embl|GQ167354|GQ167354 Uncultured eukaryote isolate DGGE gel band B2 18S ribosomal RNA, partial sequence"),
                                 ("cov", "100.00")
                             ])),
                             ("HG_BLRn", OrderedDict([
                                 ("name", "Dummy_match"),
                                 ("cov", "98.04")
                             ]))
                         ])),
                         ("struct", 'NA'),
                         ("other", 'NA')
                     ]
          
        expDict = OrderedDict(preExpDict)
        obsDict = ClassifUtils.getNewClassifLineAsDict(line)

        self.assertEqual(expDict, obsDict)

    def test_getNewClassifLineAsDict_coding_case2(self):
        line = "Atha_TEdenovoGr-B-G1633-Map3\t509\t.\tFalse\tNA\tNA\trDNA\tNA\t100\t" \
                "coding=(rDNA_BLRn: embl|AY848693|AY848693 Vitis pseudoreticulata clone EST-4 23S ribosomal RNA, partial sequence, chloroplast.: 96.27%)\t" \
                "struct=(NA)\tother=(NA)"

        preExpDict = [("name", "Atha_TEdenovoGr-B-G1633-Map3"),
                      ("length", 509),
                      ("strand", "."),
                      ("confused", False),
                      ("class", "NA"),
                      ("order", "NA"),
                      ("wCode", "rDNA"),
                      ("sFamily", "NA"),
                      ("CI", '100'),
                      ("coding", OrderedDict([
                          ("rDNA_BLRn", OrderedDict([
                              ("name",
                               "embl|AY848693|AY848693 Vitis pseudoreticulata clone EST-4 23S ribosomal RNA, partial sequence, chloroplast."),
                              ("cov", "96.27")
                          ])),
                      ])),
                      ("struct", "NA"),
                      ("other", "NA")
                      ]

        expDict = OrderedDict(preExpDict)
        obsDict = ClassifUtils.getNewClassifLineAsDict(line)

        self.assertEqual(expDict, obsDict)

    def test_getNewClassifLineAsDict_struct(self):
        line = "DHX-comp_Qwer\t1234\t.\tFalse\tII\tHelitron\tDHX\tNA\t99\tcoding=()\tstruct=(TElength: >4000bps; TermRepeats: termLTR: 290; ORF: >1000bps GAG, >1000bps AP; " \
                "SSR: (AT)30_end; SSRCoverage=6.00%)\tother=()"
              
        preExpDict = [("name", "DHX-comp_Qwer"),
                         ("length", 1234),
                         ("strand", "."),
                         ("confused", False),
                         ("class", "II"),
                         ("order", "Helitron"),
                         ("wCode", "DHX"),
                         ("sFamily", "NA"),
                         ("CI", '99'),
                         ("coding", 'NA'),
                         ("struct", OrderedDict([
                             ("TElength", ">4000bps"),
                             ("TermRepeats", OrderedDict([("termLTR", 290)])),
                             ("ORF", [('>1000bps', 'GAG'), ('>1000bps', 'AP')]),
                             ("SSR", ["(AT)30_end"]),
                             ("SSRCoverage", 6.00)
                         ])),
                         ("other", 'NA')
                     ]
          
        expDict = OrderedDict(preExpDict)
        obsDict = ClassifUtils.getNewClassifLineAsDict(line)
  
        self.assertEqual(expDict, obsDict)
 
    def test_getNewClassifLineAsDict_other(self):
        line = "DHX-comp_Abcd\t1234\t-\tTrue\tII\tHelitron\tDHX\tNA\t99\tcoding=()\tstruct=()\t" \
                "other=(TE_BLRtx: Foo:Helitron: 86.84%, Bar:Helitron: 80.12%; TE_BLRx: Foo:Gypsy: 72.87%; " \
                "profiles: _Foo_Profile: 94.23%(94.23%); Other_profiles: PF01__OTHER_25.0: 100.00%(100.00%); " \
                "rDNA_BLRn: embl|GQ167354|GQ167354 Uncultured eukaryote isolate DGGE gel band B2 18S ribosomal RNA, partial sequence: 100.00%; " \
                "HG_BLRn: Dummy_match: 98.04%; TElength: >4000bps; TermRepeats: termLTR: 290; ORF: >1000bps GAG, >1000bps AP; " \
                "SSR: (AT)30_end; SSRtrf: (AT)30_end; SSRCoverage=6.00%; polyAtail; helitronExtremities: Foo:classII:Helitron: (2e-39, 1, 82))"
              
        preExpDict = [("name", "DHX-comp_Abcd"),
                         ("length", 1234),
                         ("strand", "-"),
                         ("confused", True),
                         ("class", "II"),
                         ("order", "Helitron"),
                         ("wCode", "DHX"),
                         ("sFamily", "NA"),
                         ("CI", '99'),
                         ("coding", 'NA'),
                         ("struct", 'NA'),
                         ("other", OrderedDict([
                             ("TE_BLRtx", OrderedDict([
                                 ('Foo:Helitron', '86.84%'),
                                 ('Bar:Helitron', '80.12%')
                                 ])),
                             ("TE_BLRx", OrderedDict([
                                 ('Foo:Gypsy', '72.87%')
                                 ])),
                             ("profiles", OrderedDict([
                                 ("_Foo_Profile", OrderedDict([
                                     ("cov", "94.23"),
                                     ("covOnSubject", "94.23")
                                 ])),
                             ])),
                             ("Other_profiles", OrderedDict([
                                 ("PF01__OTHER_25.0", OrderedDict([
                                     ("cov", "100.00"),
                                     ("covOnSubject", "100.00")
                                 ]))
                             ])),
                             ("rDNA_BLRn", OrderedDict([
                                 ("name", "embl|GQ167354|GQ167354 Uncultured eukaryote isolate DGGE gel band B2 18S ribosomal RNA, partial sequence"),
                                 ("cov", "100.00")
                             ])),
                             ("HG_BLRn", OrderedDict([
                                 ("name", "Dummy_match"),
                                 ("cov", "98.04")
                             ])),
                             ("TElength", ">4000bps"),
                             ("TermRepeats", OrderedDict([
                                 ("termLTR", 290)
                             ])),
                             ("ORF", [('>1000bps', 'GAG'), ('>1000bps', 'AP')]),
                             ("SSR", ["(AT)30_end"]),
                             ("SSRtrf", ["(AT)30_end"]),
                             ("SSRCoverage", 6.00),
                             ("polyAtail", True),
                             ("helitronExtremities", OrderedDict([
                                 ("Foo:classII:Helitron", OrderedDict([
                                     ("start", 1),
                                     ("end", 82),
                                     ("eValue", 2e-39)
                                 ]))
                             ])
                              )]))
                      ]
          
        expDict = OrderedDict(preExpDict)
        obsDict = ClassifUtils.getNewClassifLineAsDict(line)
          
        self.assertEqual(expDict, obsDict)

    def test_getClassifInfosAsDict_withNewClassifFormat_withHeaders(self):
        dummyClassifFile = "dummyClassif_{}.classif".format(self._uniqId)
        with open(dummyClassifFile, "w") as f:
            f.write("Name\tLength\tStrand\tConfused\tClass\tOrder\tWcode\tsFamily\tCI\tCoding\tStruct\tOther\n")
            f.write("Abcd\t1234\t-\tFalse\tII\tHelitron\tDHX\tNA\t99\t" \
                    "coding=(TE_BLRtx: Crypton-2_TCa:ClassII:Crypton:Crypton: 100.00%; " \
                    "TE_BLRx: Crypton-1_TC_1p:ClassII:Crypton:Crypton: 5.26%, Crypton-2_TCa_1p:ClassII:Crypton:Crypton: 99.65%; " \
                    "profiles: PF00589.17_Phage_integrase_INT_21.6: 84.39%(84.39%))\t" \
                    "struct=(TElength: >4000bps; TermRepeats: non-termLTR: 1188; ORF: >1000bps, >3000bps INT, >1000bps RT RH; " \
                    "SSR: (ATAATAAAAAC)3_end, (A)31_end; helitronExtremities: ATREP3:ClassII:Helitron:Helitron: (0.0, 1, 2097))\t" \
                    "other=(Other_profiles: PF13384.1_HTH_23_OTHER_24.6: 70%(70%); HG_BLRn: FBtr0073959_Dmel_r4.3: 21.90%; " \
                    "TermRepeats: termLTR: 390, non-termLTR: 388, termTIR: 390; SSRCoverage=9.00%)")
          
        expDict = self._writeClassifDictNewFormat()
        obsDict = ClassifUtils.getClassifInfosAsDict(dummyClassifFile)
  
        self.assertEqual(expDict, obsDict)

    def test_getClassifInfosAsDict_withNewClassifFormat(self):
        dummyClassifFile = "dummyClassif_{}.classif".format(self._uniqId)
        with open(dummyClassifFile, "w") as f:
            f.write("Seq_name\tlength\tstrand\tconfused\tclass\torder\tWcode\tsFamily\tCI\tcoding\tstruct\tother\n")
            f.write("Abcd\t1234\t-\tFalse\tII\tHelitron\tDHX\tNA\t99\t" \
                    ""
                    "coding=(TE_BLRtx: Crypton-2_TCa:ClassII:Crypton:Crypton: 100.00%; " \
                    "TE_BLRx: Crypton-1_TC_1p:ClassII:Crypton:Crypton: 5.26%, Crypton-2_TCa_1p:ClassII:Crypton:Crypton: 99.65%; " \
                    "profiles: PF00589.17_Phage_integrase_INT_21.6: 84.39%(84.39%))\t" \
                    "struct=(TElength: >4000bps; TermRepeats: non-termLTR: 1188; ORF: >1000bps, >3000bps INT, >1000bps RT RH; " \
                    "SSR: (ATAATAAAAAC)3_end, (A)31_end; helitronExtremities: ATREP3:ClassII:Helitron:Helitron: (0.0, 1, 2097))\t" \
                    "other=(Other_profiles: PF13384.1_HTH_23_OTHER_24.6: 70%(70%); HG_BLRn: FBtr0073959_Dmel_r4.3: 21.90%; " \
                    "TermRepeats: termLTR: 390, non-termLTR: 388, termTIR: 390; SSRCoverage=9.00%)")
          
        expDict = self._writeClassifDictNewFormat()
        obsDict = ClassifUtils.getClassifInfosAsDict(dummyClassifFile)
  
        self.assertEqual(expDict, obsDict)

    def test_formatProfilesResultsAsDict(self):#old format
        profiles = ["PF00589.17_Phage_integrase_INT_21.6: 84.39%(84.39%)"]
        dObs = ClassifUtils.formatProfilesResultsAsDict(profiles)
        dExp = OrderedDict([("PF00589.17_Phage_integrase_INT_21.6", OrderedDict([("cov", "84.39"), ("covOnSubject", "84.39")]))])
        self.assertEqual(dExp,dObs)
 
    def test_formatNewCodingFeaturesAsDict(self):
        codingInfo = 'coding=(TE_BLRtx: Crypton-2_TCa:ClassII:Crypton:Crypton: 100.00%; TE_BLRx: Crypton-1_TC_1p:ClassII:Crypton:Crypton: 5.26%, Crypton-2_TCa_1p:ClassII:Crypton:Crypton: 99.65%; profiles: PF00589.17_Phage_integrase_INT_21.6: 84.39%(84.39%))'
        #dObs = OrderedDict()
        dObs = ClassifUtils.formatNewCodingFeaturesAsDict(codingInfo)
        dExp = OrderedDict([
        ("TE_BLRtx", OrderedDict([("Crypton-2_TCa:ClassII:Crypton:Crypton", "100.00%")])),
        ("TE_BLRx", OrderedDict([("Crypton-1_TC_1p:ClassII:Crypton:Crypton", "5.26%"),("Crypton-2_TCa_1p:ClassII:Crypton:Crypton","99.65%")])),
        ("profiles", OrderedDict([
            ("PF00589.17_Phage_integrase_INT_21.6", OrderedDict([("cov", "84.39"), ("covOnSubject", "84.39")]))
            ]))
        ])
        self.assertEqual(dExp, dObs)
        codingInfo = 'coding=(NA)'
        Exp = 'NA'
        Obs = ClassifUtils.formatNewCodingFeaturesAsDict(codingInfo)
        self.assertEqual(Exp, Obs)

    def test_formatNewStructuresFeaturesAsDict(self):
        structInfos = "struct=(TElength: >4000bps; TermRepeats: non-termLTR: 1188; ORF: >1000bps, >3000bps INT, >1000bps RT RH; SSR: (ATAATAAAAAC)3_end, (A)31_end; helitronExtremities: ATREP3:ClassII:Helitron:Helitron: (0.0, 1, 2097); SSRCoverage=9.00%)"
        dExp = OrderedDict([
        ("TElength", ">4000bps"),
        ("TermRepeats", OrderedDict([("non-termLTR", 1188)])),
        ("ORF", [(">1000bps", ""), (">3000bps", "INT"), (">1000bps", "RT RH")]),
        ("SSR", ["(ATAATAAAAAC)3_end", "(A)31_end"]),
        ("helitronExtremities", OrderedDict([("ATREP3:ClassII:Helitron:Helitron", OrderedDict([("start", 1),("end", 2097),("eValue", 0.0)]))])),
        ("SSRCoverage", 9.00)])
        dObs = ClassifUtils.formatNewStructFeaturesAsDict(structInfos)
        self.assertEqual(dExp,dObs)
        structInfos = 'struct=(NA)'
        Exp = 'NA'
        Obs = ClassifUtils.formatNewStructFeaturesAsDict(structInfos)
        self.assertEqual(Exp, Obs)

    def test_formatNewOtherFeaturesAsDict(self):
        otherInfos = "Other_profiles: PF13384.1_HTH_23_OTHER_24.6: 70%(70%); HG_BLRn: FBtr0073959_Dmel_r4.3: 21.90; TermRepeats: termLTR: 390, non-termLTR: 388, termTIR: 390; SSRCoverage=9.00%)"
        dExp = OrderedDict([
             ("Other_profiles", OrderedDict([
                 ("PF13384.1_HTH_23_OTHER_24.6",OrderedDict([("cov", "70"), ("covOnSubject", "70")]))
                 ])), 
                            ("HG_BLRn", OrderedDict([("name", "FBtr0073959_Dmel_r4.3"), ("cov","21.90")])), 
             ("TermRepeats", OrderedDict([
                 ("termLTR", 390), 
                 ("non-termLTR", 388), 
                 ("termTIR", 390)
                                 ])),
                            ("SSRCoverage", 9.00)
                            ])
        dObs = ClassifUtils.formatNewOtherFeaturesAsDict(otherInfos)
        self.assertEqual(dExp,dObs)
        otherInfos = 'struct=(NA)'
        Exp = 'NA'
        Obs = ClassifUtils.formatNewStructFeaturesAsDict(otherInfos)
        self.assertEqual(Exp, Obs)

    def test_parseClassifToNewClassif(self):
        # the reversed part in consensus name will kept
        oldClassifLineCompReversed = "DTX-comp_DmelChr4-B-P1.0-Map9_reversed\t1237\t-\tok\tII\tTIR\tcomplete\tCI=50; coding=(TE_BLRtx: PROTOP:classII:TIR: 6.70%, PROTOP_A:classII:TIR: 66.43%, PROTOP_B:classII:TIR: 6.42%); struct=(TElength: >700bps; TermRepeats: termTIR: 52); other=(HG_BLRn: FBtr0089196_Dmel_r4.3: 51.19%; SSRCoverage=0.22<0.75)"
        expNewClassifLineCompReversed = "DTX_DmelChr4-B-P1.0-Map9_reversed\t1237\t-\tFalse\tII\tTIR\tDTX\tNA\t50\tcoding=(TE_BLRtx: PROTOP:classII:TIR: 6.70%, PROTOP_A:classII:TIR: 66.43%, PROTOP_B:classII:TIR: 6.42%)\tstruct=(TElength: >700bps; TermRepeats: termTIR: 52)\tother=(HG_BLRn: FBtr0089196_Dmel_r4.3: 51.19%; SSRCoverage=0.22<0.75)"
        obsNewClassifLineCompReversed = ClassifUtils.parseClassifToNewClassif(oldClassifLineCompReversed)
        self.assertEqual(expNewClassifLineCompReversed, obsNewClassifLineCompReversed)

        oldClassifLineConfused = "RYX-incomp-chim_Spip\t10853\t+\tPotentialChimeric\tI\tDIRS\tincomplete\tCI=25; coding=(profiles: PF00098.18_zf-CCHC_GAG_16.7: 94.44%(94.44%)); struct=(TElength: >4000bps; TermRepeats: termLTR: 3315, non-termLTR: 166; ORF: >1000bps GAG); other=(Other_profiles: PF05754.9_DUF834_OTHER_23.0: 120.00%(98.57%); SSRCoverage=0.22)"
        expClassifLineConfused = "RYX_Spip\t10853\t+\tTrue\tI|NA\tDIRS|NA\tRYX|NA\tNA|NA\t25|NA\tcoding=(profiles: PF00098.18_zf-CCHC_GAG_16.7: 94.44%(94.44%))\tstruct=(TElength: >4000bps; TermRepeats: termLTR: 3315, non-termLTR: 166; ORF: >1000bps GAG)\tother=(Other_profiles: PF05754.9_DUF834_OTHER_23.0: 120.00%(98.57%); SSRCoverage=0.22)"
        obsNewClassifLineConfused = ClassifUtils.parseClassifToNewClassif(oldClassifLineConfused)
        self.assertEqual(expClassifLineConfused, obsNewClassifLineConfused)

        oldClassifLineNocat = "noCat_DmelChr4-B-R2-Map6\t4638\t.\tok\tnoCat\tnoCat\tNA\tCI=NA; struct=(TElength: >4000bps; SSRCoverage=0.05<0.95)"
        expNewClassifLineNocat = "noCat_DmelChr4-B-R2-Map6\t4638\t.\tFalse\tUnclassified\tUnclassified\tNA\tNA\tNA\tcoding=(NA)\tstruct=(TElength: >4000bps; SSRCoverage=0.05<0.95)\tother=(NA)"
        obsNewClassifLineNocat = ClassifUtils.parseClassifToNewClassif(oldClassifLineNocat)
        self.assertEqual(expNewClassifLineNocat, obsNewClassifLineNocat)

        oldClassifLineNocat2 = "noCat_LStag_chk16k-B-G3361-Map20\t1118\t.\tok\tnoCat\tnoCat\tNA\tCI=NA; struct=(TermRepeats: non-termLTR: 378; SSRCoverage=0.33)"
        expNewClassifLineNocat2 = "noCat_LStag_chk16k-B-G3361-Map20\t1118\t.\tFalse\tUnclassified\tUnclassified\tNA\tNA\tNA\tcoding=(NA)\tstruct=(TermRepeats: non-termLTR: 378; SSRCoverage=0.33)\tother=(NA)"
        obsNewClassifLineNocat2 = ClassifUtils.parseClassifToNewClassif(oldClassifLineNocat2)
        self.assertEqual(expNewClassifLineNocat2, obsNewClassifLineNocat2)

        oldClassifLinerDNAReversed = "rDNA-chim_DmelChr4-B-G9-Map3_reversed\t1590\t-\tok\tNA\trDNA\tNA\tCI=NA; struct=(TElength: >1000bps; SSRCoverage=0.21<0.95)"
        expNewClassifLinerDNAReversed = "rDNA_DmelChr4-B-G9-Map3_reversed\t1590\t-\tFalse\tNA\tNA\trDNA\tNA\tNA\tcoding=(NA)\tstruct=(TElength: >1000bps; SSRCoverage=0.21<0.95)\tother=(NA)"
        obsNewClassifLinerDNAReversed = ClassifUtils.parseClassifToNewClassif(oldClassifLinerDNAReversed)
        self.assertEqual(expNewClassifLinerDNAReversed, obsNewClassifLinerDNAReversed)

        oldClassifLineSSR = "SSR_LStag_chk16k-B-G1691-Map8\t890\t.\tok\tNA\tSSR\tNA\tCI=100; struct=(TElength: >100bps; SSRCoverage=1.00); other=(Other_profiles: PF07382.6_HC2_NA_OTHER_40.0: 78.35%(78.35%)"
        expNewClassifLineSSR = "SSR_LStag_chk16k-B-G1691-Map8\t890\t.\tFalse\tNA\tNA\tSSR\tNA\t100\tcoding=(NA)\tstruct=(TElength: >100bps; SSRCoverage=1.00)\tother=(Other_profiles: PF07382.6_HC2_NA_OTHER_40.0: 78.35%(78.35%)"
        obsNewClassifLineSSR = ClassifUtils.parseClassifToNewClassif(oldClassifLineSSR)
        self.assertEqual(expNewClassifLineSSR, obsNewClassifLineSSR)

        oldClassifLineChimReversed = 'DXX-chim_POLINTN1_SM:classII:Polinton_reversed\t3153\t-\tPotentialChimeric\tII\tTIR\tNA\tCI=12; coding=(TE_BLRtx: POLINTN1_SM:classII:Polinton: 34.22%, POLINTN1_SM:classII:Polinton: 64.57%); struct=(TermRepeats: termTIR: 1216; SSRCoverage=0.44)'
        expNewClassifLineChimReversed = "DXX_POLINTN1_SM:classII:Polinton_reversed\t3153\t-\tTrue\tII|NA\tTIR|NA\tDTX|NA\tNA|NA\t12|NA\tcoding=(TE_BLRtx: POLINTN1_SM:classII:Polinton: 34.22%, POLINTN1_SM:classII:Polinton: 64.57%)\tstruct=(TermRepeats: termTIR: 1216; SSRCoverage=0.44)\tother=(NA)"
        obsNewClassifLineChimReversed = ClassifUtils.parseClassifToNewClassif(oldClassifLineChimReversed)
        self.assertEqual(expNewClassifLineChimReversed, obsNewClassifLineChimReversed)

        oldClassifLineChim2 = "RLX-incomp-chim_DIRS-1_CB:classI:LTR_retrotransposon\t4357\t+\tPotentialChimeric\tI\tLTR\tincomplete\tCI=42; coding=(TE_BLRtx: DIRS-1_CB:classI:LTR_retrotransposon: 99.95%; TE_BLRx: DIRS1_CB_ORF1:classI:LTR_retrotransposon: 84.19%, DIRS1_CB_ORF2:classI:LTR_retrotransposon: 100.00%; profiles: PF00078.22_RVT_1_RT_20.7: 89.25%(89.25%), PF13456.1_RVT_3_RT_21.6: 68.18%(68.18%)); struct=(TElength: >4000bps; ORF: >1000bps RT RT); other=(TermRepeats: non-termLTR: 148; SSRCoverage=0.09)"
        expNewClassifLineChim2 = "RLX_DIRS-1_CB:classI:LTR_retrotransposon\t4357\t+\tTrue\tI|NA\tLTR|NA\tRLX|NA\tNA|NA\t42|NA\tcoding=(TE_BLRtx: DIRS-1_CB:classI:LTR_retrotransposon: 99.95%; TE_BLRx: DIRS1_CB_ORF1:classI:LTR_retrotransposon: 84.19%, DIRS1_CB_ORF2:classI:LTR_retrotransposon: 100.00%; profiles: PF00078.22_RVT_1_RT_20.7: 89.25%(89.25%), PF13456.1_RVT_3_RT_21.6: 68.18%(68.18%))\tstruct=(TElength: >4000bps; ORF: >1000bps RT RT)\tother=(TermRepeats: non-termLTR: 148; SSRCoverage=0.09)"
        obsNewClassifLineChim2 = ClassifUtils.parseClassifToNewClassif(oldClassifLineChim2)
        self.assertEqual(expNewClassifLineChim2, obsNewClassifLineChim2)

        oldClassifLineNotWithTrigramme = "Toto\t4638\t.\tok\tnoCat\tnoCat\tNA\tCI=NA; struct=(TElength: >4000bps; SSRCoverage=0.05<0.95)"
        expClassifLineNotWithTrigramme = "Toto\t4638\t.\tFalse\tUnclassified\tUnclassified\tNA\tNA\tNA\tcoding=(NA)\tstruct=(TElength: >4000bps; SSRCoverage=0.05<0.95)\tother=(NA)"
        obsNewClassifLineNotWithTrigramme = ClassifUtils.parseClassifToNewClassif(oldClassifLineNotWithTrigramme)
        self.assertEqual(expClassifLineNotWithTrigramme, obsNewClassifLineNotWithTrigramme)

        oldClassifLinePH = "PotentialHostGene_LStag_chk16k-B-G334-Map8\t2401\t.\tok\tNA\tPotentialHostGene\tNA\tCI=100; coding=(Other_profiles: PF07645.10_EGF_CA_NA_OTHER_21.0: 80.95%(80.95%)); other=(SSRCoverage=0.16)"
        expClassifLinePH = "PotentialHostGene_LStag_chk16k-B-G334-Map8\t2401\t.\tFalse\tNA\tNA\tPHG\tNA\t100\tcoding=(Other_profiles: PF07645.10_EGF_CA_NA_OTHER_21.0: 80.95%(80.95%))\tstruct=(NA)\tother=(SSRCoverage=0.16)"
        obsNewClassifLinePH = ClassifUtils.parseClassifToNewClassif(oldClassifLinePH)
        self.assertEqual(expClassifLinePH, obsNewClassifLinePH)

    def test_classifRLX2SupFamily(self):
        inputFile = "inputClassifFile.classif"
        obsFile = "ClassifFile_PC.classif"
        self._writeClassif(inputFile)
        expOutputFile = "expClassifFile_PC.classif"
        self._writeExpClassif_PC(expOutputFile)
        ClassifUtils.classifRLX2SupFamily(inputFile, obsFile)
        self.assertTrue(FileUtils.are2FilesIdentical(expOutputFile, obsFile))

    def test_createClassifInstanceDictFromClassifFile( self ):
        fileName = "classifFile.classif"
        with open(fileName, "w") as f:
            f.write("Seq_name\tlength\tstrand\tconfused\tclass\torder\tWcode\tsFamily\tCI\tcoding\tstruct\tother\n")
            f.write("denovoMDO_kr-B-G1981-Map5_reversed\t9864\t+\tFalse\tI\tLTR\tRLX\tNA\t35\tcoding=(TE_BLRtx: Gypsy-16_Mad-I_#2:ClassI:LTR:Gypsy:?: 12.92%; TE_BLRx: Gypsy-4_PX-I_1p:ClassI:LTR:Gypsy:?: 18.89%; profiles: PF00078.19_RVT_1_RT_32.0: 90.91%)\tstruct=(TElength: >4000bps; TermRepeats: termLTR: 2214; ORF: >1000bps)\tother=(Other_profiles: PF13961.1_DUF4219_OTHER_27.0: 100.00%; TermRepeats: termTIR: 12; SSRCoverage=0.09)\n")
            f.write("denovoMDO_kr-B-G4980-Map5\t1192\t+\tTrue\tI|I\tLTR|LINE\tRLX|RIX\tNA|NA\t14|6\tcoding=(TE_BLRtx: LTR-2_Mad-LTR:ClassI:LTR:?:?: 69.35%)\tstruct=(TElength: >700bps)\tother=(SSRCoverage=0.09)\n")
        dConsName2ClassifInstances = ClassifUtils.createClassifInstanceDictFromClassifFile(fileName)
        iClassA = Classif(consensusName = "denovoMDO_kr-B-G1981-Map5_reversed", code = "RLX", outConfuseness = False, consensusLength = 9864,
                 consensusStrand = "+", consensusClass = "I", consensusOrder = "LTR", consensusSuperFam = "NA", consensusCI = "35",
                 consensusCoding = "TE_BLRtx: Gypsy-16_Mad-I_#2:ClassI:LTR:Gypsy:?: 12.92%; TE_BLRx: Gypsy-4_PX-I_1p:ClassI:LTR:Gypsy:?: 18.89%; profiles: PF00078.19_RVT_1_RT_32.0: 90.91%",
                 consensusStruct = "TElength: >4000bps; TermRepeats: termLTR: 2214; ORF: >1000bps",
                 consensusOther = "Other_profiles: PF13961.1_DUF4219_OTHER_27.0: 100.00%; TermRepeats: termTIR: 12; SSRCoverage=0.09")
        iClassA.setNewFormat(True)
        iClassB = Classif(consensusName = "denovoMDO_kr-B-G4980-Map5", code = "RLX|RIX", outConfuseness = True, consensusLength = 1192,
                 consensusStrand = "+", consensusClass = "I|I", consensusOrder = "LTR|LINE", consensusSuperFam = "NA|NA", consensusCI = "14|6",
                 consensusCoding = "TE_BLRtx: LTR-2_Mad-LTR:ClassI:LTR:?:?: 69.35%",
                 consensusStruct = "TElength: >700bps",
                 consensusOther = "SSRCoverage=0.09")
        iClassB.setNewFormat(True)
        dExp = OrderedDict({"denovoMDO_kr-B-G1981-Map5_reversed": iClassA, "denovoMDO_kr-B-G4980-Map5": iClassB})
        self.assertEqual(dExp, dConsName2ClassifInstances)

    def test_createClassifInstanceDictFromClassifFile_notClassifFile( self ):
        fileName = "classifFile.txt"
        with open(fileName, "w") as f:
            f.write("Seq_name\tlength\tstrand\tconfused\tclass\torder\tWcode\tsFamily\tCI\tcoding\tstruct\tother\n")
        self.assertRaises(SystemExit, lambda: list(ClassifUtils.createClassifInstanceDictFromClassifFile(fileName)))

    def test_createClassifInstanceDictFromClassifFile_withEmptyFile( self ):
        fileName = "classifFile.classif"
        with open(fileName, "w") as f:
            f.write("")
        self.assertRaises(SystemExit, lambda: list(ClassifUtils.createClassifInstanceDictFromClassifFile(fileName)))

    def test_createClassifInstanceDictFromClassifFile_not12Colomns( self ):
        fileName = "classifFile.classif"
        with open(fileName, "w") as f:
            f.write("Seq_name\tlength\tstrand\tconfused\tclass\torder\tWcode\n")
        self.assertRaises(SystemExit, lambda: list(ClassifUtils.createClassifInstanceDictFromClassifFile(fileName)))

    def test_renameConsensus( self ):
        with open("initClassif.classif", "w") as fInitCl:
            fInitCl.write("Seq_name\tlength\tstrand\tconfused\tclass\torder\tWcode\tsFamily\tCI\tcoding\tstruct\tother\n")
            fInitCl.write("Bdis_MCL543_TEdenovoGr-B-G10037-Map3\t9864\t+\tFalse\tI\tLTR\tRLX\tNA\t35\tcoding=(TE_BLRtx: Gypsy-16_Mad-I_#2:ClassI:LTR:Gypsy:?: 12.92%, Gypsy-36_Mad-I:ClassI:LTR:Gypsy:?: 7.39%, Gypsy-44_Mad-LTR:ClassI:LTR:Gypsy:?: 46.07%, Gypsy-48_Mad-I:ClassI:LTR:Gypsy:?: 46.68%, Gypsy-4_PX-I:ClassI:LTR:Gypsy:?: 16.81%, LTR-2_Mad:ClassI:LTR:Gypsy:?: 7.15%, Copia-7_Mad-I:ClassI:LTR:Copia:?: 5.93%; TE_BLRx: Gypsy-4_PX-I_1p:ClassI:LTR:Gypsy:?: 18.89%, Gypsy-46_Mad-I_4p:ClassI:LTR:Gypsy:?: 27.23%)\tstruct=(TElength: >4000bps; TermRepeats: termLTR: 2214; ORF: >1000bps)\tother=(NA)\n")
            fInitCl.write("Bdis_MCL30_TEdenovoGr-B-G1004-Map20_reversed\t1192\t+\tFalse\tI\tLTR\tRLX\tNA\t14\tcoding=(TE_BLRtx: LTR-2_Mad-LTR:ClassI:LTR:?:?: 69.35%)\tstruct=(TElength: >700bps)\tother=(NA)\n")

        with open("expClassif", "w") as fInitCl:
            fInitCl.write("Seq_name\tlength\tstrand\tconfused\tclass\torder\tWcode\tsFamily\tCI\tcoding\tstruct\tother\n")
            fInitCl.write("Bdis_TEdenovoGr-B-G10037-Map3\t9864\t+\tFalse\tI\tLTR\tRLX\tNA\t35\tcoding=(TE_BLRtx: Gypsy-16_Mad-I_#2:ClassI:LTR:Gypsy:?: 12.92%, Gypsy-36_Mad-I:ClassI:LTR:Gypsy:?: 7.39%, Gypsy-44_Mad-LTR:ClassI:LTR:Gypsy:?: 46.07%, Gypsy-48_Mad-I:ClassI:LTR:Gypsy:?: 46.68%, Gypsy-4_PX-I:ClassI:LTR:Gypsy:?: 16.81%, LTR-2_Mad:ClassI:LTR:Gypsy:?: 7.15%, Copia-7_Mad-I:ClassI:LTR:Copia:?: 5.93%; TE_BLRx: Gypsy-4_PX-I_1p:ClassI:LTR:Gypsy:?: 18.89%, Gypsy-46_Mad-I_4p:ClassI:LTR:Gypsy:?: 27.23%)\tstruct=(TElength: >4000bps; TermRepeats: termLTR: 2214; ORF: >1000bps)\tother=(NA)\n")
            fInitCl.write("Bdis_TEdenovoGr-B-G1004-Map20_reversed\t1192\t+\tFalse\tI\tLTR\tRLX\tNA\t14\tcoding=(TE_BLRtx: LTR-2_Mad-LTR:ClassI:LTR:?:?: 69.35%)\tstruct=(TElength: >700bps)\tother=(NA)\n")

        with open("indexName.tab", "w") as f:
            f.write("Bdis_MCL543_TEdenovoGr-B-G10037-Map3\tBdis_TEdenovoGr-B-G10037-Map3\n")
            f.write("Bdis_MCL30_TEdenovoGr-B-G1004-Map20_reversed\tBdis_TEdenovoGr-B-G1004-Map20_reversed\n")

        ClassifUtils.renameConsensus("initClassif.classif", "newClassif.classif", "indexName.tab")
        self.assertTrue(FileUtils.are2FilesIdentical("expClassif", "newClassif.classif"))

    def _writeClassifDictNewFormat(self):
        return OrderedDict([( "Abcd", OrderedDict([
                        ("name", "Abcd"), 
                        ("length", 1234), 
                        ("strand", "-"), 
                        ("confused", False), 
                        ("class", "II"), 
                        ("order", "Helitron"), 
                        ("wCode", "DHX"), 
                        ("sFamily", "NA"), 
                        ("CI", '99'),
                        ("coding", OrderedDict([
                            ("TE_BLRtx", OrderedDict([
                                ("Crypton-2_TCa:ClassII:Crypton:Crypton", "100.00%")
                                                  ])), 
                                    ("TE_BLRx", OrderedDict([("Crypton-1_TC_1p:ClassII:Crypton:Crypton", "5.26%"),
                                                             ("Crypton-2_TCa_1p:ClassII:Crypton:Crypton", "99.65%")
                                                 ])), 
                                    ("profiles", OrderedDict([("PF00589.17_Phage_integrase_INT_21.6", OrderedDict([('cov', '84.39'), ('covOnSubject', '84.39')]))
                                                  ]))
                        ])),
                        ("struct", OrderedDict([ 
                              ("TElength", ">4000bps"), 
                              ("TermRepeats", OrderedDict([ 
                                  ("non-termLTR", 1188), 
                                  ])), 
                              ("ORF", [
                                  (">1000bps", ""), #if it was a dictionnary, it may get several identical keys
                                  (">3000bps", "INT"), 
                                  (">1000bps", "RT RH") 
                                  ]), 
                              ("SSR", ["(ATAATAAAAAC)3_end", 
                                       "(A)31_end"
                                       ]), 
                              ('helitronExtremities', OrderedDict([
                                  ("ATREP3:ClassII:Helitron:Helitron",OrderedDict([
                                      ("start", 1), 
                                      ("end", 2097), 
                                      ("eValue", 0.0)
                                      ]))
                                   ]))
                          ])), 
                          ("other", OrderedDict([
                                ("Other_profiles", OrderedDict([
                                    ("PF13384.1_HTH_23_OTHER_24.6", OrderedDict([('cov', '70'), ('covOnSubject', '70')]))
                                    ])), 
                                                   ("HG_BLRn", OrderedDict([
                                                       ("name", "FBtr0073959_Dmel_r4.3"), 
                                                       ("cov","21.90")
                                                       ])), 
                                                   ("TermRepeats", OrderedDict([
                                                       ("termLTR", 390), 
                                                       ("non-termLTR", 388), 
                                                       ("termTIR", 390)
                                                       ])), 
                                                   ("SSRCoverage", 9.00)
                                                   ]))
                            ]))])
                                    
    def _writeClassifDictOldFormat(self):          

        return OrderedDict([("DHX-incomp_DmelChr4-B-R1-Map4", OrderedDict([
                     ("name", "DHX-incomp_DmelChr4-B-R1-Map4"),
                     ("wCode", "DHX"),
                     ("length", 2367),
                     ("strand", "."),
                     ("chimeric", False),
                     ("class", "II"),
                     ("order", "Helitron"),
                     ("complete", False),
                     ("CI", 20),
                     ("coding", OrderedDict([
                         ("TE_BLRtx", OrderedDict([
                             ("DNAREP1_DM:ClassII:Helitron:Helitron", OrderedDict([
                                 ("cov", "16.84")
                             ])),
                             ("DNAREP1_DSim:ClassII:Helitron:Helitron", OrderedDict([
                                 ("cov", "10.12")
                             ])),
                             ("DNAREP1_DYak:ClassII:Helitron:Helitron", OrderedDict([
                                 ("cov", "9.08")
                             ]))
                         ]))
                     ])),
                     ("struct", OrderedDict([
                         ("TElength", ">700bps")
                     ])),
                     ("other", OrderedDict([
                         ("SSRCoverage", "18.00")
                     ]))
                 ])),
                 ("RLX-comp_DmelChr4-L-B6-Map1", OrderedDict([
                     ("name", "RLX-comp_DmelChr4-L-B6-Map1"),
                     ("wCode", "RLX"),
                     ("length", 6439),
                     ("strand", "+"),
                     ("chimeric", False),
                     ("class", "I"),
                     ("order", "LTR"),
                     ("complete", True),
                     ("CI", 92),
                     ("coding", OrderedDict([
                         ("TE_BLRtx", OrderedDict([
                             ("QUASIMODO2-I_DM:ClassI:LTR:Gypsy", OrderedDict([
                                 ("cov", "98.20")
                             ])),
                             ("QUASIMODO2-LTR_DM:ClassI:LTR:Gypsy", OrderedDict([
                                 ("cov", "100.00")
                             ]))
                         ])),
                         ("TE_BLRx", OrderedDict([
                             ("Gypsy-4_DBi-I_1p:ClassI:LTR:Gypsy", OrderedDict([
                                 ("cov", "27.74")
                             ])),
                             ("Gypsy-6_DSim-I_1p:ClassI:LTR:Gypsy", OrderedDict([
                                 ("cov", "59.00")
                             ])),
                             ("Gypsy-6_DSim-I_2p:ClassI:LTR:Gypsy", OrderedDict([
                                 ("cov", "79.67")
                             ]))
                         ])),
                         ("profiles", OrderedDict([
                             ("_GAG_17_6_GAG_NA", OrderedDict([
                                 ("cov", "96.63"),
                                 ("covOnSubject", "96.63")
                             ])),
                             ("_RNaseH_17_6_RH_NA", OrderedDict([
                                 ("cov", "100.00"),
                                 ("covOnSubject", "100.00")
                             ])),
                             ("_AP_17_6_AP_NA", OrderedDict([
                                 ("cov", "93.81"),
                                 ("covOnSubject", "93.81")
                             ])),
                             ("_INT_17_6_INT_NA", OrderedDict([
                                 ("cov", "100.00"),
                                 ("covOnSubject", "100.00")
                             ])),
                             ("_RT_17_6_RT_NA", OrderedDict([
                                 ("cov", "100.00"),
                                 ("covOnSubject", "100.00")
                             ]))
                         ]))
                     ])),
                     ("struct", OrderedDict([
                         ("TElength", ">4000bps"),
                         ("TermRepeats", OrderedDict([
                             ("termLTR", 499)
                         ])),
                         ("ORF", [">1000bps GAG", ">1000bps RT RH INT"])
                     ])),
                     ("other", OrderedDict([
                         ("SSRCoverage", "16.00")
                     ]))
                 ]))])

    def _writeClassif(self, fileName):
        f = open(fileName, "w")
        f.write("Seq_name\tlength\tstrand\tconfused\tclass\torder\tWcode\tsFamily\tCI\tcoding\tstruct\tother\n")
        #line with Copia AND Gypsy in coding field ==> it stays RLX
        f.write("denovoMDO_kr-B-G1981-Map5_reversed\t9864\t+\tFalse\tI\tLTR\tRLX\tNA\t35\tcoding=(TE_BLRtx: Gypsy-16_Mad-I_#2:ClassI:LTR:Gypsy:?: 12.92%, Gypsy-36_Mad-I:ClassI:LTR:Gypsy:?: 7.39%, Gypsy-44_Mad-LTR:ClassI:LTR:Gypsy:?: 46.07%, Gypsy-48_Mad-I:ClassI:LTR:Gypsy:?: 46.68%, Gypsy-4_PX-I:ClassI:LTR:Gypsy:?: 16.81%, LTR-2_Mad:ClassI:LTR:Gypsy:?: 7.15%, Copia-7_Mad-I:ClassI:LTR:Copia:?: 5.93%; TE_BLRx: Gypsy-4_PX-I_1p:ClassI:LTR:Gypsy:?: 18.89%, Gypsy-46_Mad-I_4p:ClassI:LTR:Gypsy:?: 27.23%)\tstruct=(TElength: >4000bps; TermRepeats: termLTR: 2214; ORF: >1000bps)\tother=(NA)\n")
        #line with no Copia AND no Gypsy in coding field ==> it stays RLX
        f.write("denovoMDO_kr-B-G4980-Map5\t1192\t+\tFalse\tI\tLTR\tRLX\tNA\t14\tcoding=(TE_BLRtx: LTR-2_Mad-LTR:ClassI:LTR:?:?: 69.35%)\tstruct=(TElength: >700bps)\tother=(NA)\n")
        #line with ONLY Copia AND no Gypsy in coding field ==> it becomes RLC
        f.write("denovoMDO_kr-B-G1470-Map16_reversed\t9325\t+\tFalse\tI\tLTR\tRLX\tNA\t35\tcoding=(TE_BLRtx: Copia-99_Mad-I:CLASSI:LTR:copia:?: 95.62%, Copia-99_Mad-LTR:ClassI:LTR:Copia:?: 99.73%; TE_BLRx: Copia-100_Mad-I_1p:ClassI:LTR:Copia:?: 69.29%)\tstruct=(TElength: >4000bps; TermRepeats: termLTR: 1272)\tother=(NA)\n")
        #line with ONLY Gypsy AND no Copia in coding field ==> it becomes RLG
        f.write("denovoMDO_kr-B-G8184-Map7\t7786\t+\tFalse\tI\tLTR\tRLX\tNA\t71\tcoding=(TE_BLRtx: Gypsy-41_Mad-I:ClassI:LTR:Gypsy:?: 65.98%, Gypsy-41_Mad-LTR:ClassI:LTR:Gypsy:?: 30.93%, Gypsy-6_Mad-LTR_#2:ClassI:LTR:Gypsy:?: 85.47%; TE_BLRx: Gypsy-41_Mad-I_1p:ClassI:LTR:Gypsy:?: 27.12%, Gypsy-41_Mad-I_2p:ClassI:LTR:Gypsy:?: 70.43%, Gypsy-4_Pru-I_1p:ClassI:LTR:Gypsy:?: 9.88%; profiles: _RNaseH_tat_NA_RH_NA: 99.18%, PF00075.19_RNase_H_NA_RH_21.3: 30.30%, PF13456.1_RVT_3_NA_RT_21.6: 96.59%, _INT_tat_NA_INT_NA: 99.45%)\tstruct=(TElength: >4000bps; TermRepeats: termLTR: 1671; ORF: >1000bps, >1000bps RH RH RT INT)\tother=(NA)\n")
        #Chimeric consensus line with RLX ==> it stays identical
        f.write("denovoMDO_kr-B-G93-Map9\t8043\t+\tTrue\tI|I\tLTR|LINE\tRLX|RIX\tNA|NA\t35|27\tcoding=(TE_BLRtx: Gypsy-29_Mad-I:ClassI:LTR:Gypsy:?: 42.23%, Gypsy-52_Mad-LTR:ClassI:LTR:Gypsy:?: 41.80%; TE_BLRx: Gypsy-29_Mad-I_1p:ClassI:LTR:Gypsy:?: 26.74%, Gypsy-46_Mad-I_4p:ClassI:LTR:Gypsy:?: 39.73%)\tstruct=(TElength: >4000bps; TermRepeats: termLTR: 232)\tother=(TE_BLRtx: RTE-1B_Mad:ClassI:LINE:RTE:RTE: 21.82%, RTE-1_Mad:ClassI:LINE:RTE:RTE: 14.20%; TElength: >1000bps)\n")
        #Consensus line with no RLX ==> it stays identical
        f.write("denovoMDO_kr-B-R7-Map20\t2901\t+\tFalse\tII\tTIR\tDTX\tNA\t37\tcoding=(coding=(TE_BLRtx: hAT-20_Mad:ClassII:TIR:hAT:?: 65.95%; TE_BLRx: hAT-20_Mad_1p:ClassII:TIR:hAT:?: 99.16%; profiles: PF05699.9_Dimer_Tnp_hAT_NA_Tase_21.4: 98.84%)\tstruct=(TElength: >4000bps; TermRepeats: termLTR: 232)\tother=(NA)\n")
        f.close()

    def _writeExpClassif_PC(self, fileName):
        f = open(fileName, "w")
        f.write("Seq_name\tlength\tstrand\tconfused\tclass\torder\tWcode\tsFamily\tCI\tcoding\tstruct\tother\n")
        #line with Copia AND Gypsy in coding field ==> it stays RLX
        f.write("denovoMDO_kr-B-G1981-Map5_reversed\t9864\t+\tFalse\tI\tLTR\tRLX\tNA\t35\tcoding=(TE_BLRtx: Gypsy-16_Mad-I_#2:ClassI:LTR:Gypsy:?: 12.92%, Gypsy-36_Mad-I:ClassI:LTR:Gypsy:?: 7.39%, Gypsy-44_Mad-LTR:ClassI:LTR:Gypsy:?: 46.07%, Gypsy-48_Mad-I:ClassI:LTR:Gypsy:?: 46.68%, Gypsy-4_PX-I:ClassI:LTR:Gypsy:?: 16.81%, LTR-2_Mad:ClassI:LTR:Gypsy:?: 7.15%, Copia-7_Mad-I:ClassI:LTR:Copia:?: 5.93%; TE_BLRx: Gypsy-4_PX-I_1p:ClassI:LTR:Gypsy:?: 18.89%, Gypsy-46_Mad-I_4p:ClassI:LTR:Gypsy:?: 27.23%)\tstruct=(TElength: >4000bps; TermRepeats: termLTR: 2214; ORF: >1000bps)\tother=(NA)\n")
        #line with no Copia AND no Gypsy in coding field ==> it stays RLX
        f.write("denovoMDO_kr-B-G4980-Map5\t1192\t+\tFalse\tI\tLTR\tRLX\tNA\t14\tcoding=(TE_BLRtx: LTR-2_Mad-LTR:ClassI:LTR:?:?: 69.35%)\tstruct=(TElength: >700bps)\tother=(NA)\n")
        #line with ONLY Copia AND no Gypsy in coding field ==> it becomes RLC
        f.write("denovoMDO_kr-B-G1470-Map16_reversed\t9325\t+\tFalse\tI\tLTR\tRLC\tCopia\t35\tcoding=(TE_BLRtx: Copia-99_Mad-I:CLASSI:LTR:copia:?: 95.62%, Copia-99_Mad-LTR:ClassI:LTR:Copia:?: 99.73%; TE_BLRx: Copia-100_Mad-I_1p:ClassI:LTR:Copia:?: 69.29%)\tstruct=(TElength: >4000bps; TermRepeats: termLTR: 1272)\tother=(NA)\n")
        #line with ONLY Gypsy AND no Copia in coding field ==> it becomes RLG
        f.write("denovoMDO_kr-B-G8184-Map7\t7786\t+\tFalse\tI\tLTR\tRLG\tGypsy\t71\tcoding=(TE_BLRtx: Gypsy-41_Mad-I:ClassI:LTR:Gypsy:?: 65.98%, Gypsy-41_Mad-LTR:ClassI:LTR:Gypsy:?: 30.93%, Gypsy-6_Mad-LTR_#2:ClassI:LTR:Gypsy:?: 85.47%; TE_BLRx: Gypsy-41_Mad-I_1p:ClassI:LTR:Gypsy:?: 27.12%, Gypsy-41_Mad-I_2p:ClassI:LTR:Gypsy:?: 70.43%, Gypsy-4_Pru-I_1p:ClassI:LTR:Gypsy:?: 9.88%; profiles: _RNaseH_tat_NA_RH_NA: 99.18%, PF00075.19_RNase_H_NA_RH_21.3: 30.30%, PF13456.1_RVT_3_NA_RT_21.6: 96.59%, _INT_tat_NA_INT_NA: 99.45%)\tstruct=(TElength: >4000bps; TermRepeats: termLTR: 1671; ORF: >1000bps, >1000bps RH RH RT INT)\tother=(NA)\n")
        #Chimeric consensus line with RLX ==> it stays identical
        f.write("denovoMDO_kr-B-G93-Map9\t8043\t+\tTrue\tI|I\tLTR|LINE\tRLX|RIX\tNA|NA\t35|27\tcoding=(TE_BLRtx: Gypsy-29_Mad-I:ClassI:LTR:Gypsy:?: 42.23%, Gypsy-52_Mad-LTR:ClassI:LTR:Gypsy:?: 41.80%; TE_BLRx: Gypsy-29_Mad-I_1p:ClassI:LTR:Gypsy:?: 26.74%, Gypsy-46_Mad-I_4p:ClassI:LTR:Gypsy:?: 39.73%)\tstruct=(TElength: >4000bps; TermRepeats: termLTR: 232)\tother=(TE_BLRtx: RTE-1B_Mad:ClassI:LINE:RTE:RTE: 21.82%, RTE-1_Mad:ClassI:LINE:RTE:RTE: 14.20%; TElength: >1000bps)\n")
        #Consensus line with no RLX ==> it stays identical
        f.write("denovoMDO_kr-B-R7-Map20\t2901\t+\tFalse\tII\tTIR\tDTX\tNA\t37\tcoding=(coding=(TE_BLRtx: hAT-20_Mad:ClassII:TIR:hAT:?: 65.95%; TE_BLRx: hAT-20_Mad_1p:ClassII:TIR:hAT:?: 99.16%; profiles: PF05699.9_Dimer_Tnp_hAT_NA_Tase_21.4: 98.84%)\tstruct=(TElength: >4000bps; TermRepeats: termLTR: 232)\tother=(NA)\n")
        f.close()


if __name__ == "__main__":
    unittest.main()
