# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import os
import sys
import time
import shutil
import unittest
import subprocess
from commons.core.utils.FileUtils import FileUtils

class Test_FileUtils( unittest.TestCase ):
    
    def setUp( self ):
        self._uniqId = "{}_{}" .format(time.strftime("%Y%m%d%H%M%S"), os.getpid())
        
    def test_getNbLinesInSingleFile_non_empty_noNewline( self ):
        file = "dummyFile_{}" .format ( self._uniqId )
        with open( file, "w" ) as f:
            f.write( "line1\n" )
            f.write( "line2\n" )
            f.write( "line3" )
        exp = 3
        obs = FileUtils.getNbLinesInSingleFile( file )
        self.assertEqual(exp, obs)
        os.remove( file )
        
    def test_getNbLinesInSingleFile_non_empty( self ):
        file = "dummyFile_{}" .format ( self._uniqId )
        with open(file, "w") as f:
            f.write("line1\n")
            f.write("line2\n")
            f.write("line3\n")
        exp = 3
        obs = FileUtils.getNbLinesInSingleFile( file )
        self.assertEqual( exp, obs )
        os.remove( file )
        
    def test_getNbLinesInSingleFile_non_empty_endEmptyLine(self):
        file = "dummyFile_{}" .format (self._uniqId)
        with open( file, "w" ) as f:
            f.write("line1\n")
            f.write("line2\n")
            f.write("line3\n")
            f.write("\n")
        exp = 3
        obs = FileUtils.getNbLinesInSingleFile( file )
        self.assertEqual( exp, obs )
        os.remove( file )
        
    def test_getNbLinesInSingleFile_empty( self ):
        file = "dummyFile_{}" .format(self._uniqId)
        subprocess.call("touch {}" .format(file), shell=True)
        exp = 0
        obs = FileUtils.getNbLinesInSingleFile(file)
        self.assertEqual(exp, obs)
        os.remove(file)
        
    def test_getNbLinesInSingleFile_empty_endEmptyLine( self ):
        file = "dummyFile_{}" .format ( self._uniqId )
        with open( file, "w" ) as f:
            f.write( "\n" )
        exp = 0
        obs = FileUtils.getNbLinesInSingleFile( file )
        self.assertEqual( exp, obs )
        os.remove( file )
        
    def test_getNbLinesInSingleFile_withOnlyEmptyLines( self ):
        file = "dummyFile_{}" .format ( self._uniqId )
        with open( file, "w" ) as f:
            f.write( "\n" )
            f.write( "\n" )
            f.write( "\n" )
        exp = 0
        obs = FileUtils.getNbLinesInSingleFile( file )
        self.assertEqual( exp, obs )
        os.remove( file )
        
    def test_getNbLinesInFileList_non_empty( self ):
        with open("dummy1.txt", "w") as f:
            f.write("line1\n")
            f.write("line2\n")
            f.write("line3")

        with open("dummy2.txt", "w") as f:
            f.write("line1\n")
            f.write("line2\n")
            f.write("line3")

        with open("dummy3.txt", "w") as f:
            f.write("line1\n")
            f.write("line2\n")
            f.write("line3")

        lFiles = [ "dummy1.txt", "dummy2.txt", "dummy3.txt" ]
        exp = 9
        obs = FileUtils.getNbLinesInFileList( lFiles )
        self.assertEqual( exp, obs )
        for f in lFiles:
            os.remove( f )

    def test_getOccNbRegexpInFile( self ):
        with open("dummy1.txt", "w") as f:
            f.write("tt\tline\tt\n")
            f.write("t\tline\tuuuuuu\n")
            f.write("line3\tuuuuuu\tt\n")

        obs = FileUtils.getOccNbRegexpInFile("dummy1.txt", "\tline\t")
        exp = 2
        self.assertEqual(exp, obs)
        obs = FileUtils.getOccNbRegexpInFile("dummy1.txt", "t\tline")
        exp = 2
        self.assertEqual(exp, obs)
        obs = FileUtils.getOccNbRegexpInFile("dummy1.txt", "line")
        exp = 3
        self.assertEqual(exp, obs)
        obs = FileUtils.getOccNbRegexpInFile("dummy1.txt", "u")
        exp = 12
        self.assertEqual(exp, obs)
        obs = FileUtils.getOccNbRegexpInFile("dummy1.txt", "toto")
        exp = 0
        self.assertEqual(exp, obs)
           
    def test_catFilesByPattern( self ):
        with open("dummy1.txt", "w") as f:
            f.write("line11\n")
            f.write("line12\n")
            f.write("line13")

        with open("dummy2.txt", "w") as f:
            f.write("line21\n")
            f.write("line22\n")
            f.write("line23\n")

        with open("dummy3.txt", "w") as f:
            f.write("line31\n")
            f.write("line32\n")
            f.write("line33")

        lFiles = [ "dummy1.txt", "dummy2.txt", "dummy3.txt" ]
        outFile = "concatFiles.txt"
        FileUtils.catFilesByPattern( "dummy*.txt", outFile )
        self.assertTrue( os.path.exists( outFile ) )
        exp = "line11\nline12\nline13line21\nline22\nline23\nline31\nline32\nline33"
        obs = FileUtils.getFileContent( [ outFile ] )
        self.assertEqual( exp, obs )
        for f in lFiles:
            os.remove( f )
        os.remove(outFile)
            
    def test_catFilesByPattern_with_headers( self ):
        with open("dummy1.txt", "w") as f:
            f.write("line11\n")
            f.write("line12\n")
            f.write("line13\n")

        with open("dummy2.txt", "w") as f:
            f.write("line21\n")
            f.write("line22\n")
            f.write("line23\n")

        with open("dummy3.txt", "w") as f:
            f.write("line31\n")
            f.write("line32\n")
            f.write("line33\n")

        lFiles = [ "dummy1.txt", "dummy2.txt", "dummy3.txt" ]
        outFile = "concatFiles.txt"
        FileUtils.catFilesByPattern( "dummy*.txt", outFile, skipHeaders = True)
        self.assertTrue( os.path.exists( outFile ) )
        exp = "line12\nline13\nline22\nline23\nline32\nline33\n"
        obs = FileUtils.getFileContent( [ outFile ] )
        self.assertEqual( exp, obs )
        for f in lFiles:
            os.remove( f )
        os.remove(outFile)
            
    def test_catFilesByPattern_with_separator( self ):
        with open("dummy1.txt", "w") as f:
            f.write("line11\n")
            f.write("line12\n")
            f.write("line13")

        with open("dummy2.txt", "w") as f:
            f.write("line21\n")
            f.write("line22\n")
            f.write("line23\n")

        with open("dummy3.txt", "w") as f:
            f.write("line31\n")
            f.write("line32\n")
            f.write("line33")

        lFiles = [ "dummy1.txt", "dummy2.txt", "dummy3.txt" ]
        outFile = "concatFiles.txt"
        FileUtils.catFilesByPattern( "dummy*.txt", outFile, separator = "\n+------------+\n")
        self.assertTrue( os.path.exists( outFile ) )
        exp = "line11\nline12\nline13\n+------------+\nline21\nline22\nline23\n\n+------------+\nline31\nline32\nline33"
        obs = FileUtils.getFileContent( [ outFile ] )
        self.assertEqual( exp, obs )
        for f in lFiles:
            os.remove( f )
        os.remove(outFile)
            
    def test_catFilesByPattern_with_headers_and_separator( self ):
        with open("dummy1.txt", "w") as f:
            f.write("line11\n")
            f.write("line12\n")
            f.write("line13\n")

        with open("dummy2.txt", "w") as f:
            f.write("line21\n")
            f.write("line22\n")
            f.write("line23\n")

        with open("dummy3.txt", "w") as f:
            f.write("line31\n")
            f.write("line32\n")
            f.write("line33\n")

        lFiles = [ "dummy1.txt", "dummy2.txt", "dummy3.txt" ]
        outFile = "concatFiles.txt"
        FileUtils.catFilesByPattern( "dummy*.txt", outFile, separator = "\n+------------+\n", skipHeaders = True)
        self.assertTrue( os.path.exists( outFile ) )
        exp = "line12\nline13\n\n+------------+\nline22\nline23\n\n+------------+\nline32\nline33\n"
        obs = FileUtils.getFileContent( [ outFile ] )
        self.assertEqual( exp, obs )
        for f in lFiles:
            os.remove( f )
        os.remove(outFile)
        
    def test_catFilesOfDir(self):        
        currentDir = os.getcwd()
        rootDir = os.path.join(currentDir,"dummy")
        if os.path.exists( rootDir ):
            shutil.rmtree(rootDir)
        os.mkdir(rootDir)
        
        directory = "testDir"
        os.mkdir(os.path.join(rootDir,directory))
        fileName1 = "dummyFile1.gff"
        fullFileName1 = os.path.join(rootDir, directory, fileName1)
        with open(fullFileName1, "w") as file1:
            file1.write("file1\n")

        fileName2 = "dummyFile2.gff"
        fullFileName2 = os.path.join(rootDir, directory, fileName2)
        with open(fullFileName2, "w") as file2:
            file2.write("file2\n")

        obsFile = "obsFile"
        expFile = "expFile"
        with open(expFile, "w") as expF:
            expF.write("file1\nfile2\n")

        FileUtils.catFilesOfDir(os.path.join(rootDir, directory), obsFile)
        self.assertTrue(FileUtils.are2FilesIdentical(expFile, obsFile))
        
        shutil.rmtree(rootDir)
        os.remove(expFile)
        os.remove(obsFile)
            
    def test_isRessourceExists_exists(self): 
        f = open("dummyFile.txt", "w")
        f.close()
        self.assertTrue(FileUtils.isRessourceExists("dummyFile.txt"))
        subprocess.call("rm dummyFile.txt", shell=True)
            
    def test_isRessourceExists_not_exists(self):
        self.assertFalse(FileUtils.isRessourceExists("dummyFile.txt"))
            
    def test_isEmpty_empty( self ):
        file = "dummyFile_{}" .format ( self._uniqId )
        subprocess.call( "touch {}" .format ( file ), shell=True )
        self.assertTrue( FileUtils.isEmpty( file ) )
        os.remove( file )
        
    def test_isEmpty_empty_endEmptyLine( self ):
        file = "dummyFile_{}" .format ( self._uniqId )
        with open( file, "w" ) as fileHandler:
            fileHandler.write( "\n" )

        self.assertTrue( FileUtils.isEmpty( file ) )
        os.remove( file )
        
    def test_isEmpty_empty_endEmptyLines( self ):
        file = "dummyFile_{}" .format ( self._uniqId )
        with open( file, "w" ) as fileHandler:
            fileHandler.write( "\n" )
            fileHandler.write( "\n" )
            fileHandler.write( "\n" )

        self.assertTrue( FileUtils.isEmpty( file ) )
        os.remove( file )
        
    def test_isEmpty_non_empty( self ):
        file = "dummyFile_{}" .format ( self._uniqId )
        with open( file, "w" ) as fileHandler:
            fileHandler.write( "line1\n" )
        self.assertFalse( FileUtils.isEmpty( file ) )
        os.remove( file )
        
    def test_are2FilesIdentical_true( self ):
        with open("dummy1.txt", "w") as f:
            f.write("line11\n")

        with open("dummy2.txt", "w") as f:
            f.write("line11\n")

        self.assertTrue( FileUtils.are2FilesIdentical( "dummy1.txt", "dummy2.txt" ) )
        for f in [ "dummy1.txt", "dummy2.txt" ]:
            os.remove( f )
            
    def test_are2FilesIdentical_false( self ):
        with open("dummy1.txt", "w") as f:
            f.write("line11\n")
        with open("dummy2.txt", "w") as f:
            f.write("line21\n")

        self.assertFalse( FileUtils.are2FilesIdentical( "dummy1.txt", "dummy2.txt" ) )
        for f in [ "dummy1.txt", "dummy2.txt" ]:
            os.remove( f )
            
    def test_getFileContent( self ):
        inFile = "dummyInFile_{}".format(self._uniqId)
        with open( inFile, "w" ) as inFileHandler:
            inFileHandler.write( "zgdfet\n" )
            inFileHandler.write( "agdfet\n" )
        exp = "zgdfet\nagdfet\n"
        obs = FileUtils.getFileContent( [ inFile ] )
        self.assertEqual( exp, obs )
        os.remove( inFile )
        
    def test_sortFileContent( self ):
        inFile = "dummyInFile_{}" .format ( self._uniqId )
        with open( inFile, "w" ) as inFileHandler:
            inFileHandler.write( "zgdfet\n" )
            inFileHandler.write( "agdfet\n" )
        
        expFile = "dummyExpFile_{}" .format ( self._uniqId )
        with open( expFile, "w" ) as expFileHandler:
            expFileHandler.write( "agdfet\n" )
            expFileHandler.write( "zgdfet\n" )
        
        FileUtils.sortFileContent( inFile )
        
        self.assertTrue( FileUtils.are2FilesIdentical( expFile, inFile ) )
        for f in [ inFile, expFile ]:
            os.remove( f )
            
    def test_removeFilesByPattern_prefix( self ):
        fileName1 = "filetest.fa"
        fileName2 = "test.fa.Nstretch.map"
        fileName3 = "test.fa_cut"
        subprocess.call("touch {}" .format(fileName1), shell=True)
        subprocess.call("touch {}" .format(fileName2), shell=True)
        subprocess.call("touch {}" .format(fileName3), shell=True)
        FileUtils.removeFilesByPattern("test*")
        self.assertTrue(os.path.exists(fileName1))
        self.assertFalse(os.path.exists(fileName2))
        self.assertFalse(os.path.exists(fileName3))
        os.remove(fileName1)
            
    def test_removeFilesByPattern_suffix( self ):
        fileName1 = "filetest"
        fileName2 = "test.fa.Nstretch.map"
        fileName3 = "test.fa_cut"
        subprocess.call("touch {}" .format(fileName1), shell=True)
        subprocess.call("touch {}" .format(fileName2), shell=True)
        subprocess.call("touch {}" .format(fileName3), shell=True)
        FileUtils.removeFilesByPattern("*test")
        self.assertFalse(os.path.exists(fileName1))
        self.assertTrue(os.path.exists(fileName2))
        self.assertTrue(os.path.exists(fileName3))
        os.remove(fileName2)
        os.remove(fileName3)
            
    def test_removeFilesBySuffixList( self ):
        tmpDir = "dummyDir_{}" .format ( self._uniqId )
        if not os.path.exists( tmpDir ):
            os.mkdir( tmpDir )
        commonPrefix = "dummyFile_{}"  .format(self._uniqId)
        subprocess.call("touch {}/{}.fa" .format(tmpDir, commonPrefix), shell=True)
        subprocess.call("touch {}/{}.fa.Nstretch.map" .format(tmpDir, commonPrefix), shell=True)
        subprocess.call("touch {}/{}.fa_cut" .format(tmpDir, commonPrefix), shell=True)
        lSuffixes = [ ".Nstretch.map", "_cut" ]
        FileUtils.removeFilesBySuffixList( tmpDir, lSuffixes )
        self.assertTrue( os.path.exists("{}/{}.fa" .format ( tmpDir, commonPrefix ) ) )
        self.assertFalse( os.path.exists("{}/{}.fa.Nstretch.map" .format ( tmpDir, commonPrefix ) ) )
        self.assertFalse( os.path.exists("{}/{}.fa_cut" .format ( tmpDir, commonPrefix ) ) )
        shutil.rmtree( tmpDir )
        
    def test_removeRepeatedBlanks( self ):
        inFileName = "dummyWithRepeatedBlanks.dum"
        obsFileName = "dummyWithoutRepeatedBlanks.dum"
        expFileName = "dummyExpWithoutRepeatedBlanks.dum"
        self._writeFileWithRepeatedBlanks( inFileName )
        self._writeFileWithoutRepeatedBlanks( expFileName )
        FileUtils.removeRepeatedBlanks( inFileName, obsFileName )
        self.assertTrue( FileUtils.are2FilesIdentical( expFileName, obsFileName ) )
        for f in [ inFileName, expFileName, obsFileName ]:
            os.remove( f )
            
    def test_RemoveRepeatedBlanks_without_outfileName (self):
        inFileName = "dummyWithRepeatedBlanks.dum"
        expFileName = "dummyExpWithoutRepeatedBlanks.dum"
        obsFileName = inFileName
        self._writeFileWithRepeatedBlanks( inFileName )
        self._writeFileWithoutRepeatedBlanks( expFileName )
        FileUtils.removeRepeatedBlanks( inFileName )
        self.assertTrue( FileUtils.are2FilesIdentical( expFileName, obsFileName ) )
        for f in [ inFileName, expFileName ]:
            os.remove( f )
            
    def test_removeFilesFromList(self):
        fileName1 = "filetest.fa"
        fileName2 = "test.fa.Nstretch.map"
        fileName3 = "test.fa_cut"
        lFiles = [fileName1, fileName2, fileName3]
        for f in lFiles:
            # open(f, "w")
            with open(f, 'w') as file:
                file.write("ttt")
        FileUtils.removeFilesFromList(lFiles)
        self.assertFalse(os.path.exists(fileName1))
        self.assertFalse(os.path.exists(fileName2))
        self.assertFalse(os.path.exists(fileName3))
            
            
    def test_removeFilesFromList_exception(self):
        fileName1 = "filetest.fa"
        fileName2 = "test.fa.Nstretch.map"
        fileName3 = "test.fa_cut"
        lFiles = [fileName1, fileName2, fileName3]
        for f in lFiles[:-1]:
            with open(f, 'w') as file:
                file.write("ttt")
        
        isExceptionRaised = False
        try:
            FileUtils.removeFilesFromList(lFiles)
        except OSError:
            isExceptionRaised = True
            
        self.assertTrue(isExceptionRaised)
            
            
    def test_removeFilesFromListIfExist(self):
        fileName1 = "filetest.fa"
        fileName2 = "test.fa.Nstretch.map"
        fileName3 = "test.fa_cut"
        lFiles = [fileName1, fileName2, fileName3]
        for f in lFiles[:-1]:
            with open(f, 'w') as file:
                file.write("ttt")
        
        isExceptionRaised = False
        try:
            FileUtils.removeFilesFromListIfExist(lFiles)
        except OSError:
            isExceptionRaised = True
            
        self.assertFalse(isExceptionRaised)
        self.assertFalse(os.path.exists(fileName1))
        self.assertFalse(os.path.exists(fileName2))
        self.assertFalse(os.path.exists(fileName3))
        

    def test_fromWindowsToUnixEof( self ):
        inFile = "dummyInFile"
        with open( inFile, "w" ) as inFileHandler:
            inFileHandler.write( "toto\r\n" )
        expFile = "dummyExpFile"
        with open( expFile, "w" ) as expFileHandler:
            expFileHandler.write( "toto\n" )
        FileUtils.fromWindowsToUnixEof( inFile )
        self.assertTrue( FileUtils.are2FilesIdentical( expFile, inFile ) )
        for f in [ inFile, expFile ]:
            os.remove( f )


    def test_removeDuplicatedLines( self ):
        inFile = "dummyInFile"
        with open( inFile, "w" ) as inFileHandler:
            inFileHandler.write( "toto\n" )
            inFileHandler.write( "titi\n" )
            inFileHandler.write( "toto\n" )

        expFile = "dummyExpFile"
        with open( expFile, "w" ) as expFileHandler:
            expFileHandler.write( "toto\n" )
            expFileHandler.write( "titi\n" )

        FileUtils.removeDuplicatedLines( inFile )
        self.assertTrue( FileUtils.are2FilesIdentical( expFile, inFile ) )
        for f in [ inFile, expFile ]:
            os.remove( f )
            
            
    def test_writeLineListInFile( self ):
        inFile = "dummyInFile"
        lLines = [ "toto\n", "titi\n" ]
        expFile = "dummyExpFile"
        expFileHandler = open( expFile, "w" )
        expFileHandler.write( "toto\n" )
        expFileHandler.write( "titi\n" )
        expFileHandler.close()
        FileUtils.writeLineListInFile( inFile, lLines )
        self.assertTrue( FileUtils.are2FilesIdentical( expFile, inFile ) )
        for f in [ inFile, expFile ]:
            os.remove( f )
            
        
    def test_getAbsoluteDirectoryPathList(self):
        currentDir = os.getcwd()
        rootDir = currentDir + "/" + "dummy"
        if os.path.exists( rootDir ):
            shutil.rmtree(rootDir)
        os.mkdir(rootDir)
        
        os.mkdir(rootDir + "/" + "dummyDir1")
        os.mkdir(rootDir + "/" + "dummyDir2")
        
        expLDir = [rootDir + "/" + "dummyDir1", rootDir + "/" + "dummyDir2"]
        obsLDir = FileUtils.getAbsoluteDirectoryPathList(rootDir)
        
        expLDir.sort()
        obsLDir.sort()
        self.assertEqual(expLDir, obsLDir)
        
        shutil.rmtree(rootDir + "/" + "dummyDir1")
        shutil.rmtree(rootDir + "/" + "dummyDir2")
        shutil.rmtree(rootDir)
        
        
    def test_getAbsoluteDirectoryPathList_empty_dir(self):
        currentDir = os.getcwd()
        rootDir = os.path.join(currentDir, "dummy")
        if os.path.exists( rootDir ):
            shutil.rmtree(rootDir)
        os.mkdir(rootDir)
        
        expLDir = []
        obsLDir = FileUtils.getAbsoluteDirectoryPathList(rootDir)
        
        self.assertEqual(expLDir, obsLDir)
        
        shutil.rmtree(rootDir)
        
        
    def test_getSubListAccordingToPattern_match(self):
        lPath = ["/home/repet/titi", "/pattern/test", "/patter/test", "/_patternic_/test", "/home/patternic/test", "/home/pattern", "pattern", ""]
        
        expL = ["/pattern/test", "/_patternic_/test", "/home/patternic/test", "/home/pattern", "pattern"]
        obsL = FileUtils.getSubListAccordingToPattern(lPath, "pattern")
        
        self.assertEqual(expL, obsL)
        
        
    def test_getSubListAccordingToPattern_not_match(self):
        lPath = ["/home/repet/titi", "/pattern/test", "/patter/test", "/_patternic_/test", "/home/patternic/test", "/home/pattern", "pattern", ""]
        
        expL = ["/home/repet/titi", "/patter/test", ""]
        obsL = FileUtils.getSubListAccordingToPattern(lPath, "pattern", False)

        self.assertEqual(expL, obsL)

        obsL = FileUtils.getSubListAccordingToPattern(lPath, "", False)
        expL = lPath
        self.assertEqual(expL, obsL)
        
    def test_getFileNamesList(self):
        currentDir = os.getcwd()
        rootDir = os.path.join(currentDir, "dummy")
        if os.path.exists( rootDir ):
            shutil.rmtree(rootDir)
        os.mkdir(rootDir)
        
        directory = "testDir"
        os.mkdir(os.path.join(rootDir, directory))
        fileName1 = "dummyFile1.gff"
        fullFileName1 = os.path.join(rootDir, directory, fileName1)
        file1 = open(fullFileName1, "w")
        file1.close()
        fileName2 = "dummyFile2.gff"
        fullFileName2 = os.path.join(rootDir, directory, fileName2)
        file2 = open(fullFileName2, "w")
        file2.close()
        
        expLFiles = [fileName1, fileName2]
        obsLFiles = FileUtils.getFileNamesList(os.path.join(rootDir, directory))
        
        self.assertEqual(expLFiles, sorted(obsLFiles))
        
        shutil.rmtree(rootDir)
        
    def test_getFileNamesList_withPattern(self):
        currentDir = os.getcwd()
        rootDir = os.path.join(currentDir, "dummy")
        if os.path.exists( rootDir ):
            shutil.rmtree(rootDir)
        os.mkdir(rootDir)
        
        directory = "testDir"
        os.mkdir(os.path.join(rootDir, directory))
        fileName1 = "dummyFile1.gff"
        fullFileName1 = os.path.join(rootDir, directory, fileName1)
        file1 = open(fullFileName1, "w")
        file1.close()
        fileName2 = "dummyFile2.gff"
        fullFileName2 = os.path.join(rootDir, directory, fileName2)
        file2 = open(fullFileName2, "w")
        file2.close()
        
        expLFiles = [fileName1]
        obsLFiles = FileUtils.getFileNamesList(os.path.join(rootDir, directory), "dummyFile1.*")
        
        self.assertEqual(expLFiles, obsLFiles)
        
        shutil.rmtree(rootDir)
        
    def test_getFileNamesList_empty_dir(self):
        currentDir = os.getcwd()
        rootDir = os.path.join(currentDir, "dummy")
        os.mkdir(rootDir)
    
        directory = "testDir"
        os.mkdir(os.path.join(rootDir, directory))
        
        expLFiles = []
        obsLFiles = FileUtils.getFileNamesList(os.path.join(rootDir, directory))
        
        self.assertEqual(expLFiles, obsLFiles)
        
        shutil.rmtree(rootDir)
        
    def test_getMd5SecureHash( self ):
        if "hashlib" in sys.modules:
            inFile = "dummyInFile"
            with open( inFile, "w" ) as inFileHandler:
                inFileHandler.write( "DLZIH17T63B;?" )
            exp = "50d1e2ded8f03881f940f70226e2b986"
            obs = FileUtils.getMd5SecureHash( inFile )
            self.assertEqual( exp, obs )
            os.remove( inFile )
            
    def test_isSizeNotNull_True(self):
        file = "dummyExpFile"
        with open( file, "w" ) as fileHandler:
            fileHandler.write( "toto\n" )
            fileHandler.write( "titi\n" )

        obsSize = FileUtils.isSizeNotNull(file)
        self.assertTrue(obsSize)
        os.remove(file)
        
    def test_isSizeNotNull_False(self):
        file = "dummyExpFile"
        fileHandler = open( file, "w" )
        fileHandler.close()
        obsSize = FileUtils.isSizeNotNull(file)
        self.assertFalse(obsSize)
        os.remove(file)
            
    def test_splitFileIntoNFiles_3_files(self):
        inputFile = "dummy.txt"
        obsFile1 = "dummy-1.txt"
        obsFile2 = "dummy-2.txt"
        obsFile3 = "dummy-3.txt"
        
        with open(inputFile, "w") as f:
            f.write("line1\n")
            f.write("line2\n")
            f.write("line3\n")

        exp1 = "line1\n"
        exp2 = "line2\n"
        exp3 = "line3\n"
        
        FileUtils.splitFileIntoNFiles(inputFile, 3)
        
        with open(obsFile1, 'r') as f:
            obs1 = f.read()
        with open(obsFile2, 'r') as f:
            obs2 = f.read()
        with open(obsFile3, 'r') as f:
            obs3 = f.read()

        self.assertEqual(exp1, obs1)
        self.assertEqual(exp2, obs2)
        self.assertEqual(exp3, obs3)
        self.assertFalse(FileUtils.isRessourceExists("dummy-4.txt"))
        FileUtils.removeFilesByPattern("dummy*")
            
    def test_splitFileIntoNFiles_2_files(self):
        inputFile = "dummy.txt"
        obsFile1 = "dummy-1.txt"
        obsFile2 = "dummy-2.txt"
        
        with open(inputFile, "w") as f:
            f.write("line1\n")
            f.write("line2\n")
            f.write("line3\n")

        exp1 = "line1\nline2\n"
        exp2 = "line3\n"
        
        FileUtils.splitFileIntoNFiles(inputFile, 2)
        
        with open(obsFile1, 'r') as file:
            obs1 = file.read()
        with open(obsFile2, 'r') as file:
            obs2 = file.read()

        self.assertEqual(exp1, obs1)
        self.assertEqual(exp2, obs2)
        self.assertFalse(FileUtils.isRessourceExists("dummy-3.txt"))
        FileUtils.removeFilesByPattern("dummy*")
            
    def test_splitFileIntoNFiles_one_file(self):
        inputFile = "dummy.txt"
        obsFile1 = "dummy-1.txt"
        
        with open(inputFile, "w") as f:
            f.write("line1\n")
            f.write("line2\n")
            f.write("line3\n")

        exp1 = "line1\nline2\nline3\n"
        
        FileUtils.splitFileIntoNFiles(inputFile, 1)
        
#        obs1 = open(obsFile1).read()
        with open(obsFile1, 'r') as file:
            obs1 = file.read()

        self.assertEqual(exp1, obs1)
        self.assertFalse(FileUtils.isRessourceExists("dummy-2.txt"))
        FileUtils.removeFilesByPattern("dummy*")
            
    def test_splitFileIntoNFiles_more_file_than_lines(self):
        inputFile = "dummy.txt"
        obsFile1 = "dummy-1.txt"
        obsFile2 = "dummy-2.txt"
        obsFile3 = "dummy-3.txt"
        
        with open(inputFile, "w") as f:
            f.write("line1\n")
            f.write("line2\n")
            f.write("line3\n")

        exp1 = "line1\n"
        exp2 = "line2\n"
        exp3 = "line3\n"
        
        FileUtils.splitFileIntoNFiles(inputFile, 10)
        
        with open(obsFile1, 'r') as file:
            obs1 = file.read()
        with open(obsFile2, 'r') as file:
            obs2 = file.read()
        with open(obsFile3, 'r') as file:
            obs3 = file.read()

        self.assertEqual(exp1, obs1)
        self.assertEqual(exp2, obs2)
        self.assertEqual(exp3, obs3)
        self.assertFalse(FileUtils.isRessourceExists("dummy-4.txt"))
        FileUtils.removeFilesByPattern("dummy*")
        
    def test_splitFileIntoNFiles_empty_file(self):
        inputFile = "dummy.txt"
        obsFile1 = "dummy-1.txt"

        subprocess.call( "touch {}" .format ( inputFile ), shell=True)
        exp1 = ""
        
        FileUtils.splitFileIntoNFiles(inputFile, 10)
        
        with open(obsFile1, 'r') as file:
            obs1 = file.read()

        self.assertEqual(exp1, obs1)
        self.assertFalse(FileUtils.isRessourceExists("dummy-2.txt"))
        FileUtils.removeFilesByPattern("dummy*")
        
    def test_splitFileIntoNFiles_0_file(self):
        inputFile = "dummy.txt"
        obsFile1 = "dummy-1.txt"
        
        with open(inputFile, "w") as f:
            f.write("line1\n")
            f.write("line2\n")
            f.write("line3\n")
        
        exp1 = "line1\nline2\nline3\n"
        
        FileUtils.splitFileIntoNFiles(inputFile, 0)
        
        with open(obsFile1, 'r') as file:
            obs1 = file.read()

        self.assertEqual(exp1, obs1)
        self.assertFalse(FileUtils.isRessourceExists("dummy-2.txt"))
        FileUtils.removeFilesByPattern("dummy*")
        
    def test_splitFileAccordingToLineNumber_3_files(self):
        inputFile = "dummy.txt"
        obsFile1 = "dummy-1.txt"
        obsFile2 = "dummy-2.txt"
        obsFile3 = "dummy-3.txt"
        
        with open(inputFile, "w") as f:
            f.write("line1\n")
            f.write("line2\n")
            f.write("line3\n")

        exp1 = "line1\n"
        exp2 = "line2\n"
        exp3 = "line3\n"
        
        FileUtils.splitFileAccordingToLineNumber(inputFile, 1)
        
        with open(obsFile1, 'r') as file:
            obs1 = file.read()
        with open(obsFile2, 'r') as file:
            obs2 = file.read()
        with open(obsFile3, 'r') as file:
            obs3 = file.read()

        self.assertEqual(exp1, obs1)
        self.assertEqual(exp2, obs2)
        self.assertEqual(exp3, obs3)
        self.assertFalse(FileUtils.isRessourceExists("dummy-4.txt"))
        FileUtils.removeFilesByPattern("dummy*")
            
    def test_splitFileAccordingToLineNumber_2_files(self):
        inputFile = "dummy.txt"
        obsFile1 = "dummy-1.txt"
        obsFile2 = "dummy-2.txt"
        
        with open(inputFile, "w") as f:
            f.write("line1\n")
            f.write("line2\n")
            f.write("line3\n")


        exp1 = "line1\nline2\n"
        exp2 = "line3\n"
        
        FileUtils.splitFileAccordingToLineNumber(inputFile, 2)
        
        with open(obsFile1, 'r') as file:
            obs1 = file.read()
        with open(obsFile2, 'r') as file:
            obs2 = file.read()

        self.assertEqual(exp1, obs1)
        self.assertEqual(exp2, obs2)
        self.assertFalse(FileUtils.isRessourceExists("dummy-3.txt"))
        FileUtils.removeFilesByPattern("dummy*")
            
    def test_splitFileAccordingToLineNumber_one_file(self):
        inputFile = "dummy.txt"
        obsFile1 = "dummy-1.txt"
        
        with open(inputFile, "w") as f:
            f.write("line1\n")
            f.write("line2\n")
            f.write("line3\n")

        exp1 = "line1\nline2\nline3\n"
        
        FileUtils.splitFileAccordingToLineNumber(inputFile, 3)
        
        with open(obsFile1, 'r') as file:
            obs1 = file.read()

        self.assertEqual(exp1, obs1)
        self.assertFalse(FileUtils.isRessourceExists("dummy-2.txt"))
        FileUtils.removeFilesByPattern("dummy*")
            
    def test_splitFileAccordingToLineNumber_more_maxLines_than_lines(self):
        inputFile = "dummy.txt"
        obsFile1 = "dummy-1.txt"
        
        with open(inputFile, "w") as f:
            f.write("line1\n")
            f.write("line2\n")
            f.write("line3\n")

        exp1 = "line1\nline2\nline3\n"
        
        FileUtils.splitFileAccordingToLineNumber(inputFile, 10)
        
        with open(obsFile1, 'r') as file:
            obs1 = file.read()

        self.assertEqual(exp1, obs1)
        self.assertFalse(FileUtils.isRessourceExists("dummy-2.txt"))
        FileUtils.removeFilesByPattern("dummy*")
            
    def test_splitFileAccordingToLineNumber_empty_file(self):
        inputFile = "dummy.txt"
        obsFile1 = "dummy-1.txt"

        subprocess.call( "touch {}" .format ( inputFile ), shell=True)

        exp1 = ""
        
        FileUtils.splitFileAccordingToLineNumber(inputFile, 10)

        with open(obsFile1) as f:
            obs1 = f.read()
        
        self.assertEqual(exp1, obs1)
        self.assertFalse(FileUtils.isRessourceExists("dummy-2.txt"))
        FileUtils.removeFilesByPattern("dummy*")
            
    def test_splitFileAccordingToLineNumber_0_lines(self):
        inputFile = "dummy.txt"
        obsFile1 = "dummy-1.txt"
        
        with open(inputFile, "w") as f:
            f.write("line1\n")
            f.write("line2\n")
            f.write("line3\n")


        exp1 = "line1\nline2\nline3\n"
        
        FileUtils.splitFileAccordingToLineNumber(inputFile, 0)

        with open(obsFile1, 'r') as file:
            obs1 = file.read()

        self.assertEqual(exp1, obs1)
        self.assertFalse(FileUtils.isRessourceExists("dummy-2.txt"))
        FileUtils.removeFilesByPattern("dummy*")
        
    def test_concatenateFileNamesFromList(self):
        lFilesNames = ["titi.fa", "toto.fa", "tutu.fa", "tata.fa"]
        
        obsName = FileUtils.concatenateFileNamesFromList(lFilesNames)
        expName = "tata_titi_toto_tutu.fa"
        
        self.assertEqual(expName, obsName)
        
    def test_concatenateFileNamesFromList_empty(self):
        lFilesNames = []
        
        obsName = FileUtils.concatenateFileNamesFromList(lFilesNames)
        expName = ""
        
        self.assertEqual(expName, obsName)
        
    def test_concatenateFileNamesFromList_only_one_file(self):
        lFilesNames = ["toto.fsa"]
        
        obsName = FileUtils.concatenateFileNamesFromList(lFilesNames)
        expName = "toto.fsa"
        
        self.assertEqual(expName, obsName)
        
    def test_concatenateFileNamesFromList_same_file_name(self):
        lFilesNames = ["titi.fa", "titi.fsa", "titi.fsa", "titi.fa"]
        
        obsName = FileUtils.concatenateFileNamesFromList(lFilesNames)
        expName = "titi_titi_titi_titi.fa"
        
        self.assertEqual(expName, obsName)
        
    def test_concatenateFileNamesFromList_no_extensions(self):
        lFilesNames = ["titi", "toto", "tutu", "tata"]
        
        obsName = FileUtils.concatenateFileNamesFromList(lFilesNames)
        expName = "tata_titi_toto_tutu"
        
        self.assertEqual(expName, obsName)
        
    def test_concatenateFileNamesFromList_multiple_file_extensions_with_majority(self):
        lFilesNames = ["titi.fsa", "toto.txt", "tutu.fsa", "tata"]
        
        obsName = FileUtils.concatenateFileNamesFromList(lFilesNames)
        expName = "tata_titi_toto_tutu.fsa"
        
        self.assertEqual(expName, obsName)
        
    def test_concatenateFileNamesFromList_multiple_file_extensions_without_majority(self):
        lFilesNames = ["titi.fsa", "toto.txt", "tutu.db", "tata"]
        
        obsName = FileUtils.concatenateFileNamesFromList(lFilesNames)
        expName = "tata_titi_toto_tutu"
        
        self.assertEqual(expName, obsName)
        
    def test_concatenateFileNamesFromList_with_paths(self):
        lFilesNames = ["/home/titi.fa", "/home/toto.fa", "/tutu.fa", "../../tata.fa"]
        
        obsName = FileUtils.concatenateFileNamesFromList(lFilesNames)
        expName = "tata_titi_toto_tutu.fa"
        
        self.assertEqual(expName, obsName)
        
    def test_concatenateFileNamesFromList_override_join_separator(self):
        lFilesNames = ["/home/titi.fa", "/home/toto.fa", "/tutu.fa", "../../tata.fa"]
        
        obsName = FileUtils.concatenateFileNamesFromList(lFilesNames, sep = "SEP")
        expName = "tataSEPtitiSEPtotoSEPtutu.fa"
        
        self.assertEqual(expName, obsName)
        
    def test_concatenateFileNamesFromList_override_join_separator_empty(self):
        lFilesNames = ["/home/titi.fa", "/home/toto.fa", "/tutu.fa", "../../tata.fa"]
        
        obsName = FileUtils.concatenateFileNamesFromList(lFilesNames, sep = "")
        expName = "tatatititototutu.fa"
        
        self.assertEqual(expName, obsName)
        
    def test_concatenateFileNamesFromList_override_file_extension(self):
        lFilesNames = ["titi.fsa", "toto.txt", "tutu.db", "tata"]
        
        obsName = FileUtils.concatenateFileNamesFromList(lFilesNames, ext = "db")
        expName = "tata_titi_toto_tutu.db"
        
        self.assertEqual(expName, obsName)
        
    def test_concatenateFileNamesFromList_override_file_extension_with_period(self):
        lFilesNames = ["titi.fsa", "toto.txt", "tutu.db", "tata"]
        
        obsName = FileUtils.concatenateFileNamesFromList(lFilesNames, ext = ".db")
        expName = "tata_titi_toto_tutu.db"
        
        self.assertEqual(expName, obsName)
        
    def test_concatenateFileNamesFromList_override_file_extension_empty(self):
        lFilesNames = ["titi.fsa", "toto.txt", "tutu.db", "tata"]
        
        obsName = FileUtils.concatenateFileNamesFromList(lFilesNames, ext = "")
        expName = "tata_titi_toto_tutu"
        
        self.assertEqual(expName, obsName)
        
    def test_concatenateFileNamesFromList_override_file_extension_and_separator(self):
        lFilesNames = ["titi.fsa", "toto.txt", "tutu.db", "tata"]
        
        obsName = FileUtils.concatenateFileNamesFromList(lFilesNames, ext = ".dummyExtension", sep = "-")
        expName = "tata-titi-toto-tutu.dummyExtension"
        
        self.assertEqual(expName, obsName)
        
    def test_concatenateFileNamesFromString(self):
        filesNames = "titi.fa,toto.fa,tutu.fa,tata.fa"
        
        obsName, obsListFileName = FileUtils.concatenateFileNamesFromString(filesNames)
        expName = "tata_titi_toto_tutu.fa"
        expList = ["tata.fa", "titi.fa", "toto.fa", "tutu.fa"]
        
        self.assertEqual(expName, obsName)
        self.assertEqual(expList, obsListFileName)
        
    def test_concatenateFileNamesFromString_empty_input(self):
        filesNames = ""
        
        obsName, obsListFileName = FileUtils.concatenateFileNamesFromString(filesNames)
        expName = ""
        expList = [""]
        
        self.assertEqual(expName, obsName)
        self.assertEqual(expList, obsListFileName)
        
    def test_concatenateFileNamesFromString_empty_split_separator(self):
        filesNames = "titi.fa toto.fa tutu.fa tata.fa"
        
        obsName, obsListFileName = FileUtils.concatenateFileNamesFromString(filesNames, splitSep = "")
        expName = "titi.fa toto.fa tutu.fa tata.fa"
        expList = ["titi.fa toto.fa tutu.fa tata.fa"]
        
        self.assertEqual(expName, obsName)
        self.assertEqual(expList, obsListFileName)
        
    def test_concatenateFileNamesFromString_override_split_separator(self):
        filesNames = "titi.fa toto tutu.fa tata.fa"
        
        obsName, obsListFileName = FileUtils.concatenateFileNamesFromString(filesNames, splitSep = " ")
        expName = "tata_titi_toto_tutu.fa"
        expList = ["tata.fa", "titi.fa", "toto", "tutu.fa"]
        
        self.assertEqual(expName, obsName)
        self.assertEqual(expList, obsListFileName)
        
    def test_concatenateFileNamesFromString_override_everything(self):
        filesNames = "titi.fa toto tutu.fa tata.txt"
        
        obsName, obsListFileName = FileUtils.concatenateFileNamesFromString(filesNames, splitSep = " ", joinSep = "-", ext = "db")
        expName = "tata-titi-toto-tutu.db"
        expList = ["tata.txt", "titi.fa", "toto", "tutu.fa"]
        
        self.assertEqual(expName, obsName)
        self.assertEqual(expList, obsListFileName)
        
    def _writeFile( self, fileName ):
        with open(fileName, 'w') as inFile:
            inFile.write(">Sequence_de_reference\n")
            inFile.write("ATTTTGCAGTCTTATTCGAG-----GCCATTGCT\n")
            inFile.write(">Lignee1_mismatch\n")
            inFile.write("ATTTTGCAGACTTATTCGAG-----GCCATTGCT\n")
            inFile.write(">Lignee2_insertion\n")
            inFile.write("ATTTTGCAGTCTTATTCGAGATTACGCCATTGCT\n")
            inFile.write(">Lignee3_deletion\n")
            inFile.write("A---TGCAGTCTTATTCGAG-----GCCATTGCT\n")
        
    def _writeFileWithEmptyLine( self, fileName ):
        with open(fileName, 'w') as fileWithEmptyLine:
            fileWithEmptyLine.write(">Sequence_de_reference\n")
            fileWithEmptyLine.write("ATTTTGCAGTCTTATTCGAG-----GCCATTGCT\n")
            fileWithEmptyLine.write("\n\n")
            fileWithEmptyLine.write(">Lignee1_mismatch\n")
            fileWithEmptyLine.write("ATTTTGCAGACTTATTCGAG-----GCCATTGCT\n")
            fileWithEmptyLine.write("\n\n")
            fileWithEmptyLine.write(">Lignee2_insertion\n")
            fileWithEmptyLine.write("ATTTTGCAGTCTTATTCGAGATTACGCCATTGCT\n")
            fileWithEmptyLine.write("\n")
            fileWithEmptyLine.write(">Lignee3_deletion\n")
            fileWithEmptyLine.write("A---TGCAGTCTTATTCGAG-----GCCATTGCT\n")
        
    def _writeFileWithRepeatedBlanks( self, fileName ):
        with open(fileName, 'w') as fileWithRepeatedBlanks:
            fileWithRepeatedBlanks.write(">Sequ  ence_de     _reference\n")
            fileWithRepeatedBlanks.write("ATTTT  GCAGTCTT TTCGAG-  ----GCCATT  GCT\n")
        
    def _writeFileWithoutRepeatedBlanks( self, fileName ):
        with open(fileName, 'w') as fileWithoutRepeatedBlanks:
            fileWithoutRepeatedBlanks.write(">Sequ ence_de _reference\n")
            fileWithoutRepeatedBlanks.write("ATTTT GCAGTCTT TTCGAG- ----GCCATT GCT\n")
        
if __name__ == "__main__":
    unittest.main()