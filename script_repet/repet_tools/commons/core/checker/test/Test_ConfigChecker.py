import os
import unittest
from commons.core.checker.ConfigChecker import ConfigChecker
from commons.core.checker.ConfigChecker import ConfigRules
from commons.core.checker.RepetException import RepetException

class Test_ConfigChecker(unittest.TestCase):
    
    def setUp(self):
        self._configFileName = "testConfigChecker.cfg"
        self._iMock = MockConfig()

    def tearDown(self) :
        if os.path.exists(self._configFileName) :
            os.remove(self._configFileName)

    def test_checkIfExistsConfigFile_file_exist(self):
        f=open(self._configFileName, "w")
        f.close()
        
        doesFileExists = True
        iConfigRules = ConfigRules()
        try:
            iConfigChecker = ConfigChecker(self._configFileName, iConfigRules)
            iConfigChecker.checkIfExistsConfigFile()
        except RepetException:
            doesFileExists = False
        os.remove(self._configFileName)        
        self.assertTrue(doesFileExists)
        
    def test_checkIfExistsConfigFile_file_not_exist(self):
        iConfigRules = ConfigRules()
        expMsg ="CONFIG FILE not found - '{}'".format(self._configFileName)
        doesFileExists = True
        try:
            iConfigChecker = ConfigChecker(self._configFileName, iConfigRules)   
            iConfigChecker.checkIfExistsConfigFile()     
        except RepetException as re:
            doesFileExists = False
            self.assertFalse(doesFileExists)
            self.assertEqual(expMsg, re.getMessage())
        
    def test_readConfigFile(self):
        self._iMock.write_config(self._configFileName)
        iConfigRules = ConfigRules()
        expDictRawConfigValues = {"dir_name" : {"work_dir":"toto"},
                                  "organism" : {"abbreviation":"T.aestivum",
                                                "genus":"triticum",
                                                "species":"aestivum",
                                                "common_name":"wheat",
                                                "comment":""},
                                  'analysis1': {'description': '',
                                                'gff_name': 'BLASTX.gff2',
                                                'name': 'BLASTXWheat2',
                                                'program': 'BLASTX2',
                                                'programversion': '3.32',
                                                'sourcename': 'dummyDesc_BLASTX2'}
                                 }
        isNoExceptionRaised = True
        try: 
            iConfigChecker = ConfigChecker(self._configFileName, iConfigRules)
            iConfig = iConfigChecker.readConfigFile()
            iConfigChecker.setRawConfig(iConfig)
            obsDictRawConfigValues = iConfigChecker._iRawConfig.dOptionsValues4Sections
        except RepetException:
            isNoExceptionRaised = False

        self.assertTrue(isNoExceptionRaised)
        self.assertEqual(obsDictRawConfigValues, expDictRawConfigValues)
        
    def test_readConfigFile_section_define_twice(self):
        self._iMock.write_case_section_define_twice(self._configFileName)
        iConfigRules = ConfigRules()
        #expMsg = 'Unexpected error: Duplicate section in config file {}'.format(self._configFileName)
        expMsg = "Unexpected error: Duplicate value section While reading from \'testConfigChecker.cfg\' [line 12]: section \'analysis1\' already exists"
        doesExceptionRaised = False
        obsMessage = ""
        try:
            iConfigChecker = ConfigChecker(self._configFileName, iConfigRules)
            iConfig = iConfigChecker.readConfigFile()
            iConfigChecker.setRawConfig(iConfig)
        except RepetException as re:
            doesExceptionRaised = True
            obsMessage = re.getMessage()

        self.assertTrue(doesExceptionRaised)
        self.assertEqual(expMsg, obsMessage)

    def test_readConfigFile_option_define_twice(self):
        self._iMock.write_case_option_define_twice(self._configFileName)
        iConfigRules = ConfigRules()
        expMsg = "Unexpected error: Duplicate value option While reading from \'testConfigChecker.cfg\' [line  9]: option \'program\' in section \'analysis1\' already exists"
        doesExceptionRaised = False
        try:
            iConfigChecker = ConfigChecker(self._configFileName, iConfigRules)
            iConfig = iConfigChecker.readConfigFile()
            iConfigChecker.setRawConfig(iConfig)
        except RepetException as re:
            doesExceptionRaised = True
            obsMessage = re.getMessage()

        self.assertTrue(doesExceptionRaised)
        self.assertEqual(expMsg, obsMessage)

    def test_checkMandatorySections(self):
        self._iMock.write_config(self._configFileName)
        iConfigRules = ConfigRules()
        iConfigRules.addRuleSection(section="dir_name", mandatory=True)
        iConfigRules.addRuleSection(section="organism", mandatory=True)
        iConfigChecker = ConfigChecker(self._configFileName, iConfigRules)
        iConfig = iConfigChecker.readConfigFile()
        iConfigChecker.setRawConfig(iConfig)
        iConfigChecker.extendConfigRulesWithPatternRules()
        areAllMandatorySectionsFound = True
        try:
            iConfigChecker.checkMandatorySections()
        except RepetException:
            areAllMandatorySectionsFound = False

        self.assertTrue(areAllMandatorySectionsFound)
 
    def test_checkMandatorySections_one_missing (self):
        self._iMock.write_config(self._configFileName)
        iConfigRules = ConfigRules()
        iConfigRules.addRuleSection(section="dir_name", mandatory=True)
        iConfigRules.addRuleSection(section="target", mandatory=True)
        iConfigChecker = ConfigChecker(self._configFileName, iConfigRules)
        iConfig = iConfigChecker.readConfigFile()
        iConfigChecker.setRawConfig(iConfig)
        iConfigChecker.extendConfigRulesWithPatternRules()
        expMsg = "Error in configuration file {}, following sections are missing:\n - target\n".format(self._configFileName)
        areAllMandatorySectionsFound = True
        obsMessage = ""
        try:
            iConfigChecker.checkMandatorySections()
        except RepetException as re:
            areAllMandatorySectionsFound = False
            obsMessage = re.getMessage()

        self.assertFalse(areAllMandatorySectionsFound)
        self.assertEqual(expMsg, obsMessage)
        
    def test_checkMandatorySections_mandatory_section_with_pattern_section_is_missing (self):
        self._iMock.write_config(self._configFileName)
        iConfigRules = ConfigRules()
        iConfigRules.addRuleSection(section="dir_name", mandatory=True)
        iConfigRules.addRuleSection(section="mandatorySection", mandatory=True, isPattern = True)
        iConfigChecker = ConfigChecker(self._configFileName, iConfigRules)
        iConfig = iConfigChecker.readConfigFile()
        iConfigChecker.setRawConfig(iConfig)
        iConfigChecker.extendConfigRulesWithPatternRules()
        expMsg = "Error in configuration file {}, following sections are missing:\n - mandatorySection\n".format(self._configFileName)
        areAllMandatorySectionsFound = True
        try:
            iConfigChecker.checkMandatorySections()
        except RepetException as re:
            areAllMandatorySectionsFound = False
            obsMessage = re.getMessage()

        self.assertFalse(areAllMandatorySectionsFound)
        self.assertEqual(expMsg, obsMessage)

    def test_checkMandatorySections_mandatory_section_is_pattern (self):
        self._iMock.write_case_pattern_rule(self._configFileName)
        iConfigRules = ConfigRules()
        iConfigRules.addRuleSection(section="dir_name", mandatory=True)
        iConfigRules.addRuleSection(section="analysis[0-9]*", mandatory=True, isPattern = True)
        iConfigChecker = ConfigChecker(self._configFileName, iConfigRules)
        iConfig = iConfigChecker.readConfigFile()
        iConfigChecker.setRawConfig(iConfig)
        iConfigChecker.extendConfigRulesWithPatternRules()
        areAllMandatorySectionsFound = True
        try:
            iConfigChecker.checkMandatorySections()
        except RepetException:
            areAllMandatorySectionsFound = False

        self.assertTrue(areAllMandatorySectionsFound)

    def test_checkMandatoryOptions_in_mandatory_section (self):
        self._iMock.write_config(self._configFileName)
        iConfigRules = ConfigRules()
        iConfigRules.addRuleSection(section="organism", mandatory=True)
        iConfigRules.addRuleOption(section="organism", option ="genus", mandatory=True)
        iConfigChecker = ConfigChecker(self._configFileName, iConfigRules)
        iConfig = iConfigChecker.readConfigFile()
        iConfigChecker.setRawConfig(iConfig)
        iConfigChecker.extendConfigRulesWithPatternRules()
        areAllMandatoryOptionsFound = True
        try:
            iConfigChecker.checkMandatoryOptions()
        except RepetException:
            areAllMandatoryOptionsFound = False

        self.assertTrue(areAllMandatoryOptionsFound)

    def test_checkMandatoryOptions_in_mandatory_section_option_is_missing (self):
        self._iMock.write_config(self._configFileName)
        iConfigRules = ConfigRules()
        iConfigRules.addRuleSection(section="organism", mandatory=True)
        iConfigRules.addRuleOption(section="organism", option ="MissingOption", mandatory=True)
        iConfigChecker = ConfigChecker(self._configFileName, iConfigRules)
        iConfig = iConfigChecker.readConfigFile()
        iConfigChecker.setRawConfig(iConfig)
        iConfigChecker.extendConfigRulesWithPatternRules()
        expMsg = "Error in configuration file {}, following options are missing: \n - [organism]: MissingOption\n".format(self._configFileName)
        areAllMandatoryOptionsFound = True
        try:
            iConfigChecker.checkMandatoryOptions()
        except RepetException as re:
            areAllMandatoryOptionsFound = False
            obsMessage = re.getMessage()

        self.assertFalse(areAllMandatoryOptionsFound)
        self.assertEqual(expMsg, obsMessage)

    def test_checkMandatoryOptions_in_non_mandatory_section_and_section_and_option_exist (self):
        self._iMock.write_config(self._configFileName)
        iConfigRules = ConfigRules()
        iConfigRules.addRuleOption(section="organism", option ="genus", mandatory=True)
        iConfigChecker = ConfigChecker(self._configFileName, iConfigRules)
        iConfig = iConfigChecker.readConfigFile()
        iConfigChecker.setRawConfig(iConfig)
        iConfigChecker.extendConfigRulesWithPatternRules()
        areAllMandatoryOptionsFound = True
        try:
            iConfigChecker.checkMandatoryOptions()
        except RepetException:
            areAllMandatoryOptionsFound = False

        self.assertTrue(areAllMandatoryOptionsFound)
        
    def test_checkMandatoryOptions_in_non_mandatory_section_and_section_exist_option_is_missing (self):
        self._iMock.write_config(self._configFileName)
        iConfigRules = ConfigRules()
        iConfigRules.addRuleOption(section="organism", option ="MissingOption", mandatory=True)
        iConfigChecker = ConfigChecker(self._configFileName, iConfigRules)
        iConfig = iConfigChecker.readConfigFile()
        iConfigChecker.setRawConfig(iConfig)
        iConfigChecker.extendConfigRulesWithPatternRules()
        expMsg = "Error in configuration file {}, following options are missing: \n - [organism]: MissingOption\n".format(self._configFileName)
        areAllMandatoryOptionsFound = True
        try:
            iConfigChecker.checkMandatoryOptions()
        except RepetException as re:
            areAllMandatoryOptionsFound = False
            obsMessage = re.getMessage()

        self.assertFalse(areAllMandatoryOptionsFound)
        self.assertEqual(expMsg,obsMessage)
        
    def test_checkMandatoryOptions_in_non_mandatory_section_and_section_does_not_exist (self):
        self._iMock.write_config(self._configFileName)
        iConfigRules = ConfigRules()
        iConfigRules.addRuleOption(section="NonExistingAndNonMandatorySection", option ="genus", mandatory=True)
        iConfigChecker = ConfigChecker(self._configFileName, iConfigRules)
        iConfig = iConfigChecker.readConfigFile()
        iConfigChecker.setRawConfig(iConfig)
        iConfigChecker.extendConfigRulesWithPatternRules()
        areAllMandatoryOptionsFound = True
        try:
            iConfigChecker.checkMandatoryOptions()
        except RepetException:
            areAllMandatoryOptionsFound = False

        self.assertTrue(areAllMandatoryOptionsFound)
        
    def test_checkMandatoryOptions_with_Pattern_rules_in_section  (self):
        self._iMock.write_case_pattern_rule(self._configFileName)
        iConfigRules = ConfigRules()
        iConfigRules.addRuleSection(section="analysis", isPattern=True)
        iConfigRules.addRuleOption(section="analysis", option ="name", mandatory=True)
        iConfigChecker = ConfigChecker(self._configFileName, iConfigRules)
        iConfig = iConfigChecker.readConfigFile()
        iConfigChecker.setRawConfig(iConfig)
        iConfigChecker.extendConfigRulesWithPatternRules()
        areAllMandatoryOptionsFound = True
        try:
            iConfigChecker.checkMandatoryOptions()
        except RepetException:
            areAllMandatoryOptionsFound = False

        self.assertTrue(areAllMandatoryOptionsFound)

    def test_checkMandatoryOptions_with_pattern_rules_in_option_section_is_mandatory (self):
        self._iMock.write_case_pattern_rule(self._configFileName)
        iConfigRules = ConfigRules()
        iConfigRules.addRuleSection(section="section_with_option_pattern", mandatory=True)
        iConfigRules.addRuleOption(section="section_with_option_pattern", option ="option", isPattern= True, mandatory=True)
        iConfigChecker = ConfigChecker(self._configFileName, iConfigRules)
        iConfig = iConfigChecker.readConfigFile()
        iConfigChecker.setRawConfig(iConfig)
        iConfigChecker.extendConfigRulesWithPatternRules()
        areAllMandatoryOptionsFound = True
        try:
            iConfigChecker.checkMandatoryOptions()
        except RepetException:
            areAllMandatoryOptionsFound = False

        self.assertTrue(areAllMandatoryOptionsFound)

    def test_checkMandatoryOptions_with_pattern_rules_in_option_in_mandatory_section_option_is_missing (self):
        self._iMock.write_case_pattern_rule(self._configFileName)
        iConfigRules = ConfigRules()
        iConfigRules.addRuleSection(section="section_with_option_pattern", mandatory=True)
        iConfigRules.addRuleOption(section="section_with_option_pattern", option ="MissingOption", isPattern= True, mandatory=True)
        iConfigChecker = ConfigChecker(self._configFileName, iConfigRules)
        iConfig = iConfigChecker.readConfigFile()
        iConfigChecker.setRawConfig(iConfig)
        iConfigChecker.extendConfigRulesWithPatternRules()
        expMsg = "Error in configuration file {}, following options are missing: \n - [section_with_option_pattern]: MissingOption\n".format(self._configFileName)
        areAllMandatoryOptionsFound = True
        try:
            iConfigChecker.checkMandatoryOptions()
        except RepetException as re:
            areAllMandatoryOptionsFound = False
            obsMessage = re.getMessage()

        self.assertFalse(areAllMandatoryOptionsFound)
        self.assertEqual(expMsg, obsMessage)

    def test_checkMandatoryOptions_with_pattern_rules_in_non_mandatory_section_and_section_and_option_exist (self):
        self._iMock.write_case_pattern_rule(self._configFileName)
        iConfigRules = ConfigRules()
        iConfigRules.addRuleOption(section="section_with_option_pattern", option ="option", isPattern= True, mandatory=True)
        iConfigChecker = ConfigChecker(self._configFileName, iConfigRules)
        iConfig = iConfigChecker.readConfigFile()
        iConfigChecker.setRawConfig(iConfig)
        iConfigChecker.extendConfigRulesWithPatternRules()
        areAllMandatoryOptionsFound = True
        try:
            iConfigChecker.checkMandatoryOptions()
        except RepetException:
            areAllMandatoryOptionsFound = False

        self.assertTrue(areAllMandatoryOptionsFound)
        
    def test_checkMandatoryOptions_with_pattern_rules_in_non_mandatory_section_and_section_exist_option_is_missing (self):
        self._iMock.write_case_pattern_rule(self._configFileName)
        iConfigRules = ConfigRules()
        iConfigRules.addRuleOption(section="section_with_option_pattern", option ="MissingOption", isPattern= True, mandatory=True)
        iConfigChecker = ConfigChecker(self._configFileName, iConfigRules)
        iConfig = iConfigChecker.readConfigFile()
        iConfigChecker.setRawConfig(iConfig)
        iConfigChecker.extendConfigRulesWithPatternRules()
        expMsg = "Error in configuration file {}, following options are missing: \n - [section_with_option_pattern]: MissingOption\n".format(self._configFileName)
        areAllMandatoryOptionsFound = True
        try:
            iConfigChecker.checkMandatoryOptions()
        except RepetException as re:
            areAllMandatoryOptionsFound = False
            obsMessage = re.getMessage()

        self.assertFalse(areAllMandatoryOptionsFound)
        self.assertEqual(expMsg,obsMessage)
        
    def test_checkMandatoryOptions_with_pattern_rules_in_non_mandatory_section_and_section_does_not_exist (self):
        self._iMock.write_case_pattern_rule(self._configFileName)
        iConfigRules = ConfigRules()
        iConfigRules.addRuleOption(section="non_mandatory_section", option ="MissingOption", isPattern= True, mandatory=True)
        iConfigChecker = ConfigChecker(self._configFileName, iConfigRules)
        iConfig = iConfigChecker.readConfigFile()
        iConfigChecker.setRawConfig(iConfig)
        iConfigChecker.extendConfigRulesWithPatternRules()
        areAllMandatoryOptionsFound = True
        try:
            iConfigChecker.checkMandatoryOptions()
        except RepetException:
            areAllMandatoryOptionsFound = False

        self.assertTrue(areAllMandatoryOptionsFound)
        
    def test_checkMandatoryOptions_with_pattern_rules_for_both_section_and_option  (self):
        self._iMock.write_case_pattern_rule(self._configFileName)
        iConfigRules = ConfigRules()
        iConfigRules.addRuleSection(section="section_with_option_pattern", isPattern=True)
        iConfigRules.addRuleOption(section="section_with_option_pattern", option ="option", isPattern= True, mandatory=True)
        iConfigChecker = ConfigChecker(self._configFileName, iConfigRules)
        iConfig = iConfigChecker.readConfigFile()
        iConfigChecker.setRawConfig(iConfig)
        iConfigChecker.extendConfigRulesWithPatternRules()
        areAllMandatoryOptionsFound = True
        try:
            iConfigChecker.checkMandatoryOptions()
        except RepetException:
            areAllMandatoryOptionsFound = False
        os.remove(self._configFileName)
        self.assertTrue(areAllMandatoryOptionsFound)
       
    def test_checkMandatoryOptions_caseOptionNotMandatory(self):
        self._iMock.write_config_case(self._configFileName)
        iConfigRules = ConfigRules()
        iConfigRules.addRuleSection("dir_name", True)
        iConfigRules.addRuleOption("dir_name", "work_dir", mandatory=True)
        iConfigRules.addRuleSection("organism", True)
        iConfigRules.addRuleOption("organism", "min_SSR_coverage", mandatory=True)
        iConfigChecker = ConfigChecker(self._configFileName, iConfigRules)
        iConfig = iConfigChecker.readConfigFile()
        iConfigChecker.setRawConfig(iConfig)
        iConfigChecker.extendConfigRulesWithPatternRules()
        areAllMandatoryOptionsFound = True
        try:
            iConfigChecker.checkMandatoryOptions()
        except RepetException:
            areAllMandatoryOptionsFound = False

        self.assertTrue(areAllMandatoryOptionsFound)


class MockConfig (object):
    
    def write_config(self, configFileName):
        with open(configFileName, "w" ) as configF:
            configF.write( "[dir_name]\n")
            configF.write( "work_dir : toto \n")
            configF.write( "\n")
            configF.write( "[organism]\n")
            configF.write( "abbreviation: T.aestivum\n")
            configF.write( "genus: triticum\n")
            configF.write( "species: aestivum\n")
            configF.write( "common_name: wheat\n")
            configF.write( "comment: \n")
            configF.write( "[analysis1]\n")
            configF.write( "name: BLASTXWheat2\n")
            configF.write( "program: BLASTX2\n")
            configF.write( "programversion: 3.32\n")
            configF.write( "sourcename: dummyDesc_BLASTX2\n")
            configF.write( "description: \n")
            configF.write( "gff_name: BLASTX.gff2\n")
            configF.write( "\n")

    def write_case_section_define_twice(self, configFileName):
        with open(configFileName, "w" ) as configF:
            configF.write( "[dir_name]\n")
            configF.write( "work_dir : toto \n")
            configF.write( "\n")
            configF.write( "[analysis1]\n")
            configF.write( "name: BLASTXWheat\n")
            configF.write( "program: BLASTX\n")
            configF.write( "programversion: 3.3\n")
            configF.write( "sourcename: dummyDesc_BLASTX\n")
            configF.write( "description: \n")
            configF.write( "gff_name: BLASTX.gff\n")
            configF.write( "\n")
            configF.write( "[analysis1]\n")
            configF.write( "name: BLASTXWheat2\n")
            configF.write( "program: BLASTX2\n")
            configF.write( "programversion: 3.32\n")
            configF.write( "sourcename: dummyDesc_BLASTX2\n")
            configF.write( "description: \n")
            configF.write( "gff_name: BLASTX.gff2\n")
            configF.write( "\n")

    def write_case_option_define_twice(self, configFileName):
        with open(configFileName, "w" ) as configF:
            configF.write( "[dir_name]\n")
            configF.write( "work_dir : toto \n")
            configF.write( "\n")
            configF.write( "[analysis1]\n")
            configF.write( "name: BLASTXWheat\n")
            configF.write( "program: BLASTX\n")
            configF.write( "programversion: 3.3\n")
            configF.write( "sourcename: dummyDesc_BLASTX\n")
            configF.write( "program: BLASTX2\n")
            configF.write( "description: \n")
            configF.write( "gff_name: BLASTX.gff\n")
            configF.write( "\n")
            configF.write( "\n")

    #configuration file with section with option depends on presence of other options
    def write_with_one_option_depends_of_an_other_one(self, configFileName ):
        with open(configFileName, "w" ) as configF:
            configF.write( "[dir_name]\n")
            configF.write( "work_dir : toto\n")
            configF.write( "\n")
            configF.write( "[organism]\n")
            configF.write( "abbreviation: T.aestivum\n")
            configF.write( "genus: Triticum\n")
            configF.write( "species: aestivum\n")
            configF.write( "common_name: wheat\n")
            configF.write( "comment: \n")
            configF.write( "\n")
            configF.write( "[analysis1]\n")
            configF.write( "name: BLASTXWheat\n")
            configF.write( "program: BLASTX\n")
            configF.write( "programversion: 3.3\n")
            configF.write( "sourcename: src_BLASTX\n")
            configF.write( "description: \n")
            configF.write( "gff_name: BLASTX.gff\n")
            configF.write( "\n")
            configF.write( "[analysis2]\n")
            configF.write( "name: GMHMMWheat\n")
            configF.write( "program: GMHMM\n")
            configF.write( "programversion: 4.3\n")
            configF.write( "sourcename: src_GMHMM\n")
            configF.write( "description: \n")
            configF.write( "gff_name: GMHMM.gff\n")
            configF.write( "\n")
            configF.write( "[target]\n")
            configF.write( "target_used: yes\n")
            configF.write( "target_used_list: target.lst\n")

    def write_case_pattern_rule(self, configFileName ):
        with open(configFileName, "w" ) as configF:
            configF.write( "[dir_name]\n")
            configF.write( "work_dir : toto\n" )
            configF.write( "\n")
            configF.write( "[organism]\n")
            configF.write( "abbreviation: T.aestivum\n")
            configF.write( "genus: Triticum\n")
            configF.write( "species: aestivum\n")
            configF.write( "common_name: wheat\n")
            configF.write( "comment: \n")
            configF.write( "\n")
            configF.write( "[analysis1]\n")
            configF.write( "name: BLASTXWheat\n")
            configF.write( "program: BLASTX\n")
            configF.write( "programversion: 3.3\n")
            configF.write( "sourcename: src_BLASTX\n")
            configF.write( "description: \n")
            configF.write( "gff_name: BLASTX.gff\n")
            configF.write( "\n")
            configF.write( "[analysis2]\n")
            configF.write( "name: GMHMMWheat\n")
            configF.write( "program: GMHMM\n")
            configF.write( "programversion: 4.3\n")
            configF.write( "sourcename: src_GMHMM\n")
            configF.write( "description: \n")
            configF.write( "gff_name: GMHMM.gff\n")
            configF.write( "\n")
            configF.write( "[target]\n")
            configF.write( "target_used: yes\n")
            configF.write( "target_used_list: target.lst\n")
            configF.write( "\n")
            configF.write( "[section_with_option_pattern]\n")
            configF.write( "option1: value1\n")
            configF.write( "option2: value2\n")
            configF.write( "[second_section_with_option_pattern]\n")
            configF.write( "option1: value1\n")
            configF.write( "option2: value2\n")

    def write_config_case(self, configFileName):
        with open(configFileName, "w" ) as configF:
            configF.write( "[dir_name]\n")
            configF.write( "work_dir: toto \n")
            configF.write( "\n")
            configF.write( "[organism]\n")
            configF.write( "min_SSR_coverage: truc\n")
            configF.write( "\n")

if __name__ == "__main__":
    unittest.main()