import os
import unittest
from commons.core.checker.ConfigChecker import ConfigChecker
from commons.core.checker.ConfigChecker import ConfigRules
from commons.core.checker.ConfigValue import ConfigValue
from commons.core.checker.RepetException import RepetException

class Test_F_ConfigChecker(unittest.TestCase):
    
    def setUp(self):
        self._configFileName = "test_conf_checker"
        
    def tearDown(self):
        os.remove(self._configFileName)
     
    def test_run(self):
        iMock = MockConfig()
        iMock.write_config(self._configFileName)
        
        iConfigRules = ConfigRules()
        iConfigRules.addRuleSection(section="dir_name", mandatory=True)
        iConfigRules.addRuleOption(section="dir_name", option ="work_dir", mandatory=True)
        iConfigRules.addRuleSection(section="organism", mandatory=True)
        iConfigRules.addRuleOption(section="organism", option ="abbreviation", mandatory=True)
        iConfigRules.addRuleOption(section="organism", option ="genus", mandatory=True)
        iConfigRules.addRuleOption(section="organism", option ="species", mandatory=True)
        iConfigRules.addRuleOption(section="organism", option ="common_name", mandatory=True)
        iConfigRules.addRuleOption(section="organism", option ="comment")
        iConfigRules.addRuleSection(section="analysis", mandatory=True, isPattern=True)
        iConfigRules.addRuleOption(section="analysis", option ="name", mandatory=True)
        iConfigRules.addRuleOption(section="analysis", option ="program", mandatory=True)
        iConfigRules.addRuleOption(section="analysis", option ="sourcename", mandatory=True)
        iConfigRules.addRuleOption(section="analysis", option ="description")
        iConfigRules.addRuleOption(section="analysis", option ="gff_name")
        
        iConfigChecker = ConfigChecker(self._configFileName,iConfigRules)
        
        obsValidatedConfig = iConfigChecker.getConfig()
        
        expValidatedConfig = ConfigValue()
        d = {"dir_name" : {"work_dir":"toto"},
             "organism" : {"abbreviation":"T.aestivum",
                                  "genus":"triticum",
                                  "species":"aestivum",
                                  "common_name":"wheat",
                                  "comment":""},
             'analysis1': {'description': '',
                  'gff_name': 'BLASTX.gff2',
                  'name': 'BLASTXWheat2',
                  'program': 'BLASTX2',
                  'programversion': '3.32',
                  'sourcename': 'dummyDesc_BLASTX2'}
                                 }
        expValidatedConfig.setdOptionsValues4Sections(d)

        self.assertEqual(expValidatedConfig, obsValidatedConfig)
        
        
    def test_run_exception_section_missing(self):
        iMock = MockConfig()
        iMock.write_config_section_missing(self._configFileName)
        
        iConfigRules = ConfigRules()
        iConfigRules.addRuleSection(section="dir_name", mandatory=True)
        iConfigRules.addRuleOption(section="dir_name", option ="work_dir", mandatory=True)
        iConfigRules.addRuleSection(section="organism", mandatory=True)
        iConfigRules.addRuleOption(section="organism", option ="abbreviation", mandatory=True)
        iConfigRules.addRuleOption(section="organism", option ="genus", mandatory=True)
        iConfigRules.addRuleOption(section="organism", option ="species", mandatory=True)
        iConfigRules.addRuleOption(section="organism", option ="common_name", mandatory=True)
        iConfigRules.addRuleOption(section="organism", option ="comment")
        iConfigRules.addRuleSection(section="analysis", mandatory=True, isPattern=True)
        iConfigRules.addRuleOption(section="analysis", option ="name", mandatory=True)
        iConfigRules.addRuleOption(section="analysis", option ="program", mandatory=True)
        iConfigRules.addRuleOption(section="analysis", option ="sourcename", mandatory=True)
        iConfigRules.addRuleOption(section="analysis", option ="description")
        iConfigRules.addRuleOption(section="analysis", option ="gff_name")
        
        iConfigChecker = ConfigChecker(self._configFileName,iConfigRules)
        
        expMessage = "Error in configuration file {}, following sections are missing:\n - organism\n".format(self._configFileName)
        
        try :
            iConfigChecker.getConfig()
        except RepetException as e:
            obsMessage = e.getMessage()

        self.assertEqual(expMessage, obsMessage)
        
        
    def test_run_exception_section_pattern_false(self):
        iMock = MockConfig()
        iMock.write_config(self._configFileName)
        
        iConfigRules = ConfigRules()
        iConfigRules.addRuleSection(section="dir_name", mandatory=True)
        iConfigRules.addRuleOption(section="dir_name", option ="work_dir", mandatory=True)
        iConfigRules.addRuleSection(section="organism", mandatory=True)
        iConfigRules.addRuleOption(section="organism", option ="abbreviation", mandatory=True)
        iConfigRules.addRuleOption(section="organism", option ="genus", mandatory=True)
        iConfigRules.addRuleOption(section="organism", option ="species", mandatory=True)
        iConfigRules.addRuleOption(section="organism", option ="common_name", mandatory=True)
        iConfigRules.addRuleOption(section="organism", option ="comment")
        iConfigRules.addRuleSection(section="analysis", mandatory=True)
        iConfigRules.addRuleOption(section="analysis", option ="name", mandatory=True)
        iConfigRules.addRuleOption(section="analysis", option ="program", mandatory=True)
        iConfigRules.addRuleOption(section="analysis", option ="sourcename", mandatory=True)
        iConfigRules.addRuleOption(section="analysis", option ="description")
        iConfigRules.addRuleOption(section="analysis", option ="gff_name")
        
        iConfigChecker = ConfigChecker(self._configFileName,iConfigRules)
        
        expMessage = "Error in configuration file {}, following sections are missing:\n - analysis\n".format(self._configFileName)
        
        try :
            iConfigChecker.getConfig()
        except RepetException as e:
            obsMessage = e.getMessage()

        self.assertEqual(expMessage, obsMessage)
        
        
    def test_run_exception_option_missing(self):
        iMock = MockConfig()
        iMock.write_config_option_missing(self._configFileName)
        
        iConfigRules = ConfigRules()
        iConfigRules.addRuleSection(section="dir_name", mandatory=True)
        iConfigRules.addRuleOption(section="dir_name", option ="work_dir", mandatory=True)
        iConfigRules.addRuleSection(section="organism", mandatory=True)
        iConfigRules.addRuleOption(section="organism", option ="abbreviation", mandatory=True)
        iConfigRules.addRuleOption(section="organism", option ="genus", mandatory=True)
        iConfigRules.addRuleOption(section="organism", option ="species", mandatory=True)
        iConfigRules.addRuleOption(section="organism", option ="common_name", mandatory=True)
        iConfigRules.addRuleOption(section="organism", option ="comment")
        iConfigRules.addRuleSection(section="analysis", mandatory=True, isPattern=True)
        iConfigRules.addRuleOption(section="analysis", option ="name", mandatory=True)
        iConfigRules.addRuleOption(section="analysis", option ="program", mandatory=True)
        iConfigRules.addRuleOption(section="analysis", option ="sourcename", mandatory=True)
        iConfigRules.addRuleOption(section="analysis", option ="description")
        iConfigRules.addRuleOption(section="analysis", option ="gff_name")
        
        iConfigChecker = ConfigChecker(self._configFileName,iConfigRules)
        
        expMessage = "Error in configuration file {}, following options are missing: \n - [organism]: abbreviation\n".format(self._configFileName)
        
        try :
            iConfigChecker.getConfig()
        except RepetException as e:
            obsMessage = e.getMessage()

        self.assertEqual(expMessage, obsMessage)
            
class MockConfig (object):
   
    def write_config(self, configFileName):
        with open(configFileName, "w" ) as configF:
            configF.write( "[dir_name]\n")
            configF.write( "work_dir : toto \n")
            configF.write( "\n")
            configF.write( "[organism]\n")
            configF.write( "abbreviation: T.aestivum\n")
            configF.write( "genus: triticum\n")
            configF.write( "species: aestivum\n")
            configF.write( "common_name: wheat\n")
            configF.write( "comment: \n")
            configF.write( "[analysis1]\n")
            configF.write( "name: BLASTXWheat2\n")
            configF.write( "program: BLASTX2\n")
            configF.write( "programversion: 3.32\n")
            configF.write( "sourcename: dummyDesc_BLASTX2\n")
            configF.write( "description: \n")
            configF.write( "gff_name: BLASTX.gff2\n")
            configF.write( "\n")
        
    def write_config_section_missing(self, configFileName):
        with open(configFileName, "w" ) as configF:
            configF.write( "[dir_name]\n")
            configF.write( "work_dir : toto \n")
            configF.write( "\n")
            configF.write( "[analysis1]\n")
            configF.write( "name: BLASTXWheat2\n")
            configF.write( "program: BLASTX2\n")
            configF.write( "programversion: 3.32\n")
            configF.write( "sourcename: dummyDesc_BLASTX2\n")
            configF.write( "description: \n")
            configF.write( "gff_name: BLASTX.gff2\n")
            configF.write( "\n")

        
    def write_config_option_missing(self, configFileName):
        with open(configFileName, "w" ) as configF:
            configF.write( "[dir_name]\n")
            configF.write( "work_dir : toto \n")
            configF.write( "\n")
            configF.write( "[organism]\n")
            configF.write( "genus: triticum\n")
            configF.write( "species: aestivum\n")
            configF.write( "common_name: wheat\n")
            configF.write( "comment: \n")
            configF.write( "[analysis1]\n")
            configF.write( "name: BLASTXWheat2\n")
            configF.write( "program: BLASTX2\n")
            configF.write( "programversion: 3.32\n")
            configF.write( "sourcename: dummyDesc_BLASTX2\n")
            configF.write( "description: \n")
            configF.write( "gff_name: BLASTX.gff2\n")
            configF.write( "\n")

        
if __name__ == "__main__":
    unittest.main()