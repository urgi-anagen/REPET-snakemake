# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import subprocess
import unittest
import os

from commons.core.checker.CheckerUtils import CheckerUtils
from commons.core.checker.CheckerException import CheckerException
try :
    import ConfigParser
    from ConfigParser import NoOptionError
    from ConfigParser import NoSectionError
except ImportError :
    import configparser as ConfigParser
    from configparser import NoOptionError
    from configparser import NoSectionError

class Test_CheckerUtils( unittest.TestCase ):
    
    def setUp(self):
        self.queueFileName = "queueName.txt"
        self.configFileName = "dummyConfig.cfg"
    
    def tearDown(self):
        if os.path.exists(self.queueFileName):
            os.remove(self.queueFileName)
        if os.path.exists(self.configFileName):
            os.remove(self.configFileName)
    
    def test_isBlastNameInBlastValues( self ):
        correctValueList = [ "blastn", "blastp", "blastx", "tblastn", "tblastx" ]
        for value in correctValueList:
            self.assertFalse( CheckerUtils.isBlastNameNotInBlastValues( value ) )
            
        incorrectValueList = [ "badbalst", "wublast" ]
        for value in incorrectValueList:
            self.assertTrue( CheckerUtils.isBlastNameNotInBlastValues( value ) )
            
    def test_isNotTRUEisNotFALSE( self ):
        correctValueList = [ "TRUE", "FALSE" ]
        for value in correctValueList:
            self.assertFalse( CheckerUtils.isNotTRUEisNotFALSE( value ) )
            
        incorrectValueList = [ "True", "False" ]
        for value in incorrectValueList:
            self.assertTrue( CheckerUtils.isNotTRUEisNotFALSE( value ) )
            
    def test_isRessourceNotExists( self ):
        fileName = "dummyFile.txt"
        self.assertTrue( CheckerUtils.isRessourceNotExits( fileName ) )
        subprocess.call( "touch {}".format( fileName ), shell=True)
        self.assertFalse( CheckerUtils.isRessourceNotExits( fileName ) )
        os.remove( fileName )
        
    def test_isNotAeValueWithOneDigit2DecimalsAtLeast( self ):
        correctEValueList = [ "5e-32", "7e-45", "1e-2122", "9e-32" ]
        for value in correctEValueList:
            self.assertFalse( CheckerUtils.isNotAeValueWithOneDigit2DecimalsAtLeast( value ) )
            
        incorrecEValueStr = [ "10e-32", "2e-3", "2e-2", "1", "cxhhe" ]
        for value in incorrecEValueStr:
            self.assertTrue( CheckerUtils.isNotAeValueWithOneDigit2DecimalsAtLeast( value ) )
            
    def test_isNotADigit( self ):
        correctDigitList = [ "1", "21", "345" ]
        for value in correctDigitList:
            self.assertFalse( CheckerUtils.isNotANumber( value ) )
            
        incorrectDigitList = [ "A", "dfdf", "((((" ]
        for value in incorrectDigitList:
            self.assertTrue( CheckerUtils.isNotANumber( value ) )
            
    def test_isExecutableInUserPath( self ):
        exeName = "ls"
        self.assertTrue( CheckerUtils.isExecutableInUserPath( exeName ) )

        exeName = "hsvegdtefaobfbcheta"
        self.assertFalse( CheckerUtils.isExecutableInUserPath( exeName ) )
    
    def test_isExecutableInUserPath_case_endsWith( self ):
        exeName = "s"
        self.assertFalse( CheckerUtils.isExecutableInUserPath( exeName ) )

    def test_isQueueNameValid_valid(self):
        cmd = "qconf -sql > {}".format(self.queueFileName)
        subprocess.call(cmd, shell=True)
        with open(self.queueFileName, "r") as f:
            lQueueNames = f.readlines()

        if lQueueNames == []:
            print ("Warning you need SGE installed\n")
        else:
            self.assertTrue(CheckerUtils.isQueueNameValid(lQueueNames[0].strip()))
        
    def test_isQueueNameValid_not_valid(self):
        cmd = "qconf -sql > {}".format(self.queueFileName)
        subprocess.call(cmd, shell=True)
        with open(self.queueFileName, "r") as f :
            lQueueNames = f.readlines()

        if lQueueNames == []:
            print ("Warning you need SGE installed\n")
        else:
            self.assertFalse(CheckerUtils.isQueueNameValid("dummyQueueName"))

    def test_isQueueNameValid_valid_with_parameter(self):
        cmd = "qconf -sql > {}".format(self.queueFileName)
        subprocess.call(cmd, shell=True)
        with open(self.queueFileName, "r") as f:
            lQueueNames = f.readlines()

        if lQueueNames == []:
            print("Warning you need SGE installed\n")
        else:
            queueNameWithParameter = "{} test=TRUE".format(lQueueNames[0].strip())
            self.assertTrue(CheckerUtils.isQueueNameValid(queueNameWithParameter))
            
    def test_isCharAlphanumOrUnderscore_valid(self):
        project_name = "Project_Name"
        self.assertTrue(CheckerUtils.isCharAlphanumOrUnderscore(project_name))
        
    def test_isCharAlphanumOrUnderscore_not_valid(self):
        project_name = "Project_Name,"
        self.assertFalse(CheckerUtils.isCharAlphanumOrUnderscore(project_name))
        
    def test_isMax15Char_valid(self):
        project_name = "Project_Name"
        self.assertTrue(CheckerUtils.isMax15Char(project_name))
        
    def test_isMax15Char_not_valid(self):
        project_name = "Project_Name_tooLong"
        self.assertFalse(CheckerUtils.isMax15Char(project_name))
        
    def test_checkSectionInConfigFile_empty_section(self):
        sectionName = "sectionName"
        line = "[{}]".format(sectionName)
        with open(self.configFileName, "w") as configFile :
            configFile.write(line)

        config = ConfigParser.ConfigParser()
        with open(self.configFileName, "r") as configFile :
            try :
                config.read_file(configFile)
            except AttributeError :
                config.readfp(configFile)
        
        isSectionExceptionRaised = False
        try:
            CheckerUtils.checkSectionInConfigFile(config, sectionName)
        except NoSectionError:
            isSectionExceptionRaised = True

        self.assertFalse(isSectionExceptionRaised)
    
    def test_checkSectionInConfigFile(self):
        sectionName = "sectionName"
        lines = "[{}]\ndummyOption : dummyOption\n".format(sectionName)
        with open(self.configFileName, "w") as configFile :
            configFile.write(lines)
        config = ConfigParser.ConfigParser()
        with open(self.configFileName, "r") as configFile:
            try :
                config.read_file(configFile)
            except AttributeError :
                config.readfp(configFile)

        isSectionExceptionRaised = False
        try:
            CheckerUtils.checkSectionInConfigFile(config, sectionName)
        except NoSectionError:
            isSectionExceptionRaised = True

        self.assertFalse(isSectionExceptionRaised)
        
    def test_checkSectionInConfigFile_not_valid(self):
        sectionName = "sectionName"
        line = " "
        with open(self.configFileName, "w") as configFile:
            configFile.write(line)
        config = ConfigParser.ConfigParser()
        with open(self.configFileName, "r") as configFile:
            try :
                config.read_file(configFile)
            except AttributeError :
                config.readfp(configFile)
        
        isSectionExceptionRaised = False
        try:
            CheckerUtils.checkSectionInConfigFile(config, sectionName)
        except NoSectionError:
            isSectionExceptionRaised = True

        self.assertTrue(isSectionExceptionRaised)
        
    def test_checkOptionInSectionInConfigFile(self):
        optionName = "optionName"
        sectionName = "sectionName"
        with open(self.configFileName, "w") as configFile:
            configFile.write("[{}]\n".format(sectionName))
            configFile.write("{}: ".format(optionName))

        config = ConfigParser.ConfigParser()
        with open(self.configFileName, "r") as configFile:
            try :
                config.read_file(configFile)
            except AttributeError :
                config.readfp(configFile)
        
        isSectionExceptionRaised = False
        try:
            CheckerUtils.checkOptionInSectionInConfigFile(config, sectionName, optionName)
        except NoOptionError:
            isSectionExceptionRaised = True

        self.assertFalse(isSectionExceptionRaised)
        
    def test_checkOptionInSectionInConfigFile_not_present(self):
        optionName = " "
        sectionName = "sectionName"
        with open(self.configFileName, "w") as configFile:
            configFile.write("[{}]\n".format(sectionName))
            configFile.write(optionName)

        config = ConfigParser.ConfigParser()
        with open(self.configFileName, "r") as configFile:
            try :
                config.read_file(configFile)
            except AttributeError :
                config.readfp(configFile)
        
        isSectionExceptionRaised = False
        try:
            CheckerUtils.checkOptionInSectionInConfigFile(config, sectionName, optionName)
        except NoOptionError:
            isSectionExceptionRaised = True

        self.assertTrue(isSectionExceptionRaised)
        
    def test_checkOptionInSectionInConfigFile_not_in_the_right_section(self):
        optionName = "optionName"
        rightSection = "dummySection2"
        with open(self.configFileName, "w") as configFile:
            configFile.write("[dummySection1]\n")
            configFile.write("{}: \n".format(optionName))
            configFile.write("[{}]".format(rightSection))

        config = ConfigParser.ConfigParser()
        with open(self.configFileName, "r") as configFile:
            try:
                config.read_file(configFile)
            except AttributeError:
                config.readfp(configFile)
        
        isSectionExceptionRaised = False
        try:
            CheckerUtils.checkOptionInSectionInConfigFile(config, rightSection, optionName)
        except NoOptionError:
            isSectionExceptionRaised = True
        self.assertTrue(isSectionExceptionRaised)
        
    def test_checkConfigVersion_different_version_between_changeLog_and_configFile(self):
        changeLogFileName = "dummyCHANGELOG"
        
        self._writeChangeLogFile(changeLogFileName)
        self._writeConfigFile("repet_version: 1.3.5\n")
        
        with open(changeLogFileName, "r") as changeLogFileHandler:

            config = ConfigParser.ConfigParser()
            with open(self.configFileName, "r") as configFileHandler :
                try:
                    config.read_file(configFileHandler)
                except AttributeError:
                    config.readfp(configFileHandler)

            self.assertRaises(CheckerException, CheckerUtils.checkConfigVersion, changeLogFileHandler, config)

        os.remove(changeLogFileName)
        
    def test_checkConfigVersion_same_version_between_changeLog_and_configFile(self):
        changeLogFileName = "dummyCHANGELOG"
        self._writeChangeLogFile(changeLogFileName)
        self._writeConfigFile("repet_version: 1.3.6\n")
        
        with open(changeLogFileName, "r") as changeLogFileHandler:

            config = ConfigParser.ConfigParser()
            with open(self.configFileName, "r") as configFileHandler:
                try:
                    config.read_file(configFileHandler)
                except AttributeError:
                    config.readfp(configFileHandler)

            isCheckerExceptionRaised = False
            try:
                CheckerUtils.checkConfigVersion(changeLogFileHandler, config)
            except CheckerException:
                isCheckerExceptionRaised = True
            self.assertFalse(isCheckerExceptionRaised)

        os.remove(changeLogFileName)
        
    def test_getVersionFromChangelogFile(self):
        changeLogFileName = "dummyCHANGELOG"
        self._writeChangeLogFile(changeLogFileName)
        obsVer = CheckerUtils.getVersionFromChangelogFile(changeLogFileName)
        expVer = "1.3.6"
        
        self.assertEqual(obsVer, expVer)
        
        os.remove(changeLogFileName)
        
    def test_checkConfigVersion_check_exception_message(self):
        changeLogFileName = "dummyCHANGELOG"
        self._writeChangeLogFile(changeLogFileName)
        self._writeConfigFile("repet_version: 1.3.5\n")
        
        with open(changeLogFileName, "r") as changeLogFileHandler:
            config = ConfigParser.ConfigParser()
            with open(self.configFileName, "r") as configFileHandler:
                try :
                    config.read_file(configFileHandler)
                except AttributeError:
                    config.readfp(configFileHandler)
            checkerExceptionInstance = None
            try:
                CheckerUtils.checkConfigVersion(changeLogFileHandler, config)
            except CheckerException as e:
                checkerExceptionInstance = e
        
        expMessage = "*** Error: wrong config file version. Expected version num is 1.3.6 but actual in config file is 1.3.5"
        obsMessage = checkerExceptionInstance.msg
           
        self.assertEqual(expMessage, obsMessage)
        
    def test_checkHeaders_first_header_with_question_mark(self):
        fastaFileName = "dummyFasta.fa"
        self._writeFastaFile_first_header_with_question_mark(fastaFileName)

        isExceptionRaised = False
        with open(fastaFileName, "r") as fastaFileHandler:
            try:
                CheckerUtils.checkHeaders(fastaFileHandler)
            except CheckerException as e:
                isExceptionRaised = True
                os.remove(fastaFileName)
                self.assertTrue(isExceptionRaised)
                expMessages = ["DmelChr4_Bla?ster_Piler_0.0_Map_3"]
                obsMessages = e.getMessages()

                self.assertEqual(expMessages, obsMessages)

    def test_checkHeaders_with_2_wrong_headers(self):
        fastaFileName = "dummyFasta.fa"
        self._writeFastaFile_with_2_wrong_headers(fastaFileName)
        fastaFileHandler = open(fastaFileName,"r")
        isExceptionRaised = False

        try:
            CheckerUtils.checkHeaders(fastaFileHandler)
        except CheckerException as e:
            isExceptionRaised = True
            
            fastaFileHandler.close()
            os.remove(fastaFileName)

            self.assertTrue(isExceptionRaised)

            expMessages = ["DmelChr4_Bla?ster_Piler_0.0_Map_3","DmelChr4_Bla!ster_Piler_1.0_Map_9"]
            obsMessages = e.getMessages()

            self.assertEqual(expMessages, obsMessages)

    def test_checkHeaders_with_pipe(self):
        fastaFileName = "dummyFasta.fa"
        self._writeFastaFile_with_pipe(fastaFileName)
        fastaFileHandler = open(fastaFileName,"r")
        isExceptionRaised = False
        try:
            CheckerUtils.checkHeaders(fastaFileHandler)
        except CheckerException as e:
            isExceptionRaised = True
            
            fastaFileHandler.close()
            os.remove(fastaFileName)

            self.assertTrue(isExceptionRaised)

            expMessages = ["DmelC|hr4_Blas-ter_Piler_1.0_Map_9"]
            obsMessages = e.getMessages()

            self.assertEqual(expMessages, obsMessages)

    def test_checkHeaders_with_equal(self):
        fastaFileName = "dummyFasta.fa"
        self._writeFastaFile_with_equal(fastaFileName)
        fastaFileHandler = open(fastaFileName,"r")
        isExceptionRaised = False
        try:
            CheckerUtils.checkHeaders(fastaFileHandler)
        except CheckerException as e:
            isExceptionRaised = True
            
            fastaFileHandler.close()
            os.remove(fastaFileName)

            self.assertTrue(isExceptionRaised)

            expMessages = ["DmelC:hr4_Blas=ter_Piler_1.0_Map_9"]
            obsMessages = e.getMessages()

            self.assertEqual(expMessages, obsMessages)

    def test_checkHeaders_all_headers_ok(self):
        fastaFileName = "dummyFasta.fa"
        self._writeFastaFile_all_headers_ok(fastaFileName)
        fastaFileHandler = open(fastaFileName, "r")
        isExceptionRaised = False
        try:
            CheckerUtils.checkHeaders(fastaFileHandler)
        except CheckerException:
            isExceptionRaised = True
            
        fastaFileHandler.close()
        os.remove(fastaFileName)
            
        self.assertFalse(isExceptionRaised)

    def _writeFastaFile_first_header_with_question_mark(self, fastaFileName):    
        with open(fastaFileName, "w") as fastaFileHandler :
            fastaFileHandler.write(">DmelChr4_Bla?ster_Piler_0.0_Map_3\n")
            fastaFileHandler.write("ACCAAAGACACTAGAATAACAAGATGCGTAACGCCATACGATTTTTTGGCACACTATTTT\n")
            fastaFileHandler.write("TTCGCCGTGGCTCTAGAGGTGGCTCCAGGCTCTCTCGAATTTTTGTTAGAGAGCGAGAGA\n")
            fastaFileHandler.write("GCTGAGAGCGCTACAGCGAACAGCTCTTTTCTACACATAAAGTGATAGCAGACAACTGTA\n")
            fastaFileHandler.write("TGTGTGCACACGTGTGCTCATGCATTGTAAATTTGACAAAATATGCCCTTCACCTTCAAA\n")
            fastaFileHandler.write(">DmelChr4_Blaster_Piler_1.0_Map_9\n")
            fastaFileHandler.write("AGTTTAAAAACCAAAGACACTAGAATAACAAGATGCGTAACGGCCATACATTGGTTTGGC\n")
            fastaFileHandler.write("ACTATGCAGCCACTTTTTTGGTGACGGCCAAAATTACTCTCTTTCCGCTCACTCCCGCTG\n")
            fastaFileHandler.write("AGAGCGTAAGAAATCTAAAAATATAATTTGCTTGCTTGTGTGAGTAAAAACAAGAGACGA\n")
            fastaFileHandler.write("GAACGCGTATAAGTGTGCGTGTTGTGCTAGAAGACGATTTTCGGGACCGAAATCAATTCT\n")
            fastaFileHandler.write("GATCGAAGAAACGAATTTACATGGTACATATTAGGGTAGTTTTTGCCAATTTCCTAGCAA\n")
    
    def _writeFastaFile_with_2_wrong_headers(self, fastaFileName):    
        with open(fastaFileName, "w") as fastaFileHandler:
            fastaFileHandler.write(">DmelChr4_Bla?ster_Piler_0.0_Map_3\n")
            fastaFileHandler.write("ACCAAAGACACTAGAATAACAAGATGCGTAACGCCATACGATTTTTTGGCACACTATTTT\n")
            fastaFileHandler.write("TTCGCCGTGGCTCTAGAGGTGGCTCCAGGCTCTCTCGAATTTTTGTTAGAGAGCGAGAGA\n")
            fastaFileHandler.write("GCTGAGAGCGCTACAGCGAACAGCTCTTTTCTACACATAAAGTGATAGCAGACAACTGTA\n")
            fastaFileHandler.write("TGTGTGCACACGTGTGCTCATGCATTGTAAATTTGACAAAATATGCCCTTCACCTTCAAA\n")
            fastaFileHandler.write(">DmelChr4_Bla!ster_Piler_1.0_Map_9\n")
            fastaFileHandler.write("AGTTTAAAAACCAAAGACACTAGAATAACAAGATGCGTAACGGCCATACATTGGTTTGGC\n")
            fastaFileHandler.write("ACTATGCAGCCACTTTTTTGGTGACGGCCAAAATTACTCTCTTTCCGCTCACTCCCGCTG\n")
            fastaFileHandler.write("AGAGCGTAAGAAATCTAAAAATATAATTTGCTTGCTTGTGTGAGTAAAAACAAGAGACGA\n")
            fastaFileHandler.write("GAACGCGTATAAGTGTGCGTGTTGTGCTAGAAGACGATTTTCGGGACCGAAATCAATTCT\n")
            fastaFileHandler.write("GATCGAAGAAACGAATTTACATGGTACATATTAGGGTAGTTTTTGCCAATTTCCTAGCAA\n")
    
    def _writeFastaFile_all_headers_ok(self, fastaFileName):    
        with open(fastaFileName, "w") as fastaFileHandler:
            fastaFileHandler.write(">DmelChr4_Blaster_Piler_0.0_Map_3\n")
            fastaFileHandler.write("ACCAAAGACACTAGAATAACAAGATGCGTAACGCCATACGATTTTTTGGCACACTATTTT\n")
            fastaFileHandler.write("TTCGCCGTGGCTCTAGAGGTGGCTCCAGGCTCTCTCGAATTTTTGTTAGAGAGCGAGAGA\n")
            fastaFileHandler.write("GCTGAGAGCGCTACAGCGAACAGCTCTTTTCTACACATAAAGTGATAGCAGACAACTGTA\n")
            fastaFileHandler.write("TGTGTGCACACGTGTGCTCATGCATTGTAAATTTGACAAAATATGCCCTTCACCTTCAAA\n")
            fastaFileHandler.write(">DmelC:hr4_Blas-ter_Piler_1.0_Map_9\n")
            fastaFileHandler.write("AGTTTAAAAACCAAAGACACTAGAATAACAAGATGCGTAACGGCCATACATTGGTTTGGC\n")
            fastaFileHandler.write("ACTATGCAGCCACTTTTTTGGTGACGGCCAAAATTACTCTCTTTCCGCTCACTCCCGCTG\n")
            fastaFileHandler.write("AGAGCGTAAGAAATCTAAAAATATAATTTGCTTGCTTGTGTGAGTAAAAACAAGAGACGA\n")
            fastaFileHandler.write("GAACGCGTATAAGTGTGCGTGTTGTGCTAGAAGACGATTTTCGGGACCGAAATCAATTCT\n")
            fastaFileHandler.write("GATCGAAGAAACGAATTTACATGGTACATATTAGGGTAGTTTTTGCCAATTTCCTAGCAA\n")
    
    def _writeFastaFile_with_pipe(self, fastaFileName):    
        with open(fastaFileName, "w") as fastaFileHandler:
            fastaFileHandler.write(">DmelChr4_Blaster_Piler_0.0_Map_3\n")
            fastaFileHandler.write("ACCAAAGACACTAGAATAACAAGATGCGTAACGCCATACGATTTTTTGGCACACTATTTT\n")
            fastaFileHandler.write("TTCGCCGTGGCTCTAGAGGTGGCTCCAGGCTCTCTCGAATTTTTGTTAGAGAGCGAGAGA\n")
            fastaFileHandler.write("GCTGAGAGCGCTACAGCGAACAGCTCTTTTCTACACATAAAGTGATAGCAGACAACTGTA\n")
            fastaFileHandler.write("TGTGTGCACACGTGTGCTCATGCATTGTAAATTTGACAAAATATGCCCTTCACCTTCAAA\n")
            fastaFileHandler.write(">DmelC|hr4_Blas-ter_Piler_1.0_Map_9\n")
            fastaFileHandler.write("AGTTTAAAAACCAAAGACACTAGAATAACAAGATGCGTAACGGCCATACATTGGTTTGGC\n")
            fastaFileHandler.write("ACTATGCAGCCACTTTTTTGGTGACGGCCAAAATTACTCTCTTTCCGCTCACTCCCGCTG\n")
            fastaFileHandler.write("AGAGCGTAAGAAATCTAAAAATATAATTTGCTTGCTTGTGTGAGTAAAAACAAGAGACGA\n")
            fastaFileHandler.write("GAACGCGTATAAGTGTGCGTGTTGTGCTAGAAGACGATTTTCGGGACCGAAATCAATTCT\n")
            fastaFileHandler.write("GATCGAAGAAACGAATTTACATGGTACATATTAGGGTAGTTTTTGCCAATTTCCTAGCAA\n")
    
    def _writeFastaFile_with_equal(self, fastaFileName):    
        with open(fastaFileName, "w") as fastaFileHandler:
            fastaFileHandler.write(">DmelChr4_Blaster_Piler_0.0_Map_3\n")
            fastaFileHandler.write("ACCAAAGACACTAGAATAACAAGATGCGTAACGCCATACGATTTTTTGGCACACTATTTT\n")
            fastaFileHandler.write("TTCGCCGTGGCTCTAGAGGTGGCTCCAGGCTCTCTCGAATTTTTGTTAGAGAGCGAGAGA\n")
            fastaFileHandler.write("GCTGAGAGCGCTACAGCGAACAGCTCTTTTCTACACATAAAGTGATAGCAGACAACTGTA\n")
            fastaFileHandler.write("TGTGTGCACACGTGTGCTCATGCATTGTAAATTTGACAAAATATGCCCTTCACCTTCAAA\n")
            fastaFileHandler.write(">DmelC:hr4_Blas=ter_Piler_1.0_Map_9\n")
            fastaFileHandler.write("AGTTTAAAAACCAAAGACACTAGAATAACAAGATGCGTAACGGCCATACATTGGTTTGGC\n")
            fastaFileHandler.write("ACTATGCAGCCACTTTTTTGGTGACGGCCAAAATTACTCTCTTTCCGCTCACTCCCGCTG\n")
            fastaFileHandler.write("AGAGCGTAAGAAATCTAAAAATATAATTTGCTTGCTTGTGTGAGTAAAAACAAGAGACGA\n")
            fastaFileHandler.write("GAACGCGTATAAGTGTGCGTGTTGTGCTAGAAGACGATTTTCGGGACCGAAATCAATTCT\n")
            fastaFileHandler.write("GATCGAAGAAACGAATTTACATGGTACATATTAGGGTAGTTTTTGCCAATTTCCTAGCAA\n")

    def _writeChangeLogFile(self, changeLogFileName ):
        with open(changeLogFileName, "w") as changeLogFileHandler :
            changeLogFileHandler.write("ChangeLog of REPET\n")
            changeLogFileHandler.write("\n")
            changeLogFileHandler.write("\n")
            changeLogFileHandler.write("\n")
            changeLogFileHandler.write("REPET release 1.3.6\n")
            changeLogFileHandler.write("(release date XX/XX/2010)\n")
            changeLogFileHandler.write("\n")

    def _writeConfigFile(self, lineVersion):

        with open(self.configFileName, "w") as configFileHandler:
            configFileHandler.write("[repet_env]\n")
            configFileHandler.write(lineVersion)
            configFileHandler.write("repet_host: <your_MySQL_host>\n")
        
        

if __name__ == "__main__":
    test_suite = unittest.TestSuite()
    test_suite.addTest(unittest.makeSuite(Test_CheckerUtils))
    unittest.TextTestRunner(verbosity=2).run(test_suite)
