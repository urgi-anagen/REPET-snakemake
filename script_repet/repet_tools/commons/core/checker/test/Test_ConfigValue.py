import unittest
from commons.core.checker.ConfigValue import ConfigValue

class Test_ConfigValue(unittest.TestCase):
    
    def setUp(self):
        self._iConfigValue = ConfigValue()
        
    def test__eq__True(self):
        self._iConfigValue.dOptionsValues4Sections = {
                    "dir_name" : {"work_dir":"toto"},
                    "organism" : {"abbreviation":"T.aestivum",
                                  "genus":"triticum",
                                  "species":"aestivum",
                                  "common_name":"wheat",
                                  "comment":""}
                                                        }
        iConfigValue1 = ConfigValue()                         
        iConfigValue1.dOptionsValues4Sections = {
                    "dir_name" : {"work_dir":"toto"},
                    "organism" : {"abbreviation":"T.aestivum",
                                  "genus":"triticum",
                                  "species":"aestivum",
                                  "common_name":"wheat",
                                  "comment":""}
                                                }
        
        self.assertEqual(self._iConfigValue, iConfigValue1)
        
    def test__eq__False_not_same_section(self):
        self._iConfigValue.dOptionsValues4Sections = {
                    "dir_name" : {"work_dir":"toto"},
                    "organisms" : {"abbreviation":"T.aestivum",
                                  "genus":"triticum",
                                  "species":"aestivum",
                                  "common_name":"wheat",
                                  "comment":""}
                                                        }
        iConfigValue1 = ConfigValue()                         
        iConfigValue1.dOptionsValues4Sections = {
                    "dir_name" : {"work_dir":"toto"},
                    "organism" : {"abbreviation":"T.aestivum",
                                  "genus":"triticum",
                                  "species":"aestivum",
                                  "common_name":"wheat",
                                  "comment":""}
                                                }
        
        self.assertNotEqual(self._iConfigValue, iConfigValue1)
                                                
                                                
    def test__eq__False_not_same_option(self):
        self._iConfigValue.dOptionsValues4Sections = {
                    "dir_name" : {"work_dir":"toto"},
                    "organism" : {"abbreviation":"T.aestivum",
                                  "family":"triticum",
                                  "species":"aestivum",
                                  "common_name":"wheat",
                                  "comment":""}
                                                        }
        iConfigValue1 = ConfigValue()                         
        iConfigValue1.dOptionsValues4Sections = {
                    "dir_name" : {"work_dir":"toto"},
                    "organism" : {"abbreviation":"T.aestivum",
                                  "genus":"triticum",
                                  "species":"aestivum",
                                  "common_name":"wheat",
                                  "comment":""}
                                                }
        
        self.assertNotEqual(self._iConfigValue, iConfigValue1)
        
    def test__eq__False_not_same_value(self):
        self._iConfigValue.dOptionsValues4Sections = {
                    "dir_name" : {"work_dir":"toto"},
                    "organism" : {"abbreviation":"T.aestivum",
                                  "genus":"vitis",
                                  "species":"aestivum",
                                  "common_name":"wheat",
                                  "comment":""}
                                                        }
        iConfigValue1 = ConfigValue()                         
        iConfigValue1.dOptionsValues4Sections = {
                    "dir_name" : {"work_dir":"toto"},
                    "organism" : {"abbreviation":"T.aestivum",
                                  "genus":"triticum",
                                  "species":"aestivum",
                                  "common_name":"wheat",
                                  "comment":""}
                                                }
        
        self.assertNotEqual(self._iConfigValue, iConfigValue1)
                                                
    def test_has_section_true(self):
        self._iConfigValue.dOptionsValues4Sections = {
                    "dir_name" : {"work_dir":"toto"},
                    "organism" : {"abbreviation":"T.aestivum",
                                  "genus":"triticum",
                                  "species":"aestivum",
                                  "common_name":"wheat",
                                  "comment":""}
                                 }
        
        obsSectionExist = self._iConfigValue.has_section("organism")
        self.assertTrue(obsSectionExist)
 
    def test_has_section_false(self):
        self._iConfigValue.dOptionsValues4Sections = {
                    "dir_name" : {"work_dir":"toto"},
                    "organism" : {"abbreviation":"T.aestivum",
                                  "genus":"triticum",
                                  "species":"aestivum",
                                  "common_name":"wheat",
                                  "comment":""}
                                 }
        
        obsSectionExist = self._iConfigValue.has_section("toto")
        self.assertFalse(obsSectionExist)  
        
    def test_has_option_true(self):
        self._iConfigValue.dOptionsValues4Sections = {
                    "dir_name" : {"work_dir":"toto"},
                    "organism" : {"abbreviation":"T.aestivum",
                                  "genus":"triticum",
                                  "species":"aestivum",
                                  "common_name":"wheat",
                                  "comment":""}
                                 }
        
        obsOptionExist = self._iConfigValue.has_option("organism","genus")
        self.assertTrue(obsOptionExist)
 
    def test_has_option_false(self):
        self._iConfigValue.dOptionsValues4Sections = {
                    "dir_name" : {"work_dir":"toto"},
                    "organism" : {"abbreviation":"T.aestivum",
                                  "genus":"triticum",
                                  "species":"aestivum",
                                  "common_name":"wheat",
                                  "comment":""}
                                 }
        
        obsOptionExist = self._iConfigValue.has_option("organism","toto")
        self.assertFalse(obsOptionExist)
        obsOptionExist = self._iConfigValue.has_option("toto","genus")
        self.assertFalse(obsOptionExist)

    def test_sections(self):
        self._iConfigValue.dOptionsValues4Sections = {
                    "dir_name" : {"work_dir":"toto"},
                    "organism" : {"abbreviation":"T.aestivum",
                                  "genus":"triticum",
                                  "species":"aestivum",
                                  "common_name":"wheat",
                                  "comment":""}
                                 }
        expListSections = ["dir_name", "organism"]
        obsListSections = self._iConfigValue.sections()
        self.assertEqual(expListSections, obsListSections)
        
    def test_sections_empty_config(self):
        self._iConfigValue.dOptionsValues4Sections = {}
        expListSections = []
        obsListSections = self._iConfigValue.sections()
        self.assertEqual(expListSections, obsListSections)

    def test_options(self):
        self._iConfigValue.dOptionsValues4Sections = {
                    "dir_name" : {"work_dir":"toto"},
                    "organism" : {"abbreviation":"T.aestivum",
                                  "genus":"triticum",
                                  "species":"aestivum",
                                  "common_name":"wheat",
                                  "comment":""}
                                 }
        expListOptions = ['abbreviation', 'comment', 'common_name', 'genus', 'species' ]
        obsListOptions = self._iConfigValue.options("organism")
        self.assertEqual(expListOptions, obsListOptions)
   
        expListOptions = ["work_dir"]
        obsListOptions = self._iConfigValue.options("dir_name")
        self.assertEqual(expListOptions, obsListOptions)
             
    def test_options_empty_config(self):
        self._iConfigValue.dOptionsValues4Sections = {}
        expListOptions = []
        obsListOptions = self._iConfigValue.options("toto")
        self.assertEqual(expListOptions, obsListOptions)

    def test_set(self):
        self._iConfigValue.dOptionsValues4Sections = {}
        expDictOptionsValue = {"dir_name" : {"work_dir":"toto"}}
        self._iConfigValue.set("dir_name", "work_dir", "toto")
        obsDictOptionsValue = self._iConfigValue.dOptionsValues4Sections
        self.assertEqual(expDictOptionsValue, obsDictOptionsValue)
        
    def test_get(self):
        self._iConfigValue.dOptionsValues4Sections = {
                    "dir_name" : {"work_dir":"toto"},
                    "organism" : {"abbreviation":"T.aestivum",
                                  "genus":"triticum",
                                  "species":"aestivum",
                                  "common_name":"wheat",
                                  "comment":""}
                                 }
        expValue = "aestivum"
        obsValue = self._iConfigValue.get("organism", "species")
        self.assertEqual(expValue, obsValue)
        expValue = None
        obsValue = self._iConfigValue.get("toto", "species")
        self.assertEqual(expValue, obsValue)
        expValue = None
        obsValue = self._iConfigValue.get("organism", "dummyopt")
        self.assertEqual(expValue, obsValue)
        
if __name__ == "__main__":
    unittest.main()