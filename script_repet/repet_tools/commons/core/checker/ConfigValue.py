# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.


class ConfigValue(object):
    
    def __init__(self):
        self.dOptionsValues4Sections={}
        
    def has_section(self,sectionName):
        return sectionName in self.dOptionsValues4Sections
    
    def has_option(self, sectionName, optionName):
        isOptionExist = False
        if self.has_section(sectionName):
            isOptionExist = optionName in self.dOptionsValues4Sections[sectionName]
        return isOptionExist
        
    def sections(self):    
        lSectionsKeys = self.dOptionsValues4Sections.keys()
        return list(lSectionsKeys)
    
    def options(self, sectionName):
        lOptionsKeys = [] 
        if self.has_section(sectionName):
            lOptionsKeys = self.dOptionsValues4Sections[sectionName].keys()
        return sorted(list(lOptionsKeys))
    
    def get(self, sectionName, optionName):   
        if self.has_option(sectionName, optionName):
            return self.dOptionsValues4Sections[sectionName][optionName]
        return None
    
    def set(self, sectionName, optionName, optionValue):   
        if not (self.has_section(sectionName)):
            self.dOptionsValues4Sections[sectionName] = {}
        self.dOptionsValues4Sections[sectionName][optionName] = optionValue
        
    def setdOptionsValues4Sections(self, dOptionsValues4Sections):
        self.dOptionsValues4Sections = dOptionsValues4Sections
        
    def __eq__(self, o):
        if type(o) is not type(self):
            return False
        else:
            return self.dOptionsValues4Sections == o.dOptionsValues4Sections
    
    def __ne__(self, o):
        return not self.__eq__(o)
