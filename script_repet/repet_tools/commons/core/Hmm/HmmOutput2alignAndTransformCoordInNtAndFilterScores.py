#!/usr/bin/env python

import os
import sys
import getopt
from commons.core.Hmm.HmmpfamOutput2align import HmmpfamOutput2align
from commons.core.Hmm.HmmscanOutput2align import HmmscanOutput2align
from commons.tools.TransformAACoordIntoNtCoordInAlignFormat import TransformAACoordIntoNtCoordInAlignFormat
from commons.core.utils.FileUtils import FileUtils

class HmmOutput2alignAndTransformCoordInNtAndFilterScores(object):

    def __init__(self, inFileName, outFileName, consensusFileName, verbose=0, program="hmmpfam", clean=True):
        self.inFileName = inFileName
        self.outFileName = outFileName
        self.consensusFileName = consensusFileName
        self.verbose = verbose # 0, 1, or 2
        self.program = program # either hmmpfam (default) or hmmscan
        self.clean = clean

    def check_args(self):
        if self.inFileName =="":
            raise Exception("Input file not specified, shutting down")
        if self.outFileName =="":
            if self.verbose > 1:
                print("output File Name not specified, defaulting the name to", self.inFileName,".align")
            self.outFileName = self.inFileName + ".align"

    def run(self):
        self.check_args()
        if self.program == "hmmpfam":
            hmmpfamOutput2align = HmmpfamOutput2align()
            hmmpfamOutput2align.setInputFile(self.inFileName)
            if self.consensusFileName == "":
                hmmpfamOutput2align.setOutputFile(self.outFileName)
            else:
                hmmpfamOutput2align.setOutputFile(self.outFileName + ".tmp" )
            hmmpfamOutput2align.run()
        elif self.program == "hmmscan":
            hmmscanOutput2align = HmmscanOutput2align()
            hmmscanOutput2align.setInputFile(self.inFileName)
            if self.consensusFileName == "":
                hmmscanOutput2align.setOutputFile(self.outFileName)
            else:
                hmmscanOutput2align.setOutputFile(self.outFileName + ".tmp" )
            hmmscanOutput2align.run()
        else:
            print ("\nWarning: You must specify a valid program (-p option). Only hmmpfam or hmmscan are supported !\n")

        if self.consensusFileName != "":
            alignTransformation = TransformAACoordIntoNtCoordInAlignFormat()
            alignTransformation.setInFileName(self.outFileName + ".tmp" )
            alignTransformation.setOutFileName(self.outFileName )
            alignTransformation.setConsensusFileName(self.consensusFileName )
            alignTransformation.setIsFiltered(True)
            alignTransformation.run()
            os.remove(self.outFileName + ".tmp")


        if self.clean == True:
            os.remove(self.inFileName)

        if self.verbose > 0:
            if FileUtils.isRessourceExists( self.outFileName ) and not(FileUtils.isEmpty( self.outFileName )):
                print ("{} finished successfully".format("HmmOutput2alignAndTransformCoordInNtAndFilterScores"))
            else:
                print ("warning {} execution failed".format(sys.argv[0].split("/")[-1]))

    #------------------------------------------------------------------------------

    if __name__ == '__main__':
        main()
