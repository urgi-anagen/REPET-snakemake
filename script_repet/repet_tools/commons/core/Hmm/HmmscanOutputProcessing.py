import re
#from commons.pyRepetUnit.hmmer.hmmOutput.HmmOutput import HmmOutput
#from commons.pyRepetUnit.hmmer.HmmOutputProcessing import HmmOutputProcessing
from commons.core.Hmm.HmmOutput import HmmOutput
from commons.core.Hmm.HmmOutputProcessing import HmmOutputProcessing 


##Concrete implementation for hmmscan output specific methods
#
class HmmscanOutputProcessing (HmmOutputProcessing):

    ## read an hmmscan output from a file, return a array with results useful to build a .align file
    #
    # @param file handle of file generated by software searching hmm profiles
    #
    def readHmmOutput( self, hmmerOutputFile ):
        #Tested with HMMER 3 on Linux
        line = hmmerOutputFile.readline()
        tabResult = None
        if (line == ""):
            tabResult = None
            return tabResult
        tabResult = HmmOutput()

        while line != "":
            line = hmmerOutputFile.readline()
            if not(re.match("^#.*$", line)) and line != "":
                lLines = re.split("\s+", line)
                seqName = lLines[3]
                profilName = lLines[0]
                iValue = lLines[12]
                score =  lLines[13]
                queryCoordStart =lLines[17]
                queryCoordEnd = lLines[18]
                subjectCoordStart = lLines[15]
                subjectCoordEnd = lLines[16]
                tabResult.append([seqName, queryCoordStart, queryCoordEnd, profilName, subjectCoordStart, subjectCoordEnd, iValue, score])
        return tabResult
