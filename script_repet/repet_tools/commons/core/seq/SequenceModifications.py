#!/usr/bin/env python

# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

from operator import itemgetter
from commons.core.coord.Range import Range
            
class SequenceModifications(object):
    
    def __init__(self, originalHeader = "", mutatedHeader = ""):
        self._originalHeader = originalHeader
        self._mutatedHeader = mutatedHeader
        self._lMutations = []
        self._lDeletionsRanges = []
        self._lInsertionsRanges = []
        
    def __str__(self):
        result = "{}\t{}\n" .format (self.getOriginalHeader(), self.getMutatedHeader())
        result += "Insertions\n"
        for insertion in self._lInsertionsRanges:
            result += "{}\n" .format (insertion.toString())
        result += "Deletions\n"
        for insertion in self._lDeletionsRanges:
            result += "{}\n" .format (insertion.toString())
        result += "Mutations"
        for mutation in self._lMutations:
            result += "\n{}\t{}\t{}" .format (mutation[0], mutation[1], mutation[2])
        return result
    
    def __eq__(self, o):
        if type(o) is type(self):
            self.sort()
            o.sort()
            return self._originalHeader == o._originalHeader and self._mutatedHeader == o._mutatedHeader and self._lMutations == o._lMutations \
                and self._lDeletionsRanges == o._lDeletionsRanges and self._lInsertionsRanges == o._lInsertionsRanges
        return False
            
    def __ne__(self, o):
        return not self.__eq__(o)
                
    def getOriginalHeader(self):
        return self._originalHeader
                
    def getMutatedHeader(self):
        return self._mutatedHeader
        
    def getMutations(self):
        self.sort()
        return self._lMutations
    
    def getInsertions(self):
        self.sort()
        return self._lInsertionsRanges
    
    def getDeletions(self):
        self.sort()
        return self._lDeletionsRanges
        
    def setOriginalHeader(self, originalHeader):
        self._originalHeader = originalHeader
        
    def setMutatedHeader(self, mutatedHeader):
        self._mutatedHeader = mutatedHeader
        
    def setMutations(self, lMutations):
        self._lMutations = lMutations
        
    def addMutation(self, tupleMute):
        #tuple: (position, oldNT, newNT)
        self._lMutations.append(tupleMute)
        
    def addInsertion(self, start, end, insertedSeqName = "."):
        self._lInsertionsRanges.append(Range(insertedSeqName, start, end))
        
    def addDeletion(self, start, end):
        self._lDeletionsRanges.append(Range(self.getOriginalHeader(), start, end))
        
    def clear(self):
        self._lMutations = []
        self._lDeletionsRanges = []
        self._lInsertionsRanges = []
        
    def sort(self):
        self._lMutations.sort(key = itemgetter(0), reverse = False)
        self._lDeletionsRanges.sort(key = lambda delRange: delRange.getStart(), reverse = False)
        self._lInsertionsRanges.sort(key = lambda insRange: insRange.getStart(), reverse = False)