import unittest
from commons.core.coord.Range import Range
from commons.core.seq.SequenceModifications import SequenceModifications

class Test_SequenceModifications(unittest.TestCase):

    def test_SequenceModifications_addMutation(self):
        iSeqModif = SequenceModifications("seq")
        iSeqModif.addMutation((10, 'A', 'T'))
        iSeqModif.addMutation((1, 'G', 'T'))
        
        obsMutationList = iSeqModif.getMutations()
        expMutationList = [(1, 'G', 'T'), (10, 'A', 'T')]
        
        self.assertEqual(obsMutationList, expMutationList)

    def test_SequenceModifications_addInsertion(self):
        iSeqModif = SequenceModifications("seq")
        iSeqModif.addInsertion(50, 150, "inserted_sequence_name")
        iSeqModif.addInsertion(20, 25)
        iSeqModif.addInsertion(80, 200, "inserted_sequence_name2")
        
        obsInsertionsList = iSeqModif.getInsertions()
        expInsertionsList = [Range(".", 20, 25), Range("inserted_sequence_name", 50, 150), Range("inserted_sequence_name2", 80, 200)]
        
        self.assertEqual(obsInsertionsList, expInsertionsList)

    def test_SequenceModifications_addDeletion(self):
        iSeqModif = SequenceModifications("seq")
        iSeqModif.addDeletion(50, 150)
        iSeqModif.addDeletion(20, 25)
        iSeqModif.addDeletion(80, 200)
        
        obsDeletionsList = iSeqModif.getDeletions()
        expDeletionsList = [Range("seq", 20, 25), Range("seq", 50, 150), Range("seq", 80, 200)]
        
        self.assertEqual(obsDeletionsList, expDeletionsList)

    def test_SequenceModifications_clear(self):
        iSeqModif = SequenceModifications("seq")
        iSeqModif.addDeletion(50, 150)
        iSeqModif.addInsertion(20, 25)
        iSeqModif.addDeletion(80, 200)
        iSeqModif.addMutation((10, 'A', 'T'))
        iSeqModif.addMutation((1, 'G', 'T'))
        
        iSeqModif.clear()
        
        self.assertEqual(iSeqModif, SequenceModifications("seq"))


if __name__ == "__main__":
    unittest.main()