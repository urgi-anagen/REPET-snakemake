import os
import unittest
from commons.core.utils.FileUtils import FileUtils
from commons.core.checker.RepetException import RepetException
from commons.core.seq.SequenceModifications import SequenceModifications
from commons.core.seq.SequenceModificationsCollection import SequenceModificationsCollection

class Test_SequenceModificationsCollection(unittest.TestCase):

    def test_SequenceModificationsCollection_add_and_get(self):
        iSeqModifCollection = SequenceModificationsCollection()
        iSeqModif1 = SequenceModifications("seq2", "seq2_mutated")
        iSeqModif1.addMutation((1, 'A', 'T'))
        iSeqModifCollection.add(iSeqModif1)
        
        iSeqModif2 = SequenceModifications("seq1")
        iSeqModif2.addMutation((12, 'C', 'G'))
        iSeqModifCollection.add(iSeqModif2 )
        
        iSeqModif3 = iSeqModifCollection.get("seq2")
        iSeqModif3.addMutation((10, 'T', 'A'))
        
        iSeqModif4 = iSeqModifCollection.get("seq2_mutated", mutated = True)
        iSeqModif4.addMutation((15, 'T', 'A'))
        
        expSeqModifCollection = SequenceModificationsCollection()
        expSeqModif1 = SequenceModifications("seq2", "seq2_mutated")
        expSeqModif1.addMutation((1, 'A', 'T'))
        expSeqModif1.addMutation((10, 'T', 'A'))
        expSeqModif1.addMutation((15, 'T', 'A'))
        
        expSeqModif2 = SequenceModifications("seq1")
        expSeqModif2.addMutation((12, 'C', 'G'))
        
        expSeqModifCollection.add(expSeqModif2)
        expSeqModifCollection.add(expSeqModif1)
        
        self.assertEqual(expSeqModifCollection, iSeqModifCollection)

    def test_SequenceModificationsCollection_add_and_get_not_found(self):
        iSeqModifCollection = SequenceModificationsCollection()
        iSeqModif = SequenceModifications("seq")
        iSeqModif.addMutation((1, 'A', 'T'))
        iSeqModifCollection.add(iSeqModif)
        
        obsSeqModif = iSeqModifCollection.get("titi")
        
        self.assertEqual(None, obsSeqModif)

    def test_SequenceModificationsCollection_add_already_in_collection(self):
        iSeqModifCollection = SequenceModificationsCollection()
        iSeqModif = SequenceModifications("seq")
        iSeqModif.addMutation((1, 'A', 'T'))
        iSeqModifCollection.add(iSeqModif)
        
        isExceptionRaised = False
        obsMsg = ""
        try:
            iSeqModifCollection.add(SequenceModifications("seq"))
        except RepetException as ex:
            isExceptionRaised = True
            obsMsg = ex.getMessage()
            
        self.assertTrue(isExceptionRaised)
        self.assertEqual("ERROR: 'seq' already in SequenceModificationsCollection", obsMsg)

    def test_SequenceModificationsCollection_add_already_in_collection_override(self):
        iSeqModifCollection = SequenceModificationsCollection()
        iSeqModif = SequenceModifications("seq")
        iSeqModif.addMutation((1, 'A', 'T'))
        iSeqModifCollection.add(iSeqModif)
        
        iSeqModifCollection.add(SequenceModifications("seq"), override = True)
        
        expSeqModifCollection = SequenceModificationsCollection()
        expSeqModifCollection.add(SequenceModifications("seq"))
        
        self.assertEqual(expSeqModifCollection, iSeqModifCollection)

    def test_SequenceModificationsCollection_writeMutations(self):
        iSeqModifCollection = SequenceModificationsCollection()
        iSeqModif1 = SequenceModifications("Seq2")
        iSeqModif1.addMutation((10, 'T', 'A'))
        iSeqModif1.addMutation((1, 'A', 'T'))
        
        iSeqModif2 = SequenceModifications("seq1")
        iSeqModif2.addMutation((12, 'C', 'G'))
        
        iSeqModif3 = SequenceModifications("Seq3")
        iSeqModif3.addMutation((125, 'T', 'G'))
        iSeqModif3.addMutation((25, 'G', 'T'))
        iSeqModif3.addMutation((8, 'A', 'T'))
        
        iSeqModifCollection.add(iSeqModif3)
        iSeqModifCollection.add(iSeqModif1)
        iSeqModifCollection.add(iSeqModif2)
        
        obsFile = "obsMutations.txt"
        iSeqModifCollection.writeMutations(obsFile)
        
        expFile = "expMutations.txt"
        with open(expFile, "w") as fH:
            fH.write("#Mutations:\n")
            fH.write("seqName\tposition\toldNt\tnewNt\n")
            fH.write("seq1\t12\tC\tG\n")
            fH.write("Seq2\t1\tA\tT\n")
            fH.write("Seq2\t10\tT\tA\n")
            fH.write("Seq3\t8\tA\tT\n")
            fH.write("Seq3\t25\tG\tT\n")
            fH.write("Seq3\t125\tT\tG\n")
        
        self.assertTrue(FileUtils.are2FilesIdentical(obsFile, expFile))
        
        os.remove(obsFile)
        os.remove(expFile)

    def test_SequenceModificationsCollection_writeInsertions(self):
        iSeqModifCollection = SequenceModificationsCollection()
        iSeqModif1 = SequenceModifications("Seq2")
        iSeqModif1.addInsertion(12, 50, "toto")
        iSeqModif1.addInsertion(1, 10, "titi")
        
        iSeqModif2 = SequenceModifications("seq1")
        iSeqModif2.addInsertion(100, 110, "toto")
        iSeqModif2.addInsertion(30, 92, "titi")
        
        iSeqModif3 = SequenceModifications("Seq3")
        iSeqModif3.addInsertion(1, 10, "toto")
        iSeqModif3.addInsertion(201, 208, "titi")
        iSeqModif3.addInsertion(78, 184)
        
        iSeqModifCollection.add(iSeqModif3)
        iSeqModifCollection.add(iSeqModif1)
        iSeqModifCollection.add(iSeqModif2)
        
        obsFile = "obsInsertions.txt"
        iSeqModifCollection.writeInsertions(obsFile)
        
        expFile = "expInsertions.txt"
        with open(expFile, "w") as fH:
            fH.write("#Insertions:\n")
            fH.write("seqName\tstart\tend\tinsertedSeqName\n")
            fH.write("seq1\t30\t92\ttiti\n")
            fH.write("seq1\t100\t110\ttoto\n")
            fH.write("Seq2\t1\t10\ttiti\n")
            fH.write("Seq2\t12\t50\ttoto\n")
            fH.write("Seq3\t1\t10\ttoto\n")
            fH.write("Seq3\t78\t184\t.\n")
            fH.write("Seq3\t201\t208\ttiti\n")
        
        self.assertTrue(FileUtils.are2FilesIdentical(obsFile, expFile))
        
        os.remove(obsFile)
        os.remove(expFile)
        
    def test_SequenceModificationsCollection_writeDeletions(self):
        iSeqModifCollection = SequenceModificationsCollection()
        iSeqModif1 = SequenceModifications("Seq2")
        iSeqModif1.addDeletion(12, 50)
        iSeqModif1.addDeletion(1, 10)
        
        iSeqModif2 = SequenceModifications("seq1")
        iSeqModif2.addDeletion(100, 110)
        iSeqModif2.addDeletion(30, 92)
        
        iSeqModif3 = SequenceModifications("Seq3")
        iSeqModif3.addDeletion(1, 10)
        iSeqModif3.addDeletion(201, 208)
        iSeqModif3.addDeletion(78, 184)
        
        iSeqModifCollection.add(iSeqModif3)
        iSeqModifCollection.add(iSeqModif1)
        iSeqModifCollection.add(iSeqModif2)
        
        obsFile = "obsDeletions.txt"
        iSeqModifCollection.writeDeletions(obsFile)
        
        expFile = "expDeletions.txt"
        with open(expFile, "w") as fH:
            fH.write("#Deletions:\n")
            fH.write("seqName\tstart\tend\n")
            fH.write("seq1\t30\t92\n")
            fH.write("seq1\t100\t110\n")
            fH.write("Seq2\t1\t10\n")
            fH.write("Seq2\t12\t50\n")
            fH.write("Seq3\t1\t10\n")
            fH.write("Seq3\t78\t184\n")
            fH.write("Seq3\t201\t208\n")
        
        self.assertTrue(FileUtils.are2FilesIdentical(obsFile, expFile))
        
        os.remove(obsFile)
        os.remove(expFile)
        
    def test_SequenceModificationsCollection_write(self):
        iSeqModifCollection = SequenceModificationsCollection()
        iSeqModif1 = SequenceModifications("Seq2")
        iSeqModif1.addInsertion(12, 50, "toto")
        iSeqModif1.addInsertion(1, 10, "titi")
        iSeqModif1.addDeletion(12, 50)
        iSeqModif1.addDeletion(1, 10)
        iSeqModif1.addMutation((10, 'T', 'A'))
        iSeqModif1.addMutation((1, 'A', 'T'))
        
        iSeqModif2 = SequenceModifications("seq1")
        iSeqModif2.addDeletion(100, 110)
        iSeqModif2.addDeletion(30, 92)
        iSeqModif2.addInsertion(100, 110, "toto")
        iSeqModif2.addInsertion(30, 92, "titi")
        iSeqModif2.addMutation((12, 'C', 'G'))
        
        iSeqModif3 = SequenceModifications("Seq3")
        iSeqModif3.addDeletion(1, 10)
        iSeqModif3.addDeletion(201, 208)
        iSeqModif3.addDeletion(78, 184)
        iSeqModif3.addInsertion(1, 10, "toto")
        iSeqModif3.addInsertion(201, 208, "titi")
        iSeqModif3.addInsertion(78, 184)
        iSeqModif3.addMutation((125, 'T', 'G'))
        iSeqModif3.addMutation((25, 'G', 'T'))
        iSeqModif3.addMutation((8, 'A', 'T'))
        
        iSeqModifCollection.add(iSeqModif3)
        iSeqModifCollection.add(iSeqModif1)
        iSeqModifCollection.add(iSeqModif2)
        
        obsDeletionsFile = "obsDeletions.txt"
        obsInsertionsFile = "obsInsertions.txt"
        obsMutationsFile = "obsMutations.txt"
        
        expDeletionsFile = "expDeletions.txt"
        expInsertionsFile = "expInsertions.txt"
        expMutationsFile = "expMutations.txt"

        with open(expDeletionsFile, "w") as fH:
            fH.write("#Deletions:\n")
            fH.write("seqName\tstart\tend\n")
            fH.write("seq1\t30\t92\n")
            fH.write("seq1\t100\t110\n")
            fH.write("Seq2\t1\t10\n")
            fH.write("Seq2\t12\t50\n")
            fH.write("Seq3\t1\t10\n")
            fH.write("Seq3\t78\t184\n")
            fH.write("Seq3\t201\t208\n")

        with open(expInsertionsFile, "w") as fH:
            fH.write("#Insertions:\n")
            fH.write("seqName\tstart\tend\tinsertedSeqName\n")
            fH.write("seq1\t30\t92\ttiti\n")
            fH.write("seq1\t100\t110\ttoto\n")
            fH.write("Seq2\t1\t10\ttiti\n")
            fH.write("Seq2\t12\t50\ttoto\n")
            fH.write("Seq3\t1\t10\ttoto\n")
            fH.write("Seq3\t78\t184\t.\n")
            fH.write("Seq3\t201\t208\ttiti\n")

        with open(expMutationsFile, "w") as fH:
            fH.write("#Mutations:\n")
            fH.write("seqName\tposition\toldNt\tnewNt\n")
            fH.write("seq1\t12\tC\tG\n")
            fH.write("Seq2\t1\tA\tT\n")
            fH.write("Seq2\t10\tT\tA\n")
            fH.write("Seq3\t8\tA\tT\n")
            fH.write("Seq3\t25\tG\tT\n")
            fH.write("Seq3\t125\tT\tG\n")

        iSeqModifCollection.write(mutationsFileName = obsMutationsFile, insertionsFileName = obsInsertionsFile, deletionsFileName = obsDeletionsFile)
            
        self.assertTrue(FileUtils.are2FilesIdentical(obsDeletionsFile, expDeletionsFile))
        self.assertTrue(FileUtils.are2FilesIdentical(obsInsertionsFile, expInsertionsFile))
        self.assertTrue(FileUtils.are2FilesIdentical(obsMutationsFile, expMutationsFile))
        
        os.remove(obsDeletionsFile)
        os.remove(obsInsertionsFile)
        os.remove(obsMutationsFile)
        os.remove(expDeletionsFile)
        os.remove(expInsertionsFile)
        os.remove(expMutationsFile)
        
    def test_SequenceModificationsCollection_write_gff(self):
        iSeqModifCollection = SequenceModificationsCollection()
        iSeqModif1 = SequenceModifications("Seq2")
        iSeqModif1.addInsertion(12, 50, "toto")
        iSeqModif1.addInsertion(1, 10, "titi")
        iSeqModif1.addDeletion(12, 50)
        iSeqModif1.addDeletion(1, 10)
        iSeqModif1.addMutation((10, 'T', 'A'))
        iSeqModif1.addMutation((1, 'A', 'T'))
        
        iSeqModif2 = SequenceModifications("seq1")
        iSeqModif2.addDeletion(100, 110)
        iSeqModif2.addDeletion(30, 92)
        iSeqModif2.addInsertion(100, 110, "toto")
        iSeqModif2.addInsertion(30, 92, "titi")
        iSeqModif2.addMutation((12, 'C', 'G'))
        
        iSeqModif3 = SequenceModifications("Seq3")
        iSeqModif3.addDeletion(1, 10)
        iSeqModif3.addDeletion(201, 208)
        iSeqModif3.addDeletion(78, 184)
        iSeqModif3.addInsertion(1, 10, "toto")
        iSeqModif3.addInsertion(201, 208, "titi")
        iSeqModif3.addInsertion(78, 184)
        iSeqModif3.addMutation((125, 'T', 'G'))
        iSeqModif3.addMutation((25, 'G', 'T'))
        iSeqModif3.addMutation((8, 'A', 'T'))
        
        iSeqModifCollection.add(iSeqModif3)
        iSeqModifCollection.add(iSeqModif1)
        iSeqModifCollection.add(iSeqModif2)
        
        obsDeletionsFile = "obsDeletions.gff"
        obsInsertionsFile = "obsInsertions.gff"
        obsMutationsFile = "obsMutations.gff"
        
        expDeletionsFile = "expDeletions.gff"
        expInsertionsFile = "expInsertions.gff"
        expMutationsFile = "expMutations.gff"
        
        with open(expDeletionsFile, "w") as fH:
            fH.write("##gff-version 3\n")
            fH.write("seq1\tMutateSequence\tdeletion\t30\t92\t.\t.\t.\tName=deletion_30-92\n")
            fH.write("seq1\tMutateSequence\tdeletion\t100\t110\t.\t.\t.\tName=deletion_100-110\n")
            fH.write("Seq2\tMutateSequence\tdeletion\t1\t10\t.\t.\t.\tName=deletion_1-10\n")
            fH.write("Seq2\tMutateSequence\tdeletion\t12\t50\t.\t.\t.\tName=deletion_12-50\n")
            fH.write("Seq3\tMutateSequence\tdeletion\t1\t10\t.\t.\t.\tName=deletion_1-10\n")
            fH.write("Seq3\tMutateSequence\tdeletion\t78\t184\t.\t.\t.\tName=deletion_78-184\n")
            fH.write("Seq3\tMutateSequence\tdeletion\t201\t208\t.\t.\t.\tName=deletion_201-208\n")

        with open(expInsertionsFile, "w") as fH:
            fH.write("##gff-version 3\n")
            fH.write("seq1\tMutateSequence\tinsertion\t30\t92\t.\t.\t.\tName=insertion_30-92;insert=titi\n")
            fH.write("seq1\tMutateSequence\tinsertion\t100\t110\t.\t.\t.\tName=insertion_100-110;insert=toto\n")
            fH.write("Seq2\tMutateSequence\tinsertion\t1\t10\t.\t.\t.\tName=insertion_1-10;insert=titi\n")
            fH.write("Seq2\tMutateSequence\tinsertion\t12\t50\t.\t.\t.\tName=insertion_12-50;insert=toto\n")
            fH.write("Seq3\tMutateSequence\tinsertion\t1\t10\t.\t.\t.\tName=insertion_1-10;insert=toto\n")
            fH.write("Seq3\tMutateSequence\tinsertion\t78\t184\t.\t.\t.\tName=insertion_78-184\n")
            fH.write("Seq3\tMutateSequence\tinsertion\t201\t208\t.\t.\t.\tName=insertion_201-208;insert=titi\n")
            
        with open(expMutationsFile, "w") as fH:
            fH.write("##gff-version 3\n")
            fH.write("seq1\tMutateSequence\tSNP\t12\t12\t.\t.\t.\tName=SNP_12;REF=C;ALT=G\n")
            fH.write("Seq2\tMutateSequence\tSNP\t1\t1\t.\t.\t.\tName=SNP_1;REF=A;ALT=T\n")
            fH.write("Seq2\tMutateSequence\tSNP\t10\t10\t.\t.\t.\tName=SNP_10;REF=T;ALT=A\n")
            fH.write("Seq3\tMutateSequence\tSNP\t8\t8\t.\t.\t.\tName=SNP_8;REF=A;ALT=T\n")
            fH.write("Seq3\tMutateSequence\tSNP\t25\t25\t.\t.\t.\tName=SNP_25;REF=G;ALT=T\n")
            fH.write("Seq3\tMutateSequence\tSNP\t125\t125\t.\t.\t.\tName=SNP_125;REF=T;ALT=G\n")

        iSeqModifCollection.write(mutationsFileName = obsMutationsFile, insertionsFileName = obsInsertionsFile, deletionsFileName = obsDeletionsFile, outFormat = "gff")
            
        self.assertTrue(FileUtils.are2FilesIdentical(obsDeletionsFile, expDeletionsFile))
        self.assertTrue(FileUtils.are2FilesIdentical(obsInsertionsFile, expInsertionsFile))
        self.assertTrue(FileUtils.are2FilesIdentical(obsMutationsFile, expMutationsFile))
        
        os.remove(obsDeletionsFile)
        os.remove(obsInsertionsFile)
        os.remove(obsMutationsFile)
        os.remove(expDeletionsFile)
        os.remove(expInsertionsFile)
        os.remove(expMutationsFile)

    def test_SequenceModificationsCollection_write_vcf(self):
        iSeqModifCollection = SequenceModificationsCollection()
        iSeqModif1 = SequenceModifications("Seq2")
        iSeqModif1.addInsertion(12, 50, "toto")
        iSeqModif1.addInsertion(1, 10, "titi")
        iSeqModif1.addDeletion(12, 50)
        iSeqModif1.addDeletion(1, 10)
        iSeqModif1.addMutation((10, 'T', 'A'))
        iSeqModif1.addMutation((1, 'A', 'T'))
        
        iSeqModif2 = SequenceModifications("seq1")
        iSeqModif2.addDeletion(100, 110)
        iSeqModif2.addDeletion(30, 92)
        iSeqModif2.addInsertion(100, 110, "toto")
        iSeqModif2.addInsertion(30, 92, "titi")
        iSeqModif2.addMutation((12, 'C', 'G'))
        
        iSeqModif3 = SequenceModifications("Seq3")
        iSeqModif3.addDeletion(1, 10)
        iSeqModif3.addDeletion(201, 208)
        iSeqModif3.addDeletion(78, 184)
        iSeqModif3.addInsertion(1, 10, "toto")
        iSeqModif3.addInsertion(201, 208, "titi")
        iSeqModif3.addInsertion(78, 184)
        iSeqModif3.addMutation((125, 'T', 'G'))
        iSeqModif3.addMutation((25, 'G', 'T'))
        iSeqModif3.addMutation((8, 'A', 'T'))
        
        iSeqModifCollection.add(iSeqModif3)
        iSeqModifCollection.add(iSeqModif1)
        iSeqModifCollection.add(iSeqModif2)
        
        obsVCFFile = "obsVCF.vcf"
        obsVCFFileTrimmed = "obsVCF_trimmed.vcf"
        expVCFFile = "expVCF.vcf"
        
        fastaFileName = "fasta.fa"
        
        with open(fastaFileName, "w") as fH:
            fH.write(">seq1\n")
            fH.write("GAATTCGCGTCCGCTTACCCATGTGCCTGTGGATGCCGAACAGGAGGCGCCGTTGACGGC\n")
            fH.write("GAATGACTTACTCAAGGGAGTAGCCAATCTGTCGGATACGCCCGGATTGGAGCTGCCCAT\n")
            fH.write("GGAGGGTTCTACAAGAAAGCGGTGGAGGATTGCTCGCATACTGCGAGACCGTTTCTGAAG\n")
            fH.write("GAGATGGCTCATGGAGTACCTGCCTACGCTTGTGCGCCGCGAGAAGTGGTGAAGAAGAAC\n")
            fH.write("GGAGCCCATACACCAGGGTGATATGGTCTTCGTCTGCGATCCCGCCTTGCCCCGGCGAGA\n")
            fH.write("GTGGTGCAAGGGCATCATGGAGGAAGTCTCCAGCAGAGCAGATGGAGCAACGGCCTATAG\n")
            fH.write("AGGACACTGATGCTACCCGTCTCTAAG\n")
            fH.write(">Seq2\n")
            fH.write("GAATTCGCGTCCGCTTACCCATGTGCCTGTGGATGCCGAACAGGAGGCGCCGTTGACGGC\n")
            fH.write("GAATGACTTACTCAAGGGAGTAGCCAATCTGTCGGATACGCCCGGATTGGAGCTGCCCAT\n")
            fH.write("GGAGGGTTCTACAAGAAAGCGGTGGAGGATTGCTCGCATACTGCGAGACCGTTTCTGAAG\n")
            fH.write("GAGATGGCTCATGGAGTACCTGCCTACGCTTGTGCGCCGCGAGAAGTGGTGAAGAAGAAC\n")
            fH.write("GGAGCCCATACACCAGGGTGATATGGTCTTCGTCTGCGATCCCGCCTTGCCCCGGCGAGA\n")
            fH.write("GTGGTGCAAGGGCATCATGGAGGAAGTCTCCAGCAGAGCAGATGGAGCAACGGCCTATAG\n")
            fH.write("AGGACACTGATGCTACCCGTCTCTAAG\n")
            fH.write(">Seq3\n")
            fH.write("GAATTCGCGTCCGCTTACCCATGTGCCTGTGGATGCCGAACAGGAGGCGCCGTTGACGGC\n")
            fH.write("GAATGACTTACTCAAGGGAGTAGCCAATCTGTCGGATACGCCCGGATTGGAGCTGCCCAT\n")
            fH.write("GGAGGGTTCTACAAGAAAGCGGTGGAGGATTGCTCGCATACTGCGAGACCGTTTCTGAAG\n")
            fH.write("GAGATGGCTCATGGAGTACCTGCCTACGCTTGTGCGCCGCGAGAAGTGGTGAAGAAGAAC\n")
            fH.write("GGAGCCCATACACCAGGGTGATATGGTCTTCGTCTGCGATCCCGCCTTGCCCCGGCGAGA\n")
            fH.write("GTGGTGCAAGGGCATCATGGAGGAAGTCTCCAGCAGAGCAGATGGAGCAACGGCCTATAG\n")
            fH.write("AGGACACTGATGCTACCCGTCTCTAAG\n")
        
        with open(expVCFFile, "w") as fH:
            fH.write("##fileformat=VCFv4.1\n")
            fH.write("##INFO=<ID=SVLEN,Number=.,Type=Integer,Description=\"Difference in length between REF and ALT alleles\">\n")
            fH.write("##INFO=<ID=SVTYPE,Number=1,Type=String,Description=\"Type of structural variant\">\n")
            fH.write("##INFO=<ID=ALTSTART,Number=1,Type=Integer,Description=\"ALT start position on query sequence\">\n")
            fH.write("##INFO=<ID=SOFTWARE,Number=1,Type=String,Description=\"Software used to generate this VCF\">\n")
            fH.write("##INFO=<ID=INSERTED,Number=1,Type=String,Description=\"Inserted sequence name\">\n")
            fH.write("#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\n")
            fH.write("seq1\t12\t.\tC\tG\t.\t.\tAN=2;REF=C;ALT=G;SOFTWARE=MutateSequences\n")
            fH.write("seq1\t29\t.\tG\t.\t.\t.\tSVTYPE=INS;AN=2;SVLEN=63;SOFTWARE=MutateSequences;INSERTED=titi\n")
            fH.write("seq1\t99\t.\tC\t.\t.\t.\tSVTYPE=INS;AN=2;SVLEN=11;SOFTWARE=MutateSequences;INSERTED=toto\n")
            fH.write("seq1\t29\t.\tGTGGATGCCGAACAGGAGGCGCCGTTGACGGCGAATGACTTACTCAAGGGAGTAGCCAATCTGT\tG\t.\t.\tSVTYPE=DEL;AN=2;SVLEN=-63;SOFTWARE=MutateSequences\n")
            fH.write("seq1\t99\t.\tCGCCCGGATTGG\tC\t.\t.\tSVTYPE=DEL;AN=2;SVLEN=-11;SOFTWARE=MutateSequences\n")
            fH.write("Seq2\t1\t.\tA\tT\t.\t.\tAN=2;REF=A;ALT=T;SOFTWARE=MutateSequences\n")
            fH.write("Seq2\t10\t.\tT\tA\t.\t.\tAN=2;REF=T;ALT=A;SOFTWARE=MutateSequences\n")
            fH.write("Seq2\t1\t.\t.\t.\t.\t.\tSVTYPE=INS;AN=2;SVLEN=10;SOFTWARE=MutateSequences;INSERTED=titi\n")
            fH.write("Seq2\t11\t.\tC\t.\t.\t.\tSVTYPE=INS;AN=2;SVLEN=39;SOFTWARE=MutateSequences;INSERTED=toto\n")
            fH.write("Seq2\t1\t.\tGAATTCGCGTC\t.\t.\t.\tSVTYPE=DEL;AN=2;SVLEN=-10;SOFTWARE=MutateSequences\n")
            fH.write("Seq2\t11\t.\tCCGCTTACCCATGTGCCTGTGGATGCCGAACAGGAGGCGC\tC\t.\t.\tSVTYPE=DEL;AN=2;SVLEN=-39;SOFTWARE=MutateSequences\n")
            fH.write("Seq3\t8\t.\tA\tT\t.\t.\tAN=2;REF=A;ALT=T;SOFTWARE=MutateSequences\n")
            fH.write("Seq3\t25\t.\tG\tT\t.\t.\tAN=2;REF=G;ALT=T;SOFTWARE=MutateSequences\n")
            fH.write("Seq3\t125\t.\tT\tG\t.\t.\tAN=2;REF=T;ALT=G;SOFTWARE=MutateSequences\n")
            fH.write("Seq3\t1\t.\t.\t.\t.\t.\tSVTYPE=INS;AN=2;SVLEN=10;SOFTWARE=MutateSequences;INSERTED=toto\n")
            fH.write("Seq3\t77\t.\tG\t.\t.\t.\tSVTYPE=INS;AN=2;SVLEN=107;SOFTWARE=MutateSequences\n")
            fH.write("Seq3\t200\t.\tC\t.\t.\t.\tSVTYPE=INS;AN=2;SVLEN=8;SOFTWARE=MutateSequences;INSERTED=titi\n")
            fH.write("Seq3\t1\t.\tGAATTCGCGTC\t.\t.\t.\tSVTYPE=DEL;AN=2;SVLEN=-10;SOFTWARE=MutateSequences\n")
            fH.write("Seq3\t77\t.\tGGAGTAGCCAATCTGTCGGATACGCCCGGATTGGAGCTGCCCATGGAGGGTTCTACAAGAAAGCGGTGGAGGATTGCTCGCATACTGCGAGACCGTTTCTGAAGGAGA\tG\t.\t.\tSVTYPE=DEL;AN=2;SVLEN=-107;SOFTWARE=MutateSequences\n")
            fH.write("Seq3\t200\t.\tCTGCCTACG\tC\t.\t.\tSVTYPE=DEL;AN=2;SVLEN=-8;SOFTWARE=MutateSequences\n")
            
        iSeqModifCollection.writeVCF(VCFFileName = obsVCFFile, fastaFileName = fastaFileName)
        
        os.system("grep -v '##fileDate=' %s | grep -v '##reference=' | grep -v '##assembly=' > %s" % (obsVCFFile, obsVCFFileTrimmed))
        self.assertTrue(FileUtils.are2FilesIdentical(obsVCFFileTrimmed, expVCFFile))
        
        os.remove(obsVCFFileTrimmed)
        os.remove(fastaFileName)
        os.remove(obsVCFFile)
        os.remove(expVCFFile)

        
    def test_SequenceModificationsCollection_write_empty(self):
        iSeqModifCollection = SequenceModificationsCollection()
        iSeqModif1 = SequenceModifications("Seq2")
        iSeqModif2 = SequenceModifications("seq1")
        iSeqModif3 = SequenceModifications("Seq3")
        
        iSeqModifCollection.add(iSeqModif3)
        iSeqModifCollection.add(iSeqModif1)
        iSeqModifCollection.add(iSeqModif2)
        
        obsDeletionsFile = "obsDeletions.txt"
        obsInsertionsFile = "obsInsertions.txt"
        obsMutationsFile = "obsMutations.txt"
        
        expDeletionsFile = "expDeletions.txt"
        expInsertionsFile = "expInsertions.txt"
        expMutationsFile = "expMutations.txt"

        with open(expDeletionsFile, "w") as fH:
            fH.write("#Deletions:\n")
            fH.write("seqName\tstart\tend\n")

        with open(expInsertionsFile, "w") as fH:
            fH.write("#Insertions:\n")
            fH.write("seqName\tstart\tend\tinsertedSeqName\n")

        with open(expMutationsFile, "w") as fH:
            fH.write("#Mutations:\n")
            fH.write("seqName\tposition\toldNt\tnewNt\n")

        iSeqModifCollection.write(mutationsFileName = obsMutationsFile, insertionsFileName = obsInsertionsFile, deletionsFileName = obsDeletionsFile)
            
        self.assertTrue(FileUtils.are2FilesIdentical(obsDeletionsFile, expDeletionsFile))
        self.assertTrue(FileUtils.are2FilesIdentical(obsInsertionsFile, expInsertionsFile))
        self.assertTrue(FileUtils.are2FilesIdentical(obsMutationsFile, expMutationsFile))
        
        os.remove(obsDeletionsFile)
        os.remove(obsInsertionsFile)
        os.remove(obsMutationsFile)
        os.remove(expDeletionsFile)
        os.remove(expInsertionsFile)
        os.remove(expMutationsFile)
        
    def test_SequenceModificationsCollection_getHeadersList_mutated_false(self):
        iSeqModifCollection = SequenceModificationsCollection()
        iSeqModif1 = SequenceModifications("Seq2")
        iSeqModif2 = SequenceModifications("seq1")
        iSeqModif3 = SequenceModifications("Seq3")
        
        iSeqModifCollection.add(iSeqModif3)
        iSeqModifCollection.add(iSeqModif1)
        iSeqModifCollection.add(iSeqModif2)
        
        expHeadersList = ["seq1", "Seq2", "Seq3"]
        obsHeaderList = iSeqModifCollection.getHeadersList()
        
        self.assertEqual(expHeadersList, obsHeaderList)
        
    def test_SequenceModificationsCollection_getHeadersList_mutated_true(self):
        iSeqModifCollection = SequenceModificationsCollection()
        iSeqModif1 = SequenceModifications("Seq2", "Seq2_mutated_0.200000")
        iSeqModif2 = SequenceModifications("seq1", "seq1_mutated_0.200000")
        iSeqModif3 = SequenceModifications("Seq3", "Seq3_mutated_0.200000")
        
        iSeqModifCollection.add(iSeqModif3)
        iSeqModifCollection.add(iSeqModif1)
        iSeqModifCollection.add(iSeqModif2)
        
        expHeadersList = ["seq1_mutated_0.200000", "Seq2_mutated_0.200000", "Seq3_mutated_0.200000"]
        obsHeaderList = iSeqModifCollection.getHeadersList(mutated = True)
        
        self.assertEqual(expHeadersList, obsHeaderList)
    
    def test_getCollectionBasedOnMutatedSequence(self):
        inModifCollection = SequenceModificationsCollection()
        iSeqModif1 = SequenceModifications("Seq2")
        iSeqModif1.setMutatedHeader("Seq2_mutated_0.020000")
        iSeqModif1.addInsertion(50, 100)
        iSeqModif1.addInsertion(110, 120)
        iSeqModif1.addDeletion(1, 10)
        iSeqModif1.addDeletion(105, 108)
        iSeqModif1.addMutation((10, "G", "T"))
        iSeqModif1.addMutation((150, "A", "G"))
        
        iSeqModif2 = SequenceModifications("seq1")
        iSeqModif2.setMutatedHeader("seq1_mutated_0.020000")
        iSeqModif2.addInsertion(50, 100)
        iSeqModif2.addInsertion(140, 170)
        iSeqModif2.addInsertion(190, 195)
        iSeqModif2.addDeletion(175, 180)
        iSeqModif2.addDeletion(200, 210)
        iSeqModif2.addMutation((22, "C", "G"))
        iSeqModif2.addMutation((272, "A", "T"))
        iSeqModif2.addMutation((222, "T", "A"))
        
        inModifCollection.add(iSeqModif1)
        inModifCollection.add(iSeqModif2)
        
        obsCollec = inModifCollection.getCollectionBasedOnMutatedSequence()
        
        exp = SequenceModificationsCollection()
        sm1 = SequenceModifications("Seq2_mutated_0.020000", "Seq2")
        sm1.addInsertion(1+0-0, 10+0-0)
        sm1.addDeletion(50+0-10, 100+0-10)
        sm1.addInsertion(105+51-10, 108+51-10)
        sm1.addDeletion(110+51-14, 120+51-14)
        sm1.addMutation((10, "T", "G"))
        sm1.addMutation((150, "G", "A"))
        
        sm2 = SequenceModifications("seq1_mutated_0.020000", "seq1")
        sm2.addDeletion(50+0-0, 100+0-0)
        sm2.addDeletion(140+51-0, 170+51-0)
        sm2.addInsertion(175+82-0, 180+82-0)
        sm2.addDeletion(190+82-6, 195+82-6)
        sm2.addInsertion(200+88-6, 210+88-6)
        sm2.addMutation((22, "G", "C"))
        sm2.addMutation((272, "T", "A"))
        sm2.addMutation((222, "A", "T"))
        
        exp.add(sm1)
        exp.add(sm2)
        
        self.assertEqual(obsCollec, exp)


if __name__ == "__main__":
    unittest.main()