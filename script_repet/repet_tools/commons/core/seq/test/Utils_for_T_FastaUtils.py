# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.


class Utils_for_T_FastaUtils( object ):
    
    def _createFastaFile_for_empty_file(fileName):
        with open(fileName, 'w') as f:
            f.write("")

        
    _createFastaFile_for_empty_file = staticmethod ( _createFastaFile_for_empty_file )
    
    
    def _createFastaFile_one_sequence(fileName):
        with open(fileName, 'w') as f:
            f.write(">seq 1\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")

        
    _createFastaFile_one_sequence = staticmethod ( _createFastaFile_one_sequence )

    
    def _createFastaFile_two_sequences_with_one_with_only_header( fileName ):
        with open(fileName, 'w') as f:
            f.write( ">seq 0\n" )
            f.write( ">seq 1\n" )
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
            f.write( ">seq 5\n" )

    _createFastaFile_two_sequences_with_one_with_only_header = staticmethod ( _createFastaFile_two_sequences_with_one_with_only_header )
    
    
    def createFastaFile_twoSequences( fileName ):
        with open(fileName, 'w') as f:
            f.write( ">seq 1\n" )
            f.write( "ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n" )
            f.write( "ATATTCG\n" )
            f.write( ">seq 2\n" )
            f.write( "ATATTCTTTCATCGATCGATCGGCGGCTATATGCTAGTGACGAAGCTAGTGTGAGTAGTA\n" )
            f.write( "ATATTCG\n" )
        
    createFastaFile_twoSequences = staticmethod ( createFastaFile_twoSequences )
    
    
    def createFastaFile_seq_1( fileName ):
        with open(fileName, 'w') as f:
            f.write( ">seq 1\n" )
            f.write( "ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n" )
            f.write( "ATATTCG\n" )
        
    createFastaFile_seq_1 = staticmethod( createFastaFile_seq_1 )
    
    
    def createFastaFile_seq_2( fileName ):
        with open(fileName, 'w') as f:
            f.write( ">seq 2\n" )
            f.write( "ATATTCTTTCATCGATCGATCGGCGGCTATATGCTAGTGACGAAGCTAGTGTGAGTAGTA\n" )
            f.write( "ATATTCG\n" )
        
    createFastaFile_seq_2 = staticmethod( createFastaFile_seq_2 )
    
    
    def _createFastaFile_sequence_without_header(fileName):
        with open(fileName, 'w') as f:
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
        
    _createFastaFile_sequence_without_header = staticmethod ( _createFastaFile_sequence_without_header )
    
        
    def _createFastaFile_four_sequences(fileName):
        with open(fileName, 'w') as f:
            f.write(">seq 1\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
            f.write(">seq 2\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
            f.write(">seq 3\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
            f.write(">seq 4\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")

    _createFastaFile_four_sequences = staticmethod ( _createFastaFile_four_sequences )
 
    def _createFastaFile_four_sequences_and_one_sequence_with_only_header(fileName):
        with open(fileName, 'w') as f:
            f.write(">seq 0\n")
            f.write(">seq 1\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
            f.write(">seq 2\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
            f.write(">seq 5\n")
            f.write(">seq 3\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
            f.write(">seq 4\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")

       
    _createFastaFile_four_sequences_and_one_sequence_with_only_header = staticmethod ( _createFastaFile_four_sequences_and_one_sequence_with_only_header )
    
    
    def _createFastaFile_three_sequences(fileName):
        with open(fileName, 'w') as f:
            f.write(">seq 1\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
            f.write(">seq 2\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
            f.write(">seq 4\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
        
    _createFastaFile_three_sequences = staticmethod ( _createFastaFile_three_sequences )
    
    
# ------------------ for dbSplit ---------------- #

    def _createBatch1_two_sequences(inFileName):
        with open(inFileName, 'w') as f:
            f.write(">seq 1\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
            f.write(">seq 2\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
        
    _createBatch1_two_sequences = staticmethod ( _createBatch1_two_sequences )
    
    
    def _createBatch2_two_sequences(inFileName):
        with open(inFileName, 'w') as f:
            f.write(">seq 3\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
            f.write(">seq 4\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
        
    _createBatch2_two_sequences = staticmethod ( _createBatch2_two_sequences )
    
    
    def _createBatch1_three_sequences(inFileName):
        with open(inFileName, 'w') as f:
            f.write(">seq 1\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
            f.write(">seq 2\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
            f.write(">seq 3\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
        
    _createBatch1_three_sequences = staticmethod ( _createBatch1_three_sequences )
    
    
    def _createBatch2_one_sequence(inFileName):
        with open(inFileName, 'w') as f:
            f.write(">seq 4\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
        
    _createBatch2_one_sequence = staticmethod ( _createBatch2_one_sequence )
    
    
    def _createFastaFile_ten_sequences(fileName):
        with open(fileName, 'w')as f:
            f.write(">seq 1\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write(">seq 2\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write(">seq 3\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write(">seq 4\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write(">seq 5\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write(">seq 6\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write(">seq 7\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write(">seq 8\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write(">seq 9\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write(">seq 10\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
        
    _createFastaFile_ten_sequences = staticmethod ( _createFastaFile_ten_sequences )
    
    
    def _createBatch_one_small_sequence(fileName, seqName):
        with open(fileName, 'w')as f:
            f.write(">{}\n".format(seqName))
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
        
    _createBatch_one_small_sequence = staticmethod ( _createBatch_one_small_sequence )
    
    
# ------------------ for dbChunks ------------------- #

    def _createFastaFile_big_sequence(fileName):
        with open(fileName, 'w') as f:
            f.write(">sequence\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGNNNNNNNNNNNNNTGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCANNNNNCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN\n")
            f.write("NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ANNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
            f.write(">sequence2\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCNNNNNNNNNNNNNNTGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATNNNNNNNNNNNNNNNNNNNGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
        
    _createFastaFile_big_sequence = staticmethod ( _createFastaFile_big_sequence )
    
    
    def _createFastaFile_of_Chunks(fileName):
        with open(fileName, 'w') as f:
            f.write('>chunk01\n')
            f.write('ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n')
            f.write('>chunk02\n')
            f.write('GTGAGTAGTAATATTCGCGCATCGATCG\n')
            f.write('>chunk03\n')
            f.write('TGCTAGTCAGCTAGCTAGTGTGAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATA\n')
            f.write('>chunk04\n')
            f.write('GGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTAATATTCGCGCATCGATCGATC\n')
            f.write('>chunk05\n')
            f.write('TCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTAATATTCGCGCA\n')
            f.write('>chunk06\n')
            f.write('TATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTAA\n')
            f.write('>chunk07\n')
            f.write('TGAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCANNNNNCTAGTG\n')
            f.write('>chunk08\n')
            f.write('NNNNCTAGTGTGAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAG\n')
            f.write('>chunk09\n')
            f.write('TGCTAGTCAGCTAGCTAGTGTGAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATA\n')
            f.write('>chunk10\n')
            f.write('GGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n')
            f.write('>chunk11\n')
            f.write('ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n')
            f.write('>chunk12\n')
            f.write('GTGAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGT\n')
            f.write('>chunk13\n')
            f.write('GCTAGCTAGTGTGAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCA\n')
            f.write('>chunk14\n')
            f.write('ATGCTAGTCAGCTAGCTAGTGTGAGTAGTAA\n')
            f.write('>chunk15\n')
            f.write('GAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGT\n')
            f.write('>chunk16\n')
            f.write('TAGCTAGTGTGAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGC\n')
            f.write('>chunk17\n')
            f.write('GCTAGTCAGCTAGCTAGTGTGAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATAT\n')
            f.write('>chunk18\n')
            f.write('GCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTAATATTCGCGCATCGATCGATCG\n')
            f.write('>chunk19\n')
            f.write('CGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTAATATTCGCGCAT\n')
            f.write('>chunk20\n')
            f.write('ATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTAAT\n')
            f.write('>chunk21\n')
            f.write('GAGTAGTAATATTCG\n')
            f.write('>chunk22\n')
            f.write('ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n')
            f.write('>chunk23\n')
            f.write('GTGAGTAGTAATATTCGCGCATCGATC\n')
            f.write('>chunk24\n')
            f.write('TGCTAGTCAGCTAGCTAGTGTGAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATA\n')
            f.write('>chunk25\n')
            f.write('GGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTAATATTCGCGCATCGATCGATC\n')
            f.write('>chunk26\n')
            f.write('TCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTAATATTCGCGCA\n')
            f.write('>chunk27\n')
            f.write('TATTCGCGCATCGATCGATCGGCGGC\n')
            f.write('>chunk28\n')
            f.write('AATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGT\n')
            f.write('>chunk29\n')
            f.write('GGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n')

    _createFastaFile_of_Chunks = staticmethod ( _createFastaFile_of_Chunks )

    def _createFastaFile_of_Chunks2(fileName):
        with open(fileName, 'w') as f:
            f.write('>chunk1\n')
            f.write('ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n')
            f.write('ATATTCGCGCATCGATCG\n')
            f.write('>chunk2\n')
            f.write('TGCTAGTCAGCTAGCTAGTGTGAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATA\n')
            f.write('TGCTAGTCAGCTAGCTAGTGTGAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATA\n')
            f.write('TGCTAGTCAGCTAGCTAGTGTGAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATA\n')
            f.write('TGCTAGTCAGCTAGCTAGTGTGAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATA\n')
            f.write('TGCTAGTCA\n')
            f.write('>chunk3\n')
            f.write('CTAGTGTGAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAG\n')
            f.write('CTAGTGTGAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAG\n')
            f.write('CTAGTGTGAGTAGTA\n')
            f.write('>chunk4\n')
            f.write('ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n')
            f.write('ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n')
            f.write('ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n')
            f.write('A\n')
            f.write('>chunk5\n')
            f.write('GAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGT\n')
            f.write('GAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGT\n')
            f.write('GAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGT\n')
            f.write('GAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGT\n')
            f.write('GAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGT\n')
            f.write('GAGTAGTAATATTCG\n')
            f.write('>chunk6\n')
            f.write('ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n')
            f.write('ATATTCGCGCATCGATC\n')
            f.write('>chunk7\n')
            f.write('TGCTAGTCAGCTAGCTAGTGTGAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATA\n')
            f.write('TGCTAGTCAGCTAGCTAGTGTGAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATA\n')
            f.write('TGCTAGTCAGCTAGCTAGTGTGAGTAGTAATATTCGCGCATCGATCGATCGGCGGC\n')
            f.write('>chunk8\n')
            f.write('AATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGT\n')
            f.write('AAT\n')
            f.write('>chunk9\n')
            f.write('GGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n')
        
    _createFastaFile_of_Chunks2 = staticmethod ( _createFastaFile_of_Chunks2 )

    
    
    def _createMapFile_of_Chunks(fileName):
        with open(fileName, 'w') as f:
            f.write("chunk01\tsequence\t1\t60\n")
            f.write("chunk02\tsequence\t51\t78\n")
            f.write("chunk03\tsequence\t92\t151\n")
            f.write("chunk04\tsequence\t142\t201\n")
            f.write("chunk05\tsequence\t192\t251\n")
            f.write("chunk06\tsequence\t242\t301\n")
            f.write("chunk07\tsequence\t292\t351\n")
            f.write("chunk08\tsequence\t342\t401\n")
            f.write("chunk09\tsequence\t392\t451\n")
            f.write("chunk10\tsequence\t442\t480\n")
            f.write("chunk11\tsequence\t601\t660\n")
            f.write("chunk12\tsequence\t651\t710\n")
            f.write("chunk13\tsequence\t701\t760\n")
            f.write("chunk14\tsequence\t751\t781\n")
            f.write("chunk15\tsequence\t833\t892\n")
            f.write("chunk16\tsequence\t883\t942\n")
            f.write("chunk17\tsequence\t933\t992\n")
            f.write("chunk18\tsequence\t983\t1042\n")
            f.write("chunk19\tsequence\t1033\t1092\n")
            f.write("chunk20\tsequence\t1083\t1142\n")
            f.write("chunk21\tsequence\t1133\t1147\n")
            f.write("chunk22\tsequence2\t1\t60\n")
            f.write("chunk23\tsequence2\t51\t77\n")
            f.write("chunk24\tsequence2\t92\t151\n")
            f.write("chunk25\tsequence2\t142\t201\n")
            f.write("chunk26\tsequence2\t192\t251\n")
            f.write("chunk27\tsequence2\t242\t267\n")
            f.write("chunk28\tsequence2\t300\t359\n")
            f.write("chunk29\tsequence2\t382\t420\n")
        
    _createMapFile_of_Chunks = staticmethod ( _createMapFile_of_Chunks )
    

    def _createMapFile_of_Chunks2(fileName):
        with open(fileName, 'w') as f:
            f.write("chunk1\tsequence\t1\t78\n")
            f.write("chunk2\tsequence\t92\t340\n")
            f.write("chunk3\tsequence\t346\t480\n")
            f.write("chunk4\tsequence\t601\t781\n")
            f.write("chunk5\tsequence\t833\t1147\n")
            f.write("chunk6\tsequence2\t1\t77\n")
            f.write("chunk7\tsequence2\t92\t267\n")
            f.write("chunk8\tsequence2\t300\t362\n")
            f.write("chunk9\tsequence2\t382\t420\n")
        f.close()
        
    _createMapFile_of_Chunks2 = staticmethod ( _createMapFile_of_Chunks2 )



    
    def _createFastaFile_of_cut(fileName):
        with open(fileName, 'w') as f:
            f.write(">1  sequence {Cut} 1..60\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write(">2  sequence {Cut} 51..78\n")
            f.write("GTGAGTAGTAATATTCGCGCATCGATCG\n")
            f.write(">3  sequence {Cut} 92..151\n")
            f.write("TGCTAGTCAGCTAGCTAGTGTGAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATA\n")
            f.write(">4  sequence {Cut} 142..201\n")
            f.write("GGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTAATATTCGCGCATCGATCGATC\n")
            f.write(">5  sequence {Cut} 192..251\n")
            f.write("TCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTAATATTCGCGCA\n")
            f.write(">6  sequence {Cut} 242..301\n")
            f.write("TATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTAA\n")
            f.write(">7  sequence {Cut} 292..351\n")
            f.write("TGAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCANNNNNCTAGTG\n")
            f.write(">8  sequence {Cut} 342..401\n")
            f.write("NNNNCTAGTGTGAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAG\n")
            f.write(">9  sequence {Cut} 392..451\n")
            f.write("TGCTAGTCAGCTAGCTAGTGTGAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATA\n")
            f.write(">10  sequence {Cut} 442..480\n")
            f.write("GGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write(">11  sequence {Cut} 601..660\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write(">12  sequence {Cut} 651..710\n")
            f.write("GTGAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGT\n")
            f.write(">13  sequence {Cut} 701..760\n")
            f.write("GCTAGCTAGTGTGAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCA\n")
            f.write(">14  sequence {Cut} 751..781\n")
            f.write("ATGCTAGTCAGCTAGCTAGTGTGAGTAGTAA\n")
            f.write(">15  sequence {Cut} 833..892\n")
            f.write("GAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGT\n")
            f.write(">16  sequence {Cut} 883..942\n")
            f.write("TAGCTAGTGTGAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGC\n")
            f.write(">17  sequence {Cut} 933..992\n")
            f.write("GCTAGTCAGCTAGCTAGTGTGAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATAT\n")
            f.write(">18  sequence {Cut} 983..1042\n")
            f.write("GCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTAATATTCGCGCATCGATCGATCG\n")
            f.write(">19  sequence {Cut} 1033..1092\n")
            f.write("CGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTAATATTCGCGCAT\n")
            f.write(">20  sequence {Cut} 1083..1142\n")
            f.write("ATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTAAT\n")
            f.write(">21  sequence {Cut} 1133..1147\n")
            f.write("GAGTAGTAATATTCG\n")
            f.write(">22  sequence2 {Cut} 1..60\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write(">23  sequence2 {Cut} 51..77\n")
            f.write("GTGAGTAGTAATATTCGCGCATCGATC\n")
            f.write(">24  sequence2 {Cut} 92..151\n")
            f.write("TGCTAGTCAGCTAGCTAGTGTGAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATA\n")
            f.write(">25  sequence2 {Cut} 142..201\n")
            f.write("GGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTAATATTCGCGCATCGATCGATC\n")
            f.write(">26  sequence2 {Cut} 192..251\n")
            f.write("TCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTAATATTCGCGCA\n")
            f.write(">27  sequence2 {Cut} 242..267\n")
            f.write("TATTCGCGCATCGATCGATCGGCGGC\n")
            f.write(">28  sequence2 {Cut} 300..359\n")
            f.write("AATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGT\n")
            f.write(">29  sequence2 {Cut} 382..420\n")
            f.write("GGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
        
    _createFastaFile_of_cut = staticmethod ( _createFastaFile_of_cut )
    

    def _createFastaFile_of_cut2(fileName):
        with open(fileName, 'w') as f:
            f.write(">1  sequence {Cut} 1..78\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCG\n")
            f.write(">2  sequence {Cut} 92..340\n")
            f.write('TGCTAGTCAGCTAGCTAGTGTGAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATA\n')
            f.write('TGCTAGTCAGCTAGCTAGTGTGAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATA\n')
            f.write('TGCTAGTCAGCTAGCTAGTGTGAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATA\n')
            f.write('TGCTAGTCAGCTAGCTAGTGTGAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATA\n')
            f.write('TGCTAGTCA\n')
            f.write(">3  sequence {Cut} 346..480\n")
            f.write('CTAGTGTGAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAG\n')
            f.write('CTAGTGTGAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAG\n')
            f.write('CTAGTGTGAGTAGTA\n')
            f.write(">4  sequence {Cut} 601..781\n")
            f.write('ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n')
            f.write('ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n')
            f.write('ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n')
            f.write('A\n')
            f.write(">5  sequence {Cut} 833..1147\n")
            f.write('GAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGT\n')
            f.write('GAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGT\n')
            f.write('GAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGT\n')
            f.write('GAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGT\n')
            f.write('GAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGT\n')
            f.write('GAGTAGTAATATTCG\n')
            f.write(">6  sequence2 {Cut} 1..77\n")
            f.write('ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n')
            f.write('ATATTCGCGCATCGATC\n')
            f.write(">7  sequence2 {Cut} 92..267\n")
            f.write('TGCTAGTCAGCTAGCTAGTGTGAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATA\n')
            f.write('TGCTAGTCAGCTAGCTAGTGTGAGTAGTAATATTCGCGCATCGATCGATCGGCGGCTATA\n')
            f.write('TGCTAGTCAGCTAGCTAGTGTGAGTAGTAATATTCGCGCATCGATCGATCGGCGGC\n')
            f.write(">8  sequence2 {Cut} 300..362\n")
            f.write('AATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGT\n')
            f.write('AAT\n')
            f.write(">9  sequence2 {Cut} 382..420\n")
            f.write('GGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n')
        
    _createFastaFile_of_cut2 = staticmethod ( _createFastaFile_of_cut2 )

    
    def _createFastaFile_of_Nstretch(fileName):
        with open(fileName, 'w') as f:
            f.write("N_stretch\tsequence\t79\t91\n")
            f.write("N_stretch\tsequence\t481\t600\n")
            f.write("N_stretch\tsequence\t782\t832\n")
            f.write("N_stretch\tsequence2\t78\t91\n")
            f.write("N_stretch\tsequence2\t268\t299\n")
            f.write("N_stretch\tsequence2\t363\t381\n")
        
    _createFastaFile_of_Nstretch = staticmethod ( _createFastaFile_of_Nstretch )


    def _createFastaFile_of_Nstretch2(fileName):
        with open(fileName, 'w') as f:
            f.write("N_stretch\tsequence\t79\t91\n")
            f.write("N_stretch\tsequence\t341\t345\n")
            f.write("N_stretch\tsequence\t481\t600\n")
            f.write("N_stretch\tsequence\t782\t832\n")
            f.write("N_stretch\tsequence2\t78\t91\n")
            f.write("N_stretch\tsequence2\t268\t299\n")
            f.write("N_stretch\tsequence2\t363\t381\n")
        
    _createFastaFile_of_Nstretch2 = staticmethod ( _createFastaFile_of_Nstretch2 )


    
    
# ------------------ for splitSeqPerCluster ------------------- #
    
    def _createFastaFile_of_four_sequences_with_specific_header(inFileName):
        with open(inFileName, 'w') as f:
            f.write(">ReconCluster1Mb155 chunk183 {Fragment} 1..5506\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
            f.write(">MbQ3Gr2Cl0 chunk440 {Fragment} 2678..3645\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
            f.write(">MbS2Gr2Cl0 chunk622 {Fragment} 104..1078\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
            f.write(">PilerCluster3.574Mb796 chunk0117 {Fragment} 51582..50819\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
        
    _createFastaFile_of_four_sequences_with_specific_header = staticmethod ( _createFastaFile_of_four_sequences_with_specific_header )
    
    def _createFastaFile_of_four_sequences_with_specific_header_forMCL(inFileName):
        with open(inFileName, 'w') as f:
            f.write(">MCLCluster1Mb4 chunk183 {Fragment} 1..5506\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
            f.write(">MCLCluster1Mb5 chunk045 {Fragment} 2678..3645\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
            f.write(">MCLCluster11Mb1 chunk156 {Fragment} 104..1078\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
            f.write(">MCLCluster63Mb1 chunk129 {Fragment} 51582..50819\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
        
    _createFastaFile_of_four_sequences_with_specific_header_forMCL = staticmethod ( _createFastaFile_of_four_sequences_with_specific_header_forMCL )
    
    def _createFastaFile_of_four_sequences_with_specific_header_shuffle(inFileName):
        with open(inFileName, 'w') as f:
            f.write(">MbQ3Gr2Cl0 chunk440 {Fragment} 2678..3645\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
            f.write(">ReconCluster1Mb155 chunk183 {Fragment} 1..5506\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
            f.write(">MbS2Gr2Cl0 chunk622 {Fragment} 104..1078\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
            f.write(">PilerCluster3.574Mb796 chunk0117 {Fragment} 51582..50819\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
        
    _createFastaFile_of_four_sequences_with_specific_header_shuffle = staticmethod ( _createFastaFile_of_four_sequences_with_specific_header_shuffle )
    
    
    def _createFastaFile_of_four_sequences_with_specific_header_in_same_cluster(inFileName):
        with open(inFileName, 'w') as f:
            f.write(">ReconCluster1Mb155 chunk1 {Fragment} 1..5506\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
            f.write(">ReconCluster1Mb155 chunk2 {Fragment} 1..5506\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
            f.write(">ReconCluster1Mb155 chunk3 {Fragment} 1..5506\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
            f.write(">ReconCluster1Mb155 chunk4 {Fragment} 1..5506\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")

    _createFastaFile_of_four_sequences_with_specific_header_in_same_cluster = staticmethod ( _createFastaFile_of_four_sequences_with_specific_header_in_same_cluster )
    
    
    def _createFastaFile_of_first_cluster_result(inFileName):
        with open(inFileName, 'w') as f:
            f.write(">ReconCluster1Mb155 chunk183 {Fragment} 1..5506\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
        
    _createFastaFile_of_first_cluster_result = staticmethod ( _createFastaFile_of_first_cluster_result )
    
    
    def _createFastaFile_of_second_cluster_result(inFileName):
        with open(inFileName, 'w') as f:
            f.write(">MbQ3Gr2Cl0 chunk440 {Fragment} 2678..3645\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
            f.write(">MbS2Gr2Cl0 chunk622 {Fragment} 104..1078\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
        
    _createFastaFile_of_second_cluster_result = staticmethod ( _createFastaFile_of_second_cluster_result )
    
    
    def _createFastaFile_of_third_cluster_result(inFileName): 
        with open(inFileName, 'w') as f:
            f.write(">PilerCluster3.574Mb796 chunk0117 {Fragment} 51582..50819\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
        
    _createFastaFile_of_third_cluster_result = staticmethod ( _createFastaFile_of_third_cluster_result )
    
    
    def _createFastaFile_of_first_cluster_result_with_simplify_header(inFileName):
        with open(inFileName, 'w') as f:
            f.write(">Piler_Cluster1_Seq155\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
        
    _createFastaFile_of_first_cluster_result_with_simplify_header = staticmethod ( _createFastaFile_of_first_cluster_result_with_simplify_header )
    
    
    def _createFastaFile_of_second_cluster_result_with_simplify_header(inFileName):
        with open(inFileName, 'w') as f:
            f.write(">Piler_Cluster2_Seq3\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
            f.write(">Piler_Cluster2_Seq2\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
        
    _createFastaFile_of_second_cluster_result_with_simplify_header = staticmethod ( _createFastaFile_of_second_cluster_result_with_simplify_header )
    
    
    def _createFastaFile_of_third_cluster_result_with_simplify_header(inFileName): 
        with open(inFileName, 'w') as f:
            f.write(">Piler_Cluster3.574_Seq796\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
        
    _createFastaFile_of_third_cluster_result_with_simplify_header = staticmethod ( _createFastaFile_of_third_cluster_result_with_simplify_header )
# -----------------------------------
    def _createFastaFile_of_first_cluster_result_with_simplify_MCLheader(inFileName):
        with open(inFileName, 'w') as f:
            f.write(">MCL_Cluster1_Seq4\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
            f.write(">MCL_Cluster1_Seq5\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
        
    _createFastaFile_of_first_cluster_result_with_simplify_MCLheader = staticmethod ( _createFastaFile_of_first_cluster_result_with_simplify_MCLheader )
    
    
    def _createFastaFile_of_second_cluster_result_with_simplify_MCLheader(inFileName):
        with open(inFileName, 'w') as f:
            f.write(">MCL_Cluster11_Seq1\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
        
    _createFastaFile_of_second_cluster_result_with_simplify_MCLheader = staticmethod ( _createFastaFile_of_second_cluster_result_with_simplify_MCLheader )
    
    
    def _createFastaFile_of_third_cluster_result_with_simplify_MCLheader(inFileName): 
        with open(inFileName, 'w') as f:
            f.write(">MCL_Cluster63_Seq1\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
        
    _createFastaFile_of_third_cluster_result_with_simplify_MCLheader = staticmethod ( _createFastaFile_of_third_cluster_result_with_simplify_MCLheader )
        

        
# ---------------------------------- #
    
    def _createPatternFile(fileName):
        with open(fileName, 'w') as f:
            f.write('seq 3\n')
            f.write('f\n')
            f.write('s.q 1\n')
            f.write('q 8\n')

        
    _createPatternFile = staticmethod ( _createPatternFile )
    
        
    def _createResult_of_dbLengthFilter_sup(inFileName): 
        with open(inFileName, 'w') as f:
            f.write(">seq 2\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
            f.write(">seq 3\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
            f.write(">seq 4\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
        
    _createResult_of_dbLengthFilter_sup = staticmethod ( _createResult_of_dbLengthFilter_sup )
    
        
    def _createFastaFile_four_sequences_for_header_filtering(fileName):
        with open(fileName, 'w') as f:
            f.write(">seq1_HostGene\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
            f.write(">seq2_SSR\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
            f.write(">seq3_Map2_NoCat\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
            f.write(">seq4_confused\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write("ATATTCG\n")
        
    _createFastaFile_four_sequences_for_header_filtering = staticmethod ( _createFastaFile_four_sequences_for_header_filtering )
    
   
    def _createFastaFile_three_sequences_with_ORFs(fileName):
        with open(fileName, 'w') as f:
            f.write(">Mivi_sl_Blaster_Grouper_1_Map_3\n")
            f.write("TACAGTCAAACCTGAGAAAATTTATATCGCCCAATCTTATACCTGGCAAAATTTATGGAC\n")
            f.write("TTTTCAATCTCCCGACTTTTTTGGCTGACAGAACCTGTCCAATCTTATACCTTATAATTT\n")
            f.write("ATGGGTTTACAGGTCAAATTATAAGATAAGAGAGAGCCGGAAATGAGCGATTGATCAGGA\n")
            f.write("ATTGGCCTCTCCTTCGGCGTTCTTGTTGACGAGCCGCCTGCTGGAACAGTCCGAAATGCC\n")
            f.write("CCGAAACAGCCGAGAAGCGAGCACAGTAGAGCAAAGGCGGCGGTTGACACGTTGACAGGC\n")
            f.write("TATTAGTCGCGACGTGCTTCTCTCTCCTCTTCCGCCGACAACGACGCCGAAATGAACATC\n")
            f.write("TTCACAATTCGAGAAGCCCTCATCAGAAATTCCTCTTCATATCATCATGGTCTCATTCCC\n")
            f.write("TTCCCGGAATTCAACTGCTGGCGCCACGCTCAAATCTTGTCTGATCGCAAGAACAAAGGA\n")
            f.write("TCGACCGACCATACACCTCTTCCCAACGGTCGCGGGCCTCTCGATGGCGATCGCGGCATC\n")
            f.write("ATCAACGCGATGAACGCTCTGAAGACGGCGAACGATGAGCTGGTCGAAAAGGGGATAGTT\n")
            f.write("ACGAGCAACGAGAAGATGAGTATCCACTTTCTCGTCGATGTGGAGGAGGAGAACCTTGCT\n")
            f.write("GGTGGGGCTGAAATGACCGATCAGGAGATTGTGGACCTGGTCATTGCCGAAGAGTTGGAG\n")
            f.write("GAGAATTGCGAGGGAGAAGGAGGAGAGGAGGAAGCGGAGATAGAGCTCCGACCGGCGGAG\n")
            f.write("CGAACGACGCTGAAGGAGGCAATCTCAGCGCTTGATGTTTTCCTTAGGTTTGCTATGAAT\n")
            f.write("GGGTACTCAGGTCCAACGGATTTGATTTCTTTTGAGGACGAGGCTAGGAGGATTCGCAGG\n")
            f.write("ACTCTTGTTGCCGAGCAGGATGCCGCGAGGATTCAGACGACAATGACTTCGTATTTCCAG\n")
            f.write("CGTCAGTAG\n")
            f.write(">Mivi_sl_Blaster_Grouper_2_Map_3\n")
            f.write("TACAGTCAGACCTGAGAAAATTTATATCGCCCAATCTTATACCTGTCAAAATTTATGGAC\n")
            f.write("TTTTCAATCTCCCGACTTTTTTGGCTGACAGAACCTGTCCAATCTTATACCTTATAATTT\n")
            f.write("ATGGGTTTACAGGTCAAATTATAAGATAAGAGAGAGCCGGAAATGAGCGATTGATCAGGA\n")
            f.write("ATTGGCCTCTCCTTCGGCGTTCTTGTTGACGAGCCGCCTGCTGGAAGAGTCCGAAATGCC\n")
            f.write("CCGAAACAGCCAAGAAGCGAGCACAGTAGAGCAAAGGCGGCGGTTGACACGTTGACAGGC\n")
            f.write("TAGTCGCGACGTGCTTCTCTCTCCTCTTCCGCCGACAACGACGCCGAAATGAACATCTTC\n")
            f.write("ACAATCCGAGAAGCCCTCATCAGAAATTCCTCTTCATATCATCATGGTCTCATTCCCTTC\n")
            f.write("CCGGAATTCAACTGCTGGCGCCACGCTCAAATCTTGTCTGATCGCAAGAACAAAGGATCG\n")
            f.write("ACCGACCATACACCTCTTCCCAACGGTCGCGGGCCTCTCGATGGCGATCGCGGCATCATC\n")
            f.write("AACGCGATGAACGCTCTGAAGACGGCGAACGATGAGCTGGTCGAAAAGGGGATAGTTACG\n")
            f.write("AGCAACGAGAAGATGAGTATCCACTTTCTCGTCGATGTGGAGGAGGAGAACCTTGCTGGT\n")
            f.write("GGGGCTGAAATGACCGATCAGGAGATTGTGGACCTGGGGTCATTGCCGAAGAGTTGGAGG\n")
            f.write("AGAATTGCGAGGGAGAACTTGAAGGAGGAGAGGAGGAAGCGGAGATAGAGCTCCGACCGG\n")
            f.write("CGGAGCGAACGACGCTGAAGGAGGCAATCTCAGCGCTTGGTGTTTTCCTTAGGTTTGCTA\n")
            f.write("TGAATGGGTACTCAGGTCCAACGAATTTGATTTCTTTTGAGGACGAGGCTAGGAGGATTC\n")
            f.write("GCAGGACTCTTGTTGCCGAGCAGGATGCCGCGAGGATTCAGACGACAATGACTTCGTATT\n")
            f.write("TCCAGCGTCAGTAGGTCAAGATATTTTTTCTTGGAGCTTTCATAACTTGTTAGATGTCAT\n")
            f.write("CATGCCTCAGTAGAGGCCGTACATTGGCTGCAATTCAGTAATTCGGGCCTTCTGAATTTC\n")
            f.write("CTCCCCCTCAAAAGTCAATGTATCGACTCAAACGATGCACGAAAATTCTTGATTTGATGT\n")
            f.write("AGGAAGCTCAAAAACGGAGTTTCAGGTCATAATGTAGTGATCTGAAGCCGAACTCTCCAA\n")
            f.write("GCTTATACCTGTCTTAATTTATGGGAAAATCCGTCCACCGAAAATATAAATTTTCTCAGG\n")
            f.write("TCTGACTGTA\n")
            f.write(">Mivi_sl_Blaster_Grouper_3_Map_3\n")
            f.write("CAACACAAAGATAAGGCACCTTCATCCAGTGGCTCGCACGAACTTTCAATTGCAAGTCGT\n")
            f.write("TGGCGTGCGCTGGGCGGGATCAAAACCGGGTCTGGGAGTCGCGTAACCCACTGGAGCAAG\n")
            f.write("TAGGTGGTAGCCTGGACTGACAACGTGGACGTCAGCGACTTGAAATAGGTATTCGCGGCT\n")
            f.write("TTCGTGGTCGGTGTACCCCTGTGTGTCCGGCATCGGCTGTCCAGAGTCGCCAGCCTGCCC\n")
            f.write("GGTGCGAATGTGGGGGGCAGTCTCGGTCGCGCACGTGCGATGTCCCTCTCCCCACACGGG\n")
            f.write("CCTGTCGGCTGCGGCTAACGGCCCCTGCAATGAGTCCTATCGACAGAAGTCCACCCAAGC\n")
            f.write("TCAGCCTTCCCTCCACCAACACCAACATCGCAAGCCTCTCAAAAATTCACATCTCTTACA\n")
            f.write("TCCCCTCGAAACACCCACTAAATGGATCCCTACCCTATCACTTGGCGGCAGTACTAGTAG\n")
            f.write("CACGGGCCCAACTTCGACGGATCTCGAACGGACTTCCCACGGGTTCTCGGTGTGCAAGCG\n")
            f.write("TCTCAGGTCAAGGTTTTGGTTGGTGGGCTTTCTGGGTGTGCAAAGGGGATTATTATAGAG\n")
            f.write("GGAAGAAGAAGAAGGATGGGACCAGGGAGGCAGAGACGTGCGGGGTCAGGTTGATGGGGG\n")
            f.write("AAGGTCGTGGAAGCGAAGAACGAGAGGCGAGCTTGTCGTTTGAGGAGAAAGGGGCCTCGC\n")
            f.write("TGCCTGCAGAGAAAGGCGGTAAGTTGACCGGCGCAAAGTTATCATGAGACCGTAGCTCAG\n")
            f.write("TATCGTTTCCTGTCGAGGGCATGGGTTACAGTCCGTCGAACTATGCGCGAGATCATCTGG\n")
            f.write("TAGTAAAGAATCCCGCCGCCACTGACCAGATCTCCTGTGTCCTCTCAGCTCTCACAAAAC\n")
            f.write("CTAGAAAAGCTTCGCATGGAGCCAAACGAATACTCCAATCCGACATTGAGCAAGACCCTT\n")
            f.write("CCCTCTCGCCTCATAGGAACGAAACGGCCCATCTTCGAAGGGACAACTCTCTGTCGGCCG\n")
            f.write("AGGAGATGACTTTTCTGACCGAGCGTAAGAGGAGATCGCACAGAGTCAGGGGTTGGTCGA\n")
            f.write("GCTTTTGGGATTGGAGGAAGGGGGAGAGATTGATGAACGAGACGTCCCAATAGTGAGTCG\n")
            f.write("CTCCGCACTGGAGCAATGATGGCAAACCGTGGAGTTGAAGAGTCTTGAACGCTATCAGAT\n")
            f.write("CGGGCTAGGAGGGTCGGGAGGCGGTTGAATGCGTGCCAACCTTGGCTTCCTGTCAACGAT\n")
            f.write("CCTCGCCTTTCAAGAGCTCGATCTATGGCAGTTGTTGACCTACACAGCGGGGGTATCCGG\n")
            f.write("GTCCTGTTGGGCTCTGGCGAGTTTATATACGCTACCGATCGCACGTCCGCTCACACCATC\n")
            f.write("CATCACTCAACGCCGCTGCGGTGATTGAGCACTTTATGTCCGTCTCAGGGGATCACCGTC\n")
            f.write("TGAGTACAAAATCGGTCAACCGAGTCAGGGAGGCACACGGAGGGTACCAATCGCTTTTCG\n")
            f.write("GACCTTTGATTTCAAAGTCTAAGAACAGTCAAAAGATCTGTAGTGAGTACAGACCACTGA\n")
            f.write("CCCTCACTTGAGTAGATATTGACACTGACGAAGATCGCTACACTGCGGGCCTTGACAGTG\n")
            f.write("ATTGATCTGTACTCGACACTGGTCTCATCGTACTTCTGGATGATGCCCAGAAAAGATGGG\n")
            f.write("AAGGATCAGGTTGATCCGTATGTACCCCTGCGTCGGCTCGTGAGGTCCAGAGAGGGTCAT\n")
            f.write("TCCACTGACTGTCCCGAACCCCCCCTGGCTGATCGTCAGCGCCTCGATGAAATGGTCGCG\n")
            f.write("CGCGTACGATAATGCGGGCCTGGCTCACGGATGCGCGCCTTTCCCTATCGTCAGTCACGC\n")
            f.write("AAATGTAGGCTTCCATCTGGAACGCTGCTTGATGGCCTAAGAATGGGCCGTCACGGAACA\n")
            f.write("GCTCACCGCCTGCAGACACGAACGGCCGTGGCGGTCATGGAAGGATCTGAACGTGTCGCC\n")
            f.write("CCATACGATTGACGAAGAGATGTAAGCTCCCTTGGTA\n")
    
    _createFastaFile_three_sequences_with_ORFs = staticmethod ( _createFastaFile_three_sequences_with_ORFs )
    
    
    def _createFastaFile_three_sequences_with_ORFs_expected(fileName):  
        with open(fileName, 'w') as f:
            f.write("ORF|1|662\tMivi_sl_Blaster_Grouper_1_Map_3\t307\t969\n")
            f.write("ORF|-3|254\tMivi_sl_Blaster_Grouper_1_Map_3\t793\t539\n")
            f.write("ORF|2|197\tMivi_sl_Blaster_Grouper_1_Map_3\t356\t553\n")
            f.write("ORF|3|176\tMivi_sl_Blaster_Grouper_1_Map_3\t288\t464\n")
            f.write("ORF|-1|176\tMivi_sl_Blaster_Grouper_1_Map_3\t786\t610\n")
            f.write("ORF|3|143\tMivi_sl_Blaster_Grouper_1_Map_3\t672\t815\n")
            f.write("ORF|1|131\tMivi_sl_Blaster_Grouper_1_Map_3\t175\t306\n")
            f.write("ORF|-2|131\tMivi_sl_Blaster_Grouper_1_Map_3\t797\t666\n")
            f.write("ORF|2|128\tMivi_sl_Blaster_Grouper_1_Map_3\t167\t295\n")
            f.write("ORF|-2|119\tMivi_sl_Blaster_Grouper_1_Map_3\t242\t123\n")
            f.write("ORF|1|464\tMivi_sl_Blaster_Grouper_2_Map_3\t304\t768\n")
            f.write("ORF|3|305\tMivi_sl_Blaster_Grouper_2_Map_3\t669\t974\n")
            f.write("ORF|-3|251\tMivi_sl_Blaster_Grouper_2_Map_3\t1094\t843\n")
            f.write("ORF|-2|245\tMivi_sl_Blaster_Grouper_2_Map_3\t531\t286\n")
            f.write("ORF|-3|224\tMivi_sl_Blaster_Grouper_2_Map_3\t791\t567\n")
            f.write("ORF|-2|215\tMivi_sl_Blaster_Grouper_2_Map_3\t1098\t883\n")
            f.write("ORF|2|197\tMivi_sl_Blaster_Grouper_2_Map_3\t353\t550\n")
            f.write("ORF|3|173\tMivi_sl_Blaster_Grouper_2_Map_3\t288\t461\n")
            f.write("ORF|-1|173\tMivi_sl_Blaster_Grouper_2_Map_3\t1087\t914\n")
            f.write("ORF|-1|143\tMivi_sl_Blaster_Grouper_2_Map_3\t310\t167\n")
            f.write("ORF|3|626\tMivi_sl_Blaster_Grouper_3_Map_3\t141\t767\n")
            f.write("ORF|2|434\tMivi_sl_Blaster_Grouper_3_Map_3\t164\t598\n")
            f.write("ORF|3|365\tMivi_sl_Blaster_Grouper_3_Map_3\t768\t1133\n")
            f.write("ORF|-3|359\tMivi_sl_Blaster_Grouper_3_Map_3\t1514\t1155\n")
            f.write("ORF|-1|320\tMivi_sl_Blaster_Grouper_3_Map_3\t1879\t1559\n")
            f.write("ORF|3|272\tMivi_sl_Blaster_Grouper_3_Map_3\t1299\t1571\n")
            f.write("ORF|-2|248\tMivi_sl_Blaster_Grouper_3_Map_3\t1503\t1255\n")
            f.write("ORF|1|236\tMivi_sl_Blaster_Grouper_3_Map_3\t1576\t1812\n")
            f.write("ORF|-1|227\tMivi_sl_Blaster_Grouper_3_Map_3\t1423\t1196\n")
            f.write("ORF|-3|227\tMivi_sl_Blaster_Grouper_3_Map_3\t368\t141\n")

    _createFastaFile_three_sequences_with_ORFs_expected = staticmethod ( _createFastaFile_three_sequences_with_ORFs_expected )
    
    
    def _createLinkFile_four_sequences_with_new_headers(fileName):
        with open(fileName, 'w') as f:
            f.write("seq 1\tReconCluster1Mb155 chunk183 {Fragment} 1..5506\t1\t127\n")
            f.write("seq 2\tMbQ3Gr2Cl0 chunk440 {Fragment} 2678..3645\t1\t307\n")
            f.write("seq 3\tMbS2Gr2Cl0 chunk622 {Fragment} 104..1078\t1\t427\n")
            f.write("seq 4\tPilerCluster3.574Mb796 chunk0117 {Fragment} 51582..50819\t1\t307\n")

    _createLinkFile_four_sequences_with_new_headers = staticmethod ( _createLinkFile_four_sequences_with_new_headers )
    
    
    def _createLinkFile_four_sequences_same_headers(fileName):
        with open(fileName, 'w') as f:
            f.write("seq 1\tseq 1\t1\t127\n")
            f.write("seq 2\tseq 2\t1\t307\n")
            f.write("seq 3\tseq 3\t1\t427\n")
            f.write("seq 4\tseq 4\t1\t307\n")

    _createLinkFile_four_sequences_same_headers = staticmethod ( _createLinkFile_four_sequences_same_headers )
    
    
    def _write_writeNstreches_2_input(fileName):
        with open(fileName, "w") as f:
            f.write(">seq2\n")
            f.write("NNNNxxNNnNTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTT\n")
            f.write("AGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTT\n")
            f.write("AGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTT\n")
            f.write("AGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTT\n")
            f.write("AGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTT\n")
            f.write("AGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTT\n")
            f.write("AGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGGTTAGGGTTAGGGTTAGGGTTAGGGT\n")
            f.write("TAGGGCTAGGGTTAGGGGTTAGGGTTAGGGTTAGGCTTAGGGTTAGGGTTAGGGTTAGGG\n")
            f.write("\n")
            f.write("TTAGGGTTAGGGTTAGGGTTAGGAGTTAGGGTGTAGGGTTAGGGTTAGGGTTAGGGTTAG\n")
            f.write("GGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAG\n")
            f.write("GGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGCTAGGGTTAGGGTTAG\n")
            f.write("GGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAG\n")
            f.write("\n")
            f.write(">seq1\n")
            f.write("AAGTTGGACATTGAGGGCTTTCTTCGCCGTGTTTCGTTCTTTTCGACAAACAGCAGTGCT\n")
            f.write("TTGCGGATCATxxxxxxxxxxxxxxxTTTGTTTGAACAACCGACAATGCGACCAATTTCA\n")
            f.write("GCGTAGGTTTTACCTTCAGAGATCACGTTTTTAATCAAATTTCTTTTTTCGACGGTACAA\n")
            f.write("TGCTTTCCGCGACCCATGACTAGAGAATTTTTGGTCTTCGTTTGGAAAAAATTCAATTAA\n")
            f.write("AACCTTTAATACAACTCCTTNNTTTTCAAAATTTTTCGAAAAAAACCCAAAGCAATCACT\n")
            f.write("CCTATTAATTTTATTCAGCAAATACGTGTTCAGTGCTATTTTTGTNTACCGCCTCATTTC\n")
            f.write("\n")
            f.write("GCGCACTTTTGCAGCAAGTGCCCAAAAACAAAAAGAACCGTTACATTGAGAGACTAAAAA\n")
            f.write("TTTCTTGCTCAGAGAGCCAACATATGGTACTTATTATTCATGCAATCTGACTTAAAAAAA\n")
            f.write("TATAAACATTTAATAATTTTTTTTAGGAANNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN\n")
            f.write("NNATCAACTTTCCACCTGCAGTAGTGCTATTATTTTAACCGCAGCTGTATAxx\n")
            f.write("\n")
            f.write("")
        
    _write_writeNstreches_2_input = staticmethod(_write_writeNstreches_2_input)
    
    
    def _write_writeNstreches_1_input(fileName):
        with open(fileName, "w") as f:
            f.write(">seq2\n")
            f.write("NNNNNNNNNNTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTT\n")
            f.write("AGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTT\n")
            f.write("AGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTT\n")
            f.write("AGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTT\n")
            f.write("AGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTT\n")
            f.write("AGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTT\n")
            f.write("AGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGGTTAGGGTTAGGGTTAGGGTTAGGGT\n")
            f.write("TAGGGCTAGGGTTAGGGGTTAGGGTTAGGGTTAGGCTTAGGGTTAGGGTTAGGGTTAGGG\n")
            f.write("TTAGGGTTAGGGTTAGGGTTAGGAGTTAGGGTGTAGGGTTAGGGTTAGGGTTAGGGTTAG\n")
            f.write("GGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAG\n")
            f.write("GGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGCTAGGGTTAGGGTTAG\n")
            f.write("GGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAG\n")
            f.write(">seq1\n")
            f.write("AAGTTGGACATTGAGGGCTTTCTTCGCCGTGTTTCGTTCTTTTCGACAAACAGCAGTGCT\n")
            f.write("TTGCGGATCATNNNNNNNNNNNNNNNTTTGTTTGAACAACCGACAATGCGACCAATTTCA\n")
            f.write("GCGTAGGTTTTACCTTCAGAGATCACGTTTTTAATCAAATTTCTTTTTTCGACGGTACAA\n")
            f.write("TGCTTTCCGCGACCCATGACTAGAGAATTTTTGGTCTTCGTTTGGAAAAAATTCAATTAA\n")
            f.write("AACCTTTAATACAACTCCTTNNTTTTCAAAATTTTTCGAAAAAAACCCAAAGCAATCACT\n")
            f.write("CCTATTAATTTTATTCAGCAAATACGTGTTCAGTGCTATTTTTGTNTACCGCCTCATTTC\n")
            f.write("GCGCACTTTTGCAGCAAGTGCCCAAAAACAAAAAGAACCGTTACATTGAGAGACTAAAAA\n")
            f.write("TTTCTTGCTCAGAGAGCCAACATATGGTACTTATTATTCATGCAATCTGACTTAAAAAAA\n")
            f.write("TATAAACATTTAATAATTTTTTTTAGGAANNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN\n")
            f.write("NNATCAACTTTCCACCTGCAGTAGTGCTATTATTTTAACCGCAGCTGTATAx\n")
        
    _write_writeNstreches_1_input = staticmethod(_write_writeNstreches_1_input)
    
    
    def _write_writeNstreches_0_input(fileName):
        with open(fileName, "w") as f:
            f.write(">seq2\n")
            f.write("NNNNNNNNNNTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTT\n")
            f.write("AGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTT\n")
            f.write("AGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTT\n")
            f.write("AGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTT\n")
            f.write("AGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTT\n")
            f.write("AGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTT\n")
            f.write("AGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGGTTAGGGTTAGGGTTAGGGTTAGGGT\n")
            f.write("TAGGGCTAGGGTTAGGGGTTAGGGTTAGGGTTAGGCTTAGGGTTAGGGTTAGGGTTAGGG\n")
            f.write("TTAGGGTTAGGGTTAGGGTTAGGAGTTAGGGTGTAGGGTTAGGGTTAGGGTTAGGGTTAG\n")
            f.write("GGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAG\n")
            f.write("GGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGCTAGGGTTAGGGTTAG\n")
            f.write("GGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAG\n")
            f.write(">seq1\n")
            f.write("AAGTTGGACATTGAGGGCTTTCTTCGCCGTGTTTCGTTCTTTTCGACAAACAGCAGTGCT\n")
            f.write("TTGCGGATCATNNNNNNNNNNNNNNNTTTGTTTGAACAACCGACAATGCGACCAATTTCA\n")
            f.write("GCGTAGGTTTTACCTTCAGAGATCACGTTTTTAATCAAATTTCTTTTTTCGACGGTACAA\n")
            f.write("TGCTTTCCGCGACCCATGACTAGAGAATTTTTGGTCTTCGTTTGGAAAAAATTCAATTAA\n")
            f.write("AACCTTTAATACAACTCCTTNNTTTTCAAAATTTTTCGAAAAAAACCCAAAGCAATCACT\n")
            f.write("CCTATTAATTTTATTCAGCAAATACGTGTTCAGTGCTATTTTTGTNTACCGCCTCATTTC\n")
            f.write("GCGCACTTTTGCAGCAAGTGCCCAAAAACAAAAAGAACCGTTACATTGAGAGACTAAAAA\n")
            f.write("TTTCTTGCTCAGAGAGCCAACATATGGTACTTATTATTCATGCAATCTGACTTAAAAAAA\n")
            f.write("TATAAACATTTAATAATTTTTTTTAGGAANNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN\n")
            f.write("NNATCAACTTTCCACCTGCAGTAGTGCTATTATTTTAACCGCAGCTGTATANN\n")
        
    _write_writeNstreches_0_input = staticmethod(_write_writeNstreches_0_input)
    
    
    def _write_writeNstreches_2_gff_input(fileName):
        with open(fileName, "w") as f:
            f.write(">seq2\n")
            f.write("NNNNxxNNnNTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTT\n")
            f.write("AGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTT\n")
            f.write("AGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTT\n")
            f.write("AGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTT\n")
            f.write("AGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTT\n")
            f.write("AGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTT\n")
            f.write("AGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGGTTAGGGTTAGGGTTAGGGTTAGGGT\n")
            f.write("TAGGGCTAGGGTTAGGGGTTAGGGTTAGGGTTAGGCTTAGGGTTAGGGTTAGGGTTAGGG\n")
            f.write("\n")
            f.write("TTAGGGTTAGGGTTAGGGTTAGGAGTTAGGGTGTAGGGTTAGGGTTAGGGTTAGGGTTAG\n")
            f.write("GGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAG\n")
            f.write("GGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGCTAGGGTTAGGGTTAG\n")
            f.write("GGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAG\n")
            f.write("\n")
            f.write(">seq1\n")
            f.write("AAGTTGGACATTGAGGGCTTTCTTCGCCGTGTTTCGTTCTTTTCGACAAACAGCAGTGCT\n")
            f.write("TTGCGGATCATxxxxxxxxxxxxxxxTTTGTTTGAACAACCGACAATGCGACCAATTTCA\n")
            f.write("GCGTAGGTTTTACCTTCAGAGATCACGTTTTTAATCAAATTTCTTTTTTCGACGGTACAA\n")
            f.write("TGCTTTCCGCGACCCATGACTAGAGAATTTTTGGTCTTCGTTTGGAAAAAATTCAATTAA\n")
            f.write("AACCTTTAATACAACTCCTTNNTTTTCAAAATTTTTCGAAAAAAACCCAAAGCAATCACT\n")
            f.write("CCTATTAATTTTATTCAGCAAATACGTGTTCAGTGCTATTTTTGTNTACCGCCTCATTTC\n")
            f.write("\n")
            f.write("GCGCACTTTTGCAGCAAGTGCCCAAAAACAAAAAGAACCGTTACATTGAGAGACTAAAAA\n")
            f.write("TTTCTTGCTCAGAGAGCCAACATATGGTACTTATTATTCATGCAATCTGACTTAAAAAAA\n")
            f.write("TATAAACATTTAATAATTTTTTTTAGGAANNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN\n")
            f.write("NNATCAACTTTCCACCTGCAGTAGTGCTATTATTTTAACCGCAGCTGTATAxx\n")
            f.write("\n")
            f.write("")
        
    _write_writeNstreches_2_gff_input = staticmethod(_write_writeNstreches_2_gff_input)
