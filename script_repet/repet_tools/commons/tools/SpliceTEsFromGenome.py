#!/usr/bin/env python
from __future__ import print_function
import os
import sys
import getopt

#from commons.core.sql.DbFactory import DbFactory
from commons.core.seq.FastaUtils import FastaUtils
from commons.core.coord.MapUtils import MapUtils
from commons.core.coord.AlignUtils import AlignUtils
from commons.core.coord.PathUtils import PathUtils

class SpliceTEsFromGenome( object ):
    
    def __init__( self, inputData = "",formatData = "",genomeFile="",outFile="",verbose=0,db=None  ):
        self._inputData = inputData
        self._formatData = formatData
        self._genomeFile = genomeFile
        self._outFile = outFile
        self._verbose = verbose
        #self._db = db
        
        
    def setAttributesFromCmdLine( self ):
        epilog = ""
        description = "Tool to splice sequences from genome\n"
        usage = "usage: SpliceTEsFromGenome.py [ options ]"

        from commons.core.utils.RepetOptionParser import RepetOptionParser
        parser = RepetOptionParser(description=description, epilog=epilog, usage=usage)
        parser.add_option("-i", "--inputData", dest="InputFileName", action="store", type="string", help="input TE coordinates (can be file or table) TEs as subjects if align or path format", default="")
        parser.add_option("-f", "--formatData", dest="formatData", action="store", type="string", help="format of the data (map/align/path)", default="")
        parser.add_option("-g", "--genome", dest="genomeFile", action="store", type="string", help="genome file (format=fasta)", default="")
        #parser.add_option("-C", "--configurationFile", dest = "configurationFile", action = "store", type = "string", help = "configuration file (if table as input)", default = "")
        parser.add_option("-o", "--outputFastaFile", dest="outputFastaFile", action="store", type="string", help="output fasta file (default=genomeFile+'.splice')", default="genomeFile+'.splice'")
        parser.add_option("-v", "--verbosity", dest="verbosity", action="store", type="int", help="verbosity [optional] [default: 3]", default=3)
        options = parser.parse_args()[0]
        self._setAttributesFromOptions(options)

    def _setAttributesFromOptions(self, options):
        self.setInputData(options.InputFileName)
        self.setFormatData(options.formatData)
        self.setGenomeFile(options.genomeFile)
        #self.setConfigFile(options.configurationFile)
        self.setOutFile(options.outputFastaFile)
        self.setVerbosity(options.verbosity)

    def setInputData(self, InputFileName):
        self._inputData = InputFileName

    def setFormatData(self, formatData):
        self._formatData = formatData

    def setGenomeFile(self, genomeFile):
        self._genomeFile = genomeFile

    # def setConfigFile(self, configurationFile):
    #     self._configFile = configurationFile

    def setOutFile(self, outFile):
        self._outFile = outFile

    def setVerbosity(self, verbosity):
        self._verbose = verbosity
                
                
    def checkAttributes( self ):
        if self._inputData == "":
            msg = "ERROR: missing input data (-i)"
            sys.stderr.write("{}\n".format(msg))
            self.help()
            sys.exit(1)
        # if not os.path.exists(self._inputData):
        #     if not os.path.exists(self._configFile):
        #         msg = "ERROR: neither input file '{}' nor configuration file '{}'".format(self._inputData, self._configFile)
        #         sys.stderr.write("{}\n".format(msg))
        #         self.help()
        #         sys.exit(1)
        #     if not os.path.exists(self._configFile):
        #         msg = "ERROR: can't find config file '{}'".format(self._configFile)
        #         sys.stderr.write("{}\n".format(msg))
        #         sys.exit(1)
        #     self._db = DbFactory.createInstance(configFileName = self._configFile)
        #     if not self._db.doesTableExist(self._inputData):
        #         msg = "ERROR: can't find table '{}'".format(self._inputData)
        #         sys.stderr.write("{}\n".format(msg))
        #         self.help()
        #         sys.exit(1)
        if self._formatData == "":
            msg = "ERROR: need to precise format (-f)"
            sys.stderr.write("{}\n".format(msg))
            self.help()
            sys.exit(1)
        if self._formatData not in ["map", "align", "path"]:
            msg = "ERROR: format '{}' not yet supported".format(self._formatData)
            sys.stderr.write("{}\n".format(msg))
            self.help()
            sys.exit(1)
        if self._genomeFile == "":
            msg = "ERROR: missing genome file (-g)"
            sys.stderr.write("{}\n".format(msg))
            self.help()
            sys.exit(1)
        if not os.path.exists(self._genomeFile):
            msg = "ERROR: can't find genome file '{}'".format(self._genomeFile)
            sys.stderr.write("{}\n".format(msg))
            self.help()
            sys.exit(1)
        if self._outFile == "":
            self._outFile = "{}.splice".format(self._genomeFile)
            if self._verbose > 0:
                print("output fasta file: {}".format(self._outFile))
                
                
    def getCoordsAsMapFile(self):
        if self._verbose > 0:
            print("get TE coordinates as 'Map' file")
            sys.stdout.flush()
        # if self._db:
        #     self._db.exportDataToFile(self._inputData,  "{}.{}".format(self._inputData, self._formatData))
        #     self._inputData += ".{}".format(self._formatData)
           
        if self._formatData == "map":
            return self._inputData
        elif self._formatData == "align":
            mapFile = "{}.map".format(self._inputData)
            AlignUtils.convertAlignFileIntoMapFileWithSubjectsOnQueries(self._inputData, mapFile)
            return mapFile
        elif self._formatData == "path":
            mapFile = "{}.map".format(self._inputData)
            PathUtils.convertPathFileIntoMapFileWithSubjectsOnQueries(self._inputData, mapFile)
            return mapFile
        
        
    def mergeCoordsInMapFile(self, mapFile):
        if self._verbose > 0:
            print("merge TE coordinates")
            sys.stdout.flush()
        mergeFile = "{}.merge".format(mapFile)
        MapUtils.mergeCoordsInFile(mapFile, mergeFile)
        if self._formatData != "map" : #or self._db is not None:
            os.remove(mapFile)
        return mergeFile
    
    
    def spliceFastaFromCoords(self, mergeFile):
        if self._verbose > 0:
            print("splice TE copies from the genome")
            sys.stdout.flush()
        FastaUtils.spliceFromCoords(self._genomeFile, mergeFile, self._outFile)
    
        os.remove(mergeFile)
        
        
    def start(self):
        self.checkAttributes()
        if self._verbose > 0:
            print("START SpliceTEsFromGenome.py")
            sys.stdout.flush()
            
            
    def end(self):
        # if self._db:
        #     self._db.close()
        if self._verbose > 0:
            print("END SpliceTEsFromGenome.py")
            sys.stdout.flush()
            
            
    def run(self):
        self.start()
        
        mapFile = self.getCoordsAsMapFile()

        mergeFile = self.mergeCoordsInMapFile(mapFile)
        
        self.spliceFastaFromCoords(mergeFile)
        
        self.end()
        
        
if __name__ == "__main__":
    iSTEFG = SpliceTEsFromGenome()
    iSTEFG.setAttributesFromCmdLine()
    iSTEFG.run()