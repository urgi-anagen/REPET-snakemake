#!/usr/bin/env python

# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.


import sys
import argparse
from commons.core.seq.AlignedBioseqDB import AlignedBioseqDB


def dbConsensus(inFileName="", minNbNt=1, minPropNt=0.0, outFileName="", header_SATannot=False, verbose=0):

    if verbose > 0:
        print("START dbConsensus.py")
        sys.stdout.flush()
    alnDB = AlignedBioseqDB(inFileName)

    if alnDB.getSize() < minNbNt:
        print("WARNING: not enough sequences (<{})".format(int(minNbNt)))

    else:
        consensus = alnDB.getConsensus(minNbNt, minPropNt, verbose, header_SATannot)
        if consensus is not None:
            consensus.upCase()
            if outFileName == "":
                outFileName = "{}.cons".format(inFileName)
            with open(outFileName, "w") as outFile:
                consensus.write(outFile)

    if verbose > 0:
        print("END dbConsensus.py")
        sys.stdout.flush()

    return 0


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='usage: dbConsensus.py [ options ]\n',
        formatter_class=argparse.RawTextHelpFormatter, add_help=True)
    parser.add_argument('-i', '--input', required=True, help="name of the input file (format=aligned fasta)")
    parser.add_argument('-n', type=int, default=1,
                        help="minimum number of nucleotides in a column to edit a consensus (default=1)")
    parser.add_argument('-p', type=float, default=0.0,
                        help="minimum proportion for the major nucleotide to be used, otherwise add 'N' (default=0.0)")
    parser.add_argument('-o', '--output', default="",help="name of the output file (default=inFileName+'.cons')")
    parser.add_argument('-H', action='store_true',
                        help="format the header with pyramid and piles informations (SATannot)")
    parser.add_argument('-v', '--verbose', default=0, type=int, help="verbose (default=0/1/2)")

    args = parser.parse_args()
    inFileName = args.input
    minNbNt = args.n
    minPropNt = args.p
    outFileName = args.output
    header_SATannot = args.H
    verbose = args.verbose

    dbConsensus(inFileName, minNbNt, minPropNt, outFileName, header_SATannot, verbose)

