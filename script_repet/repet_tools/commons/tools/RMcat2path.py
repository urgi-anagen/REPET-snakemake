#!/usr/bin/env python

# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import os
from commons.core.LoggerFactory import LoggerFactory
from commons.core.utils.RepetOptionParser import RepetOptionParser

LOG_DEPTH = "repet.annot_pipe"


class RMcat2path(object):

    def __init__(self, inFileName="", outFileName="", verbosity=1, log_name="RMcat2path.log"):
        self._inCatFileName = inFileName
        self._outPathFileName = outFileName
        self._verbosity = verbosity
        self._log = LoggerFactory.createLogger("{}.{}".format((LOG_DEPTH, self.__class__.__name__), self._verbosity),
                                               log_name)

    def setAttributesFromCmdLine(self):
        description = "Convert the output file from RepeatMasker ('cat' format) into the 'path' format."
        epilog = "\n"
        parser = RepetOptionParser(description=description, epilog=epilog)
        parser.add_option("-i", "--inFileName", dest="inFileName", action="store", type="string",
                          help="input file name in cat format [compulsory] ", default="")
        parser.add_option("-o", "--outFileName", dest="outFileName", action="store", type="string",
                          help="Out file name in path format [optional] [default: inFileName.path]", default="")
        parser.add_option("-v", "--verbosity", dest="verbosity", action="store", type="int",
                          help="verbosity [optional] [default: 1]", default=1)
        options = parser.parse_args()[0]
        self._setAttributesFromOptions(options)

    def _setAttributesFromOptions(self, options):
        self._inCatFileName = options.inFileName
        self._outPathFileName = options.outFileName
        self._verbosity = options.verbosity

    def setInCatFileName(self, fileName):
        self._inCatFileName = fileName

    def setOutPathFileName(self, fileName):
        self._outPathFileName = fileName

    def setVerbosity(self, verbosity):
        self._verbosity = verbosity

    def _checkOptions(self):
        if self._inCatFileName == "":
            self._logAndRaise("ERROR: please provide the input file name")
        else:
            if not os.path.exists(self._inCatFileName):
                self._logAndRaise("ERROR: can't find input file '{}'".format(self._inCatFileName))

        if self._outPathFileName == "":
            self._outPathFileName = "{}.path".format(self._inCatFileName)

    def _logAndRaise(self, errorMsg):
        self._log.error(errorMsg)
        raise Exception(errorMsg)

    def run(self):
        LoggerFactory.setLevel(self._log, self._verbosity)
        self._checkOptions()
        self._log.info("START RMcat2path on {}".format(self._inCatFileName))
        outFileHandler = open(self._outPathFileName, 'w')
        countLines = 0
        with open(self._inCatFileName, 'r') as f:
            for line in f:
                items = line.split(" ")
                # Only lines starting with an integer are used, to be compatible with output format *.cat from RepeatMasker V4.*
                if (line != "\n" and items[0].isdigit()):
                    countLines += 1
                    scoreSW = items[0]  # not used
                    percDiv = items[1]
                    percId = 100.0 - float(percDiv)
                    percDel = items[2]  # not used
                    percIns = items[3]  # not used

                    # query coordinates are always direct (start<end)
                    qryName = items[4]
                    qryStart = int(items[5])
                    qryEnd = int(items[6])
                    qryAfterMatch = items[7]  # not used

                    # if subject on direct strand
                    if len(items) == 13:
                        sbjName = items[8]
                        sbjStart = int(items[9])
                        sbjEnd = int(items[10])
                        sbjAfterMatch = items[11]  # not used

                    # if subject on reverse strand
                    elif items[8] == "C":
                        sbjName = items[9]
                        sbjAfterMatch = items[10]  # not used
                        sbjStart = int(items[11])
                        sbjEnd = int(items[12])

                    # compute a new score: match length on query times identity
                    matchLengthOnQuery = qryEnd - qryStart + 1
                    newScore = int(matchLengthOnQuery * float(percId) / 100.0)

                    string = "{}\t{}\t{}\t{}\t{}\t{}\t{}\t0.0\t{}\t{}\n".format(
                        countLines, qryName, qryStart, qryEnd, sbjName, sbjStart, sbjEnd, newScore, percId)

                    outFileHandler.write(string)

        outFileHandler.close()

        self._log.info("The result file contains {} lines".format(countLines))
        self._log.info("END RMcat2path")

if __name__ == "__main__":
    iRC2P = RMcat2path()
    iRC2P.setAttributesFromCmdLine()
    iRC2P.run()
