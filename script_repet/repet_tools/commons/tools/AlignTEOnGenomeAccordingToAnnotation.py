#!/usr/bin/env python

# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import re
import os
import pandas
import sys
from commons.core.LoggerFactory import LoggerFactory
from commons.core.utils.RepetOptionParser import RepetOptionParser
import subprocess
from commons.core.seq.AlignedBioseqDB import AlignedBioseqDB
from commons.core.coord.PathUtils import PathUtils
from commons.core.coord.SetUtils import SetUtils
from commons.core.coord.Set import Set
from commons.core.seq.Bioseq import Bioseq
from commons.core.coord.Path import Path
from commons.core.seq.FastaUtils import FastaUtils
import multiprocessing
import random
import time
## Align a TE on genome according to annotation

LOG_DEPTH = "repet.commons.tools"


class AlignTEOnGenomeAccordingToAnnotation(object):

    def __init__(self, join_path_file="", genome_fasta="", consensus_fasta="", merge_same_path_id=False,
                 out_file_name="",
                 match_penalty=10, mismatch=8, gap_opening=16, gap_extend=4, gap_length=20, do_clean=False, verbosity=0,
                 singularity_img="",
                 log_name="AlignTEOnGenomeAccordingToAnnotation.log"):
        self._pathFileName = join_path_file
        self._queryFileName = genome_fasta
        self._subjectFileName = consensus_fasta
        self._singularityImg = singularity_img

        if out_file_name == "" :
            self._outFileName = "{}_join_align".format(join_path_file)
        else :
            self._outFileName =out_file_name

        self._mergeSamePathId = merge_same_path_id
        self._matchPenalty = match_penalty
        self._mismatch = mismatch
        self._gapOpening = gap_opening
        self._gapExtend = gap_extend
        self._gapLength = gap_length

        self._doClean = do_clean
        self._verbosity = verbosity

        self._log = LoggerFactory.createLogger("{}.{}".format((LOG_DEPTH, self.__class__.__name__), self._verbosity),
                                               log_name)
    def _check_options(self):
        if self._pathFileName == "" or not os.path.exists(self._pathFileName):
            self._log_and_raise("ERROR: Missing path table")
        if self._queryFileName == "" or not os.path.exists(self._queryFileName):
            self._log_and_raise("ERROR: Missing query table")
        if self._subjectFileName == "" or not os.path.exists(self._subjectFileName):
            self._log_and_raise("ERROR: Missing subject table")

    def _log_and_raise(self, error_msg):
        self._log.error(error_msg)
        raise Exception(error_msg)

    # def align_bioseq_with_NWalign(self, bioseq1, bioseq2, singularity_img):
    #     uniqID = random.randint(1, 100000)
    #     t = time.strftime("%Y%m%d%H%M%S")
    #     fasta_file_name1 = "seqtoalign1{}_{}.tmp".format(t, uniqID)
    #     bioseq1.save(fasta_file_name1, mode="w")
    #     fasta_file_name2 = "seqtoalign2{}_{}.tmp".format(t, uniqID)
    #     bioseq2.save(fasta_file_name2, mode="w")
    #     align_bioseq_file_name = "aligned_{}_{}.tmp".format(t, uniqID)
    #     if singularity_img == "":
    #         cmd = "NWalign"
    #     else:
    #         cmd = "singularity exec {} NWalign".format(singularity_img)
    #     cmd += " {}".format(fasta_file_name1)
    #     cmd += " {}".format(fasta_file_name2)
    #     cmd += " -m {}".format(self._matchPenalty)
    #     cmd += " -d {}".format(self._mismatch)
    #     cmd += " -g {}".format(self._gapOpening)
    #     cmd += " -e {}".format(self._gapExtend)
    #     cmd += " -l {}".format(self._gapLength)
    #     cmd += " -D"
    #     cmd += " -o {}".format(align_bioseq_file_name)
    #
    #     iAlignedBioseqDB = AlignedBioseqDB()
    #     if os.path.exists(fasta_file_name1) and os.path.exists(fasta_file_name2):
    #         self._log.debug("Running : {}".format(cmd))
    #         process = subprocess.call(cmd, shell=True)
    #         # process.communicate()
    #         if process != 0:
    #             self._log_and_raise("ERROR when launching '{}'".format(cmd))
    #         else:
    #             iAlignedBioseqDB.load(align_bioseq_file_name)
    #             os.remove(fasta_file_name1)
    #             os.remove(fasta_file_name2)
    #             os.remove(align_bioseq_file_name)
    #     return iAlignedBioseqDB

    @staticmethod
    def _get_path_list_from_id(df_join_path, path_nb):
        list_paths = list()
        tmp_df = df_join_path.loc[path_nb]
        if tmp_df.shape == (9,):
            p = Path()
            p.setFromTuple([tmp_df.name] + tmp_df.to_list())
            list_paths.append(p)
        else:
            for index, row in tmp_df.iterrows():
                p = Path()
                p.setFromTuple([index] + list(row))
                list_paths.append(p)
        return list_paths

    @staticmethod
    def _get_bioseq_from_set_list(list_sets, fasta_dict):
        header = "{}::{} {} ".format(list_sets[0].name, list_sets[0].id, list_sets[0].seqname)
        sequence = ""
        list_sorted_sets = SetUtils.getSetListSortedByIncreasingMinThenMax(list_sets)
        if not list_sets[0].isOnDirectStrand():
            list_sorted_sets.reverse()
        for iSet in list_sorted_sets:
            #pprint (list_sorted_sets)
            start = iSet.getStart()
            end = iSet.getEnd()
            header += "{}..{},".format(start, end)
            bs = Bioseq()
            if start <= 0 or end <= 0:
                print("ERROR with coordinates start={} or end={}".format(start, end))
                sys.exit(1)
            if iSet.seqname not in fasta_dict.keys():
                print("ERROR: accession '{}' absent from fasta".format(iSet.seqname))
                sys.exit(1)
            length_accession = len(fasta_dict[iSet.seqname])
            if start > length_accession or end > length_accession:
                print("ERROR: coordinates start={} end={} out of sequence '{}' range ({} bp)".format(start, end,
                                                                                                     iSet.seqname,
                                                                                                     length_accession))
                sys.exit(1)
            subseq = fasta_dict[iSet.seqname][min(start, end) - 1: (min(start, end) + abs(end - start))]
            bs.sequence = subseq
            if start > end:
                bs.reverseComplement()
            sequence += bs.sequence

        return Bioseq(header[:-1], sequence)




    def _launch_nwalign(self, fastas):
        key = fastas[0]
        fasta1 = fastas[1][0]
        fasta2 = fastas[1][1]
        # iAlignedBioseqDB = self.align_bioseq_with_NWalign(fasta1, fasta2, self._singularityImg)
        # uniqID = random.randint(1, 100000)
        t = time.strftime("%Y%m%d%H%M%S")
        fasta_file_name1 = "seqtoalign1_{}_{}.tmp".format(t, key)
        print(fasta1.header,fasta2.header)
        fasta1.save(fasta_file_name1, mode="w")
        fasta_file_name2 = "seqtoalign2_{}_{}.tmp".format(t, key)
        fasta2.save(fasta_file_name2, mode="w")
        align_bioseq_file_name = "aligned_{}_{}.tmp".format(t, key)
        stdout_tmp = "stdout_{}_{}.tmp".format(t, key)
        if self._singularityImg == "":
            cmd = "NWalign"
        else:
            cmd = "singularity exec {} NWalign".format(self._singularityImg)
        cmd += " {}".format(fasta_file_name1)
        cmd += " {}".format(fasta_file_name2)
        cmd += " -m {}".format(self._matchPenalty)
        cmd += " -d {}".format(self._mismatch)
        cmd += " -g {}".format(self._gapOpening)
        cmd += " -e {}".format(self._gapExtend)
        cmd += " -l {}".format(self._gapLength)
        cmd += " -D"
        cmd += " -o {}".format(align_bioseq_file_name)
        cmd += " -v 4"# output score and identity on the stdout file, see the comment at the end of the file to get an explanation as to why i do that


        iAlignedBioseqDB = AlignedBioseqDB()
        if os.path.exists(fasta_file_name1) and os.path.exists(fasta_file_name2):
            self._log.debug("Running : {}".format(cmd))
            #process = subprocess.run(cmd, shell=True, capture_output=True, text=True)
            process = subprocess.run(cmd, shell=True, capture_output=True, text=True) #replaced the subprocess.call by a subprocess.run, gonne are the python2 dark days
            #self._log.debug( "captured {} as stdout".format(process.stdout))
            #self._log.debug( "the score should be {} ".format(int(re.findall(r'\d+',re.split("score=", process.stdout)[1])[0])))
            #self._log.debug( "the identity should be {} ".format(float(re.split("identity=", process.stdout)[1])))


            if process.returncode != 0:
                self._log_and_raise("ERROR when launching '{}'".format(cmd))

            else:
                if os.path.exists(align_bioseq_file_name):
                    iAlignedBioseqDB.load(align_bioseq_file_name)
                    if not re.match("Score=", iAlignedBioseqDB.db[0].header): # If no alignement score on the header
                        iAlignedBioseqDB.db[0].header += " Score={}".format(int(re.findall(r'\d+',re.split("score=", process.stdout)[1])[0])) #Add the missing score to the header,
                    if not re.match("Identity=", iAlignedBioseqDB.db[0].header): #same logic whit the identity
                        iAlignedBioseqDB.db[0].header += " Identity={}".format(float(re.split("identity=", process.stdout)[1]))#add the missing identity to the header
                    os.remove(fasta_file_name1)
                    os.remove(fasta_file_name2)
                    os.remove(align_bioseq_file_name)

        self._log.debug( "AlignedBioseqDB.db[0] : '{}'" .format(iAlignedBioseqDB.db[0].header))
        score_with_end_line = re.split("Score=", iAlignedBioseqDB.db[0].header)[1] #split the header before and after the "Score=" string -> get two str -> we grab the second
        score = int(score_with_end_line.split()[0]) #Grabs the integer in the str, the first integer is assumed to be the score
        identity = re.split("Identity=", score_with_end_line)[1]
        if identity == "nan" or identity == "-nan":
            identity = "0.0"
        identity = float(identity) * 100.0
        new_row = pandas.DataFrame({"query_aligned_seq": [iAlignedBioseqDB.db[0].sequence],
                                    "subject_aligned_seq": [iAlignedBioseqDB.db[1].sequence],
                                    "score": [score],
                                    "identity": [round(identity, 4)],
                                    }, index=[key])
        self._log.debug("header: {}".format(iAlignedBioseqDB.db[0].header))
        self._log.debug("path {} Score={} Identity={} ok".format(key, score, identity))
        self._log.debug(iAlignedBioseqDB.db[1].sequence[:80])
        return new_row

    def align_seq_according_to_path_and_build_aligned_seq_file(self):
        self._log.debug("out table name: {}".format(self._outFileName))
        consensus_fasta = FastaUtils.read_fasta(self._subjectFileName)
        genome_fasta = FastaUtils.read_fasta(self._queryFileName)
        df_join_path = pandas.read_csv(self._pathFileName, sep='\t', header=None, index_col=0, low_memory=False)
        list_path_id = df_join_path.index.unique()
        path_nb = len(list_path_id)
        dict_path_id_to_list_bioseq = {}
        count = 0

        df_out = pandas.DataFrame(columns=["query_aligned_seq", "subject_aligned_seq", "score", "identity"])

        if self._mergeSamePathId:  # le merge des fragments de l'annotation consiste a dire que la sequence de la copie commence au start de 1ier fragment et qu'elle se finit au end du dernier fragment
            for pathNum in list_path_id:
                count += 1
                self._log.debug("{}/{} => {}....".format(count, path_nb, pathNum))
                list_paths = self._get_path_list_from_id(df_join_path, pathNum)
                list_query_sets = PathUtils.getSetListFromQueries(list_paths)
                list_query_merged_sets = SetUtils.mergeSetsInList(list_query_sets)
                list_subject_sets = PathUtils.getSetListFromSubjects(list_paths)
                list_subject_merged_sets = SetUtils.mergeSetsInList(list_subject_sets)
                # dico, pathId as key, list of queryBioseq and subjectBioseq as value
                dict_path_id_to_list_bioseq[pathNum] = [
                    self._get_bioseq_from_set_list(list_query_merged_sets, genome_fasta),
                    self._get_bioseq_from_set_list(list_subject_merged_sets, consensus_fasta)]
        else:
            for pathNum in list_path_id:
                count += 1
                self._log.debug("{}/{} => {}....".format(count, path_nb, pathNum))
                list_paths = self._get_path_list_from_id(df_join_path, pathNum)
                query_name = list_paths[0].getquery_name()
                subject_name = list_paths[0].getsubject_name()
                list_query_start = []
                list_query_end = []
                list_subject_start = []
                list_subject_end = []
                is_reversed = not list_paths[0].isSubjectOnDirectStrand()
                for iPath in list_paths:
                    list_query_start.append(iPath.getquery_start())
                    list_query_end.append(iPath.getquery_end())
                    list_subject_start.append(iPath.getsubject_start())
                    list_subject_end.append(iPath.getsubject_end())
                query_start = min(list_query_start)
                query_end = max(list_query_end)
                if is_reversed:
                    subject_start = max(list_subject_start)
                    subject_end = min(list_subject_end)
                else:
                    subject_start = min(list_subject_start)
                    subject_end = max(list_subject_end)
                list_query_sets = [Set(pathNum, subject_name, query_name, query_start, query_end)]
                list_subject_sets = [Set(pathNum, query_name, subject_name, subject_start, subject_end)]
                dict_path_id_to_list_bioseq[pathNum] = [self._get_bioseq_from_set_list(list_query_sets, genome_fasta),
                                                        self._get_bioseq_from_set_list(list_subject_sets,
                                                                                       consensus_fasta)]

        # for each pathId, alignment of annotation copy and its match(s) on TE or consensus
        tmp = list()
        nb_process = int(multiprocessing.cpu_count() - 2)
        pool = multiprocessing.Pool(nb_process)
        for p in pool.map(self._launch_nwalign, dict_path_id_to_list_bioseq.items()):
            tmp.append(p)

        df_out = pandas.concat([df_out] + tmp, axis=0)
        df_out.to_csv(self._outFileName, sep="\t", index=True, header=None)

    @staticmethod
    def _write_align_file_with_align_length(filename):
        with open("{}.tab".format(filename), "w") as outFile:
            outFile.write("PathId\talignLength\talignIdent\tscore\n")
            with open(filename, "r") as theFile:
                for iLine in theFile:
                    list_items = iLine.rstrip("\n").split(
                        "\t")  ## the fields are pathId, qAlign (corresponding to copy), sAlign (corrsponding to consensus), score, identity
                    outFile.write(
                        "{}\t{}\t{:0.2f}\t{}\n".format(list_items[0], len(list_items[1]), float(list_items[4]),
                                                       int(list_items[3])))

    def run(self):
        LoggerFactory.setLevel(self._log, self._verbosity)
        self._check_options()
        self._log.info("START AlignTEOnGenomeAccordingToAnnotation")
        self._log.debug("path table name: {}".format(self._pathFileName))
        self._log.debug("query table name: {}".format(self._queryFileName))
        self._log.debug("subject table name: {}".format(self._subjectFileName))
        self.align_seq_according_to_path_and_build_aligned_seq_file()
        self._write_align_file_with_align_length(
            self._outFileName)  ## that will provide an ouput file like  <self._outFileName>.tab
        self._log.info("END AlignTEOnGenomeAccordingToAnnotation")

#Parser that does not work
"""
if __name__ == "__main__":

    usage = "AlignTEOnGenomeAccordingToAnnotation.py [options]"
    description = "Align a TE on genome according to annotation."
    description += "\nOutputs are align file and table with fields 'pathId, qSequence aligned, sSequence aligned, score, identity'"
    description += "\nand tab file with fields 'pathId, alignLength, alignIdentity, alignScore'"
    epilog = "\nExample 1: launch without verbosity and keep temporary files.\n"
    epilog += "\t$ AlignTEOnGenomeAccordingToAnnotation.py -p DmelChr4_chr_allTEs_nr_noSSR_join_path -q DmelChr4_chr_seq -s DmelChr4_refTEs_seq -v 0"
    epilog += "\n"
    parser = RepetOptionParser(description=description, epilog=epilog, usage=usage)
    parser.add_option("-q", "--query", dest="queryFileName", action="store", type="string",
                      help="queries file name, file containing genome sequences [compulsory] [format: fasta]",
                      default="")
    parser.add_option("-s", "--subject", dest="subjectFileName", action="store", type="string",
                      help="subjects file name, file containing library sequences used to annotate the genome [compulsory] [format: fasta]",
                      default="")
    parser.add_option("-p", "--path", dest="pathFileName", action="store", type="string",
                      help="path file name, file containing genome annotations with library sequences [compulsory] [format: tsv]",
                      default="")
    parser.add_option("-m", "--merge", dest="mergeSamePathId", action="store_true",
                      help="merge joined matchs [optional] [default: False]", default=False)
    parser.add_option("-o", "--out", dest="outFileName", action="store", type="string",
                      help="output file name, file that will contain for a annotation (pathId) 2 aligned sequences (query and subject) with score and identity [default: <pathTableName>_align] [format: align]",
                      default="")

    parser.add_option("-a", "--match_penalty", dest="matchPenalty", action="store", type="int",
                      help="match penalty [default=10]", default=10)
    parser.add_option("-i", "--mismatch", dest="mismatch", action="store", type="int",
                      help="mismatch penalty [default=8]", default=8)
    parser.add_option("-g", "--gap_opening", dest="gapOpening", action="store", type="int",
                      help="gap opening penalty [default=16]", default=16)
    parser.add_option("-e", "--gap_extend", dest="gapExtend", action="store", type="int",
                      help="gap extend penalty [default=4]", default=4)
    parser.add_option("-l", "--gap_length", dest="gapLength", action="store", type="int",
                      help="max length of penalized gap [default=20]", default=20)

    parser.add_option("-c", "--clean", dest="doClean", action="store_true",
                      help="clean temporary files [optional] [default: False]", default=False)
    parser.add_option("-v", "--verbosity", dest="verbosity", action="store", type="int",
                      help="verbosity [optional] [default: 1]", default=1)
    options = parser.parse_args()[0]

    iATEOGAT = AlignTEOnGenomeAccordingToAnnotation(join_path_file=options.pathFileName, genome_fasta=options.queryFileName,
                                                    consensus_fasta=options.subjectFileName, merge_same_path_id=options.mergeSamePathId,
                                                    out_file_name=options.outFileName, match_penalty=options.matchPenalty,
                                                    mismatch=options.mismatch, gap_opening=options.gapOpening, gap_extend=options.gapExtend,
                                                    gap_length=options.gapLength, do_clean=options.doClean, verbosity=options.verbosity)
    #iATEOGAT.setAttributesFromCmdLine()
    iATEOGAT.run()
"""
    #quick hard wired test
"""
if __name__ == "__main__":
    join_path_file = "./DmelChr4_TEannot_Matcher_chk_allTEs.path-nr_noSSR.on_chr_join"
    genome_file = "DmelChr4.fa"
    consensus = "all_consensus_repet.fa"
    sif = "../REPET-snakemake/Singularity/repet_debian.sif"


    iATOGATA = AlignTEOnGenomeAccordingToAnnotation(join_path_file=join_path_file,
                                                    genome_fasta=genome_file,
                                                    consensus_fasta=consensus,
                                                    merge_same_path_id=True,
                                                    verbosity=4,
                                                    match_penalty=10,
                                                    mismatch=8,
                                                    gap_opening=16,
                                                    gap_extend=4,
                                                    gap_length=20,
                                                    #singularity_img=sif,
                                                    out_file_name="NWalign_2.31_hack_test")
    iATOGATA.run()
"""
########EXPLANATIONS OF stdout_tmp##########
"""
    NWalign, as of TE_finder2.31, does not output identity and score on the alignement file anymore,
    But it still output these two on the stdout if there is a verbose parameter given to NWAlign that is superior or equal to 1
    As a patch job, we grabed these two and append them where they should be
    (The lack of score and identity is suposedly caused by a change in BLAST parameter, where some blast sensitivity value can cause theirs disapearence)
    (I can't change the blast sensitivity of NWalign whitout modifying the program itself, so this is the solution that i went whit)
"""
