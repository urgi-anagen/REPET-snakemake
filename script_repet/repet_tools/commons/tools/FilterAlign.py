#!/usr/bin/env python

# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
import subprocess
from commons.core.LoggerFactory import LoggerFactory
from commons.core.utils.RepetOptionParser import RepetOptionParser
from commons.core.checker.CheckerUtils import CheckerUtils

LOG_DEPTH = "repet.tools"


class FilterAlign(object):
    """
    This program filters the output from BLASTER ('align' file recording HSPs).
    """

    def __init__(self, inFileName="", outFileName="", maxEValue=1, minScore=0, minIdentity=0, minLength=0,
                 maxLength=1000000000, minSubLength=20, resetEvalues=False, verbosity=1, log_name="FilterAlign.log"):
        self._setInFileName(inFileName)
        self._setOutFileName(outFileName)
        self.maxEValue = float(maxEValue)
        self.minScore = int(float(minScore))
        self.minIdentity = int(float(minIdentity))
        self.minLength = int(minLength)
        self.maxLength = int(maxLength)
        self.minSubLength = int(minSubLength)
        self.mustResetEvalues = resetEvalues
        self._verbosity = verbosity
        #self._log = LoggerFactory.createLogger("{}.{}".format(LOG_DEPTH, self.__class__.__name__), self._verbosity)
        self._log = LoggerFactory.createLogger("{}.{}".format((LOG_DEPTH, self.__class__.__name__), self._verbosity), log_name)

    def setAttributesFromCmdLine(self):
        usage = "FilterAlign.py [options]"
        description = "This program filters the output from BLASTER ('align' file recording HSPs)"
        epilog = "Example: launch without verbosity.\n"
        epilog += "\t$ FilterAlign.py -i file.fa -v 0\n"
        parser = RepetOptionParser(description=description, epilog=epilog, usage=usage)
        parser.add_option("-i", "--inFileName", dest="inFileName", action="store", type="string",
                          help="name of the input file [compulsory] [format: align]", default="")
        parser.add_option("-E", "--maxEvalue", dest="maxEvalue", action="store", type="float",
                          help="maximum E-value [optional] [default: 1]", default=1)
        parser.add_option("-S", "--minScore", dest="minScore", action="store", type="int",
                          help="minimum score [optional] [default: 0]", default=0)
        parser.add_option("-I", "--minIdentity", dest="minIdentity", action="store", type="int",
                          help="minimum identity [optional] [default: 0]", default=0)
        parser.add_option("-l", "--minLength", dest="minLength", action="store", type="int",
                          help="minimum query length [optional] [default: 0]", default=0)
        parser.add_option("-L", "--maxLength", dest="maxLength", action="store", type="int",
                          help="maximum query length [optional] [default: 1000000000]", default=1000000000)
        parser.add_option("-s", "--minSubLength", dest="minSubLength", action="store", type="int",
                          help="minimum subject length [optional] [default: 20]", default=20)
        parser.add_option("-r", "--reset", dest="resetEvalues", action="store_true",
                          help="set all E-values to 0 after filtering according to -E [optional] [default: False]",
                          default=False)
        parser.add_option("-o", "--out", dest="outFileName", action="store", type="string",
                          help="output file name [default: <input>.filtered]", default="")
        parser.add_option("-v", "--verbosity", dest="verbosity", action="store", type="int",
                          help="verbosity [optional] [default: 1]", default=1)
        options = parser.parse_args()[0]
        self._setAttributesFromOptions(options)

    def _setAttributesFromOptions(self, options):
        self._setInFileName(options.inFileName)
        self._setOutFileName(options.outFileName)
        self.maxEValue = float(options.maxEvalue)
        self.minScore = int(float(options.minScore))
        self.minIdentity = int(float(options.minIdentity))
        self.minLength = int(options.minLength)
        self.maxLength = int(options.maxLength)
        self.minSubLength = int(options.minSubLength)
        self.mustResetEvalues = options.resetEvalues
        self._verbosity = options.verbosity

    def _setInFileName(self, inFileName):
        if inFileName == "":
            self._logAndRaise("filterAlign ERROR, no input file specified")
        else:
            self.inFileName = "{}".format(inFileName) #snakemake has a tendency to send back file in the form of Namedlist , not str or path object
                                                      #this makes sure that InFilename is an str

    def _setOutFileName(self, outFileName):
        if outFileName == "":
            self.outFileName = "{}.filtered".format(self.inFileName)
        else:
            self.outFileName = outFileName

    def _checkOptions(self):
        if self.inFileName == "":
            self._logAndRaise("ERROR: Missing input file name")

    def _logAndRaise(self, errorMsg):
        self._log.error(errorMsg)
        raise Exception(errorMsg)

    def _runFiltering(self):
        inFile = open(self.inFileName, "r")
        outFile = open(self.outFileName, "w")

        nbMatches = 0
        nbFiltered = 0

        line = inFile.readline()
        while line:
            nbMatches += 1
            data = line.split("\t")
            qryName = data[0]
            qryStart = data[1]
            qryEnd = data[2]
            sbjName = data[3]
            sbjStart = data[4]
            sbjEnd = data[5]
            Evalue = float(data[6])
            if Evalue == 0.0:
                Evalue = 0
            score = int(data[7])
            identity = float(data[8].split("\n")[0])
            sbjLength = abs(int(sbjEnd) - int(sbjStart)) + 1
            matchLength = abs(int(qryEnd) - int(qryStart)) + 1

            if Evalue <= self.maxEValue and matchLength >= self.minLength and \
                    identity >= self.minIdentity and matchLength <= self.maxLength and \
                    score >= self.minScore and sbjLength >= self.minSubLength:

                if self.mustResetEvalues:
                    Evalue = 0
                string = "{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{:.1f}\n".format(qryName, qryStart, qryEnd, sbjName, sbjStart,
                                                                           sbjEnd, Evalue, score, identity)

                outFile.write(string)
            else:
                nbFiltered += 1
                string = "qry {} ({}-{}) vs subj {} ({}-{}): Eval={} identity={} matchLength={} score={}".format \
                    (qryName, qryStart, qryEnd, sbjName, sbjStart, sbjEnd, Evalue, identity, matchLength, score)
                self._log.debug(string)
            line = inFile.readline()

        inFile.close()
        outFile.close()

        self._log.debug("Total number of matches: {} \nNumber of filtered matches: {}".format(nbMatches, nbFiltered))

    def run(self):
        LoggerFactory.setLevel(self._log, self._verbosity)
        self._checkOptions()
        self._log.info("START FilterAlign")
        self._log.debug(
            "Input file name: {}, maxEvalue: {}, minScore: {}, minId: {}, minLength: {}, maxLength {}, minSubLength: {}".format(
                self.inFileName, self.maxEValue, self.minScore, self.minIdentity, self.minLength, self.maxLength,
                self.minSubLength))

        returncode = 0
        if CheckerUtils.isExecutableInUserPath("filterAlign"):
            cmd = "filterAlign"
            cmd += " -i {}".format(self.inFileName)
            cmd += " -E {}".format(self.maxEValue)
            cmd += " -S {}".format(self.minScore)
            cmd += " -I {}".format(self.minIdentity)
            cmd += " -l {}".format(self.minLength)
            cmd += " -L {}".format(self.maxLength)
            #cmd += " -s {}".format(self.minSubLength) # deprecated in TE_finder2.23
            if self.mustResetEvalues:
                cmd += " -r"
            cmd += " -o {}".format(self.outFileName)
            cmd += " -v {}".format(self._verbosity)
            self._log.debug("Running : {}".format(cmd))
            returncode = subprocess.call(cmd, shell=True)
        else:
            self._runFiltering()

        if returncode != 0:
            self._logAndRaise("ERROR when launching '{}'".format(cmd))
        self._log.info("END FilterAlign")


if __name__ == "__main__":
    iLaunch = FilterAlign()
    iLaunch.setAttributesFromCmdLine()
    iLaunch.run()
