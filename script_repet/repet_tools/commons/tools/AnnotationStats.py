#!/usr/bin/env python

# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

##@file
# Give summary information on a TE annotation table.
# options:
#     -h: this help
#     -t: analysis type (default = 1, 1: per transposable element (TE), 2: per cluster, 3: per classification, 4: with map input file)
#     -p: name of the table (_path) or file (.path) with the annotated TE copies
#     -s: name of the table (_seq) or file (.fasta or .fa) with the TE reference sequences
#     -g: length of the genome (in bp)
#     -m: name of the file with the group and the corresponding TE names (format = 'map')
#     -o: name of the output file (default = pathTableName + '_stats.txt')
#     -C: name of the configuration file to access MySQL (e.g. 'TEannot.cfg')
#     -c: remove map files and blastclust file (if analysis type is 2 or 3)
#     -I: identity coverage threshold (default = 0)
#     -L: length coverage threshold (default=0.8)
#     -v: verbosity level (default = 0)

from commons.core.LoggerFactory import LoggerFactory
from commons.core.stat.Stat import Stat
from commons.tools.getCumulLengthFromTEannot import getCumulLengthFromTEannot
import os
import pandas
from commons.core.seq.FastaUtils import FastaUtils

LOG_DEPTH = "repet.tools"

# TODO: use templating engine instead of raw strings for AnnotationStatsWriter
class AnnotationStats(object):

    def __init__(self, analysisName = "TE", clusterFileName = "", seqFileName = "", pathFileName = "", classifConsensusFile = "",
                 genomeLength = 0, statsFileName = "", globalStatsFileName = "", verbosity = 3, singularityImg = "",
                 logName = "AnnotationStats.log"):

        self._analysis_name = analysisName
        self._cluster_file_name = clusterFileName

        self._consensus_fasta_file = seqFileName
        self._join_path_file = pathFileName
        self._genome_length = genomeLength
        self._classif_file = classifConsensusFile
        self._singularity_img = singularityImg

        self._consensus_fasta = None
        self._df_join_path = None
        self._dict_cons_name_to_trigram = {}

        self._stats_file_name = statsFileName
        self._global_stats_file_name = globalStatsFileName

        self._clean = False
        self._verbosity = verbosity
        self._log = LoggerFactory.createLogger("{}.{}" .format((LOG_DEPTH, self.__class__.__name__), self._verbosity),
                                               logName)

        self._check_attributes()


    def _check_attributes(self):
        if os.path.exists(self._classif_file) :
            df_classif = pandas.read_csv(self._classif_file, sep='\t', low_memory=False, keep_default_na=False)
            for i in df_classif.index:
                self._dict_cons_name_to_trigram[df_classif.loc[i, "Seq_name"]] = (df_classif.loc[i, "Wcode"], df_classif.loc[i, "class"])

        if os.path.exists(self._consensus_fasta_file) :
            self._consensus_fasta = FastaUtils.read_fasta(self._consensus_fasta_file)
        else :
            msg = "The fasta file does not exist"
            self._log_and_raise(msg)

        if os.path.exists(self._join_path_file) :
            self._df_join_path = pandas.read_csv(self._join_path_file, sep='\t', header=None, index_col=0, low_memory=False)
        else :
            msg = "The path file does not exist"
            self._log_and_raise(msg)


    def _log_and_raise(self, errorMsg):
        self._log.error(errorMsg)
        raise Exception(errorMsg)

    

    ## Get the coverage of TE copies for a given family (using 'mapOp')
    #
    # @param consensus string name of a TE family ('subject_name' in the 'path' table)
    # @return cumulCoverage integer cumulative coverage
    #
    def get_cumul_coverage(self, consensus = ""):
        gclft = getCumulLengthFromTEannot(join_path_file=self._join_path_file,
                                          TE_refseq=consensus, clean=False,
                                          singularity_img=self._singularity_img)
        map_file_name = gclft.get_all_subjects_as_map_of_queries()
        merge_file_name = gclft.merge_ranges(map_file_name)
        cumul_coverage = gclft.get_cumul_length(merge_file_name)
        return cumul_coverage

    ## Get the number of full-lengths (95% <= L =< 105%)
    #
    # @param consensus_length integer
    # @param list_lengths list of integers
    # @return full_length_consensus_nb integer
    #
    def get_nb_full_lengths(self, consensus_length, list_lengths):
        full_length_consensus_nb = 0
        for i in list_lengths:
            if (i / float(consensus_length) >= 0.95) and (i / float(consensus_length) <= 1.05):
                full_length_consensus_nb += 1
        return full_length_consensus_nb

# TODO: copies flagged as full-length fragment CAN'T belong to multi-fragments copies (fragments that have been joined). Use the beginning of implementation commented below.
    def get_stat_per_TE(self, consensus_name):
        dict_consensus_stats = {}
        sub_df = self._df_join_path.loc[self._df_join_path[4] == consensus_name]
        list_length_per_fragment = (abs(sub_df[3] - sub_df[2]) + 1).tolist()
        list_length_per_copy = (abs(sub_df[3] - sub_df[2]) + 1).groupby(0).sum().tolist()
        list_identity_per_copy = ((sub_df[9] * (abs(sub_df[3] - sub_df[2]) + 1)).groupby(0).sum() / (
                    abs(sub_df[3] - sub_df[2]) + 1).groupby(0).sum()).tolist()

        if self._dict_cons_name_to_trigram:
            dict_consensus_stats["Wcode"] = self._dict_cons_name_to_trigram[consensus_name][0]

        dict_consensus_stats["length"] = len(self._consensus_fasta[consensus_name])
        dict_consensus_stats["cumulCoverage"] = self.get_cumul_coverage(consensus_name)
        dict_consensus_stats["nbFragments"] = len(list_length_per_fragment)
        dict_consensus_stats["nbFullLengthFragments"] = self.get_nb_full_lengths(dict_consensus_stats["length"], list_length_per_fragment)
        dict_consensus_stats["nbCopies"] = len(list_length_per_copy)
        dict_consensus_stats["nbFullLengthCopies"] = self.get_nb_full_lengths(dict_consensus_stats["length"], list_length_per_copy)
        dict_consensus_stats["statsIdentityPerChain"] = Stat()
        dict_consensus_stats["statsLengthPerChain"] = Stat()
        dict_consensus_stats["statsLengthPerChainPerc"] = Stat()
        self._stats_for_identity_and_length(dict_consensus_stats, list_length_per_copy, list_identity_per_copy)
        return dict_consensus_stats

    def get_stat_per_cluster(self, list_consensus_names):
        dict_consensus_cluster_stats = {}
        list_length_per_fragment = []
        list_length_per_copy = []
        cumul_coverage_length = 0
        for consensus_name in list_consensus_names:
            sub_df = self._df_join_path.loc[self._df_join_path[4] == consensus_name]
            cumul_coverage_length += self.get_cumul_coverage(consensus_name)
            list_length_per_fragment.extend((abs(sub_df[3] - sub_df[2]) + 1).tolist())
            list_length_per_copy.extend((abs(sub_df[3] - sub_df[2]) + 1).groupby(0).sum().tolist())
        dict_consensus_cluster_stats["cumulCoverage"] = cumul_coverage_length
        dict_consensus_cluster_stats["nbFragments"] = len(list_length_per_fragment)
        dict_consensus_cluster_stats["nbCopies"] = len(list_length_per_copy)
        return dict_consensus_cluster_stats

    def get_cluster_list_from_file(self):
        list_clusters = []
        with open(self._cluster_file_name) as fCluster:
            for line in fCluster:
                list_consensus_names = line.rstrip().split("\t")
                list_clusters.append(list_consensus_names)
        return list_clusters




    def run(self):
        LoggerFactory.setLevel(self._log, self._verbosity)
        self._log.info("START")

        iASW = AnnotationStatsWriter()
        if self._analysis_name == "TE":

            with open(self._stats_file_name, "w") as fStats:
                if self._classif_file != "":
                    string = "Wcode\t{}\tlength\tcovg\tfrags\tfullLgthFrags\tcopies\tfullLgthCopies\t".format(
                        self._analysis_name)
                else:
                    string = "{}\tlength\tcovg\tfrags\tfullLgthFrags\tcopies\tfullLgthCopies\t".format(self._analysis_name)
                string += "meanId\tsdId\tminId\tq25Id\tmedId\tq75Id\tmaxId\t" # 7 items
                string += "meanLgth\tsdLgth\tminLgth\tq25Lgth\tmedLgth\tq75Lgth\tmaxLgth\t" # 7 items
                string += "meanLgthPerc\tsdLgthPerc\tminLgthPerc\tq25LgthPerc\tmedLgthPerc\tq75LgthPerc\tmaxLgthPerc\n" # 7 items
                fStats.write(string)

                list_names_TE_ref_seq = self._consensus_fasta.keys()
                list_distinct_subjects = set(self._df_join_path[4].tolist())
                total_cumul_coverage = self.get_cumul_coverage()

                with open(self._global_stats_file_name, "w") as fG:
                    fG.write("{}\n" .format(iASW.print_resume(list_names_TE_ref_seq, list_distinct_subjects, total_cumul_coverage, self._genome_length)))
                    for consensus_name in list_names_TE_ref_seq:
                        self._log.debug("processing '{}'..." .format(consensus_name))
                        dict_stat_for_one_consensus = self.get_stat_per_TE(consensus_name)
                        iASW.add_calculs_of_one_TE(dict_stat_for_one_consensus)
                        fStats.write("{}\n" .format(iASW.get_stat_as_string_for_TE(consensus_name, dict_stat_for_one_consensus)))
                    fG.write(iASW.print_stats_for_all_TEs(len(list_names_TE_ref_seq)))

        elif self._analysis_name == "Cluster":
            list_clusters = self.get_cluster_list_from_file()
            list_clusters.sort(key = lambda k: len(k), reverse = True)
            with open(self._stats_file_name, "w") as fStats:
                string = "{}\tcovg\tfrags\tcopies\n" .format(self._analysis_name)
                # TODO: add fullLgthFrags and fullLgthCopies ? Is addition of previous results significant ?
                fStats.write(string)
                for index, lConsensus in enumerate(list_clusters):
                    self._log.debug("processing '{}'..." .format(lConsensus))
                    dict_stat_for_one_cluster = self.get_stat_per_cluster(lConsensus)
                    fStats.write("{}\n" .format(iASW.get_stat_as_string_for_cluster(str(index + 1), dict_stat_for_one_cluster)))
        self._log.info("END")

    def _stats_for_identity_and_length(self, dStat, list_length_per_copy, list_identity_per_copy):
        for i in list_identity_per_copy:
            dStat["statsIdentityPerChain"].add(i)
        list_length_perc_per_copy = []
        for i in list_length_per_copy:
            dStat["statsLengthPerChain"].add(i)
            lperc = 100 * i / float(dStat["length"])
            list_length_perc_per_copy.append(lperc)
            dStat["statsLengthPerChainPerc"].add(lperc)



class AnnotationStatsWriter(object):

    def __init__(self):
        self._dict_all_TE_ref_seqs = { "sumCumulCoverage": 0,
                         "totalNbFragments": 0,
                         "totalNbFullLengthFragments": 0,
                         "totalNbCopies": 0,
                         "totalNbFullLengthCopies": 0,
                         "nbFamWithFullLengthFragments": 0,
                         "nbFamWithOneFullLengthFragment": 0,
                         "nbFamWithTwoFullLengthFragments": 0,
                         "nbFamWithThreeFullLengthFragments": 0,
                         "nbFamWithMoreThanThreeFullLengthFragments": 0,
                         "nbFamWithFullLengthCopies": 0,
                         "nbFamWithOneFullLengthCopy": 0,
                         "nbFamWithTwoFullLengthCopies": 0,
                         "nbFamWithThreeFullLengthCopies": 0,
                         "nbFamWithMoreThanThreeFullLengthCopies": 0,
                         "statsAllCopiesMedIdentity": Stat(),
                         "statsAllCopiesMedLengthPerc": Stat()
                         }

    def get_all_TEs_ref_seq_dict(self):
        return self._dict_all_TE_ref_seqs

    def get_stat_as_string_for_TE(self, name, d):
        """
        Return a string with all data properly formatted.
        """

        string = ""
        if "Wcode" in d.keys():
            string += "{}\t".format(d["Wcode"])
        string += "{}" .format(name)
        string += "\t{}" .format(d["length"])
        string += "\t{}" .format(d["cumulCoverage"])
        string += "\t{}" .format(d["nbFragments"])
        string += "\t{}" .format(d["nbFullLengthFragments"])
        string += "\t{}" .format(d["nbCopies"])
        string += "\t{}" .format(d["nbFullLengthCopies"])

        if d["statsIdentityPerChain"].getValuesNumber() != 0:
            string += "\t{:.2f}" .format(d["statsIdentityPerChain"].mean())

            string += "\t{:.2f}" .format(d["statsIdentityPerChain"].sd())
            string += "\t{:.2f}" .format(d["statsIdentityPerChain"].getMin())
            string += "\t{:.2f}" .format(d["statsIdentityPerChain"].quantile(0.25))
            string += "\t{:.2f}" .format(d["statsIdentityPerChain"].median())
            string += "\t{:.2f}" .format(d["statsIdentityPerChain"].quantile(0.75))
            string += "\t{:.2f}" .format(d["statsIdentityPerChain"].getMax())

        else:
            for i in range(0, 7):
                string += "\tNA"

        if d["statsLengthPerChain"].getValuesNumber() != 0:
            string += "\t{:.2f}" .format(d["statsLengthPerChain"].mean())

            string += "\t{:.2f}" .format(d["statsLengthPerChain"].sd())
            string += "\t{}" .format(int(d["statsLengthPerChain"].getMin()))
            string += "\t{:.2f}" .format(d["statsLengthPerChain"].quantile(0.25))
            string += "\t{:.2f}" .format(d["statsLengthPerChain"].median())
            string += "\t{:.2f}" .format(d["statsLengthPerChain"].quantile(0.75))
            string += "\t{}" .format(int(d["statsLengthPerChain"].getMax()))
        else:
            for i in range(0, 7):
                string += "\tNA"

        if d["statsLengthPerChainPerc"].getValuesNumber() != 0:
            string += "\t{:.2f}" .format(d["statsLengthPerChainPerc"].mean())

            string += "\t{:.2f}" .format(d["statsLengthPerChainPerc"].sd())
            string += "\t{:.2f}" .format(d["statsLengthPerChainPerc"].getMin())
            string += "\t{:.2f}" .format(d["statsLengthPerChainPerc"].quantile(0.25))
            string += "\t{:.2f}" .format(d["statsLengthPerChainPerc"].median())
            string += "\t{:.2f}" .format(d["statsLengthPerChainPerc"].quantile(0.75))
            string += "\t{:.2f}" .format(d["statsLengthPerChainPerc"].getMax())
        else:
            for i in range(0, 7):
                string += "\tNA"

        return string

    def get_stat_as_string_for_cluster(self, name, d):
        """
        Return a string with all data properly formatted.
        """
        string = ""
        string += "{}" .format(name)
        string += "\t{}" .format(d["cumulCoverage"])
        string += "\t{}" .format(d["nbFragments"])
        string += "\t{}" .format(d["nbCopies"])

        return string

    def add_calculs_of_one_TE(self, dict_one_TE_ref_seq):
        self._dict_all_TE_ref_seqs[ "sumCumulCoverage" ] += dict_one_TE_ref_seq[ "cumulCoverage" ]

        self._dict_all_TE_ref_seqs[ "totalNbFragments" ] += dict_one_TE_ref_seq[ "nbFragments" ]
        self._dict_all_TE_ref_seqs[ "totalNbFullLengthFragments" ] += dict_one_TE_ref_seq[ "nbFullLengthFragments" ]
        if dict_one_TE_ref_seq[ "nbFullLengthFragments" ] > 0:
            self._dict_all_TE_ref_seqs[ "nbFamWithFullLengthFragments" ] += 1
        if dict_one_TE_ref_seq[ "nbFullLengthFragments" ] == 1:
            self._dict_all_TE_ref_seqs[ "nbFamWithOneFullLengthFragment" ] += 1
        elif dict_one_TE_ref_seq[ "nbFullLengthFragments" ] == 2:
            self._dict_all_TE_ref_seqs[ "nbFamWithTwoFullLengthFragments" ] += 1
        elif dict_one_TE_ref_seq[ "nbFullLengthFragments" ] == 3:
            self._dict_all_TE_ref_seqs[ "nbFamWithThreeFullLengthFragments" ] += 1
        elif dict_one_TE_ref_seq[ "nbFullLengthFragments" ] > 3:
            self._dict_all_TE_ref_seqs[ "nbFamWithMoreThanThreeFullLengthFragments" ] += 1

        self._dict_all_TE_ref_seqs[ "totalNbCopies" ] += dict_one_TE_ref_seq[ "nbCopies" ]
        self._dict_all_TE_ref_seqs[ "totalNbFullLengthCopies" ] += dict_one_TE_ref_seq[ "nbFullLengthCopies" ]
        if dict_one_TE_ref_seq[ "nbFullLengthCopies" ] > 0:
            self._dict_all_TE_ref_seqs[ "nbFamWithFullLengthCopies" ] += 1
        if dict_one_TE_ref_seq[ "nbFullLengthCopies" ] == 1:
            self._dict_all_TE_ref_seqs[ "nbFamWithOneFullLengthCopy" ] += 1
        elif dict_one_TE_ref_seq[ "nbFullLengthCopies" ] == 2:
            self._dict_all_TE_ref_seqs[ "nbFamWithTwoFullLengthCopies" ] += 1
        elif dict_one_TE_ref_seq[ "nbFullLengthCopies" ] == 3:
            self._dict_all_TE_ref_seqs[ "nbFamWithThreeFullLengthCopies" ] += 1
        elif dict_one_TE_ref_seq[ "nbFullLengthCopies" ] > 3:
            self._dict_all_TE_ref_seqs[ "nbFamWithMoreThanThreeFullLengthCopies" ] += 1

        if dict_one_TE_ref_seq[ "statsIdentityPerChain" ].getValuesNumber() != 0:
            self._dict_all_TE_ref_seqs[ "statsAllCopiesMedIdentity" ].add(dict_one_TE_ref_seq[ "statsIdentityPerChain" ].median())

        if dict_one_TE_ref_seq[ "statsLengthPerChainPerc" ].getValuesNumber() != 0:
            self._dict_all_TE_ref_seqs[ "statsAllCopiesMedLengthPerc" ].add(dict_one_TE_ref_seq[ "statsLengthPerChainPerc" ].median())

    def print_stats_for_all_TEs(self, TEnb):
        stat_string = "total nb of TE fragments: {}\n" .format(self._dict_all_TE_ref_seqs[ "totalNbFragments" ])

        if self._dict_all_TE_ref_seqs[ "totalNbFragments" ] != 0:

            stat_string += "total nb full-length fragments: {} ({:.2f}%)\n" .format \
            (self._dict_all_TE_ref_seqs[ "totalNbFullLengthFragments" ], \
              100 * self._dict_all_TE_ref_seqs[ "totalNbFullLengthFragments" ] / float(self._dict_all_TE_ref_seqs[ "totalNbFragments" ]))

            stat_string += "total nb of TE copies: {}\n" .format(self._dict_all_TE_ref_seqs[ "totalNbCopies" ])

            stat_string += "total nb full-length copies: {} ({:.2f}%)\n" .format \
            (self._dict_all_TE_ref_seqs[ "totalNbFullLengthCopies" ], \
              100 * self._dict_all_TE_ref_seqs[ "totalNbFullLengthCopies" ] / float(self._dict_all_TE_ref_seqs[ "totalNbCopies" ]))

            stat_string += "families with full-length fragments: {} ({:.2f}%)\n" .format \
            (self._dict_all_TE_ref_seqs[ "nbFamWithFullLengthFragments" ], \
              100 * self._dict_all_TE_ref_seqs[ "nbFamWithFullLengthFragments" ] / float(TEnb))
            stat_string += " with only one full-length fragment: {}\n" .format(self._dict_all_TE_ref_seqs[ "nbFamWithOneFullLengthFragment" ])
            stat_string += " with only two full-length fragments: {}\n" .format(self._dict_all_TE_ref_seqs[ "nbFamWithTwoFullLengthFragments" ])
            stat_string += " with only three full-length fragments: {}\n" .format(self._dict_all_TE_ref_seqs[ "nbFamWithThreeFullLengthFragments" ])
            stat_string += " with more than three full-length fragments: {}\n" .format(self._dict_all_TE_ref_seqs[ "nbFamWithMoreThanThreeFullLengthFragments" ])

            stat_string += "families with full-length copies: {} ({:.2f}%)\n" .format \
            (self._dict_all_TE_ref_seqs[ "nbFamWithFullLengthCopies" ], \
              100 * self._dict_all_TE_ref_seqs[ "nbFamWithFullLengthCopies" ] / float(TEnb))
            stat_string += " with only one full-length copy: {}\n" .format(self._dict_all_TE_ref_seqs[ "nbFamWithOneFullLengthCopy" ])
            stat_string += " with only two full-length copies: {}\n" .format(self._dict_all_TE_ref_seqs[ "nbFamWithTwoFullLengthCopies" ])
            stat_string += " with only three full-length copies: {}\n" .format(self._dict_all_TE_ref_seqs[ "nbFamWithThreeFullLengthCopies" ])
            stat_string += " with more than three full-length copies: {}\n" .format(self._dict_all_TE_ref_seqs[ "nbFamWithMoreThanThreeFullLengthCopies" ])

            stat_string += "mean of median identity of all families: {:.2f} +- {:.2f}\n" .format \
            (self._dict_all_TE_ref_seqs[ "statsAllCopiesMedIdentity" ].mean(), \
              self._dict_all_TE_ref_seqs[ "statsAllCopiesMedIdentity" ].sd())

            stat_string += "mean of median length percentage of all families: {:.2f} +- {:.2f}\n" .format \
            (self._dict_all_TE_ref_seqs[ "statsAllCopiesMedLengthPerc" ].mean(), \
              self._dict_all_TE_ref_seqs[ "statsAllCopiesMedLengthPerc" ].sd())
        return stat_string

    def print_resume(self, list_names_TE_ref_seq, list_distinct_subjects, total_cumul_coverage, genomeLength):
        stat_string = "nb of sequences: {}\n" .format(len(list_names_TE_ref_seq))
        stat_string += "nb of matched sequences: {}\n" .format(len(list_distinct_subjects))
        stat_string += "cumulative coverage: {} bp\n" .format(total_cumul_coverage)
        stat_string += "coverage percentage: {:.2f}%\n" .format(100 * total_cumul_coverage / float(genomeLength))
        return stat_string

#
# if __name__ == "__main__":
#
#     #classif_consensus_file = "/home/mwan/REPET/REPET-snakemake/results_DmelChr4_repet/DmelChr4_TEdenovo/DmelChr4_Blaster_GrpRecPil_Map_TEclassif_Filtered/DmelChr4_sim_denovoLibTEs_PC_filtered.classif"
#     classif_consensus_file=""
#     consensus_fasta_file = "/home/mwan/REPET/REPET-snakemake/results_DmelChr4_repet/DmelChr4_refTEs.fa"
#     join_path_file = '/home/mwan/REPET/REPET-snakemake/results_DmelChr4_repet/DmelChr4_TEdetect/Dmel4_chr_noSSR_path_join.tsv'
#
#     genome_size = 1281640
#     sglrt_img = "/home/mwan/REPET/REPET-snakemake/singularity_img/te_finder_2.30.2.sif"
#
#     annotStatsPerTEFileName = "{}.annotStatsPerCluster.tab".format(join_path_file)
#     globalAnnotStatsPerTEFileName = "{}.globalAnnotStatsPerTE.txt".format(join_path_file)
#     iAnnotationStats = AnnotationStats(analysisName="TE", seqFileName=consensus_fasta_file,
#                                        classifConsensusFile =classif_consensus_file,
#                                        pathFileName=join_path_file,
#                                        genomeLength=genome_size, statsFileName=annotStatsPerTEFileName,
#                                        globalStatsFileName=globalAnnotStatsPerTEFileName, verbosity=4,
#                                        singularityImg = sglrt_img)
#     iAnnotationStats.run()
    


                    
