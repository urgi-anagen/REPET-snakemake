#!/usr/bin/env python

"""
Remove hits due to chunk overlaps.
"""

import os
import sys
import getopt
import argparse

from commons.core.coord.Align import *


class RmvPairAlignInChunkOverlaps(object):
    """
    Remove hits due to chunk overlaps.
    """

    def __init__(self, inFileName="", chunkLength=200000, chunkOverlap=10000, margin=10, outFileName="", verbose=0,logFile=""):
        """
        Constructor.
        """
        self._logFile = logFile
        if self._logFile == "":
            self._logFile = "RmvPairAlignInChunkOverlaps.log"
        self._handleLog = open(self._logFile, 'w') # c'est degeu ><

        self._inFileName = inFileName
        self._chunkLength = chunkLength
        self._chunkOverlap = chunkOverlap
        self._margin = margin
        self._verbose = verbose
        self._outFileName = outFileName
        if self._outFileName == "":
            self._outFileName = "{}.not_over".format(self._inFileName)

        if not os.path.exists(self._inFileName):
            print("ERROR: input file '{}' doesn't exist".format(self._inFileName))
            self._handleLog.write("ERROR: input file '{}' doesn't exist\n".format(self._inFileName))
            self._handleLog.close()
            #sys.exit(1)



    def isPairAlignAChunkOverlap(self, a, chunkQuery, chunkSubject):
        """
        Return True if the pairwise alignment exactly corresponds to a 2-chunk overlap, False otherwise.
        Take into account cases specific to BLASTER or PALS.
        """

        if a.range_query.isOnDirectStrand() != a.range_subject.isOnDirectStrand():
            if self._verbose > 1: self._handleLog.write("on different strand")
            return False

        matchSameSizeAsOverlap = (a.range_query.getLength() == self._chunkOverlap
                                  or a.range_query.getLength() == self._chunkOverlap + 1) \
                                 and (a.range_subject.getLength() == self._chunkOverlap
                                      or a.range_subject.getLength() == self._chunkOverlap + 1)

        if chunkQuery == chunkSubject + 1:
            if self._verbose > 1: self._handleLog.write("chunk query after chunk subject\n")
            if a.range_query.start == 1 and a.range_subject.end == self._chunkLength \
                    and matchSameSizeAsOverlap:
                if self._verbose > 1: self._handleLog.write("chunk overlap\n")
                return True

        elif chunkQuery == chunkSubject - 1:
            if self._verbose > 1: self._handleLog.write("chunk query before chunk subject\n")
            if a.range_query.end == self._chunkLength and a.range_subject.start == 1 \
                    and matchSameSizeAsOverlap:
                if self._verbose > 1: self._handleLog.write("chunk overlap\n")
                return True

        if self._verbose > 1: self._handleLog.write("match not exactly same size as chunk overlap\n")
        return False

    def isInInterval(self, coord, midpoint):
        """
        Check if a given coordinate is inside the interval [midpoint-margin;midpoint+margin].
        """
        if coord >= midpoint - self._margin and coord <= midpoint + self._margin:
            return True
        return False

    def isPairAlignWithinAndDueToAChunkOverlap(self, a, chunkQuery, chunkSubject):
        """
        Return True if the pairwise alignment lies within an overlap between two contiguous chunks and is due to it, False otherwise.
        """
        uniqLength = self._chunkLength - self._chunkOverlap

        if a.range_query.isOnDirectStrand() != a.range_subject.isOnDirectStrand():
            if self._verbose > 1: self._handleLog.write("on different strand\n")
            return False

        if chunkQuery == chunkSubject + 1:
            if self._verbose > 1: self._handleLog.write("chunk query after chunk subject\n")
            if a.range_query.getMin() >= 1 and a.range_query.getMax() <= self._chunkOverlap \
                    and a.range_subject.getMin() >= self._chunkLength - self._chunkOverlap + 1 \
                    and a.range_subject.getMax() <= self._chunkLength:
                if self._verbose > 1: self._handleLog.write("included\n")
                if self.isInInterval(a.range_query.getMin(), a.range_subject.getMin() - uniqLength) \
                        and self.isInInterval(self._chunkOverlap - a.range_query.getMax(),
                                              self._chunkLength - a.range_subject.getMax()):
                    if self._verbose > 1: self._handleLog.write("due to overlap\n")
                    return True
                else:
                    if self._verbose > 1: self._handleLog.write("not due to overlap\n")
                    return False

        elif chunkQuery == chunkSubject - 1:
            if self._verbose > 1: self._handleLog.write("chunk query before chunk subject\n")
            if a.range_query.getMin() >= self._chunkLength - self._chunkOverlap + 1 \
                    and a.range_query.getMax() <= self._chunkLength \
                    and a.range_subject.getMin() >= 1 \
                    and a.range_subject.getMax() <= self._chunkOverlap:
                if self._verbose > 1: self._handleLog.write("included\n")
                if self.isInInterval(a.range_subject.getMin(), a.range_query.getMin() - uniqLength) \
                        and self.isInInterval(self._chunkOverlap - a.range_subject.getMax(),
                                              self._chunkLength - a.range_query.getMax()):
                    if self._verbose > 1: self._handleLog.write("due to overlap\n")
                    return True
                else:
                    if self._verbose > 1: self._handleLog.write("not due to overlap\n")
                    return False

        else:
            if self._verbose > 1: self._handleLog.write("not contiguous chunks\n")
            return False

        if self._verbose > 1: self._handleLog.write("not included\n")
        return False

    def matchAlreadyExists(self, alignList, alignToCheck):

        def included(a, chunkQuery, chunkSubject):
            uniqLength = self._chunkLength - self._chunkOverlap
            if a.range_query.isOnDirectStrand() != a.range_subject.isOnDirectStrand():
                if self._verbose > 1: self._handleLog.write("on different strand\n")
                return False

            if chunkQuery >= chunkSubject:
                if self._verbose > 1: self._handleLog.write("chunk query after chunk subject\n")
                if a.range_query.getMin() >= 1 and a.range_query.getMax() <= self._chunkOverlap \
                        and a.range_subject.getMin() >= self._chunkLength - self._chunkOverlap + 1 \
                        and a.range_subject.getMax() <= self._chunkLength:
                    if self._verbose > 1: self._handleLog.write("included\n")
                    if self.isInInterval(a.range_query.getMin(), a.range_subject.getMin() - uniqLength) \
                            and self.isInInterval(self._chunkOverlap - a.range_query.getMax(),
                                                  self._chunkLength - a.range_subject.getMax()):
                        if self._verbose > 1: self._handleLog.write("due to overlap\n")
                        return True
                    else:
                        if self._verbose > 1: self._handleLog.write("not due to overlap\n")
                        return False

            elif chunkQuery <= chunkSubject:
                if self._verbose > 1: self._handleLog.write("chunk query before chunk subject\n")
                if a.range_query.getMin() >= self._chunkLength - self._chunkOverlap + 1 \
                        and a.range_query.getMax() <= self._chunkLength \
                        and a.range_subject.getMin() >= 1 \
                        and a.range_subject.getMax() <= self._chunkOverlap:
                    if self._verbose > 1: self._handleLog.write("included\n")
                    if self.isInInterval(a.range_subject.getMin(), a.range_query.getMin() - uniqLength) \
                            and self.isInInterval(self._chunkOverlap - a.range_subject.getMax(),
                                                  self._chunkLength - a.range_query.getMax()):
                        if self._verbose > 1: self._handleLog.write("due to overlap\n")
                        return True
                    else:
                        if self._verbose > 1: self._handleLog.write("not due to overlap\n")
                        return False

            else:
                if self._verbose > 1: self._handleLog.write("not contiguous chunks\n")
                return False

            if self._verbose > 1: self._handleLog.write("not included\n")
            return False
            

        for align in alignList:
            if (align == alignToCheck):
                return True
            chunkOld = int(align.range_subject.seqname.replace("chunk", ""))
            chunkNew = int(alignToCheck.range_subject.seqname.replace("chunk", ""))
            if chunkOld == chunkNew or chunkOld == chunkNew + 1 or chunkNew == chunkOld + 1:
                chunkOld = int(align.range_subject.seqname.replace("chunk", ""))
                chunkNew = int(alignToCheck.range_subject.seqname.replace("chunk", ""))
                if included(alignToCheck, chunkOld, chunkNew):
                    return True

        return False

    def convertRangeToChromCoords(self, chunkIndex, range):
        end = chunkIndex * self._chunkLength - self._chunkOverlap * (chunkIndex - 1)
        start = end - self._chunkLength

        min = start + range.getMin()
        max = start + range.getMax()
        self._handleLog.write("min", min, "max", max, "start", start, "end", end,"\n")
        return (min, max)

    def removeChunkOverlaps(self):
        """
        Remove pairwise alignments exactly corresponding to chunk overlaps or those included within such overlaps.
        """
        totalNbPairAlign = 0
        nbChunkOverlaps = 0
        dQueryToAlign = {}
        nbPairAlignWithinChunkOverlaps = 0


        with open(self._outFileName, "w") as outF,open(self._inFileName, "r") as inF:
            alignInstance = Align()

            while True:
                if not alignInstance.read(inF): break
                totalNbPairAlign += 1
                if self._verbose > 1:
                    #alignInstance.show()
                    self._handleLog.write(alignInstance.toString()+"\n")

                # if "chunk" not in alignInstance.range_query.seqname or "chunk" not in alignInstance.range_subject.seqname:
                #    print("WARNING: no 'chunk' in query or subject name")
                #    return False
                if "chunk" in alignInstance.range_query.seqname :
                    chunkQuery = int(alignInstance.range_query.seqname.strip("chunk"))
                else:
                    chunkQuery = int(alignInstance.range_query.seqname.split("_")[0])

                if "chunk" in alignInstance.range_subject.seqname:
                    chunkSubject = int(alignInstance.range_subject.seqname.strip("chunk"))
                else :
                    chunkSubject = int(alignInstance.range_subject.seqname.split("_")[0])

                if abs(chunkSubject - chunkQuery) > 1:
                    if self._verbose > 1: self._handleLog.write("non contiguous chunks -> keep\n")
                    alignInstance.write(outF)
                    continue

                if alignInstance.range_query.isOnDirectStrand() != alignInstance.range_subject.isOnDirectStrand():
                    if self._verbose > 1: self._handleLog.write("on different strand\n")
                    alignInstance.write(outF)
                    continue

                if abs(chunkSubject - chunkQuery) == 0:
                    if alignInstance.range_query.start == 1 \
                            and alignInstance.range_query.end == self._chunkLength \
                            and alignInstance.range_subject.start == 1 \
                            and alignInstance.range_subject.end == self._chunkLength:
                        if self._verbose > 1: self._handleLog.write("self-alignment on whole chunk -> remove\n")
                        continue

                if self.isPairAlignAChunkOverlap(alignInstance, chunkQuery, chunkSubject):
                    if self._verbose > 1: self._handleLog.write("chunk overlap -> remove\n")
                    nbChunkOverlaps += 1

                elif self.isPairAlignWithinAndDueToAChunkOverlap(alignInstance, chunkQuery, chunkSubject):
                    if self._verbose > 1: self._handleLog.write("within chunk overlap -> remove\n")
                    nbPairAlignWithinChunkOverlaps += 1

                else:
                    if self._verbose > 1: self._handleLog.write("keep\n")
                    alignInstance.write(outF)


            if self._verbose > 0: self._handleLog.write("nb of pairwise alignments in input file: {}\n".format(totalNbPairAlign))
            if self._verbose > 0: self._handleLog.write("nb of chunk overlaps: {}\n".format(nbChunkOverlaps))
            if self._verbose > 0: self._handleLog.write(
                "nb of pairwise alignments within chunk overlaps: {}\n".format(nbPairAlignWithinChunkOverlaps))

            for names, lAligns in dQueryToAlign.items():
                for alignInstance in lAligns:
                    alignInstance.write(outF)



    def start(self):
        """
        Useful commands before running the program.
        """
        if self._verbose > 0:
            self._handleLog.write("START {}\n".format(type(self).__name__))
            sys.stdout.flush()
        #self.checkAttributes()

    def end(self):
        """
        Useful commands before ending the program.
        """
        if self._verbose > 0:
            self._handleLog.write("END {}\n".format(type(self).__name__))
            self._handleLog.close()
            sys.stdout.flush()

    def run(self):
        """
        Run the program.
        """
        self.start()
        self.removeChunkOverlaps()
        self.end()


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Remove hits due to chunk overlaps.',
                                     formatter_class=argparse.RawTextHelpFormatter, add_help=True)
    parser.add_argument('-i', '--input', required=True, help="name of the input file (format='align')")
    parser.add_argument('-l', '--length', type=int, default=200000,
                        help="chunk length (in bp)")
    parser.add_argument('-o', '--overlap', type=int, default=10000,
                        help="chunk overlap (in bp)")
    parser.add_argument('-m', '--margin', type=int, default=10,
                        help="margin to remove match included into a chunk overlap (default=10)")
    parser.add_argument('-O', '--output', default="",
                        help="name of the output file (default=inFileName+'.not_over')")
    parser.add_argument('-v', '--verbose', default=0, type=int, help="verbose (default=0/1)")

    args = parser.parse_args()
    inFileName = args.input
    chunkLength = args.length
    chunkOverlap = args.overlap
    margin = args.margin
    outFileName = args.output
    verbose = args.verbose

    i = RmvPairAlignInChunkOverlaps(inFileName, chunkLength, chunkOverlap, margin, outFileName, verbose)
    i.run()
