#!/usr/bin/env python

# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import os
import subprocess
import shutil
from commons.core.utils.FileUtils import FileUtils
from commons.core.LoggerFactory import LoggerFactory
from commons.core.utils.RepetOptionParser import RepetOptionParser
from commons.core.checker.CheckerUtils import CheckerUtils
from commons.core.checker.CheckerException import CheckerException
from commons.core.seq.BioseqUtils import BioseqUtils
from commons.tools.GiveInfoFasta import GiveInfoFasta
from commons.core.seq.FastaUtils import FastaUtils
from collections import OrderedDict
from commons.launcher import LaunchTRF
from commons.tools.SpliceTEsFromGenome import SpliceTEsFromGenome


LOG_DEPTH = "repet.commons.tools"
LOG_FORMAT = "%(asctime)s - %(levelname)s - %(message)s"


class PreProcess(object):
    def __init__(self, fastaFileName="", outputFasta="", step=1, word=11, maxPeriod = 15, length=100, substep3=1, InfoForStep3=15000,
                 verbosity=3, log_name ="PreProcess.log",singularity_img=""):
        self._fastaFileName = fastaFileName
        self._step = step
        self._outputFasta = outputFasta
        self._verbosity = verbosity
        self._word = word
        self._maxPeriod = maxPeriod
        self._length = length
        self._substep3 = substep3
        self._InfoForStep3 = InfoForStep3
        self._singularityImg = singularity_img
        #self._log = LoggerFactory.createLogger("{}.{}".format(LOG_DEPTH, self.__class__.__name__), self._verbosity, LOG_FORMAT)
        self._base_dir = os.getcwd()
        self._log = LoggerFactory.createLogger("{}.{}".format((LOG_DEPTH, self.__class__.__name__), self._verbosity),
                                               log_name)

    def setAttributesFromCmdLine(self):
        usage = "PreProcess.py [options]"
        description = "Tool to format a fasta file as input of REPET pipelines\n"
        description += "step 1 : format headers, nucleic sequences, formatting column, metrics calculation, remove N stretchs\n"
        description += "step 2 : launch TRF to mask SSR, remove N stretchs, metrics calculation.\n"
        description += "step 3 : extraction of genome sub-set.\n"
        description += "    substep 3-1 : minimal size of sequences kept in subset genome.\n"
        description += "    substep 3-2 : number of longest sequences.\n"
        description += "    substep 3-3 : genome subset size defined with longest sequences.\n"
        epilog = "\nExample 1: fasta file checking\n"
        epilog += "\t$ PreProcess.py -S 1 -i input.fa -v 3\n"
        epilog += "Example 2: mask SSR with a max periode of 20nt and a max length of 200 nt\n"
        epilog += "\t$ PreProcess.py -S 2 -i input.fa -m 20 -L 200\n"
        epilog += "Example 3: fasta file checking and genome sub-set with sequences longest than 16000nt and metrics\n"
        epilog += "\t$ PreProcess.py -S 13 -i input.fa -I 16000\n"
        epilog += "Example 4: genome sub-set with sequences longest than 16000nt and metrics\n"
        epilog += "\t$ PreProcess.py -S 3 -i input.fa -I 16000\n"
        epilog += "Example 5: genome sub-set with 300 longest sequences and metrics\n"
        epilog += "\t$ PreProcess.py -S 3 -i input.fa -s 2 -I 300\n"
        epilog += "Example 6: genome sub-set of 300000000 nt the longest sequences and metrics\n"
        epilog += "\t$ PreProcess.py -S 3 -i input.fa -s 3 -I 300000000\n"

        parser = RepetOptionParser(description=description, epilog=epilog, usage=usage)
        parser.add_option("-S", "--step", dest="step", action="store", type="int",
                          help="step number [values: 1, 2, 3] [default: 1]", default=1)
        parser.add_option("-i", "--fasta", dest="fastaFileName", action="store", type="string",
                          help="input fasta file name [mandatory][format: fasta]", default="")
        parser.add_option("-o", "--formated", dest="outputFasta", action="store", type="string",
                          help="output formated fasta file name [optional] [format: fasta]", default="")
        parser.add_option("-w", "--word", dest="word", action="store", type="int",
                          help="size of N stretch to cut sequences in formated fasta file [optional][default: 11]",
                          default=11)
        parser.add_option("-m", "--maxPeriod", dest = "maxPeriod", action = "store", type = "int",
                          help = "maximum period size to report with TRF [default: 6]", default = 6)
        parser.add_option("-L", "--length", dest="length", action="store", type="int",
                          help="size of SSR to mask in TRF.set fasta file [optional][default: 100]", default=100)
        parser.add_option("-s", "--substep3", dest="substep3", action="store", type="int",
                          help="substep3 number [values: 1, 2, 3] [default: 1]", default=1)
        parser.add_option("-I", "--InfoForStep3", dest="InfoForStep3", action="store", type="int",
                          help="size of the shortest sequence keeped [default: 15K]", default=15000)
        parser.add_option("-v", "--verbosity", dest="verbosity", action="store", type="int",
                          help="verbosity [optional] [default: 3]", default=3)
        options = parser.parse_args()[0]
        self._setAttributesFromOptions(options)

    def _setAttributesFromOptions(self, options):
        self.setFastaFileName(options.fastaFileName)
        self.setoutputFasta(options.outputFasta)
        self.setStep(options.step)
        self.setWord(options.word)
        self.setMaxPeriod(options.maxPeriod)
        self.setLength(options.length)
        self.setsubstep3(options.substep3)
        self.setInfoForStep3(options.InfoForStep3)
        self.setVerbosity(options.verbosity)

    def setFastaFileName(self, filename):
        self._fastaFileName = filename

    def setoutputFasta(self, filename):
        self._outputFasta = filename

    def setStep(self, step):
        self._step = step

    def setWord(self, length):
        self._word = length

    def setMaxPeriod(self, max_period):
        self._maxPeriod = max_period

    def setLength(self, length):
        self._length = length

    def setsubstep3(self, typeSubSet):
        self._substep3 = typeSubSet

    def setInfoForStep3(self, length):
        self._InfoForStep3 = length

    def setVerbosity(self, verbosity):
        self._verbosity = verbosity

    def _checkOptions(self):
        if self._step not in [1, 2, 3, 12, 13, 123]:
            self._logAndRaise("ERROR: Unexpected step number")

        if self._substep3 not in [1, 2, 3]:
            self._logAndRaise("ERROR: Unexpected substep3 number")

        if self._fastaFileName == "":
            self._logAndRaise("ERROR: Missing fasta file")

        if self._outputFasta == "":
            if self._step == 1 or self._step == 12:
                self._outputFasta = "{}_formated.fa".format(self._fastaFileName)
            if self._step == 2:
                self._outputFasta = "{}_subset.fa".format(self._fastaFileName)

        if not FileUtils.isRessourceExists(self._fastaFileName):
            self._logAndRaise("ERROR: fasta file '{}' doesn't exist".format(self._fastaFileName))

    def _logAndRaise(self, errorMsg):
        self._log.error(errorMsg)
        raise Exception(errorMsg)

    def checkFastaFile(self,fastaFileName):
        with open(fastaFileName, "r") as inGenomeFileHandler:
            listeError = list()
            lWrongHeader = list()
            try:
                CheckerUtils.checkHeaders(inGenomeFileHandler, listeError)
            except CheckerException as e:
                text = "Error in file {}\n".format(self._fastaFileName)
                text += "Wrong headers are {}\n".format(e.getMessages())
                text += "Authorized characters are : a-z A-Z 0-9 - . : _\n"
                text += "Forbidden characters {} will be deleted\n".format(listeError)
                text += "Formatting wrongs headers in progress"
                self._log.error(text)
                lWrongHeader = e.getMessages()
                pass

            inGenomeFileHandler.close()
            self._log.info("Start file treatment on {}".format(self._fastaFileName))
            iGIF = GiveInfoFasta(fastaFileName, verbose=self._verbosity)
            iGIF.run()
            nbOtherNt = iGIF.getnbOtherNt()

        if nbOtherNt > 0:
            self._log.info("WARNING!!! Your fasta file contains {} characters not in [A, T, G, C, N]".format(nbOtherNt))

        self._log.info(
            "Formatting of {} to get sequences with only ATGCN and 60bases column width in {}.formated".format(
                self._fastaFileName, fastaFileName))
        lBioSeq = BioseqUtils.extractBioseqListFromFastaFile(fastaFileName)
        with open("{}.formated".format(fastaFileName), "w") as formatedFasta:
            if lWrongHeader != []:
                lWrongHeader = map(lambda s: s.rstrip("\t\n\r"), lWrongHeader)
            for itemBioSeq in lBioSeq:
                length = itemBioSeq.getLength()
                header = itemBioSeq.getHeader()
                if not length == 0:
                    if lWrongHeader != [] and header in lWrongHeader:
                        for i in listeError:
                            header = header.replace(i, "")

                    sequence = itemBioSeq.getSeqWithOnlyATGCNStrict()

                    formatedFasta.write(">{}\n".format(header))
                    for i in range(0, length, 60):
                        formatedFasta.write("{}\n".format(sequence[i:i + 60]))
            formatedFasta.close()

        self._log.info("End formatting {} in {}.formated".format(self._fastaFileName, fastaFileName))

        if nbOtherNt > 0:
            self._log.info("Start metrics analyses on {}.formated".format(self._fastaFileName))
            iGIF = GiveInfoFasta("{}.formated".format(fastaFileName), verbose=self._verbosity)
            iGIF.run()
        self._log.info(
            "Start genome spliting from Nstretch > {} on {}.formated".format(self._word, fastaFileName))
        lengthmax = max(FastaUtils.dbLengths("{}.formated".format(fastaFileName))) + 1
        FastaUtils.dbChunks(inFileName="{}.formated".format(fastaFileName), chkLgth=lengthmax, chkOver='0', wordN=self._word,
                            outFilePrefix="{}_chunks".format(fastaFileName), verbose=0,singularity_img=self._singularityImg)
        self._log.info("Start metrics analyses on {}_chunks.fa".format(fastaFileName))
        iGIF = GiveInfoFasta("{}_chunks.fa".format(fastaFileName), verbose=self._verbosity)
        iGIF.run()

    def lowComplexityMask(self):
        fasta_file = ""
        if self._step == 2:
            self._log.info("If your fasta file is not verified by PreProcess step 1 it could contain some errors")
            fasta_file = self._fastaFileName
        if self._step == 12 or self._step == 123:
            fasta_file = "{}_chunks.fa".format(self._fastaFileName)

        ltrf = LaunchTRF.LaunchTRF(inFileName=fasta_file, outFileName="", maxPeriod=self._maxPeriod, doClean=False, verbosity=3)
        ltrf.run()
        cmd = "awk '{{if ($5-$4 > {}) print  $0}}' {} > {}.filtered".format(self._length, fasta_file + ".TRF.set", fasta_file + ".TRF.set")
        subprocess.call(cmd, shell=True)

        tmp = '{print $1"\t"$3"\t"$4"\t"$5"\t"}'
        cmd2 = "awk '{}' {}.filtered > {}.formated".format (tmp, fasta_file + ".TRF.set", fasta_file + ".TRF.set")
        subprocess.call(cmd2, shell=True)

        iSTEFG = SpliceTEsFromGenome(inputData=fasta_file+".TRF.set.formated", formatData="map", genomeFile=fasta_file, verbose=3)
        iSTEFG.run()

        self._log.info("Start metrics analyses on {}.splice".format(fasta_file))
        iGIF = GiveInfoFasta("{}.splice".format(fasta_file), verbose=self._verbosity)
        iGIF.run()

    def subsetGenome(self,fastaFileName=""):

        if self._step == 3:
            self._log.info("If your fasta file is not verified by PreProcess step 1 it could contain some errors")
            inFileName = fastaFileName

        if self._step == 13 or self._step == 123:
            inFileName = "{}_chunks.fa".format(fastaFileName)

        nbSeq = FastaUtils.dbSize(inFileName) + 1

        if self._substep3 == 1:
            self._log.info(
                "Establishment of a subset with sequence(s) with a minimum length of {} nt".format(self._InfoForStep3))
            FastaUtils.dbLongestSequences(nbSeq, inFileName, "{}_subset".format(inFileName), self._verbosity - 2,
                                          self._InfoForStep3)

            self._log.info("Start metrics analyses on {}_subset".format(inFileName))
            iGIF = GiveInfoFasta("{}_subset".format(inFileName), verbose=self._verbosity)
            iGIF.run()

        if self._substep3 == 2:
            self._log.info("Establishment of a subset with {} longest sequence(s)".format(self._InfoForStep3))
            FastaUtils.dbLongestSequences(self._InfoForStep3, inFileName, "{}_subset".format(inFileName),
                                          self._verbosity - 2, 0)

            self._log.info("Start metrics analyses on {}_subset".format(inFileName))
            iGIF = GiveInfoFasta("{}_subset".format(inFileName), verbose=self._verbosity)
            iGIF.run()

        if self._substep3 == 3:
            self._log.info("Establishment of a subset of {} nt with the longest sequence(s)".format(self._InfoForStep3))
            cutoff = self._InfoForStep3
            d1 = FastaUtils.getLengthPerHeader(inFileName, self._verbosity)
            genomeSize = 0
            for val in d1.values():
                genomeSize += val
            if genomeSize < cutoff:
                self._log.info("The size of the subset is higher than your input sequences : {} nt".format(genomeSize))
            else:
                cumul = 0
                minSizeSupCutOff = 0
                minSizeInfCutOff = 0
                d2 = OrderedDict(sorted(d1.items(), key=lambda t: t[1], reverse=True))
                longestSequence = list(d2.values())[0]

                if longestSequence > cutoff:
                    FastaUtils.dbLongestSequences(nbSeq, inFileName, "{}_subset".format(inFileName),
                                                  self._verbosity - 2, longestSequence)
                else:
                    for val in d2.values():
                        cumul += val
                        if cutoff < cumul:
                            minSizeSupCutOff = val
                            break
                        else:
                            minSizeInfCutOff = val

                    if (cumul - cutoff) < (cutoff - (cumul - minSizeInfCutOff)):
                        FastaUtils.dbLongestSequences(nbSeq, inFileName, "{}_subset".format(inFileName),
                                                      self._verbosity - 2, minSizeSupCutOff)
                    else:
                        FastaUtils.dbLongestSequences(nbSeq, inFileName, "{}_subset".format(inFileName),
                                                      self._verbosity - 2, minSizeInfCutOff)

                self._log.info("Start metrics analyses on {}_subset".format(inFileName))
                iGIF = GiveInfoFasta("{}_subset".format(inFileName), verbose=self._verbosity)
                iGIF.run()

    def run(self,dirStep = ""):
        LoggerFactory.setLevel(self._log, self._verbosity)
        self._checkOptions()
        self._log.info("START PreProcess on {}".format(self._fastaFileName))
        if dirStep =="" : 
            dirStep = self._fastaFileName.split(".fa")[0]

        if self._step == 1 or self._step == 12 or self._step == 123 or self._step == 13:
            self._log.info(
                "Start step 1 : check headers, nucleic sequences, formatting column, remove N stretchs, metrics calculation in '{}_CheckFasta' directory\n ".format(
                    dirStep))
            if os.path.exists('{}_CheckFasta'.format(dirStep)) : 
                shutil.rmtree('{}_CheckFasta'.format(dirStep))
            os.mkdir('{}_CheckFasta'.format(dirStep))
            os.chdir('{}_CheckFasta'.format(dirStep))
            os.symlink("{}".format(self._fastaFileName), os.path.split(self._fastaFileName)[1])
            fasta_to_check = os.path.join(os.getcwd(),os.path.split(self._fastaFileName)[1])
            self.checkFastaFile(fasta_to_check)
            os.chdir("..")
            
           # pour output snakemake
            if self._step == 1 and os.path.exists("{}_CheckFasta".format(dirStep)): 
                fasta_to_check = os.path.split(self._fastaFileName)[1]
                shutil.copy("{}_CheckFasta/{}.formated".format(dirStep,fasta_to_check),self._outputFasta)
                self._log.info("The outputfile is {}_CheckFasta/{}.formated".format(dirStep,fasta_to_check))
        
            self._log.info("End of step 1.")

        # TODO, je n'ai pas fait les modif necessaire pour prendre en compre path absolue
        if self._step == 2 or self._step == 12 or self._step == 123:
            self._log.info(
                "Start step 2 : mask SSR, remove N stretchs, metrics calculation in '{}_maskSSR' directory\n ".format(
                    dirStep))
            os.mkdir('{}_maskSSR'.format(dirStep))
            os.chdir('{}_maskSSR'.format(dirStep))
            if self._step == 12 or self._step == 123:
                if os.path.exists("../{}_CheckFasta/{}_chunks.fa".format(dirStep, self._fastaFileName)):
                    os.symlink("../{}_CheckFasta/{}_chunks.fa".format(dirStep, self._fastaFileName), "{}_chunks.fa".format(self._fastaFileName))
                    self.lowComplexityMask()
                else:
                    self._log.info("Warning: the outputfile of step 1 is unavailable!!!")

            if self._step == 2:
                os.symlink("../{}".format(self._fastaFileName), self._fastaFileName)
                self.lowComplexityMask()
            os.chdir("..")
            self._log.info("End of step 2.")

        if self._step == 3 or self._step == 23 or self._step == 123 or self._step == 13:
            self._log.info("Start step 3 : extraction of genome sub-set in '{}_SubSet' directory\n ".format(dirStep))
            if os.path.exists('{}_SubSet'.format(dirStep)) : 
                shutil.rmtree('{}_SubSet'.format(dirStep))

            os.mkdir('{}_SubSet'.format(dirStep))
            os.chdir('{}_SubSet'.format(dirStep))

            if self._step == 23 or self._step == 123:
                if os.path.exists("../{}_maskSSR/{}_chunks.fa".format(dirStep, self._fastaFileName)):
                    os.symlink("../{}_maskSSR/{}_chunks.fa".format(dirStep, self._fastaFileName),
                               "{}_chunks.fa".format(self._fastaFileName))
                    self.subsetGenome()
                else:
                    self._log.info("Warning: the outputfile of step 2 is unavailable!!!")

            if self._step == 13:
                if os.path.exists("{}_CheckFasta".format(dirStep, os.path.split(self._fastaFileName)[1])):
                    os.symlink("{}_CheckFasta/{}_chunks.fa".format(dirStep, os.path.split(self._fastaFileName)[1]),
                               "{}_chunks.fa".format(os.path.split(self._fastaFileName)[1]))
                    self.subsetGenome(os.path.join(os.getcwd(),os.path.split(self._fastaFileName)[1]))
                else:
                    self._log.info("Warning: the outputfile of step 1 is unavailable!!!")

            if self._step == 3:
                os.symlink("../{}".format(self._fastaFileName), self._fastaFileName)
                self.subsetGenome()
            os.chdir("..")
            
            # pour output snakemake
            if os.path.exists("{}_SubSet".format(dirStep)) and self._step == 13 : # or self._step == 123:
                fasta_to_check = os.path.split(self._fastaFileName)[1]
                inFileName = "{}_chunks.fa".format(fasta_to_check)
                subset_file = "{}_SubSet/{}_subset".format(dirStep,inFileName)

                if os.path.exists(subset_file):
                    #print(subset_file, self._outputFasta)
                    shutil.copy(subset_file, self._outputFasta)
                    self._log.info("The outputfile is {}".format(subset_file))
                else : 
                    shutil.copy("{}_CheckFasta/{}.formated".format(dirStep,fasta_to_check),self._outputFasta)
                    self._log.info("The outputfile is {}_CheckFasta/{}.formated".format(dirStep,fasta_to_check))
        
            self._log.info("End of step 3.")
        os.chdir(self._base_dir)
        self._log.info("END PreProcess")


if __name__ == "__main__":
#    simg="/home/centos/REPET-snakemake/Singularity/te_finder_2.30.2.sif"
#    p = PreProcess(fastaFileName="/mnt/volume_data_mariene/REPET-snakemake/data/DmelChr4.fa",outputFasta="/home/centos/REPET-snakemake/TEdenovo_snakemake/results_DmelChr4/DmelChr4_preprocess_output.fa",step=13, substep3=3, InfoForStep3=1600, verbosity=3,singularity_img = simg)
#    p.run(dirStep = "/home/centos/REPET-snakemake/TEdenovo_snakemake/results_DmelChr4/DmelChr4")
    iPP = PreProcess()
    iPP.setAttributesFromCmdLine()
    iPP.run()



