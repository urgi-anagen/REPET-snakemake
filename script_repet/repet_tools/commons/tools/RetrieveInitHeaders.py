#! /usr/bin/env python

# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

from optparse import OptionParser
from commons.core.sql.DbMySql import DbMySql

class RetrieveInitHeaders(object):

    def __init__(self, inTableName = "", linkFileName = "", outTableName = "", isQueryHeaders = True, clean = False, verbose = 0):
        self._inTableName = inTableName
        self._linkFileName = linkFileName
        if outTableName == "":
            self._outTableName = self._inTableName
        else:
            self._outTableName = outTableName
        self._isQueryHeaders = isQueryHeaders
        self._clean = clean
        self._verbose = verbose
        self._iDb = None
        self._tmpTableName = "{}_tmp".format(self._inTableName)

    #TODO: can handle config file
    #TODO: description, help...
    def setAttributesFromCmdLine(self):
        usage = "RetrieveInitHeaders.py [ options ]"
        description = "Rename sequences shorted header with their initial header"
        parser = OptionParser(description = description, usage=usage)
        parser.add_option("-i", "--input", dest = "inTableName", type = "string", help = "", default = "")
        parser.add_option("-l", "--link", dest = "linkFileName", type = "string", help = "", default = "")
        parser.add_option("-o", "--output", dest = "outTableName", type = "string", help = "(default = input table name)", default = "")
        parser.add_option("-s", "--subject", dest = "isQueryHeaders", action = "store_false", help = "change subject name and not query name", default = True)
        parser.add_option("-c", "--clean", dest = "clean", action = "store_true", help = "drop input table", default = False)
        parser.add_option("-v", "--verbose", dest = "verbose", type = "int", help = "0 or 1", default = 0)
        options, args = parser.parse_args()
        self.setAttributesFromOptions(options)
        
    def setAttributesFromOptions(self, options):
        self.setInTableName(options.inTableName)
        self.setLinkFileName(options.linkFileName)
        self.setOutTableName(options.outTableName)
        self.setIsQueryHeaders(options.isQueryHeaders)
        self.setClean(options.clean)
        self.setVerbose(options.verbose)
        
    def setInTableName(self, inTableName):
        self._inTableName = inTableName
        self._tmpTableName = "{}_tmp".format(self._inTableName)
        
    def setLinkFileName(self, linkFileName):
        self._linkFileName = linkFileName
        
    def setOutTableName(self, outTableName):
        if outTableName == "":
            self._outTableName = self._inTableName
        else:
            self._outTableName = outTableName
        
    def setIsQueryHeaders(self, isQueryHeaders):
        self._isQueryHeaders = isQueryHeaders
        
    def setClean(self, clean):
        self._clean = clean
        
    def setVerbose(self, verbose):
        self._verbose = verbose
        
    #TODO: checkOptions
    def checkOptions(self):
        pass    
    
    def run(self):
        if self._verbose > 0:
            print("START RetrieveInitHeaders.py")
        self.checkOptions()
        
        if self._verbose > 0:
            print("copy '{}' table to '{}' table".format(self._inTableName, self._tmpTableName))
        self._iDb = DbMySql()
        self._iDb.copyTable(self._inTableName, self._tmpTableName)
        
        if self._verbose > 0:
            print("read '{}' file".format(self._linkFileName))
        f = open(self._linkFileName)
        line = f.readline()
        count = 0
        while line:
            oldHeader = line.split()[0]
            newHeader = line.split()[1]
            if self._isQueryHeaders:
                self._updateQueryName(oldHeader, newHeader)
            else:
                self._updateSubjectName(oldHeader, newHeader)
            count += 1
            line = f.readline()
        f.close()
        
        if self._verbose > 0:
            print("nb of relationships: {:d}".format(count))
        if self._clean:
            self._iDb.dropTable(self._inTableName)
            if self._verbose > 0:
                print("drop '{}' table".format(self._inTableName))
        if self._verbose > 0:
            print("rename '{}' table to '{}' table".format(self._tmpTableName, self._outTableName))
        self._iDb.renameTable(self._tmpTableName, self._outTableName)
        self._iDb.close()
        if self._verbose > 0:
            print("END RetrieveInitHeaders.py")

    #TODO: methods must be in TablePathAdaptator ?
    def _updateQueryName(self, oldH, newH):
        sqlCmd = "UPDATE {} SET query_name = '{}' WHERE query_name = '{}'".format(self._tmpTableName, newH, oldH)
        self._iDb.execute(sqlCmd)
        
    def _updateSubjectName(self, oldH, newH):
        sqlCmd = "UPDATE {} SET subject_name = '{}' WHERE subject_name = '{}'".format(self._tmpTableName, newH, oldH)
        self._iDb.execute(sqlCmd)
    
if __name__ == "__main__":
    iRIH = RetrieveInitHeaders()
    iRIH.setAttributesFromCmdLine()
    iRIH.run()