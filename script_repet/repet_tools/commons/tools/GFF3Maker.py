#!/usr/bin/env python

##@file GFF3Maker.py

# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
import re
import sys
import os
import pandas
from commons.core.utils.RepetOptionParser import RepetOptionParser
from commons.core.utils.FileUtils import FileUtils
from commons.core.LoggerFactory import LoggerFactory
from commons.core.seq.Bioseq import Bioseq
from commons.core.seq.FastaUtils import FastaUtils
from commons.core.coord.Path import Path

LOG_DEPTH = "commons.tools"


# LOG_FORMAT = "%(message)s"


## GFF3Maker exports annotations from a 'path' table into a GFF3 file.
#
class GFF3Maker(object):

    def __init__(self, inFastaName="", joinPathFile="", classifFile="", isChado=False,
                 isGFF3WithoutAnnotation=False, isWithSequence=False, areMatchPartsCompulsory=False,
                 verbose=0, doMergeIdenticalMatches=False, doSplit=False, alignInformationsFile="",
                 logName="GFF3Maker.log", projectDir=""):
        self._input_fasta_name = inFastaName
        self._join_path_file = joinPathFile
        self._classif_file_name = classifFile
        self._nb_col_in_classif = 0
        self._is_chado = isChado
        self._is_gff3_without_annotation = isGFF3WithoutAnnotation
        self._is_with_sequence = isWithSequence
        self._are_match_parts_compulsory = areMatchPartsCompulsory

        self._project_dir = projectDir

        self._do_merge_identical_matches = doMergeIdenticalMatches
        self._do_split = doSplit

        self._align_informations_file_name = alignInformationsFile
        self._verbose = verbose
        self._has_align_infos = True
        self._log = LoggerFactory.createLogger("{}.{}".format((LOG_DEPTH, self.__class__.__name__), self._verbose),
                                               logName)

        self._genome_fasta = None
        self._df_join_path = pandas.DataFrame()
        self._df_classif = pandas.DataFrame()
        self._df_align_tab = pandas.DataFrame()

    def _log_and_raise(self, errorMsg):
        self._log.error(errorMsg)
        raise Exception(errorMsg)

    def set_attributes_from_cmd_line(self):
        usage = "GFF3Maker.py [options]"
        description = "GFF3Maker exports annotations from 'path', 'set' and/or 'classif' tables into a GFF3 file\n"

        parser = RepetOptionParser(description=description, usage=usage)
        parser.add_option("-f", "--inseq", dest="inFastaName", action="store", type="string",
                          help="Genome fasta file ", default="")
        parser.add_option("-t", "--pathFile", dest="pathFile", action="store", type="string",
                          help="Path file in tsv format",
                          default="")
        parser.add_option("-w", "--withSequence", dest="isWithSequence", action="store_true",
                          help="write the sequence at the end of GFF3 file", default=False)
        parser.add_option("-a", "--withoutAnnotation", dest="isGFF3WithoutAnnotation", action="store_true",
                          help="write GFF3 files even if no annotation", default=False)
        parser.add_option("-p", "--matchPart", dest="areMatchPartsCompulsory", action="store_true",
                          help="always associate a match_part to a match", default=False)
        parser.add_option("-i", "--classifFile", dest="classifFile", action="store", type="string",
                          help="name of the TE library classification file [optional]", default="")
        parser.add_option("-c", "--chado", dest="isChado", action="store_true", help="Chado compliance", default=False)
        parser.add_option("-m", "--doMergeIdenticalMatches", dest="doMergeIdenticalMatches", action="store_true",
                          help="merge identical matches based on query start, query end, score", default=False)
        parser.add_option("-s", "--doSplit", dest="doSplit", action="store_true",
                          help="split each GFF3 per annotation type", default=False)
        parser.add_option("-A", "--alignInfos", dest="alignInformationsFileName", action="store", type="string",
                          help="tabulated alignment informations file obtained in step 7", default="")
        parser.add_option("-v", "--verbose", dest="verbose", action="store", type="int",
                          help="verbosity level (default=0, else 1 or 2)", default=0)
        options = parser.parse_args()[0]
        self._set_attributes_from_options(options)

    # TODO: write a "setAttributesFromConfig"
    def _set_attributes_from_options(self, options):
        self.set_fasta_file(options.inFastaName)
        self.set_path_file_name(options.pathFile)
        self.set_classif_table(options.classifFile)
        self.set_is_chado(options.isChado)
        self.set_do_merge_identical_matches(options.doMergeIdenticalMatches)
        self.set_is_with_sequence(options.isWithSequence)
        self.set_is_gff3_without_annotation(options.isGFF3WithoutAnnotation)
        self.set_are_match_part_compulsory(options.areMatchPartsCompulsory)
        self.set_do_split(options.doSplit)

        self.set_align_informations_file_name(options.alignInformationsFileName)
        self.set_verbose(options.verbose)

    def set_fasta_file(self, inFastaName):
        self._input_fasta_name = inFastaName

    def set_path_file_name(self, tablesFileName):
        self._join_path_file = tablesFileName

    def set_is_with_sequence(self, isWithSequence):
        self._is_with_sequence = isWithSequence

    def set_is_gff3_without_annotation(self, isGFF3WithoutAnnotation):
        self._is_gff3_without_annotation = isGFF3WithoutAnnotation

    def set_are_match_part_compulsory(self, areMatchPartsCompulsory):
        self._are_match_parts_compulsory = areMatchPartsCompulsory

    def set_classif_table(self, classifTable):
        self._classif_file_name = classifTable

    def set_is_chado(self, isChado):
        self._is_chado = isChado

    def set_do_merge_identical_matches(self, doMergeIdenticalMatches):
        self._do_merge_identical_matches = doMergeIdenticalMatches

    def set_do_split(self, doSplit):
        self._do_split = doSplit

    def set_align_informations_file_name(self, fileName):
        self._align_informations_file_name = fileName

    def set_verbose(self, verbose):
        self._verbose = verbose

    def check_options(self):
        # fasta
        if not self._input_fasta_name:
            self._log_and_raise("ERROR: options -f required")
        else:
            self._genome_fasta = FastaUtils.read_fasta(self._input_fasta_name)

        # path
        if not os.path.exists(self._join_path_file):
            self._log_and_raise("ERROR: {} file doesn't exist!".format(self._join_path_file))
        else:
            self._df_join_path = pandas.read_csv(self._join_path_file, sep='\t', header=None, index_col=0,
                                                 low_memory=False)

        # classif
        if os.path.exists(self._classif_file_name):
            self._df_classif = pandas.read_csv(self._classif_file_name, sep='\t', index_col=0, low_memory=False)
            nbColInClassif = len(self._df_classif) + 1
            if nbColInClassif == 8 or nbColInClassif == 12:
                self._nb_col_in_classif = nbColInClassif
            else:
                self._log_and_raise(
                    "ERROR: classification table '{}' hasn't the good format!".format(self._classif_file_name))
        else:
            self._log.info("No classification information in GFF3 file!")

        if not os.path.exists(self._align_informations_file_name):
            self._has_align_infos = False
            self._log.info(
                "Tabulated alignment informations file '{}' does not exist!, the 'alignIdentity' and 'alignLength' attributes wont be written in GFF3 file".format(
                    self._align_informations_file_name))
        else:
            self._df_align_tab = pandas.read_csv(self._align_informations_file_name, sep='\t', index_col=0,
                                                 low_memory=False)

        if self._project_dir == "":
            self._project_dir = os.path.join(os.path.split(self._input_fasta_name)[0], "GFF3_chr")

        self._log.info("GFF3 files will be found in this directory : {}".format( self._project_dir))
        if not os.path.exists(self._project_dir):
            os.mkdir(self._project_dir)
        else:
            pass

        # Je sais pas a quoi ca sert
        # projectName = self._input_fasta_name.split("_chr_seq")[0]
        # self._dSource2AlignInfos["{}_REPET_SSRs".format(projectName)] = False
        # self._dSource2AlignInfos["{}_REPET_tblastx".format(projectName)] = False
        # self._dSource2AlignInfos["{}_REPET_tblasx".format(projectName)] = False
        # self._dSource2AlignInfos["{}_REPET_TEs".format(projectName)] = True

    ## Retrieve the features to write in the GFF3 file.
    #
    # @param pathTable string name of the table recording the annotations (i.e. the features)
    # @param seqName string name of the sequence (the source feature) on which we want to visualize the matches (the features)
    # @param source string the program that generated the feature (i.e. REPET)
    # @param frame string "." by default (or "+", "-")
    # @return pathString string which will be printed in path file 
    #
    def _get_path_features(self, seqName, feature, frame):
        pathString = ""
        # iTPA = TablePathAdaptator(self._iDB, pathTable)
        # lPaths = iTPA.getPathListSortedByQueryCoordAndScoreFromQuery(seqName)
        sub_df = self._df_join_path.loc[self._df_join_path[1] == seqName]
        sub_df_sorted = sub_df.sort_values(by=[2, 3, 8, 9], ascending=True)
        lPaths = list()
        vect_df = sub_df_sorted.values
        list_id = sub_df_sorted.index
        for i in range(len(list_id)):
            row = vect_df[i]
            index = list_id[i]
        #for index, row in sub_df_sorted.iterrows():
            p = Path()
            p.setFromTuple([index] + list(row))
            lPaths.append(p)
        # organise them into 'match' and 'match_part'
        self._log.debug("In _get_path_features lPath:")
        self._log.debug(lPaths)

        if lPaths:
            dPathID2Data = self._gather_same_path_features(lPaths)
            # build the output string
            self._log.debug("In _get_path_features pathString:")
            for pathID in dPathID2Data:
                pathString += self._organize_each_path_feature(pathID, dPathID2Data[pathID], seqName, frame)
            self._log.debug(pathString)
        return pathString

    ## Gather matches with the same path ID.
    #
    # if a path contains info in otherTarget attribut, the information subjectName, subjectStart, subjectEnd is added
    # @param list of string lists results of a SQL request
    # @return dPathID2Matchs dict whose keys are path IDs and values are matches data
    #
    def _gather_same_path_features(self, lPaths):
        dPathID2Matchs = {}
        iPreviousPath = lPaths[0]
        iPreviousPath.otherTargets = []
        self._log.debug("In _gather_same_path_features dPathID2Matchs[]:")
        for iPath in lPaths[1:]:
            if self._do_merge_identical_matches and iPreviousPath.getQueryStart() == iPath.getQueryStart() and iPreviousPath.getQueryEnd() == iPath.getQueryEnd() and iPreviousPath.getScore() == iPath.getScore() and iPreviousPath.getSubjectName() != iPath.getSubjectName():
                iPreviousPath.otherTargets.append(
                    "{} {} {}".format(iPath.getSubjectName(), iPath.getSubjectStart(), iPath.getSubjectEnd()))
            else:
                self._add_path_to_match_dict(dPathID2Matchs, iPreviousPath)
                iPreviousPath = iPath
                iPreviousPath.otherTargets = []
        self._add_path_to_match_dict(dPathID2Matchs, iPreviousPath)
        self._log.debug(dPathID2Matchs)
        return dPathID2Matchs

    def _get_classif_evidence_by_seq_name(self, seqName):
        qry = self._df_classif.loc[seqName]
        targetLength = qry.length
        targetDesc = "Wcode={}; CI={}; {}; {}; {}".format(qry.Wcode, qry.CI, qry.coding, qry.struct, qry.other)
        return (targetLength, targetDesc)

    ## Add path object in match data dict. The path attributs used are
    # queryStart, queryEnd, strand, subjectName, subjectStart, subjectEnd, eValue, identity and
    # otherTargets
    #
    # @param iPath path object
    # @param dPathID2Matchs to be completed
    # @return dPathID2Matchs dict whose keys are path IDs and values are matches data with otherTarget information
    #
    def _add_path_to_match_dict(self, dPathID2Matchs, iPreviousPath):
        pathId = iPreviousPath.getIdentifier()
        subjectStart = iPreviousPath.getSubjectStart()
        subjectEnd = iPreviousPath.getSubjectEnd()
        strand = iPreviousPath.getSubjectStrand()
        if subjectStart > subjectEnd:
            tmp = subjectStart
            subjectStart = subjectEnd
            subjectEnd = tmp
        queryStart = iPreviousPath.getQueryStart()
        queryEnd = iPreviousPath.getQueryEnd()
        subjectName = iPreviousPath.getSubjectName()
        eValue = iPreviousPath.getEvalue()
        identity = iPreviousPath.getIdentity()
        otherTargets = iPreviousPath.otherTargets
        if pathId in dPathID2Matchs.keys():
            dPathID2Matchs[pathId].append(
                [queryStart, queryEnd, strand, subjectName, subjectStart, subjectEnd, eValue, identity, otherTargets])
        else:
            dPathID2Matchs[pathId] = [
                [queryStart, queryEnd, strand, subjectName, subjectStart, subjectEnd, eValue, identity, otherTargets]]

    ## For a specific path ID, organize match data according to the GFF3 format.
    #
    # @param pathID string path ID
    # @param lMatches match list
    # @param seqName string name of the source feature
    # @param source string 'source' field for GFF3 format
    # @param frame string 'frame' field for GFF3 format
    # @return lines string to write in the GFF3 file
    #
    def _organize_each_path_feature(self, pathID, lMatches, seqName, frame, source="REPET_TEs"):
        lines = ""
        minStart = lMatches[0][0]
        maxEnd = lMatches[0][1]
        minStartSubject = lMatches[0][4]
        maxEndSubject = lMatches[0][5]
        strand = lMatches[0][2]

        # for each match
        for i in lMatches:
            if i[0] < minStart:
                minStart = i[0]
            if i[1] > maxEnd:
                maxEnd = i[1]
            if i[4] < minStartSubject:
                minStartSubject = i[4]
            if i[5] > maxEndSubject:
                maxEndSubject = i[5]

        target = lMatches[0][3]

        targetDescTag = ""
        targetLengthTag = ""
        attributes = "ID=ms{}_{}_{}".format(pathID, seqName, target)
        if self._is_chado:
            attributes += ";Target={}+{}+{}".format(target, minStartSubject, maxEndSubject)
        else:
            attributes += ";Target={} {} {}".format(target, minStartSubject, maxEndSubject)

        TE_annot = True if re.search("REPET_TEs", source) else False

        if TE_annot and self._classif_file_name != "":
            targetLength, targetDesc = self._get_classif_evidence_by_seq_name(target)
            if targetLength != 0:
                targetLengthTag = ";TargetLength={}".format(targetLength)
            if targetDesc != "":
                targetDescTag = ";TargetDescription={}".format(
                    targetDesc.replace('=', ':').replace(';', '').replace(',', ' |'))
        else:
            self._log.info("No targetLength information and no target Description for pathID: {}".format(pathID))

        attributes += targetLengthTag
        attributes += targetDescTag

        if not self._df_align_tab.empty and pathID in self._df_align_tab.index:
            alignLength = self._df_align_tab.loc[pathID, 'alignLength']
            alignIdentity = self._df_align_tab.loc[pathID, 'alignIdent']
            attributes += ";AlignIdentity={:0.2f};AlignLength={}".format(alignIdentity, alignLength)
        else:
            if len(lMatches) == 1:
                attributes += ";Identity={}".format(lMatches[0][7])

        if lMatches[0][8]:
            otherTargets = ", ".join(lMatches[0][8])
            attributes += ";OtherTargets={}".format(otherTargets)

        lines += "{}\t{}\tmatch\t{}\t{}\t0.0\t{}\t{}\t{}\n".format(seqName, source, minStart, maxEnd, strand, frame,
                                                                   attributes)

        if len(lMatches) > 1 or self._are_match_parts_compulsory:
            count = 1
            for i in lMatches:
                attributes = "ID=mp{}-{}_{}_{}".format(pathID, count, seqName, target)
                attributes += ";Parent=ms{}{}{}{}{}".format(pathID, "_", seqName, "_", target)
                if self._is_chado:
                    attributes += ";Target={}+{}+{}".format(target, i[4], i[5])
                else:
                    attributes += ";Target={} {} {}".format(target, i[4], i[5])

                if not i[8]:
                    attributes += ";Identity={}".format(i[7])
                lines += "{}\t{}\tmatch_part\t{}\t{}\t{}\t{}\t{}\t{}\n".format(seqName, source, i[0], i[1], i[6], i[2],
                                                                               frame, attributes)
                count += 1

        return lines


    def run(self):
        def get_bioseq_from_header(header, fasta):
            if "'" in header:
                newHeader = header.replace("'", "\\'")
            else:
                newHeader = header
            return Bioseq(newHeader, fasta[newHeader])

        LoggerFactory.setLevel(self._log, self._verbose + 2)

        self.check_options()
        self._log.info("START GFF3Maker")
        self._log.info("Create copies fasta files")

        feature = "region"
        frame = "."

        lAccessionLengthTuples = []
        for i in self._genome_fasta.keys():
            lAccessionLengthTuples.append((i, len(self._genome_fasta[i])))

        self._log.debug("lTuples: {}".format(lAccessionLengthTuples))
        for seqName, length in lAccessionLengthTuples:
            if not self._do_split:  ## all annotations are in the same GFF3
                fileName = os.path.join(self._project_dir, "{}.gff3".format(seqName))
                with open(fileName, "w") as outFile:
                    outFile.write("##gff-version 3\n")
                    outFile.write("##sequence-region {} 1 {}\n".format(seqName, length))
                    annotations = self._get_path_features(seqName, feature, frame)
                    outFile.write(annotations)

                if not self._is_gff3_without_annotation and FileUtils.getNbLinesInSingleFile(fileName) == 2:
                    os.remove(fileName)
                elif self._is_with_sequence:
                    with open(fileName, "a") as outFile:
                        outFile.write("##FASTA\n")
                        iBioseq = get_bioseq_from_header(seqName)
                        iBioseq.write(outFile)
            else:  ## each annotation type are in their own GGF3 file
                count = 1
                fileName = os.path.join(self._project_dir, "{}_Annot{}.gff3".format(seqName, count))
                with open(fileName, "w") as outFile:
                    outFile.write("##gff-version 3\n")
                    outFile.write("##sequence-region {} 1 {}\n".format(seqName, length))
                    annotations = self._get_path_features(seqName, feature, frame)
                    outFile.write(annotations)

                    # TODO: check getNbLinesInSingleFile() to handle big files
                    if not self._is_gff3_without_annotation and FileUtils.getNbLinesInSingleFile(fileName) == 2:
                        os.remove(fileName)
                    elif self._is_with_sequence:
                        outFile = open(fileName, "a")
                        outFile.write("##FASTA\n")
                        iBioseq = get_bioseq_from_header(seqName)
                        iBioseq.write(outFile)
                count += 1
        self._log.info("END GFF3Maker")


if __name__ == "__main__":
    # align_tab_file = "/home/mwan/REPET/REPET-snakemake/test/DmelChr4_chr_allTEs_nr_noSSR_join_path_align.tab"
    # genome_fasta_file = "/home/mwan/REPET/REPET-snakemake/data/DmelChr4.fa"
    # join_path_file = '/home/mwan/REPET/REPET-snakemake/test/Dmel4_chr_noSSR_path_join.tsv'
    # classifTableFile ="/home/mwan/REPET/REPET-snakemake/test/DmelChr4_sim_denovoLibTEs_PC.classif"
    # classifTableFile=""
    # iGFF3Maker = GFF3Maker(inFastaName=genome_fasta_file, joinPathFile=join_path_file, classifFile=classifTableFile, isChado=False,
    #             isGFF3WithoutAnnotation=False, isWithSequence=False, areMatchPartsCompulsory=False, 
    #             verbose=1, doMergeIdenticalMatches=False, doSplit=False, alignInformationsFile=align_tab_file)
    #iGFF3Maker = GFF3Maker()
    #iGFF3Maker.set_attributes_from_cmd_line()
    #iGFF3Maker.run()
    p = "/home/mwan/REPET/REPET-snakemake/TEannot_snakemake/results_TEannot_DmelChr4/chunk_dir/DmelChr4.fa.Nstretch.map"
    csp =pandas.read_csv(p, header = None, sep = '\t')
    fruits= csp.loc[:,1].tolist()