import unittest
from commons.tools.ShortenConsensusHeader import ShortenConsensusHeader
import os
from commons.core.utils.FileUtils import FileUtils


class Test_ShortenConsensusHeader(unittest.TestCase):

    def setUp(self):
        self.iSCH = ShortenConsensusHeader(projectName = "Vigne_1_3_12_1")

    def tearDown(self):
        pass
    
    def test_shortenConsensusName(self):
        obsConsensusName = self.iSCH.shortenConsensusName("Vigne_1_3_12_1_Blaster_Grouper_3678_Map_14")
        expConsensusName = "Vigne_1_3_12_1-B-G3678-Map14"
        self.assertEqual(expConsensusName, obsConsensusName)
        
    def test_shortenConsensusName_MCL(self):
        obsConsensusName = self.iSCH.shortenConsensusName("Vigne_1_3_12_1_LTRharvest_MCL_6101988_Map_14")
        expConsensusName = "Vigne_1_3_12_1-L-M6101988-Map14"
        self.assertEqual(expConsensusName, obsConsensusName)
        
    def test_shortenConsensusName_reversed(self):
        obsConsensusName = self.iSCH.shortenConsensusName("Vigne_1_3_12_1_Blaster_Grouper_3678_Map_14_reversed")
        expConsensusName = "Vigne_1_3_12_1-B-G3678-Map14_reversed"
        self.assertEqual(expConsensusName, obsConsensusName)
        
        # The 3 following tests are designed to test RepeatScout's headers shortening.
        # By now, RS consensus' headers are kept as it (cf. Classif.createNewConsensusName method).
        # If you want to shorten them, uncomment these tests and make them pass, baby.
        #
        # /!\ Be careful! Modify functional tests too, and check TEdenovo's following steps and tests for resulting errors!
        
#    def test_shortenConsensusName_RS(self):
#        projectName = "Vigne_1_3_12_1"
#        iClassif = Classif("Vigne_1_3_12_1_RS_2", "RE", "chim", "comp")
#        iClassif.setProjectName(projectName)
#        obsConsensusName = iClassif.shortenConsensusName()
#        expConsensusName = "Vigne_1_3_12_1-RS2"
#        self.assertEquals(expConsensusName, obsConsensusName)
#        
#    def test_shortenConsensusName_RS_reversed(self):
#        projectName = "Vigne_1_3_12_1"
#        iClassif = Classif("Vigne_1_3_12_1_RS_2_reversed", "RE", "chim", "comp")
#        iClassif.setProjectName(projectName)
#        obsConsensusName = iClassif.shortenConsensusName()
#        expConsensusName = "Vigne_1_3_12_1-RS2_reversed"
#        self.assertEquals(expConsensusName, obsConsensusName)
#        
#    def test_shortenConsensusName_RS_reversed_simpleProjectName(self):
#        projectName = "test"
#        iClassif = Classif("test_RS_2_reversed", "RE", "chim", "comp")
#        iClassif.setProjectName(projectName)
#        obsConsensusName = iClassif.shortenConsensusName()
#        expConsensusName = "test-RS2_reversed"
#        self.assertEquals(expConsensusName, obsConsensusName)

    def test_renameHeaderInConsensusFastaFile(self):
        inFile = "consensusFile.fa"
        obsFile = "consensusFileNew.fa"
        expFile = "exptconsensusFileNew.fa"
        self._write_inputFile(inFile)
        self._write_expFile(expFile)
        self.iSCH.renameHeaderInConsensusFastaFile(inFile)
        self.assertTrue(FileUtils.are2FilesIdentical(obsFile, expFile))
        os.remove(inFile)
        os.remove(obsFile)
        os.remove(expFile)
        
    def _write_inputFile(self, fileName):
        with open(fileName, "w") as consensusFile:
            consensusFile.write(">Vigne_1_3_12_1_Blaster_Grouper_1_Map_3\n")
            consensusFile.write("ATACAGCTGCGGTTAAAATAATAGCACTACTGCAGGTGGAAAGTTGATTTCCTAAAAAAA\n")
        
    def _write_expFile(self, fileName):
        with open(fileName, "w") as consensusFile:
            consensusFile.write(">Vigne_1_3_12_1-B-G1-Map3\n")
            consensusFile.write("ATACAGCTGCGGTTAAAATAATAGCACTACTGCAGGTGGAAAGTTGATTTCCTAAAAAAA\n")
