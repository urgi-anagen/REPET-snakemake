import unittest
import shutil
import time
import os
import subprocess
from commons.core.utils.FileUtils import FileUtils
from commons.core.sql.DbMySql import DbMySql
from commons.tools.GFF3Maker import GFF3Maker
from commons.tools.ListAndDropTables import ListAndDropTables


class Test_F_GFF3Maker(unittest.TestCase):

    def setUp(self):
        self.curDir = os.getcwd()
        self._uniqId = "GFF3Maker_{}_{}".format(time.strftime("%Y%m%d%H%M%S"), os.getpid())
        os.makedirs(self._uniqId)
        os.chdir(self._uniqId)

        self._projectName = "PN"
        self._iDb = DbMySql()
        self._tablesFileName = "annotation_tables.txt"
        self._fastaFileName = "{}_chr.fa".format(self._projectName)
        self._fastaTableName = "{}_chr_seq".format(self._projectName)
        self._writeFastaFile(self._fastaFileName)
        self._iDb.createTable(self._fastaTableName, "seq", self._fastaFileName, True)
        self._inputFileName = "{}_chr_allTEs_nr_noSSR_join.pathOrSet".format(self._projectName)
        self._expGFFFileName = "explm_SuperContig_29_v2.gff3"
        self._obsGFFFileName = "lm_SuperContig_29_v2.gff3"
        self._obsGFFEmptyFileName = "lm_SuperContig_30_v2.gff3"

    def tearDown(self):
        self._iDb.close()
        iLADT = ListAndDropTables(action="drop", tableNames=self._projectName)
        iLADT.run()
        os.chdir(self.curDir)
        shutil.rmtree(self._uniqId)
        self._uniqId = None

    def test_run_path_with_seq_withAllFiles_withoutClassif(self):
        self._writeTablesFile("path")
        self._writePathFile(self._inputFileName)
        self._inputTableName = "{}_chr_allTEs_nr_noSSR_join_path".format(self._projectName)
        self._iDb.createTable(self._inputTableName, "path", self._inputFileName, True)
        self._writeExpPathGFFFile(self._expGFFFileName)
        expGFFEmptyFileName = "explm_SuperContig_30_v2.gff3"
        self._writeExpEmptyPathGFFFileWithSeq(expGFFEmptyFileName)
        inputAlignTabFileName = "{}_align.tab".format(self._inputTableName)
        self._writeAlignTabFile(inputAlignTabFileName)

        iGFF3Maker = GFF3Maker()
        iGFF3Maker.setTablesFileName(self._tablesFileName)
        iGFF3Maker.setInFastaName(self._fastaTableName)
        iGFF3Maker.setAreMatchPartCompulsory(True)
        iGFF3Maker.setIsWithSequence(True)
        iGFF3Maker.setIsGFF3WithoutAnnotation(True)
        iGFF3Maker.setAlignInformationsFileName(inputAlignTabFileName)
        iGFF3Maker.run()

        self.assertTrue(FileUtils.are2FilesIdentical(self._expGFFFileName, self._obsGFFFileName))
        self.assertTrue(FileUtils.are2FilesIdentical(expGFFEmptyFileName, self._obsGFFEmptyFileName))

    def test_run_without_seq(self):
        self._writeTablesFile("path")
        self._writePathFile(self._inputFileName)
        self._inputTableName = "{}_chr_allTEs_nr_noSSR_join_path".format(self._projectName)
        self._iDb.createTable(self._inputTableName, "path", self._inputFileName, True)
        self._writeExpPathGFFFile_without_seq(self._expGFFFileName)
        inputAlignTabFileName = "{}_align.tab".format(self._inputTableName)
        self._writeAlignTabFile(inputAlignTabFileName)

        iGFF3Maker = GFF3Maker()
        iGFF3Maker.setTablesFileName(self._tablesFileName)
        iGFF3Maker.setInFastaName(self._fastaTableName)
        iGFF3Maker.setAreMatchPartCompulsory(True)
        iGFF3Maker.setAlignInformationsFileName(inputAlignTabFileName)
        iGFF3Maker.run()

        self.assertTrue(FileUtils.are2FilesIdentical(self._expGFFFileName, self._obsGFFFileName))
        self.assertFalse(FileUtils.isRessourceExists(self._obsGFFEmptyFileName))

    def test_run_without_seq_and_match_part_not_compulsory(self):
        self._writeTablesFile("path")
        self._writePathFile(self._inputFileName)
        self._inputTableName = "{}_chr_allTEs_nr_noSSR_join_path".format(self._projectName)
        self._iDb.createTable(self._inputTableName, "path", self._inputFileName, True)
        self._writeExpPathGFFFile_without_seq_and_match_part_not_compulsory(self._expGFFFileName)
        inputAlignTabFileName = "{}_align.tab".format(self._inputTableName)
        self._writeAlignTabFile(inputAlignTabFileName)

        iGFF3Maker = GFF3Maker()
        iGFF3Maker.setTablesFileName(self._tablesFileName)
        iGFF3Maker.setInFastaName(self._fastaTableName)
        iGFF3Maker.setAlignInformationsFileName(inputAlignTabFileName)
        iGFF3Maker.run()

        self.assertTrue(FileUtils.are2FilesIdentical(self._expGFFFileName, self._obsGFFFileName))
        self.assertFalse(FileUtils.isRessourceExists(self._obsGFFEmptyFileName))

    def test_run_as_script_pathReversed(self):
        self._writeTablesFile("path")
        self._writePathFileReverse(self._inputFileName)
        self._inputTableName = "{}_chr_allTEs_nr_noSSR_join_path".format(self._projectName)
        self._iDb.createTable(self._inputTableName, "path", self._inputFileName, True)
        self._writeExpPathGFFFileReversed(self._expGFFFileName)
        inputAlignTabFileName = "{}_align.tab".format(self._inputTableName)
        self._writeAlignTabFile(inputAlignTabFileName)

        cmd = "GFF3Maker.py -t {} -f {} -w -p -A {}".format(self._tablesFileName, self._fastaTableName,
                                                            inputAlignTabFileName)
        process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell = True)
        output = process.communicate()

        self.assertTrue(FileUtils.are2FilesIdentical(self._expGFFFileName, self._obsGFFFileName))
        self.assertFalse(FileUtils.isRessourceExists(self._obsGFFEmptyFileName))

    def test_run_as_script_set(self):  # no re-alignment
        self._writeTablesFile("set")
        self._writeSetFile(self._inputFileName)
        self._inputTableName = "{}_chr_allTEs_nr_noSSR_join_set".format(self._projectName)
        self._iDb.createTable(self._inputTableName, "set", self._inputFileName, True)
        self._writeExpSetGFFFile(self._expGFFFileName)
        inputAlignTabFileName = "{}_align.tab".format(self._inputTableName)
        self._writeAlignTabFile(inputAlignTabFileName)

        cmd = "GFF3Maker.py -t {} -f {} -w -p ".format(self._tablesFileName, self._fastaTableName)
        process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell = True)
        output = process.communicate()

        self.assertTrue(FileUtils.are2FilesIdentical(self._expGFFFileName, self._obsGFFFileName))

    def test_run_as_script_setReversed(self):  # pas de re-alignement
        self._writeTablesFile("set")
        self._writeSetFileReverse(self._inputFileName)
        self._inputTableName = "{}_chr_allTEs_nr_noSSR_join_set".format(self._projectName)
        self._iDb.createTable(self._inputTableName, "set", self._inputFileName, True)
        self._writeExpSetGFFFileReversed(self._expGFFFileName)
        inputAlignTabFileName = "{}_align.tab".format(self._inputTableName)
        self._writeAlignTabFile(inputAlignTabFileName)

        cmd = "GFF3Maker.py -t {} -f {} -w -p -A {}".format(self._tablesFileName, self._fastaTableName,
                                                            inputAlignTabFileName)
        process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell = True)
        output = process.communicate()

        self.assertTrue(FileUtils.are2FilesIdentical(self._expGFFFileName, self._obsGFFFileName))
        self.assertFalse(FileUtils.isRessourceExists(self._obsGFFEmptyFileName))

    def test_run_as_script_path_without_seq_withAllFilesWithNewClassif_TEdenovo_step6_and_step8(self):
        self._writeTablesFile("path")
        self._writePathFile_withClassif(self._inputFileName)
        self._inputTableName = "{}_chr_allTEs_nr_noSSR_join_path".format(self._projectName)
        self._iDb.createTable(self._inputTableName, "path", self._inputFileName, True)

        self._writeNewClassifFile("input.classif")
        inputClassifTableName = "{}_classif".format(self._projectName)
        self._iDb.createTable(inputClassifTableName, "classifnew", "input.classif", True)
        inputAlignTabFileName = "{}_align.tab".format(self._inputTableName)
        self._writeAlignTabFile(inputAlignTabFileName)

        self._expGFFFileName = "explm_SuperContig_30_v2.gff3"
        self._obsGFFFileName = "lm_SuperContig_30_v2.gff3"
        self._writeExpPathGFFFile_without_seq_withClassif(self._expGFFFileName)

        iGFF3Maker = GFF3Maker()
        iGFF3Maker.setTablesFileName(self._tablesFileName)
        iGFF3Maker.setInFastaName(self._fastaTableName)
        iGFF3Maker.setClassifTable(inputClassifTableName)
        iGFF3Maker.setAreMatchPartCompulsory(True)
        iGFF3Maker.setAlignInformationsFileName(inputAlignTabFileName)
        iGFF3Maker.run()

        self.assertTrue(FileUtils.are2FilesIdentical(self._expGFFFileName, self._obsGFFFileName))

    def test_run_as_script_path_without_seq_withAllFilesWithTargetLength(self):
        fastaFileName = "{}.fa".format(self._projectName)
        self._writeFastaFileExtended(fastaFileName)
        self._iDb.createTable(self._fastaTableName, "seq", fastaFileName, True)
        self._writeTablesFile_withTESeqTables("path")

        self._writePathFile(self._inputFileName)
        self._inputTableName = "{}_chr_allTEs_nr_noSSR_join_path".format(self._projectName)
        self._iDb.createTable(self._inputTableName, "path", self._inputFileName, True)

        inFileName2 = "{}_chr_bankBLRtx.path".format(self._projectName)
        self._writePathFile2(inFileName2)
        bankPathTableName = "{}_chr_bankBLRtx_path".format(self._projectName)
        self._iDb.createTable(bankPathTableName, "path", inFileName2, True)

        refTEsFastaFileName = "{}_refTEs.fa".format(self._projectName)
        self._writeRefTEsSeqFile(refTEsFastaFileName)
        refTESeqTableName = "{}_refTEs_seq".format(self._projectName)
        self._iDb.createTable(refTESeqTableName, "seq", refTEsFastaFileName, True)

        bankBLRtxFastaFileName = "dummyRepbase.fa"
        self._writeBankBLRtxSeqFile(bankBLRtxFastaFileName)
        bankBLRtxTableName = "{}_bankBLRtx_nt_seq".format(self._projectName)
        self._iDb.createTable(bankBLRtxTableName, "seq", bankBLRtxFastaFileName, True)

        self._expGFFFileName = "explm_SuperContig_29_v2.gff3"
        expSeq2GFFFileName = "expChr1.gff3"
        self._obsGFFFileName = "lm_SuperContig_29_v2.gff3"
        obsSeq2GFFFileName = "chr1.gff3"
        self._writeExpPathGFFFile_without_seq_withTargetLength_seq1(self._expGFFFileName)
        self._writeExpPathGFFFile_without_seq_withTargetLength_seq2(expSeq2GFFFileName)

        inputAlignTabFileName = "{}_align.tab".format(self._inputTableName)
        self._writeAlignTabFile(inputAlignTabFileName)

        cmd = "GFF3Maker.py -t {} -f {} -p -A {}".format(self._tablesFileName, self._fastaTableName,
                                                         inputAlignTabFileName)
        process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell = True)
        output = process.communicate()

        self.assertTrue(FileUtils.are2FilesIdentical(self._expGFFFileName, self._obsGFFFileName))
        self.assertTrue(FileUtils.are2FilesIdentical(expSeq2GFFFileName, obsSeq2GFFFileName))

    def test_run_as_script_split_file_by_annotation_method(self):
        fastaFileName = "{}.fa".format(self._projectName)
        self._writeFastaFile_DmelChr4(fastaFileName)
        self._iDb.createTable(self._fastaTableName, "seq", fastaFileName, True)

        self._writeTablesFile_withTESeqTables("path")

        self._writePathFile_refTEs_annotation(self._inputFileName)
        self._inputTableName = "{}_chr_allTEs_nr_noSSR_join_path".format(self._projectName)
        self._iDb.createTable(self._inputTableName, "path", self._inputFileName, True)

        inFileName2 = "{}_chr_bankBLRtx.path".format(self._projectName)
        self._writePathFile_bankBLRtx_annotation(inFileName2)
        bankPathTableName = "{}_chr_bankBLRtx_path".format(self._projectName)
        self._iDb.createTable(bankPathTableName, "path", inFileName2, True)

        refTEsFastaFileName = "{}_refTEs.fa".format(self._projectName)
        self._writeRefTEsSeqFile(refTEsFastaFileName)
        refTESeqTableName = "{}_refTEs_seq".format(self._projectName)
        self._iDb.createTable(refTESeqTableName, "seq", refTEsFastaFileName, True)

        bankBLRtxFastaFileName = "dummyRepbase.fa"
        self._writeBankBLRtxSeqFile(bankBLRtxFastaFileName)
        bankBLRtxTableName = "{}_bankBLRtx_nt_seq".format(self._projectName)
        self._iDb.createTable(bankBLRtxTableName, "seq", bankBLRtxFastaFileName, True)

        self._expGFFFileName = "expdmel_chr4_Annot1.gff3"
        expGFFFileName2 = "expdmel_chr1_Annot1.gff3"
        expGFFFileName3 = "expdmel_chr4_Annot2.gff3"
        self._obsGFFFileName = "dmel_chr4_Annot1.gff3"
        obsGFFFileName2 = "dmel_chr1_Annot1.gff3"
        obsGFFFileName3 = "dmel_chr4_Annot2.gff3"
        self._writeExpPathGFFFile_split_file1(self._expGFFFileName)
        self._writeExpPathGFFFile_split_file2(expGFFFileName2)
        self._writeExpPathGFFFile_split_file3(expGFFFileName3)

        inputAlignTabFileName = "{}_align.tab".format(self._inputTableName)
        self._writeAlignTabFile2(inputAlignTabFileName)

        cmd = "GFF3Maker.py -t {} -f {} -s -p -A {}".format(self._tablesFileName, self._fastaTableName,
                                                            inputAlignTabFileName)
        process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell = True)
        output = process.communicate()

        self.assertTrue(FileUtils.are2FilesIdentical(self._expGFFFileName, self._obsGFFFileName))
        self.assertTrue(FileUtils.are2FilesIdentical(expGFFFileName2, obsGFFFileName2))
        self.assertTrue(FileUtils.are2FilesIdentical(expGFFFileName3, obsGFFFileName3))

    def test_run_path_without_seq_withAllFilesWithClassif_headers_TEdenovo_step6_and_step8_withOtherTarget(self):
        self._writeTablesFile("path")
        self._writePathFile_withClassif_withIdenticalMatches(self._inputFileName)
        self._inputTableName = "{}_chr_allTEs_nr_noSSR_join_path".format(self._projectName)
        self._iDb.createTable(self._inputTableName, "path", self._inputFileName, True)

        self._writeClassifFile("input.classif")
        inputClassifTableName = "{}_classif".format(self._projectName)
        self._iDb.createTable(inputClassifTableName, "classif", "input.classif", True)
        inputAlignTabFileName = "{}_align.tab".format(self._inputTableName)
        self._writeAlignTabFile(inputAlignTabFileName)

        self._expGFFFileName = "explm_SuperContig_30_v2.gff3"
        self._obsGFFFileName = "lm_SuperContig_30_v2.gff3"
        self._writeExpPathGFFFile_without_seq_withClassif_withIdenticalMatches(self._expGFFFileName)

        iGFF3Maker = GFF3Maker()
        iGFF3Maker.setTablesFileName(self._tablesFileName)
        iGFF3Maker.setInFastaName(self._fastaTableName)
        iGFF3Maker.setClassifTable(inputClassifTableName)
        iGFF3Maker.setDoMergeIdenticalMatches(True)
        iGFF3Maker.setAreMatchPartCompulsory(True)
        iGFF3Maker.setAlignInformationsFileName(inputAlignTabFileName)
        iGFF3Maker.run()

        self.assertTrue(FileUtils.are2FilesIdentical(self._expGFFFileName, self._obsGFFFileName))

    def test_run_as_script_path_without_seq_withAllFilesWithClassif_headers_TEdenovo_step6_and_step8_withOtherTarget(self):
        self._writeTablesFile("path")
        self._writePathFile_withClassif_withIdenticalMatches(self._inputFileName)
        self._inputTableName = "{}_chr_allTEs_nr_noSSR_join_path".format(self._projectName)
        self._iDb.createTable(self._inputTableName, "path", self._inputFileName, True)

        self._writeClassifFile("input.classif")
        inputClassifTableName = "{}_classif".format(self._projectName)
        self._iDb.createTable(inputClassifTableName, "classif", "input.classif", True)
        inputAlignTabFileName = "{}_align.tab".format(self._inputTableName)
        self._writeAlignTabFile(inputAlignTabFileName)

        self._expGFFFileName = "explm_SuperContig_30_v2.gff3"
        self._obsGFFFileName = "lm_SuperContig_30_v2.gff3"
        self._writeExpPathGFFFile_without_seq_withClassif_withIdenticalMatches(self._expGFFFileName)

        cmd = "GFF3Maker.py -t {} -f {} -i {} -m -p -A {}".format(self._tablesFileName, self._fastaTableName,
                                                                  inputClassifTableName, inputAlignTabFileName)
        subprocess.call(cmd, shell=True)

        self.assertTrue(FileUtils.are2FilesIdentical(self._expGFFFileName, self._obsGFFFileName))

    def _writeRefTEsSeqFile(self, inFileName):
        with open(inFileName, "w") as f:
            f.write(">DTX-incomp_DmelChr4-L-B1-Map3\n")
            f.write("ATCGATCGTT\n")
            f.write(">DTX-incomp_DmelChr4-B-P0.0-Map3\n")
            f.write("GCTAGCTA\n")

    def _writeBankBLRtxSeqFile(self, inFileName):
        with open(inFileName, "w") as f:
            f.write(">PROTOP_B:classII:TIR\n")
            f.write("ATCGATCGTT\n")
            f.write(">DMRT1C:classI:?\n")
            f.write("GCTAGCTA\n")
            f.write(">BATUMI_I:classI:LTR_retrotransposon\n")
            f.write("GCTAATGGCATA\n")

    def _writeExpPathGFFFile_without_seq_withTargetLength_seq1(self, inFileName):
        with open(inFileName, "w") as f:
            f.write("##gff-version 3\n")
            f.write("##sequence-region lm_SuperContig_29_v2 1 120\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch\t78031\t78588\t0.0\t+\t.\tID=ms3_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-B-P0.0-Map3;Target=DTX-incomp_DmelChr4-B-P0.0-Map3 19 209;TargetLength=8;AlignIdentity=32.31;AlignLength=2367\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch_part\t78031\t78080\t3e-21\t+\t.\tID=mp3-1_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-B-P0.0-Map3;Parent=ms3_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-B-P0.0-Map3;Target=DTX-incomp_DmelChr4-B-P0.0-Map3 19 209;Identity=30.89\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch_part\t78081\t78588\t3e-21\t+\t.\tID=mp3-2_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-B-P0.0-Map3;Parent=ms3_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-B-P0.0-Map3;Target=DTX-incomp_DmelChr4-B-P0.0-Map3 19 209;Identity=30.89\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch\t192832\t193704\t0.0\t-\t.\tID=ms2_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-L-B1-Map3;Target=DTX-incomp_DmelChr4-L-B1-Map3 229 522;TargetLength=10;AlignIdentity=23.99;AlignLength=1210\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch_part\t192832\t193704\t1e-40\t-\t.\tID=mp2-1_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-L-B1-Map3;Parent=ms2_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-L-B1-Map3;Target=DTX-incomp_DmelChr4-L-B1-Map3 229 522;Identity=23.99\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch\t193781\t194212\t0.0\t-\t.\tID=ms1_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-L-B1-Map3;Target=DTX-incomp_DmelChr4-L-B1-Map3 85 228;TargetLength=10;AlignIdentity=31.02;AlignLength=1200\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch_part\t193781\t194212\t1e-40\t-\t.\tID=mp1-1_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-L-B1-Map3;Parent=ms1_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-L-B1-Map3;Target=DTX-incomp_DmelChr4-L-B1-Map3 85 228;Identity=30.56\n")

    def _writeExpPathGFFFile_without_seq_withTargetLength_seq2(self, inFileName):
        with open(inFileName, "w") as f:
            f.write("##gff-version 3\n")
            f.write("##sequence-region chr1 1 18\n")
            f.write(
                "chr1\tPN_REPET_tblasx\tmatch\t1\t100\t0.0\t-\t.\tID=ms1_chr1_PROTOP_B:classII:TIR;Target=PROTOP_B:classII:TIR 85 228;TargetLength=10;Identity=30.56\n")
            f.write(
                "chr1\tPN_REPET_tblasx\tmatch_part\t1\t100\t1e-20\t-\t.\tID=mp1-1_chr1_PROTOP_B:classII:TIR;Parent=ms1_chr1_PROTOP_B:classII:TIR;Target=PROTOP_B:classII:TIR 85 228;Identity=30.56\n")
            f.write(
                "chr1\tPN_REPET_tblasx\tmatch\t100\t1000\t0.0\t-\t.\tID=ms2_chr1_DMRT1C:classI:?;Target=DMRT1C:classI:? 85 228;TargetLength=8;Identity=30.56\n")
            f.write(
                "chr1\tPN_REPET_tblasx\tmatch_part\t100\t1000\t1e-30\t-\t.\tID=mp2-1_chr1_DMRT1C:classI:?;Parent=ms2_chr1_DMRT1C:classI:?;Target=DMRT1C:classI:? 85 228;Identity=30.56\n")

    def _writeExpPathGFFFile_split_file1(self, inFileName):
        with open(inFileName, 'w') as f:
            f.write("##gff-version 3\n")
            f.write("##sequence-region dmel_chr4 1 18\n")
            f.write(
                "dmel_chr4\t{}_REPET_TEs\tmatch\t4364\t4611\t0.0\t+\t.\tID=ms6_dmel_chr4_DTX-incomp_DmelChr4-L-B1-Map3;Target=DTX-incomp_DmelChr4-L-B1-Map3 854 1150;TargetLength=10;AlignIdentity=91.24;AlignLength=296\n".format(
                    self._projectName))
            f.write(
                "dmel_chr4\t{}_REPET_TEs\tmatch_part\t4364\t4611\t0.0\t+\t.\tID=mp6-1_dmel_chr4_DTX-incomp_DmelChr4-L-B1-Map3;Parent=ms6_dmel_chr4_DTX-incomp_DmelChr4-L-B1-Map3;Target=DTX-incomp_DmelChr4-L-B1-Map3 854 1150;Identity=91.24\n".format(
                    self._projectName))
            f.write(
                "dmel_chr4\t{}_REPET_TEs\tmatch\t4630\t4889\t0.0\t+\t.\tID=ms21_dmel_chr4_DTX-incomp_DmelChr4-B-P0.0-Map3;Target=DTX-incomp_DmelChr4-B-P0.0-Map3 232 512;TargetLength=8;AlignIdentity=84.44;AlignLength=126\n".format(
                    self._projectName))
            f.write(
                "dmel_chr4\t{}_REPET_TEs\tmatch_part\t4630\t4704\t0.0\t+\t.\tID=mp21-1_dmel_chr4_DTX-incomp_DmelChr4-B-P0.0-Map3;Parent=ms21_dmel_chr4_DTX-incomp_DmelChr4-B-P0.0-Map3;Target=DTX-incomp_DmelChr4-B-P0.0-Map3 232 312;Identity=84.4417\n".format(
                    self._projectName))
            f.write(
                "dmel_chr4\t{}_REPET_TEs\tmatch_part\t4837\t4889\t0.0\t+\t.\tID=mp21-2_dmel_chr4_DTX-incomp_DmelChr4-B-P0.0-Map3;Parent=ms21_dmel_chr4_DTX-incomp_DmelChr4-B-P0.0-Map3;Target=DTX-incomp_DmelChr4-B-P0.0-Map3 456 512;Identity=84.4417\n".format(
                    self._projectName))

    def _writeExpPathGFFFile_split_file2(self, inFileName):
        with open(inFileName, 'w') as f:
            f.write("##gff-version 3\n")
            f.write("##sequence-region dmel_chr1 1 25\n")
            f.write(
                "dmel_chr1\t{}_REPET_TEs\tmatch\t4364\t4611\t0.0\t+\t.\tID=ms35_dmel_chr1_DTX-incomp_DmelChr4-L-B1-Map3;Target=DTX-incomp_DmelChr4-L-B1-Map3 854 1150;TargetLength=10;AlignIdentity=91.24;AlignLength=247\n".format(
                    self._projectName))
            f.write(
                "dmel_chr1\t{}_REPET_TEs\tmatch_part\t4364\t4611\t0.0\t+\t.\tID=mp35-1_dmel_chr1_DTX-incomp_DmelChr4-L-B1-Map3;Parent=ms35_dmel_chr1_DTX-incomp_DmelChr4-L-B1-Map3;Target=DTX-incomp_DmelChr4-L-B1-Map3 854 1150;Identity=91.24\n".format(
                    self._projectName))

    def _writeExpPathGFFFile_split_file3(self, inFileName):
        with open(inFileName, 'w') as f:
            f.write("##gff-version 3\n")
            f.write("##sequence-region dmel_chr4 1 18\n")
            f.write(
                "dmel_chr4\t{}_REPET_tblasx\tmatch\t3143\t4364\t0.0\t-\t.\tID=ms66_dmel_chr4_DMRT1C:classI:?;Target=DMRT1C:classI:? 24 2206;TargetLength=8\n".format(
                    self._projectName))
            f.write(
                "dmel_chr4\t{}_REPET_tblasx\tmatch_part\t3143\t3361\t0.0\t-\t.\tID=mp66-1_dmel_chr4_DMRT1C:classI:?;Parent=ms66_dmel_chr4_DMRT1C:classI:?;Target=DMRT1C:classI:? 1988 2206;Identity=89.7202\n".format(
                    self._projectName))
            f.write(
                "dmel_chr4\t{}_REPET_tblasx\tmatch_part\t3345\t3410\t0.0\t-\t.\tID=mp66-2_dmel_chr4_DMRT1C:classI:?;Parent=ms66_dmel_chr4_DMRT1C:classI:?;Target=DMRT1C:classI:? 1654 1719;Identity=54.55\n".format(
                    self._projectName))
            f.write(
                "dmel_chr4\t{}_REPET_tblasx\tmatch_part\t3448\t3572\t0.0\t-\t.\tID=mp66-3_dmel_chr4_DMRT1C:classI:?;Parent=ms66_dmel_chr4_DMRT1C:classI:?;Target=DMRT1C:classI:? 1495 1619;Identity=77.3433\n".format(
                    self._projectName))
            f.write(
                "dmel_chr4\t{}_REPET_tblasx\tmatch_part\t3564\t3693\t0.0\t-\t.\tID=mp66-4_dmel_chr4_DMRT1C:classI:?;Parent=ms66_dmel_chr4_DMRT1C:classI:?;Target=DMRT1C:classI:? 1380 1509;Identity=83.7306\n".format(
                    self._projectName))
            f.write(
                "dmel_chr4\t{}_REPET_tblasx\tmatch_part\t3705\t3857\t0.0\t-\t.\tID=mp66-5_dmel_chr4_DMRT1C:classI:?;Parent=ms66_dmel_chr4_DMRT1C:classI:?;Target=DMRT1C:classI:? 539 691;Identity=84.5762\n".format(
                    self._projectName))
            f.write(
                "dmel_chr4\t{}_REPET_tblasx\tmatch_part\t3861\t3992\t0.0\t-\t.\tID=mp66-6_dmel_chr4_DMRT1C:classI:?;Parent=ms66_dmel_chr4_DMRT1C:classI:?;Target=DMRT1C:classI:? 403 534;Identity=67.1422\n".format(
                    self._projectName))
            f.write(
                "dmel_chr4\t{}_REPET_tblasx\tmatch_part\t3985\t4106\t0.0\t-\t.\tID=mp66-7_dmel_chr4_DMRT1C:classI:?;Parent=ms66_dmel_chr4_DMRT1C:classI:?;Target=DMRT1C:classI:? 290 411;Identity=76.612\n".format(
                    self._projectName))
            f.write(
                "dmel_chr4\t{}_REPET_tblasx\tmatch_part\t4102\t4248\t0.0\t-\t.\tID=mp66-8_dmel_chr4_DMRT1C:classI:?;Parent=ms66_dmel_chr4_DMRT1C:classI:?;Target=DMRT1C:classI:? 139 285;Identity=75.3027\n".format(
                    self._projectName))
            f.write(
                "dmel_chr4\t{}_REPET_tblasx\tmatch_part\t4236\t4364\t0.0\t-\t.\tID=mp66-9_dmel_chr4_DMRT1C:classI:?;Parent=ms66_dmel_chr4_DMRT1C:classI:?;Target=DMRT1C:classI:? 24 152;Identity=66.6657\n".format(
                    self._projectName))
            f.write(
                "dmel_chr4\t{}_REPET_tblasx\tmatch\t4412\t4889\t0.0\t-\t.\tID=ms27_dmel_chr4_PROTOP_B:classII:TIR;Target=PROTOP_B:classII:TIR 553 1082;TargetLength=10\n".format(
                    self._projectName))
            f.write(
                "dmel_chr4\t{}_REPET_tblasx\tmatch_part\t4412\t4501\t5e-108\t-\t.\tID=mp27-1_dmel_chr4_PROTOP_B:classII:TIR;Parent=ms27_dmel_chr4_PROTOP_B:classII:TIR;Target=PROTOP_B:classII:TIR 993 1082;Identity=91.3066\n".format(
                    self._projectName))
            f.write(
                "dmel_chr4\t{}_REPET_tblasx\tmatch_part\t4483\t4652\t9e-146\t-\t.\tID=mp27-2_dmel_chr4_PROTOP_B:classII:TIR;Parent=ms27_dmel_chr4_PROTOP_B:classII:TIR;Target=PROTOP_B:classII:TIR 821 990;Identity=86.5572\n".format(
                    self._projectName))
            f.write(
                "dmel_chr4\t{}_REPET_tblasx\tmatch_part\t4672\t4775\t9e-146\t-\t.\tID=mp27-3_dmel_chr4_PROTOP_B:classII:TIR;Parent=ms27_dmel_chr4_PROTOP_B:classII:TIR;Target=PROTOP_B:classII:TIR 684 787;Identity=88.8013\n".format(
                    self._projectName))
            f.write(
                "dmel_chr4\t{}_REPET_tblasx\tmatch_part\t4782\t4889\t9e-141\t-\t.\tID=mp27-4_dmel_chr4_PROTOP_B:classII:TIR;Parent=ms27_dmel_chr4_PROTOP_B:classII:TIR;Target=PROTOP_B:classII:TIR 553 661;Identity=53.7067\n".format(
                    self._projectName))
            f.write(
                "dmel_chr4\t{}_REPET_tblasx\tmatch\t4917\t5195\t0.0\t+\t.\tID=ms141_dmel_chr4_BATUMI_I:classI:LTR_retrotransposon;Target=BATUMI_I:classI:LTR_retrotransposon 7030 7303;TargetLength=12\n".format(
                    self._projectName))
            f.write(
                "dmel_chr4\t{}_REPET_tblasx\tmatch_part\t4917\t5007\t2e-144\t+\t.\tID=mp141-1_dmel_chr4_BATUMI_I:classI:LTR_retrotransposon;Parent=ms141_dmel_chr4_BATUMI_I:classI:LTR_retrotransposon;Target=BATUMI_I:classI:LTR_retrotransposon 7030 7120;Identity=94.1756\n".format(
                    self._projectName))
            f.write(
                "dmel_chr4\t{}_REPET_tblasx\tmatch_part\t4976\t5195\t2e-144\t+\t.\tID=mp141-2_dmel_chr4_BATUMI_I:classI:LTR_retrotransposon;Parent=ms141_dmel_chr4_BATUMI_I:classI:LTR_retrotransposon;Target=BATUMI_I:classI:LTR_retrotransposon 7087 7303;Identity=82.2343\n".format(
                    self._projectName))

    def _writeClassifFile(self, inputFileName):
        with open(inputFileName, "w") as f:
            f.write(
                "PotentialHostGene-chim_fTest05105818-B-G11-Map20\t1240\t+\tPotentialChimeric\tNA\tPotentialHostGene\tNA\tCI=100; coding=(HG_BLRn: FBtr0089196_Dmel_r4.3: 95.65%); other=(TE_BLRtx: PROTOP:classII:TIR: 12.03%, PROTOP_A:classII:TIR: 49.14%; TermRepeats: termTIR: 49; SSRCoverage=0.25<0.75)\n")
            f.write(
                "DTX-incomp_fTest05105818-B-G1-Map3\t1944\t-\tPotentialChimeric\tII\tTIR\tcomplete\tCI=33; coding=(TE_BLRtx: PROTOP:classII:TIR: 12.77%, PROTOP_A:classII:TIR: 25.16%, PROTOP_A:classII:TIR: 100.00%); struct=(TElength: <700bps; TermRepeats: termTIR: 844); other=(HG_BLRn: FBtr0089196_Dmel_r4.3: 29.48%; SSRCoverage=0.24<0.75)\n")
            f.write(
                "DTX-incomp_fTest05105818-B-G9-Map3_reversed\t1590\t-\tok\tII\tTIR\tincomplete\tCI=33; coding=(TE_BLRtx: PROTOP:classII:TIR: 10.92%, PROTOP:classII:TIR: 11.03%, PROTOP_A:classII:TIR: 55.20%); struct=(TElength: >700bps); other=(HG_BLRn: FBtr0089196_Dmel_r4.3: 35.60%; SSRCoverage=0.21<0.75)\n")
            f.write(
                "DTX-incomp_fTest05105818-B-P0.0-Map3\t1042\t.\tok\tII\tTIR\tincomplete\tCI=50; coding=(TE_BLRtx: PROTOP:classII:TIR: 17.39%, PROTOP_A:classII:TIR: 22.17%); struct=(TElength: >700bps; TermRepeats: termTIR: 50); other=(HG_BLRn: FBtr0089196_Dmel_r4.3: 47.22%; SSRCoverage=0.25<0.75)\n")
            f.write(
                "DTX-comp_fTest05105818-B-P1.0-Map9_reversed\t1137\t-\tok\tII\tTIR\tcomplete\tCI=50; coding=(TE_BLRtx: PROTOP:classII:TIR: 6.70%, PROTOP_A:classII:TIR: 66.43%, PROTOP_B:classII:TIR: 6.42%); struct=(TElength: >700bps; TermRepeats: termTIR: 52); other=(HG_BLRn: FBtr0089196_Dmel_r4.3: 51.19%; SSRCoverage=0.22<0.75)\n")
            f.write(
                "RLX-incomp_fTest05105818-B-R12-Map3_reversed\t2284\t-\tok\tI\tLTR\tincomplete\tCI=28; coding=(TE_BLRtx: ROOA_I:classI:LTR_retrotransposon: 27.57%, ROOA_LTR:classI:LTR_retrotransposon: 94.56%; TE_BLRx: BEL11_AGp:classI:LTR_retrotransposon: 19.47%, BEL2-I_Dmoj_1p:classI:LTR_retrotransposon: 11.49%); struct=(TElength: >700bps); other=(SSRCoverage=0.07<0.75)\n")
            f.write(
                "DTX-incomp_fTest05105818-B-R19-Map4\t705\t+\tok\tII\tTIR\tincomplete\tCI=66; coding=(TE_BLRtx: TC1-2_DM:classII:TIR: 42.70%; TE_BLRx: TC1-2_DMp:classII:TIR: 41.18%); struct=(TElength: >700bps); other=(SSRCoverage=0.14<0.75)\n")
            f.write(
                "DHX-incomp_fTest05105818-B-R1-Map4\t2367\t.\tok\tII\tHelitron\tincomplete\tCI=20; coding=(TE_BLRtx: DNAREP1_DM:classII:Helitron: 17.00%, DNAREP1_DYak:classII:Helitron: 9.08%); struct=(TElength: >700bps); other=(HG_BLRn: FBtr0089179_Dmel_r4.3: 13.52%; SSRCoverage=0.18<0.75)\n")
            f.write(
                "noCat_fTest05105818-B-R2-Map6\t4638\t.\tok\tnoCat\tnoCat\tNA\tCI=NA; coding=(HG_BLRn: FBtr0089179_Dmel_r4.3: 73.65%); struct=(SSRCoverage=0.05<0.75)\n")
            f.write(
                "PotentialHostGene-chim_fTest05105818-B-R4-Map5_reversed\t1067\t-\tPotentialChimeric\tNA\tPotentialHostGene\tNA\tCI=100; coding=(HG_BLRn: FBtr0089196_Dmel_r4.3: 99.91%); other=(TE_BLRtx: PROTOP:classII:TIR: 13.06%, PROTOP_A:classII:TIR: 37.47%; SSRCoverage=0.27<0.75)\n")
            f.write(
                "DTX-incomp_fTest05105818-B-R9-Map3_reversed\t714\t-\tok\tII\tTIR\tincomplete\tCI=66; coding=(TE_BLRtx: TC1_DM:classII:TIR: 40.88%; TE_BLRx: Tc1-1_TCa_1p:classII:TIR: 30.18%, Tc1-3_FR_1p:classII:TIR: 9.97%); struct=(TElength: >700bps); other=(SSRCoverage=0.08<0.75)\n")
            f.write(
                "1nc550_030\t1200\t-\tok\tII\tTIR\tincomplete\tCI=66; coding=(TE_BLRtx: TC1_DM:classII:TIR: 40.88%; TE_BLRx: Tc1-1_TCa_1p:classII:TIR: 30.18%, Tc1-3_FR_1p:classII:TIR: 9.97%); struct=(TElength: >700bps); other=(SSRCoverage=0.08<0.75)\n")


    def _writeNewClassifFile(self, inputFileName):
        with open(inputFileName, "w") as f:
            f.write(
                "Seq_name\tlength\tstrand\tconfused\tclass\torder\tWcode\tsFamily\tCI\tcoding\tstruct\tother\n")  # Ligne des entetes
            f.write(
                "PotentialHostGene-chim_fTest05105818-B-G11-Map20\t1240\t+\tTrue\tPotentialHostGene|NA\tNA|NA\tNA|NA\tNA|NA\t100|NA\tcoding=(HG_BLRn: FBtr0089196_Dmel_r4.3: 95.65%)\tstruct=(NA)\tother=(TE_BLRtx: PROTOP:classII:TIR: 12.03%, PROTOP_A:classII:TIR: 49.14%; TermRepeats: termTIR: 49; SSRCoverage=0.25<0.75)\n")
            f.write(
                "DTX-incomp_fTest05105818-B-G1-Map3\t1944\t-\tTrue\tII|NA\tTIR|NA\tDTX|NA\tNA|NA\t33|NA\tcoding=(TE_BLRtx: PROTOP:classII:TIR: 12.77%, PROTOP_A:classII:TIR: 25.16%, PROTOP_A:classII:TIR: 100.00%)\tstruct=(TElength: <700bps; TermRepeats: termTIR: 844)\tother=(HG_BLRn: FBtr0089196_Dmel_r4.3: 29.48%; SSRCoverage=0.24<0.75)\n")
            f.write(
                "DTX-incomp_fTest05105818-B-G9-Map3_reversed\t1590\t-\tFalse\tII\tTIR\tDTX\tNA\t33\tcoding=(TE_BLRtx: PROTOP:classII:TIR: 10.92%, PROTOP:classII:TIR: 11.03%, PROTOP_A:classII:TIR: 55.20%)\tstruct=(TElength: >700bps)\tother=(HG_BLRn: FBtr0089196_Dmel_r4.3: 35.60%; SSRCoverage=0.21<0.75)\n")
            f.write(
                "DTX-incomp_fTest05105818-B-P0.0-Map3\t1042\t+\tFalse\tII\tTIR\tDTX\tNA\t50\tcoding=(TE_BLRtx: PROTOP:classII:TIR: 17.39%, PROTOP_A:classII:TIR: 22.17%); struct=(TElength: >700bps; TermRepeats: termTIR: 50)\tstruct=(NA)\tother=(HG_BLRn: FBtr0089196_Dmel_r4.3: 47.22%; SSRCoverage=0.25<0.75)\n")
            f.write(
                "DTX-comp_fTest05105818-B-P1.0-Map9_reversed\t1137\t-\tFalse\tII\tTIR\tDTX\tNA\t50\tcoding=(TE_BLRtx: PROTOP:classII:TIR: 6.70%, PROTOP_A:classII:TIR: 66.43%, PROTOP_B:classII:TIR: 6.42%)\tstruct=(TElength: >700bps; TermRepeats: termTIR: 52)\tother=(HG_BLRn: FBtr0089196_Dmel_r4.3: 51.19%; SSRCoverage=0.22<0.75)\n")
            f.write(
                "RLX-incomp_fTest05105818-B-R12-Map3_reversed\t2284\t-\tFalse\tI\tLTR\tRLX\tNA\t28\tcoding=(TE_BLRtx: ROOA_I:classI:LTR_retrotransposon: 27.57%, ROOA_LTR:classI:LTR_retrotransposon: 94.56%; TE_BLRx: BEL11_AGp:classI:LTR_retrotransposon: 19.47%, BEL2-I_Dmoj_1p:classI:LTR_retrotransposon: 11.49%)\tstruct=(TElength: >700bps)\tother=(SSRCoverage=0.07<0.75)\tother=(NA)\n")
            f.write(
                "DTX-incomp_fTest05105818-B-R19-Map4\t705\t+\tFalse\tII\tTIR\tDTX\tNA\t66\tcoding=(TE_BLRtx: TC1-2_DM:classII:TIR: 42.70%; TE_BLRx: TC1-2_DMp:classII:TIR: 41.18%)\tstruct=(TElength: >700bps)\tother=(SSRCoverage=0.14<0.75)\n")
            f.write(
                "DHX-incomp_fTest05105818-B-R1-Map4\t2367\t+\tFalse\tII\tHelitron\tDHX\tNA\t20\tcoding=(TE_BLRtx: DNAREP1_DM:classII:Helitron: 17.00%, DNAREP1_DYak:classII:Helitron: 9.08%)\tstruct=(TElength: >700bps)\tother=(HG_BLRn: FBtr0089179_Dmel_r4.3: 13.52%; SSRCoverage=0.18<0.75)\n")
            f.write(
                "noCat_fTest05105818-B-R2-Map6\t4638\t.\tFalse\tUnclassified\tUnclassified\tNA\tNA\tNA\tcoding=(HG_BLRn: FBtr0089179_Dmel_r4.3: 73.65%)\tstruct=(SSRCoverage=0.05<0.75)\tother=(NA)\n")
            f.write(
                "PotentialHostGene-chim_fTest05105818-B-R4-Map5_reversed\t1067\t-\tTrue\tPotentialHostGene|NA\tNA|NA\tNA|NA\tNA|NA\t100|NA\tcoding=(HG_BLRn: FBtr0089196_Dmel_r4.3: 99.91%)\tstruct=(NA)\tother=(TE_BLRtx: PROTOP:classII:TIR: 13.06%, PROTOP_A:classII:TIR: 37.47%; SSRCoverage=0.27<0.75)\n")
            f.write(
                "DTX-incomp_fTest05105818-B-R9-Map3_reversed\t714\t-\tFalse\tII\tTIR\tDTX\tNA\t66\tcoding=(TE_BLRtx: TC1_DM:classII:TIR: 40.88%; TE_BLRx: Tc1-1_TCa_1p:classII:TIR: 30.18%, Tc1-3_FR_1p:classII:TIR: 9.97%)\tstruct=(TElength: >700bps)\tother=(SSRCoverage=0.08<0.75)\n")
            f.write(
                "1nc550_030\t1200\t-\tFalse\tII\tTIR\tDTX\tNA\t66\tcoding=(TE_BLRtx: TC1_DM:classII:TIR: 40.88%; TE_BLRx: Tc1-1_TCa_1p:classII:TIR: 30.18%, Tc1-3_FR_1p:classII:TIR: 9.97%)\tstruct=(TElength: >700bps)\tother=(SSRCoverage=0.08<0.75)\n")


    def _writePathFile_withClassif(self, inFileName):
        with open(inFileName, 'w') as f:
            f.write('1\tlm_SuperContig_29_v2\t193781\t194212\t1nc550_030\t228\t85\t1e-40\t84\t30.56\n')
            f.write('2\tlm_SuperContig_29_v2\t192832\t193704\t1nc550_030\t522\t229\t1e-40\t106\t23.99\n')
            f.write(
                '3\tlm_SuperContig_30_v2\t78081\t78088\tDHX-incomp_fTest05105818-B-R1-Map4\t19\t209\t3e-21\t101\t30.89\n')
            f.write(
                '3\tlm_SuperContig_30_v2\t78089\t78588\tDHX-incomp_fTest05105818-B-R1-Map4\t150\t350\t3e-22\t101\t35.89\n')
            f.write(
                '4\tlm_SuperContig_30_v2\t88031\t88080\tDTX-incomp_fTest05105818-B-G1-Map3\t370\t420\t3e-23\t101\t31.89\n')
            f.write(
                '5\tlm_SuperContig_30_v2\t108588\t108081\tDTX-incomp_fTest05105818-B-G9-Map3_reversed\t590\t820\t3e-24\t101\t32.89\n')
            f.write(
                '6\tlm_SuperContig_30_v2\t118081\t118588\tPotentialHostGene-chim_fTest05105818-B-R4-Map5_reversed\t154\t289\t3e-25\t101\t33.89\n')
            f.write(
                '7\tlm_SuperContig_30_v2\t288031\t288080\tnoCat_fTest05105818-B-R2-Map6\t1900\t2090\t3e-26\t101\t34.89\n')


    def _writePathFile_withClassif_withIdenticalMatches(self, inFileName):
        with open(inFileName, 'w') as f:
            f.write('1\tlm_SuperContig_29_v2\t193781\t194212\t1nc550_030\t228\t85\t1e-40\t84\t30.56\n')
            f.write('2\tlm_SuperContig_29_v2\t192832\t193704\t1nc550_030\t522\t229\t1e-40\t106\t23.99\n')
            f.write(
                '3\tlm_SuperContig_30_v2\t78081\t78088\tDHX-incomp_fTest05105818-B-R1-Map4\t19\t209\t3e-21\t101\t30.89\n')
            f.write(
                '3\tlm_SuperContig_30_v2\t78089\t78588\tDHX-incomp_fTest05105818-B-R1-Map4\t150\t350\t3e-22\t101\t35.89\n')
            f.write(
                '4\tlm_SuperContig_30_v2\t88031\t88080\tDTX-incomp_fTest05105818-B-G1-Map3\t370\t420\t3e-23\t101\t31.89\n')
            f.write(
                '5\tlm_SuperContig_30_v2\t108588\t108081\tDTX-incomp_fTest05105818-B-G9-Map3_reversed\t590\t820\t3e-24\t101\t32.89\n')
            f.write(
                '6\tlm_SuperContig_30_v2\t118081\t118588\tPotentialHostGene-chim_fTest05105818-B-R4-Map5_reversed\t154\t289\t3e-25\t101\t33.89\n')
            f.write(
                '7\tlm_SuperContig_30_v2\t288031\t288080\tnoCat_fTest05105818-B-R2-Map6\t1900\t2090\t3e-26\t101\t34.89\n')
            # These pathId 8, 9, 10 are identiqual than pathId:7 and are otherTarget for pathId:7
            f.write(
                '8\tlm_SuperContig_30_v2\t288031\t288080\tDTX-incomp_fTest05105818-B-P0.0-Map3\t100\t190\t3e-26\t101\t39.89\n')
            f.write(
                '9\tlm_SuperContig_30_v2\t288031\t288080\tRLX-incomp_fTest05105818-B-R12-Map3_reversed\t1100\t1290\t3e-26\t101\t40.89\n')
            f.write(
                '10\tlm_SuperContig_30_v2\t288031\t288080\tPotentialHostGene-chim_fTest05105818-B-G11-Map20\t990\t1890\t3e-26\t101\t38.09\n')

            f.write(
                '11\tlm_SuperContig_30_v2\t288031\t288080\tDTX-incomp_fTest05105818-B-G1-Map3\t990\t1890\t3e-26\t301\t38.09\n')

            f.write(
                '12\tlm_SuperContig_30_v2\t388031\t388080\tDHX-incomp_fTest05105818-B-R1-Map4\t19\t209\t3e-21\t101\t30.89\n')
            f.write(
                '12\tlm_SuperContig_30_v2\t388081\t388380\tDHX-incomp_fTest05105818-B-R1-Map4\t150\t350\t3e-22\t101\t35.89\n')
            # this pathID:13 is identiqual than 13 and it is otherTarget for pathId:12
            f.write(
                '13\tlm_SuperContig_30_v2\t388031\t388080\tDTX-incomp_fTest05105818-B-P0.0-Map3\t119\t309\t3e-21\t101\t30.89\n')
            f.write(
                '13\tlm_SuperContig_30_v2\t388081\t388380\tDTX-incomp_fTest05105818-B-P0.0-Map3\t250\t450\t3e-22\t101\t35.89\n')


    def _writeAlignTabFile(self, inFileName):
        with open(inFileName, 'w') as f:
            f.write('PathId\talignLength\talignIdent\tscore\n')
            f.write('1\t1200\t31.02\t868\n')
            f.write('2\t1210\t23.99\t436\n')
            f.write('3\t2367\t32.31\t672\n')
            f.write('4\t1944\t89.95\t1436\n')
            f.write('5\t1591\t40.36\t1436\n')
            f.write('6\t1067\t30.60\t1906\n')
            f.write('7\t4638\t25.38\t894\n')
            f.write('8\t1042\t32.85\t4196\n')
            f.write('9\t2284\t40.89\t4196\n')
            f.write('10\t1240\t50.52\t868\n')
            f.write('11\t1946\t40.01\t4324\n')
            f.write('12\t2367\t33.23\t4324\n')
            f.write('13\t1042\t30.89\t4324\n')


    def _writeAlignTabFile2(self, inFileName):
        with open(inFileName, 'w') as f:
            f.write('PathId\talignLength\talignIdent\tscore\n')
            f.write('6\t296\t91.24\t1475\n')
            f.write('21\t126\t84.4417\t65\n')
            f.write('35\t247\t91.24\t1475\n')

    def _writeExpPathGFFFile_without_seq_withClassif(self, inFileName):
        with open(inFileName, 'w') as f:
            f.write("##gff-version 3\n")
            f.write("##sequence-region lm_SuperContig_30_v2 1 120\n")
            f.write(
                "lm_SuperContig_30_v2\tPN_REPET_TEs\tmatch\t78081\t78588\t0.0\t+\t.\tID=ms3_lm_SuperContig_30_v2_DHX-incomp_fTest05105818-B-R1-Map4;Target=DHX-incomp_fTest05105818-B-R1-Map4 19 350;TargetLength=2367;TargetDescription=Wcode:DHX CI:20 coding:(TE_BLRtx: DNAREP1_DM:classII:Helitron: 17.00% | DNAREP1_DYak:classII:Helitron: 9.08%) struct:(TElength: >700bps) other:(HG_BLRn: FBtr0089179_Dmel_r4.3: 13.52% SSRCoverage:0.18<0.75);AlignIdentity=32.31;AlignLength=2367\n")
            f.write(
                "lm_SuperContig_30_v2\tPN_REPET_TEs\tmatch_part\t78081\t78088\t3e-21\t+\t.\tID=mp3-1_lm_SuperContig_30_v2_DHX-incomp_fTest05105818-B-R1-Map4;Parent=ms3_lm_SuperContig_30_v2_DHX-incomp_fTest05105818-B-R1-Map4;Target=DHX-incomp_fTest05105818-B-R1-Map4 19 209;Identity=30.89\n")
            f.write(
                "lm_SuperContig_30_v2\tPN_REPET_TEs\tmatch_part\t78089\t78588\t3e-22\t+\t.\tID=mp3-2_lm_SuperContig_30_v2_DHX-incomp_fTest05105818-B-R1-Map4;Parent=ms3_lm_SuperContig_30_v2_DHX-incomp_fTest05105818-B-R1-Map4;Target=DHX-incomp_fTest05105818-B-R1-Map4 150 350;Identity=35.89\n")

            f.write(
                "lm_SuperContig_30_v2\tPN_REPET_TEs\tmatch\t88031\t88080\t0.0\t+\t.\tID=ms4_lm_SuperContig_30_v2_DTX-incomp_fTest05105818-B-G1-Map3;Target=DTX-incomp_fTest05105818-B-G1-Map3 370 420;TargetLength=1944;TargetDescription=Wcode:DTX|NA CI:33|NA coding:(TE_BLRtx: PROTOP:classII:TIR: 12.77% | PROTOP_A:classII:TIR: 25.16% | PROTOP_A:classII:TIR: 100.00%) struct:(TElength: <700bps TermRepeats: termTIR: 844) other:(HG_BLRn: FBtr0089196_Dmel_r4.3: 29.48% SSRCoverage:0.24<0.75);AlignIdentity=89.95;AlignLength=1944\n")
            f.write(
                "lm_SuperContig_30_v2\tPN_REPET_TEs\tmatch_part\t88031\t88080\t3e-23\t+\t.\tID=mp4-1_lm_SuperContig_30_v2_DTX-incomp_fTest05105818-B-G1-Map3;Parent=ms4_lm_SuperContig_30_v2_DTX-incomp_fTest05105818-B-G1-Map3;Target=DTX-incomp_fTest05105818-B-G1-Map3 370 420;Identity=31.89\n")

            f.write(
                "lm_SuperContig_30_v2\tPN_REPET_TEs\tmatch\t108081\t108588\t0.0\t-\t.\tID=ms5_lm_SuperContig_30_v2_DTX-incomp_fTest05105818-B-G9-Map3_reversed;Target=DTX-incomp_fTest05105818-B-G9-Map3_reversed 590 820;TargetLength=1590;TargetDescription=Wcode:DTX CI:33 coding:(TE_BLRtx: PROTOP:classII:TIR: 10.92% | PROTOP:classII:TIR: 11.03% | PROTOP_A:classII:TIR: 55.20%) struct:(TElength: >700bps) other:(HG_BLRn: FBtr0089196_Dmel_r4.3: 35.60% SSRCoverage:0.21<0.75);AlignIdentity=40.36;AlignLength=1591\n")
            f.write(
                "lm_SuperContig_30_v2\tPN_REPET_TEs\tmatch_part\t108081\t108588\t3e-24\t-\t.\tID=mp5-1_lm_SuperContig_30_v2_DTX-incomp_fTest05105818-B-G9-Map3_reversed;Parent=ms5_lm_SuperContig_30_v2_DTX-incomp_fTest05105818-B-G9-Map3_reversed;Target=DTX-incomp_fTest05105818-B-G9-Map3_reversed 590 820;Identity=32.89\n")

            f.write(
                "lm_SuperContig_30_v2\tPN_REPET_TEs\tmatch\t118081\t118588\t0.0\t+\t.\tID=ms6_lm_SuperContig_30_v2_PotentialHostGene-chim_fTest05105818-B-R4-Map5_reversed;Target=PotentialHostGene-chim_fTest05105818-B-R4-Map5_reversed 154 289;TargetLength=1067;TargetDescription=Wcode:NA|NA CI:100|NA coding:(HG_BLRn: FBtr0089196_Dmel_r4.3: 99.91%) struct:(NA) other:(TE_BLRtx: PROTOP:classII:TIR: 13.06% | PROTOP_A:classII:TIR: 37.47% SSRCoverage:0.27<0.75);AlignIdentity=30.60;AlignLength=1067\n")
            f.write(
                "lm_SuperContig_30_v2\tPN_REPET_TEs\tmatch_part\t118081\t118588\t3e-25\t+\t.\tID=mp6-1_lm_SuperContig_30_v2_PotentialHostGene-chim_fTest05105818-B-R4-Map5_reversed;Parent=ms6_lm_SuperContig_30_v2_PotentialHostGene-chim_fTest05105818-B-R4-Map5_reversed;Target=PotentialHostGene-chim_fTest05105818-B-R4-Map5_reversed 154 289;Identity=33.89\n")

            f.write(
                "lm_SuperContig_30_v2\tPN_REPET_TEs\tmatch\t288031\t288080\t0.0\t+\t.\tID=ms7_lm_SuperContig_30_v2_noCat_fTest05105818-B-R2-Map6;Target=noCat_fTest05105818-B-R2-Map6 1900 2090;TargetLength=4638;TargetDescription=Wcode:NA CI:NA coding:(HG_BLRn: FBtr0089179_Dmel_r4.3: 73.65%) struct:(SSRCoverage:0.05<0.75) other:(NA);AlignIdentity=25.38;AlignLength=4638\n")
            f.write(
                "lm_SuperContig_30_v2\tPN_REPET_TEs\tmatch_part\t288031\t288080\t3e-26\t+\t.\tID=mp7-1_lm_SuperContig_30_v2_noCat_fTest05105818-B-R2-Map6;Parent=ms7_lm_SuperContig_30_v2_noCat_fTest05105818-B-R2-Map6;Target=noCat_fTest05105818-B-R2-Map6 1900 2090;Identity=34.89\n")



    def _writeExpPathGFFFile_without_seq_withClassif_withIdenticalMatches(self, inFileName):
        with open(inFileName, 'w') as f:
            f.write("##gff-version 3\n")
            f.write("##sequence-region lm_SuperContig_30_v2 1 120\n")
            f.write(
                "lm_SuperContig_30_v2\tPN_REPET_TEs\tmatch\t78081\t78588\t0.0\t+\t.\tID=ms3_lm_SuperContig_30_v2_DHX-incomp_fTest05105818-B-R1-Map4;Target=DHX-incomp_fTest05105818-B-R1-Map4 19 350;TargetLength=2367;TargetDescription=CI:20 coding:(TE_BLRtx: DNAREP1_DM:classII:Helitron: 17.00% | DNAREP1_DYak:classII:Helitron: 9.08%) struct:(TElength: >700bps) other:(HG_BLRn: FBtr0089179_Dmel_r4.3: 13.52% SSRCoverage:0.18<0.75);AlignIdentity=32.31;AlignLength=2367\n")
            f.write(
                "lm_SuperContig_30_v2\tPN_REPET_TEs\tmatch_part\t78081\t78088\t3e-21\t+\t.\tID=mp3-1_lm_SuperContig_30_v2_DHX-incomp_fTest05105818-B-R1-Map4;Parent=ms3_lm_SuperContig_30_v2_DHX-incomp_fTest05105818-B-R1-Map4;Target=DHX-incomp_fTest05105818-B-R1-Map4 19 209;Identity=30.89\n")
            f.write(
                "lm_SuperContig_30_v2\tPN_REPET_TEs\tmatch_part\t78089\t78588\t3e-22\t+\t.\tID=mp3-2_lm_SuperContig_30_v2_DHX-incomp_fTest05105818-B-R1-Map4;Parent=ms3_lm_SuperContig_30_v2_DHX-incomp_fTest05105818-B-R1-Map4;Target=DHX-incomp_fTest05105818-B-R1-Map4 150 350;Identity=35.89\n")

            f.write(
                "lm_SuperContig_30_v2\tPN_REPET_TEs\tmatch\t88031\t88080\t0.0\t+\t.\tID=ms4_lm_SuperContig_30_v2_DTX-incomp_fTest05105818-B-G1-Map3;Target=DTX-incomp_fTest05105818-B-G1-Map3 370 420;TargetLength=1944;TargetDescription=CI:33 coding:(TE_BLRtx: PROTOP:classII:TIR: 12.77% | PROTOP_A:classII:TIR: 25.16% | PROTOP_A:classII:TIR: 100.00%) struct:(TElength: <700bps TermRepeats: termTIR: 844) other:(HG_BLRn: FBtr0089196_Dmel_r4.3: 29.48% SSRCoverage:0.24<0.75);AlignIdentity=89.95;AlignLength=1944\n")
            f.write(
                "lm_SuperContig_30_v2\tPN_REPET_TEs\tmatch_part\t88031\t88080\t3e-23\t+\t.\tID=mp4-1_lm_SuperContig_30_v2_DTX-incomp_fTest05105818-B-G1-Map3;Parent=ms4_lm_SuperContig_30_v2_DTX-incomp_fTest05105818-B-G1-Map3;Target=DTX-incomp_fTest05105818-B-G1-Map3 370 420;Identity=31.89\n")

            f.write(
                "lm_SuperContig_30_v2\tPN_REPET_TEs\tmatch\t108081\t108588\t0.0\t-\t.\tID=ms5_lm_SuperContig_30_v2_DTX-incomp_fTest05105818-B-G9-Map3_reversed;Target=DTX-incomp_fTest05105818-B-G9-Map3_reversed 590 820;TargetLength=1590;TargetDescription=CI:33 coding:(TE_BLRtx: PROTOP:classII:TIR: 10.92% | PROTOP:classII:TIR: 11.03% | PROTOP_A:classII:TIR: 55.20%) struct:(TElength: >700bps) other:(HG_BLRn: FBtr0089196_Dmel_r4.3: 35.60% SSRCoverage:0.21<0.75);AlignIdentity=40.36;AlignLength=1591\n")
            f.write(
                "lm_SuperContig_30_v2\tPN_REPET_TEs\tmatch_part\t108081\t108588\t3e-24\t-\t.\tID=mp5-1_lm_SuperContig_30_v2_DTX-incomp_fTest05105818-B-G9-Map3_reversed;Parent=ms5_lm_SuperContig_30_v2_DTX-incomp_fTest05105818-B-G9-Map3_reversed;Target=DTX-incomp_fTest05105818-B-G9-Map3_reversed 590 820;Identity=32.89\n")

            f.write(
                "lm_SuperContig_30_v2\tPN_REPET_TEs\tmatch\t118081\t118588\t0.0\t+\t.\tID=ms6_lm_SuperContig_30_v2_PotentialHostGene-chim_fTest05105818-B-R4-Map5_reversed;Target=PotentialHostGene-chim_fTest05105818-B-R4-Map5_reversed 154 289;TargetLength=1067;TargetDescription=CI:100 coding:(HG_BLRn: FBtr0089196_Dmel_r4.3: 99.91%) other:(TE_BLRtx: PROTOP:classII:TIR: 13.06% | PROTOP_A:classII:TIR: 37.47% SSRCoverage:0.27<0.75);AlignIdentity=30.60;AlignLength=1067\n")
            f.write(
                "lm_SuperContig_30_v2\tPN_REPET_TEs\tmatch_part\t118081\t118588\t3e-25\t+\t.\tID=mp6-1_lm_SuperContig_30_v2_PotentialHostGene-chim_fTest05105818-B-R4-Map5_reversed;Parent=ms6_lm_SuperContig_30_v2_PotentialHostGene-chim_fTest05105818-B-R4-Map5_reversed;Target=PotentialHostGene-chim_fTest05105818-B-R4-Map5_reversed 154 289;Identity=33.89\n")

            f.write(
                "lm_SuperContig_30_v2\tPN_REPET_TEs\tmatch\t288031\t288080\t0.0\t+\t.\tID=ms9_lm_SuperContig_30_v2_RLX-incomp_fTest05105818-B-R12-Map3_reversed;Target=RLX-incomp_fTest05105818-B-R12-Map3_reversed 1100 1290;TargetLength=2284;TargetDescription=CI:28 coding:(TE_BLRtx: ROOA_I:classI:LTR_retrotransposon: 27.57% | ROOA_LTR:classI:LTR_retrotransposon: 94.56% TE_BLRx: BEL11_AGp:classI:LTR_retrotransposon: 19.47% | BEL2-I_Dmoj_1p:classI:LTR_retrotransposon: 11.49%) struct:(TElength: >700bps) other:(SSRCoverage:0.07<0.75);AlignIdentity=40.89;AlignLength=2284;OtherTargets=DTX-incomp_fTest05105818-B-P0.0-Map3 100 190, PotentialHostGene-chim_fTest05105818-B-G11-Map20 990 1890, noCat_fTest05105818-B-R2-Map6 1900 2090\n")
            f.write(
                "lm_SuperContig_30_v2\tPN_REPET_TEs\tmatch_part\t288031\t288080\t3e-26\t+\t.\tID=mp9-1_lm_SuperContig_30_v2_RLX-incomp_fTest05105818-B-R12-Map3_reversed;Parent=ms9_lm_SuperContig_30_v2_RLX-incomp_fTest05105818-B-R12-Map3_reversed;Target=RLX-incomp_fTest05105818-B-R12-Map3_reversed 1100 1290\n")

            f.write(
                "lm_SuperContig_30_v2\tPN_REPET_TEs\tmatch\t288031\t288080\t0.0\t+\t.\tID=ms11_lm_SuperContig_30_v2_DTX-incomp_fTest05105818-B-G1-Map3;Target=DTX-incomp_fTest05105818-B-G1-Map3 990 1890;TargetLength=1944;TargetDescription=CI:33 coding:(TE_BLRtx: PROTOP:classII:TIR: 12.77% | PROTOP_A:classII:TIR: 25.16% | PROTOP_A:classII:TIR: 100.00%) struct:(TElength: <700bps TermRepeats: termTIR: 844) other:(HG_BLRn: FBtr0089196_Dmel_r4.3: 29.48% SSRCoverage:0.24<0.75);AlignIdentity=40.01;AlignLength=1946\n")
            f.write(
                "lm_SuperContig_30_v2\tPN_REPET_TEs\tmatch_part\t288031\t288080\t3e-26\t+\t.\tID=mp11-1_lm_SuperContig_30_v2_DTX-incomp_fTest05105818-B-G1-Map3;Parent=ms11_lm_SuperContig_30_v2_DTX-incomp_fTest05105818-B-G1-Map3;Target=DTX-incomp_fTest05105818-B-G1-Map3 990 1890;Identity=38.09\n")

            # TODO:
            # Should this case really occur : If merging multiple match-parts, the current behaviour needs to be fixed to get correct subject start/end coordinates
            f.write(
                "lm_SuperContig_30_v2\tPN_REPET_TEs\tmatch\t388031\t388380\t0.0\t+\t.\tID=ms12_lm_SuperContig_30_v2_DHX-incomp_fTest05105818-B-R1-Map4;Target=DHX-incomp_fTest05105818-B-R1-Map4 19 350;TargetLength=2367;TargetDescription=CI:20 coding:(TE_BLRtx: DNAREP1_DM:classII:Helitron: 17.00% | DNAREP1_DYak:classII:Helitron: 9.08%) struct:(TElength: >700bps) other:(HG_BLRn: FBtr0089179_Dmel_r4.3: 13.52% SSRCoverage:0.18<0.75);AlignIdentity=33.23;AlignLength=2367;OtherTargets=DTX-incomp_fTest05105818-B-P0.0-Map3 119 309\n")
            f.write(
                "lm_SuperContig_30_v2\tPN_REPET_TEs\tmatch_part\t388031\t388080\t3e-21\t+\t.\tID=mp12-1_lm_SuperContig_30_v2_DHX-incomp_fTest05105818-B-R1-Map4;Parent=ms12_lm_SuperContig_30_v2_DHX-incomp_fTest05105818-B-R1-Map4;Target=DHX-incomp_fTest05105818-B-R1-Map4 19 209\n")
            f.write(
                "lm_SuperContig_30_v2\tPN_REPET_TEs\tmatch_part\t388081\t388380\t3e-22\t+\t.\tID=mp12-2_lm_SuperContig_30_v2_DHX-incomp_fTest05105818-B-R1-Map4;Parent=ms12_lm_SuperContig_30_v2_DHX-incomp_fTest05105818-B-R1-Map4;Target=DHX-incomp_fTest05105818-B-R1-Map4 150 350\n")


    def _writeTablesFile_withTESeqTables(self, tableType):
        with open(self._tablesFileName, "w") as tableFile:
            string = "{}_REPET_TEs\t{}\t{}_chr_allTEs_nr_noSSR_join_{}\t{}_refTEs_seq\n".format(self._projectName,
                                                                                                tableType,
                                                                                                self._projectName,
                                                                                                tableType,
                                                                                                self._projectName)
            tableFile.write(string)
            string = "{}_REPET_tblasx\t{}\t{}_chr_bankBLRtx_{}\t{}_bankBLRtx_nt_seq\n".format(self._projectName, tableType,
                                                                                              self._projectName, tableType,
                                                                                              self._projectName)
            tableFile.write(string)

    def _writeTablesFile(self, tableType):
        with open(self._tablesFileName, "w") as tableFile:
            string = "{}_REPET_TEs\t{}\t{}_chr_allTEs_nr_noSSR_join_{}\n".format(self._projectName, tableType,
                                                                                 self._projectName, tableType)
            tableFile.write(string)


    def _writePathFile(self, inFileName):
        with open(inFileName, 'w') as f:
            f.write('1\tlm_SuperContig_29_v2\t193781\t194212\tDTX-incomp_DmelChr4-L-B1-Map3\t228\t85\t1e-40\t84\t30.56\n')
            f.write('2\tlm_SuperContig_29_v2\t192832\t193704\tDTX-incomp_DmelChr4-L-B1-Map3\t522\t229\t1e-40\t106\t23.99\n')
            f.write('3\tlm_SuperContig_29_v2\t78031\t78080\tDTX-incomp_DmelChr4-B-P0.0-Map3\t19\t209\t3e-21\t101\t30.89\n')
            f.write('3\tlm_SuperContig_29_v2\t78081\t78588\tDTX-incomp_DmelChr4-B-P0.0-Map3\t19\t209\t3e-21\t101\t30.89\n')


    def _writePathFile2(self, inFileName):
        with open(inFileName, 'w') as f:
            f.write('1\tchr1\t1\t100\tPROTOP_B:classII:TIR\t228\t85\t1e-20\t84\t30.56\n')
            f.write('2\tchr1\t100\t1000\tDMRT1C:classI:?\t228\t85\t1e-30\t84\t30.56\n')


    def _writePathFile_refTEs_annotation(self, inFileName):
        with open(inFileName, 'w') as f:
            f.write('6\tdmel_chr4\t4364\t4611\tDTX-incomp_DmelChr4-L-B1-Map3\t854\t1150\t0\t1475\t91.24\n')
            f.write('21\tdmel_chr4\t4630\t4704\tDTX-incomp_DmelChr4-B-P0.0-Map3\t232\t312\t0\t65\t84.4417\n')
            f.write('21\tdmel_chr4\t4837\t4889\tDTX-incomp_DmelChr4-B-P0.0-Map3\t456\t512\t0\t46\t84.4417\n')
            f.write('35\tdmel_chr1\t4364\t4611\tDTX-incomp_DmelChr4-L-B1-Map3\t854\t1150\t0\t1475\t91.24\n')


    def _writePathFile_bankBLRtx_annotation(self, inFileName):
        with open(inFileName, 'w') as f:
            f.write('27\tdmel_chr4\t4412\t4501\tPROTOP_B:classII:TIR\t1082\t993\t5e-108\t702\t91.3066\n')
            f.write('27\tdmel_chr4\t4483\t4652\tPROTOP_B:classII:TIR\t990\t821\t9e-146\t707\t86.5572\n')
            f.write('27\tdmel_chr4\t4672\t4775\tPROTOP_B:classII:TIR\t787\t684\t9e-146\t707\t88.8013\n')
            f.write('27\tdmel_chr4\t4782\t4889\tPROTOP_B:classII:TIR\t661\t553\t9e-141\t356\t53.7067\n')
            f.write('66\tdmel_chr4\t3143\t3361\tDMRT1C:classI:?\t2206\t1988\t0\t1878\t89.7202\n')
            f.write('66\tdmel_chr4\t3345\t3410\tDMRT1C:classI:?\t1719\t1654\t0\t313\t54.55\n')
            f.write('66\tdmel_chr4\t3448\t3572\tDMRT1C:classI:?\t1619\t1495\t0\t1252\t77.3433\n')
            f.write('66\tdmel_chr4\t3564\t3693\tDMRT1C:classI:?\t1509\t1380\t0\t1565\t83.7306\n')
            f.write('66\tdmel_chr4\t3705\t3857\tDMRT1C:classI:?\t691\t539\t0\t1252\t84.5762\n')
            f.write('66\tdmel_chr4\t3861\t3992\tDMRT1C:classI:?\t534\t403\t0\t1565\t67.1422\n')
            f.write('66\tdmel_chr4\t3985\t4106\tDMRT1C:classI:?\t411\t290\t0\t1252\t76.612\n')
            f.write('66\tdmel_chr4\t4102\t4248\tDMRT1C:classI:?\t285\t139\t0\t1565\t75.3027\n')
            f.write('66\tdmel_chr4\t4236\t4364\tDMRT1C:classI:?\t152\t24\t0\t1565\t66.6657\n')
            f.write('141\tdmel_chr4\t4917\t5007\tBATUMI_I:classI:LTR_retrotransposon\t7030\t7120\t2e-144\t984\t94.1756\n')
            f.write('141\tdmel_chr4\t4976\t5195\tBATUMI_I:classI:LTR_retrotransposon\t7087\t7303\t2e-144\t2098\t82.2343\n')


    def _writePathFileReverse(self, inFileName):
        with open(inFileName, 'w') as f:
            f.write(
                '1\tlm_SuperContig_29_v2\t193781\t194212\t1nc550_030 related to putative multidrug transporter Mfs1.1 (major facilitator family protein)\t228\t85\t1e-40\t84\t30.56\n')
            f.write(
                '2\tlm_SuperContig_29_v2\t192832\t193704\t1nc550_030 related to putative multidrug transporter Mfs1.1 (major facilitator family protein)\t522\t229\t1e-40\t106\t23.99\n')
            f.write(
                '3\tlm_SuperContig_29_v2\t78080\t78031\txnc164_090 related to multidrug resistance protein\t19\t209\t3e-21\t101\t30.89\n')
            f.write(
                '3\tlm_SuperContig_29_v2\t78588\t78081\txnc164_090 related to multidrug resistance protein\t19\t209\t3e-21\t101\t30.89\n')


    def _writeSetFile(self, inFileName):
        with open(inFileName, 'w') as f:
            f.write('1\tset1\tlm_SuperContig_29_v2\t193781\t194212\n')
            f.write('2\tset2\tlm_SuperContig_29_v2\t192832\t193704\n')
            f.write('3\tset3\tlm_SuperContig_29_v2\t78031\t78080\n')
            f.write('3\tset3\tlm_SuperContig_29_v2\t78081\t78588\n')


    def _writeSetFileReverse(self, inFileName):
        with open(inFileName, 'w') as f:
            f.write('1\tset1\tlm_SuperContig_29_v2\t193781\t194212\n')
            f.write('2\tset2\tlm_SuperContig_29_v2\t192832\t193704\n')
            f.write('3\tset3\tlm_SuperContig_29_v2\t78080\t78031\n')
            f.write('3\tset3\tlm_SuperContig_29_v2\t78588\t78081\n')


    def _writeExpEmptyPathGFFFile(self, inFileName):
        with open(inFileName, 'w') as f:
            f.write("##gff-version 3\n")
            f.write("##sequence-region lm_SuperContig_30_v2 1 120\n")


    def _writeExpPathGFFFile(self, inFileName):
        with open(inFileName, 'w') as f:
            f.write("##gff-version 3\n")
            f.write("##sequence-region lm_SuperContig_29_v2 1 120\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch\t78031\t78588\t0.0\t+\t.\tID=ms3_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-B-P0.0-Map3;Target=DTX-incomp_DmelChr4-B-P0.0-Map3 19 209;AlignIdentity=32.31;AlignLength=2367\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch_part\t78031\t78080\t3e-21\t+\t.\tID=mp3-1_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-B-P0.0-Map3;Parent=ms3_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-B-P0.0-Map3;Target=DTX-incomp_DmelChr4-B-P0.0-Map3 19 209;Identity=30.89\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch_part\t78081\t78588\t3e-21\t+\t.\tID=mp3-2_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-B-P0.0-Map3;Parent=ms3_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-B-P0.0-Map3;Target=DTX-incomp_DmelChr4-B-P0.0-Map3 19 209;Identity=30.89\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch\t192832\t193704\t0.0\t-\t.\tID=ms2_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-L-B1-Map3;Target=DTX-incomp_DmelChr4-L-B1-Map3 229 522;AlignIdentity=23.99;AlignLength=1210\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch_part\t192832\t193704\t1e-40\t-\t.\tID=mp2-1_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-L-B1-Map3;Parent=ms2_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-L-B1-Map3;Target=DTX-incomp_DmelChr4-L-B1-Map3 229 522;Identity=23.99\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch\t193781\t194212\t0.0\t-\t.\tID=ms1_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-L-B1-Map3;Target=DTX-incomp_DmelChr4-L-B1-Map3 85 228;AlignIdentity=31.02;AlignLength=1200\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch_part\t193781\t194212\t1e-40\t-\t.\tID=mp1-1_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-L-B1-Map3;Parent=ms1_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-L-B1-Map3;Target=DTX-incomp_DmelChr4-L-B1-Map3 85 228;Identity=30.56\n")
            f.write("##FASTA\n")
            self._writeSeq1(f)


    def _writeExpEmptyPathGFFFileWithSeq(self, inFileName):
        with open(inFileName, 'w') as f:
            f.write("##gff-version 3\n")
            f.write("##sequence-region lm_SuperContig_30_v2 1 120\n")
            f.write("##FASTA\n")
            self._writeSeq2(f)


    def _writeExpPathGFFFile_without_seq(self, inFileName):
        with open(inFileName, 'w') as f:
            f.write("##gff-version 3\n")
            f.write("##sequence-region lm_SuperContig_29_v2 1 120\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch\t78031\t78588\t0.0\t+\t.\tID=ms3_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-B-P0.0-Map3;Target=DTX-incomp_DmelChr4-B-P0.0-Map3 19 209;AlignIdentity=32.31;AlignLength=2367\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch_part\t78031\t78080\t3e-21\t+\t.\tID=mp3-1_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-B-P0.0-Map3;Parent=ms3_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-B-P0.0-Map3;Target=DTX-incomp_DmelChr4-B-P0.0-Map3 19 209;Identity=30.89\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch_part\t78081\t78588\t3e-21\t+\t.\tID=mp3-2_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-B-P0.0-Map3;Parent=ms3_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-B-P0.0-Map3;Target=DTX-incomp_DmelChr4-B-P0.0-Map3 19 209;Identity=30.89\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch\t192832\t193704\t0.0\t-\t.\tID=ms2_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-L-B1-Map3;Target=DTX-incomp_DmelChr4-L-B1-Map3 229 522;AlignIdentity=23.99;AlignLength=1210\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch_part\t192832\t193704\t1e-40\t-\t.\tID=mp2-1_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-L-B1-Map3;Parent=ms2_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-L-B1-Map3;Target=DTX-incomp_DmelChr4-L-B1-Map3 229 522;Identity=23.99\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch\t193781\t194212\t0.0\t-\t.\tID=ms1_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-L-B1-Map3;Target=DTX-incomp_DmelChr4-L-B1-Map3 85 228;AlignIdentity=31.02;AlignLength=1200\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch_part\t193781\t194212\t1e-40\t-\t.\tID=mp1-1_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-L-B1-Map3;Parent=ms1_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-L-B1-Map3;Target=DTX-incomp_DmelChr4-L-B1-Map3 85 228;Identity=30.56\n")


    def _writeExpPathGFFFile_without_seq_and_match_part_not_compulsory(self, inFileName):
        with open(inFileName, 'w') as f :
            f.write("##gff-version 3\n")
            f.write("##sequence-region lm_SuperContig_29_v2 1 120\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch\t78031\t78588\t0.0\t+\t.\tID=ms3_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-B-P0.0-Map3;Target=DTX-incomp_DmelChr4-B-P0.0-Map3 19 209;AlignIdentity=32.31;AlignLength=2367\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch_part\t78031\t78080\t3e-21\t+\t.\tID=mp3-1_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-B-P0.0-Map3;Parent=ms3_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-B-P0.0-Map3;Target=DTX-incomp_DmelChr4-B-P0.0-Map3 19 209;Identity=30.89\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch_part\t78081\t78588\t3e-21\t+\t.\tID=mp3-2_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-B-P0.0-Map3;Parent=ms3_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-B-P0.0-Map3;Target=DTX-incomp_DmelChr4-B-P0.0-Map3 19 209;Identity=30.89\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch\t192832\t193704\t0.0\t-\t.\tID=ms2_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-L-B1-Map3;Target=DTX-incomp_DmelChr4-L-B1-Map3 229 522;AlignIdentity=23.99;AlignLength=1210\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch\t193781\t194212\t0.0\t-\t.\tID=ms1_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-L-B1-Map3;Target=DTX-incomp_DmelChr4-L-B1-Map3 85 228;AlignIdentity=31.02;AlignLength=1200\n")


    def _writeExpPathGFFFileReversed(self, inFileName):
        with open(inFileName, 'w') as f:
            f.write("##gff-version 3\n")
            f.write("##sequence-region lm_SuperContig_29_v2 1 120\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch\t78031\t78588\t0.0\t-\t.\tID=ms3_lm_SuperContig_29_v2_xnc164_090 related to multidrug resistance protein;Target=xnc164_090 related to multidrug resistance protein 19 209;AlignIdentity=32.31;AlignLength=2367\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch_part\t78031\t78080\t3e-21\t-\t.\tID=mp3-1_lm_SuperContig_29_v2_xnc164_090 related to multidrug resistance protein;Parent=ms3_lm_SuperContig_29_v2_xnc164_090 related to multidrug resistance protein;Target=xnc164_090 related to multidrug resistance protein 19 209;Identity=30.89\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch_part\t78081\t78588\t3e-21\t-\t.\tID=mp3-2_lm_SuperContig_29_v2_xnc164_090 related to multidrug resistance protein;Parent=ms3_lm_SuperContig_29_v2_xnc164_090 related to multidrug resistance protein;Target=xnc164_090 related to multidrug resistance protein 19 209;Identity=30.89\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch\t192832\t193704\t0.0\t-\t.\tID=ms2_lm_SuperContig_29_v2_1nc550_030 related to putative multidrug transporter Mfs1.1 (major facilitator family protein);Target=1nc550_030 related to putative multidrug transporter Mfs1.1 (major facilitator family protein) 229 522;AlignIdentity=23.99;AlignLength=1210\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch_part\t192832\t193704\t1e-40\t-\t.\tID=mp2-1_lm_SuperContig_29_v2_1nc550_030 related to putative multidrug transporter Mfs1.1 (major facilitator family protein);Parent=ms2_lm_SuperContig_29_v2_1nc550_030 related to putative multidrug transporter Mfs1.1 (major facilitator family protein);Target=1nc550_030 related to putative multidrug transporter Mfs1.1 (major facilitator family protein) 229 522;Identity=23.99\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch\t193781\t194212\t0.0\t-\t.\tID=ms1_lm_SuperContig_29_v2_1nc550_030 related to putative multidrug transporter Mfs1.1 (major facilitator family protein);Target=1nc550_030 related to putative multidrug transporter Mfs1.1 (major facilitator family protein) 85 228;AlignIdentity=31.02;AlignLength=1200\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch_part\t193781\t194212\t1e-40\t-\t.\tID=mp1-1_lm_SuperContig_29_v2_1nc550_030 related to putative multidrug transporter Mfs1.1 (major facilitator family protein);Parent=ms1_lm_SuperContig_29_v2_1nc550_030 related to putative multidrug transporter Mfs1.1 (major facilitator family protein);Target=1nc550_030 related to putative multidrug transporter Mfs1.1 (major facilitator family protein) 85 228;Identity=30.56\n")
            f.write("##FASTA\n")
            self._writeSeq1(f)

    def _writeExpSetGFFFile(self, inFileName):
        with open(inFileName, 'w') as f:
            f.write("##gff-version 3\n")
            f.write("##sequence-region lm_SuperContig_29_v2 1 120\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch\t193781\t194212\t0.0\t+\t.\tID=ms1_lm_SuperContig_29_v2_set1;Target=set1 1 432\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch_part\t193781\t194212\t0.0\t+\t.\tID=mp1-1_lm_SuperContig_29_v2_set1;Parent=ms1_lm_SuperContig_29_v2_set1;Target=set1 1 432\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch\t192832\t193704\t0.0\t+\t.\tID=ms2_lm_SuperContig_29_v2_set2;Target=set2 1 873\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch_part\t192832\t193704\t0.0\t+\t.\tID=mp2-1_lm_SuperContig_29_v2_set2;Parent=ms2_lm_SuperContig_29_v2_set2;Target=set2 1 873\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch\t78031\t78588\t0.0\t+\t.\tID=ms3_lm_SuperContig_29_v2_set3;Target=set3 1 558\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch_part\t78031\t78080\t0.0\t+\t.\tID=mp3-1_lm_SuperContig_29_v2_set3;Parent=ms3_lm_SuperContig_29_v2_set3;Target=set3 1 50\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch_part\t78081\t78588\t0.0\t+\t.\tID=mp3-2_lm_SuperContig_29_v2_set3;Parent=ms3_lm_SuperContig_29_v2_set3;Target=set3 1 508\n")
            f.write("##FASTA\n")
            self._writeSeq1(f)


    def _writeExpSetGFFFileReversed(self, inFileName):
        with open(inFileName, 'w') as f:
            f.write("##gff-version 3\n")
            f.write("##sequence-region lm_SuperContig_29_v2 1 120\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch\t193781\t194212\t0.0\t+\t.\tID=ms1_lm_SuperContig_29_v2_set1;Target=set1 1 432\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch_part\t193781\t194212\t0.0\t+\t.\tID=mp1-1_lm_SuperContig_29_v2_set1;Parent=ms1_lm_SuperContig_29_v2_set1;Target=set1 1 432\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch\t192832\t193704\t0.0\t+\t.\tID=ms2_lm_SuperContig_29_v2_set2;Target=set2 1 873\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch_part\t192832\t193704\t0.0\t+\t.\tID=mp2-1_lm_SuperContig_29_v2_set2;Parent=ms2_lm_SuperContig_29_v2_set2;Target=set2 1 873\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch\t78031\t78588\t0.0\t-\t.\tID=ms3_lm_SuperContig_29_v2_set3;Target=set3 1 558\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch_part\t78031\t78080\t0.0\t-\t.\tID=mp3-1_lm_SuperContig_29_v2_set3;Parent=ms3_lm_SuperContig_29_v2_set3;Target=set3 1 50\n")
            f.write(
                "lm_SuperContig_29_v2\tPN_REPET_TEs\tmatch_part\t78081\t78588\t0.0\t-\t.\tID=mp3-2_lm_SuperContig_29_v2_set3;Parent=ms3_lm_SuperContig_29_v2_set3;Target=set3 1 508\n")
            f.write("##FASTA\n")
            self._writeSeq1(f)


    def _writeFastaFile(self, inFileName):
        with open(inFileName, 'w') as f:
            self._writeSeq2(f)
            self._writeSeq1(f)


    def _writeFastaFileExtended(self, inFileName):
        with open(inFileName, 'w') as f:
            self._writeSeq2(f)
            self._writeSeq1(f)
            f.write(">chr1\n")
            f.write("CTAAGCTGCGCTATGTAG\n")

    def _writeSeq1(self, f):
        f.write('>lm_SuperContig_29_v2\n')
        f.write('CCTAGACAATTAATTATAATAATTAATAAACTATTAGGCTAGTAGTAGGTAATAATAAAA\n')
        f.write('GGATTACTACTAAGCTGCGCTATGTAGATATTTAAAACATGTGGCTTAGGCAAGAGTATA\n')

    def _writeSeq2(self, f):
        f.write('>lm_SuperContig_30_v2\n')
        f.write('TGTTCATATTCATAGGATGGAGCTAGTAAGCGATGTCGGCTTAGCTCATCCACATGAATG\n')
        f.write('CAGGAATCATGAAGGGTACGACTGTTCGTCGATTAAAGAGCTACACGAGCTGGGTTAAAT\n')

    def _writeFastaFile_DmelChr4(self, inFileName):
        with open(inFileName, 'w') as f:
            f.write(">dmel_chr4\n")
            f.write("CTAAGCTGCGCTATGTAG\n")
            f.write(">dmel_chr1\n")
            f.write("CGTAACGCTAGCGCTTATAGTGAGC\n")


if __name__ == "__main__":
    unittest.main()
