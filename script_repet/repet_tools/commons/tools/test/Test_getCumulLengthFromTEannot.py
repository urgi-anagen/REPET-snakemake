import os
import time
import random
import unittest
from shutil import rmtree
from commons.core.sql.DbMySql import DbMySql
from commons.core.utils.FileUtils import FileUtils
from commons.tools.getCumulLengthFromTEannot import getCumulLengthFromTEannot


class Test_getCumulLengthFromTEannot( unittest.TestCase ):
    
    def setUp( self ):
        self.fileUtils = FileUtils()
        self._iGCLFTa = None
        self._curTestDir = os.getcwd()
        self._testDir = 'test_F_GCLFT_{}_{}'.format(time.strftime("%H%M%S"),random.randint(0, 1000))
        try:
            os.makedirs(self._testDir)
        except:pass
        os.chdir(self._testDir)
        self._configFileName = "dummyConfig.cfg"
        with open( self._configFileName, "w" ) as configFile:
            configFile.write("[repet_env]\n")
            configFile.write( "repet_host: {}\n" .format ( os.environ["REPET_HOST"] ) )
            configFile.write( "repet_user: {}\n" .format ( os.environ["REPET_USER"] ) )
            configFile.write( "repet_pw: {}\n" .format ( os.environ["REPET_PW"] ) )
            configFile.write( "repet_db: {}\n" .format ( os.environ["REPET_DB"] ) )
            configFile.write( "repet_port: {}\n" .format ( os.environ["REPET_PORT"] ) )
        
    def tearDown( self ):
        os.chdir(self._curTestDir)
        try:
            rmtree(self._testDir)
        except:pass

    def test_getAllSubjectsAsMapOfQueries( self ):
        inFileName = "dummyInFile.path"
        expFileName = "dummyExp.path"
        with open( inFileName, "w" ) as inFile:
            inFile.write( "1\tchr1\t1501\t2500\tTE1\t1\t500\t0.0\t880\t95.7\n" )
            inFile.write( "2\tchr1\t3401\t4000\tTE3\t101\t700\t0.0\t950\t97.2\n" )
        inTable = "dummyPathTable"
        db = DbMySql(cfgFileName=self._configFileName)
        db.createTable(inTable, "path", inFileName)
        self._iGCLFTa = getCumulLengthFromTEannot()
        self._iGCLFTa.setInputTable(inTable)
        self._iGCLFTa.setConfigFileName(self._configFileName)
        self._iGCLFTa.setAdaptatorToTable()
        obsFileName = self._iGCLFTa.getAllSubjectsAsMapOfQueries()
        self._iGCLFTa._db.close()
        with open( expFileName, "w" ) as expFile:
            expFile.write( "TE1\tchr1\t1501\t2500\n" )
            expFile.write( "TE3\tchr1\t3401\t4000\n" )
        self.assertTrue( self.fileUtils.are2FilesIdentical( obsFileName, expFileName ) )
        db.dropTable(inTable)
        db.close()
        
        
    def test_getCumulLength_oneSubject( self ):
        mergeFileName = "dummyInFile_{}"
        with open( mergeFileName, "w" ) as mergeFile:
            mergeFile.write( "TE1\tchr1\t1501\t2500\n" )
            mergeFile.write( "TE3\tchr1\t4000\t3401\n" )

        exp = 1000+600
        dExp = {'chr1':1600}
        self._iGCLFTa = getCumulLengthFromTEannot()
        obs = self._iGCLFTa.getCumulLength( mergeFileName )
        dObs = self._iGCLFTa.getdCumulLenthByQuery()
        self.assertEqual( obs, exp )
        self.assertEqual( dObs, dExp )

    def test_getCumulLength_severalSubjects( self ):
        mergeFileName = "dummyInFile_{}"
        with open( mergeFileName, "w" ) as mergeFile:
            mergeFile.write( "TE1\tchr1\t1501\t2500\n" )
            mergeFile.write( "TE3\tchr1\t4000\t3401\n" )
            mergeFile.write( "TE1\tchr2\t1501\t2500\n" )
            mergeFile.write( "TE3\tchr2\t4000\t3401\n" )

        exp = 1000+600+1000+600
        dExp = {'chr1':1600, 'chr2':1600}
        self._iGCLFTa = getCumulLengthFromTEannot()
        obs = self._iGCLFTa.getCumulLength( mergeFileName )
        dObs = self._iGCLFTa.getdCumulLenthByQuery()
        self.assertEqual( obs, exp )
        self.assertEqual( dObs, dExp )

    def test_mergeRanges(self):
        with open( "mapFileName", "w" ) as mapFile:
            mapFile.write( "TE1\tchr1\t1501\t2500\n" )#this match and the followed are merged
            mapFile.write( "TE1\tchr1\t2400\t2700\n" )
            mapFile.write( "TE3\tchr1\t3401\t4000\n" )
            mapFile.write( "TE3\tchr1\t5000\t6000\n" )
            mapFile.write( "TE3\tchr1\t5100\t5200\n" )#this match is included in the previous one, it isn't kept in merged file
            mapFile.write( "TE3\tchr1\t4900\t5200\n" )#this match overlaps the 2 previous one, they become 1 match in merged file

        expMergeFileName = "dummyInFile_{}"
        with open( expMergeFileName, "w" ) as expMergeFile:
            expMergeFile.write( "TE1\tchr1\t1501\t2700\n" )
            expMergeFile.write( "TE3\tchr1\t3401\t4000\n" )
            expMergeFile.write( "TE3\tchr1\t4900\t6000\n" )

        gclft = getCumulLengthFromTEannot()
        obsMergeFileName = gclft.mergeRanges("mapFileName")
        self.assertTrue(self.fileUtils.are2FilesIdentical( obsMergeFileName, expMergeFileName ))
        os.remove( expMergeFileName )
        os.remove( obsMergeFileName )
        os.remove( "mapFileName" )
        

if __name__ == "__main__":
        unittest.main()