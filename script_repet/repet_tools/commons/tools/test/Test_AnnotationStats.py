import os
import unittest
import time
import shutil
from commons.tools.AnnotationStats import AnnotationStats
import random

class Test_AnnotationStats(unittest.TestCase):

    def setUp(self):
        self.singularity_img = "../../../../../../Singularity/te_finder_2.30.2.sif"
        self._curTestDir = os.getcwd()
        self._testPrefix = 'test_AS_{}_{}'.format(time.strftime("%H%M%S"), random.randint(0, 1000))
        try:
            os.makedirs(self._testPrefix)
        except:pass
        os.chdir(self._testPrefix)
        
        self._uniqId = time.strftime("%Y%m%d%H%M%S")
        self._seqFileName = "Dummy{}.fa".format(self._uniqId)
        self._writeRefTESeq_file(self._seqFileName)
        self._pathFileName = "Dummy{}.path".format(self._uniqId)
        self._writePath_File(self._pathFileName )
        self._clusterFileName = "DummyClusterFile.txt"
        self._writeClusterFile(self._clusterFileName)
        
        self.iAS = AnnotationStats(seqFileName=self._seqFileName,pathFileName=self._pathFileName,clusterFileName=self._clusterFileName,singularityImg = self.singularity_img)
        
        

    def tearDown(self):
        self._uniqId = None
        os.chdir(self._curTestDir)
        try:
            shutil.rmtree(self._testPrefix)
        except:pass

        

    def test_getNbFullLengths(self):
#        self.iAS = AnnotationStats()
        consensusLength = 100
        lLengths = [12, 100, 95, 105, 106, 94]
        obsNbFullLength = self.iAS.get_nb_full_lengths(consensusLength, lLengths)
        expNbFullLength = 3
        self.assertEqual(expNbFullLength, obsNbFullLength)

    def test_getClusterListFromFile(self):
        
        #self.iAS = AnnotationStats(clusterFileName=clusterFileName)
        obslClusters = self.iAS.get_cluster_list_from_file()
        explClusters = [["Cons1", "Cons2", "Cons3", "Cons4"], ["Cons5", "Cons6", "Cons7"], ["Cons8"], ["Cons9"]]
        self.assertEqual(explClusters, obslClusters)



    def test_getCumulCoverage_forAllConsensus(self):
#        self.iAS = AnnotationStats()
        pathFileName = "Dummy{}.path".format(self._uniqId)
        self._writePath_File(pathFileName)
        self.iAS._join_path_file = pathFileName
        seqFileName = "Dummy{}.fa".format(self._uniqId)
        self._writeRefTESeq_file(seqFileName)
        self.iAS._consensus_fasta_file = seqFileName


        obsGetCumulCoverage = self.iAS.get_cumul_coverage()
        expGetCumulCoverage = 40 + 10 + 10 + 40 + 31 + 19 + 40 + 51 + 54
        self.assertEqual(expGetCumulCoverage, obsGetCumulCoverage)



    def test_getCumulCoverage_forOneConsensus(self):
        pathFileName = "Dummy{}.path".format(self._uniqId)
        self._writePath_File(pathFileName)
        self.iAS._join_path_file = pathFileName
        obsGetCumulCoverage = self.iAS.get_cumul_coverage("refTE_227")
        expGetCumulCoverage = 22 + 22 + 51
        self.assertEqual(expGetCumulCoverage, obsGetCumulCoverage)


    def test_getTrigramTableFromClassifTable( self ):
        classif_file = 'classifFile.classif'
        self._writeClassifFile(classif_file)
        self.iAS._classif_file = classif_file
        self.iAS._check_attributes() # remet a jour avec le fichier classif setter 

        expTab = {"noCat_MCL199_Atha_TEdenovo-B-G1416-Map12": ("NA", "Unclassified"),
                  "PotentialHostGene_MCL362_Atha_TEdenovo-B-R1696-Map4": ("PHG", "NA"),
                  "RIX-comp_MCL111_Atha_TEdenovo-B-R1524-Map5": ("RIX", "I"),
                  "RIX-incomp-chim_MCL106_Atha_TEdenovo-B-R880-Map8_reversed": ("RIX|RLX", "I|I")}
        self.assertEqual(expTab, self.iAS._dict_cons_name_to_trigram)

    def _writeRefTESeq_file(self, fileName):
        with open(fileName, "w") as f:
            f.write(">refTE_227\n")
            f.write("ATCGATCGATGCATGCATCGATCGATCGATGCATGCATCGATCGATCGATGCATGCATCG\n")
            f.write("ATCGATCGATGCATGCATCGATCGATCGATGCATGCATCGATCGATCGATGCATGCATCG\n")
            f.write("ATCGATCGATGCATGCATCGATCGATCGATGCATGCATCGATCGATCGATGCATGC\n")
            f.write(">refTE_324\n")
            f.write("ATCGATCGATGCATGCATCGATCGATCGATGCATGCATCGATCGATCGATGCATGCATCG\n")
            f.write("ATCGATCGATGCATGCATCGATCGATCGATGCATGCATCGATCGATCGATGCATGCATCG\n")
            f.write("ATCGATCGATGCATGCATC\n")
            f.write(">refTE_617\n")
            f.write("ATCGATCGATGCATGCATCGATCGATCGATGCATGCATCGATCGATCGATGCATGCATCG\n")
            f.write("ATCGATCGATGCATGCATCGATCGATCGATGCATGCATCGATCGATCGATGCATGCATCG\n")
            f.write("ATCGATCGATGCATGCATCGATCGATCGATGCATGCATCGATCGATCGATGCATGCATCG\n")
            f.write(">refTE_200\n")
            f.write("ATCGATCGATGCATGCATCGATCGATCGATGCATGCATCGATCGATCGATGCATGCATCG\n")
            f.write("ATCGATCGATGCATGCATCGATCGATCGATGCATGCATCGATCGATCGATGCATGCATCG\n")
            f.write("ATCGATCGATGCATGCATCGATCGATCGATGCATGCATCGATCGATCGATGCATGCATCG\n")
            f.write(">refTE_878\n")
            f.write("ATCGATCGATGCATGCATCGATCGATCGATGCATGCATCGATCGATCGATGCATGCATCG\n")
            f.write("ATCGATCGATGCATGCATCGATCGATCGATGCATGCATCGATCGATCGATGCATGCATCG\n")
            f.write("ATCGATCGATGCATGCATCGATCGATCGATGCATGCATCGATCGATCGATGCATGCATCG\n")
            f.write(">refTE_161\n")
            f.write("ATCGATCGATGCATGCATCGATCGATCGATGCATGCATCGATCGATCGATGCATGCATCG\n")
            f.write("ATCGATCGATGCATGCATCGATCGATCGATGCATGCATCGATCGATCGATGCATGCATCG\n")
            f.write("ATCGATCGATGCATGCATCGATCGATCGATGCATGCATCGATCGATCGATGCATGCATCG\n")
            f.write(">refTE_43\n")
            f.write("ATCGATCGATGCATGCATCGATCGATCGATGCATGCATCGATCGATCGATGCATGCATCG\n")
            f.write("ATCGATCGATGCATGCATCGATCGATCGATGCATGCATCGATCGATCGATGCATGCATCG\n")
            f.write("ATCGATCGATGCATGC\n")
            f.write(">refTE_45\n")
            f.write("ATCGATCGATGCATGCATCGATCGATCGATGCATGCATCGATCGATCGATGCATGCATCG\n")
            f.write("ATCGATCGATGCATGCATCGATCGATCGATGCATGCATCGATCGATCGATGCATGCATCG\n")

    def _writeClassifFile( self, filename ):
        with open(filename, "w") as f:
            f.write("Seq_name\tlength\tstrand\tconfused\tclass\torder\tWcode\tsFamily\tCI\tcoding\tstruct\tother\n")
            f.write("noCat_MCL199_Atha_TEdenovo-B-G1416-Map12\t661\t.\tFalse\tUnclassified\tUnclassified\tNA\tNA\tNA\tcoding=(NA)\tstruct=(NA)\tother=(NA)\n")
            f.write("PotentialHostGene_MCL362_Atha_TEdenovo-B-R1696-Map4\t3254\t.\tFalse\tNA\tNA\tPHG\tNA\t100\tcoding=(Other_profiles: PF01095.19_Pectinesterase_NA_OTHER_21.4: 99.66%, PF04043.15_PMEI_NA_OTHER_26.7: 94.16%)\tstruct=(NA)\tother=(NA)\n")
            f.write("RIX-comp_MCL111_Atha_TEdenovo-B-R1524-Map5\t5622\t+\tFalse\tI\tLINE\tRIX\tNA\t90\tcoding=(TE_BLRtx: ATLINE1_10A:ClassI:LINE:L1:L1: 99.15%; TE_BLRx: ATLINE1_10A_1p:ClassI:LINE:L1:L1: 100.00%, ATLINE1_10A_2p:ClassI:LINE:L1:L1: 100.00%; profiles: PF00078.27_RVT_1_NA_RT_29.6: 94.59%, PF03372.23_Exo_endo_phos_NA_EN_24.5: 99.57%, PF13456.6_RVT_3_NA_RT_21.6: 99.19%, PF13966.6_zf-RVT_NA_RT_27.5: 100.00%, PF14111.6_DUF4283_NA_LINErelated_27.0: 94.48%, PF14392.6_zf-CCHC_4_NA_ClassIrelated_6.0: 91.84%)\tstruct=(TElength: >1000bps; ORF: >3000bps EN RT RT RT; SSR: (A)12_end)\tother=(NA)\n")
            f.write("RIX-incomp-chim_MCL106_Atha_TEdenovo-B-R880-Map8_reversed\t6017\t+\tTrue\tI|I\tLINE|LTR\tRIX|RLX\tNA|NA\t63|28\tcoding=(TE_BLRtx: ATLINE1_1:ClassI:LINE:L1:L1: 45.68%; TE_BLRx: L1-1_CPa_2p:ClassI:LINE:L1:L1: 62.04%; profiles: PF00078.27_RVT_1_NA_RT_29.6: 96.40%, PF13456.6_RVT_3_NA_RT_21.6: 100.00%, PF13966.6_zf-RVT_NA_RT_27.5: 100.00%)\tstruct=(TElength: >1000bps; ORF: >1000bps RT RT RT; SSR: (A)10_end)\tother=(TE_BLRtx: ATLANTYS3I:ClassI:LTR:Gypsy:?: 6.79%; TElength: >4000bps)\n")

    def _writePath_File(self, fileName):
        with open(fileName, "w") as f:
            f.write("1\tdmel_chr4\t1\t22\trefTE_227\t128\t145\t0\t148\t69.7\n")  # 22
            f.write(
                "2\tdmel_chr4\t1\t22\trefTE_227\t146\t168\t0\t148\t80.7\n")  # these 22 bp are not taken in account for refTE_227 coverage and for global cumul coverage
            f.write(
                "3\tdmel_chr4\t2\t40\trefTE_324\t48\t86\t0\t767\t85.8\n")  # 39 these 39bp + 1bp 40bp from refTE_227 are taken in account for global cumul coverage
            f.write("4\tdmel_chr4\t91\t100\trefTE_617\t41\t48\t0\t580\t85.4\n")  # 10
            f.write("5\tdmel_chr4\t110\t119\trefTE_617\t41\t48\t0\t743\t84.9\n")  # 9
            f.write("6\tdmel_chr4\t150\t189\trefTE_200\t50\t89\t0\t221\t95.3\n")  # 40
            f.write("7\tdmel_chr4\t235\t265\trefTE_878\t76\t49\t0\t238\t90.7\n")  # 31
            f.write("8\tdmel_chr4\t370\t388\trefTE_161\t231\t249\t0\t244\t91.3\n")  # 19
            f.write(
                "9\tdmel_chrU\t1\t22\trefTE_227\t220\t128\t145\t251\t92.3\n")  # 22 from 2 to 22bp are not taken in account for global cumul coverage but the 22bp are taken in account for refTE_227
            f.write(
                "10\tdmel_chrU\t2\t40\trefTE_324\t48\t86\t0\t244\t91.3\n")  # 39 these 39bp are taken in account for refTE_324 coverage  but 39bp + 1bp 40bp from refTE_227 are taken in account for global cumul coverage
            f.write(
                "11\tdmel_chrU\t91\t100\trefTE_43\t41\t48\t0\t240\t90.8\n")  # 10 these 2 followed copies are merged 91-144 for global cumul coverage
            f.write("11\tdmel_chrU\t100\t144\trefTE_45\t218\t260\t0\t262\t93.8\n")  # 45
            f.write("13\tdmel_chrU\t200\t250\trefTE_227\t82\t30\t0\t266\t94.4\n")  # 51

    def _writeClusterFile(self, fileName):
        with open(fileName, "w") as f :
            f.write("Cons1\tCons2\tCons3\tCons4\n")
            f.write("Cons5\tCons6\tCons7\n")
            f.write("Cons8\n")
            f.write("Cons9\n")


if __name__ == "__main__":
    unittest.main()
