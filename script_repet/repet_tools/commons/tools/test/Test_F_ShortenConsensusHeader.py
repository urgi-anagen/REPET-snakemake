import os
import time
import shutil
import unittest
import subprocess
from commons.core.utils.FileUtils import FileUtils
from commons.tools.ShortenConsensusHeader import ShortenConsensusHeader


class Test_F_ShortenConsensusHeader(unittest.TestCase):

    def setUp(self):
        self.consensusFileName = "{}/TEdenovo/Step5/DmelChr4_Blaster_Grouper_Map_consensus.fa".format(
            os.environ["REPET_DATA"])
        self.obsFileName = "resultObs.fa"
        self.expFileName = "{}/commons/exp_ShortenConsensusHeader.fa".format(os.environ["REPET_DATA"])
        self._uniqId = "{}_{}".format(time.strftime("%Y%m%d%H%M%S"), os.getpid())
        self._curDir = os.getcwd()
        self._workdir = "Conversion_{}".format(self._uniqId)
        os.makedirs(self._workdir)
        os.chdir(self._workdir)

    def tearDown(self):
        self._uniqId = None
        os.chdir(self._curDir)
        shutil.rmtree(self._workdir)

    def test_run_only_Grouper(self):
        iSCH = ShortenConsensusHeader(self.consensusFileName, self.obsFileName, projectName="DmelChr4",
                                      selfAlign="Blaster", clustering="Grouper", multAlign="Map")
        iSCH.run()
        self.assertTrue(FileUtils.are2FilesIdentical(self.expFileName, self.obsFileName))

    def test_run_as_script_only_Grouper(self):
        cmd = "ShortenConsensusHeader.py"
        cmd += " -p DmelChr4"
        cmd += " -s Blaster"
        cmd += " -c Grouper"
        cmd += " -i {}".format(self.consensusFileName)
        cmd += " -o {}".format(self.obsFileName)
        cmd += " -m Map"
        process = subprocess.Popen(cmd, shell=True)
        process.communicate()

        self.assertTrue(FileUtils.are2FilesIdentical(self.expFileName, self.obsFileName))

    def test_run_all_clustering(self):
        self._write_consensusFile4Grouper("consensuFile4Grouper.fa")
        self._write_consensusFile4Piler("consensuFile4Piler.fa")
        self._write_consensusFile4Recon("consensuFile4Recon.fa")
        self._write_consensusFile4BlastClust("consensuFile4BlastClust.fa")
        self._write_expFile("exp_consensus_all_clustering.fa")

        iSCH = ShortenConsensusHeader("consensuFile4Grouper.fa", "obsFileName4Grouper.fa", "DmelChr4", "Blaster",
                                      "Grouper", "Map")
        iSCH.setVerbosity(4)
        iSCH.run()

        iSCH = ShortenConsensusHeader("consensuFile4Piler.fa", "obsFileName4Piler.fa", "DmelChr4", "Blaster", "Piler",
                                      "Map")
        iSCH.run()

        iSCH = ShortenConsensusHeader("consensuFile4Recon.fa", "obsFileName4Recon.fa", "DmelChr4", "Blaster", "Recon",
                                      "Map")
        iSCH.run()

        iSCH = ShortenConsensusHeader("consensuFile4BlastClust.fa", "obsFileName4BlastClust.fa", "DmelChr4",
                                      "LTRharvest", "BlastClust", "Map")
        iSCH.run()

        lFiles = ["obsFileName4Grouper.fa", "obsFileName4Piler.fa", "obsFileName4Recon.fa", "obsFileName4BlastClust.fa"]
        FileUtils.catFilesFromList(lFiles, self.obsFileName, sort=False, skipHeaders=False, separator="")
        self.assertTrue(FileUtils.are2FilesIdentical("exp_consensus_all_clustering.fa", self.obsFileName))

    def _write_consensusFile4Recon(self, fileName):
        with open(fileName, "w") as consensusFile:
            consensusFile.write(">consensus=seqCluster1.fa.fa_aln length=2367 nbAlign=4\n")
            consensusFile.write("TTATTTGTCTCCGAACTTCTTACTGACAATGGCACCAAAATATGATCAGTGTTTGTTGGT\n")
            consensusFile.close()

    def _write_consensusFile4Piler(self, fileName):
        with open(fileName, "w") as consensusFile:
            consensusFile.write(">consensus=seqCluster0.0.fa.fa_aln length=1042 nbAlign=3\n")
            consensusFile.write("TTATTTGTCTCCGAACTTCTTACTGACAATGGCACCAAAATATGATCAGTGTTTGTTGGT\n")
            consensusFile.close()

    def _write_consensusFile4Grouper(self, fileName):
        with open(fileName, "w") as consensusFile:
            consensusFile.write(">consensus=seqCluster1.fa.fa_aln length=542 nbAlign=3\n")
            consensusFile.write("TTATTTGTCTCCGAACTTCTTACTGACAATGGCACCAAAATATGATCAGTGTTTGTTGGT\n")
            consensusFile.close()

    def _write_consensusFile4BlastClust(self, fileName):
        with open(fileName, "w") as consensusFile:
            consensusFile.write(">consensus=seqCluster1.fa.fa_aln length=25944 nbAlign=11\n")
            consensusFile.write("TTATTTGTCTCCGAACTTCTTACTGACAATGGCACCAAAATATGATCAGTGTTTGTTGGT\n")
            consensusFile.write("\n")
            consensusFile.close()

    def _write_expFile(self, fileName):
        with open(fileName, "w") as consensusFile:
            consensusFile.write(">DmelChr4-B-G1-Map3\n")
            consensusFile.write("TTATTTGTCTCCGAACTTCTTACTGACAATGGCACCAAAATATGATCAGTGTTTGTTGGT\n")
            consensusFile.write(">DmelChr4-B-P0.0-Map3\n")
            consensusFile.write("TTATTTGTCTCCGAACTTCTTACTGACAATGGCACCAAAATATGATCAGTGTTTGTTGGT\n")
            consensusFile.write(">DmelChr4-B-R1-Map4\n")
            consensusFile.write("TTATTTGTCTCCGAACTTCTTACTGACAATGGCACCAAAATATGATCAGTGTTTGTTGGT\n")
            consensusFile.write(">DmelChr4-L-B1-Map11\n")
            consensusFile.write("TTATTTGTCTCCGAACTTCTTACTGACAATGGCACCAAAATATGATCAGTGTTTGTTGGT\n")
            consensusFile.write("\n")
            consensusFile.close()


if __name__ == "__main__":
    unittest.main()
