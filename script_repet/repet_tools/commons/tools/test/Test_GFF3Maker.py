import unittest
import time
import shutil
import os
from commons.core.sql.DbMySql import DbMySql
from commons.tools.GFF3Maker import GFF3Maker
from commons.tools.ListAndDropTables import ListAndDropTables
from commons.core.sql.TablePathAdaptator import TablePathAdaptator

class Test_GFF3Maker(unittest.TestCase):

    def setUp(self):
        self.curDir = os.getcwd()
        self._uniqId = "GFF3Maker_{}_{}".format( time.strftime("%Y%m%d%H%M%S") , os.getpid() )
        os.makedirs(self._uniqId)     
        os.chdir(self._uniqId)

        self._projectName = "PN"
        self._iDb = DbMySql()
        self._tablesFileName = "annotation_tables.txt"
        self._fastaFileName = "{}_chr.fa".format(self._projectName)
        self._fastaTableName = "{}_chr_seq".format(self._projectName)
        self._writeFastaFile(self._fastaFileName)
        self._iDb.createTable(self._fastaTableName, "seq", self._fastaFileName, True)
        self._writeClassifFile("input.classif")
        self._writeNewClassifFile("inputNew.classif")
        self._inputClassifTableName = "{}_classif".format(self._projectName)
        self._iDb.createTable(self._inputClassifTableName, "classif", "input.classif", True)
        self._inputNewClassifTableName = "{}_newClassif".format(self._projectName)
        self._iDb.createTable(self._inputNewClassifTableName, "classifNew", "inputNew.classif", True)
        self._pathFileName = "{}.path".format(self._projectName)
        self._writePathFile(self._pathFileName)
        self._pathTableName = "{}_chr_allTEs_nr_noSSR_join_path".format(self._projectName)
        self._iDb.createTable(self._pathTableName, "path", self._pathFileName, True)

        self.iGFF3M = GFF3Maker(inFastaName = self._fastaFileName, tablesFileName = self._fastaTableName, classifTableName = self._inputClassifTableName)

    def tearDown( self ):
        self._iDb.close()
        iLADT = ListAndDropTables(action = "drop", tableNames = self._projectName)
        iLADT.run()
        os.chdir(self.curDir)
        shutil.rmtree(self._uniqId)
        self._uniqId = None

    def test_gatherSamePathFeatures_onlyOneCopy(self):
        iTPA = TablePathAdaptator(self._iDb, self._pathTableName)
        lPaths = iTPA.getPathListFromId(3)

        obsDict = self.iGFF3M._gatherSamePathFeatures(lPaths)
        expDict = {3: [[78031, 78080, '+', 'DTX-incomp_DmelChr4-B-P0.0-Map3', 19, 209, 3e-21, 30.89, []],[78081, 78588, '+', 'DTX-incomp_DmelChr4-B-P0.0-Map3', 419, 609, 3e-21, 50.89, []]]}
        self.assertEqual(obsDict, expDict)

    def test_gatherSamePathFeatures_severalCopies(self):
        iTPA = TablePathAdaptator(self._iDb, self._pathTableName)
        lPaths = iTPA.getListOfAllCoordObject()

        obsDict = self.iGFF3M._gatherSamePathFeatures(lPaths)
        expDict = {1:[[193781, 194212, '-', 'DTX-incomp_DmelChr4-L-B1-Map3', 85, 228, 1e-40, 30.56, []]], 2:[[192832, 193704, '-', 'DTX-incomp_DmelChr4-L-B1-Map3', 229, 522, 1e-40, 23.99, []]],3: [[78031, 78080, '+', 'DTX-incomp_DmelChr4-B-P0.0-Map3', 19, 209, 3e-21, 30.89, []],[78081, 78588, '+', 'DTX-incomp_DmelChr4-B-P0.0-Map3', 419, 609, 3e-21, 50.89, []]]}
        self.assertEqual(obsDict, expDict)

    def test_getClassifEvidenceBySeqName_fromOldClassifFormat(self):
        self.iGFF3M._iDB = self._iDb
        self.iGFF3M.setNbColInClassif(8)
        seqName = "DTX-incomp_DmelChr4-L-B1-Map3"

        obsResult = self.iGFF3M._getClassifEvidenceBySeqName(seqName)
        expResult = 714, "CI=66; coding=(TE_BLRtx: TC1_DM:classII:TIR: 40.88%; TE_BLRx: Tc1-1_TCa_1p:classII:TIR: 30.18%, Tc1-3_FR_1p:classII:TIR: 9.97%); struct=(TElength: >700bps); other=(SSRCoverage=0.08<0.75)"
        self.assertEqual(expResult, obsResult)

        seqName = "PotentialHostGene-chim_fTest05105818-B-R4-Map5_reversed"

        obsResult = self.iGFF3M._getClassifEvidenceBySeqName(seqName)
        expResult = 1067, "CI=100; coding=(HG_BLRn: FBtr0089196_Dmel_r4.3: 99.91%); other=(TE_BLRtx: PROTOP:classII:TIR: 13.06%, PROTOP_A:classII:TIR: 37.47%; SSRCoverage=0.27<0.75)"
        self.assertEqual(expResult, obsResult)

    def test_getClassifEvidenceBySeqName_fromNewClassifFormat(self):
        self.iGFF3M._iDB = self._iDb
        self.iGFF3M.setNbColInClassif(12)
        self.iGFF3M.setClassifTable(self._inputNewClassifTableName)
        seqName = "DTX-incomp_DmelChr4-L-B1-Map3"
        obsResult = self.iGFF3M._getClassifEvidenceBySeqName(seqName)
        expResult = 714, "Wcode=DTX; CI=66; coding=(TE_BLRtx: TC1_DM:classII:TIR: 40.88%; TE_BLRx: Tc1-1_TCa_1p:classII:TIR: 30.18%, Tc1-3_FR_1p:classII:TIR: 9.97%); struct=(TElength: >700bps); other=(SSRCoverage=0.08<0.75)"
        self.assertEqual(expResult, obsResult)

        seqName = "PotentialHostGene-chim_fTest05105818-B-R4-Map5_reversed"
        obsResult = self.iGFF3M._getClassifEvidenceBySeqName(seqName)
        expResult = 1067, "Wcode=NA|NA; CI=100|NA; coding=(HG_BLRn: FBtr0089196_Dmel_r4.3: 99.91%); struct=(NA); other=(TE_BLRtx: PROTOP:classII:TIR: 13.06%, PROTOP_A:classII:TIR: 37.47%; SSRCoverage=0.27<0.75)"
        self.assertEqual(expResult, obsResult)

    def test_organizeEachPathFeature_forCopyWithOneFragment(self):
        self.iGFF3M._iDB = self._iDb
        self.iGFF3M.setNbColInClassif(8)
        self.iGFF3M._hasAlignInfos = False
        self.iGFF3M._dSource2AlignInfos = {"REPET_SSRs":False, "REPET_tblastx": False, "REPET_tblasx": False, "REPET_TEs": True}

        seqName = "lm_SuperContig_29_v2"
        iTPA = TablePathAdaptator(self._iDb, self._pathTableName)
        pathNum = 1
        lPaths = iTPA.getPathListFromId(1)
        dPathID2Data = self.iGFF3M._gatherSamePathFeatures(lPaths)

        obsLine = self.iGFF3M._organizeEachPathFeature(pathNum, dPathID2Data[pathNum], seqName, "REPET_TEs", ".", seqTable = self._fastaTableName)
        expLine = "lm_SuperContig_29_v2\tREPET_TEs\tmatch\t193781\t194212\t0.0\t-\t.\tID=ms1_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-L-B1-Map3;Target=DTX-incomp_DmelChr4-L-B1-Map3 85 228;TargetLength=714;TargetDescription=CI:66 coding:(TE_BLRtx: TC1_DM:classII:TIR: 40.88% TE_BLRx: Tc1-1_TCa_1p:classII:TIR: 30.18% | Tc1-3_FR_1p:classII:TIR: 9.97%) struct:(TElength: >700bps) other:(SSRCoverage:0.08<0.75);Identity=30.56\n"
        self.assertEqual(expLine, obsLine)

    def test_organizeEachPathFeature_forCopyWithOneFragment_newClassif(self):
        self._writeTabFile_alignCopyOnConsensus("{}_align.tab".format(self._pathTableName))
        self.iGFF3M._iDB = self._iDb
        self.iGFF3M.setClassifTable(self._inputNewClassifTableName)
        self.iGFF3M.setNbColInClassif(12)
        self.iGFF3M._hasAlignInfos = True
        self.iGFF3M._dSource2AlignInfos = {"REPET_SSRs":False, "REPET_tblastx": False, "REPET_tblasx": False, "REPET_TEs": True}
        self.iGFF3M._dPath2alignInformationsTab = {1: (720, 29.06), 2: (716, 23.99), 3: (923, 80.97)}
        seqName = "lm_SuperContig_29_v2"
        iTPA = TablePathAdaptator(self._iDb, self._pathTableName)
        pathNum = 1
        lPaths = iTPA.getPathListFromId(1)
        dPathID2Data = self.iGFF3M._gatherSamePathFeatures(lPaths)

        obsLine = self.iGFF3M._organizeEachPathFeature(pathNum, dPathID2Data[pathNum], seqName, "REPET_TEs", ".", seqTable = self._fastaTableName)
        expLine = "lm_SuperContig_29_v2\tREPET_TEs\tmatch\t193781\t194212\t0.0\t-\t.\tID=ms1_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-L-B1-Map3;Target=DTX-incomp_DmelChr4-L-B1-Map3 85 228;TargetLength=714;TargetDescription=Wcode:DTX CI:66 coding:(TE_BLRtx: TC1_DM:classII:TIR: 40.88% TE_BLRx: Tc1-1_TCa_1p:classII:TIR: 30.18% | Tc1-3_FR_1p:classII:TIR: 9.97%) struct:(TElength: >700bps) other:(SSRCoverage:0.08<0.75);AlignIdentity=29.06;AlignLength=720\n"
        self.assertEqual(expLine, obsLine)

    def test_organizeEachPathFeature_forCopyWithTwoFragment_newClassif(self):
        self._writeTabFile_alignCopyOnConsensus("{}_align.tab".format(self._pathTableName))
        self.iGFF3M._iDB = self._iDb
        self.iGFF3M.setClassifTable(self._inputNewClassifTableName)
        self.iGFF3M.setNbColInClassif(12)
        self.iGFF3M._hasAlignInfos = True
        self.iGFF3M._dSource2AlignInfos = {"REPET_SSRs":False, "REPET_tblastx": False, "REPET_tblasx": False, "REPET_TEs": True}
        self.iGFF3M._dPath2alignInformationsTab = {1: (720, 29.06), 2: (716, 23.99), 3: (923, 80.97)}
        seqName = "lm_SuperContig_29_v2"
        iTPA = TablePathAdaptator(self._iDb, self._pathTableName)
        pathNum = 3
        lPaths = iTPA.getPathListFromId(3)
        dPathID2Data = self.iGFF3M._gatherSamePathFeatures(lPaths)

        obsLine = self.iGFF3M._organizeEachPathFeature(pathNum, dPathID2Data[pathNum], seqName, "REPET_TES", ".", seqTable = self._fastaTableName)
        expLine = "lm_SuperContig_29_v2\tREPET_TEs\tmatch\t193781\t194212\t0.0\t-\t.\tID=ms1_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-L-B1-Map3;Target=DTX-incomp_DmelChr4-L-B1-Map3 85 228;TargetLength=714;TargetDescription=Wcode:DTX CI:66 coding:(TE_BLRtx: TC1_DM:classII:TIR: 40.88% TE_BLRx: Tc1-1_TCa_1p:classII:TIR: 30.18% | Tc1-3_FR_1p:classII:TIR: 9.97%) struct:(TElength: >700bps) other:(SSRCoverage:0.08<0.75);AlignIdentity=29.06;AlignLength=720\n"
        expLine += "lm_SuperContig_29_v2\tREPET_TEs\tmatch_partt78031\t78080\tID=mp;Parent=ms3_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-B-P0.0-Map3;Target=DTX-incomp_DmelChr4-B-P0.0-Map3 19 209;Identity=30.89\n"
        expLine += "lm_SuperContig_29_v2\tREPET_TEs\tmatch_part\t78081\t78588\tID=mp;Parent=ms3_lm_SuperContig_29_v2_DTX-incomp_DmelChr4-B-P0.0-Map3;Target=DTX-incomp_DmelChr4-B-P0.0-Map3 419 609;Identity=50.89\n"
        self.assertTrue(expLine, obsLine)

    def test_getAlignInfos( self ):
        self._writeTabFile_alignCopyOnConsensus("{}_align.tab".format(self._tablesFileName))
        exp_dAlignInfos = {1: (720, 29.06), 2: (716, 23.99), 3: (923, 80.97)}
        self.iGFF3M._alignInformationsFileName = "{}_align.tab".format(self._tablesFileName)
        obs_dAlignInfos = self.iGFF3M.getAlignInfos()
        self.assertTrue(exp_dAlignInfos, obs_dAlignInfos)


    def _writeClassifFile(self, inputFileName):
        with open(inputFileName, "w") as f:
            f.write("PotentialHostGene-chim_fTest05105818-B-G11-Map20\t1240\t+\tPotentialChimeric\tNA\tPotentialHostGene\tNA\tCI=100; coding=(HG_BLRn: FBtr0089196_Dmel_r4.3: 95.65%); other=(TE_BLRtx: PROTOP:classII:TIR: 12.03%, PROTOP_A:classII:TIR: 49.14%; TermRepeats: termTIR: 49; SSRCoverage=0.25<0.75)\n")
            f.write("DTX-comp-chim_fTest05105818-B-G7-Map3_reversed\t1944\t-\tPotentialChimeric\tII\tTIR\tcomplete\tCI=33; coding=(TE_BLRtx: PROTOP:classII:TIR: 12.77%, PROTOP_A:classII:TIR: 25.16%, PROTOP_A:classII:TIR: 100.00%); struct=(TElength: <700bps; TermRepeats: termTIR: 844); other=(HG_BLRn: FBtr0089196_Dmel_r4.3: 29.48%; SSRCoverage=0.24<0.75)\n")
            f.write("DTX-incomp_fTest05105818-B-G9-Map3_reversed\t1590\t-\tok\tII\tTIR\tincomplete\tCI=33; coding=(TE_BLRtx: PROTOP:classII:TIR: 10.92%, PROTOP:classII:TIR: 11.03%, PROTOP_A:classII:TIR: 55.20%); struct=(TElength: >700bps); other=(HG_BLRn: FBtr0089196_Dmel_r4.3: 35.60%; SSRCoverage=0.21<0.75)\n")
            f.write("DTX-incomp_fTest05105818-B-P0.0-Map3\t1042\t.\tok\tII\tTIR\tincomplete\tCI=50; coding=(TE_BLRtx: PROTOP:classII:TIR: 17.39%, PROTOP_A:classII:TIR: 22.17%); struct=(TElength: >700bps; TermRepeats: termTIR: 50); other=(HG_BLRn: FBtr0089196_Dmel_r4.3: 47.22%; SSRCoverage=0.25<0.75)\n")
            f.write("DTX-comp_fTest05105818-B-P1.0-Map9_reversed\t1137\t-\tok\tII\tTIR\tcomplete\tCI=50; coding=(TE_BLRtx: PROTOP:classII:TIR: 6.70%, PROTOP_A:classII:TIR: 66.43%, PROTOP_B:classII:TIR: 6.42%); struct=(TElength: >700bps; TermRepeats: termTIR: 52); other=(HG_BLRn: FBtr0089196_Dmel_r4.3: 51.19%; SSRCoverage=0.22<0.75)\n")
            f.write("RLX-incomp_fTest05105818-B-R12-Map3_reversed\t2284\t-\tok\tI\tLTR\tincomplete\tCI=28; coding=(TE_BLRtx: ROOA_I:classI:LTR_retrotransposon: 27.57%, ROOA_LTR:classI:LTR_retrotransposon: 94.56%; TE_BLRx: BEL11_AGp:classI:LTR_retrotransposon: 19.47%, BEL2-I_Dmoj_1p:classI:LTR_retrotransposon: 11.49%); struct=(TElength: >700bps); other=(SSRCoverage=0.07<0.75)\n")
            f.write("DTX-incomp_fTest05105818-B-R19-Map4\t705\t+\tok\tII\tTIR\tincomplete\tCI=66; coding=(TE_BLRtx: TC1-2_DM:classII:TIR: 42.70%; TE_BLRx: TC1-2_DMp:classII:TIR: 41.18%); struct=(TElength: >700bps); other=(SSRCoverage=0.14<0.75)\n")
            f.write("DHX-incomp_fTest05105818-B-R1-Map4\t2367\t.\tok\tII\tHelitron\tincomplete\tCI=20; coding=(TE_BLRtx: DNAREP1_DM:classII:Helitron: 17.00%, DNAREP1_DYak:classII:Helitron: 9.08%); struct=(TElength: >700bps); other=(HG_BLRn: FBtr0089179_Dmel_r4.3: 13.52%; SSRCoverage=0.18<0.75)\n")
            f.write("noCat_fTest05105818-B-R2-Map6\t4638\t.\tok\tnoCat\tnoCat\tNA\tCI=NA; coding=(HG_BLRn: FBtr0089179_Dmel_r4.3: 73.65%); struct=(SSRCoverage=0.05<0.75)\n")
            f.write("PotentialHostGene-chim_fTest05105818-B-R4-Map5_reversed\t1067\t-\tPotentialChimeric\tNA\tPotentialHostGene\tNA\tCI=100; coding=(HG_BLRn: FBtr0089196_Dmel_r4.3: 99.91%); other=(TE_BLRtx: PROTOP:classII:TIR: 13.06%, PROTOP_A:classII:TIR: 37.47%; SSRCoverage=0.27<0.75)\n")
            f.write("DTX-incomp_fTest05105818-B-R9-Map3_reversed\t714\t-\tok\tII\tTIR\tincomplete\tCI=66; coding=(TE_BLRtx: TC1_DM:classII:TIR: 40.88%; TE_BLRx: Tc1-1_TCa_1p:classII:TIR: 30.18%, Tc1-3_FR_1p:classII:TIR: 9.97%); struct=(TElength: >700bps); other=(SSRCoverage=0.08<0.75)\n")
            f.write("DTX-incomp_DmelChr4-L-B1-Map3\t714\t-\tok\tII\tTIR\tincomplete\tCI=66; coding=(TE_BLRtx: TC1_DM:classII:TIR: 40.88%; TE_BLRx: Tc1-1_TCa_1p:classII:TIR: 30.18%, Tc1-3_FR_1p:classII:TIR: 9.97%); struct=(TElength: >700bps); other=(SSRCoverage=0.08<0.75)\n")
            f.write("DTX-incomp_DmelChr4-B-P0.0-Map3\t714\t-\tok\tII\tTIR\tincomplete\tCI=66; coding=(TE_BLRtx: TC1_DM:classII:TIR: 40.88%; TE_BLRx: Tc1-1_TCa_1p:classII:TIR: 30.18%, Tc1-3_FR_1p:classII:TIR: 9.97%); struct=(TElength: >700bps); other=(SSRCoverage=0.08<0.75)\n")

    def _writeNewClassifFile(self, inputFileName):
        with open(inputFileName, "w") as f:
            f.write("Seq_name\tlength\tstrand\tconfused\tclass\torder\tWcode\tsFamily\tCI\tcoding\tstruct\tother\n")
            f.write("PotentialHostGene-chim_fTest05105818-B-G11-Map20\t1240\t+\tTrue\tPotentialHostGene|NA\tNA|NA\tNA|NA\tNA|NA\t100|NA\tcoding=(HG_BLRn: FBtr0089196_Dmel_r4.3: 95.65%)\tstruct=(NA)\tother=(TE_BLRtx: PROTOP:classII:TIR: 12.03%, PROTOP_A:classII:TIR: 49.14%; TermRepeats: termTIR: 49; SSRCoverage=0.25<0.75)\n")
            f.write("DTX-comp-chim_fTest05105818-B-G7-Map3_reversed\t1944\t-\tTrue\tII|NA\tTIR|NA\tDTX|NA\tNA|NA\t33|NA\tcoding=(TE_BLRtx: PROTOP:classII:TIR: 12.77%, PROTOP_A:classII:TIR: 25.16%, PROTOP_A:classII:TIR: 100.00%)\tstruct=(TElength: <700bps; TermRepeats: termTIR: 844)\tother=(HG_BLRn: FBtr0089196_Dmel_r4.3: 29.48%; SSRCoverage=0.24<0.75)\n")
            f.write("DTX-incomp_fTest05105818-B-G9-Map3_reversed\t1590\t-\tFalse\tII\tTIR\tDTX\tNA\t33\tcoding=(TE_BLRtx: PROTOP:classII:TIR: 10.92%, PROTOP:classII:TIR: 11.03%, PROTOP_A:classII:TIR: 55.20%)\tstruct=(TElength: >700bps)\tother=(HG_BLRn: FBtr0089196_Dmel_r4.3: 35.60%; SSRCoverage=0.21<0.75)\n")
            f.write("DTX-incomp_fTest05105818-B-P0.0-Map3\t1042\t.\tFalse\tII\tTIR\tDTX\tNA\t50\tcoding=(TE_BLRtx: PROTOP:classII:TIR: 17.39%, PROTOP_A:classII:TIR: 22.17%)\tstruct=(TElength: >700bps; TermRepeats: termTIR: 50)\tother=(HG_BLRn: FBtr0089196_Dmel_r4.3: 47.22%; SSRCoverage=0.25<0.75)\n")
            f.write("DTX-comp_fTest05105818-B-P1.0-Map9_reversed\t1137\t-\tFalse\tII\tTIR\tDTX\tNA\t50\tcoding=(TE_BLRtx: PROTOP:classII:TIR: 6.70%, PROTOP_A:classII:TIR: 66.43%, PROTOP_B:classII:TIR: 6.42%)\tstruct=(TElength: >700bps; TermRepeats: termTIR: 52)\tother=(HG_BLRn: FBtr0089196_Dmel_r4.3: 51.19%; SSRCoverage=0.22<0.75)\n")
            f.write("RLX-incomp_fTest05105818-B-R12-Map3_reversed\t2284\t-\tFalse\tI\tLTR\tRLX\tNA\t28\tcoding=(TE_BLRtx: ROOA_I:classI:LTR_retrotransposon: 27.57%, ROOA_LTR:classI:LTR_retrotransposon: 94.56%; TE_BLRx: BEL11_AGp:classI:LTR_retrotransposon: 19.47%, BEL2-I_Dmoj_1p:classI:LTR_retrotransposon: 11.49%)\tstruct=(TElength: >700bps)\tother=(SSRCoverage=0.07<0.75)\n")
            f.write("DTX-incomp_fTest05105818-B-R19-Map4\t705\t+\tFalse\tII\tTIR\tDTX\tNA\t66\tcoding=(TE_BLRtx: TC1-2_DM:classII:TIR: 42.70%; TE_BLRx: TC1-2_DMp:classII:TIR: 41.18%)\tstruct=(TElength: >700bps)\tother=(SSRCoverage=0.14<0.75)\n")
            f.write("DHX-incomp_fTest05105818-B-R1-Map4\t2367\t.\tFalse\tII\tHelitron\tDHX\tNA\t20\tcoding=(TE_BLRtx: DNAREP1_DM:classII:Helitron: 17.00%, DNAREP1_DYak:classII:Helitron: 9.08%)\tstruct=(TElength: >700bps)\tother=(HG_BLRn: FBtr0089179_Dmel_r4.3: 13.52%; SSRCoverage=0.18<0.75)\n")
            f.write("noCat_fTest05105818-B-R2-Map6\t4638\t.\tFalse\tUnclassified\tUnclassified\tNA\tNA\tNA\tcoding=(HG_BLRn: FBtr0089179_Dmel_r4.3: 73.65%)\tstruct=(SSRCoverage=0.05<0.75)\tother=(NA)\n")
            f.write("PotentialHostGene-chim_fTest05105818-B-R4-Map5_reversed\t1067\t-\tTrue\tPotentialHostGene|NA\tNA|NA\tNA|NA\tNA|NA\t100|NA\tcoding=(HG_BLRn: FBtr0089196_Dmel_r4.3: 99.91%)\tstruct=(NA)\tother=(TE_BLRtx: PROTOP:classII:TIR: 13.06%, PROTOP_A:classII:TIR: 37.47%; SSRCoverage=0.27<0.75)\n")
            f.write("DTX-incomp_fTest05105818-B-R9-Map3_reversed\t714\t-\tFalse\tII\tTIR\tDTX\tNA\t66\tcoding=(TE_BLRtx: TC1_DM:classII:TIR: 40.88%; TE_BLRx: Tc1-1_TCa_1p:classII:TIR: 30.18%, Tc1-3_FR_1p:classII:TIR: 9.97%)\tstruct=(TElength: >700bps)\tother=(SSRCoverage=0.08<0.75)\n")
            f.write("DTX-incomp_DmelChr4-L-B1-Map3\t714\t-\tFalse\tII\tTIR\tDTX\tNA\t66\tcoding=(TE_BLRtx: TC1_DM:classII:TIR: 40.88%; TE_BLRx: Tc1-1_TCa_1p:classII:TIR: 30.18%, Tc1-3_FR_1p:classII:TIR: 9.97%)\tstruct=(TElength: >700bps)\tother=(SSRCoverage=0.08<0.75)\n")
            f.write("DTX-incomp_DmelChr4-B-P0.0-Map3\t923\t-\tFalse\tII\tTIR\tDTX\tNA\t66\tcoding=(TE_BLRtx: TC1_DM:classII:TIR: 40.88%; TE_BLRx: Tc1-1_TCa_1p:classII:TIR: 30.18%, Tc1-3_FR_1p:classII:TIR: 9.97%)\tstruct=(TElength: >700bps)\tother=(SSRCoverage=0.08<0.75)\n")

    def _writePathFile_withClassif_withIdenticalMatches(self, inFileName):
        with open(inFileName, 'w') as f:
            f.write('1\tlm_SuperContig_29_v2\t193781\t194212\t1nc550_030\t228\t85\t1e-40\t84\t30.56\n')
            f.write('2\tlm_SuperContig_29_v2\t192832\t193704\t1nc550_030\t522\t229\t1e-40\t106\t23.99\n')
            f.write('3\tlm_SuperContig_30_v2\t78081\t78088\tDHX-incomp_Blc1_fTest05105818-B-R1-Map4\t19\t209\t3e-21\t101\t30.89\n')
            f.write('3\tlm_SuperContig_30_v2\t78089\t78588\tDHX-incomp_Blc1_fTest05105818-B-R1-Map4\t150\t350\t3e-22\t101\t35.89\n')
            f.write('4\tlm_SuperContig_30_v2\t88031\t88080\tDTX-incomp_fTest05105818-B-G1-Map3\t370\t420\t3e-23\t101\t31.89\n')
            f.write('5\tlm_SuperContig_30_v2\t108588\t108081\tDTX-incomp_fTest05105818-B-G9-Map3_reversed\t590\t820\t3e-24\t101\t32.89\n')
            f.write('6\tlm_SuperContig_30_v2\t118081\t118588\tPotentialHostGene-chim_Blc6_fTest05105818-B-R4-Map5_reversed\t154\t289\t3e-25\t101\t33.89\n')

            f.write('7\tlm_SuperContig_30_v2\t288031\t288080\tnoCat_Blc22_fTest05105818-B-R2-Map6\t1900\t2090\t3e-26\t101\t34.89\n')
            f.write('8\tlm_SuperContig_30_v2\t288031\t288080\tDTX-incomp_fTest05105818-B-P0.0-Map3\t100\t190\t3e-26\t101\t39.89\n')
            f.write('9\tlm_SuperContig_30_v2\t288031\t288080\tRLX-incomp_fTest05105818-B-R12-Map3_reversed\t1100\t1290\t3e-26\t101\t40.89\n')
            f.write('10\tlm_SuperContig_30_v2\t288031\t288080\tPotentialHostGene-chim_fTest05105818-B-G11-Map20\t990\t1890\t3e-26\t101\t38.09\n')

            f.write('11\tlm_SuperContig_30_v2\t288031\t288080\tDTX-incomp_fTest05105818-B-G1-Map3\t990\t1890\t3e-26\t301\t38.09\n')

            f.write('12\tlm_SuperContig_30_v2\t388031\t388080\tDHX-incomp_Blc1_fTest05105818-B-R1-Map4\t19\t209\t3e-21\t101\t30.89\n')
            f.write('12\tlm_SuperContig_30_v2\t388081\t388380\tDHX-incomp_Blc1_fTest05105818-B-R1-Map4\t150\t350\t3e-22\t101\t35.89\n')

            f.write('13\tlm_SuperContig_30_v2\t388031\t388080\tDTX-incomp_fTest05105818-B-P0.0-Map3\t119\t309\t3e-21\t101\t30.89\n')
            f.write('13\tlm_SuperContig_30_v2\t388081\t388380\tDTX-incomp_fTest05105818-B-P0.0-Map3\t250\t450\t3e-22\t101\t35.89\n')

    def _writeExpPathGFFFile_without_seq_withClassif_withIdenticalMatches(self, inFileName):
        with open(inFileName, 'w') as f:
            f.write("##gff-version 3\n")
            f.write("##sequence-region lm_SuperContig_30_v2 1 120\n")
            f.write("lm_SuperContig_30_v2\tPN_REPET_TEs\tmatch\t78081\t78588\t0.0\t+\t.\tID=ms3_lm_SuperContig_30_v2_DHX-incomp_Blc1_fTest05105818-B-R1-Map4;Target=DHX-incomp_Blc1_fTest05105818-B-R1-Map4 19 350;TargetDescription=CI:20 coding:(TE_BLRtx: DNAREP1_DM:classII:Helitron: 17.00% | DNAREP1_DYak:classII:Helitron: 9.08%) struct:(TElength: >700bps) other:(HG_BLRn: FBtr0089179_Dmel_r4.3: 13.52% SSRCoverage:0.18<0.75)\n")
            f.write("lm_SuperContig_30_v2\tPN_REPET_TEs\tmatch_part\t78081\t78088\t3e-21\t+\t.\tID=mp3-1_lm_SuperContig_30_v2_DHX-incomp_Blc1_fTest05105818-B-R1-Map4;Parent=ms3_lm_SuperContig_30_v2_DHX-incomp_Blc1_fTest05105818-B-R1-Map4;Target=DHX-incomp_Blc1_fTest05105818-B-R1-Map4 19 209;Identity=30.89\n")
            f.write("lm_SuperContig_30_v2\tPN_REPET_TEs\tmatch_part\t78089\t78588\t3e-22\t+\t.\tID=mp3-2_lm_SuperContig_30_v2_DHX-incomp_Blc1_fTest05105818-B-R1-Map4;Parent=ms3_lm_SuperContig_30_v2_DHX-incomp_Blc1_fTest05105818-B-R1-Map4;Target=DHX-incomp_Blc1_fTest05105818-B-R1-Map4 150 350;Identity=35.89\n")

            f.write("lm_SuperContig_30_v2\tPN_REPET_TEs\tmatch\t88031\t88080\t0.0\t+\t.\tID=ms4_lm_SuperContig_30_v2_DTX-incomp_fTest05105818-B-G1-Map3;Target=DTX-incomp_fTest05105818-B-G1-Map3 370 420;Identity=31.89\n")
            f.write("lm_SuperContig_30_v2\tPN_REPET_TEs\tmatch_part\t88031\t88080\t3e-23\t+\t.\tID=mp4-1_lm_SuperContig_30_v2_DTX-incomp_fTest05105818-B-G1-Map3;Parent=ms4_lm_SuperContig_30_v2_DTX-incomp_fTest05105818-B-G1-Map3;Target=DTX-incomp_fTest05105818-B-G1-Map3 370 420;Identity=31.89\n")

            f.write("lm_SuperContig_30_v2\tPN_REPET_TEs\tmatch\t108081\t108588\t0.0\t-\t.\tID=ms5_lm_SuperContig_30_v2_DTX-incomp_fTest05105818-B-G9-Map3_reversed;Target=DTX-incomp_fTest05105818-B-G9-Map3_reversed 590 820;TargetDescription=CI:33 coding:(TE_BLRtx: PROTOP:classII:TIR: 10.92% | PROTOP:classII:TIR: 11.03% | PROTOP_A:classII:TIR: 55.20%) struct:(TElength: >700bps) other:(HG_BLRn: FBtr0089196_Dmel_r4.3: 35.60% SSRCoverage:0.21<0.75);Identity=32.89\n")
            f.write("lm_SuperContig_30_v2\tPN_REPET_TEs\tmatch_part\t108081\t108588\t3e-24\t-\t.\tID=mp5-1_lm_SuperContig_30_v2_DTX-incomp_fTest05105818-B-G9-Map3_reversed;Parent=ms5_lm_SuperContig_30_v2_DTX-incomp_fTest05105818-B-G9-Map3_reversed;Target=DTX-incomp_fTest05105818-B-G9-Map3_reversed 590 820;Identity=32.89\n")

            f.write("lm_SuperContig_30_v2\tPN_REPET_TEs\tmatch\t118081\t118588\t0.0\t+\t.\tID=ms6_lm_SuperContig_30_v2_PotentialHostGene-chim_Blc6_fTest05105818-B-R4-Map5_reversed;Target=PotentialHostGene-chim_Blc6_fTest05105818-B-R4-Map5_reversed 154 289;TargetDescription=CI:100 coding:(HG_BLRn: FBtr0089196_Dmel_r4.3: 99.91%) other:(TE_BLRtx: PROTOP:classII:TIR: 13.06% | PROTOP_A:classII:TIR: 37.47% SSRCoverage:0.27<0.75);Identity=33.89\n")
            f.write("lm_SuperContig_30_v2\tPN_REPET_TEs\tmatch_part\t118081\t118588\t3e-25\t+\t.\tID=mp6-1_lm_SuperContig_30_v2_PotentialHostGene-chim_Blc6_fTest05105818-B-R4-Map5_reversed;Parent=ms6_lm_SuperContig_30_v2_PotentialHostGene-chim_Blc6_fTest05105818-B-R4-Map5_reversed;Target=PotentialHostGene-chim_Blc6_fTest05105818-B-R4-Map5_reversed 154 289;Identity=33.89\n")

            f.write("lm_SuperContig_30_v2\tPN_REPET_TEs\tmatch\t288031\t288080\t0.0\t+\t.\tID=ms10_lm_SuperContig_30_v2_PotentialHostGene-chim_fTest05105818-B-G11-Map20;Target=PotentialHostGene-chim_fTest05105818-B-G11-Map20 990 1890;OtherTargets=RLX-incomp_fTest05105818-B-R12-Map3_reversed 1100 1290, DTX-incomp_fTest05105818-B-P0.0-Map3 100 190, noCat_Blc22_fTest05105818-B-R2-Map6 1900 2090\n")
            f.write("lm_SuperContig_30_v2\tPN_REPET_TEs\tmatch_part\t288031\t288080\t3e-26\t+\t.\tID=mp10-1_lm_SuperContig_30_v2_PotentialHostGene-chim_fTest05105818-B-G11-Map20;Parent=ms10_lm_SuperContig_30_v2_PotentialHostGene-chim_fTest05105818-B-G11-Map20;Target=PotentialHostGene-chim_fTest05105818-B-G11-Map20 990 1890\n")

            f.write("lm_SuperContig_30_v2\tPN_REPET_TEs\tmatch\t288031\t288080\t0.0\t+\t.\tID=ms11_lm_SuperContig_30_v2_DTX-incomp_fTest05105818-B-G1-Map3;Target=DTX-incomp_fTest05105818-B-G1-Map3 990 1890;Identity=38.09\n")
            f.write("lm_SuperContig_30_v2\tPN_REPET_TEs\tmatch_part\t288031\t288080\t3e-26\t+\t.\tID=mp11-1_lm_SuperContig_30_v2_DTX-incomp_fTest05105818-B-G1-Map3;Parent=ms11_lm_SuperContig_30_v2_DTX-incomp_fTest05105818-B-G1-Map3;Target=DTX-incomp_fTest05105818-B-G1-Map3 990 1890;Identity=38.09\n")

            # TODO:
            # Should this case really occur : If merging multiple match-parts, the current behaviour needs to be fixed to get correct subject start/end coordinates
            f.write("lm_SuperContig_30_v2\tPN_REPET_TEs\tmatch\t388031\t388380\t0.0\t+\t.\tID=ms12_lm_SuperContig_30_v2_DHX-incomp_Blc1_fTest05105818-B-R1-Map4;Target=DHX-incomp_Blc1_fTest05105818-B-R1-Map4 19 350;OtherTargets=DTX-incomp_fTest05105818-B-P0.0-Map3 119 309\n")
            f.write("lm_SuperContig_30_v2\tPN_REPET_TEs\tmatch_part\t388031\t388080\t3e-21\t+\t.\tID=mp12-1_lm_SuperContig_30_v2_DHX-incomp_Blc1_fTest05105818-B-R1-Map4;Parent=ms12_lm_SuperContig_30_v2_DHX-incomp_Blc1_fTest05105818-B-R1-Map4;Target=DHX-incomp_Blc1_fTest05105818-B-R1-Map4 19 209\n")
            f.write("lm_SuperContig_30_v2\tPN_REPET_TEs\tmatch_part\t388081\t388380\t3e-22\t+\t.\tID=mp12-2_lm_SuperContig_30_v2_DHX-incomp_Blc1_fTest05105818-B-R1-Map4;Parent=ms12_lm_SuperContig_30_v2_DHX-incomp_Blc1_fTest05105818-B-R1-Map4;Target=DHX-incomp_Blc1_fTest05105818-B-R1-Map4 150 350\n")

    def _writeTablesFile(self, tableType):
        with open(self._tablesFileName, "w") as tableFile:
            string = "{}_REPET_TEs\t{}\t{}_chr_allTEs_nr_noSSR_join_{}\n".format(self._projectName, tableType, self._projectName, tableType)
            tableFile.write(string)

    def _writePathFile(self, inFileName):
        with open(inFileName, 'w') as f:
            f.write('1\tlm_SuperContig_29_v2\t193781\t194212\tDTX-incomp_DmelChr4-L-B1-Map3\t228\t85\t1e-40\t84\t30.56\n')
            f.write('2\tlm_SuperContig_29_v2\t192832\t193704\tDTX-incomp_DmelChr4-L-B1-Map3\t522\t229\t1e-40\t106\t23.99\n')
            f.write('3\tlm_SuperContig_29_v2\t78031\t78080\tDTX-incomp_DmelChr4-B-P0.0-Map3\t19\t209\t3e-21\t101\t30.89\n')
            f.write('3\tlm_SuperContig_29_v2\t78081\t78588\tDTX-incomp_DmelChr4-B-P0.0-Map3\t419\t609\t3e-21\t101\t50.89\n')

    def _writeTabFile_alignCopyOnConsensus(self, inFileName):
        with open(inFileName, 'w') as f:
            f.write('PathId\talignLength\talignIdent\tscore\n')
            f.write('1\t720\t29.06\t456\n')
            f.write('2\t716\t23.99\t56\n')
            f.write('3\t923\t80.97\t3002\n')

    def _writeFastaFile(self, inFileName):
        with open(inFileName, 'w') as f:
            self._writeSeq2(f)
            self._writeSeq1(f)

    def _writeSeq1(self, f):
        f.write('>lm_SuperContig_29_v2\n')
        f.write('CCTAGACAATTAATTATAATAATTAATAAACTATTAGGCTAGTAGTAGGTAATAATAAAA\n')
        f.write('GGATTACTACTAAGCTGCGCTATGTAGATATTTAAAACATGTGGCTTAGGCAAGAGTATA\n')

    def _writeSeq2(self, f):
        f.write('>lm_SuperContig_30_v2\n')
        f.write('TGTTCATATTCATAGGATGGAGCTAGTAAGCGATGTCGGCTTAGCTCATCCACATGAATG\n')
        f.write('CAGGAATCATGAAGGGTACGACTGTTCGTCGATTAAAGAGCTACACGAGCTGGGTTAAAT\n')

    def _writeFastaFile_DmelChr4(self, inFileName):
        with open(inFileName, 'w') as f:
            f.write(">dmel_chr4\n")
            f.write("CTAAGCTGCGCTATGTAG\n")
            f.write(">dmel_chr1\n")
            f.write("CGTAACGCTAGCGCTTATAGTGAGC\n")


if __name__ == "__main__":
    unittest.main()
