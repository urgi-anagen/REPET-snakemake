import os
import random
import shutil

import time
import unittest
import pandas
#from commons.core.sql.DbFactory import DbFactory
from commons.core.utils.FileUtils import FileUtils
from commons.tools.AnnotationStats import AnnotationStats


class Test_F_AnnotationStats(unittest.TestCase):

    def setUp(self):
        self._uniqId = "{}_{}".format(time.strftime("%H%M%S"),random.randint(0, 1000))
        self.singularity_img = "../../../../../../Singularity/te_finder_2.30.2.sif"
        self._curTestDir = os.getcwd()
        self._dirName = "Test_F_AS_{}".format(self._uniqId)
        self._workingDir = os.path.join(os.getcwd(), self._dirName)
        os.makedirs(self._workingDir)
        os.chdir(self._workingDir)
        self.iAS = None

        self.pathFileName = "Dummy.path"
        self._writePath_File(self.pathFileName)
        self.fastaTEFileName = "DummyTE.fa"
        self._writeTEFastaFile(self.fastaTEFileName)
        self.fileStats = "DummyStat.txt"
        self.globalFileStats = "DummyGlobalStat.txt"

    def tearDown(self):
        os.chdir(self._curTestDir)
        try:
            shutil.rmtree(self._workingDir)
        except:pass

    def test_run_TEanalysis(self):
        classif_file_name = 'classifFile.classif'
        self._writeClassifFile(classif_file_name)
        self.iAS = AnnotationStats(analysisName="TE", seqFileName=self.fastaTEFileName,
                                                pathFileName=self.pathFileName,
                                                classifConsensusFile =classif_file_name,
                                                genomeLength=702, 
                                                statsFileName=self.fileStats,
                                                globalStatsFileName=self.globalFileStats,
                                                singularityImg = self.singularity_img)
        self.iAS.run()
        obsGlobalStatsResults = self.globalFileStats
        expGlobalStatsResults = "expGlobalAnnotationStats.txt"
        self._writeExpGlobalStatsResultsFile(expGlobalStatsResults)

        
        obsStatsResults = self.fileStats
        expStatsResults = "expAnnotationStats.txt"
        self._writeExpStatsResultsFile(expStatsResults)
        df_expected = pandas.read_csv(expStatsResults, sep='\t',  index_col=0, low_memory=False)
        df_observed =  pandas.read_csv(obsStatsResults, sep='\t',  index_col=0, low_memory=False)
       
        df_expected_sorted = df_expected.sort_values(by=['Wcode','TE']).reset_index(drop=True)
        df_observed_sorted = df_observed.sort_values(by=['Wcode','TE']).reset_index(drop=True)
       

        pandas.testing.assert_frame_equal(df_expected_sorted,df_observed_sorted)
        self.assertTrue(df_expected_sorted.equals(df_observed_sorted))
        self.assertTrue(FileUtils.are2FilesIdentical(expGlobalStatsResults, obsGlobalStatsResults))
        
        #self.assertTrue(FileUtils.are2FilesIdentical(expStatsResults, obsStatsResults))

    def test_run_TEanalysis_noTables(self):
        #self.assertRaises(Exception, iAS.run)
        with self.assertRaises(Exception):
            iAS = AnnotationStats(analysisName="TE",  seqFileName="", pathFileName="",
                                  globalStatsFileName=self.globalFileStats, 
                                  statsFileName=self.fileStats, genomeLength=702,
                                  singularityImg = self.singularity_img)
           

    def test_run_clusterAnalysis(self):
        clusterFileName = "DummyCluster.fa"
        self._writeClusterFile(clusterFileName)

        self.iAS = AnnotationStats(analysisName="Cluster", clusterFileName=clusterFileName,
                                    seqFileName=self.fastaTEFileName,pathFileName=self.pathFileName,
                                    statsFileName=self.fileStats,singularityImg = self.singularity_img)
        self.iAS.run()
        obsStatsResults = self.fileStats
        expStatsClusterResults = "expClusterAnnotationStats.txt"
        self._writeExpStatsClusterResultsFile(expStatsClusterResults)
        self.assertTrue(FileUtils.are2FilesIdentical(expStatsClusterResults, obsStatsResults))


    def _writePath_File(self, fileName):
        with open(fileName, "w") as f:
            f.write("1\tdmel_chr4\t1\t22\trefTE_227\t128\t145\t0\t148\t69.7\n")  # 22
            f.write("2\tdmel_chr4\t2\t40\trefTE_324\t648\t686\t0\t767\t85.8\n")  # 39
            f.write("3\tdmel_chr4\t91\t100\trefTE_617\t41\t48\t0\t580\t85.4\n")  # 10
            f.write("4\tdmel_chr4\t110\t119\trefTE_617\t41\t48\t0\t743\t84.9\n")  # 9
            f.write("5\tdmel_chr4\t150\t189\trefTE_200\t50\t89\t0\t221\t95.3\n")  # 40
            f.write("6\tdmel_chr4\t235\t265\trefTE_878\t76\t109\t0\t238\t90.7\n")  # 31
            f.write("7\tdmel_chr4\t370\t388\trefTE_161\t231\t249\t0\t244\t91.3\n")  # 19
            f.write("8\tdmel_chrU\t1\t22\trefTE_227\t220\t128\t145\t251\t92.3\n")  # 22
            f.write("9\tdmel_chrU\t2\t40\trefTE_324\t648\t686\t0\t244\t91.3\n")  # 39
            f.write("10\tdmel_chrU\t91\t100\trefTE_43\t41\t48\t0\t240\t90.8\n")  # 10
            f.write("11\tdmel_chrU\t100\t144\trefTE_45\t218\t260\t0\t262\t93.8\n")  # 45
            f.write("12\tdmel_chrU\t200\t250\trefTE_227\t4182\t4230\t0\t266\t94.4\n")  # 51

    def _writeClassifFile( self,filename ):
        with open(filename, "w") as f:
            f.write("Seq_name\tlength\tstrand\tconfused\tclass\torder\tWcode\tsFamily\tCI\tcoding\tstruct\tother\n")
            f.write("refTE_227\t661\t.\tTrue\tI|II\tLINE|TIR\tRIX|DTX\tNA\tcoding=(NA)\tstruct=(NA)\tother=(NA)\n")
            f.write("refTE_324\t3254\t.\tFalse\tNA\tNA\tPHG\tNA\t100\tcoding=(NA)\tstruct=(NA)\tother=(NA)\n")
            f.write("refTE_617\t5622\t+\tFalse\tI\tLINE\tRIX\tNA\t90\tcoding=(NA)\tstruct=(NA)\tother=(NA)\n")
            f.write("refTE_200\t6017\t+\tTrue\tI|I\tLINE|LTR\tRIX|RLX\tNA|NA\t63|28\tcoding=(NA)\tstruct=(NA)\tother=(NA)\n")
            f.write("refTE_878\t661\t.\tFalse\tUnclassified\tUnclassified\tNA\tNA\tNA\tcoding=(NA)\tstruct=(NA)\tother=(NA)\n")
            f.write("refTE_161\t3254\t.\tFalse\tNA\tNA\tSSR\tNA\t100\tcoding=(NA)\tstruct=(NA)\tother=(NA)\n")
            f.write("refTE_43\t5622\t+\tFalse\tI\tPLE\tRIX\tNA\t90\tcoding=(NA)\tstruct=(NA)\tother=(NA)\n")
            f.write("refTE_45\t6017\t+\tFalse\tII\tHelitron\tDHX\tNA\t63\tcoding=(NA)\tstruct=(NA)\tother=(TNA)\n")

    def _writeTEFastaFile(self, fileName):
        with open(fileName, "w") as f:  # total length 702
            f.write(">refTE_227\n")  # seq length 53
            f.write("AAGTCAGTCTGTTTGGATCGCTCTCTGCTCGGAAATCCTCTATATTCGCGCA\n")
            f.write(">refTE_324\n")  # seq length 409
            f.write("AAGTCAGTCTGTTTGGATCGCTCTCTCAGCTAGCTAGTGTGAGTTTCGCGCATCGATCGA\n")
            f.write(">refTE_617\n")  # seq length 49
            f.write("ATCGCTAGCTAGCTCGATCTAGTCAGTCTGTTTGGATCGATCGATCGAT\n")
            f.write(">refTE_200\n")  # seq length 34
            f.write("CTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write(">refTE_878\n")  # seq length 65
            f.write("AGCTAGTGTGAGTAGTAGTTTGGATCGCGATCGGCGGCTATATGCTAGTCAGCTAGCTAG\n")
            f.write("TAGTG\n")
            f.write(">refTE_161\n")  # seq length 26
            f.write("TATGCTAGTCAGCTAGCTAGCTAGTG\n")
            f.write(">refTE_43\n")  # seq length 25
            f.write("AGTCAGCTAGCTAGTGTGAGTAGTA\n")
            f.write(">refTE_45\n")  # seq length 60
            f.write("ATATTCGCGCATCGATCGATCGGCGGCTATATGCTAGTCAGCTAGCTAGTGTGAGTAGTA\n")

    def _writeClusterFile(self, fileName):
        with open(fileName, "w") as f:  # total length 702
            f.write("refTE_227\trefTE_324\trefTE_617\n")
            f.write("refTE_200\trefTE_878\n")
            f.write("refTE_161\n")
            f.write("refTE_43\n")
            f.write("refTE_45\n")

    def _writeExpStatsResultsFile(self, fileName):
        with open(fileName, "w") as f:
            f.write(
                "Wcode\tTE\tlength\tcovg\tfrags\tfullLgthFrags\tcopies\tfullLgthCopies\tmeanId\tsdId\tminId\tq25Id\tmedId\tq75Id\tmaxId\tmeanLgth\tsdLgth\tminLgth\tq25Lgth\tmedLgth\tq75Lgth\tmaxLgth\tmeanLgthPerc\tsdLgthPerc\tminLgthPerc\tq25LgthPerc\tmedLgthPerc\tq75LgthPerc\tmaxLgthPerc\n")
            f.write(
                "SSR\trefTE_161\t26\t19\t1\t0\t1\t0\t91.30\t0.00\t91.30\t91.30\t91.30\t91.30\t91.30\t19.00\t0.00\t19\t19.00\t19.00\t19.00\t19\t73.08\t0.00\t73.08\t73.08\t73.08\t73.08\t73.08\n")
            f.write(
                "RIX|RLX\trefTE_200\t34\t40\t1\t0\t1\t0\t95.30\t0.00\t95.30\t95.30\t95.30\t95.30\t95.30\t40.00\t0.00\t40\t40.00\t40.00\t40.00\t40\t117.65\t0.00\t117.65\t117.65\t117.65\t117.65\t117.65\n")
            f.write(
                "RIX|DTX\trefTE_227\t52\t95\t3\t1\t3\t1\t85.47\t13.69\t69.70\t69.70\t92.30\t94.40\t94.40\t31.67\t16.74\t22\t22.00\t22.00\t51.00\t51\t60.90\t32.20\t42.31\t42.31\t42.31\t98.08\t98.08\n")
            f.write(
                "PHG\trefTE_324\t60\t78\t2\t0\t2\t0\t88.55\t3.89\t85.80\t85.80\t88.55\t91.30\t91.30\t39.00\t0.00\t39\t39.00\t39.00\t39.00\t39\t65.00\t0.00\t65.00\t65.00\t65.00\t65.00\t65.00\n")
            f.write(
                "RIX\trefTE_43\t25\t10\t1\t0\t1\t0\t90.80\t0.00\t90.80\t90.80\t90.80\t90.80\t90.80\t10.00\t0.00\t10\t10.00\t10.00\t10.00\t10\t40.00\t0.00\t40.00\t40.00\t40.00\t40.00\t40.00\n")
            f.write(
                "DHX\trefTE_45\t60\t45\t1\t0\t1\t0\t93.80\t0.00\t93.80\t93.80\t93.80\t93.80\t93.80\t45.00\t0.00\t45\t45.00\t45.00\t45.00\t45\t75.00\t0.00\t75.00\t75.00\t75.00\t75.00\t75.00\n")
            f.write(
                "RIX\trefTE_617\t49\t20\t2\t0\t2\t0\t85.15\t0.35\t84.90\t84.90\t85.15\t85.40\t85.40\t10.00\t0.00\t10\t10.00\t10.00\t10.00\t10\t20.41\t0.00\t20.41\t20.41\t20.41\t20.41\t20.41\n")
            f.write(
                "NA\trefTE_878\t65\t31\t1\t0\t1\t0\t90.70\t0.00\t90.70\t90.70\t90.70\t90.70\t90.70\t31.00\t0.00\t31\t31.00\t31.00\t31.00\t31\t47.69\t0.00\t47.69\t47.69\t47.69\t47.69\t47.69\n")

    def _writeExpGlobalStatsResultsFile(self, fileName):
        with open(fileName, "w") as f:
            f.write("nb of sequences: 8\n")
            f.write("nb of matched sequences: 8\n")
            f.write("cumulative coverage: 295 bp\n")
            f.write("coverage percentage: 42.02%\n")
            f.write("\n")
            f.write("total nb of TE fragments: 12\n")
            f.write("total nb full-length fragments: 1 (8.33%)\n")
            f.write("total nb of TE copies: 12\n")
            f.write("total nb full-length copies: 1 (8.33%)\n")
            f.write("families with full-length fragments: 1 (12.50%)\n")
            f.write(" with only one full-length fragment: 1\n")
            f.write(" with only two full-length fragments: 0\n")
            f.write(" with only three full-length fragments: 0\n")
            f.write(" with more than three full-length fragments: 0\n")
            f.write("families with full-length copies: 1 (12.50%)\n")
            f.write(" with only one full-length copy: 1\n")
            f.write(" with only two full-length copies: 0\n")
            f.write(" with only three full-length copies: 0\n")
            f.write(" with more than three full-length copies: 0\n")
            f.write("mean of median identity of all families: 90.99 +- 3.13\n")
            f.write("mean of median length percentage of all families: 60.14 +- 29.70\n")

    def _writeExpStatsClusterResultsFile(self, fileName):
        with open(fileName, "w") as f:
            f.write("Cluster\tcovg\tfrags\tcopies\n")
            f.write("1\t193\t7\t7\n")
            f.write("2\t71\t2\t2\n")
            f.write("3\t19\t1\t1\n")
            f.write("4\t10\t1\t1\n")
            f.write("5\t45\t1\t1\n")

    def _writeConfigFile(self):
        with open(self.configFileName, "w") as f:
            f.write("[repet_env]\n")
            f.write("repet_host: {}\n".format(os.environ["REPET_HOST"]))
            f.write("repet_user: {}\n".format(os.environ["REPET_USER"]))
            f.write("repet_pw: {}\n".format(os.environ["REPET_PW"]))
            f.write("repet_db: {}\n".format(os.environ["REPET_DB"]))
            f.write("repet_port: {}\n".format(os.environ["REPET_PORT"]))
            f.write("[project]\n")
            f.write("project_name: project\n")


if __name__ == "__main__":
    unittest.main()
