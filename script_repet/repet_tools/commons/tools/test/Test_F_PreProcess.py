import unittest
import os
import time
from shutil import rmtree
from commons.core.utils.FileUtils import FileUtils
from commons.tools.PreProcess import PreProcess
import random
import subprocess


class Test_F_PreProcess(unittest.TestCase):

    def setUp(self):
        self._curTestDir = os.getcwd()
        self._testPrefix = 'test_F_{}{}'.format(time.strftime("%H%M%S"), random.randint(0, 1000))
        self.inputFastaFile = "PreProcess.fa"
        self.inputFastaFile2 = "PreProcessStep12.fa"

        self.expStatInputFile = "exp_PreProcess.fa.stats"
        self.inputStatFile = "PreProcess.fa.stats"

        self.expStatFormatedFile = "exp_PreProcess.fa.formated.stats"
        self.outputStatTmpFile = "PreProcess.fa.formated.stats"

        self.outputlowComplexityMaskFile2 = "PreProcessStep12.fa.formated"

        self.outputFastaFile = "PreProcess.fa_chunks.fa"
        self.expectedFastaFile = "exp_PreProcess.fa_chunks"

        self.expStatChunkedFile = "exp_PreProcess.fa_chunks.stats"
        self.outputStatChunkedFile = "{}.stats".format(self.outputFastaFile)
        self.outputChunkMapFile = "{}_chunks.map".format(self.inputFastaFile)

        self.expChunkFastaFile = "exp_PreProcess_withNStretch.fa_chunks.fa"
        self.expChunkMapFile = "exp_PreProcess_withNStretch.fa_chunks.map"

        self.expSubsetFile = "exp_PreProcess_step3_3seq.fa"
        self.outputSubsetFile = "PreProcess.fa_subset.fa"
        self.expSubsetFile2 = "exp_PreProcess_step3_1seq.fa"
        self.expStatStep2 = "{}_subset.stats".format(self.expChunkFastaFile)
        self.outputStatStep2 = "exp_PreProcess_withNStretch.fa_chunks.fa_subset.stats"

        try:
            os.makedirs(self._testPrefix)
        except:
            pass
        os.chdir(self._testPrefix)

        cmd = "ln -s {}/commons/tools/tests/data/{} {} ".format(os.environ["REPET_PATH"], self.inputFastaFile,
                                                                self.inputFastaFile)
        subprocess.call(cmd, shell=True)

        cmd1 = "ln -s {}/commons/tools/tests/data/{} {} ".format(os.environ["REPET_PATH"], self.expectedFastaFile,
                                                                 self.expectedFastaFile)
        subprocess.call(cmd1, shell=True)

        cmd2 = "ln -s {}/commons/tools/tests/data/{} {} ".format(os.environ["REPET_PATH"], self.expChunkFastaFile,
                                                                 self.expChunkFastaFile)
        subprocess.call(cmd2, shell=True)

        cmd3 = "ln -s {}/commons/tools/tests/data/{} {} ".format(os.environ["REPET_PATH"], self.expChunkMapFile,
                                                                 self.expChunkMapFile)
        subprocess.call(cmd3, shell=True)

        cmd4 = "ln -s {}/commons/tools/tests/data/{} {} ".format(os.environ["REPET_PATH"], self.expSubsetFile,
                                                                 self.expSubsetFile)
        subprocess.call(cmd4, shell=True)

        cmd5 = "ln -s {}/commons/tools/tests/data/{} {} ".format(os.environ["REPET_PATH"], self.expSubsetFile2,
                                                                 self.expSubsetFile2)
        subprocess.call(cmd5, shell=True)

        cmd6 = "ln -s {}/commons/tools/tests/data/{} {} ".format(os.environ["REPET_PATH"], self.inputFastaFile2,
                                                                 self.inputFastaFile2)
        subprocess.call(cmd6, shell=True)

        self._writeStatsFile(self.expStatInputFile)
        self._writeStatsFile2(self.expStatFormatedFile)
        self._writeStatsFile3(self.expStatChunkedFile)
        self._writeStatsFile4(self.expStatStep2)

    def tearDown(self):
        os.chdir(self._curTestDir)
        try:
            rmtree(self._testPrefix)
        except:
            pass

    def test_PreProcess_with_SSR_Step12(self):

        inFileName = self.inputFastaFile2
        self._writeExpFormatedFastaFileWithSSR("expected")
        iPP = PreProcess(inFileName, step=12, verbosity=2)
        iPP.setLength(100)
        iPP.run()
        dirStep1 = "{}_CheckFasta".format(self.inputFastaFile2.split(".fa")[0])
        self.assertTrue(
            FileUtils.are2FilesIdentical("expected", "{}/{}".format(dirStep1, self.outputlowComplexityMaskFile2)))

    def test_PreProcess_with_SSR_Step123(self):

        inFileName = self.inputFastaFile2
        self._writeExpFormatedFastaFileWithSSR("expected")
        iPP = PreProcess(inFileName, step=123, verbosity=3)
        iPP.setLength(100)
        iPP.setsubstep3(1)
        iPP.setInfoForStep3(110)
        iPP.run()

        dirStep1 = "{}_CheckFasta".format(self.inputFastaFile2.split(".fa")[0])
        dirStep3 = "{}_SubSet".format(self.inputFastaFile.split(".fa")[0])

        self.assertTrue(
            FileUtils.are2FilesIdentical("expected", "{}/{}".format(dirStep1, self.outputlowComplexityMaskFile2)))

    def test_PreProcess_with_SSR_Step2(self):

        inFileName = self.inputFastaFile2
        self._writeExpFormatedFastaFileWithSSR("expected")
        iPP = PreProcess(inFileName, step=2, verbosity=3)
        iPP.setLength(100)
        iPP.run()

    def test_PreProcess_Step1(self):

        iPP = PreProcess("{}".format(self.inputFastaFile), outputFasta=self.outputFastaFile, step=1, verbosity=5)
        iPP.setWord(3)
        iPP.run()

        dirStep1 = "{}_CheckFasta".format(self.inputFastaFile.split(".fa")[0])

        self.assertTrue(
            FileUtils.are2FilesIdentical(self.expStatInputFile, "{}/{}".format(dirStep1, self.inputStatFile)))
        self.assertTrue(
            FileUtils.are2FilesIdentical(self.expStatFormatedFile, "{}/{}".format(dirStep1, self.outputStatTmpFile)))
        self.assertTrue(
            FileUtils.are2FilesIdentical(self.expChunkFastaFile, "{}/{}".format(dirStep1, self.outputFastaFile)))
        self.assertTrue(
            FileUtils.are2FilesIdentical(self.expChunkMapFile, "{}/{}".format(dirStep1, self.outputChunkMapFile)))
        self.assertTrue(
            FileUtils.are2FilesIdentical(self.expStatChunkedFile, "{}/{}".format(dirStep1, self.outputStatChunkedFile)))

    def test_PreProcess_asScript_Step1(self):

        os.system("PreProcess.py -S 1 -i {} -o {} -w 3 -v 3".format(self.inputFastaFile, self.outputFastaFile))

        dirStep1 = "{}_CheckFasta".format(self.inputFastaFile.split(".fa")[0])

        self.assertTrue(
            FileUtils.are2FilesIdentical(self.expStatInputFile, "{}/{}".format(dirStep1, self.inputStatFile)))
        self.assertTrue(
            FileUtils.are2FilesIdentical(self.expStatFormatedFile, "{}/{}".format(dirStep1, self.outputStatTmpFile)))
        self.assertTrue(
            FileUtils.are2FilesIdentical(self.expChunkFastaFile, "{}/{}".format(dirStep1, self.outputFastaFile)))
        self.assertTrue(
            FileUtils.are2FilesIdentical(self.expChunkMapFile, "{}/{}".format(dirStep1, self.outputChunkMapFile)))
        self.assertTrue(
            FileUtils.are2FilesIdentical(self.expStatChunkedFile, "{}/{}".format(dirStep1, self.outputStatChunkedFile)))

    def test_PreProcess_step1_wrongHeader(self):
        self._writeFastaFileWithWrongHeaders("toto.fa")
        self._writeExpFormatedFastaFileWithWrongHeaders("exp_toto.fa.formated")
        self._inputFormatedFasta = "toto.fa.formated"

        iPP = PreProcess("toto.fa")
        iPP.run()

        dirStep1 = "{}_CheckFasta".format("toto.fa".split(".fa")[0])

        self.assertTrue(
            FileUtils.are2FilesIdentical("exp_toto.fa.formated", "{}/{}".format(dirStep1, "toto.fa.formated")))

    def test_PreProcess_wrongStepNumber(self):
        self._writeFastaFileWithWrongHeaders("toto.fa")
        iPP = PreProcess("toto.fa")
        iPP.setStep(29)
        self.assertRaises(Exception, iPP.run)

    def test_PreProcess_Step3_1_seqSup110nt(self):
        inFileName = self.expChunkFastaFile
        iPP = PreProcess(inFileName, outputFasta=self.outputSubsetFile, step=3, verbosity=2)
        iPP.setInfoForStep3(110)
        iPP.run()

        self.assertTrue(FileUtils.are2FilesIdentical(self.expSubsetFile,
                                                     "exp_PreProcess_withNStretch_SubSet/exp_PreProcess_withNStretch.fa_chunks.fa_subset"))

    def test_PreProcess_Step3_2_3longestSeq(self):
        inFileName = self.expChunkFastaFile
        iPP = PreProcess(inFileName, outputFasta=self.outputSubsetFile, step=3, verbosity=5)
        iPP.setsubstep3(2)
        iPP.setInfoForStep3(3)
        iPP.run()

        self.assertTrue(FileUtils.are2FilesIdentical(self.expSubsetFile,
                                                     "exp_PreProcess_withNStretch_SubSet/exp_PreProcess_withNStretch.fa_chunks.fa_subset"))

    # La step 2-3 definit une taille de subset de laquelle il faut s approcher le plus possible.
    # Pour ce 1er test le cumul est atteint en depassant le seuil defini.

    def test_PreProcess_Step3_3_minSizeSupCutOff(self):
        inFileName = self.expChunkFastaFile
        dirStep3 = "{}_SubSet".format(inFileName.split(".fa")[0])
        iPP = PreProcess(inFileName, outputFasta=self.outputSubsetFile, step=3, verbosity=5)
        iPP.setsubstep3(3)
        iPP.setInfoForStep3(600)
        iPP.run()

        self.assertTrue(FileUtils.are2FilesIdentical(self.expSubsetFile,
                                                     "exp_PreProcess_withNStretch_SubSet/exp_PreProcess_withNStretch.fa_chunks.fa_subset"))
        self.assertTrue(FileUtils.are2FilesIdentical(self.expStatStep2, "{}/{}".format(dirStep3, self.outputStatStep2)))

    # Pour ce 2eme la valeur du cumul s'approchant le plus de seuil est inferieur au seuil.

    def test_PreProcess_Step3_3_minSizeInfCutOff(self):
        inFileName = self.expChunkFastaFile
        dirStep3 = "{}_SubSet".format(inFileName.split(".fa")[0])
        iPP = PreProcess(inFileName, outputFasta=self.outputSubsetFile, step=3, verbosity=5)
        iPP.setsubstep3(3)
        iPP.setInfoForStep3(590)
        iPP.run()

        self.assertTrue(FileUtils.are2FilesIdentical(self.expSubsetFile,
                                                     "exp_PreProcess_withNStretch_SubSet/exp_PreProcess_withNStretch.fa_chunks.fa_subset"))
        self.assertTrue(FileUtils.are2FilesIdentical(self.expStatStep2, "{}/{}".format(dirStep3, self.outputStatStep2)))

    def test_PreProcess_Step3_3_SubsetSmallerThanLongestSequence(self):
        inFileName = self.expChunkFastaFile
        iPP = PreProcess(inFileName, outputFasta=self.outputSubsetFile, step=3, verbosity=5)
        iPP.setsubstep3(3)
        iPP.setInfoForStep3(270)
        iPP.run()

        self.assertTrue(FileUtils.are2FilesIdentical(self.expSubsetFile2,
                                                     "exp_PreProcess_withNStretch_SubSet/exp_PreProcess_withNStretch.fa_chunks.fa_subset"))

    def test_PreProcess_Step3_3_SubsetBiggerThanGenomeSize(self):
        inFileName = self.expChunkFastaFile
        iPP = PreProcess(inFileName, outputFasta=self.outputSubsetFile, step=3, verbosity=5)
        iPP.setsubstep3(3)
        iPP.setInfoForStep3(950)
        iPP.run()
        dirStep3 = "{}_SubSet".format(self.inputFastaFile.split(".fa")[0])

        self.assertFalse(os.path.exists("{}/{}_chunks.fa_subset".format(dirStep3, self.inputFastaFile)))

    def test_PreProcess_Step13_1(self):

        iPP = PreProcess("{}".format(self.inputFastaFile), outputFasta=self.outputFastaFile, step=13, word=3,
                         verbosity=5)
        iPP.setsubstep3(1)
        iPP.setInfoForStep3(110)
        iPP.run()
        dirStep3 = "{}_SubSet".format(self.inputFastaFile.split(".fa")[0])

        self.assertTrue(FileUtils.are2FilesIdentical(self.expSubsetFile,
                                                     "{}/{}_chunks.fa_subset".format(dirStep3, self.inputFastaFile)))
        self.assertTrue(FileUtils.are2FilesIdentical(self.expStatStep2, "{}/{}_chunks.fa_subset.stats".format(dirStep3,
                                                                                                              self.inputFastaFile)))

    def test_PreProcess_Step13_2(self):

        iPP = PreProcess("{}".format(self.inputFastaFile), outputFasta=self.outputFastaFile, step=13, word=3,
                         verbosity=5)
        iPP.setsubstep3(2)
        iPP.setInfoForStep3(3)
        iPP.run()
        dirStep3 = "{}_SubSet".format(self.inputFastaFile.split(".fa")[0])

        self.assertTrue(FileUtils.are2FilesIdentical(self.expSubsetFile,
                                                     "{}/{}_chunks.fa_subset".format(dirStep3, self.inputFastaFile)))
        self.assertTrue(FileUtils.are2FilesIdentical(self.expStatStep2, "{}/{}_chunks.fa_subset.stats".format(dirStep3,
                                                                                                              self.inputFastaFile)))

    def test_PreProcess_Step13_3(self):

        iPP = PreProcess("{}".format(self.inputFastaFile), outputFasta=self.outputFastaFile, step=13, word=3,
                         verbosity=5)
        iPP.setsubstep3(3)
        iPP.setInfoForStep3(590)
        iPP.run()
        dirStep3 = "{}_SubSet".format(self.inputFastaFile.split(".fa")[0])

        self.assertTrue(FileUtils.are2FilesIdentical(self.expSubsetFile,
                                                     "{}/{}_chunks.fa_subset".format(dirStep3, self.inputFastaFile)))
        self.assertTrue(FileUtils.are2FilesIdentical(self.expStatStep2, "{}/{}_chunks.fa_subset.stats".format(dirStep3,
                                                                                                              self.inputFastaFile)))

    def _writeFastaFileWithWrongHeaders(self, fileName):
        with open(fileName, "w") as f:
            f.write(">eee|ttt\n")
            f.write("atrdioub goiuvogbeqiubgeoi\n")
            f.write(">:::@tytyt\n")
            f.write("jhrfbhek@ljhgb\n")
            f.write("jhrfbhekljhgb\n")
            f.write(">je_suis_le_bon_header\n")
            f.write("atgctagtgatgatgctgctgctcgt\n")

    def _writeExpFormatedFastaFileWithWrongHeaders(self, fileName):
        with open(fileName, "w") as f:
            f.write(">eeettt\n")
            f.write("atNNNNNNNgNNNNNgNNNNNNgNNN\n")
            f.write(">:::tytyt\n")
            f.write("NNNNNNNNNNNNgNNNNNNNNNNNNgN\n")
            f.write(">je_suis_le_bon_header\n")
            f.write("atgctagtgatgatgctgctgctcgt\n")

    def _writeExpFormatedFastaFileWithSSR(self, fileName):
        with open(fileName, "w") as f:
            f.write(">SSRtest\n")
            f.write("GATGATCGATCGTAGCTAGCTAGCTAGCTAGCTAGCTGATCGACGTAGCTAGCTGACTGA\n")
            f.write("TCGATCGATGCTAGCTAGCTAGCTAGCTGACTGACTGATCGATCGTACGTAGCTGCTAGC\n")
            f.write("TAGCTAGCTAGCTAGCTAGCTAGCTAGCTGACTGATCGATCGTAGCTAGCTAGCTGACTG\n")
            f.write("ACTAGCTAGCTAGCTAGCTAGCTGATCGATCGTACGTAGCTAGCTGACTGATCGATC\n")
            f.write(">SSRtest1\n")
            f.write("gcgcgcgcgcgcgcgcgcgcgcgcggcagcatcgatgcatcgatcgatcgatcgatgcat\n")
            f.write("cgatcgtagctacg\n")
            f.write(">SSRtest2\n")
            f.write("atcgtcagtacgtacgtacgtagctacgacgtagctagctcNNNNNNtgtgtgtagctag\n")
            f.write("ctagctagctagctagcta\n")

    def _writeStatsFile(self, fileName):
        with open(fileName, "w") as f:
            f.write("size of the bank (nb of sequences): 6\n")
            f.write("length of the bank (cumulative sequence lengths): 965 bp\n")
            f.write("mean sequence length: 161 bp (var=12816.57, sd=113.21, cv=0.70)\n")
            f.write("n=6 min=33.000 Q1=79.000 median=144.000 Q3=292.000 max=306.000\n")
            f.write("N50=2 L50=292nt N75=3 L75=144nt N90=5 L90=79nt\n")
            f.write("GC content: 471 (48.81%)\n")
            f.write("occurrences of A: 214 (22.18%)\n")
            f.write("occurrences of T: 260 (26.94%)\n")
            f.write("occurrences of G: 264 (27.36%)\n")
            f.write("occurrences of C: 207 (21.45%)\n")
            f.write("occurrences of N: 9 (0.93%)\n")
            f.write("occurrences of other: 11 (1.14%)\n")
            f.write("MD5 secure hash of the bank: e39712d327e304f1554026119ca79b62\n")

    def _writeStatsFile2(self, fileName):
        with open(fileName, "w") as f:
            f.write("size of the bank (nb of sequences): 6\n")
            f.write("length of the bank (cumulative sequence lengths): 965 bp\n")
            f.write("mean sequence length: 161 bp (var=12816.57, sd=113.21, cv=0.70)\n")
            f.write("n=6 min=33.000 Q1=79.000 median=144.000 Q3=292.000 max=306.000\n")
            f.write("N50=2 L50=292nt N75=3 L75=144nt N90=5 L90=79nt\n")
            f.write("GC content: 471 (48.81%)\n")
            f.write("occurrences of A: 214 (22.18%)\n")
            f.write("occurrences of T: 260 (26.94%)\n")
            f.write("occurrences of G: 264 (27.36%)\n")
            f.write("occurrences of C: 207 (21.45%)\n")
            f.write("occurrences of N: 20 (2.07%)\n")
            f.write("MD5 secure hash of the bank: afc2c27d221161f9c1bcc456ef2b1d85\n")

    def _writeStatsFile3(self, fileName):
        with open(fileName, "w") as f:
            f.write("size of the bank (nb of sequences): 9\n")
            f.write("length of the bank (cumulative sequence lengths): 945 bp\n")
            f.write("mean sequence length: 105 bp (var=6935.75, sd=83.28, cv=0.79)\n")
            f.write("n=9 min=22.000 Q1=64.000 median=76.000 Q3=111.000 max=272.000\n")
            f.write("N50=2 L50=212nt N75=5 L75=76nt N90=7 L90=64nt\n")
            f.write("GC content: 471 (49.84%)\n")
            f.write("occurrences of A: 214 (22.65%)\n")
            f.write("occurrences of T: 258 (27.30%)\n")
            f.write("occurrences of G: 264 (27.94%)\n")
            f.write("occurrences of C: 207 (21.90%)\n")
            f.write("occurrences of N: 2 (0.21%)\n")
            f.write("MD5 secure hash of the bank: 9e1fe40e136ebc532a215724c1a234a7\n")

    def _writeStatsFile4(self, fileName):
        with open(fileName, "w") as f:
            f.write("size of the bank (nb of sequences): 3\n")
            f.write("length of the bank (cumulative sequence lengths): 595 bp\n")
            f.write("mean sequence length: 198 bp (var=6620.33, sd=81.37, cv=0.41)\n")
            f.write("n=3 min=111.000 Q1=111.000 median=212.000 Q3=272.000 max=272.000\n")
            f.write("N50=2 L50=212nt N75=2 L75=212nt N90=3 L90=111nt\n")
            f.write("GC content: 295 (49.58%)\n")
            f.write("occurrences of A: 134 (22.52%)\n")
            f.write("occurrences of T: 164 (27.56%)\n")
            f.write("occurrences of G: 167 (28.07%)\n")
            f.write("occurrences of C: 128 (21.51%)\n")
            f.write("occurrences of N: 2 (0.34%)\n")
            f.write("MD5 secure hash of the bank: a14da66e39e2739fdecf18bba689490b\n")


if __name__ == "__main__":
    unittest.main()
