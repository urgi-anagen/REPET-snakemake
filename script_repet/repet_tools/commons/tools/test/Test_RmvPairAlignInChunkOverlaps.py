import os
import time
import random
import shutil
import unittest
import subprocess
from commons.core.coord.Align import Align
from commons.core.utils.FileUtils import FileUtils
from commons.tools.RmvPairAlignInChunkOverlaps import RmvPairAlignInChunkOverlaps


class Test_RmvPairAlignInChunkOverlaps( unittest.TestCase ):
    
    def setUp( self ):
        self._iRmvPairAlignInChunkOverlaps = RmvPairAlignInChunkOverlaps()
        self._curTestDir = os.getcwd()
        self._testDir = "test_F_RPAICO_{}{}".format(time.strftime("%H%M%S"),random.randint(0, 1000))
        os.mkdir(self._testDir)
        os.chdir(self._testDir)


    def tearDown( self ):
        self._iRmvPairAlignInChunkOverlaps = None
        os.chdir(self._curTestDir)
        try:
            shutil.rmtree(self._testDir)
        except:
            pass
        

    def test_isPairAlignAChunkOverlap( self ):
        a = Align()
        a.setFromTuple( ( "chunk1", "401", "500", "chunk2", "1", "100", "0.0", "125", "97.6" ) )
        self._iRmvPairAlignInChunkOverlaps.setChunkLength( 500 )
        self._iRmvPairAlignInChunkOverlaps.setChunkOverlap( 100 )
        obs = self._iRmvPairAlignInChunkOverlaps.isPairAlignAChunkOverlap( a, 1, 2 )
        self.assertTrue( obs )
        a.setFromTuple( ( "chunk1", "401", "500", "chunk2", "1", "100", "0.0", "125", "97.6" ) )  # reverse subject
        obs = self._iRmvPairAlignInChunkOverlaps.isPairAlignAChunkOverlap( a, 1, 2 )
        self.assertTrue( obs )
        a.setFromTuple( ( "chunk1", "401", "500", "chunk3", "1", "100", "0.0", "125", "97.6" ) )   # chunk subject not contiguous
        obs = self._iRmvPairAlignInChunkOverlaps.isPairAlignAChunkOverlap( a, 1, 3 )
        self.assertFalse( obs )
        a.setFromTuple( ( "chunk1", "390", "500", "chunk2", "1", "110", "0.0", "125", "97.6" ) )   # hit longer than chunk overlap
        obs = self._iRmvPairAlignInChunkOverlaps.isPairAlignAChunkOverlap( a, 1, 2 )
        self.assertFalse( obs )
        a.setFromTuple( ( "chunk2", "1", "101", "chunk1", "401", "500", "0.0", "500", "100.0" ) )   # chunk overlap returned by PALS (+1), query > subject
        obs = self._iRmvPairAlignInChunkOverlaps.isPairAlignAChunkOverlap( a, 2, 1 )
        self.assertTrue( obs )
        a.setFromTuple( ( "chunk1", "401", "500", "chunk2", "1", "101", "0.0", "500", "100.0" ) )   # chunk overlap returned by PALS (+1), query < subject
        obs = self._iRmvPairAlignInChunkOverlaps.isPairAlignAChunkOverlap( a, 1, 2 )
        self.assertTrue( obs )
        
        
    def test_isPairAlignWithinAndDueToAChunkOverlap( self ):
        a = Align()
        a.setFromTuple( ( "chunk1", "411", "450", "chunk2", "11", "50", "0.0", "73", "97.6" ) )  # included, due to overlap
        self._iRmvPairAlignInChunkOverlaps.setChunkLength( 500 )
        self._iRmvPairAlignInChunkOverlaps.setChunkOverlap( 100 )
        self._iRmvPairAlignInChunkOverlaps._verbose = 0
        obs = self._iRmvPairAlignInChunkOverlaps.isPairAlignWithinAndDueToAChunkOverlap( a, 1, 2 )
        self.assertTrue( obs )
        a.setFromTuple( ( "chunk1", "411", "450", "chunk2", "50", "11", "0.0", "73", "97.6" ) )  # reverse subject
        obs = self._iRmvPairAlignInChunkOverlaps.isPairAlignWithinAndDueToAChunkOverlap( a, 1, 2 )
        self.assertFalse( obs )
        a.setFromTuple( ( "chunk1", "401", "500", "chunk3", "1", "100", "0.0", "125", "97.6" ) )   # chunk subject not contiguous
        obs = self._iRmvPairAlignInChunkOverlaps.isPairAlignWithinAndDueToAChunkOverlap( a, 1, 3 )
        self.assertFalse( obs )
        a.setFromTuple( ( "chunk1", "390", "500", "chunk2", "1", "110", "0.0", "125", "97.6" ) )   # hit longer than chunk overlap
        obs = self._iRmvPairAlignInChunkOverlaps.isPairAlignWithinAndDueToAChunkOverlap( a, 1, 2 )
        self.assertFalse( obs )
        a.setFromTuple( ( "chunk1", "411", "430", "chunk2", "16", "35", "0.0", "73", "97.6" ) )   # repeat within overlap...
        self._iRmvPairAlignInChunkOverlaps._margin = 2   # ... but not due to it
        obs = self._iRmvPairAlignInChunkOverlaps.isPairAlignWithinAndDueToAChunkOverlap( a, 1, 2 )
        self.assertFalse( obs )
        self._iRmvPairAlignInChunkOverlaps._margin = 10   # ... and due to it
        obs = self._iRmvPairAlignInChunkOverlaps.isPairAlignWithinAndDueToAChunkOverlap( a, 1, 2 )
        self.assertTrue( obs )
        
        
    def test_removeChunkOverlaps( self ):
        inFileName = "dummyInFile"
        inF = open( inFileName, "w" )
        a = Align()
        a.setFromTuple( ( "chunk1", "411", "490", "chunk2", "11", "90", "0.0", "73", "97.6" ) )
        a.write( inF )
        a.setFromTuple( ( "chunk1", "1", "500", "chunk1", "1", "500", "0.0", "500", "100.0" ) )
        a.write( inF )
        inF.close()
        self._iRmvPairAlignInChunkOverlaps.setInputFileName( inFileName )
        self._iRmvPairAlignInChunkOverlaps.setChunkLength( 500 )
        self._iRmvPairAlignInChunkOverlaps.setChunkOverlap( 100 )
        obsFileName = "dummyObsFile"
        self._iRmvPairAlignInChunkOverlaps.setOutputFileName( obsFileName )
        d = self._iRmvPairAlignInChunkOverlaps.removeChunkOverlaps()
        expFileName = "dummyExpFile"
        expF = open( expFileName, "w" )
        expF.write( "" )
        expF.close()
        self.assertTrue( FileUtils.are2FilesIdentical( obsFileName, expFileName ) )
        for f in [ inFileName, obsFileName, expFileName ]: os.remove( f )
        
        
    def test_zRunAsScript( self ):
        alignFileName = "dummyInFile"
        with open( alignFileName, "w" ) as aF:
            aF.write( "chunk1\t401\t500\tchunk2\t1\t100\t0.0\t131\t100.0\n" )  # remove
            aF.write( "chunk2\t1\t100\tchunk1\t401\t500\t0.0\t132\t100.0\n" )  # remove
            aF.write( "chunk1\t401\t500\tchunk3\t1\t100\t0.0\t132\t100.0\n" )  # keep because non-contiguous chunks
            aF.write( "chunk3\t401\t500\tchunk1\t1\t100\t0.0\t132\t100.0\n" )  # keep because non-contiguous chunks
            aF.write( "chunk1\t401\t500\tchunk2\t100\t1\t0.0\t132\t100.0\n" )  # keep because within overlap but reverse
            aF.write( "chunk1\t431\t490\tchunk2\t31\t90\t0.0\t132\t100.0\n" )  # remove because within and due to overlap
            aF.write( "chunk1\t411\t430\tchunk2\t61\t90\t0.0\t132\t100.0\n" )  # keep because within but not due to overlap
            aF.write( "chunk1\t390\t500\tchunk2\t1\t100\t0.0\t132\t100.0\n" )  # keep because longer HSP on query

        #DOES not work , or does it, maybe ??
        #aF.write( "chunk1\t1\t250\tchunk3\t450\t500\t0.0\t132\t100.0\n" )  
        #aF.write( "chunk1\t1\t250\tchunk4\t1\t250\t0.0\t132\t100.0\n" )  
#        aF.write( "chunk1\t1\t250\tchunk2\t350\t500\t0.0\t132\t100.0\n" )  
#        aF.write( "chunk1\t1\t250\tchunk3\t1\t250\t0.0\t132\t100.0\n" )  

        expFileName = "dummyExpFile"
        with open( expFileName, "w" ) as expF:
            expF.write( "chunk1\t401\t500\tchunk3\t1\t100\t0\t132\t100.0\n" )
            expF.write( "chunk3\t401\t500\tchunk1\t1\t100\t0\t132\t100.0\n" )
            expF.write( "chunk1\t401\t500\tchunk2\t100\t1\t0\t132\t100.0\n" )
            expF.write( "chunk1\t411\t430\tchunk2\t61\t90\t0\t132\t100.0\n" )
            expF.write( "chunk1\t390\t500\tchunk2\t1\t100\t0\t132\t100.0\n" )

        obsFileName = "dummyObsFile"
        cmd = "RmvPairAlignInChunkOverlaps.py"
        cmd += " -i %s" % ( alignFileName )
        cmd += " -l 500"
        cmd += " -o 100"
        cmd += " -O %s" % ( obsFileName )
        cmd += " -v 0"
        subprocess.call( cmd , shell=True)
        
        self.assertTrue( FileUtils.are2FilesIdentical( obsFileName, expFileName ) )

    def test_zRun_bug_to_fix(self):
        alignFileName = "dummyInFile"
        with open( alignFileName, "w" ) as aF:
            aF.write("chunk4\t63217\t63680\tchunk5\t195316\t194837\t0\t676\t92.71\n")
            aF.write("chunk4\t63217\t63680\tchunk6\t2618\t3101\t0\t714\t93.6\n")
            aF.write("chunk4\t63217\t63680\tchunk6\t5316\t4837\t0\t676\t92.71\n")
            aF.write("chunk5\t195316\t194837\tchunk6\t5316\t4837\t0\t676\t92.71\n")

        expFileName = "dummyExpFile"
        with open( expFileName, "w" ) as expF:
            expF.write("chunk4\t63217\t63680\tchunk5\t195316\t194837\t0\t676\t92.71\n")
            expF.write("chunk4\t63217\t63680\tchunk6\t2618\t3101\t0\t714\t93.6\n")

        obsFileName = "dummyObsFile"
        
        self._iRmvPairAlignInChunkOverlaps = RmvPairAlignInChunkOverlaps(inFileName=alignFileName, chunkLength=200000, chunkOverlap=10000, margin=10, outFileName=obsFileName, verbose=3)
        self._iRmvPairAlignInChunkOverlaps.run()
        
        self.assertTrue( FileUtils.are2FilesIdentical( obsFileName, expFileName ) )

if __name__ == "__main__":
        unittest.main()