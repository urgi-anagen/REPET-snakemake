import os
import time
import random
import unittest

from shutil import rmtree
from commons.core.utils.FileUtils import FileUtils
from commons.tools.MergeMatchsFiles import MergeMatchsFiles


class Test_F_MergeMatchsFiles(unittest.TestCase):

    def setUp(self):
        self._curTestDir = os.getcwd()
        self._testPrefix = 'test_F_MMF_{}_{}'.format(time.strftime("%H%M%S"), random.randint(0, 1000))
        self.singularity_img = "../../../../../../Singularity/te_finder_2.30.2.sif"
        try:
            os.makedirs(self._testPrefix)
        except:pass
        os.chdir(self._testPrefix)

    def tearDown(self):
        os.chdir(self._curTestDir)
        try:
            rmtree(self._testPrefix)
        except:pass

    def test_run_set(self):
        inFileName1 = "test1.set"
        inFileName2 = "test2.set"
        self._writeSetFiles(inFileName1, inFileName2)
        iMMF = MergeMatchsFiles("set", "out",singularity_img = self.singularity_img)
        iMMF.run()
        obsFileName = "out.set"
        expFileName = "exp.set"
        self._writeExpSetFile(expFileName)
        self.assertTrue(FileUtils.are2FilesIdentical(expFileName, obsFileName))


    def test_run_align(self):
        inFileName1 = "test1.align"
        inFileName2 = "test2.align"
        self._writeAlignFiles(inFileName1, inFileName2)
        iMMF = MergeMatchsFiles(fileType="align", outFileBaseName="out",singularity_img = self.singularity_img,allByAll=False)
        iMMF.run()
        obsFileName = "out.align"
        expFileName = "exp.align"
        self._writeExpAlignFile(expFileName)
        self.assertTrue(FileUtils.are2FilesIdentical(expFileName, obsFileName))

    def test_run_align_allByAll(self):
        inFileName1 = "test1.align"
        inFileName2 = "test2.align"
        self._writeAlignFiles(inFileName1, inFileName2)
        
        iMMF = MergeMatchsFiles(fileType="align",  outFileBaseName="out", allByAll=True)
        iMMF.run()
        obsFileName = "out.align"
        expFileName = "exp.align"
        self._writeExpAlignFile_allByAll(expFileName)
        self.assertTrue(FileUtils.are2FilesIdentical(expFileName, obsFileName))


    def test_run_path(self):
        inFileName1 = "test1.path"
        inFileName2 = "test2.path"
        self._writePathFiles(inFileName1, inFileName2)
        iMMF = MergeMatchsFiles("path", "out", singularity_img = self.singularity_img)
        iMMF.run() 
        obsFileName = "out.path"
        expFileName = "exp.path"
        self._writeExpPathFile(expFileName)
        self.assertTrue(FileUtils.are2FilesIdentical(expFileName, obsFileName))

# ca ne marche pas avec les tab
    # def test_run_tab(self):
    #     inFileName1 = "test1.tab"
    #     inFileName2 = "test2.tab"
    #     self._writeTabFiles(inFileName1, inFileName2)
    #     iMMF = MergeMatchsFiles("tab", "out")
    #     iMMF.run()
    #     obsFileName = "out.tab"
    #     expFileName = "exp.tab"
    #     self._writeExpTabFile(expFileName)
    #     self.assertTrue(FileUtils.are2FilesIdentical(expFileName, obsFileName))
    #     os.remove(expFileName)
    #     os.remove(obsFileName)
        
    def _writeSetFiles(self, fileName1, fileName2):
        with open(fileName1, "w") as f:
            f.write("1\t(TCTAT)3\tchunk006\t295\t309\n")
            f.write("2\t(A)33\tchunk006\t679\t711\n")
            f.write("3\t(G)16\tchunk006\t731\t746\n")
            f.write("4\t(GAG)9\tchunk006\t903\t929\n")
            f.write("5\t(GGAGGG)4\tchunk006\t905\t929\n")

        with open(fileName2, "w") as f:
            f.write("1\t(CCACT)3\tchunk011\t101\t116\n")
            f.write("2\t(TATATA)7\tchunk011\t316\t357\n")
            f.write("3\t(AT)22\tchunk011\t323\t366\n")

        
    def _writeExpSetFile(self, fileName):
        with open(fileName, "w") as f:
            f.write("1\t(TCTAT)3\tchunk006\t295\t309\n")
            f.write("2\t(A)33\tchunk006\t679\t711\n")
            f.write("3\t(G)16\tchunk006\t731\t746\n")
            f.write("4\t(GAG)9\tchunk006\t903\t929\n")
            f.write("5\t(GGAGGG)4\tchunk006\t905\t929\n")
            f.write("6\t(CCACT)3\tchunk011\t101\t116\n")
            f.write("7\t(TATATA)7\tchunk011\t316\t357\n")
            f.write("8\t(AT)22\tchunk011\t323\t366\n")


    def _writeAlignFiles(self, fileName1, fileName2):
        with open(fileName1, "w") as f:
            f.write("chunk1\t25601\t27800\tchunk1\t52250\t54450\t0\t4244\t99.36\n")
            f.write("chunk1\t27791\t28620\tchunk1\t55918\t56749\t0\t1588\t99.16\n")
            f.write("chunk1\t28618\t32380\tchunk1\t56794\t60572\t0\t7299\t99.29\n")
            f.write("chunk1\t28692\t30872\tchunk1\t49901\t52081\t0\t4260\t99.63\n")
            f.write("chunk1\t32216\t34275\tchunk1\t52084\t54143\t0\t4012\t99.56\n")
            f.write("chunk1\t41016\t45652\tchunk1\t55918\t60572\t0\t8942\t99.18\n")
            f.write("chunk1\t41964\t44144\tchunk1\t49901\t52081\t0\t4252\t99.59\n")
            f.write("chunk1\t46237\t46863\tchunk1\t73980\t74625\t0\t837\t91.36\n")
            f.write("chunk1\t46330\t46860\tchunk1\t54981\t54450\t0\t841\t95.11\n")
            f.write("chunk1\t46861\t48962\tchunk1\t52349\t54450\t0\t4103\t99.62\n")
            f.write("chunk1\t48953\t50000\tchunk1\t55918\t56967\t0\t2036\t99.52\n")
            f.write("chunk1\t49901\t52081\tchunk1\t28692\t30872\t0\t4260\t99.63\n")
            f.write("chunk1\t49901\t52081\tchunk1\t41964\t44144\t0\t4252\t99.59\n")
            f.write("chunk1\t52084\t54143\tchunk1\t32216\t34275\t0\t4012\t99.56\n")
            f.write("chunk1\t52250\t54450\tchunk1\t25601\t27800\t0\t4244\t99.36\n")
            f.write("chunk1\t52349\t54450\tchunk1\t46861\t48962\t0\t4103\t99.62\n")
            f.write("chunk1\t54450\t54980\tchunk1\t74622\t74091\t0\t769\t93.63\n")
            f.write("chunk1\t54450\t54981\tchunk1\t46860\t46330\t0\t841\t95.11\n")
            f.write("chunk1\t55918\t56749\tchunk1\t27791\t28620\t0\t1588\t99.16\n")
            f.write("chunk1\t55918\t56967\tchunk1\t48953\t50000\t0\t2036\t99.52\n")
            f.write("chunk1\t55918\t60572\tchunk1\t41016\t45652\t0\t8942\t99.18\n")
            f.write("chunk1\t56794\t60572\tchunk1\t28618\t32380\t0\t7299\t99.29\n")
            f.write("chunk1\t73980\t74625\tchunk1\t46237\t46863\t0\t837\t91.36\n")
            f.write("chunk1\t74091\t74622\tchunk1\t54980\t54450\t0\t769\t93.63\n")
            f.write("chunk1\t99211\t99791\tchunk1\t164000\t164577\t0\t767\t91.74\n")
            f.write("chunk1\t101527\t102429\tchunk1\t167161\t168059\t0\t1074\t90.14\n")
            f.write("chunk1\t164000\t164577\tchunk1\t99211\t99791\t0\t767\t91.74\n")
            f.write("chunk1\t167161\t168059\tchunk1\t101527\t102429\t0\t1074\t90.14\n")
            f.write("chunk1\t46330\t46862\tchunk6\t2564\t3101\t0\t835\t94.8\n")
            f.write("chunk1\t46372\t46862\tchunk6\t5328\t4837\t0\t753\t94.51\n")
            f.write("chunk1\t54450\t54915\tchunk6\t4839\t5304\t0\t749\t95.28\n")
            f.write("chunk1\t54450\t55021\tchunk6\t3099\t2524\t0\t878\t94.27\n")
            f.write("chunk1\t54600\t55021\tchunk6\t25505\t25926\t0\t638\t94.08\n")
            f.write("chunk1\t74091\t74624\tchunk6\t2565\t3101\t0\t898\t96.48\n")
            f.write("chunk1\t74132\t74624\tchunk6\t5328\t4837\t0\t763\t94.95\n")

        with open(fileName2, "w") as f:
            f.write("chunk6\t2524\t3099\tchunk1\t55021\t54450\t0\t878\t94.27\n")
            f.write("chunk6\t2564\t3101\tchunk1\t46330\t46862\t0\t835\t94.8\n")
            f.write("chunk6\t2565\t3101\tchunk1\t74091\t74624\t0\t898\t96.48\n")
            f.write("chunk6\t4837\t5328\tchunk1\t46862\t46372\t0\t753\t94.51\n")
            f.write("chunk6\t4837\t5328\tchunk1\t74624\t74132\t0\t763\t94.95\n")
            f.write("chunk6\t4839\t5304\tchunk1\t54450\t54915\t0\t749\t95.28\n")
            f.write("chunk6\t25505\t25926\tchunk1\t54600\t55021\t0\t638\t94.08\n")
            f.write("chunk6\t1998\t2948\tchunk6\t26450\t25506\t0\t1758\t98.42\n")
            f.write("chunk6\t2606\t3101\tchunk6\t5328\t4837\t0\t815\t95.77\n")
            f.write("chunk6\t4837\t5328\tchunk6\t3101\t2606\t0\t815\t95.77\n")
            f.write("chunk6\t25506\t26450\tchunk6\t2948\t1998\t0\t1758\t98.42\n")
            f.write("chunk6\t95526\t96047\tchunk6\t115678\t116199\t0\t688\t92.19\n")
            f.write("chunk6\t115678\t116199\tchunk6\t95526\t96047\t0\t688\t92.19\n")


    def _writeExpAlignFile_allByAll(self, fileName):
        with open(fileName, "w") as f:
            f.write("chunk1\t25601\t27800\tchunk1\t52250\t54450\t0\t4244\t99.36\n")
            f.write("chunk1\t27791\t28620\tchunk1\t55918\t56749\t0\t1588\t99.16\n")
            f.write("chunk1\t28618\t32380\tchunk1\t56794\t60572\t0\t7299\t99.29\n")
            f.write("chunk1\t28692\t30872\tchunk1\t49901\t52081\t0\t4260\t99.63\n")
            f.write("chunk1\t32216\t34275\tchunk1\t52084\t54143\t0\t4012\t99.56\n")
            f.write("chunk1\t41016\t45652\tchunk1\t55918\t60572\t0\t8942\t99.18\n")
            f.write("chunk1\t41964\t44144\tchunk1\t49901\t52081\t0\t4252\t99.59\n")
            f.write("chunk1\t46237\t46863\tchunk1\t73980\t74625\t0\t837\t91.36\n")
            f.write("chunk1\t46330\t46860\tchunk1\t54981\t54450\t0\t841\t95.11\n")
            f.write("chunk1\t46861\t48962\tchunk1\t52349\t54450\t0\t4103\t99.62\n")
            f.write("chunk1\t48953\t50000\tchunk1\t55918\t56967\t0\t2036\t99.52\n")
            f.write("chunk1\t54450\t54980\tchunk1\t74622\t74091\t0\t769\t93.63\n")
            f.write("chunk1\t99211\t99791\tchunk1\t164000\t164577\t0\t767\t91.74\n")
            f.write("chunk1\t101527\t102429\tchunk1\t167161\t168059\t0\t1074\t90.14\n")
            f.write("chunk1\t46330\t46862\tchunk6\t2564\t3101\t0\t835\t94.8\n")
            f.write("chunk1\t46372\t46862\tchunk6\t5328\t4837\t0\t753\t94.51\n")
            f.write("chunk1\t54450\t54915\tchunk6\t4839\t5304\t0\t749\t95.28\n")
            f.write("chunk1\t54450\t55021\tchunk6\t3099\t2524\t0\t878\t94.27\n")
            f.write("chunk1\t54600\t55021\tchunk6\t25505\t25926\t0\t638\t94.08\n")
            f.write("chunk1\t74091\t74624\tchunk6\t2565\t3101\t0\t898\t96.48\n")
            f.write("chunk1\t74132\t74624\tchunk6\t5328\t4837\t0\t763\t94.95\n")
            f.write("chunk6\t1998\t2948\tchunk6\t26450\t25506\t0\t1758\t98.42\n")
            f.write("chunk6\t2606\t3101\tchunk6\t5328\t4837\t0\t815\t95.77\n")
            f.write("chunk6\t95526\t96047\tchunk6\t115678\t116199\t0\t688\t92.19\n")

        
    def _writeExpAlignFile(self, fileName):
        with open(fileName, "w") as f:
            f.write("chunk1\t25601\t27800\tchunk1\t52250\t54450\t0\t4244\t99.36\n")
            f.write("chunk1\t27791\t28620\tchunk1\t55918\t56749\t0\t1588\t99.16\n")
            f.write("chunk1\t28618\t32380\tchunk1\t56794\t60572\t0\t7299\t99.29\n")
            f.write("chunk1\t28692\t30872\tchunk1\t49901\t52081\t0\t4260\t99.63\n")
            f.write("chunk1\t32216\t34275\tchunk1\t52084\t54143\t0\t4012\t99.56\n")
            f.write("chunk1\t41016\t45652\tchunk1\t55918\t60572\t0\t8942\t99.18\n")
            f.write("chunk1\t41964\t44144\tchunk1\t49901\t52081\t0\t4252\t99.59\n")
            f.write("chunk1\t46237\t46863\tchunk1\t73980\t74625\t0\t837\t91.36\n")
            f.write("chunk1\t46330\t46860\tchunk1\t54981\t54450\t0\t841\t95.11\n")
            f.write("chunk1\t46861\t48962\tchunk1\t52349\t54450\t0\t4103\t99.62\n")
            f.write("chunk1\t48953\t50000\tchunk1\t55918\t56967\t0\t2036\t99.52\n")
            f.write("chunk1\t49901\t52081\tchunk1\t28692\t30872\t0\t4260\t99.63\n")
            f.write("chunk1\t49901\t52081\tchunk1\t41964\t44144\t0\t4252\t99.59\n")
            f.write("chunk1\t52084\t54143\tchunk1\t32216\t34275\t0\t4012\t99.56\n")
            f.write("chunk1\t52250\t54450\tchunk1\t25601\t27800\t0\t4244\t99.36\n")
            f.write("chunk1\t52349\t54450\tchunk1\t46861\t48962\t0\t4103\t99.62\n")
            f.write("chunk1\t54450\t54980\tchunk1\t74622\t74091\t0\t769\t93.63\n")
            f.write("chunk1\t54450\t54981\tchunk1\t46860\t46330\t0\t841\t95.11\n")
            f.write("chunk1\t55918\t56749\tchunk1\t27791\t28620\t0\t1588\t99.16\n")
            f.write("chunk1\t55918\t56967\tchunk1\t48953\t50000\t0\t2036\t99.52\n")
            f.write("chunk1\t55918\t60572\tchunk1\t41016\t45652\t0\t8942\t99.18\n")
            f.write("chunk1\t56794\t60572\tchunk1\t28618\t32380\t0\t7299\t99.29\n")
            f.write("chunk1\t73980\t74625\tchunk1\t46237\t46863\t0\t837\t91.36\n")
            f.write("chunk1\t74091\t74622\tchunk1\t54980\t54450\t0\t769\t93.63\n")
            f.write("chunk1\t99211\t99791\tchunk1\t164000\t164577\t0\t767\t91.74\n")
            f.write("chunk1\t101527\t102429\tchunk1\t167161\t168059\t0\t1074\t90.14\n")
            f.write("chunk1\t164000\t164577\tchunk1\t99211\t99791\t0\t767\t91.74\n")
            f.write("chunk1\t167161\t168059\tchunk1\t101527\t102429\t0\t1074\t90.14\n")
            f.write("chunk1\t46330\t46862\tchunk6\t2564\t3101\t0\t835\t94.8\n")
            f.write("chunk1\t46372\t46862\tchunk6\t5328\t4837\t0\t753\t94.51\n")
            f.write("chunk1\t54450\t54915\tchunk6\t4839\t5304\t0\t749\t95.28\n")
            f.write("chunk1\t54450\t55021\tchunk6\t3099\t2524\t0\t878\t94.27\n")
            f.write("chunk1\t54600\t55021\tchunk6\t25505\t25926\t0\t638\t94.08\n")
            f.write("chunk1\t74091\t74624\tchunk6\t2565\t3101\t0\t898\t96.48\n")
            f.write("chunk1\t74132\t74624\tchunk6\t5328\t4837\t0\t763\t94.95\n")
            f.write("chunk6\t2524\t3099\tchunk1\t55021\t54450\t0\t878\t94.27\n")
            f.write("chunk6\t2564\t3101\tchunk1\t46330\t46862\t0\t835\t94.8\n")
            f.write("chunk6\t2565\t3101\tchunk1\t74091\t74624\t0\t898\t96.48\n")
            f.write("chunk6\t4837\t5328\tchunk1\t46862\t46372\t0\t753\t94.51\n")
            f.write("chunk6\t4837\t5328\tchunk1\t74624\t74132\t0\t763\t94.95\n")
            f.write("chunk6\t4839\t5304\tchunk1\t54450\t54915\t0\t749\t95.28\n")
            f.write("chunk6\t25505\t25926\tchunk1\t54600\t55021\t0\t638\t94.08\n")
            f.write("chunk6\t1998\t2948\tchunk6\t26450\t25506\t0\t1758\t98.42\n")
            f.write("chunk6\t2606\t3101\tchunk6\t5328\t4837\t0\t815\t95.77\n")
            f.write("chunk6\t4837\t5328\tchunk6\t3101\t2606\t0\t815\t95.77\n")
            f.write("chunk6\t25506\t26450\tchunk6\t2948\t1998\t0\t1758\t98.42\n")
            f.write("chunk6\t95526\t96047\tchunk6\t115678\t116199\t0\t688\t92.19\n")
            f.write("chunk6\t115678\t116199\tchunk6\t95526\t96047\t0\t688\t92.19\n")


    def _writePathFiles(self, fileName1, fileName2):
        with open(fileName1, "w") as f:
            f.write("1\tchunk001\t11046\t11071\tAT_rich#Low_complexity\t166\t141\t0.0\t9\t34.62\n")
            f.write("2\tchunk001\t11050\t11074\tAT_rich#Low_complexity\t161\t137\t0.0\t14\t56.0\n")
            f.write("3\tchunk001\t17216\t17266\tAT_rich#Low_complexity\t144\t194\t0.0\t14\t29.41\n")
            f.write("4\tchunk001\t17736\t17766\tAT_rich#Low_complexity\t118\t148\t0.0\t6\t22.58\n")
            f.write("5\tchunk001\t18723\t18758\tAT_rich#Low_complexity\t110\t145\t0.0\t5\t13.89\n")
            f.write("6\tchunk001\t20024\t20073\tAT_rich#Low_complexity\t125\t174\t0.0\t13\t26.0\n")
            f.write("7\tchunk002\t12855\t12882\tAT_rich#Low_complexity\t131\t158\t0.0\t13\t46.43\n")
            f.write("8\tchunk002\t20736\t20771\tAT_rich#Low_complexity\t135\t170\t0.0\t12\t36.11\n")

        with open(fileName2, "w") as f:
            f.write("1\tchunk006\t12114\t12143\tAT_rich#Low_complexity\t146\t117\t0.0\t6\t20.0\n")
            f.write("2\tchunk006\t25538\t25574\tAT_rich#Low_complexity\t128\t164\t0.0\t14\t37.84\n")
            f.write("3\tchunk006\t25794\t25818\tAT_rich#Low_complexity\t162\t138\t0.0\t15\t60.0\n")
            f.write("4\tchunk006\t28373\t28403\tAT_rich#Low_complexity\t135\t165\t0.0\t17\t54.84\n")
            f.write("5\tchunk006\t29180\t29201\tAT_rich#Low_complexity\t163\t142\t0.0\t14\t68.18\n")
            f.write("6\tchunk006\t33773\t33825\tAT_rich#Low_complexity\t142\t194\t0.0\t11\t22.64\n")

        
    def _writeExpPathFile(self, fileName):
        with open(fileName, "w") as f:
            f.write("1\tchunk001\t11046\t11071\tAT_rich#Low_complexity\t166\t141\t0.0\t9\t34.62\n")
            f.write("2\tchunk001\t11050\t11074\tAT_rich#Low_complexity\t161\t137\t0.0\t14\t56.0\n")
            f.write("3\tchunk001\t17216\t17266\tAT_rich#Low_complexity\t144\t194\t0.0\t14\t29.41\n")
            f.write("4\tchunk001\t17736\t17766\tAT_rich#Low_complexity\t118\t148\t0.0\t6\t22.58\n")
            f.write("5\tchunk001\t18723\t18758\tAT_rich#Low_complexity\t110\t145\t0.0\t5\t13.89\n")
            f.write("6\tchunk001\t20024\t20073\tAT_rich#Low_complexity\t125\t174\t0.0\t13\t26.0\n")
            f.write("7\tchunk002\t12855\t12882\tAT_rich#Low_complexity\t131\t158\t0.0\t13\t46.43\n")
            f.write("8\tchunk002\t20736\t20771\tAT_rich#Low_complexity\t135\t170\t0.0\t12\t36.11\n")
            f.write("9\tchunk006\t12114\t12143\tAT_rich#Low_complexity\t146\t117\t0.0\t6\t20.0\n")
            f.write("10\tchunk006\t25538\t25574\tAT_rich#Low_complexity\t128\t164\t0.0\t14\t37.84\n")
            f.write("11\tchunk006\t25794\t25818\tAT_rich#Low_complexity\t162\t138\t0.0\t15\t60.0\n")
            f.write("12\tchunk006\t28373\t28403\tAT_rich#Low_complexity\t135\t165\t0.0\t17\t54.84\n")
            f.write("13\tchunk006\t29180\t29201\tAT_rich#Low_complexity\t163\t142\t0.0\t14\t68.18\n")
            f.write("14\tchunk006\t33773\t33825\tAT_rich#Low_complexity\t142\t194\t0.0\t11\t22.64\n")

        
    def _writeTabFiles(self, fileName1, fileName2):
        with open(fileName1, "w") as f:
            f.write("query.name\tquery.start\tquery.end\tquery.length\tquery.length.%\tmatch.length.%\tsubject.name\tsubject.start\tsubject.end\tsubject.length\tsubject.length.%\tE.value\tScore\tIdentity\tpath\n")
            f.write("DmelChr4_bench_Blaster_Grouper_1_Map_3\t2\t542\t541\t0.998155\t0.32473\tTC1_DM:ClassII:TIR:Tc1-Mariner\t1\t543\t543\t0.32593\t1.4e-93\t984\t87.541\t1\n")
            f.write("DmelChr4_bench_Blaster_Grouper_2_Map_8\t2\t517\t516\t0.998066\t0.115179\tPROTOP:ClassII:TIR:P\t530\t16\t515\t0.114955\t1.2e-79\t928\t95.2118\t2\n")
            f.write("DmelChr4_bench_Blaster_Grouper_3_Map_20\t5\t598\t594\t0.978583\t0.537557\tPROTOP_A:ClassII:TIR:P\t572\t1\t572\t0.517647\t3e-93\t1048\t97.6307\t3\n")
            f.write("DmelChr4_bench_Blaster_Grouper_4_Map_3\t5\t534\t530\t0.992509\t0.479638\tPROTOP_A:ClassII:TIR:P\t1105\t576\t530\t0.479638\t2.9e-87\t1828\t98.4848\t4\n")
            f.write("DmelChr4_bench_Blaster_Grouper_5_Map_3\t5\t704\t700\t0.994318\t0.425791\tTC1-2_DM:ClassII:TIR:Tc1-Mariner\t1644\t945\t700\t0.425791\t7.3e-120\t1228\t98.1349\t5\n")

        with open(fileName2, "w") as f:
            f.write("query.name\tquery.start\tquery.end\tquery.length\tquery.length.%\tmatch.length.%\tsubject.name\tsubject.start\tsubject.end\tsubject.length\tsubject.length.%\tE.value\tScore\tIdentity\tpath\n")
            f.write("DmelChr4_bench_Blaster_Grouper_5_Map_3\t274\t702\t429\t0.609375\t1.26176\tTC1-2_DMp:ClassII:TIR:Tc1-Mariner\t340\t198\t143\t0.420588\t5e-74\t265\t98.6014\t1\n")
            f.write("DmelChr4_bench_Blaster_Recon_12_Map_3\t311\t374\t64\t0.028021\t0.0359349\tBEL-27_AA-I_1p:ClassI:LTR:Bel-Pao\t605\t582\t24\t0.0134756\t6.3e-17\t11\t30.8511\t2\n")
            f.write("DmelChr4_bench_Blaster_Recon_12_Map_3\t1472\t1552\t81\t0.0354641\t0.0463918\tBEL-76_AA-I_1p:ClassI:LTR:Bel-Pao\t79\t52\t28\t0.0160367\t1e-13\t8\t27.7778\t3\n")

        
    def _writeExpTabFile(self, fileName):
        with open(fileName, "w") as f:
            f.write("query.name\tquery.start\tquery.end\tquery.length\tquery.length.%\tmatch.length.%\tsubject.name\tsubject.start\tsubject.end\tsubject.length\tsubject.length.%\tE.value\tScore\tIdentity\tpath\n")
            f.write("DmelChr4_bench_Blaster_Grouper_1_Map_3\t2\t542\t541\t0.998155\t0.32473\tTC1_DM:ClassII:TIR:Tc1-Mariner\t1\t543\t543\t0.32593\t1.4e-93\t984\t87.541\t1\n")
            f.write("DmelChr4_bench_Blaster_Grouper_2_Map_8\t2\t517\t516\t0.998066\t0.115179\tPROTOP:ClassII:TIR:P\t530\t16\t515\t0.114955\t1.2e-79\t928\t95.2118\t2\n")
            f.write("DmelChr4_bench_Blaster_Grouper_3_Map_20\t5\t598\t594\t0.978583\t0.537557\tPROTOP_A:ClassII:TIR:P\t572\t1\t572\t0.517647\t3e-93\t1048\t97.6307\t3\n")
            f.write("DmelChr4_bench_Blaster_Grouper_4_Map_3\t5\t534\t530\t0.992509\t0.479638\tPROTOP_A:ClassII:TIR:P\t1105\t576\t530\t0.479638\t2.9e-87\t1828\t98.4848\t4\n")
            f.write("DmelChr4_bench_Blaster_Grouper_5_Map_3\t5\t704\t700\t0.994318\t0.425791\tTC1-2_DM:ClassII:TIR:Tc1-Mariner\t1644\t945\t700\t0.425791\t7.3e-120\t1228\t98.1349\t5\n")
            f.write("DmelChr4_bench_Blaster_Grouper_5_Map_3\t274\t702\t429\t0.609375\t1.26176\tTC1-2_DMp:ClassII:TIR:Tc1-Mariner\t340\t198\t143\t0.420588\t5e-74\t265\t98.6014\t6\n")
            f.write("DmelChr4_bench_Blaster_Recon_12_Map_3\t311\t374\t64\t0.028021\t0.0359349\tBEL-27_AA-I_1p:ClassI:LTR:Bel-Pao\t605\t582\t24\t0.0134756\t6.3e-17\t11\t30.8511\t7\n")
            f.write("DmelChr4_bench_Blaster_Recon_12_Map_3\t1472\t1552\t81\t0.0354641\t0.0463918\tBEL-76_AA-I_1p:ClassI:LTR:Bel-Pao\t79\t52\t28\t0.0160367\t1e-13\t8\t27.7778\t8\n")


if __name__ == "__main__":
    unittest.main()
