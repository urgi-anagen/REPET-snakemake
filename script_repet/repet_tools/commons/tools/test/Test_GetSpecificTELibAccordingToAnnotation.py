import os
import unittest
from commons.core.utils.FileUtils import FileUtils
from commons.core.sql.DbMySql import DbMySql
from commons.tools.GetSpecificTELibAccordingToAnnotation import GetSpecificTELibAccordingToAnnotation

class Test_GetSpecificTELibAccordingToAnnotation(unittest.TestCase):
    
    def test_writeFastaFileFromGiveInfoTEAnnot(self):
        self._iDb = DbMySql()
        fileName = "GiveInfoTEannot.txt"
        self._writeGiveInfoTEannotOutput(fileName)
        tableName = "projectName_refTEs_chr_seq"
        self._createConsensusTable(tableName)
        expFileName = "consensusExp.fa"
        self._writeExpConsensusFile(expFileName)
        obsFileName = "{}.fa".format(os.path.splitext(fileName)[0])
        iGetTElib = GetSpecificTELibAccordingToAnnotation()
        iGetTElib.setTableName(tableName)
        iGetTElib.writeFastaFileFromGiveInfoTEAnnot(fileName)
        self._iDb.dropTable(tableName)
        self._iDb.close()
        self.assertTrue(FileUtils.are2FilesIdentical(expFileName, obsFileName))
        os.remove(fileName)
        os.remove(expFileName)
        os.remove(obsFileName)
        
    def _writeGiveInfoTEannotOutput(self, fileName):
        with open(fileName, "w") as f:
            f.write("Wcode\tTE\tmaxLength\tmeanLength\tcovg\tfrags\tfullLgthFrags\tcopies\tfullLgthCopies\tmeanId\tsdId\tminId\tq25Id\tmedId\tq75Id\tmaxId\tmeanLgth\tsdLgth\tminLgth\tq25Lgth\tmedLgth\tq75Lgth\tmaxLgth\tmeanLgthPerc\tsdLgthPerc\tminLgthPerc\tq25LgthPerc\tmedLgthPerc\tq75LgthPerc\tmaxLgthPerc\n")
            f.write("RLX\tAtha_benchs-B-P66.38-Map20_classII-Helitron-incomp\t2150\t2150\t195657\t158\t49\t152\t52\t84.88\t8.75\t74.29\t77.16\t80.08\t96.40\t97.83\t1287.32\t776.47\t21\t570.00\t1320.50\t2077.00\t2272\t59.88\t36.11\t0.98\t26.51\t61.42\t96.60\t105.67\n")
            f.write("PHG\tAtha_benchs-B-R1047-Map6_classI-LINE-comp\t1653\t1653\t25423\t57\t4\t56\t4\t79.09\t8.21\t64.32\t73.63\t79.21\t83.65\t96.65\t453.98\t488.66\t29\t98.00\t244.00\t634.00\t1650\t27.46\t29.56\t1.75\t5.93\t14.76\t38.35\t99.82\n")
            f.write("DXX|DTH\tAtha_benchs-B-R1276-Map4_classII-Helitron-incomp\t1293\t1293\t24416\t48\t4\t47\t4\t80.31\t5.74\t75.14\t77.00\t78.25\t80.89\t99.46\t519.60\t419.01\t37\t171.00\t334.00\t838.00\t1296\t40.19\t32.41\t2.86\t13.23\t25.83\t64.81\t100.23\n")

        
    def _createConsensusTable(self, tableName):
        fileName = "consensus.fa"
        with open(fileName, "w") as f:
            f.write(">Atha_benchs-B-P66.38-Map20_classII-Helitron-incomp\n")
            f.write("ATGCTAGCTAGCT\n")
            f.write(">Atha_benchs-B-R12-Map5_classII-Helitron-comp\n")
            f.write("ATGCTAGCTAGCTATGCTAGCTAGCATGCTAGCTAGCTT\n")
            f.write(">Atha_benchs-B-R1047-Map6_classI-LINE-comp\n")
            f.write("ATGCTAGCTAGCT\n")
            f.write(">Atha_benchs-B-G56-Map5_classII-Helitron-comp\n")
            f.write("ATGCTAGCTAGCTATGCTAGCTAGCT\n")
            f.write(">Atha_benchs-B-R1276-Map4_classII-Helitron-incomp\n")
            f.write("ATGCTAGCTAGCATGCTAGCTAGCTATGCTAGCTAGCTATGCTAGCTAGCTT\n")

        self._iDb.createTable(tableName, "seq", fileName, True)
        os.remove(fileName)
    
    def _writeExpConsensusFile(self, fileName):
        with open(fileName, "w")as f:
            f.write(">Atha_benchs-B-P66.38-Map20_classII-Helitron-incomp\n")
            f.write("ATGCTAGCTAGCT\n")
            f.write(">Atha_benchs-B-R1047-Map6_classI-LINE-comp\n")
            f.write("ATGCTAGCTAGCT\n")
            f.write(">Atha_benchs-B-R1276-Map4_classII-Helitron-incomp\n")
            f.write("ATGCTAGCTAGCATGCTAGCTAGCTATGCTAGCTAGCTATGCTAGCTAGCTT\n")

        
if __name__ == "__main__":
    unittest.main()