import os
import subprocess
import time
import random
import unittest

from shutil import rmtree
from commons.core.sql.DbMySql import DbMySql
from commons.core.sql.TablePathAdaptator import TablePathAdaptator
from commons.core.coord.PathUtils import PathUtils
from commons.core.utils.FileUtils import FileUtils
from commons.tools.RetrieveInitHeaders import RetrieveInitHeaders


class Test_F_RetrieveInitHeaders(unittest.TestCase):
    
    def setUp(self):
        self._curTestDir = os.getcwd()
        self._testPrefix = 'test_F_RIH_{}_{}'.format(time.strftime("%H%M%S"), random.randint(0, 1000))
        try:
            os.makedirs(self._testPrefix)
        except:pass
        os.chdir(self._testPrefix)

        self._iDb = DbMySql()
        
    def tearDown(self):
        self._iDb.close()
        os.chdir(self._curTestDir)
        try:
            rmtree(self._testPrefix)
        except:pass

    def test_run_as_script_rename_subject_and_clean_table(self):
        shortHLinkFileName = "dummy.shortHlink"
        self._writeShortHLinkFile(shortHLinkFileName)
        pathTableName = "dummyInput_path"
        self._createPathTable(pathTableName)
        expFileName = "exp.path"
        self._writeExpFile(expFileName)
        obsTableName = "dummyOutput_path"
        
        cmd = "RetrieveInitHeaders.py -i %s -l %s -o %s -s -c -v 1" % (pathTableName, shortHLinkFileName, obsTableName)
        process = subprocess.Popen(cmd, shell = True)
        process.communicate()

        self.assertTrue(self._iDb.doesTableExist(obsTableName))        
        self.assertFalse(self._iDb.doesTableExist(pathTableName))
        obsFileName = "obs.path"
        iTPA = TablePathAdaptator(self._iDb, obsTableName)
        lPaths = iTPA.getListOfAllPaths()
        PathUtils.writeListInFile(lPaths, obsFileName)
        self.assertTrue(FileUtils.are2FilesIdentical(expFileName, obsFileName))
        
        self._iDb.dropTable(obsTableName)

    def test_run_rename_subject_and_clean_table( self ):
        shortHLinkFileName = "dummy.shortHlink"
        self._writeShortHLinkFile(shortHLinkFileName)
        pathTableName = "dummyInput_path"
        self._createPathTable(pathTableName)
        expFileName = "exp.path"
        self._writeExpFile(expFileName)
        obsTableName = "dummyOutput_path"

        iRIH = RetrieveInitHeaders(pathTableName, shortHLinkFileName, obsTableName, False, True, 1)
        iRIH.run()

        self.assertTrue(self._iDb.doesTableExist(obsTableName))
        self.assertFalse(self._iDb.doesTableExist(pathTableName))
        obsFileName = "obs.path"
        iTPA = TablePathAdaptator(self._iDb, obsTableName)
        lPaths = iTPA.getListOfAllPaths()
        PathUtils.writeListInFile(lPaths, obsFileName)
        self.assertTrue(FileUtils.are2FilesIdentical(expFileName, obsFileName))

        self._iDb.dropTable(obsTableName)

    def _writeShortHLinkFile(self, fileName):
        with open(fileName, "w") as f:
            f.write("refTE_1\tPaphi_v2-B-R11932-Map16_classI-LTR-comp\t1\t5388\n")
            f.write("refTE_2\tPaphi_v2-B-R2469-Map20_classI-LTR-comp\t1\t6385\n")
            f.write("refTE_3\tPaphi_v2-B-R8543-Map9_classI-LTR-comp\t1\t7148\n")
            f.write("refTE_4\tPaphi_v2-B-R33384-Map4_classI-LTR-incomp\t1\t262\n")
            f.write("refTE_5\tPaphi_v2-B-P73.1298-Map3_classI-LTR-incomp\t1\t389\n")
            f.write("refTE_6\tPaphi_v2-B-R36143-Map3_classI-LTR-incomp\t1\t393\n")
            f.write("refTE_7\tPaphi_v2-B-R35261-Map5_classI-LTR-incomp\t1\t408\n")
            f.write("refTE_8\tPaphi_v2-B-R12434-Map4_classI-LTR-incomp\t1\t420\n")
            f.write("refTE_9\tPaphi_v2-B-R20580-Map4_classI-LTR-incomp\t1\t426\n")
            f.write("refTE_10\tPaphi_v2-B-R5129-Map3_classI-LTR-incomp\t1\t441\n")

    def _createPathTable(self, tableName):
        fileName = "dummy.path"
        with open(fileName, "w") as f:
            f.write("7615\tchunk0030\t188432\t188611\trefTE_1\t5386\t5208\t0\t174\t97.23\n")
            f.write("86375\tchunk0372\t155816\t157157\trefTE_1\t2213\t3575\t0\t843\t62.89\n")
            f.write("88018\tchunk0381\t52141\t53500\trefTE_1\t2191\t3575\t0\t860\t63.3\n")
            f.write("110377\tchunk0473\t117281\t117518\trefTE_1\t3966\t3716\t0\t230\t97.05\n")
            f.write("39621\tchunk0169\t123563\t124003\trefTE_10\t1\t441\t0\t439\t99.55\n")
            f.write("544710\tchunk2778\t62387\t62625\trefTE_10\t100\t327\t0\t165\t69.09\n")
            f.write("601761\tchunk4780\t441\t559\trefTE_10\t5\t123\t0\t119\t100\n")
            f.write("17589\tchunk0071\t58591\t59763\trefTE_2\t5139\t6384\t0\t1150\t98.12\n")
            f.write("21050\tchunk0087\t186576\t186812\trefTE_2\t3636\t3891\t0\t232\t98.31\n")
        self._iDb.createTable(tableName, "path", fileName, True)
        os.remove(fileName)
        
    def _writeExpFile(self, fileName):
        with open(fileName, "w") as f:
            f.write("7615\tchunk0030\t188432\t188611\tPaphi_v2-B-R11932-Map16_classI-LTR-comp\t5386\t5208\t0\t174\t97.23\n")
            f.write("86375\tchunk0372\t155816\t157157\tPaphi_v2-B-R11932-Map16_classI-LTR-comp\t2213\t3575\t0\t843\t62.89\n")
            f.write("88018\tchunk0381\t52141\t53500\tPaphi_v2-B-R11932-Map16_classI-LTR-comp\t2191\t3575\t0\t860\t63.3\n")
            f.write("110377\tchunk0473\t117281\t117518\tPaphi_v2-B-R11932-Map16_classI-LTR-comp\t3966\t3716\t0\t230\t97.05\n")
            f.write("39621\tchunk0169\t123563\t124003\tPaphi_v2-B-R5129-Map3_classI-LTR-incomp\t1\t441\t0\t439\t99.55\n")
            f.write("544710\tchunk2778\t62387\t62625\tPaphi_v2-B-R5129-Map3_classI-LTR-incomp\t100\t327\t0\t165\t69.09\n")
            f.write("601761\tchunk4780\t441\t559\tPaphi_v2-B-R5129-Map3_classI-LTR-incomp\t5\t123\t0\t119\t100.0\n")
            f.write("17589\tchunk0071\t58591\t59763\tPaphi_v2-B-R2469-Map20_classI-LTR-comp\t5139\t6384\t0\t1150\t98.12\n")
            f.write("21050\tchunk0087\t186576\t186812\tPaphi_v2-B-R2469-Map20_classI-LTR-comp\t3636\t3891\t0\t232\t98.31\n")

if __name__ == "__main__":
    unittest.main()