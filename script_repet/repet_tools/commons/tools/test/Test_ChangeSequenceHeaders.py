# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
import os
import time
import random
import shutil
import unittest
import subprocess
from commons.core.utils.FileUtils import FileUtils
from commons.tools.ChangeSequenceHeaders import ChangeSequenceHeaders


class Test_ChangeSequenceHeaders(unittest.TestCase):

    def setUp(self):
        self._curTestDir = os.getcwd()
        self._testPrefix = 'test_CSH_{}_{}'.format(time.strftime("%H%M%S"), random.randint(0, 1000))
        try:
            os.makedirs(self._testPrefix)
        except:pass
        os.chdir(self._testPrefix)
        self._i = ChangeSequenceHeaders()
        self._uniqId = "{}_{}".format(time.strftime("%Y%m%d%H%M%S"), os.getpid())

    def tearDown(self):
        os.chdir(self._curTestDir)
        try:
            shutil.rmtree(self._testPrefix)
        except:pass
        self._i = None
        self._uniqId = None

    def test_script_no_input_file(self):

        inFile = "dummyInFaFile_{}".format(self._uniqId)

        obsFile = "dummyObsFile_{}".format(self._uniqId)

        cmd = "ChangeSequenceHeaders.py"
        cmd += " -i {}".format(inFile)
        cmd += " -f fasta"
        cmd += " -s 1"
        cmd += " -p TE"
        cmd += " -o {}".format(obsFile)
        exitStatus = subprocess.call(cmd, shell=True)

        self.assertFalse(exitStatus == 0)

    def test_shortenSequenceHeadersForFastaFile_fasta_script(self):

        inFile = "dummyInFaFile_{}".format(self._uniqId)
        with open(inFile, "w") as inF:
            inF.write(">DmelChr4-B-G387-MAP16\nATGTACGATGACGATCAG\n")
            inF.write(">consensus524\nGTGCGGATGGAACAGT\n")

        linkFile = "dummyLinkFile_{}".format(self._uniqId)

        expFile = "dummyExpFile_{}".format(self._uniqId)
        with open(expFile, "w") as expF:
            expF.write(">TE1\nATGTACGATGACGATCAG\n")
            expF.write(">TE2\nGTGCGGATGGAACAGT\n")

        obsFile = "dummyObsFile_{}".format(self._uniqId)

        cmd = "ChangeSequenceHeaders.py"
        cmd += " -i {}".format(inFile)
        cmd += " -f fasta"
        cmd += " -s 1"
        cmd += " -p TE"
        cmd += " -l {}".format(linkFile)
        cmd += " -o {}".format(obsFile)
        exitStatus = subprocess.call(cmd, shell=True)

        self.assertTrue(exitStatus == 0)
        self.assertTrue(FileUtils.are2FilesIdentical(expFile, obsFile))

    def test_retrieveInitialSequenceHeaders_fasta_script(self):
        inFile = "dummyInFaFile_{}".format(self._uniqId)
        with open(inFile, "w") as inF:
            inF.write(">seq2\nATGTACGATGACGATCAG\n")
            inF.write(">seq1\nGTGCGGATGGAACAGT\n")

        linkFile = "dummyLinkFile_{}".format(self._uniqId)
        with open(linkFile, "w") as linkF:
            linkF.write("seq1\tconsensus524\t1\t18\n")
            linkF.write("seq2\tDmelChr4-B-G387-MAP16\t1\t16\n")
            linkF.write("seq3\treference2\n")

        expFile = "dummyExpFile_{}".format(self._uniqId)
        with open(expFile, "w") as expF:
            expF.write(">DmelChr4-B-G387-MAP16\nATGTACGATGACGATCAG\n")
            expF.write(">consensus524\nGTGCGGATGGAACAGT\n")

        obsFile = "dummyObsFile_{}".format(self._uniqId)

        cmd = "ChangeSequenceHeaders.py"
        cmd += " -i {}".format(inFile)
        cmd += " -f fasta"
        cmd += " -s 2"
        cmd += " -l {}".format(linkFile)
        cmd += " -o {}".format(obsFile)
        exitStatus = subprocess.call(cmd, shell=True)

        self.assertTrue(exitStatus == 0)
        self.assertTrue(FileUtils.are2FilesIdentical(expFile, obsFile))

    def test_retrieveInitialSequenceHeaders_fastaFromClustering_afterLTRHarvest_Blastclust(self):
        inFile = "dummyInFaFile_{}".format(self._uniqId)
        with open(inFile, "w") as inF:
            inF.write(">BlastclustCluster1Mb3_seq2\nATGTACGATGACGATCAG\n")
            inF.write(">BlastclustCluster8Mb4_seq1\nGTGCGGATGGAACAGT\n")

        linkFile = "dummyLinkFile_{}".format(self._uniqId)
        with open(linkFile, "w") as linkF:
            linkF.write("seq1\tchunk1 (dbseq-nr 1) [41806,41825]\t1\t18\n")
            linkF.write("seq2\tchunk2 (dbseq-nr 6) [41006,41023]\t1\t16\n")
            linkF.write("seq3\treference2\n")


        expFile = "dummyExpFile_{}".format(self._uniqId)
        with open(expFile, "w") as expF:
            expF.write(">BlastclustCluster1Mb3_chunk2 (dbseq-nr 6) [41006,41023]\nATGTACGATGACGATCAG\n")
            expF.write(">BlastclustCluster8Mb4_chunk1 (dbseq-nr 1) [41806,41825]\nGTGCGGATGGAACAGT\n")

        obsFile = "dummyObsFile_{}".format(self._uniqId)

        whichCluster = "A"
        cmd = "ChangeSequenceHeaders.py"
        cmd += " -i {}".format(inFile)
        cmd += " -f fasta"
        cmd += " -s 2"
        cmd += " -l {}".format(linkFile)
        cmd += " -w {}".format(whichCluster)
        cmd += " -o {}".format(obsFile)
        exitStatus = subprocess.call(cmd, shell=True)

        self.assertTrue(exitStatus == 0)
        self.assertTrue(FileUtils.are2FilesIdentical(expFile, obsFile))

    def test_retrieveInitialSequenceHeaders_fastaFromClustering_forClusterConsensus_Blastclust(self):
        inFile = "dummyInFaFile_{}".format(self._uniqId)
        with open(inFile, "w") as inF:
            inF.write(">BlastclustCluster8Mb4_seq1\nGTGCGGATGGAACAGT\n")
            inF.write(">BlastclustCluster1Mb3_seq2\nATGTACGATGACGATCAG\n")

        linkFile = "dummyLinkFile_{}".format(self._uniqId)
        with open(linkFile, "w") as linkF:
            linkF.write("seq1\tDHX-incomp_DmelChr4-B-R1-Map4\t1\t18\n")
            linkF.write("seq2\tRLX-incomp_DmelChr4-B-R12-Map3_reversed\t1\t16\n")
            linkF.write("seq3\treference2\n")

        expFile = "dummyExpFile_{}".format(self._uniqId)
        with open(expFile, "w") as expF:
            expF.write(">DHX-incomp_Blc8_DmelChr4-B-R1-Map4\nGTGCGGATGGAACAGT\n")
            expF.write(">RLX-incomp_Blc1_DmelChr4-B-R12-Map3_reversed\nATGTACGATGACGATCAG\n")

        obsFile = "dummyObsFile_{}".format(self._uniqId)

        whichCluster = "B"
        cmd = "ChangeSequenceHeaders.py"
        cmd += " -i {}".format(inFile)
        cmd += " -f fasta"
        cmd += " -s 2"
        cmd += " -l {}".format(linkFile)
        cmd += " -w {}".format(whichCluster)
        cmd += " -o {}".format(obsFile)
        exitStatus = subprocess.call(cmd, shell=True)

        self.assertTrue(exitStatus == 0)
        self.assertTrue(FileUtils.are2FilesIdentical(expFile, obsFile))

    def test_retrieveInitialSequenceHeaders_fastaFromClustering_afterLTRHarvest_MCL(self):

        inFile = "dummyInFaFile_{}".format(self._uniqId)
        with open(inFile, "w") as inF:
            inF.write(">MCLCluster1Mb3_seq2\nATGTACGATGACGATCAG\n")
            inF.write(">MCLCluster8Mb4_seq1\nGTGCGGATGGAACAGT\n")

        linkFile = "dummyLinkFile_{}".format(self._uniqId)
        with open(linkFile, "w") as linkF:
            linkF.write("seq1\tchunk1 (dbseq-nr 1) [41806,41825]\t1\t18\n")
            linkF.write("seq2\tchunk2 (dbseq-nr 6) [41006,41023]\t1\t16\n")
            linkF.write("seq3\treference2\n")

        expFile = "dummyExpFile_{}".format(self._uniqId)
        with open(expFile, "w") as expF:
            expF.write(">MCLCluster1Mb3_chunk2 (dbseq-nr 6) [41006,41023]\nATGTACGATGACGATCAG\n")
            expF.write(">MCLCluster8Mb4_chunk1 (dbseq-nr 1) [41806,41825]\nGTGCGGATGGAACAGT\n")

        obsFile = "dummyObsFile_{}".format(self._uniqId)

        whichCluster = "A"
        cmd = "ChangeSequenceHeaders.py"
        cmd += " -i {}".format(inFile)
        cmd += " -f fasta"
        cmd += " -s 2"
        cmd += " -l {}".format(linkFile)
        cmd += " -w {}".format(whichCluster)
        cmd += " -o {}".format(obsFile)
        exitStatus = subprocess.call(cmd, shell=True)

        self.assertTrue(exitStatus == 0)
        self.assertTrue(FileUtils.are2FilesIdentical(expFile, obsFile))

    def test_retrieveInitialSequenceHeaders_fastaFromClustering_forClusterConsensus_MCL(self):
        inFile = "dummyInFaFile_{}".format(self._uniqId)
        with open(inFile, "w") as inF:
            inF.write(">MCLCluster8Mb4_seq1\nGTGCGGATGGAACAGT\n")
            inF.write(">MCLCluster1Mb3_seq2\nATGTACGATGACGATCAG\n")

        linkFile = "dummyLinkFile_{}".format(self._uniqId)
        with open(linkFile, "w") as linkF:
            linkF.write("seq1\tDHX-incomp_DmelChr4-B-R1-Map4\t1\t18\n")
            linkF.write("seq2\tRLX-incomp_DmelChr4-B-R12-Map3_reversed\t1\t16\n")
            linkF.write("seq3\treference2\n")

        expFile = "dummyExpFile_{}".format(self._uniqId)
        with open(expFile, "w") as expF:
            expF.write(">DHX-incomp_MCL8_DmelChr4-B-R1-Map4\nGTGCGGATGGAACAGT\n")
            expF.write(">RLX-incomp_MCL1_DmelChr4-B-R12-Map3_reversed\nATGTACGATGACGATCAG\n")

        obsFile = "dummyObsFile_{}".format(self._uniqId)

        whichCluster = "B"
        cmd = "ChangeSequenceHeaders.py"
        cmd += " -i {}".format(inFile)
        cmd += " -f fasta"
        cmd += " -s 2"
        cmd += " -l {}".format(linkFile)
        cmd += " -w {}".format(whichCluster)
        cmd += " -o {}".format(obsFile)
        exitStatus = subprocess.call(cmd, shell=True)

        self.assertTrue(exitStatus == 0)
        self.assertTrue(FileUtils.are2FilesIdentical(expFile, obsFile))

    def test_retrieveInitialSequenceHeaders_newick_script(self):
        inFile = "dummyInFile_{}".format(self._uniqId)
        with open(inFile, "w") as inF:
            inF.write("(seq4:0.012511,(seq3:0.005340,seq2:0.002201))")

        linkFile = "dummyLinkFile_{}".format(self._uniqId)
        with open(linkFile, "w") as linkF:
            linkF.write("seq1\t1360\n")
            linkF.write("seq2\tDmelChr4-B-P2.0-MAP3_classII-TIR-comp|1cl-1gr|26copies\n")
            linkF.write("seq3\tDmelChr4-B-G20-MAP3_classII-TIR-comp|1cl-1gr|53copies\n")
            linkF.write("seq4\tDmelChr4-B-G14-MAP17_classII-TIR-comp|1cl-1gr|41copies\n")

        expFile = "dummyExpFile_{}".format(self._uniqId)
        with open(expFile, "w") as expF:
            expF.write(
                "(DmelChr4-B-G14-MAP17_classII-TIR-comp|1cl-1gr|41copies:0.012511,(DmelChr4-B-G20-MAP3_classII-TIR-comp|1cl-1gr|53copies:0.005340,DmelChr4-B-P2.0-MAP3_classII-TIR-comp|1cl-1gr|26copies:0.002201))")

        obsFile = "dummyObsFile_{}".format(self._uniqId)

        cmd = "ChangeSequenceHeaders.py"
        cmd += " -i {}".format(inFile)
        cmd += " -f newick"
        cmd += " -s 2"
        cmd += " -l {}".format(linkFile)
        cmd += " -o {}".format(obsFile)
        exitStatus = subprocess.call(cmd, shell=True)

        self.assertTrue(exitStatus == 0)
        self.assertTrue(FileUtils.are2FilesIdentical(expFile, obsFile))

    def test_retrieveInitialSequenceHeadersForAlignFile(self):
        linkFile = "dummyLinkFile_{}".format(self._uniqId)
        with open(linkFile, "w") as linkF:
            linkF.write("seq1\tname=Dm_Blaster_Piler_30.38_Map_8|category=classI|order=LTR|completeness=comp\t1\t1000\n")
            linkF.write("seq2\tname=Dm_Blaster_Recon_34_Map_20|category=classI|order=LTR|completeness=comp\t1\t800\n")

        inFile = "dummyAlignFile_{}".format(self._uniqId)
        with open(inFile, "w") as inFileHandler:
            inFileHandler.write("seq1\t1\t100\tseq2\t110\t11\t1e-38\t254\t98.5\n")
            inFileHandler.write("seq2\t11\t110\tseq1\t100\t1\t1e-38\t254\t98.5\n")

        expFile = "dummyExpAlignFile_{}".format(self._uniqId)
        with open(expFile, "w") as expFileHandler:
            expFileHandler.write(
                "name=Dm_Blaster_Piler_30.38_Map_8|category=classI|order=LTR|completeness=comp\t1\t100\tname=Dm_Blaster_Recon_34_Map_20|category=classI|order=LTR|completeness=comp\t110\t11\t1e-38\t254\t98.5\n")
            expFileHandler.write(
                "name=Dm_Blaster_Recon_34_Map_20|category=classI|order=LTR|completeness=comp\t11\t110\tname=Dm_Blaster_Piler_30.38_Map_8|category=classI|order=LTR|completeness=comp\t100\t1\t1e-38\t254\t98.5\n")

        obsFile = "dummyObsAlignFile_{}".format(self._uniqId)

        cmd = "ChangeSequenceHeaders.py"
        cmd += " -i {}".format(inFile)
        cmd += " -f align"
        cmd += " -s 2"
        cmd += " -l {}".format(linkFile)
        cmd += " -o {}".format(obsFile)
        exitStatus = subprocess.call(cmd, shell=True)

        self.assertTrue(exitStatus == 0)
        self.assertTrue(FileUtils.are2FilesIdentical(expFile, obsFile))

    def test_retrieveInitialSequenceHeadersForPathFile(self):
        linkFile = "dummyLinkFile_{}".format(self._uniqId)
        with open(linkFile, "w") as linkF:
            linkF.write("seq1\tname=Dm_Blaster_Piler_30.38_Map_8|category=classI|order=LTR|completeness=comp\t1\t1000\n")
            linkF.write("seq2\tname=Dm_Blaster_Recon_34_Map_20|category=classI|order=LTR|completeness=comp\t1\t800\n")

        inFile = "dummyAlignFile_{}".format(self._uniqId)
        with open(inFile, "w") as inFileHandler:
            inFileHandler.write("11\tseq1\t1\t100\tseq2\t110\t11\t1e-38\t254\t98.5\n")
            inFileHandler.write("2\tseq2\t11\t110\tseq1\t100\t1\t1e-38\t254\t98.5\n")

        expFile = "dummyExpAlignFile_{}".format(self._uniqId)
        with open(expFile, "w") as expFileHandler:
            expFileHandler.write(
                "11\tname=Dm_Blaster_Piler_30.38_Map_8|category=classI|order=LTR|completeness=comp\t1\t100\tname=Dm_Blaster_Recon_34_Map_20|category=classI|order=LTR|completeness=comp\t110\t11\t1e-38\t254\t98.5\n")
            expFileHandler.write(
                "2\tname=Dm_Blaster_Recon_34_Map_20|category=classI|order=LTR|completeness=comp\t11\t110\tname=Dm_Blaster_Piler_30.38_Map_8|category=classI|order=LTR|completeness=comp\t100\t1\t1e-38\t254\t98.5\n")

        obsFile = "dummyObsAlignFile_{}".format(self._uniqId)

        cmd = "ChangeSequenceHeaders.py"
        cmd += " -i {}".format(inFile)
        cmd += " -f path"
        cmd += " -s 2"
        cmd += " -l {}".format(linkFile)
        cmd += " -o {}".format(obsFile)
        exitStatus = subprocess.call(cmd, shell=True)

        self.assertTrue(exitStatus == 0)
        self.assertTrue(FileUtils.are2FilesIdentical(expFile, obsFile))

    def test_retrieveInitialSequenceHeadersForClusterFile(self):
        linkFile = "fileName.fa.newHlink"

        with open(linkFile, "w") as linkF :
            linkF.write("seq29\tRLX-comp_aLCV2_1_5000-L-B133-Map1_reversed\t1\t5033\n")
            linkF.write("seq32\tRLX-comp_aLCV2_1_5000-L-B128-Map1_reversed\t1\t5071\n")
            linkF.write("seq82\tRLX-comp_aLCV2_1_5000-L-B28-Map1_reversed\t1\t9343\n")
            linkF.write("seq119\tRLX-incomp_aLCV2_1_5000-L-B35-Map1\t1\t7132\n")
            linkF.write("seq157\tRXX-LARD-chim_aLCV2_1_5000-L-B36-Map1_reversed\t1\t7026\n")
            linkF.write("seq159\tRXX-LARD-chim_aLCV2_1_5000-L-B33-Map1_reversed\t1\t7281\n")
            linkF.write("seq161\tRXX-LARD-chim_aLCV2_1_5000-L-B31-Map1\t1\t7551\n")
            linkF.write("seq162\tRXX-LARD-chim_aLCV2_1_5000-L-B29-Map1\t1\t7594\n")
            linkF.write("seq14\taLCV2_1_5000-B-P29.6-Map12\t1\t7512\n")

        inFile = "dummyAlignFile_{}".format(self._uniqId)
        with open(inFile, "w") as inFileHandler:
            inFileHandler.write("seq119\tseq157\tseq159\tseq161\tseq162\n")
            inFileHandler.write("seq29\tseq32\tseq82\n")
            inFileHandler.write("seq14\n")

        expFile = "expfileName_newHlink.map"
        with open(expFile, "w") as expFileHandler:
            expFileHandler.write(
                "RLX-incomp_aLCV2_1_5000-L-B35-Map1\tRXX-LARD-chim_aLCV2_1_5000-L-B36-Map1_reversed\tRXX-LARD-chim_aLCV2_1_5000-L-B33-Map1_reversed\tRXX-LARD-chim_aLCV2_1_5000-L-B31-Map1\tRXX-LARD-chim_aLCV2_1_5000-L-B29-Map1\n")
            expFileHandler.write(
                "RLX-comp_aLCV2_1_5000-L-B133-Map1_reversed\tRLX-comp_aLCV2_1_5000-L-B128-Map1_reversed\tRLX-comp_aLCV2_1_5000-L-B28-Map1_reversed\n")
            expFileHandler.write("aLCV2_1_5000-B-P29.6-Map12\n")

        obsFile = "obsfileName.map"
        iCSH = ChangeSequenceHeaders(inFile=inFile, outFile=obsFile)
        iCSH.retrieveInitialSequenceHeadersForClusterFile(iCSH.mapFile2Dict(linkFile))
        self.assertTrue(FileUtils.are2FilesIdentical(expFile, obsFile))

        inFile = "dummyClusterFile_type2".format(self._uniqId)
        with open(inFile, "w") as inFileHandler:
            inFileHandler.write("MCLCluster1\n")
            inFileHandler.write("\tseq119\n")
            inFileHandler.write("\tseq157\n")
            inFileHandler.write("\tseq159\n")
            inFileHandler.write("\tseq162\n")
            inFileHandler.write("MCLCluster2\n")
            inFileHandler.write("\tseq29\n")
            inFileHandler.write("\tseq32\n")
            inFileHandler.write("\tseq82\n")
            inFileHandler.write("MCLCluster3\n")
            inFileHandler.write("\tseq14\n")
        expFile = "expfileName_ClusterFile_type2.txt"
        with open(expFile, "w") as expFileHandler:
            expFileHandler.write("MCLCluster1\n")
            expFileHandler.write("\tRLX-incomp_aLCV2_1_5000-L-B35-Map1\n")
            expFileHandler.write("\tRXX-LARD-chim_aLCV2_1_5000-L-B36-Map1_reversed\n")
            expFileHandler.write("\tRXX-LARD-chim_aLCV2_1_5000-L-B33-Map1_reversed\n")
            expFileHandler.write("\tRXX-LARD-chim_aLCV2_1_5000-L-B29-Map1\n")
            expFileHandler.write("MCLCluster2\n")
            expFileHandler.write("\tRLX-comp_aLCV2_1_5000-L-B133-Map1_reversed\n")
            expFileHandler.write("\tRLX-comp_aLCV2_1_5000-L-B128-Map1_reversed\n")
            expFileHandler.write("\tRLX-comp_aLCV2_1_5000-L-B28-Map1_reversed\n")
            expFileHandler.write("MCLCluster3\n")
            expFileHandler.write("\taLCV2_1_5000-B-P29.6-Map12\n")

        obsFile = "obsfileName.map.txt"
        iCSH = ChangeSequenceHeaders(inFile=inFile, outFile=obsFile)
        iCSH.retrieveInitialSequenceHeadersForClusterFile(iCSH.mapFile2Dict(linkFile))
        self.assertTrue(FileUtils.are2FilesIdentical(expFile, obsFile))

    def test_retrieveInitialSequenceHeadersForClusterFile_asScript(self):
        linkFile = "fileName.fa.newHlink"

        with open(linkFile, "w") as linkF :
            linkF.write("seq29\tRLX-comp_aLCV2_1_5000-L-B133-Map1_reversed\t1\t5033\n")
            linkF.write("seq32\tRLX-comp_aLCV2_1_5000-L-B128-Map1_reversed\t1\t5071\n")
            linkF.write("seq82\tRLX-comp_aLCV2_1_5000-L-B28-Map1_reversed\t1\t9343\n")
            linkF.write("seq119\tRLX-incomp_aLCV2_1_5000-L-B35-Map1\t1\t7132\n")
            linkF.write("seq157\tRXX-LARD-chim_aLCV2_1_5000-L-B36-Map1_reversed\t1\t7026\n")
            linkF.write("seq159\tRXX-LARD-chim_aLCV2_1_5000-L-B33-Map1_reversed\t1\t7281\n")
            linkF.write("seq161\tRXX-LARD-chim_aLCV2_1_5000-L-B31-Map1\t1\t7551\n")
            linkF.write("seq162\tRXX-LARD-chim_aLCV2_1_5000-L-B29-Map1\t1\t7594\n")

        inFile = "dummyClusterFile_{}".format(self._uniqId)
        inFileHandler = open(inFile, "w")
        inFileHandler.write("seq119\tseq157\tseq159\tseq161\tseq162\n")
        inFileHandler.write("seq29\tseq32\tseq82\n")

        inFileHandler.close()

        expFile = "expfileName_newHlink.map"
        with open(expFile, "w") as  expFileHandler:

            expFileHandler.write(
                "RLX-incomp_aLCV2_1_5000-L-B35-Map1\tRXX-LARD-chim_aLCV2_1_5000-L-B36-Map1_reversed\tRXX-LARD-chim_aLCV2_1_5000-L-B33-Map1_reversed\tRXX-LARD-chim_aLCV2_1_5000-L-B31-Map1\tRXX-LARD-chim_aLCV2_1_5000-L-B29-Map1\n")
            expFileHandler.write(
                "RLX-comp_aLCV2_1_5000-L-B133-Map1_reversed\tRLX-comp_aLCV2_1_5000-L-B128-Map1_reversed\tRLX-comp_aLCV2_1_5000-L-B28-Map1_reversed\n")

        obsFile = "obsClusterFile.map"
        cmd = "ChangeSequenceHeaders.py"
        cmd += " -i {}".format(inFile)
        cmd += " -f cluster"
        cmd += " -s 2"
        cmd += " -l {}".format(linkFile)
        cmd += " -o {}".format(obsFile)
        exitStatus = subprocess.call(cmd, shell=True)

        self.assertTrue(exitStatus == 0)
        self.assertTrue(FileUtils.are2FilesIdentical(expFile, obsFile))

    def test_mapFile2Dict(self):
        linkFile = "fileName.fa.newHlink"
        with open(linkFile, "w") as linkF:
            linkF.write("seq29\tRLX-comp_aLCV2_1_5000-L-B133-Map1_reversed\t1\t5033\n")
            linkF.write("seq32\tRLX-comp_aLCV2_1_5000-L-B128-Map1_reversed\t1\t5071\n")
            linkF.write("seq82\tRLX-comp_aLCV2_1_5000-L-B28-Map1_reversed\t1\t9343\n")
            linkF.write("seq119\tRLX-incomp_aLCV2_1_5000-L-B35-Map1\t1\t7132\n")
            linkF.write("seq157\tRXX-LARD-chim_aLCV2_1_5000-L-B36-Map1_reversed\t1\t7026\n")
            linkF.write("seq159\tRXX-LARD-chim_aLCV2_1_5000-L-B33-Map1_reversed\t1\t7281\n")
            linkF.write("seq161\tRXX-LARD-chim_aLCV2_1_5000-L-B31-Map1\t1\t7551\n")
            linkF.write("seq162\tRXX-LARD-chim_aLCV2_1_5000-L-B29-Map1\t1\t7594\n")


        expDict = {"seq29": "RLX-comp_aLCV2_1_5000-L-B133-Map1_reversed",
                   "seq32": "RLX-comp_aLCV2_1_5000-L-B128-Map1_reversed",
                   "seq82": "RLX-comp_aLCV2_1_5000-L-B28-Map1_reversed", "seq119": "RLX-incomp_aLCV2_1_5000-L-B35-Map1",
                   "seq157": "RXX-LARD-chim_aLCV2_1_5000-L-B36-Map1_reversed",
                   "seq159": "RXX-LARD-chim_aLCV2_1_5000-L-B33-Map1_reversed",
                   "seq161": "RXX-LARD-chim_aLCV2_1_5000-L-B31-Map1", "seq162": "RXX-LARD-chim_aLCV2_1_5000-L-B29-Map1"}
        iCSH = ChangeSequenceHeaders()
        obsDict = iCSH.mapFile2Dict(linkFile)
        self.assertEqual(expDict, obsDict)
        os.remove(linkFile)

    def test_mapFile2Dict_emptyMapFile(self):
        linkFile = "fileName.fa.newHlink"
        linkF = open(linkFile, "w")
        linkF.close()
        expDict = {}
        iCSH = ChangeSequenceHeaders()
        obsDict = iCSH.mapFile2Dict(linkFile)
        self.assertEqual(expDict, obsDict)

    def test_SeqHeaderWithoutClusterName_step3( self ):
        with open("clusteredConsensusFastaFile.fa", "w") as inF:
            inF.write(">Bdis_MCL22_TEdenovoGr-B-G1-Map20\nATGTACGATGACGATCAG\n")
            inF.write(">Bdis_MCL9_TEdenovoGr-B-G10-Map16_reversed\nGTGCGGATGGAACAGT\n")
            inF.write(">Bdis_MCL488_TEdenovoGr-B-G100-Map20\nATGTACGATGACGATCAG\n")
            inF.write(">Bdis_MCL155_TEdenovoGr-B-G10002-Map3\nGTGCGGATGGAACAGT\n")

        with open("exp_nameWithClusterTOname.tab", "w") as inF:
            inF.write("Bdis_MCL22_TEdenovoGr-B-G1-Map20\tBdis_TEdenovoGr-B-G1-Map20\n")
            inF.write("Bdis_MCL9_TEdenovoGr-B-G10-Map16_reversed\tBdis_TEdenovoGr-B-G10-Map16_reversed\n")
            inF.write("Bdis_MCL488_TEdenovoGr-B-G100-Map20\tBdis_TEdenovoGr-B-G100-Map20\n")
            inF.write("Bdis_MCL155_TEdenovoGr-B-G10002-Map3\tBdis_TEdenovoGr-B-G10002-Map3\n")

        iCSH = ChangeSequenceHeaders(inFile="clusteredConsensusFastaFile.fa", format="fasta", step=3)
        iCSH.run()
        self.assertTrue(FileUtils.are2FilesIdentical("exp_nameWithClusterTOname.tab", "clusteredConsensusFastaFile.fa.tab"))

    def test_runAsScript_SeqHeaderWithoutClusterName_step3( self ):
        inFile = "clusteredConsensusFastaFile.fa"
        with open(inFile, "w") as inF:
            inF.write(">Bdis_Bl22_TEdenovoGr-B-G1-Map20\nATGTACGATGACGATCAG\n")
            inF.write(">Bdis_Bl9_TEdenovoGr-B-G10-Map16\nGTGCGGATGGAACAGT\n")
            inF.write(">Bdis_Bl488_TEdenovoGr-B-G100-Map20\nATGTACGATGACGATCAG\n")
            inF.write(">Bdis_Bl155_TEdenovoGr-B-G10002-Map3\nGTGCGGATGGAACAGT\n")
        expFile = "exp_nameWithClusterTOname.tab"
        with open(expFile, "w") as inF:
            inF.write("Bdis_Bl22_TEdenovoGr-B-G1-Map20\tBdis_TEdenovoGr-B-G1-Map20\n")
            inF.write("Bdis_Bl9_TEdenovoGr-B-G10-Map16\tBdis_TEdenovoGr-B-G10-Map16\n")
            inF.write("Bdis_Bl488_TEdenovoGr-B-G100-Map20\tBdis_TEdenovoGr-B-G100-Map20\n")
            inF.write("Bdis_Bl155_TEdenovoGr-B-G10002-Map3\tBdis_TEdenovoGr-B-G10002-Map3\n")

        obsFile = "nameWithClusterTOname.tab"
        cmd = "ChangeSequenceHeaders.py"
        cmd += " -i {}".format(inFile)
        cmd += " -f fasta"
        cmd += " -s 3"
        cmd += " -p Bl"
        cmd += " -o {}".format(obsFile)
        exitStatus = subprocess.call(cmd, shell=True)

        self.assertTrue(exitStatus == 0)
        self.assertTrue(FileUtils.are2FilesIdentical(expFile, obsFile))


if __name__ == "__main__":
    unittest.main()
