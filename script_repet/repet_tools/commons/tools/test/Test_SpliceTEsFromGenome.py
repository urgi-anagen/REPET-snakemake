import os
import subprocess
import unittest
from commons.core.utils.FileUtils import FileUtils

class Test_SpliceTEsFromGenome( unittest.TestCase ):

    def test_zLaunchAsScript(self):
        cDir = os.getcwd()
        
        coordFile = "dummyCoordFile"
        coordFileHandler = open( coordFile, "w" )
        coordFileHandler.write( "TE1\tchr1\t2\t5\n" )
        coordFileHandler.write( "TE2\tchr1\t11\t15\n" )
        coordFileHandler.write( "TE3\tchr1\t12\t14\n" )
        coordFileHandler.close()
        
        genomeFile = "dummyGenomeFile"
        genomeFileHandler = open( genomeFile, "w" )
        genomeFileHandler.write( ">chr1\n" )
        genomeFileHandler.write( "AGGGGAAAAACCCCCAAAAA\n" )
        genomeFileHandler.write( ">chr2\n" )
        genomeFileHandler.write( "TTTTTTTTTT\n" )
        genomeFileHandler.close()
        
        expFile = "dummyExpFile"
        expFileHandler = open( expFile, "w" )
        expFileHandler.write( ">chr1\n" )
        expFileHandler.write( "AAAAAAAAAAA\n" )
        expFileHandler.write( ">chr2\n" )
        expFileHandler.write( "TTTTTTTTTT\n" )
        expFileHandler.close()
        
        obsFile = "dummyObsFile"
        
        cmd = "SpliceTEsFromGenome.py"
        cmd += " -i {}".format( coordFile )
        cmd += " -f {}".format( "map" )
        cmd += " -g {}".format( genomeFile )
        cmd += " -o {}".format( obsFile )
        cmd += " -v {}".format( 0 )
        returnStatus = subprocess.call(cmd, shell=True)
        
        self.assertTrue( returnStatus == 0 )
        self.assertTrue( FileUtils.are2FilesIdentical( expFile, obsFile ) )
        
        for f in [ coordFile, genomeFile, expFile, obsFile ]:
            os.remove( f )
        os.chdir( cDir )
        
    def test_LaunchAsScript_TEsOverlapped( self ):
        cDir = os.getcwd()
        coordFile = "dummyCoordFile"
        coordFileHandler = open( coordFile, "w" )
        coordFileHandler.write( "TE1\tchr1\t2\t5\n" )
        coordFileHandler.write( "TE2\tchr1\t11\t15\n" )
        coordFileHandler.write( "TE3\tchr1\t14\t18\n" )
        coordFileHandler.write( "TE3\tchr1\t21\t29\n" )
        coordFileHandler.write( "TE3\tchr2\t3\t5\n" )
        coordFileHandler.write( "TE3\tchr2\t11\t13\n" )
        coordFileHandler.write( "TE3\tchr2\t10\t14\n" )
        coordFileHandler.write( "TE3\tchr2\t9\t15\n" )
        coordFileHandler.close()
        
        genomeFile = "dummyGenomeFile"
        genomeFileHandler = open( genomeFile, "w" )
        genomeFileHandler.write( ">chr1\n" )
        genomeFileHandler.write( "AGGGGAAAAACCCCCAAAAATTGGTTGG\n" )
        genomeFileHandler.write( ">chr2\n" )
        genomeFileHandler.write( "TTAAATTTTTAAATTT\n" )
        genomeFileHandler.close()
        
        expFile = "dummyExpFile"
        expFileHandler = open( expFile, "w" )
        expFileHandler.write( ">chr1\n" )
        expFileHandler.write( "AAAAAAAA\n" )
        expFileHandler.write( ">chr2\n" )
        expFileHandler.write( "TTTTTT\n" )
        expFileHandler.close()
        
        obsFile = "dummyObsFile"
        
        cmd = "SpliceTEsFromGenome.py"
        cmd += " -i {}".format( coordFile )
        cmd += " -f {}".format( "map" )
        cmd += " -g {}".format( genomeFile )
        cmd += " -o {}".format( obsFile )
        cmd += " -v {}".format( 0 )
        returnStatus = subprocess.call(cmd, shell=True)
        
        self.assertTrue( returnStatus == 0 )
        self.assertTrue( FileUtils.are2FilesIdentical( expFile, obsFile ) )
        
        for f in [ coordFile, genomeFile, expFile, obsFile ]:
            os.remove( f )
        os.chdir( cDir )
        
if __name__ == "__main__":
        unittest.main()