import os
import time
from shutil import rmtree
import random
import unittest

from commons.tools.RMcat2path import RMcat2path
from commons.core.utils.FileUtils import FileUtils


class Test_F_RMcat2path(unittest.TestCase):

    def setUp(self):
        self._curTestDir = os.getcwd()
        self._testPrefix = 'test_F_RMc2p_{}_{}'.format(time.strftime("%H%M%S"), random.randint(0, 1000))
        try:
            os.makedirs(self._testPrefix)
        except:pass
        os.chdir(self._testPrefix)
        # self._workingDir = "test_{}".format(time.strftime("%Y%m%d%H%M%S"))
        # os.makedirs(self._workingDir)
        # os.chdir(self._workingDir)
        self.inputFile = "inputResultRMSSR.cat"
        self.expFileName = "expResultRMSSR.cat.path"
        self.obsFileName = "inputResultRMSSR.cat.path"

    def tearDown(self):
        os.chdir(self._curTestDir)
        try:
            rmtree(self._testPrefix)
        except:pass
        # os.chdir("..")
        # shutil.rmtree("{}/{}".format(os.getcwd(), self._workingDir))

    def test_RMcat2path_as_script(self):
        self._writeRMoutputFile_oldFormat(self.inputFile)
        self._writeExpFile_fromOldFormat(self.expFileName)
        cmd = "RMcat2path.py -i {}".format(self.inputFile)
        os.system(cmd)
        self.assertTrue(FileUtils.are2FilesIdentical(self.expFileName, self.obsFileName))

    def test_RMcat2path(self):
        self._writeRMoutputFile_oldFormat(self.inputFile)
        self._writeExpFile_fromOldFormat(self.expFileName)
        self.obsFileName = "ResultRMFileName.path"
        iRMC2P = RMcat2path(self.inputFile, self.obsFileName, 3)
        iRMC2P.run()
        self.assertTrue(FileUtils.are2FilesIdentical(self.expFileName, self.obsFileName))

    def test_RMcat2pathWithNewFormat_as_script(self):
        self._writeRMoutputFile_newFormat(self.inputFile)
        self._writeExpFile_fromNewFormat(self.expFileName)
        cmd = "RMcat2path.py -i {}".format(self.inputFile)
        os.system(cmd)
        self.assertTrue(FileUtils.are2FilesIdentical(self.expFileName, self.obsFileName))

    def test_RMcat2pathWithNewFormat(self):
        self._writeRMoutputFile_newFormat(self.inputFile)
        self._writeExpFile_fromNewFormat(self.expFileName)
        self.obsFileName = "ResultRMFileName.path"
        iRMC2P = RMcat2path(self.inputFile, self.obsFileName, 3)
        iRMC2P.run()
        self.assertTrue(FileUtils.are2FilesIdentical(self.expFileName, self.obsFileName))

    def _writeExpFile_fromOldFormat(self, fileName):
        f = open(fileName, "w")
        f.write("1\tchunk6\t1495\t1520\tAT_rich#Low_complexity\t165\t140\t0.0\t13\t50.0\n")
        f.write("2\tchunk6\t1512\t1537\tAT_rich#Low_complexity\t162\t137\t0.0\t13\t50.0\n")
        f.write("3\tchunk6\t4664\t4713\tAT_rich#Low_complexity\t132\t181\t0.0\t9\t18.0\n")
        f.write("4\tchunk6\t11779\t11803\tAT_rich#Low_complexity\t163\t139\t0.0\t15\t60.0\n")
        f.close()

    def _writeExpFile_fromNewFormat(self, fileName):
        f = open(fileName, "w")
        f.write("1\tchunk6\t1499\t1533\t(ATT)n#Simple_repeat\t1\t36\t0.0\t31\t90.26\n")
        f.write("2\tchunk6\t1500\t1534\t(TTATT)n#Simple_repeat\t1\t35\t0.0\t30\t86.66\n")
        f.write("3\tchunk6\t1656\t1732\t(AATAT)n#Simple_repeat\t1\t73\t0.0\t58\t75.76\n")
        f.write("4\tchunk6\t1855\t1918\t(GAACTTA)n#Simple_repeat\t1\t70\t0.0\t52\t81.55\n")
        f.close()

    def _writeRMoutputFile_oldFormat(self, fileName):
        f = open(fileName, "w")
        f.write("26 50.00 0.00 0.00 chunk6 1495 1520 (198480) C AT_rich#Low_complexity (135) 165 140 5\n")
        f.write("26 50.00 0.00 0.00 chunk6 1512 1537 (198463) C AT_rich#Low_complexity (138) 162 137 5\n")
        f.write("22 82.00 0.00 0.00 chunk6 4664 4713 (195287) AT_rich#Low_complexity 132 181 (119) 5\n")
        f.write("25 40.00 0.00 0.00 chunk6 11779 11803 (188197) C AT_rich#Low_complexity (137) 163 139 5\n")
        f.close()

    def _writeRMoutputFile_newFormat(self, fileName):
        f = open(fileName, "w")
        f.write("15 9.74 5.71 2.78 chunk6 1499 1533 (198467) (ATT)n#Simple_repeat 1 36 (0) m_b1s252i0\n")
        f.write("\n")
        f.write("  chunk6              1499 ATTATT-TTA-TATTATTATTATATATTTTTTTTTTT 1533\n")
        f.write("                                 -   -            -    v  v  v\n")
        f.write("  (ATT)n#Simple          1 ATTATTATTATTATTATTATTAT-TATTATTATTATT 36\n")
        f.write("\n")
        f.write("Matrix = Unknown\n")
        f.write("Transitions / transversions = 0.00 (0/3)\n")
        f.write("Gap_init rate = 0.09 (3 / 34), avg. gap size = 1.00 (3 / 3)\n")
        f.write("\n")
        f.write("14 13.34 2.86 2.86 chunk6 1500 1534 (198466) (TTATT)n#Simple_repeat 1 35 (0) m_b1s252i1\n")
        f.write("\n")
        f.write("  chunk6              1500 TTATTTTATATTATTATTA-TATATTTTTTTTTTTT 1534\n")
        f.write("                                    v     -   - v      v    v  \n")
        f.write("  (TTATT)n#Simp          1 TTATTTTATTTTATT-TTATTTTATTTTATTTTATT 35\n")
        f.write("\n")
        f.write("\n")
        f.write("Matrix = Unknown\n")
        f.write("Transitions / transversions = 0.00 (0/4)\n")
        f.write("Gap_init rate = 0.06 (2 / 34), avg. gap size = 1.00 (2 / 2)\n")
        f.write("\n")
        f.write("17 24.24 1.30 6.85 chunk6 1656 1732 (198268) (AATAT)n#Simple_repeat 1 73 (0) m_b1s252i2\n")
        f.write("\n")
        f.write("  chunk6              1656 AATATAATATAAAATAACTATATTA-AATATTACTGTTAGTAGGATCTAA 1704\n")
        f.write("                                       v    -    v  -    -  v -v  i  vi  v   \n")
        f.write("  (AATAT)n#Simp          1 AATATAATATAATATAA-TATAATATAATA-TAAT-ATAATATAATATAA 47\n")
        f.write("\n")
        f.write("  chunk6              1705 TCATAAAATAAAAAAAGGTAAAATCAAT 1732\n")
        f.write("                            -    v    v v  vi   v  -   \n")
        f.write("  (AATAT)n#Simp         48 T-ATAATATAATATAATATAATAT-AAT 73\n")
        f.write("\n")
        f.write("Matrix = Unknown\n")
        f.write("Transitions / transversions = 0.27 (3/11)\n")
        f.write("Gap_init rate = 0.08 (6 / 76), avg. gap size = 1.00 (6 / 6)\n")
        f.write("\n")
        f.write("13 18.45 10.94 1.43 chunk6 1855 1918 (198082) (GAACTTA)n#Simple_repeat 1 70 (0) m_b1s252i3\n")
        f.write("\n")
        f.write("  chunk6              1855 GAACTGAGAAC-T-GAAATAAGGAACTGAGAAC-TACTACTGAGAAC-T- 1899\n")
        f.write("                                v     - -   v v -     v     -  vv   v     - -\n")
        f.write("  (GAACTTA)n#Si          1 GAACTTAGAACTTAGAACTTA-GAACTTAGAACTTAGAACTTAGAACTTA 49\n")
        f.write("\n")
        f.write("  chunk6              1900 --ACTTCGAACTGAGAAGTTA 1918\n")
        f.write("                           --    v     v    v   \n")
        f.write("  (GAACTTA)n#Si         50 GAACTTAGAACTTAGAACTTA 70\n")
        f.write("\n")
        f.write("Matrix = Unknown\n")
        f.write("Transitions / transversions = 0.00 (0/10)\n")
        f.write("Gap_init rate = 0.11 (7 / 63), avg. gap size = 1.14 (8 / 7)\n")
        f.write("\n")
        f.close()


if __name__ == "__main__":
    unittest.main()
