import os
import time
import random
import shutil
import subprocess
import unittest
from commons.core.utils.FileUtils import FileUtils
from commons.core.sql.DbMySql import DbMySql
from commons.tools.GetSpecificTELibAccordingToAnnotation import GetSpecificTELibAccordingToAnnotation

class Test_F_GetSpecificTELibAccordingToAnnotation(unittest.TestCase):
    
    def setUp(self):
        self._curDir = os.getcwd()
        self._workDir = "GSTELATA_{}_{}".format(time.strftime("%H%M%S"), random.randint(0, 1000))
        if not os.path.exists(self._workDir):
            os.makedirs(self._workDir)
        os.chdir(self._workDir)
        self._expFileName_FullLengthCopy = "GiveInfoTeAnnotFile_ConsensusWithFullLengthCopy.txt"
        os.symlink("{}/Tools/{}".format(os.environ["REPET_DATA"], self._expFileName_FullLengthCopy), self._expFileName_FullLengthCopy)
        self._expFileName_Copy = "GiveInfoTeAnnotFile_ConsensusWithCopy.txt"
        os.symlink("{}/Tools/{}".format(os.environ["REPET_DATA"], self._expFileName_Copy), self._expFileName_Copy)
        self._expFileName_FullLengthFrag = "GiveInfoTeAnnotFile_ConsensusWithFullLengthFrag.txt"
        os.symlink("{}/Tools/{}".format(os.environ["REPET_DATA"], self._expFileName_FullLengthFrag), self._expFileName_FullLengthFrag)
        self._expFastaFileName_FullLengthCopy = "GiveInfoTeAnnotFile_ConsensusWithFullLengthCopy.fa"
        os.symlink("{}/Tools/{}".format(os.environ["REPET_DATA"], self._expFastaFileName_FullLengthCopy), self._expFastaFileName_FullLengthCopy)
        self._expFastaFileName_Copy = "GiveInfoTeAnnotFile_ConsensusWithCopy.fa"
        os.symlink("{}/Tools/{}".format(os.environ["REPET_DATA"], self._expFastaFileName_Copy), self._expFastaFileName_Copy)
        self._expFastaFileName_FullLengthFrag = "GiveInfoTeAnnotFile_ConsensusWithFullLengthFrag.fa"
        os.symlink("{}/Tools/{}".format(os.environ["REPET_DATA"], self._expFastaFileName_FullLengthFrag), self._expFastaFileName_FullLengthFrag)

        inputFastaFileName = "input_TEannot_refTEs.fa"
        os.symlink(self._expFastaFileName_Copy, inputFastaFileName)
        self._iDb = DbMySql()
        self._tableName = "Dummy_Atha_refTEs_seq"
        self._iDb.createTable(self._tableName, "seq", inputFastaFileName, True)
        os.remove(inputFastaFileName)      
        
        self._inFileName = "GiveInfoTeAnnotFile.txt"
        os.symlink("{}/Tools/{}".format(os.environ["REPET_DATA"], self._inFileName), self._inFileName )
        self._obsFileName_FullLengthCopy = "{}_FullLengthCopy.txt".format(os.path.splitext(self._inFileName)[0])
        self._obsFileName_Copy = "{}_OneCopyAndMore.txt".format(os.path.splitext(self._inFileName)[0])
        self._obsFileName_FullLengthFrag = "{}_FullLengthFrag.txt".format(os.path.splitext(self._inFileName)[0])
        self._obsFastaFileName_FullLengthCopy = "{}.fa".format(os.path.splitext(self._obsFileName_FullLengthCopy)[0])
        self._obsFastaFileName_Copy = "{}.fa".format(os.path.splitext(self._obsFileName_Copy)[0])
        self._obsFastaFileName_FullLengthFrag = "{}.fa".format(os.path.splitext(self._obsFileName_FullLengthFrag)[0])
        
    def tearDown(self):
        self._iDb.dropTable(self._tableName)
        self._iDb.close()
        os.chdir(self._curDir)
        try:
            shutil.rmtree(self._workDir)
        except:pass

    def test_run(self):
        iGetTELib = GetSpecificTELibAccordingToAnnotation(self._inFileName)
        iGetTELib.setTableName(self._tableName)
        iGetTELib.run()

        self.assertTrue(FileUtils.are2FilesIdentical(self._expFileName_FullLengthCopy, self._obsFileName_FullLengthCopy))
        self.assertTrue(FileUtils.are2FilesIdentical(self._expFileName_Copy, self._obsFileName_Copy))
        self.assertTrue(FileUtils.are2FilesIdentical(self._expFileName_FullLengthFrag, self._obsFileName_FullLengthFrag))
        self.assertTrue(FileUtils.are2FilesIdentical(self._expFastaFileName_FullLengthCopy, self._obsFastaFileName_FullLengthCopy))
        self.assertTrue(FileUtils.are2FilesIdentical(self._expFastaFileName_Copy, self._obsFastaFileName_Copy))
        self.assertTrue(FileUtils.are2FilesIdentical(self._expFastaFileName_FullLengthFrag, self._obsFastaFileName_FullLengthFrag))

    def test_run_as_script(self):
        cmd = "GetSpecificTELibAccordingToAnnotation.py -i {} -t {} -v 4".format(self._inFileName, self._tableName)
        subprocess.call(cmd, shell = True)

        self.assertTrue(FileUtils.are2FilesIdentical(self._expFileName_FullLengthCopy, self._obsFileName_FullLengthCopy))
        self.assertTrue(FileUtils.are2FilesIdentical(self._expFileName_Copy, self._obsFileName_Copy))
        self.assertTrue(FileUtils.are2FilesIdentical(self._expFileName_FullLengthFrag, self._obsFileName_FullLengthFrag))
        self.assertTrue(FileUtils.are2FilesIdentical(self._expFastaFileName_FullLengthCopy, self._obsFastaFileName_FullLengthCopy))
        self.assertTrue(FileUtils.are2FilesIdentical(self._expFastaFileName_Copy, self._obsFastaFileName_Copy))
        self.assertTrue(FileUtils.are2FilesIdentical(self._expFastaFileName_FullLengthFrag, self._obsFastaFileName_FullLengthFrag))

if __name__ == "__main__":
    unittest.main()