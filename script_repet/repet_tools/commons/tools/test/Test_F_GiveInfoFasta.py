import subprocess
import unittest
import os
import time
from shutil import rmtree
import random
from commons.tools.GiveInfoFasta import GiveInfoFasta
from commons.core.utils.FileUtils import FileUtils

class Test_F_GiveInfoFasta(unittest.TestCase):
    
    def setUp(self):
        self._curTestDir = os.getcwd()
        self._testPrefix = 'test_F_{}{}' .format(time.strftime("%H%M%S"), random.randint(0, 1000))
        self.inputFastaFile = "PreProcess.fa"
        self.outputStatFile = "{}.stats".format(self.inputFastaFile)
        
        try:
            os.makedirs(self._testPrefix)
        except:pass
        os.chdir(self._testPrefix) 
        subprocess.call("ln -s {}/Tools/{} {} ".format(os.environ["REPET_DATA"], self.inputFastaFile, self.inputFastaFile), shell=True)
                    
    def tearDown(self):
        os.chdir(self._curTestDir)
        try:
            rmtree(self._testPrefix)
        except:pass

    def test_GiveInfoFasta(self):

        iGIF = GiveInfoFasta(self.inputFastaFile, verbose = 5)
        iGIF.run()
        self._writeStatsFile("exp_stat_file")   
        self.assertTrue(FileUtils.are2FilesIdentical("exp_stat_file", self.outputStatFile))

    def test_GiveInfoFasta_inputFreeN(self):
         
        self._writeInputFastaFileWithoutN("PreProcess2.fa")     
        iGIF = GiveInfoFasta("PreProcess2.fa")
        iGIF.run()
        self._writeStatsFile2("exp_stat_file2")   
        self.assertTrue(FileUtils.are2FilesIdentical("exp_stat_file2", "PreProcess2.fa.stats"))


 
    def test_GiveInfoFasta_as_script(self):
        
        
        subprocess.call("GiveInfoFasta.py -i {} -v 4".format(self.inputFastaFile), shell=True)
        self._writeStatsFile("exp_stat_file")
         
        self.assertTrue(FileUtils.are2FilesIdentical("exp_stat_file", self.outputStatFile))
        
        
        
    def _writeStatsFile(self, fileName):
        with open(fileName, "w") as f:
            f.write("size of the bank (nb of sequences): 6\n")
            f.write("length of the bank (cumulative sequence lengths): 965 bp\n")
            f.write("mean sequence length: 161 bp (var=12816.57, sd=113.21, cv=0.70)\n")
            f.write("n=6 min=33.000 Q1=79.000 median=144.000 Q3=292.000 max=306.000\n")
            f.write("N50=2 L50=292nt N75=3 L75=144nt N90=5 L90=79nt\n")
            f.write("GC content: 471 (48.81%)\n")
            f.write("occurrences of A: 214 (22.18%)\n")
            f.write("occurrences of T: 260 (26.94%)\n")
            f.write("occurrences of G: 264 (27.36%)\n")
            f.write("occurrences of C: 207 (21.45%)\n")
            f.write("occurrences of N: 9 (0.93%)\n")
            f.write("occurrences of other: 11 (1.14%)\n")
            f.write("MD5 secure hash of the bank: e39712d327e304f1554026119ca79b62\n")


    def _writeStatsFile2(self, fileName):
        with open(fileName, "w") as f:
            f.write("size of the bank (nb of sequences): 6\n")
            f.write("length of the bank (cumulative sequence lengths): 492 bp\n")
            f.write("mean sequence length: 82 bp (var=0.00, sd=0.00, cv=0.00)\n")
            f.write("n=6 min=82.000 Q1=82.000 median=82.000 Q3=82.000 max=82.000\n")
            f.write("N50=3 L50=82nt N75=5 L75=82nt N90=6 L90=82nt\n")
            f.write("GC content: 258 (52.44%)\n")
            f.write("occurrences of A: 156 (31.71%)\n")
            f.write("occurrences of T: 78 (15.85%)\n")
            f.write("occurrences of G: 210 (42.68%)\n")
            f.write("occurrences of C: 48 (9.76%)\n")
            f.write("occurrences of N: 0 (0%)\n")
            f.write("MD5 secure hash of the bank: 9e2cc3db8b9b27a3aad57ecb47d26817\n")
        
    def _writeInputFastaFileWithoutN(self, fileName):
        with open(fileName, "w") as f:
            f.write(">chunk1\n")
            f.write("ATGCTAGTAGTAGTATATAGAGGATAGAGAGTGATAGAGGAGAGAGAGAGAGAGAGAGAAGGGCGCGCGCGCGCGCTGTGTG\n")
            f.write(">chunk2\n")
            f.write("ATGCTAGTAGTAGTATATAGAGGATAGAGAGTGATAGAGGAGAGAGAGAGAGAGAGAGAAGGGCGCGCGCGCGCGCTGTGTG\n")
            f.write(">chunk3\n")
            f.write("ATGCTAGTAGTAGTATATAGAGGATAGAGAGTGATAGAGGAGAGAGAGAGAGAGAGAGAAGGGCGCGCGCGCGCGCTGTGTG\n")
            f.write(">chunk4\n")
            f.write("ATGCTAGTAGTAGTATATAGAGGATAGAGAGTGATAGAGGAGAGAGAGAGAGAGAGAGAAGGGCGCGCGCGCGCGCTGTGTG\n")
            f.write(">chunk5\n")
            f.write("ATGCTAGTAGTAGTATATAGAGGATAGAGAGTGATAGAGGAGAGAGAGAGAGAGAGAGAAGGGCGCGCGCGCGCGCTGTGTG\n")
            f.write(">chunk6\n")
            f.write("ATGCTAGTAGTAGTATATAGAGGATAGAGAGTGATAGAGGAGAGAGAGAGAGAGAGAGAAGGGCGCGCGCGCGCGCTGTGTG\n")

        

if __name__ == "__main__":
    unittest.main()