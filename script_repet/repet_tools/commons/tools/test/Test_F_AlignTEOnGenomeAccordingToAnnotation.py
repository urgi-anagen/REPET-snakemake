import os
import time
import random
import shutil
import unittest
import subprocess
from commons.core.sql.DbFactory import DbFactory
from commons.core.utils.FileUtils import FileUtils
from commons.tools.AlignTEOnGenomeAccordingToAnnotation import AlignTEOnGenomeAccordingToAnnotation


class Test_F_AlignTEOnGenomeAccordingToAnnotation(unittest.TestCase):

    def setUp(self):
        self._workingDir = "ATOGATA_F_{}".format(time.strftime("%Y%m%d%H%M%S"))
        os.makedirs(self._workingDir)
        os.chdir(self._workingDir)
        self.uniqID = random.randint(1, 10000)
        self.pathFileName = "{}/Tools/DmelChr4_chr_allTEs_nr_noSSR_join_path.path".format(os.environ["REPET_DATA"])
        queryFileName = "{}/commons/DmelChr4.fa".format(os.environ["REPET_DATA"])
        subjectFileName = "{}/Tools/DmelChr4_refTEs.fa".format(os.environ["REPET_DATA"])
        self.queryTableName = "testDmelChr4_chr_seq_{}".format(self.uniqID)
        self.subjectTableName = "testDmelChr4_refTEs_seq_{}".format(self.uniqID)
        self.iDb = DbFactory.createInstance()
        self.iDb.createTable(self.queryTableName, "seq", queryFileName, True)
        self.iDb.createTable(self.subjectTableName, "seq", subjectFileName, True)
        self.iDb.close()

    def tearDown(self):
        self.iDb = DbFactory.createInstance()
        self.iDb.dropTable(self.queryTableName)
        self.iDb.dropTable(self.subjectTableName)
        self.iDb.dropTable(self.pathTableName)
        self.iDb.dropTable("{}_align".format(self.pathTableName))
        self.iDb.close()
        os.chdir("..")
        shutil.rmtree("{}/{}".format(os.getcwd(), self._workingDir))

    def test_run_merge_same_paths_reducedData(self):
        self.pathFileName = "{}/Tools/DmelChr4_chr_allTEs_nr_noSSR_join_path_reduceData.path".format(
            os.environ["REPET_DATA"])
        self.pathTableName = "testDmelChr4_chr_allTEs_nr_noSSR_join_path_reduceData"
        self.iDb = DbFactory.createInstance()
        self.iDb.createTable(self.pathTableName, "path", self.pathFileName, True)
        self.iDb.close()
        expFileName = "{}/Tools/exptestDmelChr4_chr_allTEs_nr_noSSR_join_path_reduceData_merge.alignedSeq".format(
            os.environ["REPET_DATA"])
        expTabFile = "expTabFile.tab"
        with open(expTabFile, "w") as f:
            f.write("PathId\talignLength\talignIdent\tscore\n")
            f.write("2\t251\t76.14\t436\n")
            f.write("40\t298\t81.89\t1494\n")
            f.write("338\t36\t93.94\t254\n")
            f.write("542\t995\t71.58\t3006\n")

        iATOGATA = AlignTEOnGenomeAccordingToAnnotation(self.pathTableName, self.queryTableName, self.subjectTableName, True)
        iATOGATA.run()
        self.obsFileName = "{}_align".format(self.pathTableName)
        self.assertTrue(FileUtils.are2FilesIdentical(expFileName, self.obsFileName))
        self.obsTabFile = "{}.tab".format(self.obsFileName)
        self.assertTrue(FileUtils.are2FilesIdentical(expTabFile, self.obsTabFile))

    def test_run_same_paths_reducedData(self):
        import filecmp
        self.pathFileName = "{}/Tools/DmelChr4_chr_allTEs_nr_noSSR_join_path_reduceData.path".format(
            os.environ["REPET_DATA"])
        self.pathTableName = "testDmelChr4_chr_allTEs_nr_noSSR_join_path_reduceData"
        self.iDb = DbFactory.createInstance()
        self.iDb.createTable(self.pathTableName, "path", self.pathFileName, True)
        self.iDb.close()
        expFileName = "{}/Tools/exptestDmelChr4_chr_allTEs_nr_noSSR_join_path_reduceData.alignedSeq".format(
            os.environ["REPET_DATA"])
        expTabFile = "expTabFile.tab"
        with open(expTabFile, "w") as f:
            f.write("PathId\talignLength\talignIdent\tscore\n")
            f.write("2\t327\t81.50\t1006\n")
            f.write("40\t624\t82.03\t1554\n")
            f.write("338\t36\t93.94\t254\n")
            f.write("542\t2581\t72.29\t3764\n")

        iATOGATA = AlignTEOnGenomeAccordingToAnnotation(self.pathTableName, self.queryTableName, self.subjectTableName, False)
        iATOGATA.run()
        self.obsFileName = "{}_align".format(self.pathTableName)
        self.assertTrue(FileUtils.are2FilesIdentical(expFileName, self.obsFileName))
        self.obsTabFile = "{}.tab".format(self.obsFileName)
        self.assertTrue(FileUtils.are2FilesIdentical(expTabFile, self.obsTabFile))

    def test_run_merge_same_paths(self):
        self.pathTableName = "testDmelChr4_chr_allTEs_nr_noSSR_join_path_{}".format(self.uniqID)
        self.iDb = DbFactory.createInstance()
        self.iDb.createTable(self.pathTableName, "path", self.pathFileName, True)
        self.iDb.close()

        expFileName = "{}/Tools/exptestDmelChr4_chr_allTEs_nr_noSSR_join_path_merge.alignedSeq".format(
            os.environ["REPET_DATA"])

        iATOGATA = AlignTEOnGenomeAccordingToAnnotation(self.pathTableName, self.queryTableName, self.subjectTableName,
                                                        True)
        iATOGATA.run()

        self.obsFileName = "{}_align".format(self.pathTableName)
        self.assertTrue(FileUtils.are2FilesIdentical(expFileName, self.obsFileName))
        expTabFile = "../data/expATOGATAtabFile_path_merge.tab"
        self.obsTabFile = "{}.tab".format(self.obsFileName)
        self.assertTrue(FileUtils.are2FilesIdentical(expTabFile, self.obsTabFile))

    def test_run_as_script_merge_same_paths(self):
        self.pathTableName = "testDmelChr4_chr_allTEs_nr_noSSR_join_path_{}".format(self.uniqID)
        self.iDb = DbFactory.createInstance()
        self.iDb.createTable(self.pathTableName, "path", self.pathFileName, True)
        self.iDb.close()

        expFileName = "{}/Tools/exptestDmelChr4_chr_allTEs_nr_noSSR_join_path_merge.alignedSeq".format(
            os.environ["REPET_DATA"])

        cmd = "AlignTEOnGenomeAccordingToAnnotation.py -p {} -q {} -s {} -m ".format(self.pathTableName,
                                                                                     self.queryTableName,
                                                                                     self.subjectTableName)
        process = subprocess.Popen(cmd, shell=True)
        process.communicate()
        self.obsFileName = "{}_align".format(self.pathTableName)
        self.assertTrue(FileUtils.are2FilesIdentical(expFileName, self.obsFileName))
        expTabFile = "../data/expATOGATAtabFile_path_merge.tab"
        self.obsTabFile = "{}.tab".format(self.obsFileName)
        self.assertTrue(FileUtils.are2FilesIdentical(expTabFile, self.obsTabFile))

    def test_run_as_script_without_merge(self):
        self.pathTableName = "testDmelChr4_chr_allTEs_nr_noSSR_join_path_{}".format(self.uniqID)
        self.iDb = DbFactory.createInstance()
        self.iDb.createTable(self.pathTableName, "path", self.pathFileName, True)
        self.iDb.close()

        expFileName = "{}/Tools/exptestDmelChr4_chr_allTEs_nr_noSSR_join_path.alignedSeq".format(
            os.environ["REPET_DATA"])

        cmd = "AlignTEOnGenomeAccordingToAnnotation.py -p {} -q {} -s {}".format(self.pathTableName,
                                                                                 self.queryTableName,
                                                                                 self.subjectTableName)
        process = subprocess.Popen(cmd, shell=True)
        process.communicate()
        self.obsFileName = "{}_align".format(self.pathTableName)
        self.assertTrue(FileUtils.are2FilesIdentical(expFileName, self.obsFileName))
        expTabFile = "../data/expATOGATAtabFile_path.tab"
        self.obsTabFile = "{}.tab".format(self.obsFileName)
        self.assertTrue(FileUtils.are2FilesIdentical(expTabFile, self.obsTabFile))


if __name__ == "__main__":
    unittest.main()
