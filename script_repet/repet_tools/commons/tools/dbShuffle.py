#!/usr/bin/env python

# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.


import os
import sys
import getopt
import argparse

from commons.core.seq.FastaUtils import FastaUtils


def dbShuffle(inData = "", outData = "", singularity_img="", verbose = 0):
    inData = inData
    outData = outData
    verbose = verbose

    if inData == "" or (not os.path.isfile(inData) and not os.path.isdir(inData)):
        msg = "ERROR: missing input file or directory (-i or -I)"
        sys.stderr.write("{}\n".format(msg))
        sys.exit(1)

    if outData == "":
        print("ERROR: missing name of output file or directory (-o or -O)")
        sys.exit(1)

    if verbose > 0:
        print("START DB shuffle")
        sys.stdout.flush()

    FastaUtils.dbShuffle(inData, outData,singularity_img, verbose)

    if verbose > 0:
        print("END DB shuffle")
        sys.stdout.flush()

    return 0




if __name__ == "__main__":
    # (self,fileType = 'path', outFileBaseName = '', allByAll=False, clean=True, verbose=0):
    parser = argparse.ArgumentParser(description="INPUT: use '-i' or '-I'",
                                     formatter_class=argparse.RawTextHelpFormatter, add_help=True)
    parser.add_argument('-i', '--file', default='', help="name of the input file (fasta format)")
    parser.add_argument('-o', '--outFile', default='',
                        help="name of the output file (use only with '-i')")
    parser.add_argument('-I', '--inputDirectory', default='',
                        help="name of the input directory (containing fasta files)\nOUTPUT: use '-o' or '-O'")
    parser.add_argument('-O', '--outputDirectory', default='',
                        help="name of the output directory (use only with '-I')\noutput file are: prefix of input fasta file + '_shuffle.fa')")
    parser.add_argument('-v', '--verbose', default=0, type=int, help="verbose (default=0/1/2)")
    
    args = parser.parse_args()
    input_file = args.file
    input_directory = args.inputDirectory
    output_file = args.outFile
    output_directory = args.outputDirectory
    verbose = args.verbose
    
    if input_file != "" and input_directory =="": 
        dbShuffle(inData = input_file, outData = output_file, verbose = verbose)
       
    elif input_file == "" and input_directory != "":
        dbShuffle(inData = input_directory, outData = output_directory, verbose = verbose)
        
    else : 
        msg = "ERROR: missing input file or directory (-i or -I) or using both"
        sys.stderr.write("{}\n".format(msg))
        sys.exit(1)
    

