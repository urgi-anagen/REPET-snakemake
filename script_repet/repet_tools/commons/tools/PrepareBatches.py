#!/usr/bin/env python

# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import os
import sys
import shutil

try:
    from ConfigParser import NoSectionError, NoOptionError
except ImportError:
    from configparser import NoOptionError, NoSectionError

from commons.core.checker.CheckerUtils import CheckerUtils
from commons.core.checker.CheckerException import CheckerException
from commons.core.utils.FileUtils import FileUtils
from commons.core.seq.FastaUtils import FastaUtils


class PrepareBatches(object):
    # Est ce qu'il faudrait enlever le clean ?
    def __init__(self, genomeFastaFileName, singularity_img, projectDir="", verbose=1, doClean="no", chunk_length=200000, chunk_overlap=10000, min_nb_seq_per_batch=5):
        self._projectDir = projectDir
        if os.path.exists(genomeFastaFileName):
            self.genomeFastaFileName = genomeFastaFileName
        else:
            print("Fasta file not found")
            sys.exit(1)
        self._verbose = verbose
        self.doClean = doClean
        self.chunk_length = chunk_length
        self.chunk_overlap = chunk_overlap
        self.min_nb_seq_per_batch = min_nb_seq_per_batch
        self.singularity_img = singularity_img


    def run(self):
        name = os.path.splitext(os.path.split(self.genomeFastaFileName)[1])[0]

        if self._verbose > 0:
            print("beginning of step 1")
            sys.stdout.flush()

        if self._projectDir == "":
            self._projectDir = "Prepare_batch"

        if not os.path.exists(self._projectDir):
            os.mkdir(self._projectDir)
             #print("ERROR: directory '{}' already exists".format(self._projectDir))
             #sys.exit(1)


        os.chdir(self._projectDir)
        if not os.path.exists(os.path.split(self.genomeFastaFileName)[1]):
            #os.symlink(self.genomeFastaFileName, os.path.split(self.genomeFastaFileName)[1])
            shutil.copy(self.genomeFastaFileName, os.path.split(self.genomeFastaFileName)[1])

        separator = "\n"
        with open(os.path.split(self.genomeFastaFileName)[1], "r") as inGenomeFileHandler:
            try:
                CheckerUtils.checkHeaders(inGenomeFileHandler)
            except CheckerException as e:
                print("Error in file {}. Wrong headers are :".format(self.genomeFastaFileName))
                print(separator.join(e.messages))
                print("Authorized characters are : a-z A-Z 0-9 - . : _\n")
                sys.exit(1)

        chunkFilePrefix = "{}_chunks".format(name)

        FastaUtils.dbChunks(os.path.split(self.genomeFastaFileName)[1], self.singularity_img, self.chunk_length, self.chunk_overlap, 11, chunkFilePrefix, self.doClean, self._verbose - 2)

        FastaUtils.splitFastaFileInBatches("{}.fa".format(chunkFilePrefix), self.min_nb_seq_per_batch * self.chunk_length)

        if self.doClean == "yes":
            FileUtils.removeFilesByPattern("{}.fa*".format(self._projectName))
            os.remove(os.path.split(self.genomeFastaFileName)[1])

        os.chdir("..")
        if self._verbose > 0:
            print("step 1 finished successfully")
            sys.stdout.flush()


#if __name__ == "__main__":
    #simg="/home/etbardet/REPET-snakemake/Singularity/te_finder_2.31.sif"
    #pb=PrepareBatches(genomeFastaFileName="/home/etbardet/Documents/REPET-snakemake/data/DmelChr4.fa",singularity_img = simg,projectDir="/home/etbardet/Documents/REPET-snakemake/TEdenovo_snakemake/results_TEdenovo_DmelChr4", chunk_length=200000, chunk_overlap=10000, min_nb_seq_per_batch=5, verbose=1)
    #pb.run()
