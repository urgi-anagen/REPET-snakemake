#!/usr/bin/env python

# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import os
from commons.core.LoggerFactory import LoggerFactory
from commons.core.utils.RepetOptionParser import RepetOptionParser

LOG_DEPTH = "repet.tools"

class ShortenConsensusHeader(object):

    def __init__(self, fastaFileName="", consensusFileName="", projectName="", selfAlign="blaster", clustering="",
                 multAlign="", pyramidHeader=False, verbosity=0, log_name="ShortenConsensusHeader.log"):
        self._fastaFileName = fastaFileName
        self.setOutFileName(consensusFileName)
        self._projectName = projectName
        self._selfAlign = selfAlign
        self._clustering = clustering
        self._multAlign = multAlign
        self._pyramidHeader = pyramidHeader
        self._verbosity = verbosity
        self._log = LoggerFactory.createLogger("{}.{}".format((LOG_DEPTH, self.__class__.__name__), self._verbosity),
                                               log_name)
        # LoggerFactory.createLogger("{}.{}".format(LOG_DEPTH, self.__class__.__name__), self._verbosity)

    def setAttributesFromCmdLine(self):
        usage = "ShortenConsensusHeader [options]"
        description = "Tool to shorten consensus headers from TEdenovo or SATannot.\n"
        parser = RepetOptionParser(description=description, usage=usage)
        parser.add_option("-i", "--fasta", dest="fastaFileName", action="store", type="string",
                          help="input fasta file name [compulsory] [format: fasta]", default="")
        parser.add_option("-o", "--out", dest="consensusFileName", action="store", type="string",
                          help="output file name [default=inFileName+'.shortH']", default="")
        parser.add_option("-p", "--project", dest="projectName", action="store", type="string", help="project name ",
                          default="")
        parser.add_option("-s", "--selfAlign", dest="selfAlign", action="store", type="string",
                          help="self-alignment program (Blaster/Pals) [default=blaster]", default="blaster")
        parser.add_option("-c", "--clustering", dest="clustering", action="store", type="string",
                          help="clustering method (Grouper/Recon/Piler)[default=]", default="")
        parser.add_option("-m", "--multAlign", dest="multAlign", action="store", type="string",
                          help="MSA program (Map/Mafft/Muscle/Clustal/Tcoffee/Prank/...)[default=]", default="")
        parser.add_option("-H", "--pyramidHeader", dest="pyramidHeader", action="store_true",
                          help="format the header with pyramid and piles informations (SATannot)[default=]",
                          default=False)
        parser.add_option("-v", "--verbosity", dest="verbosity", action="store", type="int",
                          help="verbosity [optional] [default: 1]", default=1)
        options = parser.parse_args()[0]
        self._setAttributesFromOptions(options)

    def _setAttributesFromOptions(self, options):
        self._fastaFileName = options.fastaFileName
        self.setOutFileName(options.consensusFileName)
        self._projectName = options.projectName
        self._selfAlign = options.selfAlign
        self._clustering = options.clustering
        self._multAlign = options.multAlign
        self._pyramidHeader = options.pyramidHeader
        self.setVerbosity(options.verbosity)

    def setOutFileName(self, consensusFileName):
        if consensusFileName == "":
            self._consensusFileName = "{}.shortH".format(self._fastaFileName)
        else:
            self._consensusFileName = consensusFileName

    def setVerbosity(self, verbosity):
        self._verbosity = verbosity

    def _checkOptions(self):
        if self._fastaFileName == "":
            self._logAndRaise("ERROR: Missing input fasta file name")

    def _logAndRaise(self, errorMsg):
        self._log.error(errorMsg)
        raise Exception(errorMsg)

    def shortenConsensusName(self, consensusName):
        desc = consensusName.split(self._projectName)[1]
        palignMeth = desc.split("_")[1]
        clustMeth = desc.split("_")[2]
        clustID = desc.split("_")[3]
        lmalignMeth = desc.split("_")[4:]
        if len(lmalignMeth) > 2:
            malignMeth = "{}{}_{}".format(lmalignMeth[0], lmalignMeth[1], lmalignMeth[2])
        else:
            malignMeth = "".join(lmalignMeth)
        consensusShorten = "{}-{}-{}{}-{}".format(self._projectName, palignMeth[0], clustMeth[0], clustID, malignMeth)

        return consensusShorten

    def renameHeaderInConsensusFastaFile(self, fileName=""):
        newFileName = fileName.split(".")[0] + "New.fa"

        oldFile = open(fileName, "r")
        newFile = open(newFileName, "w")

        inputLine = oldFile.readline()
        while inputLine != "":
            if ">" in inputLine:
                consensusName = inputLine.lstrip('>')
                outputLine = ">{}".format(self.shortenConsensusName(consensusName))
                newFile.write(outputLine)
            else:
                newFile.write(inputLine)

            inputLine = oldFile.readline()

        oldFile.close()
        newFile.close()

    def run(self):
        LoggerFactory.setLevel(self._log, self._verbosity)
        self._checkOptions()
        self._log.info("START ShortenConsensusHeader")
        self._log.debug("Fasta file name: {}".format(self._fastaFileName))

        consensusFile = open(self._consensusFileName, "w")
        indexFile = open(os.path.join(os.path.split(self._consensusFileName)[0], "indexConsensus.txt"), "w")
        inFile = open(self._fastaFileName, "r")
        lines = inFile.readlines()

        countSeq = 0
        for line in lines:
            if ">" in line:
                countSeq += 1
                data = line.split()
                cons_name = data[0].split("=")[1]
                cons_nbAlign = data[2].split("=")[1]
                if self._projectName != "":
                    if self._pyramidHeader:
                        cluster_id = data[3].split("=")[1]
                        pyramid_id = data[4].split("=")[1]
                        # TODO: option "header SATannot" => autre nom pour indiquer "PilerTA" ?
                        longConsensusName = ">{}_{}_{}_{}_{}_{}_{}\n".format(self._projectName, self._selfAlign,
                                                                             self._clustering, cluster_id, pyramid_id,
                                                                             self._multAlign, cons_nbAlign)
                    else:
                        cluster_id = cons_name.split("Cluster")[1]
                        cluster_id = cluster_id.split(".fa")[0]
                        longConsensusName = ">{}_{}_{}_{}_{}_{}\n".format(self._projectName, self._selfAlign,
                                                                          self._clustering, cluster_id, self._multAlign,
                                                                          cons_nbAlign)
                    #                    iC = Classif(projectName = self._projectName)
                    #                    iC.setConsensusName(longConsensusName)
                    shortConsensusName = self.shortenConsensusName(longConsensusName)
                    consensusFile.write(">" + shortConsensusName)
                    indexFile.write("{}\t{}".format(shortConsensusName.strip(), longConsensusName[1:]))
                else:
                    consensusFile.write(">{}_nbAlign{}\n".format(cons_name, cons_nbAlign))
            else:
                seq = line.split("\n")
                consensusFile.write(seq[0] + "\n")

        inFile.close()
        consensusFile.close()
        indexFile.close()

        self._log.debug("{} consensus in {}".format(countSeq, self._fastaFileName))
        self._log.info("END ShortenConsensusHeader")


if __name__ == "__main__":
    iShortenConsensusHeader = ShortenConsensusHeader()
    iShortenConsensusHeader.setAttributesFromCmdLine()
    iShortenConsensusHeader.run()
