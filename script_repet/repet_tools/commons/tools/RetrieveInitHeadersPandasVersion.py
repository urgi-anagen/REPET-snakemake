#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  3 11:33:02 2022

@author: mwan
"""

import pandas 
import os
import argparse

class RetrieveInitHeadersPandasVersion(object):

    def __init__(self, link_file="",path_file="",output_file="", verbose = 0):
        self._link_file = link_file
        self._path_file = path_file
        
        if output_file == "":
            directory = os.path.split(path_file)[0]
            name_file = "{}_chk_allTEs.path".format(os.path.splitext(os.path.split(path_file)[1])[0])
            self._output_file = os.path.join(directory, name_file)
        else:
            self._output_file = output_file
        
        self._verbose = verbose
        
    def run(self):
        if self._verbose > 0:
            print("START RetrieveInitHeaders.py")
        
        data_link = dict()
        
        if self._verbose > 0:
            print("read '{}' file".format(self._link_file))
        with open(self._link_file, "r") as f_link :
            for line in f_link.readlines() : 
                split_line = line.split("\t")
                data_link[split_line[0]] = split_line[1]
        

        path_data = pandas.read_csv(self._path_file, sep = '\t',header=None)
        rename_path_data = path_data.replace({4:data_link})
        if self._verbose > 0:
            print("create new file '{}' file".format(self._output_file))
        rename_path_data.to_csv(self._output_file,index=False,sep="\t",header=False)
        
        if self._verbose > 0:
            print("END RetrieveInitHeaders.py")
        
        
 


if __name__ == '__main__':
    usage = "RetrieveInitHeaders.py [ options ]"
    description = "Rename sequences shorted header with their initial header"
    parser =  argparse.ArgumentParser (description = description, usage=usage,formatter_class=argparse.RawTextHelpFormatter, add_help=True)
    parser.add_argument("-p", "--path_file",required=True, dest = "path_file", help = "", default = "")
    parser.add_argument("-l", "--link_file", required=True, dest = "link_file", help = "", default = "")
    parser.add_argument("-o", "--output", dest = "output", help = "Output file name", default = "")
    parser.add_argument("-v", "--verbose", dest = "verbose", type = int, help = "verbose : 0 or 1", default = 0)
    args = parser.parse_args()
    
    link_file = (args.link_file)
    path_file =  (args.path_file)
    output_file = (args.output)
    verbose = (args.verbose)
    iRetrieveInitHeaders = RetrieveInitHeadersPandasVersion(link_file,path_file,output_file,verbose)
    iRetrieveInitHeaders.run()
    
    
