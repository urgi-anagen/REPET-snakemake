#!/usr/bin/env python

# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

from commons.core.seq.BioseqDB import BioseqDB
from commons.core.utils.FileUtils import FileUtils

from commons.core.utils.RepetOptionParser import RepetOptionParser
from commons.core.LoggerFactory import LoggerFactory

from commons.core.stat.Stat import Stat

LOG_DEPTH = "repet.commons.tools"
LOG_FORMAT = "%(message)s"

class GiveInfoFasta(object):
    def __init__(self, inFileName = "", giveStatsPerSeq = False, outFileName = "", verbose = 3, log_name = "GiveInfoFasta.log"):
        self._inFileName = inFileName
        self._giveStatsPerSeq = giveStatsPerSeq
        self._outFileName = outFileName
        self._verbose = verbose
        self._log = LoggerFactory.createLogger("{}.{}".format((LOG_DEPTH, self.__class__.__name__), self._verbose),
                                               log_name)
        self._dNt2Count = {}
        self._nbOtherNt = 0
        
    def setAttributesFromCmdLine(self):
        usage = "GiveInfoFasta.py [ options ]"
        description = "This program outputs some summary statistics on the input fasta file."

        parser = RepetOptionParser(description = description, usage = usage)
        parser.add_option("-i", "--input",          dest = "inFileName",     action = "store",       type = "string", help = "name of the input file (format='fasta')", default = "")
        parser.add_option("-l", "--sequence",          dest = "giveStatsPerSeq",  action = "store_true",  help = "give stats for each input sequence", default = False)
        parser.add_option("-v", "--verbose",      dest = "verbose",         action = "store",       type = "int",    help = "verbosity [optional] [default: 3]", default = 3)
        parser.add_option("-o", "--output",          dest = "outFileName",     action = "store",       type = "string", help = "name of the output file (default=inFileName+.'stats')", default = "")
        
        options = parser.parse_args()[0]
        self._setAttributesFromOptions(options)

    def _setAttributesFromOptions(self, options):
        self.setInFileName(options.inFileName)
        self.setOutFileName(options.outFileName)
        self.setGiveStatsPerSeq(options.giveStatsPerSeq)
        self.setVerbose(options.verbose)
        

    def setInFileName(self, inFileName):
        self._inFileName = inFileName

    def setOutFileName(self, outFileName):
        self._outFileName = outFileName

    def setGiveStatsPerSeq(self, giveStatsPerSeq):
        self._giveStatsPerSeq = giveStatsPerSeq
       
    def setVerbose(self, verbose):
        self._verbose = verbose
        
    def getdNt2Count(self):
        return self._dNt2Count
    
    def getnbOtherNt(self):
        return self._nbOtherNt
            
    def _checkOptions(self):
        if self._outFileName == "":
            self._outFileName = "{}.stats".format(self._inFileName)
      
        if  self._inFileName == "" :
            self._logAndRaise("ERROR: missing input file name (-i)")

        if  not FileUtils.isRessourceExists(self._inFileName):
            self._logAndRaise("ERROR: fasta file '{}' doesn't exist".format(self._inFileName) )

            
    def _logAndRaise(self, errorMsg):
        self._log.error(errorMsg)
        raise Exception(errorMsg)


    def run(self):
        LoggerFactory.setLevel(self._log, self._verbose)
        self._checkOptions()
        self._log.info("START GiveInfoFasta on {}".format(self._inFileName))
        
        self._log.info( "load the fasta file...")
        bsDB = BioseqDB(self._inFileName)
        #outFile = open( self._outFileName, "w" )
        with open (self._outFileName, 'w') as outFile:



            iStat = Stat()

            for bs in bsDB.db:
                iStat.add( bs.getLength() )
                if self._giveStatsPerSeq:
                    StatLine = "sequence '{}': {} bp, GC={:.2f}% N={:.2f}%".format( bs.header,
                                                                            bs.getLength(),
                                                                            bs.getGCpercentage(),
                                                                            100 * bs.propNt("N") )
                    outFile.write( "{}\n".format( StatLine ) )
                    self._log.debug(StatLine)

            iStat._sum = iStat.getSum()

            StatLine =  "size of the bank (nb of sequences): {}".format( iStat.getValuesNumber() )
            outFile.write( "{}\n".format( StatLine ) )
            self._log.debug(StatLine)
            if iStat._sum > 1000000:
                genomeSizeMega = (iStat._sum)/1000000
                StatLine = "length of the bank (cumulative sequence lengths): {} bp ({} Mb)".format( iStat._sum, genomeSizeMega)
            else:
                StatLine = "length of the bank (cumulative sequence lengths): {:.0f} bp".format  (iStat._sum)
            outFile.write( "{}\n".format( StatLine ) )

            self._log.debug(StatLine)

            StatLine = "mean sequence length: {:.0f} bp (var={:.2f}, sd={:.2f}, cv={:.2f})".format( iStat.mean(), iStat.var(), iStat.sd(), iStat.cv() )
            outFile.write( "{}\n".format( StatLine ) )
            self._log.debug(StatLine)

            StatLine = iStat.stringQuantiles()
            outFile.write( "{}\n".format( StatLine ) )
            self._log.debug(StatLine)

            N90, L90 = iStat.NandLperc(0.90)
            N75, L75 = iStat.NandLperc(0.75)
            N50, L50 = iStat.NandLperc(0.50)
            StatLine = "N50={} L50={}nt N75={} L75={}nt N90={} L90={}nt"  .format(N50, L50, N75, L75, N90, L90)
            outFile.write( "{}\n".format( StatLine ) )
            self._log.debug(StatLine)

            bsDB.upCase()
            self._dNt2Count = bsDB.countAllDifferentNt()
            StatLine = "GC content: {} ({:.2f}%)".format(( self._dNt2Count["G"] + self._dNt2Count["C"] ), (100 * ( self._dNt2Count["G"] + self._dNt2Count["C"] ) / float(iStat._sum)) )
            outFile.write( "{}\n".format( StatLine ) )
            self._log.debug(StatLine)
    #        compteurOther = 0
            dNt2StatLine = {}
            for cle in self._dNt2Count.keys():
                if cle not in ["A","T","G","C","N"]:
                    self._nbOtherNt += self._dNt2Count[cle]
                else:
                    dNt2StatLine[cle] = "occurrences of {}: {} ({:.2f}%)".format( cle, self._dNt2Count[cle], 100*self._dNt2Count[cle]/float(iStat._sum) )
            dNt2StatLine["other"] = "occurrences of other: {} ({:.2f}%)".format( self._nbOtherNt, 100*self._nbOtherNt/float(iStat._sum) )

            outFile.write( "{}\n".format( dNt2StatLine["A"] ) )
            outFile.write( "{}\n".format( dNt2StatLine["T"] ) )
            outFile.write( "{}\n".format( dNt2StatLine["G"] ) )
            outFile.write( "{}\n".format( dNt2StatLine["C"] ) )

    #        if  self._dNt2Count.has_key("N"):
            if "N" in self._dNt2Count:
                outFile.write( "{}\n".format( dNt2StatLine["N"] ) )
            else:
                outFile.write( "occurrences of N: 0 (0%)\n" )

            if self._nbOtherNt != 0:
                outFile.write( "{}\n".format( dNt2StatLine["other"] ) )


            try:
                md5sum = FileUtils.getMd5SecureHash(self._inFileName)
                StatLine = "MD5 secure hash of the bank: {}".format( md5sum )
                outFile.write( "{}\n".format( StatLine ) )
                self._log.debug(StatLine)
            except:
                pass

            self._log.info("END GiveInfoFasta")

if __name__ == "__main__":
    iGIF = GiveInfoFasta()
    iGIF.setAttributesFromCmdLine()
    iGIF.run()