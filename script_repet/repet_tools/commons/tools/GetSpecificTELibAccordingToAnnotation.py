#!/usr/bin/env python

# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.


import os
import sys
import pandas
import logging

from commons.core.utils.RepetOptionParser import RepetOptionParser
from commons.core.utils.FileUtils import FileUtils

from commons.core.LoggerFactory import LoggerFactory
from commons.core.seq.Bioseq import Bioseq
from commons.core.seq.FastaUtils import FastaUtils

LOG_DEPTH = "repet.tools"
LOG_FORMAT = "%(message)s"


# TODO: use configuration file

## Get 3 annotation files, using output from TEannot:
# - consensus with one or more full length copy,
# - consensus with one or more full length fragment,
# - consensus without copy

class GetSpecificTELibAccordingToAnnotation(object):

    def __init__(self, fasta_file="", table_name="", verbose=2, work_dir="", log_name ="GetSpecificTELibAccordingToAnnotation.log"):
        self._table_name = table_name
        self._fasta_file = fasta_file
        self._verbose = verbose
        self._consensus_fasta = None
        self._work_dir = work_dir
        #self._log = LoggerFactory.createLogger("{}.{}".format(LOG_DEPTH, self.__class__.__name__), verbosity=self._verbose, format=LOG_FORMAT)
        self._log = LoggerFactory.createLogger("{}.{}".format((LOG_DEPTH, self.__class__.__name__), self._verbose), log_file=log_name)

        

    def setAttributesFromCmdLine(self):
        usage = "GetSpecificTELibAccordingToAnnotation.py [options]"
        desc = "Splits '<projectName>.annotStatsPerTE.tab' output file from PostAnalyzeTElib.py using -a 3 option,"
        desc += "in 3 subfiles containing consensus which have at least one copy, one full length fragment or one full length copy.\n"
        desc += "A TEs library (in fasta format) is built according to each category. Connection to the database parameters are retrieved from the environment\n"

        examples = "\nExample : with a project called \"MyTEannotAnalysis\":\n"
        examples += "\t$ GetSpecificTELibAccordingToAnnotation.py  -f <project_name>_refTEs.fa -t <project_name>_chr_allTEs_nr_noSSR_join_path.annotStatsPerTE.tab\n"

        parser = RepetOptionParser(description=desc, epilog=examples, usage=usage)
        parser.add_option("-f", "--fasta_file", dest="fasta_file", action="store", type="string",
                          help="fasta with reference library",
                          default="")
        parser.add_option("-t", "--table", dest="table_name", action="store", type="string",
                          help="input file (mandatory) = output file from PostAnalyzeTElib.py (e.g. <project_name>_chr_allTEs_nr_noSSR_join_path.annotStatsPerTE.tab)",
                          default="")
        parser.add_option("-v", "--verbose", dest="verbose", action="store", type="int",
                          help="verbosity level (default=0, else 1)", default=0)
        (options, args) = parser.parse_args()
        self._setAttributesFromOptions(options)

    def _setAttributesFromOptions(self, options):
        self.setfastaFile(options.fasta_file)
        self.setTableName(options.table)
        self.setVerbose(options.verbose)

    def setTableName(self, fasta_file):
        self._fasta_file = fasta_file

    def setfastaFile(self, table_name):
        self._table_name = table_name

    def setVerbose(self, verbose):
        self._verbose = verbose

    def checkOptions(self):
        if self._table_name != "":
            if not FileUtils.isRessourceExists(self._table_name):
                self._logAndRaise("ERROR: {} file does not exist!".format(self._table_name))
        else:
            self._logAndRaise("ERROR: No specified -i option!")

        if self._fasta_file != "":
            if not os.path.exists(self._fasta_file) : 
                self._logAndRaise("ERROR: fasta file does not exist!")
            else :
                self._consensus_fasta = FastaUtils.read_fasta(self._fasta_file)
        else:
            self._logAndRaise("ERROR: No specified -t option!")

    def _logAndRaise(self, errorMsg):
        self._log.error(errorMsg)
        raise Exception(errorMsg)

    def writeFastaFileFromGiveInfoTEAnnot(self, fileName):
        
        def getBioseqFromHeader(header ):
            if "'" in header:
                newHeader=header.replace("'","\\'")
            else:
                newHeader=header
            return Bioseq( newHeader, self._consensus_fasta[newHeader])
        
        def saveAccessionsListInFastaFile(lAccessions, outFileName ):
            #print(outFileName)
            with open( outFileName, "w" ) as outFile:
                for ac in lAccessions:
                    bs = getBioseqFromHeader( ac )
                    bs.write(outFile)
                    
        
        df_stat = pandas.read_csv(fileName, sep='\t',index_col=None,keep_default_na=True,na_filter = False)
        lConsensusName = df_stat.TE.tolist()
        #if self._work_dir == "" : 
        outPutFileName = "{}.fa".format(os.path.splitext(fileName)[0])
        #else : 
        #    outPutFileName = os.path.join(self._work_dir,"{}.fa".format(os.path.splitext(fileName)[0]))
        saveAccessionsListInFastaFile(lConsensusName, outPutFileName)


    def run(self):
        self.checkOptions()
        LoggerFactory.setLevel(self._log, self._verbose)
        if self._work_dir == "":
            outInfoFileNameFullCopy = "{}_FullLengthCopy.txt".format(
                os.path.splitext(os.path.basename(self._table_name))[0])
            outInfoFileNameCopy = "{}_OneCopyAndMore.txt".format(
                os.path.splitext(os.path.basename(self._table_name))[0])
            outInfoFileNameFullFrag = "{}_FullLengthFrag.txt".format(
                os.path.splitext(os.path.basename(self._table_name))[0])
        else : 
            
            outInfoFileNameFullCopy = os.path.join(self._work_dir,"{}_FullLengthCopy.txt".format(
                os.path.splitext(os.path.basename(self._table_name))[0]))
            outInfoFileNameCopy = os.path.join(self._work_dir,"{}_OneCopyAndMore.txt".format(
                os.path.splitext(os.path.basename(self._table_name))[0]))
            outInfoFileNameFullFrag = os.path.join(self._work_dir,"{}_FullLengthFrag.txt".format(
                os.path.splitext(os.path.basename(self._table_name))[0]))
        
        with open(outInfoFileNameFullCopy, "w") as outInfoFileFullCopy, open(outInfoFileNameCopy, "w") as outInfoFileCopy, \
                open(outInfoFileNameFullFrag, "w") as outInfoFileFullFrag:

            self._log.info("START GetSpecificTELibAccordingToAnnotation\n input info file: {}".format(self._table_name))

            with open(self._table_name, "r") as inFileFh :
                line = inFileFh.readline()
               
                lHeaders = line.split()
                if "fullLgthCopies" not in lHeaders:
                    self._logAndRaise("ERROR: No headers in {}!".format(self._table_name))
                outInfoFileFullCopy.write(line)
                outInfoFileCopy.write(line)
                outInfoFileFullFrag.write(line)

                line = inFileFh.readline()
                while line:
                    dTokens = {}
                    for index, token in enumerate(line.split()):
                        dTokens[lHeaders[index]] = token

                    if int(dTokens["fullLgthCopies"]) > 0:
                        outInfoFileFullCopy.write(line)
                    if int(dTokens["copies"]) > 0:
                        outInfoFileCopy.write(line)
                    if int(dTokens["fullLgthFrags"]) > 0:
                        outInfoFileFullFrag.write(line)
                    line = inFileFh.readline()


        self.writeFastaFileFromGiveInfoTEAnnot(outInfoFileNameFullCopy)
        self.writeFastaFileFromGiveInfoTEAnnot(outInfoFileNameCopy)
        self.writeFastaFileFromGiveInfoTEAnnot(outInfoFileNameFullFrag)

        self._log.info("END GetSpecificTELibAccordingToAnnotation\n")


if __name__ == '__main__':
    #consensus_fasta_file = "/home/mwan/REPET/REPET-snakemake/results_DmelChr4_repet/DmelChr4_refTEs.fa"
    #stat_tab ="/home/mwan/REPET/REPET-snakemake/results_DmelChr4_repet/DmelChr4_TEdetect/DmelChr4_chr_allTEs_nr_noSSR_join_path.annotStatsPerTE.tab"
    
    #iGetTELib = GetSpecificTELibAccordingToAnnotation(fasta_file=consensus_fasta_file, table_name=stat_tab, verbose=0, work_dir="/home/mwan/REPET/REPET-snakemake/")
    iGetTELib = GetSpecificTELibAccordingToAnnotation()
    iGetTELib.setAttributesFromCmdLine()
    iGetTELib.run()
