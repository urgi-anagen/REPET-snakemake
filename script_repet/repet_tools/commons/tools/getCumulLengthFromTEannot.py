#!/usr/bin/env python

# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

##@file
# usage: get_cumul_lengthFromTEannot.py [ options ]
# options:
#      -h: this help
#      -i: table with the annotations (format=path)
#      -r: name of a TE reference sequence (if empty, all subjects are considered)
#      -g: length of the genome (in bp)
#      -C: configuration file
#      -c: clean
#      -v: verbosity level (default=0/1)
import subprocess
import sys
import os
from commons.core.coord.Path import Path
import pandas


class getCumulLengthFromTEannot(object):
    """
    Give the cumulative length of TE annotations (subjects mapped on queries).
    """

    def __init__(self, join_path_file="", TE_refseq="", genome_length=0, clean=False, verbose=0, singularity_img=""):
        """
        Constructor.
        """
        self._join_path_file = join_path_file
        self._df_path = None
        self._TE_ref_seq = TE_refseq
        self._genome_length = genome_length
        self._clean = clean
        self._verbose = verbose
        self._dict_cumul_length_by_query = {}
        self._singularity_img = singularity_img
        self.check_param()

    def check_param(self):
        if os.path.exists(self._join_path_file):
            self._df_path = pandas.read_csv(self._join_path_file, sep='\t', header=None, index_col=0, low_memory=False)
        else:
            print("File does not exist")
            sys.exit(1)

    def get_all_subjects_as_map_of_queries(self):

        def get_path_list_from_id(df_join_path, path_num):
            list_paths = list()
            tmp_df = df_join_path.loc[path_num]
            if tmp_df.shape == (9,):
                p = Path()
                p.setFromTuple([tmp_df.name] + tmp_df.to_list())
                list_paths.append(p)
            else:
                for index, row in tmp_df.iterrows():
                    p = Path()
                    p.setFromTuple([index] + list(row))
                    list_paths.append(p)
            return list_paths

        map_file_name = "{}.map".format(self._join_path_file)
        with open(map_file_name, "w") as mapFile:
            if self._TE_ref_seq != "":
                list_path_nums = set(self._df_path.loc[self._df_path[4] == self._TE_ref_seq].index)
                # list_path_nums = self._tpA.getIdListFromSubject(self._TE_ref_seq)
            else:
                list_path_nums = list(self._df_path.index)
            if self._verbose > 0:
                print("nb of paths: {}".format(len(list_path_nums)))
            for path_num in list_path_nums:
                list_paths = get_path_list_from_id(self._df_path, path_num)
                for path in list_paths:
                    map = path.getSubjectAsMapOfQuery()
                    map.write(mapFile)
        return map_file_name

    def merge_ranges(self, map_file_name):
        merge_file_name = "{}.merge".format((map_file_name))
        path_tmp = os.getcwd()
        os.chdir(os.path.split(os.path.abspath(self._join_path_file))[0])
        if self._singularity_img != "":
            prg = "singularity exec {} mapOp".format(self._singularity_img)
        else:
            prg = "mapOp"
        cmd = prg
        cmd += " -q {}".format(os.path.join(path_tmp, map_file_name))
        cmd += " -m"
        cmd += " 2>&1 > /dev/null"
        log = subprocess.call(cmd, shell=True)
        if log != 0:
            print("*** Error: {} returned {}".format(prg, log))
            sys.exit(1)
        if self._clean:
            os.remove(os.path.join(path_tmp, map_file_name))
        os.chdir(path_tmp)
        return os.path.abspath(merge_file_name)

    def get_cumul_length(self, merge_file_name):
        with open(merge_file_name, "r") as mergeFile:
            for line in mergeFile:
                tok = line.split("\t")
                subject_name = tok[1]
                if not subject_name in self._dict_cumul_length_by_query:
                    self._dict_cumul_length_by_query[subject_name] = 0
                self._dict_cumul_length_by_query[subject_name] += abs(int(tok[3]) - int(tok[2])) + 1

        total_coverage = sum(self._dict_cumul_length_by_query.values())

        if self._clean:
            os.remove(merge_file_name)

        return total_coverage

    def start(self):
        """
        Useful commands before running the program.
        """
        self.check_param()
        if self._verbose > 0:
            print("START {}".format(type(self).__name__))
            sys.stdout.flush()

    def end(self):
        """
        Useful commands before ending the program.
        """
        if self._verbose > 0:
            print("END {}".format(type(self).__name__))
            sys.stdout.flush()

    def run(self):
        """
        Run the program.
        """
        self.start()
        map_file_name = self.get_all_subjects_as_map_of_queries()
        merge_file_name = self.merge_ranges(map_file_name)
        total = self.get_cumul_length(merge_file_name)
        print("cumulative length: {} bp".format(total))
        if self._genome_length > 0:
            print("TE content: {:.2f}".format(100 * total / float(self._genome_length)))

        self.end()


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='usage: get_cumul_lengthFromTEannot.py [ options ]',
                                     formatter_class=argparse.RawTextHelpFormatter, add_help=True)
    parser.add_argument('-p', '--path_file', required=True, help="tsv with the annotations (format=path)")
    parser.add_argument('-ref', '--te_ref', default="",
                        help="name of a TE reference sequence (if empty, all subjects are considered)")
    parser.add_argument('-l', '--length', default=0, type="int", help="length of the genome (in bp)")
    parser.add_argument('-c', '--clean', action='store_true', help="clean files ")
    parser.add_argument('-v', '--verbose', default=0, type=int, help="verbose (default=0/1)")

    args = parser.parse_args()
    join_path_file = args.path_file
    te_ref = args.te_ref
    genome_length = args.length
    verbose = args.verbose
    clean = args.clean

    iGCLFT = getCumulLengthFromTEannot(join_path_file=join_path_file, TE_refseq=te_ref, genome_length=genome_length,
                                       clean=clean, verbose=verbose)
    iGCLFT.run()
