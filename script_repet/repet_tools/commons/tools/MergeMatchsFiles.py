#!/usr/bin/env python
# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
import os
import sys
import shutil
import subprocess
import argparse

from commons.core.utils.FileUtils import FileUtils
from commons.core.coord.Align import Align


class MergeMatchsFiles(object):
    """
    TODO: quand fileType = align ou tab, ca ne marche pas => A implementer si besoin
    """

    def __init__(self, fileType='set', outFileBaseName='', path="", allByAll=True, clean=True, singularity_img="", verbose=0):
        if path != "":
            os.chdir(path)
        self._fileType = fileType
        self._outFileBaseName = outFileBaseName
        self._allByAll = allByAll
        self._verbose = verbose
        self._clean = clean
        self._singularity_img = singularity_img

    def _filterRedundantMatches(self, inFile, outFile):
        """
        When a pairwise alignment is launched ~ all-by-all (ie one batch against all chunks),
        one filters the redundant matches. For instance we keep 'chunk3-1-100-chunk7-11-110-...'
        and we discards 'chunk7-11-110-chunk3-1-100-...'.
        Also we keep 'chunk5-1-100-chunk5-11-110-...' and we discards
        'chunk5-11-110-chunk5-1-100-...'.
        For this of course the results need to be sorted by query, on plus strand,
        and in ascending coordinates (always the case with Blaster).
        """
        with open(inFile, "r") as inFileHandler, open(outFile, "w") as outFileHandler:
            iAlign = Align()
            countMatches = 0
            tick = 100000
            line = inFileHandler.readline()
            while line:
                countMatches += 1
                iAlign.setFromString(line)
                if "chunk" not in iAlign.range_query.seqname or "chunk" not in iAlign.range_subject.seqname:
                    print("ERROR: 'chunk' not in seqname")
                    sys.exit(1)
                if int(iAlign.range_query.seqname.split("chunk")[1]) < int(
                        iAlign.range_subject.seqname.split("chunk")[1]):
                    iAlign.write(outFileHandler)
                elif int(iAlign.range_query.seqname.split("chunk")[1]) == int(
                        iAlign.range_subject.seqname.split("chunk")[1]):
                    if iAlign.range_query.getMin() < iAlign.range_subject.getMin():
                        iAlign.write(outFileHandler)
                if countMatches % tick == 0:  # need to free buffer frequently as file can be big
                    outFileHandler.flush()
                    os.fsync(outFileHandler.fileno())
                line = inFileHandler.readline()

    def run(self):
        if self._verbose > 1:
            print("concatenate the results of each job")
            sys.stdout.flush()

        tmpFileName = "{}.{}_tmp".format(self._outFileBaseName, self._fileType)
        outFileName = "{}.{}".format(self._outFileBaseName, self._fileType)
        pattern = "*.{}".format(self._fileType)

        if os.path.exists(tmpFileName):
            os.remove(tmpFileName)
        FileUtils.catFilesByPattern(pattern, tmpFileName)
        if self._clean:
            FileUtils.removeFilesByPattern(pattern)

        if self._fileType == "align":
            if self._allByAll:
                self._filterRedundantMatches(tmpFileName, outFileName)
            else:
                shutil.move(tmpFileName, outFileName)
        else:
            if self._singularity_img == "":
                prg = "{}num2id".format(self._fileType)
            else:
                prg = "singularity exec {} {}num2id".format(self._singularity_img, self._fileType)
            cmd = prg
            cmd += " -i {}".format(tmpFileName)
            cmd += " -o {}".format(outFileName)
            cmd += " -v {}".format(self._verbose - 1)
            log = subprocess.call(cmd, shell=True)
            if log != 0:
                print("*** Error: {} returned {}".format(prg, log))
                sys.exit(1)


if __name__ == '__main__':
    # (self,fileType = 'path', outFileBaseName = '', allByAll=False, clean=True, verbose=0):
    parser = argparse.ArgumentParser(description='Merge files (align or path)',
                                     formatter_class=argparse.RawTextHelpFormatter, add_help=True)
    parser.add_argument('-i', '--fileType', required=True, help="Files type (format='align' or 'path')")
    parser.add_argument('-p', '--path', required=True, help="Directory path")
    parser.add_argument('-o', '--outFileBaseName', default='',
                        help="output file base name")
    parser.add_argument('-A', '--allByAll', action='store_true',
                        help="filter redundant matches")
    parser.add_argument('-c', '--clean', action='store_true',
                        help="clean files ")
    parser.add_argument('-v', '--verbose', default=0, type=int, help="verbose (default=0/1)")

    args = parser.parse_args()
    fileType = args.fileType
    outFileName = args.outFileBaseName
    path = args.path
    verbose = args.verbose
    allByAll = args.allByAll
    clean = args.clean
    iMergeMatchsFiles = MergeMatchsFiles(fileType, outFileName, path, clean=clean, verbose=verbose, allByAll=allByAll)
    iMergeMatchsFiles.run()

