#!/usr/bin/env python

# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

##@file
# usage: ",sys.argv[0],"[ options ]
# options:
#      -h: this help
#      -i: input file name (output from the clustering method, format=map)
#      -o: output file (format=map, default=inFileName+'.filtered')
#      -c: clustering method used to produce the clusters (Recon/Piler/Blastclust, not Grouper!)
#      -L: maximum length of a HSP (in bp, default=20000, -1 to avoid)
#      -m: minimum number of sequences per group (default=3, '-1' to avoid this filter)
#      -M: maximum number of sequences (longest) per group (default=20, '-1' to avoid this filter)
#      -d: give stats per cluster, in a file inFileName+'.distribC'
#      -e: give stats per sequence, in a file inFileName+'distribS'
#      -v: verbose (default=0/1/2)


import os
import sys
from commons.core.stat.Stat import Stat
from commons.core.utils.RepetOptionParser import RepetOptionParser
from commons.core.checker.RepetException import RepetException
from commons.core.LoggerFactory import LoggerFactory
from commons.core.coord.Map import Map
from commons.core.coord.MapUtils import MapUtils

LOG_DEPTH = "repet.tools"


class FilterSeqClusters(object):

    def __init__(self, inFileName="", outFileName="", HSPLength=20000, minSeqPerGroup=3, maxSeqPerGroup=20,
                 statClust=False, statSeq=False, verbose=0, log_name="FilterSeqClusters.log"):
        self._inFileName = inFileName
        self._HSPLength = HSPLength
        self._minSeqPerGroup = minSeqPerGroup
        self._maxSeqPerGroup = maxSeqPerGroup
        self._isStatClust = statClust
        self._isStatSeq = statSeq
        self._verbose = verbose
        self._dClusterToMembers = {}
        self._dSeqNameToMembers = {}
        self._log = LoggerFactory.createLogger("{}.{}".format((LOG_DEPTH, self.__class__.__name__), self._verbose),
                                               log_name)
        self._iStat = Stat()
        if outFileName != "":
            self._outFileName = outFileName
        else:
            self._outFileName = "{}.filtered-{}-{}".format(self._inFileName, self._minSeqPerGroup, self._maxSeqPerGroup)

    def setAttributesFromCmdLine(self):
        """
        Set the attributes from the command-line.
        """

        description = "Filter clusters and gives some statistics."
        usage = "FilterSeqClusters.py -i inputFileName -c clusteringTool [options]"

        examples = "\nExample 1: filter clusters with default options and highest verbose.\n"
        examples += "\t$ python FilterSeqClusters.py -i reconOut.map -c Recon -v 2"
        examples += "\n\t"
        examples += "\t\nExample 2: filter clusters only on max sequences number per cluster and give statistics per cluster and sequence.\n"
        examples += "\t$ python FilterSeqClusters.py -i blastclustOut.map -c Blastclust -M -1 -d -e\n"

        parser = RepetOptionParser(description=description, usage=usage, version="v1.0", epilog=examples)
        parser.add_option("-i", "--input", dest="inFileName", type="string",
                          help="input file name (output from the clustering method, format=map)", default="")
        parser.add_option("-o", "--out", dest="outFileName", type="string",
                          help="output file (format=map, default=inFileName+'.filtered')", default="")
        parser.add_option("-L", "--length", dest="HSPLength", type="int",
                          help="maximum length of a HSP (in bp, default=20000, -1 to avoid)", default=20000)
        parser.add_option("-m", "--min", dest="minSeqPerGroup", type="int",
                          help="minimum number of sequences per group (default=3, '-1' to avoid)", default=3)
        parser.add_option("-M", "--max", dest="maxSeqPerGroup", type="int",
                          help="maximum number of sequences (longest) per group (default=20, '-1' to avoid)",
                          default=20)
        parser.add_option("-d", "--statclust", dest="statClust",
                          help="give stats per cluster, in a file inFileName+'.distribC'", default=False,
                          action="store_true")
        parser.add_option("-e", "--statseq", dest="statSeq",
                          help="give stats per sequence, in a file inFileName+'distribS'", default=False,
                          action="store_true")
        parser.add_option("-v", "--verbose", dest="verbose", type="int", help="verbosity level (default=0/1/2)",
                          default=0)

        options, args = parser.parse_args()
        self._inFileName = options.inFileName
        self._HSPLength = options.HSPLength
        self._minSeqPerGroup = options.minSeqPerGroup
        self._maxSeqPerGroup = options.maxSeqPerGroup
        self._isStatClust = options.statClust
        self._isStatSeq = options.statSeq
        self._verbose = options.verbose
        if options.outFileName != "":
            self._outFileName = options.outFileName
        else:
            self._outFileName = "{}.filtered-{}-{}".format(self._inFileName, self._minSeqPerGroup, self._maxSeqPerGroup)

    def _checkAttributes(self):
        """
        Check the attributes are valid before running the algorithm.
        """
        if self._inFileName == "":
            raise RepetException("ERROR: missing input file (-i)")
        if not os.path.exists(self._inFileName):
            raise RepetException("ERROR: can't find file '{}'".format(self._inFileName))

    def showStatsClusterSize(self):
        """
        Show some descriptive statistics on the clusters.
        """

        string = ""
        self._iStat.reset()

        string += "{} clusters".format(len(self._dClusterToMembers.keys()))

        for clusterID in self._dClusterToMembers.keys():
            self._iStat.add(len(self._dClusterToMembers[clusterID]))
            self._log.debug("Cluster {}: {} members".format(clusterID, len(self._dClusterToMembers[clusterID])))

        string += "\nCluster size: mean={:.3f} sd={:.3f}".format(self._iStat.mean(), self._iStat.sd())
        string += "\n{}".format(self._iStat.stringQuantiles())

        self._writeMessage(string)

    def showStatsMemberLength(self):
        """
        Show some descriptive statistics on the members.
        """

        string = ""
        self._iStat.reset()
        iStatForCoefVar = Stat()

        for clusterID in self._dClusterToMembers.keys():
            tmpStat = Stat()
            for iMap in self._dClusterToMembers[clusterID]:
                self._iStat.add(iMap.getLength())
                tmpStat.add(iMap.getLength())
            iStatForCoefVar.add(tmpStat.cv())

        string += "{} members".format(self._iStat._n)
        string += "\nMembers lengths: mean={:.3f} sd={:.3f}".format(self._iStat.mean(), self._iStat.sd())
        string += "\n{}".format(self._iStat.stringQuantiles())
        string += "\nMembers lengths coefficients of variations: mean={:.3f} sd={:.3f}".format(iStatForCoefVar.mean(),
                                                                                               iStatForCoefVar.sd())

        self._writeMessage(string)

    def writeDistribPerCluster(self):
        """
        Write some statistics on each cluster.
        """

        distribFileName = "{}.distribC".format(self._inFileName)
        with open(distribFileName, "w") as distribFile:
            distribFile.write("cluster\tsize\tmeanMbLength\tvarMbLength\tsdMbLength\tcvMbLength\n")

            for clusterID in sorted(self._dClusterToMembers.keys(), key=lambda clusterID: (float(clusterID))):
                tmpStat = Stat()
                for iMap in self._dClusterToMembers[clusterID]:
                    tmpStat.add(iMap.getLength())
                distribFile.write(
                    "{}\t{}\t{:f}\t{:f}\t{:f}\t{:f}\n".format(clusterID, len(self._dClusterToMembers[clusterID]),
                                                              tmpStat.mean(), tmpStat.var(), tmpStat.sd(),
                                                              tmpStat.cv()))

    def writeDistribPerSeq(self):
        """
        Write some statistics on each input sequence (not member).
        """

        distribFileName = "{}.distribS".format(self._inFileName)
        with open(distribFileName, "w") as distribFile:
            distribFile.write("sequence\tclusters\tmeanMbLength\tvarMbLength\tsdMbLength\tcvMbLength\n")

            for seqName in sorted(self._dSeqNameToMembers.keys()):
                tmpStat = Stat()
                for iMap in self._dSeqNameToMembers[seqName]:
                    tmpStat.add(iMap.getLength())
                distribFile.write(
                    "{}\t{}\t{:f}\t{:f}\t{:f}\t{:f}\n".format(seqName, len(self._dSeqNameToMembers[seqName]),
                                                              tmpStat.mean(), tmpStat.var(), tmpStat.sd(),
                                                              tmpStat.cv()))

    def getDictOfSequenceInfos(self, line):
        """
        Return a dictionary with sequence info from map file
        """
        data = line.split("\t")
        memberID = ""
        if "Fam" in data[0]:  # old style name
            clusterID = data[0].split("Fam")[1]
            memberID = data[0].split("Pile")[1].split("Fam")[0]
        else:
            clusterID = data[0].split("Cluster")[1].split("Mb")[0]

        dSeqInfo = {"name": data[0],
                    "seqname": data[1],
                    "start": int(data[2]),
                    "end": int(data[3][:-1]),
                    "clusterID": clusterID,
                    "memberID": memberID}
        return dSeqInfo

    def filterOnMinClusterMemberNumber(self):
        """
        Remove from _dClusterToMembers, Clusters with less than 'self._minSeqPerGroup' members
        """

        nbClWithLessThanMinSeq = 0
        *lClusters, = self._dClusterToMembers
        for clusterID in lClusters:
            if (clusterID in self._dClusterToMembers.keys()) and (
                    len(self._dClusterToMembers[clusterID]) < self._minSeqPerGroup):
                nbClWithLessThanMinSeq += 1
                del self._dClusterToMembers[clusterID]

        return nbClWithLessThanMinSeq

    def filterOnMaxClusterMemberNumber(self):
        """
        Keep in _dClusterToMembers the 'self._maxSeqPerGroup' longest sequences of each cluster
        """

        nbClWithMoreThanMaxSeq = 0
        *lClusters, = self._dClusterToMembers
        for clusterID in lClusters:
            if (clusterID in self._dClusterToMembers.keys()) and (
                    len(self._dClusterToMembers[clusterID]) > self._maxSeqPerGroup):
                nbClWithMoreThanMaxSeq += 1
                lSortedFilteredMembers = sorted(self._dClusterToMembers[clusterID], key=lambda iMap: (iMap.getLength()),
                                                reverse=True)[:self._maxSeqPerGroup]
                self._dClusterToMembers[clusterID] = lSortedFilteredMembers

        return nbClWithMoreThanMaxSeq

    def _writeOutputFile(self):
        """
        Write the remaining sequences into the output fasta file
        """
        with open(self._outFileName, "w") as outFile:
            for clusterID, lMap in sorted(self._dClusterToMembers.items(), key=lambda tuple: (float(tuple[0]))):
                lMap = MapUtils.getMapListSortedByIncreasingNameThenSeqnameThenMinThenMax(lMap)
                lMap = sorted(lMap, key=lambda k: (
                float(k.getName().split("Mb")[0].split("Cluster")[1]), float(k.getName().split("Mb")[1])))
                for iMap in lMap:
                    outFile.write(
                        "{}\t{}\t{}\t{}\n".format(iMap.getName(), iMap.getSeqname(), iMap.getStart(), iMap.getEnd()))

    def _writeMessage(self, string):
        self._log.info(string)

    def run(self):
        """
        This program filters clusters of sequences on their number of members, and keeps the longest sequences of each cluster.
        """
        LoggerFactory.setLevel(self._log, self._verbose)
        try:
            self._writeMessage("START {}".format(self.__class__.__name__))
            self._checkAttributes()

            if self._minSeqPerGroup > 0:
                self._writeMessage("Keeping clusters with at least {} members".format(self._minSeqPerGroup))
            if self._maxSeqPerGroup > 0:
                self._writeMessage("Keeping the {} longest sequences of each cluster".format(self._maxSeqPerGroup))

            nbClWithMoreThanMaxSeq = 0

            # fill a dictionary whose keys are groupIDs and values the members belonging to the same group
            self._writeMessage("Parsing input file '{}'".format(self._inFileName))

            with open(self._inFileName, "r") as inFile:
                line = inFile.readline()
                count = 1

                while line:

                    dSeqInfo = self.getDictOfSequenceInfos(line)
                    newMap = Map(dSeqInfo["name"], dSeqInfo["seqname"], dSeqInfo["start"], dSeqInfo["end"])

                    if not ((newMap.getLength() > self._HSPLength) and (self._HSPLength != -1)):

                        clusterID = dSeqInfo["clusterID"]
                        seqname = dSeqInfo["seqname"]

                        # record the data per cluster
                        if clusterID in self._dClusterToMembers.keys():
                            self._dClusterToMembers[clusterID].append(newMap)
                        else:
                            self._dClusterToMembers[clusterID] = [newMap]

                        # record the data per sequence
                        if seqname in self._dSeqNameToMembers.keys():
                            self._dSeqNameToMembers[seqname].append(newMap)
                        else:
                            self._dSeqNameToMembers[seqname] = [newMap]

                    line = inFile.readline()
                    count += 1

            self.showStatsClusterSize()
            self.showStatsMemberLength()

            if self._isStatClust:
                self.writeDistribPerCluster()
            if self._isStatSeq:
                self.writeDistribPerSeq()

            if self._minSeqPerGroup > 0:
                self._writeMessage("Filtering clusters having less than {} members".format(self._minSeqPerGroup))
                nbClWithLessThanMinSeq = self.filterOnMinClusterMemberNumber()
                self._writeMessage(
                    "{} cluster(s) with < {} members".format(nbClWithLessThanMinSeq, self._minSeqPerGroup))

            if self._maxSeqPerGroup > 0:
                self._writeMessage("Keeping the {} longest sequences of each cluster".format(self._maxSeqPerGroup))
                nbClWithMoreThanMaxSeq = self.filterOnMaxClusterMemberNumber()
                self._writeMessage(
                    "{} cluster(s) with > {} members".format(nbClWithMoreThanMaxSeq, self._maxSeqPerGroup))

            self.showStatsClusterSize()
            self.showStatsMemberLength()

            self._writeMessage("Writing output file '{}'".format(self._outFileName))
            self._writeOutputFile()
            self._writeMessage("END {}".format(self.__class__.__name__))

        except RepetException as e:
            newmsg = "ERROR in {}:".format(self.__class__.__name__)
            self._log.error("{} {}".format(newmsg, e.getMessage()))
            sys.exit(1)


if __name__ == "__main__":
    iFilterSeqClusters = FilterSeqClusters()
    iFilterSeqClusters.setAttributesFromCmdLine()
    iFilterSeqClusters.run()
