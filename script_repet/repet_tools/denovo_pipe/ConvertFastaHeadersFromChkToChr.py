#!/usr/bin/env python

## @file
# usage: ConvertFastaHeadersFromChkToChr.py [ options ]

import os
import sys
import glob
import getopt
import argparse

from commons.core.seq.FastaUtils import FastaUtils


class ConvertFastaHeadersFromChkToChr(object):

    def __init__(self, inData="", mapFile="", outFile="", verbose=0):
        self._inData = inData
        self._mapFile = mapFile
        self._outFile = outFile
        self._verbose = verbose

    def checkAttributes(self):
        if self._inData == "":
            msg = "ERROR: missing input data (-i)"
            sys.stderr.write("{}\n".format(msg))
            sys.exit(1)
        if self._mapFile == "":
            msg = "ERROR: missing map file (-m)"
            sys.stderr.write("{}\n".format(msg))
            sys.exit(1)
        if not os.path.exists(self._mapFile):
            msg = "ERROR: can't find map file '{}'".format(self._mapFile)
            sys.stderr.write("{}\n".format(msg))
            sys.exit(1)
        if self._outFile == "" and os.path.isfile(self._inData):
            self._outFile = "{}.onChr".format(self._inData)
            print(self._outFile,)

    def start(self):
        self.checkAttributes()
        if self._verbose > 0:
            msg = "START ConvertFastaHeadersFromChkToChr.py"
            msg += "\ninput data: {}".format(self._inData)
            msg += "\nmap file: {}".format(self._mapFile)
            if os.path.isfile(self._inData):
                msg += "\noutput file: {}".format(self._outFile)
            sys.stdout.write("{}\n".format(msg))
            sys.stdout.flush()

    def end(self):
        if self._verbose > 0:
            sys.stdout.write("END ConvertFastaHeadersFromChkToChr.py\n")
            sys.stdout.flush()

    def run(self):
        self.start()
        if os.path.isfile(self._inData):
            FastaUtils.convertFastaHeadersFromChkToChr(self._inData, self._mapFile, self._outFile)
        else:
            lInFiles = glob.glob(self._inData)
            if len(lInFiles) == 0:
                sys.stdout.write("WARNING: no file corresponds to pattern '{}'\n".format(self._inData))
            for inFile in lInFiles:
                outFile = "{}.onChr".format(inFile)
                FastaUtils.convertFastaHeadersFromChkToChr(inFile, self._mapFile, outFile)

        self.end()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Convert Fasta Headers From chunk To chromosom',
                                     formatter_class=argparse.RawTextHelpFormatter,
                                     add_help=True)
    parser.add_argument('-i', '--input', required=True,
                        help="input data (format='fasta')\ncan be a single fasta file\nif directory, give a pattern (e.g. '/tmp/*.fa')")

    parser.add_argument('-m', "--map", required=True,
                        help="name of the map file (format='map')")

    parser.add_argument('-o', '--output', default="",
                        help="name of the output file (format='fasta', default=inFile+'.onChr')\nif input directory, default is compulsory")

    parser.add_argument('-v', '--verbose', default=0, type=int, help="verbose (default=0/1/2)")

    args = parser.parse_args()
    input = args.input
    map = args.map
    output = args.output
    verbose = args.verbose

    i = ConvertFastaHeadersFromChkToChr(inData=input, mapFile=map, outFile=output, verbose=verbose)
    i.run()
