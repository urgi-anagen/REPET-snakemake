#!/usr/bin/env python

##@file
# usage: filterOutGrouper.py [ options ]
# options:
#      -h: this help
#      -i: name of the input file (output from Grouper, format='fasta')
#      -m: minimum number of sequences per group (default=3)
#      -M: maximum number of sequences (the longest) per group (default=20)
#      -L: maximum length of a HSP, with join or not (in bp, default=20000, -1 to avoid)
#      -J: maximum length of a join (in bp, default=30000, -1 to avoid)
#      -O: chunk overlap (default=10000; skip chunk filtering if negative, e.g. -1)
#      -o: name of the output file (format='fasta', default=inFileName+'.filtered')
#      -v: verbose (default=0/1/2)

import os
import sys
import getopt
import logging
import argparse

from commons.core.stat.Stat import Stat
from commons.core.coord.Map import Map


"""
def getStatsGroupSize( dGr2Mb ):

    string = ""
    iStat.reset()

    lGroups = sorted(dGr2Mb.keys())
    nbGroups = len(lGroups)
    string += "{} groups".format( nbGroups )

    countGroup = 0
    for groupID in lGroups:
        countGroup += 1
        iStat.add( len(dGr2Mb[groupID]) )
        if verbose > 1:
            msg = "group {} ({}/{}): {} member".format( groupID, str(countGroup).zfill(len(str(nbGroups))), nbGroups, len(dGr2Mb[groupID]) )
            if len(dGr2Mb[groupID]) > 1:
                msg += "s"
            print( msg)
            sys.stdout.flush()

    string += "\ngroup size: mean=%.3f sd=%.3f".format( iStat.mean(), iStat.sd() )
    string += "\n{}".format( iStat.stringQuantiles() )

    logging.info( string )
    if verbose > 0:
        print( string)
        sys.stdout.flush()
"""
"""
def getStatsMemberLength( dGr2Mb, dMb2Length ):

    string = ""
    iStat.reset()
    iStatForCoefVar = Stat()

    for groupID in dGr2Mb.keys():
        tmpStat = Stat()
        for memberID in dGr2Mb[groupID]:
            iStat.add( dMb2Length[memberID] )
            tmpStat.add( dMb2Length[memberID] )
        iStatForCoefVar.add( tmpStat.cv() )

    string += "{} members".format( iStat._n )
    string += "\nmember length: mean=%.3f sd=%.3f".format( iStat.mean(), iStat.sd() )
    string += "\n{}".format( iStat.stringQuantiles() )
    string += "\ncoef var of member length: mean=%.3f sd=%.3f".format( iStatForCoefVar.mean(), iStatForCoefVar.sd() )

    logging.info( string )
    if verbose > 0:
        print( string)
        sys.stdout.flush()
"""
"""
def getStatsJoinLengths( lJoinLengths ):

    string = ""
    iStat.reset()

    for i in lJoinLengths:
        iStat.add( i )

    string += "mean length of the joins (connection between 2 HSPs): %.3f bp".format( iStat.mean() )
    string += "\n{}".format( iStat.stringQuantiles() )

    logging.info( string )
    if verbose > 0:
        print( string)
        sys.stdout.flush()

"""



def removeRedundantMembersDueToChunkOverlaps(dGroupId2MemberHeaders, dGr2Mb, chunkOverlap,verbose=0):
    # for each group
    for groupID in dGroupId2MemberHeaders.keys():
        if verbose > 1:
            print("group {}:".format(groupID))
        if groupID not in ["3446"]:
            # continue
            pass

        # get members into Map object, per chunk name
        dChunkName2Map = {}
        for memberH in dGroupId2MemberHeaders[groupID]:

            if verbose > 1:
                print(memberH)
            tokens = memberH.split(" ")

            if "," not in tokens[3]:
                m = Map()
                m.name = tokens[0]
                if "chunk" in tokens[1]:
                    m.seqname = tokens[1].replace('chunk','')
                else:
                    m.seqname = tokens[1].split('_')[0]
                m.start = int(tokens[3].split("..")[0])
                m.end = int(tokens[3].split("..")[1])
                dChunkName2Map[m.seqname] = [m]
            else:
                #dChunkName2Map[tokens[1]] = []
                for i in tokens[3].split(","):
                    m = Map()
                    m.name = tokens[0]
                    if "chunk" in tokens[1]:
                        m.seqname = tokens[1].replace('chunk', '')
                    else:
                        m.seqname = tokens[1].split('_')[0]
                    m.start = int(i.split("..")[0])
                    m.end = int(i.split("..")[1])
                    if m.seqname not in dChunkName2Map.keys() :
                        dChunkName2Map[m.seqname] = [m]
                    else :
                        dChunkName2Map[m.seqname].append(m)

        # remove chunks without previous or next chunks
        dChunkName2Map_copy = {**dChunkName2Map}

        for chunkName in dChunkName2Map_copy.keys():
            if "chunk" in chunkName :
                chunkId = int(chunkName.replace('chunk',''))
            else :
                chunkId = int(chunkName.split('_')[0])

            if not ((chunkId + 1) in list(map(int, dChunkName2Map.keys())) \
                    or ((chunkId - 1) in list(map(int, dChunkName2Map.keys())))):
                del dChunkName2Map[chunkName]
                continue

        # for each pair of chunk overlap, remove one chunk
        lChunkNames = sorted(dChunkName2Map.keys())

        out = []
        for i in range(0, len(lChunkNames), 2):
            del dChunkName2Map[lChunkNames[i]]

        # remove members outside chunk overlap (~< 10000 bp)
        dChunkName2Map_copy = {**dChunkName2Map}
        for chunkName in dChunkName2Map_copy.keys():
            out = []
            for index, m in enumerate(dChunkName2Map[chunkName][:]):
                if m.getMax() <= 1.1 * chunkOverlap:
                    out.append(dChunkName2Map[chunkName][index])
            dChunkName2Map[chunkName] = out
            if len(dChunkName2Map[chunkName]) == 0:
                del dChunkName2Map[chunkName]

        if verbose > 1:
            print("all members:", dGr2Mb[groupID])
            print("chunks to clean:", dChunkName2Map.keys())
        lMembersToRemove = []
        for i in dChunkName2Map.keys():
            for j in dChunkName2Map[i]:
                mbId = j.name.split("Gr")[0]
                if "Q" in mbId:
                    mbId = mbId.split("Q")[1]
                elif "S" in mbId:
                    mbId = mbId.split("S")[1]
                lMembersToRemove.append(mbId)
        out = []
        for index, k in enumerate(dGr2Mb[groupID][:]):
            if k not in lMembersToRemove:
                out.append(dGr2Mb[groupID][index])
        dGr2Mb[groupID] = out
        if verbose > 1:
            print("members to keep:", dGr2Mb[groupID])
            sys.stdout.flush()

    return dGr2Mb


def selectTheMaxSeqLongestSequences(maxSeq, dGr2Mb, dMb2Length,verbose):
    string = "keep the {} longest sequences of each group".format(maxSeq)
    logging.info(string)
    if verbose > 0:
        print("\n* {}".format(string))
        sys.stdout.flush()

    lMbToKeep = []
    groupsWithManyMb = 0

    for groupID in dGr2Mb.keys():
        lMbFromThisGroup = []

        if len(dGr2Mb[groupID]) > maxSeq:
            groupsWithManyMb += 1
            for member in dGr2Mb[groupID]:
                mbLength = dMb2Length[member]
                lMbFromThisGroup.append((member, mbLength))

            lmaxseqMbLengthForThisGroupSorted = sorted(lMbFromThisGroup,
                                                       key=lambda mbLength: (mbLength[1], mbLength[0]), reverse=True)[
                                                :maxSeq]
            dGr2Mb[groupID] = [item[0] for item in lmaxseqMbLengthForThisGroupSorted]

        lMbToKeep.extend(dGr2Mb[groupID])

    string = "nb of groups with more than {} members: {}".format(maxSeq, groupsWithManyMb)

    logging.info(string)
    if verbose > 0:
        print(string)
        sys.stdout.flush()

    return lMbToKeep, dGr2Mb


def main(inFaFileName="", minSeq=3, maxSeq=20, maxHspLength=20000, maxJoinLength=30000, outFaFileName="",
         chunkOverlap=10000, verboseParam=0, logFileName = "filterOutGrouper.log"):
    """
    This program filters the groups made by Grouper.
    """
    verbose = verboseParam

    if inFaFileName == "":
        print("ERROR: missing compulsory options")
        help()
        sys.exit(1)

    if minSeq < 0 or maxSeq < 0 or maxSeq < minSeq:
        print("ERROR with options '-m' or '-M'")
        help()
        sys.exit(1)

    if outFaFileName == "":
        outFaFileName = "{}.filtered".format(inFaFileName)

    if verbose > 0:
        print("START {}".format(sys.argv[0].split("/")[-1]))
        sys.stdout.flush()


    if os.path.exists(logFileName):
        os.remove(logFileName)
    global logging
    handler = logging.FileHandler(logFileName)
    formatter = logging.Formatter("%(asctime)s %(levelname)s: %(message)s")
    handler.setFormatter(formatter)
    logging.getLogger('').addHandler(handler)
    logging.getLogger('').setLevel(logging.DEBUG)
    logging.info("started")

    if minSeq > 0:
        logging.info("keep groups with more than {} members".format(minSeq))
    if maxSeq > 0:
        logging.info("keep the {} longest sequences of each group".format(maxSeq))

    # create the object recording the statistics
    # global iStat
    # iStat = Stat()

    # retrieve the data about the groups and check the length of the members

    string = "parse input file '{}'".format(inFaFileName)
    logging.info(string)
    if verbose > 0:
        print("\n* {}".format(string))
        sys.stdout.flush()

    keep = True
    nbHspTooLong = 0
    nbJoinTooLong = 0
    dGr2Mb = {}
    dMb2Length = {}
    lJoinLengths = []
    dGroupId2MemberHeaders = {}

    with open(inFaFileName, "r") as inFaFile:
        line = inFaFile.readline()

        while True:

            if line == "":
                break

            if line[0] == ">":
                data = line[:-1].split(" ")
                if verbose > 2:
                    print(data)

                memberName = data[0]
                clusterID = memberName.split("Cl")[1]
                groupID = memberName.split("Cl")[0].split("Gr")[1]
                if "Q" in memberName.split("Gr")[0]:
                    memberID = memberName.split("Gr")[0].split("MbQ")[1]
                elif "S" in memberName:
                    memberID = memberName.split("Gr")[0].split("MbS")[1]

                coord = data[-1]

                # if the member was joined (concatenation of several HSPs)
                if "," in coord:
                    length = 0
                    for i in coord.split(","):
                        start = int(i.split("..")[0])
                        end = int(i.split("..")[1])
                        length += abs(end - start) + 1
                    if maxHspLength > 0 and length > maxHspLength:
                        keep = False
                        nbHspTooLong += 1
                    else:
                        if maxJoinLength > 0:
                            for i in coord.split("..")[1:-1]:
                                joinLength = abs(int(i.split(",")[1]) - int(i.split(",")[0])) + 1
                                lJoinLengths.append(joinLength)
                                if joinLength > maxJoinLength:
                                    keep = False
                                    nbJoinTooLong += 1

                # if not (the member is one HSP)
                else:
                    start = int(coord.split("..")[0])
                    end = int(coord.split("..")[1])
                    length = abs(end - start) + 1
                    if maxHspLength > 0 and length > maxHspLength:
                        keep = False
                        nbHspTooLong += 1

                if keep == True:
                    if groupID in dGr2Mb:
                        dGr2Mb[groupID].append(memberID)
                        dGroupId2MemberHeaders[groupID].append(line[0:-1])
                    else:
                        dGr2Mb[groupID] = [memberID]
                        dGroupId2MemberHeaders[groupID] = [line[0:-1]]
                    if memberID in dMb2Length:
                        print("ERROR: memberID '{}' is duplicated".format(memberID))
                    else:
                        dMb2Length[memberID] = length

            line = inFaFile.readline()
            keep = True

    if len(dMb2Length.keys()) == 0:
        string = "WARNING: input fasta file '{}' is empty".format(inFaFileName)
        logging.info(string)
        if verbose > 0:
            print(string)
            sys.stdout.flush()
        os.system("touch {}".format(outFaFileName))
        logging.info("finished")
        if verbose > 0:
            print("END {}".format(sys.argv[0].split("/")[-1]))
            sys.stdout.flush()
        sys.exit(0)

    if maxHspLength > 0:
        string = "nb of HSPs longer than {} bp: {}".format(maxHspLength, nbHspTooLong)
        logging.info(string)
        if verbose > 0:
            print(string)
            sys.stdout.flush()
    if maxJoinLength > 0:
        string = "nb of joins longer than {} bp: {}".format(maxJoinLength, nbJoinTooLong)
        logging.info(string)
        if verbose > 0:
            print(string)
            sys.stdout.flush()
    # getStatsGroupSize( dGr2Mb )
    # getStatsMemberLength( dGr2Mb, dMb2Length )
    # getStatsJoinLengths( lJoinLengths )

    if chunkOverlap > 0:
        removeRedundantMembersDueToChunkOverlaps(dGroupId2MemberHeaders, dGr2Mb, chunkOverlap, verbose=verbose)

    # delete dictionary entries corresponding to groups having less than 'minSeq' members

    nbTooSmallGroup = 0

    if minSeq > 0:

        string = "filter groups having less than {} members".format(minSeq)
        logging.info(string)
        if verbose > 0:
            print("\n* {}".format(string))
            sys.stdout.flush()

        lGr2MbKey = sorted(dGr2Mb.keys())
        for groupID in lGr2MbKey:
            if len(dGr2Mb[groupID]) < minSeq:
                del dGr2Mb[groupID]
                nbTooSmallGroup += 1

        string = "nb of groups with less than {} members: {}".format(minSeq, nbTooSmallGroup)
        logging.info(string)
        if verbose > 0:
            print(string)
            sys.stdout.flush()

    # select the 'maxSeq' longest sequences of each group
    if maxSeq >= minSeq:
        lMbToKeep, dGr2Mb = selectTheMaxSeqLongestSequences(maxSeq, dGr2Mb, dMb2Length,verbose=verbose)
    # getStatsGroupSize( dGr2Mb )
    # getStatsMemberLength( dGr2Mb, dMb2Length )

    # write the remaining sequences into the output fasta file

    with open(inFaFileName, "r") as inFaFile, open(outFaFileName, "w") as outFaFile:
        line = inFaFile.readline()

        string = "write output file '{}'".format(outFaFileName)
        logging.info(string)
        if verbose > 0:
            print("\n* {}".format(string))
            sys.stdout.flush()

        keepMember = False

        while True:

            if line == "":
                break

            if line[0] == ">":
                data = line[:-1].split(" ")
                print (data)

                memberName = data[0]
                clusterID = memberName.split("Cl")[1]
                groupID = memberName.split("Cl")[0].split("Gr")[1]
                if "Q" in memberName.split("Gr")[0]:
                    memberID = memberName.split("Gr")[0].split("MbQ")[1]
                elif "S" in memberName:
                    memberID = memberName.split("Gr")[0].split("MbS")[1]

                if memberID in lMbToKeep:
                    keepMember = True
                    outFaFile.write(line)
                else:
                    keepMember = False

            else:
                if keepMember == True:
                    outFaFile.write(line)

            line = inFaFile.readline()

    logging.info("finished")

    if verbose > 0:
        print("END {}".format(sys.argv[0].split("/")[-1]))
        sys.stdout.flush()

    return 0


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Filter grouper output', formatter_class=argparse.RawTextHelpFormatter,
                                     add_help=True)
    parser.add_argument('-i', '--input', required=True,
                        help="name of the input file (output from Grouper, format='fasta')")

    parser.add_argument('-m', "--minSeq", type=int, default=3,
                        help="minimum number of sequences per group (default=3)")

    parser.add_argument('-M', '--maxSeq', type=int, default=20,
                        help="maximum number of sequences (the longest) per group (default=20)")

    parser.add_argument('-L', '--maxLengthHSP', type=int, default=20000,
                        help="maximum length of a HSP, with join or not (in bp, default=20000, -1 to avoid)")

    parser.add_argument('-J', '--maxLengthJoin', type=int, default=30000,
                        help="maximum length of a join (in bp, default=30000, -1 to avoid)")

    parser.add_argument('-O', '--overlap', type=int, default=10000,
                        help="chunk overlap (default=10000; skip chunk filtering if negative, e.g. -1)")

    parser.add_argument('-o', '--output', default="",
                        help="name of the output file (format='fasta', default=inFileName+'.filtered')")

    parser.add_argument('-v', '--verbose', default=0, type=int, help="verbose (default=0/1/2)")

    args = parser.parse_args()
    input = args.input
    minSeq = args.minSeq
    maxSeq = args.maxSeq
    maxLengthHSP = args.maxLengthHSP
    maxLengthJoin = args.maxLengthJoin
    overlap = args.overlap
    output = args.output
    verbose = args.verbose

    main(input, minSeq, maxSeq, maxLengthHSP, maxLengthJoin, output, overlap, verbose)
