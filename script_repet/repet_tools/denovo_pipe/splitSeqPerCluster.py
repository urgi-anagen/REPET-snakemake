#!/usr/bin/env python

import os
import sys
import getopt
import argparse
import os
import glob
import shutil

from commons.core.seq.FastaUtils import FastaUtils


def main(path="",inFileName="", clusteringMethod="", createDir=False, simplifyHeader=False, verbose=0):
    """
    Record all the sequences belonging to the same cluster in the same fasta file
    """

    inFileName = inFileName
    clusteringMethod = clusteringMethod
    createDir = createDir
    simplifyHeader = simplifyHeader
    verbose = verbose


    if inFileName == "":
        msg = "ERROR: missing input file (-i)"
        sys.stderr.write("{}\n".format(msg))
        sys.exit(1)
    if clusteringMethod == "":
        msg = "ERROR: missing clustering method (-c)"
        sys.stderr.write("{}\n".format(msg))
        sys.exit(1)
    if not os.path.exists(inFileName):
        msg = "ERROR: can't find file '{}'".format(inFileName)
        sys.stderr.write("{}\n".format(msg))
        sys.exit(1)

    if verbose > 0:
        print("START splitSeqPerCluster")
        sys.stdout.flush()

    if path != "":
        FastaUtils.splitSeqPerCluster(inFileName, clusteringMethod, simplifyHeader,
                                      createDir, "seqCluster", verbose, directory = path)
    else :
        FastaUtils.splitSeqPerCluster(inFileName, clusteringMethod, simplifyHeader,
                                  createDir, "seqCluster", verbose)

    if verbose > 0:
        print("END splitSeqPerCluster")
        sys.stdout.flush()

    return 0


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Split sequences per cluster',
                                     formatter_class=argparse.RawTextHelpFormatter,
                                     add_help=True)
    parser.add_argument('-i', '--input', required=True,
                        help="name of the input file (format='fasta')")

    parser.add_argument('-c', "--clusteringMethod",
                        help="clustering method (Grouper/Recon/Piler/Blastclust)")

    parser.add_argument('-d', '--directory', action='store_true',
                        help="create a directory for each group")

    parser.add_argument('-H', '--simplify', action='store_true',
                        help="simplify sequence headers")

    parser.add_argument('-v', '--verbose', default=0, type=int, help="verbose (default=0/1/2)")

    args = parser.parse_args()
    input = args.input
    clusteringMethod = args.clusteringMethod
    directory = args.directory
    simplify = args.simplify
    verbose = args.verbose

    main(input, clusteringMethod, directory, simplify, verbose)
