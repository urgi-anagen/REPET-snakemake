import subprocess
import os
import argparse

class LaunchRecon(object):

    def __init__(self, Infile, msp, number=1, verbose=0):
        self.Infile = Infile #
        self.msp = msp
        self.number = number
        self.verbose = verbose

        #print (self.Infile)
        #print (self.msp)

        #self.inFile = "../../"+self.inFile
        #self.msp = "../../"+self.msp


    def check_if_crash(self, process, process_name):
        if process.returncode != 0:
                print ("{} failed, shuting down".format(process_name))
                return quit()
        else:
            if self.verbose > 0:
                print ("{} executed succesfully, moving on ".format(process_name))


    def setup_dirs(self, dirname):
        try:
            os.mkdir(dirname)
        except:
            if self.verbose > 0:
                print("{} already exist as dir".format(dirname))


    def run(self):

        self.setup_dirs("summary")
        self.setup_dirs("images")
        self.setup_dirs("ele_def_res")
        self.setup_dirs("ele_redef_res")
        self.setup_dirs("edge_redef_res")

        cmd = "imagespread {} {}".format(self.Infile, self.msp)
        imagespread = subprocess.run(cmd, shell=True)
        self.check_if_crash(imagespread, "imagespread")


        open("./images/images_sorted", mode='a').close() #create empty_file


        for i in range(1,self.number+1):
            sort = subprocess.run("sort -k 3,3 -k 4n,4n -k 5nr,5nr ./images/spread{} >> ./images/images_sorted".format(i), shell=True)
            self.check_if_crash(sort, "sorting")

        cmd = "eledef {} {} single".format(self.Infile, self.msp)
        eledef = subprocess.run(cmd, shell=True)
        self.check_if_crash(eledef, "eledef")

        os.symlink("ele_def_res", "tmp")
        os.symlink("ele_def_res", "tmp2")

        cmd = "eleredef {}".format(self.Infile)
        eleredef = subprocess.run(cmd, shell=True)
        self.check_if_crash(eleredef, "eleredef")

        os.remove("tmp")
        os.remove("tmp2")
        os.symlink("ele_def_res", "tmp")
        os.symlink("edge_redef_res", "tmp2")

        cmd = "edgeredef {}".format(self.Infile)
        edgeredef = subprocess.run(cmd, shell=True)
        self.check_if_crash(edgeredef, "edgeredef")


        os.remove("tmp")
        os.remove("tmp2")
        os.symlink("edge_redef_res", "tmp")

        cmd ="famdef {}".format(self.Infile, self.msp)
        famdef = subprocess.run(cmd, shell=True)
        self.check_if_crash(famdef,"famdef")

        os.remove("tmp")



if __name__ == '__main__':
    usage = "./recon.py [options]"
    description = "A py reimplementation of the recon.pl wrapper script "
    description += "\n used to sequentially run imagespread, eledef, edgeredef and famdef "
    parser = argparse.ArgumentParser(description=description, usage=usage)
    parser.add_argument('-s', '--seq', required = True)
    parser.add_argument('-m', '--msp', required = True)
    parser.add_argument('-n', '--number', required= False, default=1)
    parser.add_argument('-v', '--verbose', required= False, default=0)
    args = parser.parse_args()

    self.Infile = args.seq
    self.msp = args.msp
    self.number = args.number
    self.verbose = args.verbose
