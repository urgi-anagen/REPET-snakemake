import unittest
import os
from denovo_pipe.FilterSeqClusters import FilterSeqClusters
from commons.core.utils.FileUtils import FileUtils

class Test_F_FilterSeqClusters(unittest.TestCase):
    
    def setUp(self):
        self._inputMapName = "inFileFilterSeqClusterTest.map"
        self._outputMapName = "%s.filtered" % self._inputMapName
        self._writeInputMap()
        self._iFSQ = FilterSeqClusters(self._inputMapName, verbose = 3)
        self._iFSQ.setOutFile(self._outputMapName)
        
    def tearDown(self):
        try:
            os.remove(self._outputMapName)
            os.remove(self._inputMapName)
        except:
            pass
    
    def test_FilterSeqClusters_as_class_3Elts_20Seq_stats(self):
        self._iFSQ.setIsStatClust(True)
        self._iFSQ.setIsStatSeq(True)
        self._iFSQ.run()
        
        expFileName = "exp3Elts20Seqs.map"
        self._writeExpectedDefault(expFileName)
        
        obsStatsClustFileName = "%s.distribC" % self._inputMapName
        expStatsClustFileName = "exp3Elts20Seqs.distribC"
        self._writeExpectedStatClust(expStatsClustFileName)
        
        obsStatsSeqFileName = "%s.distribS" % self._inputMapName
        expStatsSeqFileName = "exp3Elts20Seqs.distribS"
        self._writeExpectedStatSeq(expStatsSeqFileName)
        
        self.assertTrue(FileUtils.are2FilesIdentical(expFileName, self._outputMapName))
        self.assertTrue(FileUtils.are2FilesIdentical(expStatsClustFileName, obsStatsClustFileName))
        self.assertTrue(FileUtils.are2FilesIdentical(expStatsSeqFileName, obsStatsSeqFileName))
        
        os.remove(expFileName)
        os.remove(expStatsClustFileName)
        os.remove(expStatsSeqFileName)
        os.remove(obsStatsClustFileName)
        os.remove(obsStatsSeqFileName)

    def test_FilterSeqClusters_as_class_3Elts_20Seq_HSP5000(self):
        self._iFSQ.setHSPLength(5000)
        self._iFSQ.run()
        
        expFileName = "exp3Elts20SeqsHSP5000.map"
        self._writeExpectedHSP5000(expFileName)
        
        self.assertTrue(FileUtils.are2FilesIdentical(expFileName, self._outputMapName))
        
        os.remove(expFileName)
        
    def test_FilterSeqClusters_as_class_3Elts_20Seq_2SeqPerGroup(self):
        self._iFSQ.setMinSeqPerGroup(2)
        self._iFSQ.run()
        
        expFileName = "exp3Elts20Seqs2SeqPerGroup.map"
        self._writeExpected2SeqPerGroup(expFileName)
        
        self.assertTrue(FileUtils.are2FilesIdentical(expFileName, self._outputMapName))
        
        os.remove(expFileName)
        
    def test_FilterSeqClusters_as_class_3Elts_5seq(self):
        self._iFSQ.setMaxSeqPerGroup(5)
        self._iFSQ.run()
        
        expFileName = "exp3Elts5Seqs.map"
        self._writeExpected3Elts5Seqs(expFileName)
        
        self.assertTrue(FileUtils.are2FilesIdentical(expFileName, self._outputMapName))
        
        os.remove(expFileName)
        
    def test_FilterSeqClusters_as_class_empty_input(self):
        os.remove(self._inputMapName)
        with open(self._inputMapName, 'w'):
            pass
        
        self._iFSQ.setMaxSeqPerGroup(5)
        self._iFSQ.run()
        
        expFileName = "exp3Elts5Seqs.map"
        with open(expFileName, 'w'):
            pass
        
        self.assertTrue(FileUtils.are2FilesIdentical(expFileName, self._outputMapName))
        
        os.remove(expFileName)
        
    def _writeInputMap(self):
        fH = open(self._inputMapName, "w")
        fH.write("PilerCluster1.0Mb14\tchunk7\t122568\t123660\n")
        fH.write("PilerCluster1.3Mb14\tchunk7\t122568\t123660\n")
        fH.write("PilerCluster1.5Mb14\tchunk7\t122568\t123660\n")
        fH.write("PilerCluster2.5Mb14\tchunk7\t122568\t123660\n")
        fH.write("PilerCluster3.4Mb14\tchunk7\t122568\t123660\n")

        fH.write("BlastclustCluster1Mb1\tchunk1\t44957\t60589\n")
        fH.write("BlastclustCluster1Mb2\tchunk3\t61296\t73986\n")
        fH.write("BlastclustCluster1Mb3\tchunk7\t123264\t132001\n")
        fH.write("BlastclustCluster1Mb4\tchunk5\t42363\t49343\n")
        fH.write("BlastclustCluster1Mb5\tchunk6\t26199\t31423\n")
        fH.write("BlastclustCluster1Mb6\tchunk2\t41806\t46301\n")
        fH.write("BlastclustCluster1Mb7\tchunk2\t133712\t137988\n")
        fH.write("BlastclustCluster1Mb8\tchunk6\t61279\t64955\n")
        fH.write("BlastclustCluster1Mb9\tchunk3\t94293\t97597\n")
        fH.write("BlastclustCluster1Mb10\tchunk3\t152049\t154729\n")
        fH.write("BlastclustCluster1Mb11\tchunk7\t121233\t127370\n")
        fH.write("BlastclustCluster1Mb19\tchunk2\t41806\t42166\n")
        fH.write("BlastclustCluster1Mb20\tchunk2\t41806\t42166\n")
        fH.write("BlastclustCluster1Mb23\tchunk2\t41806\t41966\n")
        fH.write("BlastclustCluster1Mb21\tchunk2\t41806\t42166\n")
        fH.write("BlastclustCluster1Mb12\tchunk2\t41806\t42166\n")
        fH.write("BlastclustCluster3Mb3\tchunk4\t145999\t146452\n")        
        fH.write("BlastclustCluster3Mb2\tchunk4\t123457\t123789\n")
        fH.write("BlastclustCluster4Mb1\tchunk1\t4887\t13246\n")
        fH.write("BlastclustCluster4Mb2\tchunk8\t8887\t13246\n")
        fH.write("BlastclustCluster5Mb1\tchunk2\t150294\t158412\n")
        fH.write("BlastclustCluster6Mb1\tchunk2\t113029\t119770\n")
        fH.write("BlastclustCluster7Mb1\tchunk1\t34275\t40713\n")
        fH.write("BlastclustCluster8Mb1\tchunk5\t170649\t173730\n")
        fH.write("BlastclustCluster9Mb1\tchunk2\t145473\t147381\n")
        fH.write("BlastclustCluster10Mb1\tchunk4\t116181\t117792\n")
        fH.write("BlastclustCluster1Mb13\tchunk2\t41806\t42166\n")
        fH.write("BlastclustCluster1Mb14\tchunk2\t41806\t42166\n")
        fH.write("BlastclustCluster1Mb15\tchunk2\t41806\t42166\n")
        fH.write("BlastclustCluster1Mb16\tchunk2\t41806\t42166\n")
        fH.write("BlastclustCluster1Mb17\tchunk2\t41806\t42166\n")
        fH.write("BlastclustCluster1Mb18\tchunk2\t41806\t42166\n")
        fH.write("BlastclustCluster1Mb22\tchunk2\t41806\t42166\n")
        fH.write("BlastclustCluster2Mb1\tchunk7\t101523\t104351\n")
        fH.write("BlastclustCluster2Mb2\tchunk7\t99136\t100579\n")
        fH.write("BlastclustCluster2Mb3\tchunk7\t44114\t44978\n")
        fH.write("BlastclustCluster2Mb4\tchunk7\t57895\t58998\n")
        fH.write("BlastclustCluster3Mb1\tchunk4\t121910\t130435\n")
        fH.close()
        
    def _writeExpectedDefault(self, expFileName):
        fH = open(expFileName, "w")
        fH.write("BlastclustCluster1Mb1\tchunk1\t44957\t60589\n")
        fH.write("BlastclustCluster1Mb2\tchunk3\t61296\t73986\n")
        fH.write("BlastclustCluster1Mb3\tchunk7\t123264\t132001\n")
        fH.write("BlastclustCluster1Mb4\tchunk5\t42363\t49343\n")
        fH.write("BlastclustCluster1Mb5\tchunk6\t26199\t31423\n")
        fH.write("BlastclustCluster1Mb6\tchunk2\t41806\t46301\n")
        fH.write("BlastclustCluster1Mb7\tchunk2\t133712\t137988\n")
        fH.write("BlastclustCluster1Mb8\tchunk6\t61279\t64955\n")
        fH.write("BlastclustCluster1Mb9\tchunk3\t94293\t97597\n")
        fH.write("BlastclustCluster1Mb10\tchunk3\t152049\t154729\n")
        fH.write("BlastclustCluster1Mb11\tchunk7\t121233\t127370\n")
        fH.write("BlastclustCluster1Mb12\tchunk2\t41806\t42166\n")
        fH.write("BlastclustCluster1Mb13\tchunk2\t41806\t42166\n")
        fH.write("BlastclustCluster1Mb14\tchunk2\t41806\t42166\n")
        fH.write("BlastclustCluster1Mb15\tchunk2\t41806\t42166\n")
        fH.write("BlastclustCluster1Mb16\tchunk2\t41806\t42166\n")
        fH.write("BlastclustCluster1Mb17\tchunk2\t41806\t42166\n")
#        fH.write("BlastclustCluster1Mb18\tchunk2\t41806\t42166\n")
        fH.write("BlastclustCluster1Mb19\tchunk2\t41806\t42166\n")
        fH.write("BlastclustCluster1Mb20\tchunk2\t41806\t42166\n")
        fH.write("BlastclustCluster1Mb21\tchunk2\t41806\t42166\n")
        fH.write("BlastclustCluster2Mb1\tchunk7\t101523\t104351\n")
        fH.write("BlastclustCluster2Mb2\tchunk7\t99136\t100579\n")
        fH.write("BlastclustCluster2Mb3\tchunk7\t44114\t44978\n")
        fH.write("BlastclustCluster2Mb4\tchunk7\t57895\t58998\n")
        fH.write("BlastclustCluster3Mb1\tchunk4\t121910\t130435\n")
        fH.write("BlastclustCluster3Mb2\tchunk4\t123457\t123789\n")
        fH.write("BlastclustCluster3Mb3\tchunk4\t145999\t146452\n")
        fH.close()    
            
    def _writeExpectedHSP5000(self, expFileName):
        fH = open(expFileName, "w")
        fH.write("BlastclustCluster1Mb6\tchunk2\t41806\t46301\n")
        fH.write("BlastclustCluster1Mb7\tchunk2\t133712\t137988\n")
        fH.write("BlastclustCluster1Mb8\tchunk6\t61279\t64955\n")
        fH.write("BlastclustCluster1Mb9\tchunk3\t94293\t97597\n")
        fH.write("BlastclustCluster1Mb10\tchunk3\t152049\t154729\n")
        fH.write("BlastclustCluster1Mb12\tchunk2\t41806\t42166\n")
        fH.write("BlastclustCluster1Mb13\tchunk2\t41806\t42166\n")
        fH.write("BlastclustCluster1Mb14\tchunk2\t41806\t42166\n")
        fH.write("BlastclustCluster1Mb15\tchunk2\t41806\t42166\n")
        fH.write("BlastclustCluster1Mb16\tchunk2\t41806\t42166\n")
        fH.write("BlastclustCluster1Mb17\tchunk2\t41806\t42166\n")
        fH.write("BlastclustCluster1Mb18\tchunk2\t41806\t42166\n")
        fH.write("BlastclustCluster1Mb19\tchunk2\t41806\t42166\n")
        fH.write("BlastclustCluster1Mb20\tchunk2\t41806\t42166\n")
        fH.write("BlastclustCluster1Mb21\tchunk2\t41806\t42166\n")
        fH.write("BlastclustCluster1Mb22\tchunk2\t41806\t42166\n")
        fH.write("BlastclustCluster1Mb23\tchunk2\t41806\t41966\n")
        fH.write("BlastclustCluster2Mb1\tchunk7\t101523\t104351\n")
        fH.write("BlastclustCluster2Mb2\tchunk7\t99136\t100579\n")
        fH.write("BlastclustCluster2Mb3\tchunk7\t44114\t44978\n")
        fH.write("BlastclustCluster2Mb4\tchunk7\t57895\t58998\n")
        fH.close()
        
    def _writeExpected2SeqPerGroup(self, expFileName):
        fH = open(expFileName, "w")
        fH.write("BlastclustCluster1Mb1\tchunk1\t44957\t60589\n")
        fH.write("BlastclustCluster1Mb2\tchunk3\t61296\t73986\n")
        fH.write("BlastclustCluster1Mb3\tchunk7\t123264\t132001\n")
        fH.write("BlastclustCluster1Mb4\tchunk5\t42363\t49343\n")
        fH.write("BlastclustCluster1Mb5\tchunk6\t26199\t31423\n")
        fH.write("BlastclustCluster1Mb6\tchunk2\t41806\t46301\n")
        fH.write("BlastclustCluster1Mb7\tchunk2\t133712\t137988\n")
        fH.write("BlastclustCluster1Mb8\tchunk6\t61279\t64955\n")
        fH.write("BlastclustCluster1Mb9\tchunk3\t94293\t97597\n")
        fH.write("BlastclustCluster1Mb10\tchunk3\t152049\t154729\n")
        fH.write("BlastclustCluster1Mb11\tchunk7\t121233\t127370\n")
        fH.write("BlastclustCluster1Mb12\tchunk2\t41806\t42166\n")
        fH.write("BlastclustCluster1Mb13\tchunk2\t41806\t42166\n")
        fH.write("BlastclustCluster1Mb14\tchunk2\t41806\t42166\n")
        fH.write("BlastclustCluster1Mb15\tchunk2\t41806\t42166\n")
        fH.write("BlastclustCluster1Mb16\tchunk2\t41806\t42166\n")
        fH.write("BlastclustCluster1Mb17\tchunk2\t41806\t42166\n")
#        fH.write("BlastclustCluster1Mb18\tchunk2\t41806\t42166\n")
        fH.write("BlastclustCluster1Mb19\tchunk2\t41806\t42166\n")
        fH.write("BlastclustCluster1Mb20\tchunk2\t41806\t42166\n")
        fH.write("BlastclustCluster1Mb21\tchunk2\t41806\t42166\n")
        fH.write("BlastclustCluster2Mb1\tchunk7\t101523\t104351\n")
        fH.write("BlastclustCluster2Mb2\tchunk7\t99136\t100579\n")
        fH.write("BlastclustCluster2Mb3\tchunk7\t44114\t44978\n")
        fH.write("BlastclustCluster2Mb4\tchunk7\t57895\t58998\n")
        fH.write("BlastclustCluster3Mb1\tchunk4\t121910\t130435\n")
        fH.write("BlastclustCluster3Mb2\tchunk4\t123457\t123789\n")
        fH.write("BlastclustCluster3Mb3\tchunk4\t145999\t146452\n")
        fH.write("BlastclustCluster4Mb1\tchunk1\t4887\t13246\n")
        fH.write("BlastclustCluster4Mb2\tchunk8\t8887\t13246\n")
        fH.close()   
        
    def _writeExpected3Elts5Seqs(self, expFileName):
        fH = open(expFileName, "w")
        fH.write("BlastclustCluster1Mb1\tchunk1\t44957\t60589\n")
        fH.write("BlastclustCluster1Mb2\tchunk3\t61296\t73986\n")
        fH.write("BlastclustCluster1Mb3\tchunk7\t123264\t132001\n")
        fH.write("BlastclustCluster1Mb4\tchunk5\t42363\t49343\n")
        fH.write("BlastclustCluster1Mb11\tchunk7\t121233\t127370\n")
        fH.write("BlastclustCluster2Mb1\tchunk7\t101523\t104351\n")
        fH.write("BlastclustCluster2Mb2\tchunk7\t99136\t100579\n")
        fH.write("BlastclustCluster2Mb3\tchunk7\t44114\t44978\n")
        fH.write("BlastclustCluster2Mb4\tchunk7\t57895\t58998\n")
        fH.write("BlastclustCluster3Mb1\tchunk4\t121910\t130435\n")
        fH.write("BlastclustCluster3Mb2\tchunk4\t123457\t123789\n")
        fH.write("BlastclustCluster3Mb3\tchunk4\t145999\t146452\n")
        fH.close()
        
    def _writeExpectedStatClust(self, expFileName):
        fH = open(expFileName, "w")
        fH.write("cluster\tsize\tmeanMbLength\tvarMbLength\tsdMbLength\tcvMbLength\n")
        fH.write("1.0\t1\t1093.000000\t0.000000\t0.000000\t0.000000\n")
        fH.write("1\t23\t3390.173913\t18307578.877470\t4278.735663\t1.262099\n")
        fH.write("1.3\t1\t1093.000000\t0.000000\t0.000000\t0.000000\n")
        fH.write("1.5\t1\t1093.000000\t0.000000\t0.000000\t0.000000\n")
        fH.write("2\t4\t1560.500000\t771592.333333\t878.403286\t0.562899\n")
        fH.write("2.5\t1\t1093.000000\t0.000000\t0.000000\t0.000000\n")
        fH.write("3\t3\t3104.333333\t22049512.333333\t4695.690826\t1.512625\n")
        fH.write("3.4\t1\t1093.000000\t0.000000\t0.000000\t0.000000\n")
        fH.write("4\t2\t6360.000000\t8000000.000000\t2828.427125\t0.444721\n")
        fH.write("5\t1\t8119.000000\t0.000000\t0.000000\t0.000000\n")
        fH.write("6\t1\t6742.000000\t0.000000\t0.000000\t0.000000\n")
        fH.write("7\t1\t6439.000000\t0.000000\t0.000000\t0.000000\n")
        fH.write("8\t1\t3082.000000\t0.000000\t0.000000\t0.000000\n")
        fH.write("9\t1\t1909.000000\t0.000000\t0.000000\t0.000000\n")
        fH.write("10\t1\t1612.000000\t0.000000\t0.000000\t0.000000\n")
        fH.close()
        
    def _writeExpectedStatSeq(self, expFileName):
        fH = open(expFileName, "w")
        fH.write("sequence\tclusters\tmeanMbLength\tvarMbLength\tsdMbLength\tcvMbLength\n")
        fH.write("chunk1\t3\t10144.000000\t23519401.000000\t4849.680505\t0.478084\n")
        fH.write("chunk2\t17\t1745.588235\t6448929.507353\t2539.474258\t1.454796\n")
        fH.write("chunk3\t3\t6225.666667\t31447745.333333\t5607.828932\t0.900760\n")
        fH.write("chunk4\t4\t2731.250000\t15256439.583333\t3905.949255\t1.430096\n")
        fH.write("chunk5\t2\t5031.500000\t7601100.500000\t2757.009340\t0.547950\n")
        fH.write("chunk6\t2\t4451.000000\t1198152.000000\t1094.601297\t0.245923\n")
        fH.write("chunk7\t11\t2416.636364\t6781490.654545\t2604.129539\t1.077584\n")
        fH.write("chunk8\t1\t4360.000000\t0.000000\t0.000000\t0.000000\n")
        fH.close()
        

if __name__ == "__main__":
    unittest.main()