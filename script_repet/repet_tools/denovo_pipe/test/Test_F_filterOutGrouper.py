import os
import time
import random
import shutil
import unittest
import subprocess

from devTools.CompareTwoFastaFiles import CompareTwoFastaFiles
from commons.core.checker.RepetException import RepetException
from commons.core.utils.FileUtils import FileUtils

class Test_F_filterOutGrouper(unittest.TestCase):
    
    def setUp(self):
        self._curTestDir = os.getcwd()
        self._testPrefix = 'test_F_FOG_{}_{}'.format(time.strftime("%H%M%S"),random.randint(0, 1000))
        os.makedirs(self._testPrefix)
        os.chdir(self._testPrefix)
        self._inputFastaName = "%s/Tools/filterOutGrouperTestInput.fa" % os.environ.get("REPET_DATA")
        self._outputFastaName = "%s.filtered" % os.path.basename(self._inputFastaName)
        self._inputFastaNameClusterMachingDefaultOptions = "filterOutGrouperTestInputClusterMachingDefaultOptions.fa"

    def tearDown(self):
        os.chdir(self._curTestDir)
        shutil.rmtree(self._testPrefix)

    def test_filterOutGrouper_3Elts_20seq(self):
        cmd = "filterOutGrouper.py "
        cmd += "-i %s " % self._inputFastaName
        cmd += "-o %s " % self._outputFastaName
        cmd += "-m 3 "
        cmd += "-M 20 "
        cmd += "-J -1 "
        cmd += "-L -1 "
        cmd += "-O -1 "
        cmd += "-v 0 "
        process = subprocess.Popen(cmd, shell = True)
        process.communicate()

        expFileName = "%s/Tools/filterOutGrouperTestExpected.fa" % os.environ.get("REPET_DATA")
        
        iCompare = CompareTwoFastaFiles()
        iCompare._testFileName = self._outputFastaName
        iCompare._refFileName = expFileName
        hasException = False
        try:
            iCompare.run()
        except RepetException:
            hasException = True
        
        self.assertFalse(hasException)

    def test_filterOutGrouper_1gr_3seq_only_J_option(self):
        self._writeinputFastaNameClusterMachingWithDefaultOptions()
        cmd = "filterOutGrouper.py "
        cmd += "-i %s " % self._inputFastaNameClusterMachingDefaultOptions
        cmd += "-o %s " % self._outputFastaName
        cmd += "-m 3 "
        cmd += "-M 20 "
        cmd += "-L -1 "
        cmd += "-O -1 "
        cmd += "-v 0 "
        process = subprocess.Popen(cmd, shell = True)
        process.communicate()

        self.assertTrue( FileUtils.isEmpty(self._outputFastaName))

    def test_filterOutGrouper_1gr_3seq_only_O_option(self):
        self._writeinputFastaNameClusterMachingWithDefaultOptions()
        cmd = "filterOutGrouper.py "
        cmd += "-i %s " % self._inputFastaNameClusterMachingDefaultOptions
        cmd += "-o %s " % self._outputFastaName
        cmd += "-m 3 "
        cmd += "-M 20 "
        cmd += "-J -1 "
        cmd += "-L -1 "
        cmd += "-v 0 "
        os.system(cmd)

        self.assertTrue( FileUtils.isEmpty(self._outputFastaName))

    def test_filterOutGrouper_1gr_3seq_only_L_option(self):
        self._writeinputFastaNameClusterMachingWithDefaultOptions()
        cmd = "filterOutGrouper.py "
        cmd += "-i %s " % self._inputFastaNameClusterMachingDefaultOptions
        cmd += "-o %s " % self._outputFastaName
        cmd += "-m 3 "
        cmd += "-M 20 "
        cmd += "-J -1 "
        cmd += "-O -1 "
        cmd += "-v 0 "
        process = subprocess.Popen(cmd, shell = True)
        process.communicate()

        self.assertTrue(FileUtils.isEmpty(self._outputFastaName))

#TODO: This test fails. MbQ87Gr7Cl0 is wrongly removed.
#It can be due to a bug: removed overlaps between two non-consecutive chunks? Removed overlaps between the same chunk?
    def test_filterOutGrouper_unchanged_cluster_bug_to_fix(self):
        inAndExpFasta = "inAndExpFasta.fa"
        self._writeCluster7(inAndExpFasta)
        
        cmd = "filterOutGrouper.py "
        cmd += "-i %s " % inAndExpFasta
        cmd += "-o %s " % self._outputFastaName
        cmd += "-v 0 "
        process = subprocess.Popen(cmd, shell = True)
        process.communicate()

        iCompare = CompareTwoFastaFiles()
        iCompare._testFileName = self._outputFastaName
        iCompare._refFileName = inAndExpFasta
        hasException = False
        try:
            iCompare.run()
        except RepetException:
            hasException = True

        self.assertFalse(hasException)

    def _writeinputFastaNameClusterMachingWithDefaultOptions(self):
        with open(self._inputFastaNameClusterMachingDefaultOptions, "w") as f:
            f.write(">MbQ58Gr21Cl0 chunk5 {Fragment} 194837..195877,50..30100\n")
            f.write("ACCAAAGACACTAGAATAACAAGATGCGTAACGCCATACGATTTTTTGGCACACTATTTT\n")
            f.write("TTCGCCGTGGCTCTAGAGGTGGCTCCAGGCTCTCTCGAATTTTTGTTAGAGAGCGAGAGA\n")
            f.write("GCTGAGAGCGCTACAGCGAACAGCTCTTTTCTACACATAAAGTGATAGCAGACAACTGTA\n")
            f.write("TGTGTGCACACGTGTGCTCATGCATTGTAAATTTGACAAAATATGCCCTTCACCTTCAAA\n")
            f.write("ATTCTTTGACTTTAAATCTATAATATTTTTGATCAATTGGCACCATGCGAAAAATTCTTG\n")
            f.write("TTTTGCATTGCCTAAACGTTATTATTATTTGAAAATAGATTAGAAATAGCCAAATCTATG\n")
            f.write("TACATATTATCACACAAATAAATTTCAAAAATGACTTTATATAAGAATATTTGTCATTAG\n")
            f.write("AGTATTCATCTTGCGGCGTGTGAAAAATTAATAAGGCAATGATGGTTGAGTGCTTGTGTC\n")
            f.write("CGCACTTCGTGCAAAGCAAAGACACTATAATAATATGAAATTTATGAAATTACAGTAGTT\n")
            f.write("TTAATAATTTCTATTGTACTTCCTTTAATTAATTAGTATATTTATTAAGTCATTTGACTT\n")
            f.write("AAAGTGATGTAACATTAGCATTAAAAGTGTTTCAAAAAAAATATTTCGCTTTTAAAAAAT\n")
            f.write("TGTCAGATGAGAGACAAATTAGAATTAAACGTAACAAATTTTAACAAACAAATTTAAAAA\n")
            f.write("CTTTAAAATTATAATAGTCAGGACGCGAATTTTTAAAAAAATTTTTATTTTATCATATTG\n")
            f.write("CTACGAAATTGGCAAAAACTACCCTAATATGTACAATGTAAATTCGTTTCTTCGATCAGA\n")
            f.write("ATTGATTTCGGTCCGAAAATCGTCTTCTAGCACAACGCGCACACATATACGCGTTCTCGT\n")
            f.write("CTCTTGTTTTTACTCACACAAGCAAGCAAACTCCGCGGGAGTGAGCGGAAAGAGAGTAAT\n")
            f.write("TTTGGCCGTCACCAAAAAAGTGGCTGCATAGTGCCAAACCAATGTATGGCCGTTACGCAT\n")
            f.write("CTTGTTATTCTAGTGTCTTTG\n")
            f.write(">MbQ60Gr21Cl0 chunk6 {Fragment} 4837..5877\n")
            f.write("ACCAAAGACACTAGAATAACAAGATGCGTAACGCCATACGATTTTTTGGCACACTATTTT\n")
            f.write("TTCGCCGTGGCTCTAGAGGTGGCTCCAGGCTCTCTCGAATTTTTGTTAGAGAGCGAGAGA\n")
            f.write("GCTGAGAGCGCTACAGCGAACAGCTCTTTTCTACACATAAAGTGATAGCAGACAACTGTA\n")
            f.write("TGTGTGCACACGTGTGCTCATGCATTGTAAATTTGACAAAATATGCCCTTCACCTTCAAA\n")
            f.write("ATTCTTTGACTTTAAATCTATAATATTTTTGATCAATTGGCACCATGCGAAAAATTCTTG\n")
            f.write("TTTTGCATTGCCTAAACGTTATTATTATTTGAAAATAGATTAGAAATAGCCAAATCTATG\n")
            f.write("TACATATTATCACACAAATAAATTTCAAAAATGACTTTATATAAGAATATTTGTCATTAG\n")
            f.write("AGTATTCATCTTGCGGCGTGTGAAAAATTAATAAGGCAATGATGGTTGAGTGCTTGTGTC\n")
            f.write("CGCACTTCGTGCAAAGCAAAGACACTATAATAATATGAAATTTATGAAATTACAGTAGTT\n")
            f.write("TTAATAATTTCTATTGTACTTCCTTTAATTAATTAGTATATTTATTAAGTCATTTGACTT\n")
            f.write("AAAGTGATGTAACATTAGCATTAAAAGTGTTTCAAAAAAAATATTTCGCTTTTAAAAAAT\n")
            f.write("TGTCAGATGAGAGACAAATTAGAATTAAACGTAACAAATTTTAACAAACAAATTTAAAAA\n")
            f.write("CTTTAAAATTATAATAGTCAGGACGCGAATTTTTAAAAAAATTTTTATTTTATCATATTG\n")
            f.write("CTACGAAATTGGCAAAAACTACCCTAATATGTACAATGTAAATTCGTTTCTTCGATCAGA\n")
            f.write("ATTGATTTCGGTCCGAAAATCGTCTTCTAGCACAACGCGCACACATATACGCGTTCTCGT\n")
            f.write("CTCTTGTTTTTACTCACACAAGCAAGCAAACTCCGCGGGAGTGAGCGGAAAGAGAGTAAT\n")
            f.write("TTTGGCCGTCACCAAAAAAGTGGCTGCATAGTGCCAAACCAATGTATGGCCGTTACGCAT\n")
            f.write("CTTGTTATTCTAGTGTCTTTG\n")
            f.write(">MbQ73Gr21Cl0 chunk7 {Fragment} 121444..1224850\n")
            f.write("ACCAAAGACACTAGAATAACAAGATGCGTAACGCCATACGATTTTTTGGCACACTATTTT\n")
            f.write("TTCGCCGTGGCTCTAGAGGTGGCTCCAGGCTCTCTCGAATTTTTGTTAGAGAGCGAGAGA\n")
            f.write("GCTGAGAGCGCTACAGCGAACAGCTCTTTTCTACACATAAAGTGATAGCAGACAACTGTA\n")
            f.write("TGTGTGCACACGTGTGCTCATGCATTGTAAATTTGACAAAATATGCCCTTCACCTTCAAA\n")
            f.write("GTTCTTTGACTTTAAATCTATAATATTTTTGATCAATTGGCACCATGCGAAAAATTCTTG\n")
            f.write("TTTTGCATTGCCTAAACGTTATTATTATTTGAAAATAGATTAGAAATAGCCAAATCTATG\n")
            f.write("TACATATTATCACACAAATAAATTTCAAAAATGACTTTATATAAGAATATTTGTCATTAG\n")
            f.write("AGTATTCATCTTGCGGCGTGTGAAAAATTAATAAGGCAATGATGGTTGAGTGCTTCTGTC\n")
            f.write("CGCACTTCGTGCAAAGCAAAGACACTATAATAATATGAAATTTATGAAATTACAGTAGTT\n")
            f.write("TTAATAATTTCTATTGTACTTGCTTTAATTAATTAGTATATTTATTAAGTCATTTGACTT\n")
            f.write("AAAGTGATGTAACATTAGCATTAAAAGTGTTTCAAAAAAAATATTTCGCTTTTAAAAAAT\n")
            f.write("TGTCAGATGAGAGACAAATTAGAATTAAACGTAACAAATTTTAACAAACAAATTTAAAAA\n")
            f.write("ACTTTAAAATTATAATAGTCAGGACGCGAATTTTTAAAAAAATTTTTATTTTATCATATT\n")
            f.write("GCTACGAAATTGGCAAAAACTACCCTAATATGTACAATGTAAATTCGTTTCTTCGATCAG\n")
            f.write("AATTGATTTCGGTCCGAAAATCGTCTTCTAGCACAACGCGCACACATATACGCGTTCTCG\n")
            f.write("TCTCTTGTTTTTACTCACACAAGCAAGCAAACTCCGCGGGAGTGAGCGGAAAGAGAGTAA\n")
            f.write("TTTTGGCCGTCACCAAAAAAGTGGCTGCATAGTGCCAAACCAATGTATGGCCGTTACGCA\n")
            f.write("TCTTGTTATTCTAGTGTCTTTG\n")
            
    def _writeCluster7(self, filename):
        with open(filename, 'w') as f:
            f.write(">MbQ85Gr7Cl0 chunk5 {Fragment} 191998..192948\n")
            f.write("CAAAGACACTAGAATAACAAGATGCGTAGCGGCCATACATTGGTTTGGCACTATGCAGCC\n")
            f.write("AATTTTTTAGTGACGGCCAAAATTGCTCTCTTTCCGCTCGCTCCCGCTGAGAGCATAAGA\n")
            f.write("AATCTAAAAATAGAATTTGCTTGCTTGTGTGAGTAAAAACAAGAGACGAGAACGCATATA\n")
            f.write("TGTGTGCGTGTTGTGCTAGAAGACGATTTTCGGACCGAAATCAATTCTGATCGAAAAAAC\n")
            f.write("GAATTTACATTGTACATATTAGGGTAGTTTTTGCCAATTTCGTAGCAATATGATAAAATA\n")
            f.write("AAATAATTTTTAACTGACTATTATCATTTTAAAGCTTTTTAAATTTGTTTGTTAAAATCG\n")
            f.write("CCGCTCGAATTAGCTACCGTTTACACATTTATATTTATGTTTAATTCTAATTTGTCTCTC\n")
            f.write("ATCTGACAATTTTTTAAGAAAGCGAAATATTTTTTTTTTTAAACACTTTTAATGTTAATG\n")
            f.write("TTACATCATATAAAGTCAAATGACATAATAAATATAGTAAATAATTAAATATGATAACTG\n")
            f.write("TTTATTGCAAAAGTCATATCAAAGACACTAGAATTATTCTAGTGTCTTTGCTTTGTTCAT\n")
            f.write("ATCTTGAAGCACGAAGTACGGACACAAGCACTCAACAATCATTGCCTTATTAATTTTTCA\n")
            f.write("CACGCCGCAAGATGAATACTCTAATGACAAATATTCTTATATAAAGTCATTTTTGAAATT\n")
            f.write("TATTTTTGTGATAATATGTACATAGATTTGGCTATTTCTAATCTATTTTCATATAATAAT\n")
            f.write("AACGTTAAGGCAATGCAAAACAAGAATTTTTCGCATGGTGCCAATTGATCAAAAATAATA\n")
            f.write("TAGATTTAAAGTCAAAGAACTTCTAAGGTGAAGGGCATATTTTGTGAAATTTACAATGCA\n")
            f.write("TGAGCGAGCATACGTGTGCACACATACAGTTGTCTGCTATCACTTTATGCG\n")
            f.write(">MbS86Gr7Cl0 chunk6 {Fragment} 26451..25499\n")
            f.write("GCAAAGACACTAGAATAACAAGATGCGTAACGGCCATACATTGGTTTTGCACTATGCAGC\n")
            f.write("CAATTTTTTAGTGACGGCCAAAATTGCTCTCTTTCCGCTCGCTCCCGCTGAGAGCATAAG\n")
            f.write("AAATCTAAAAATAGAATTTGCTTGCTTGTGTGAGTAAAAACAAGAGACGAGAACGCATAT\n")
            f.write("ATGTGTGCGTGTTGTGCTAGAAGACGATTTTCGGACCGAAATCAATTCTGATCAAAGAAA\n")
            f.write("CGAATTTACATTGTACATATTAGGGTAGTTTTTGTCAATTTCGTAGCAATATGATAAAAT\n")
            f.write("AAAATAATTTTTAACTGACTATTATCATTTTAAAGCTTTTTAAATTTGTTTGTTAAAATC\n")
            f.write("GCCGCTCGAATTAGCTACCGTTTACACATTTATATTTATGTTTAATTCTAATTTGTCTCT\n")
            f.write("CATCTGACAATTTTTTAAAAGCGAAATATTTTTTTTTTTAAACACTTTTAATGTTAATGT\n")
            f.write("TACATCATATTAAGTCAAATGACATAATAAATATAGTAAATAATTAAATATGATAACTGT\n")
            f.write("TTATTGCAAAAGTCATATCAAAGACACTAGAATTATTCTAGTGTCTTTGCTTTGTTCATA\n")
            f.write("TCTTGAAGCACGAAGTACGGACACAAGCACTCAACAATCATTGCCTTATTAATTTTTCAC\n")
            f.write("ACGCCGCAAGATGAATACTCTAATGACAAATATTCTTATATAAAGTCATTTTTGAAATTT\n")
            f.write("ATTTTTGTGATAATATGTACATAGATTTGGCTATTTCTAATCTATTTTCATATAATAATA\n")
            f.write("ACGTTAAGGCAATGCAAAACAAGAATTTTTCGCATGGTGCCAATTGATCAAAAATAATAT\n")
            f.write("AGATTTAAAGTCAAAGAACTTCTAAGGTGAAGGGCATATTTTGTGAAATTTACAATGCAT\n")
            f.write("GAGCATGCCTGTGTACACATACAGTTGTCTGCTATCACTTTATGCGTTAAAAA\n")
            f.write(">MbQ87Gr7Cl0 chunk6 {Fragment} 1998..2948\n")
            f.write("CAAAGACACTAGAATAACAAGATGCGTAGCGGCCATACATTGGTTTGGCACTATGCAGCC\n")
            f.write("AATTTTTTAGTGACGGCCAAAATTGCTCTCTTTCCGCTCGCTCCCGCTGAGAGCATAAGA\n")
            f.write("AATCTAAAAATAGAATTTGCTTGCTTGTGTGAGTAAAAACAAGAGACGAGAACGCATATA\n")
            f.write("TGTGTGCGTGTTGTGCTAGAAGACGATTTTCGGACCGAAATCAATTCTGATCGAAAAAAC\n")
            f.write("GAATTTACATTGTACATATTAGGGTAGTTTTTGCCAATTTCGTAGCAATATGATAAAATA\n")
            f.write("AAATAATTTTTAACTGACTATTATCATTTTAAAGCTTTTTAAATTTGTTTGTTAAAATCG\n")
            f.write("CCGCTCGAATTAGCTACCGTTTACACATTTATATTTATGTTTAATTCTAATTTGTCTCTC\n")
            f.write("ATCTGACAATTTTTTAAGAAAGCGAAATATTTTTTTTTTTAAACACTTTTAATGTTAATG\n")
            f.write("TTACATCATATAAAGTCAAATGACATAATAAATATAGTAAATAATTAAATATGATAACTG\n")
            f.write("TTTATTGCAAAAGTCATATCAAAGACACTAGAATTATTCTAGTGTCTTTGCTTTGTTCAT\n")
            f.write("ATCTTGAAGCACGAAGTACGGACACAAGCACTCAACAATCATTGCCTTATTAATTTTTCA\n")
            f.write("CACGCCGCAAGATGAATACTCTAATGACAAATATTCTTATATAAAGTCATTTTTGAAATT\n")
            f.write("TATTTTTGTGATAATATGTACATAGATTTGGCTATTTCTAATCTATTTTCATATAATAAT\n")
            f.write("AACGTTAAGGCAATGCAAAACAAGAATTTTTCGCATGGTGCCAATTGATCAAAAATAATA\n")
            f.write("TAGATTTAAAGTCAAAGAACTTCTAAGGTGAAGGGCATATTTTGTGAAATTTACAATGCA\n")
            f.write("TGAGCGAGCATACGTGTGCACACATACAGTTGTCTGCTATCACTTTATGCG\n")
            f.write(">MbS75Gr7Cl0 chunk7 {Fragment} 122567..123512\n")
            f.write("GCAAAGACACTAGAATAACAAGATGCGTAACGGCCATACATTGGTTTGGCACTATGCAGC\n")
            f.write("CACTTTTTTGGTGACTGCCAAAATTACTCTCTTCCGCTCACTCCCGCTGAGAGCGTAAGA\n")
            f.write("AATCTAAAAATATAATTTGCTTGCTTGTGTGAGTAAAAACAAGAGACGAGAACTCGTATA\n")
            f.write("AGTGTGCGTGTTGTGCTAGAAGACGATTTTCGGGCCGAAATCAATTCTGATCGAAGAAAC\n")
            f.write("GAATTTACATGGTACATATTAGGGTAGTTTTTGCCAATTTCCTAGCAATATGATAAAATA\n")
            f.write("AAAAATTTTTAAAAATTCGCGCCCTGACTATTATAATTTTAAAGCTTTTTAAATTTGTTT\n")
            f.write("GTTAAAATCGCCGCTCGAATTAGCTACCGTTTACACATTTATATTTATGTTTAATTCTAA\n")
            f.write("TTTGTCTCTCATCTGACAATTTTTTAAAAGCGAATTATTTTTTTTGAAACACTTTTAATG\n")
            f.write("TTAATGTTACATCATATTAAGTCAAATGATTTAATAAATATACTAAATAATTAAATATGA\n")
            f.write("TAACTGTTTATTGCAAAAGTAATATCAAAGACACTAGAATTATTCTAGTGTCTTTGCTTT\n")
            f.write("GTTCATATCTTGAGGCACGAAGTGCGGACACAAGCACTCAACAATCATTGCCTTGTTAAT\n")
            f.write("TTTTCACACGCCGCAAGCTGAATACTCTAATGACAAATATTCTAATATAAAGTCATTTTT\n")
            f.write("GTAATATTATGATTCGGCTATTACTATTTCTAAGCTATATTTAAATAATAATAACGTTAA\n")
            f.write("GGCAATGCAAAACAAGAATTTTTCGTATGATGCCAATTGATCAAAAATAATATAGATTTA\n")
            f.write("AAGTCTAAGAACTTCTAAGGTGAAGGGCATATTTTGTCAAATTTACAATGCATGAGCATA\n")
            f.write("CGTGTGCACACATACAGATGTCTGCTATCACTTTGTGCGTTGAAAA\n")
        
if __name__ == "__main__":
    unittest.main()