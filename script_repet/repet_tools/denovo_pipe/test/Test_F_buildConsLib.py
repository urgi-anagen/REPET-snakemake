import unittest
import os
from commons.core.utils.FileUtils import FileUtils

class Test_F_buildConsLib(unittest.TestCase):
    #TODO: finish tests for all cases
    
    def test_default_params_input_file_with_only_one_sequence(self):
        inputFileName = "blastclust_cluster_only_one_member.fa_aln"
        inputFilePath = "{}/Tools/{}".format(os.environ["REPET_DATA"], inputFileName)
        outFileName = "consensus.fa"
        os.symlink(inputFilePath, inputFileName)
        
        expFilePath = "{}/Tools/blastclust_cluster_only_one_member_consensus.fa".format(os.environ["REPET_DATA"])
        
        cmd = "buildConsLib.py -d $PWD -v 2"
        os.system(cmd)
        
        self.assertTrue(FileUtils.are2FilesIdentical(outFileName, expFilePath))
        
        os.remove(inputFileName)
        os.remove("{}.cons".format(inputFileName))
        os.remove(outFileName)
        os.remove("{}.log".format(outFileName))
    
    def test_default_params_input_file_with_only_one_sequence_more_than_40percent_of_N(self):
        inputFileName = "dummy.fa_aln"
        self._writeFastaFile_more_than_40percent_of_N(inputFileName)
        outFileName = "{}.cons".format(inputFileName)
        outCatFileName = "consensus.fa"
        
        cmd = "buildConsLib.py -d $PWD -v 2"
        os.system(cmd)
        
        self.assertFalse(FileUtils.isRessourceExists(outFileName))
        self.assertTrue(FileUtils.isEmpty(outCatFileName))
        
        os.remove(inputFileName)
        os.remove(outCatFileName)
        os.remove("{}.log".format(outCatFileName))
    
    def test_default_params_input_file_with_only_one_sequence_less_than_40percent_of_N(self):
        inputFileName = "dummy.fa_aln"
        self._writeFastaFile_less_than_40percent_of_N(inputFileName)
        outFileName = "{}.cons".format(inputFileName)
        outCatFileName = "consensus.fa"
        expFileName = "exp.fa"
        self._writeExpFastaFile_less_than_40percent_of_N(expFileName)
        
        cmd = "buildConsLib.py -d $PWD -v 2"
        os.system(cmd)
        
        self.assertTrue(FileUtils.isRessourceExists(outFileName))
        self.assertTrue(FileUtils.are2FilesIdentical(expFileName, outCatFileName))
        
        os.remove(inputFileName)
        os.remove(outCatFileName)
        os.remove(outFileName)
        os.remove(expFileName)
        os.remove("{}.log".format(outCatFileName))
        
    def _writeFastaFile_more_than_40percent_of_N(self, fileName):
        f = open(fileName, "w")
        f.write(">dummy\n")
        f.write("ATGNNNNNNNNNNC\n")
        f.close()
        
    def _writeFastaFile_less_than_40percent_of_N(self, fileName):
        f = open(fileName, "w")
        f.write(">dummy\n")
        f.write("ATGTCGCTATNNNC\n")
        f.close()
        
    def _writeExpFastaFile_less_than_40percent_of_N(self, fileName):
        f = open(fileName, "w")
        f.write(">consensus=dummy.fa_aln length=14 nbAlign=1\n")
        f.write("ATGTCGCTATNNNC\n")
        f.close()
        
if __name__ == "__main__":
    unittest.main()