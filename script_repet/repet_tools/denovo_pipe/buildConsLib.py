#!/usr/bin/env python

import os
import sys
import logging

from commons.core.seq.FastaUtils import FastaUtils
from commons.core.seq.BioseqDB import BioseqDB
from commons.tools.dbConsensus import dbConsensus


#def buildconsLib(targetDir="", regexp="*.fa_aln", minBase=1, minPropNt=0.0, outFileName="consensus.fa",header_SATannot=False, verbose=0):
def buildconsLib(file, minBase=1, minPropNt=0.0, header_SATannot=False, verbose=0, logFileName = "buildconsLib.log"):
    """
    This program builds a consensus for each '.fa_aln' file in the target directory and concatenate them in a fasta file.
    """

    # TODO: maxPropN in parameter ?
    maxPropN = 0.4
    if not os.path.exists(file):
        print("ERROR: target directory doesn't exist")
        sys.exit(1)

    if verbose > 0:
        print("START buildConsLib.py")
        sys.stdout.flush()

    #logFileName = "{}.log".format(outFileName)
    if os.path.exists(logFileName):
        os.remove(logFileName)
    handler = logging.FileHandler(logFileName)
    formatter = logging.Formatter("%(asctime)s %(levelname)s: %(message)s")
    handler.setFormatter(formatter)
    logging.getLogger('').addHandler(handler)
    logging.getLogger('').setLevel(logging.DEBUG)
    logging.info("started")
    #
    # currDir = os.getcwd()
    #
    # logging.info("target directory: {}".format(targetDir))

    logging.info("min bases to edit a consensus: {}".format(minBase))
    logging.info("min proportion to add nucleotide: {:.2f}".format(minPropNt))
    # logging.info("regexp: {}".format(regexp))
    #
    # if verbose > 0:
    #     print("target directory: {}".format(targetDir))
    #     print("retrieve files via '{}'".format(regexp))
    #     sys.stdout.flush()
    #os.chdir(targetDir)

    #if os.path.exists(outFileName):
    #    os.remove(outFileName)

    #lFiles = glob.glob(os.path.join(targetDir, regexp))
    #lFiles.sort()
    #if verbose > 0:
    #    print("initial nb of consensus: {}".format(len(lFiles)))
    #logging.info("initial nb of consensus: {}".format(len(lFiles)))
    #for f in lFiles:

    if verbose > 0:
        logging.info("processing '{}'...".format(file))
        sys.stdout.flush()

    if FastaUtils.dbSize(file) > 1:

        dbConsensus(inFileName=file, minNbNt=minBase, minPropNt=minPropNt, outFileName="", header_SATannot=header_SATannot, verbose=verbose - 1)

        # if log != 0:
        #     print("ERROR: {} returned {}".format(prg, log))
        #     sys.exit(1)
    elif os.stat(file).st_size == 0:
        logging.info("Empty files : {}".format(file))
        with open("{}.cons".format(file), "a"):
            pass
    else:
        iBioseqDB = BioseqDB(file)
        iBioseqDB.upCase()
        iBioseq = iBioseqDB.db[0]
        if iBioseq.propNt("N") < maxPropN:
            length = iBioseq.getLength()
            if header_SATannot:
                header = iBioseq.getHeader()
                pyramid = header.split("Gr")[1].split("Cl")[0]
                pile = header.split("Cl")[1].split(" ")[0]
                iBioseq.setHeader(
                    "consensus={} length={} nbAlign=1 pile={} pyramid={}".format(file, length, pile, pyramid))
            else:
                iBioseq.setHeader("consensus={} length={} nbAlign=1".format(file, length))
            with open("{}.cons".format(file), "w") as fileHandler:
                iBioseqDB.write(fileHandler)


    #lConsensusFiles = glob.glob("*.cons")
    #lConsensusFiles.sort()
    #if verbose > 0:
    #    print("final nb of consensus: {}".format(len(lConsensusFiles)))
    #logging.info("final nb of consensus: {}".format(len(lConsensusFiles)))

    #FileUtils.catFilesByPattern("*.cons", outFileName)

    #os.chdir(currDir)

    logging.info("finished")

    if verbose > 0:
        logging.info(("END buildConsLib.py"))
        #sys.stdout.flush()

    return 0

"""
if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Build consensus library',
        formatter_class=argparse.RawTextHelpFormatter, add_help=True)
    parser.add_argument('-d', '--directory', required=True, help="absolute path to the target directory")
    parser.add_argument('-r', '--regexp', default="*.fa_aln",
                        help="regexp to retrieve the file in the target directory (default=\"*.fa_aln\")")
    parser.add_argument('-n', '--minBase', type=int, default=1,
                        help="minimum number of nucleotides in a column to edit a consensus (default=1)")
    parser.add_argument('-p', '--minPropNt', type=float, default=0.0,
                        help="minimum proportion for the major nucleotide to be used, otherwise add 'N' (default=0.0)")
    parser.add_argument('-o', '--output', default="consensus.fa",
                        help="output file name for the consensus (default=consensus.fa)")
    parser.add_argument('-H', action='store_true',
                        help="format the header with pyramid and piles informations (SATannot)")
    parser.add_argument('-v', '--verbose', default=0, type=int, help="verbose (default=0/1/2)")

    args = parser.parse_args()
    targetDir = args.directory
    regexp = args.regexp
    minBase = args.minBase
    minPropNt = args.minPropNt
    outFileName = args.output
    header_SATannot = args.H
    verbose = args.verbose

    buildconsLib(targetDir, regexp, minBase, minPropNt, outFileName, header_SATannot, verbose)
"""
