import unittest
import os
import time
import copy

from annot_pipe.LongJoinsForTEs import LongJoinsForTEs
from commons.core.coord.Path import Path
from commons.core.sql.DbFactory import DbFactory
from commons.core.sql.TablePathAdaptator import TablePathAdaptator
from commons.core.coord.PathUtils import PathUtils

class Test_LongJoinsForTEs( unittest.TestCase ):
    
    def setUp( self ):
        self._i = LongJoinsForTEs()
        self._uniqId = "{}_{}".format(time.strftime("%Y%m%d%H%M%S"), os.getpid())
        self._db = DbFactory.createInstance()
        self._table = "dummyPathTable_{}".format(self._uniqId)
        
    def tearDown( self ):
        self._db.dropTable(self._table)
        self._db.close()
        
    def test_getDistanceBetweenRanges_onDirectStrand(self):
        p1 = Path()
        p1.setFromTuple( ( "1", "qry1", "1", "100", "sbj1", "1", "100", "0.0", "239", "90.0" ) )
        p2 = Path()
        p2.setFromTuple( ( "2", "qry1", "121", "300", "sbj1", "101", "280", "0.0", "176", "91.2" ) )
        distBetweenRangesFromQ = self._i.getDistanceBetweenRanges(p1.range_query, p2.range_query)
        expDist = 21
        self.assertEqual(expDist, distBetweenRangesFromQ)
        distBetweenRangesFromS = self._i.getDistanceBetweenRanges(p1.range_subject, p2.range_subject)
        expDist = 1        
        self.assertEqual(expDist, distBetweenRangesFromS)

    def test_getDistanceBetweenRanges_onNonDirectStrandForQ(self):
        p1 = Path()
        p1.setFromTuple( ( "1", "qry1", "1", "100", "sbj1", "100", "1", "0.0", "239", "90.0" ) )
        rQFromP1 = p1.getQueryAsRange()
        rQFromP1.reverse()
        p2 = Path()
        p2.setFromTuple( ( "2", "qry1", "121", "300", "sbj1", "280", "101", "0.0", "176", "91.2" ) )
        rQFromP2 = p2.getQueryAsRange()
        rQFromP2.reverse()
        distBetweenRangesFromQ = self._i.getDistanceBetweenRanges(rQFromP1, rQFromP2)
        expDist = 21
        self.assertEqual(expDist, distBetweenRangesFromQ)

        p1 = Path()
        p1.setFromTuple( ( "1", "qry1", "121", "300", "sbj1", "100", "1", "0.0", "239", "90.0" ) )
        rQFromP1 = p1.getQueryAsRange()
        rQFromP1.reverse()
        p2 = Path()
        p2.setFromTuple( ( "2", "qry1", "1", "100", "sbj1", "280", "101", "0.0", "176", "91.2" ) )
        rQFromP2 = p2.getQueryAsRange()
        rQFromP2.reverse()
        distBetweenRangesFromQ = self._i.getDistanceBetweenRanges(rQFromP1, rQFromP2)
        expDist = 21        
        self.assertEqual(expDist, distBetweenRangesFromQ)

    def test_getDistanceBetweenRanges_2rangesInOppStrand(self):
        p1 = Path()
        p1.setFromTuple( ( "1", "qry1", "1", "100", "sbj1", "100", "1", "0.0", "239", "90.0" ) )
        p2 = Path()
        p2.setFromTuple( ( "2", "qry1", "300", "121", "sbj1", "280", "101", "0.0", "176", "91.2" ) )
        self.assertRaises(Exception,self._i.getDistanceBetweenRanges(p1.range_query, p2.range_query))
                        
    def test_getDistanceBetweenQueries_directStrand( self ):
        p1 = Path()
        p1.setFromTuple( ( "1", "qry1", "1", "100", "sbj1", "1", "100", "0.0", "239", "90.0" ) )
        p2 = Path()
        p2.setFromTuple( ( "2", "qry1", "121", "350", "sbj1", "101", "200", "0.0", "176", "91.2" ) )
        self._db.createTable( self._table, "path", overwrite = True )
        tpA = TablePathAdaptator( self._db, self._table )
        for p in [ p1, p2 ]: tpA.insert( p )
        exp = 21
        obs = self._i.getDistanceBetweenRanges( p1.range_query, p2.range_query )
        self.assertEqual( obs, exp )
        obs = self._i.getDistanceBetweenRanges( p2.range_query, p1.range_query )
        self.assertEqual( obs, exp )
        
    def test_getDistanceBetweenQueries_reverseStrand( self ):
        p1 = Path()
        p1.setFromTuple( ( "1", "qry1", "100", "1", "sbj1", "1", "100", "0.0", "239", "90.0" ) )
        p2 = Path()
        p2.setFromTuple( ( "2", "qry1", "350", "121", "sbj1", "101", "200", "0.0", "176", "91.2" ) )
        self._db.createTable( self._table, "path", overwrite = True )
        tpA = TablePathAdaptator( self._db, self._table )
        for p in [ p1, p2 ]: tpA.insert( p )
        exp = 21
        obs = self._i.getDistanceBetweenRanges( p1.range_query, p2.range_query )
        self.assertEqual( obs, exp )
        obs = self._i.getDistanceBetweenRanges( p2.range_query, p1.range_query )
        self.assertEqual( obs, exp )
        
    def test_getDistanceBetweenSubjects_directStrand( self ):
        p1 = Path()
        p1.setFromTuple( ( "1", "qry1", "1", "100", "sbj1", "1", "100", "0.0", "239", "90.0" ) )
        p2 = Path()
        p2.setFromTuple( ( "2", "qry1", "121", "350", "sbj1", "111", "200", "0.0", "176", "91.2" ) )
        self._db.createTable( self._table, "path", overwrite = True )
        tpA = TablePathAdaptator( self._db, self._table )
        for p in [ p1, p2 ]: tpA.insert( p )
        exp = 11
        obs = self._i.getDistanceBetweenRanges( p1.range_subject, p2.range_subject )
        self.assertEqual( obs, exp )
        obs = self._i.getDistanceBetweenRanges( p2.range_subject, p1.range_subject )
        self.assertEqual( obs, exp )
        
    def test_getDistanceBetweenSubjects_reverseStrand( self ):
        p1 = Path()
        p1.setFromTuple( ( "1", "qry1", "1", "100", "sbj1", "100", "1", "0.0", "239", "90.0" ) )
        p2 = Path()
        p2.setFromTuple( ( "2", "qry1", "121", "350", "sbj1", "200", "111", "0.0", "176", "91.2" ) )
        self._db.createTable( self._table, "path", overwrite = True )
        tpA = TablePathAdaptator( self._db, self._table )
        for p in [ p1, p2 ]: tpA.insert( p )
        exp = 11
        obs = self._i.getDistanceBetweenRanges( p1.range_subject, p2.range_subject )
        self.assertEqual( obs, exp )
        obs = self._i.getDistanceBetweenRanges( p2.range_subject, p1.range_subject )
        self.assertEqual( obs, exp )
        
    def test_getDiagonalsGapAndMismatch_SbjDir( self ):
        p1 = Path()
        p1.setFromTuple( ( "1", "qry1", "1", "100", "sbj1", "1", "100", "0.0", "239", "90.0" ) )
        p2 = Path()
        p2.setFromTuple( ( "2", "qry1", "121", "300", "sbj1", "101", "280", "0.0", "176", "91.2" ) )
        distBtwQry = 20
        distBtwSbj = 2
        diag1, diag2, gap, mismatch = 0, -20, 20, 2
        obs1, obs2, obs3, obs4 = self._i.getDiagonalsGapAndMismatch( p1, p2, distBtwQry, distBtwSbj )
        self.assertEqual( obs1, diag1 )
        self.assertEqual( obs2, diag2 )
        self.assertEqual( obs3, gap )
        self.assertEqual( obs4, mismatch )
        
    def test_getDiagonalsGapAndMismatch_SbjRev( self ):
        p1 = Path()
        p1.setFromTuple( ( "1", "qry1", "1", "100", "sbj1", "100", "1", "0.0", "239", "90.0" ) )
        p2 = Path()
        p2.setFromTuple( ( "2", "qry1", "121", "300", "sbj1", "280", "101", "0.0", "176", "91.2" ) )
        distBtwQry = 20
        distBtwSbj = 2
        diag1, diag2, gap, mismatch = 0, -20, 20, 2
        obs1, obs2, obs3, obs4 = self._i.getDiagonalsGapAndMismatch( p1, p2, distBtwQry, distBtwSbj )
        self.assertEqual( obs1, diag1 )
        self.assertEqual( obs2, diag2 )
        self.assertEqual( obs3, gap )
        self.assertEqual( obs4, mismatch )
        
        #TODO: check if it's possible ? case distBetweenQueries<=0 and all other test case
               
    def test_joinTEpathsPerQuerySubject_sameStrandDir_connect( self ):
        p1 = Path()
        p1.setFromTuple( ( "1", "qry1", "1", "100", "sbj1", "1", "100", "0.0", "239", "90.0" ) )
        p2 = Path()
        p2.setFromTuple( ( "2", "qry1", "121", "350", "sbj1", "101", "200", "0.0", "176", "91.2" ) )
        self._db.createTable( self._table, "path", overwrite = True )
        tpA = TablePathAdaptator( self._db, self._table )
        for p in [ p1, p2 ]: tpA.insert( p )
        p3 = Path()
        p3.setFromTuple( ( "1", "qry1", "121", "350", "sbj1", "101", "200", "0", "176", "91.2" ) )  # connect p2 with p1
        lExp = [ p1, p3 ]
        
        self._i._inTable = self._table
        self._i._db = self._db
        self._i._tpA = tpA
        self._i._verbose = 0
        lPaths = copy.deepcopy( [ p1, p2 ] )
        self._i.joinTEpathsPerQuerySubject( "qry1", "sbj1", lPaths, [] )
        
        lObs = tpA.getListOfAllPaths()
        
        lSortedExp = PathUtils.getPathListSortedByIncreasingMinQueryThenMaxQuery( lExp )
        lSortedObs = PathUtils.getPathListSortedByIncreasingMinQueryThenMaxQuery( lObs )
        
        self.assertEqual( lSortedObs, lSortedExp )
        
        
    def test_joinTEpathsPerQuerySubject_SbjRevStrand_connect( self ):
        p1 = Path()
        p1.setFromTuple( ( "1", "qry1", "1", "100", "sbj1", "100", "1", "0.0", "239", "90.0" ) )
        p2 = Path()
        p2.setFromTuple( ( "2", "qry1", "121", "350", "sbj1", "200", "101", "0.0", "176", "91.2" ) )
        self._db.createTable( self._table, "path", overwrite = True )
        tpA = TablePathAdaptator( self._db, self._table )
        for p in [ p1, p2 ]: tpA.insert( p )
        p3 = Path()
        p3.setFromTuple( ( "1", "qry1", "121", "350", "sbj1", "200", "101", "0.0", "176", "91.2" ) )  # connect p2 with p1
        lExp = [ p1, p3 ]
        
        self._i._inTable = self._table
        self._i._db = self._db
        self._i._tpA = tpA
        self._i._verbose = 0
        lPaths = copy.deepcopy( [ p1, p2 ] )
        self._i.joinTEpathsPerQuerySubject( "qry1", "sbj1", lPaths, [] )
        
        lObs = tpA.getListOfAllPaths()
        lSortedExp = PathUtils.getPathListSortedByIncreasingMinQueryThenMaxQuery( lExp )
        lSortedObs = PathUtils.getPathListSortedByIncreasingMinQueryThenMaxQuery( lObs )
        self.assertEqual( lSortedObs, lSortedExp )
        
        
    def test_joinTEpathsPerQuerySubject_dontconnect_identity( self ):
        p1 = Path()
        p1.setFromTuple( ( "1", "qry1", "1", "100", "sbj1", "1", "100", "0.0", "239", "90.0" ) )
        p2 = Path()
        p2.setFromTuple( ( "2", "qry1", "121", "350", "sbj1", "101", "200", "0.0", "176", "93.0" ) )  # too big difference in identity
        self._db.createTable( self._table, "path", overwrite = True )
        tpA = TablePathAdaptator( self._db, self._table )
        for p in [ p1, p2 ]: tpA.insert( p )
        lExp = [ p1, p2 ]
        self._i._inTable = self._table
        self._i._db = self._db
        self._i._tpA = tpA
        self._i._identityTolerance = 2
        self._i._verbose = 0
        lPaths = copy.deepcopy( [ p1, p2 ] )
        self._i.joinTEpathsPerQuerySubject( "qry1", "sbj1", lPaths, [] )
        lObs = tpA.getListOfAllPaths()
        lSortedExp = PathUtils.getPathListSortedByIncreasingMinQueryThenMaxQuery( lExp )
        lSortedObs = PathUtils.getPathListSortedByIncreasingMinQueryThenMaxQuery( lObs )
        self.assertEqual( lSortedObs, lSortedExp )
        
        
    def test_joinTEpathsPerQuerySubject_connect_weightedIdentity( self ):
        p1 = Path()
        p1.setFromTuple( ( "1", "qry1", "1", "100", "sbj1", "1", "100", "0.0", "239", "90.0" ) )
        p2 = Path()
        p2.setFromTuple( ( "2", "qry1", "121", "350", "sbj1", "101", "200", "0.0", "176", "92.5" ) )  # too big difference in identity but ok when weighted
        self._db.createTable( self._table, "path", overwrite = True )
        tpA = TablePathAdaptator( self._db, self._table )
        for p in [ p1, p2 ]: tpA.insert( p )
        p3 = Path()
        p3.setFromTuple( ( "1", "qry1", "121", "350", "sbj1", "101", "200", "0.0", "176", "92.5" ) )
        lExp = [ p1, p3 ]
        self._i._inTable = self._table
        self._i._db = self._db
        self._i._tpA = tpA
        self._i._identityTolerance = 2
        self._i._verbose = 0
        lPaths = copy.deepcopy( [ p1, p2 ] )
        self._i.joinTEpathsPerQuerySubject( "qry1", "sbj1", lPaths, [] )
        lObs = tpA.getListOfAllPaths()
        lSortedExp = PathUtils.getPathListSortedByIncreasingMinQueryThenMaxQuery( lExp )
        lSortedObs = PathUtils.getPathListSortedByIncreasingMinQueryThenMaxQuery( lObs )
        self.assertEqual( lSortedObs, lSortedExp )
        
        
    def test_joinTEpathsPerQuerySubject_connect_nestedTEs( self ):
        p1 = Path()
        p1.setFromTuple( ("1", "qry1", "1", "100", "sbj1", "1", "100", "0.0", "239", "90.0" ) )
        p2 = Path()
        p2.setFromTuple( ("2", "qry1", "5121", "5350", "sbj1", "101", "200", "0.0", "176", "91.2" ) )
        p3 = Path()
        p3.setFromTuple( ("3", "qry1", "108", "5118", "sbj1", "101", "5200", "0.0", "7634", "97.2" ) )
        self._db.createTable( self._table, "path", overwrite = True )
        tpA = TablePathAdaptator( self._db, self._table )
        for p in [ p1, p2, p3 ]: tpA.insert( p )
        p4 = Path()
        p4.setFromTuple( ("1", "qry1", "5121", "5350", "sbj1", "101", "200", "0.0", "176", "91.2" ) )
        lExp = [ p1, p3, p4 ]
        self._i._inTable = self._table
        self._i._db = self._db
        self._i._tpA = tpA
        self._i._verbose = 0
        lPaths = copy.deepcopy( [ p1, p2 ] )
        self._i.joinTEpathsPerQuerySubject( "qry1", "sbj1", lPaths, [] )
        lObs = tpA.getListOfAllPaths()
        lSortedExp = PathUtils.getPathListSortedByIncreasingMinQueryThenMaxQuery( lExp )
        lSortedObs = PathUtils.getPathListSortedByIncreasingMinQueryThenMaxQuery( lObs )
        self.assertEqual( lSortedObs, lSortedExp )
        
        
    def test_joinTEpathsPerQuerySubject_dontconnect_coverageNestedTEs( self ):
        p1 = Path()
        p1.setFromTuple( ("1", "qry1", "1", "100", "sbj1", "1", "100", "0.0", "239", "90.0" ) )
        p2 = Path()
        p2.setFromTuple( ("2", "qry1", "5121", "5350", "sbj1", "101", "200", "0.0", "176", "91.2" ) )
        p3 = Path()
        p3.setFromTuple( ("3", "qry1", "2300", "3700", "sbj1", "1", "1400", "0.0", "1762", "97.2" ) )
        self._db.createTable( self._table, "path", overwrite = True )
        tpA = TablePathAdaptator( self._db, self._table )
        for p in [ p1, p2, p3 ]: tpA.insert( p )
        lExp = [ p1, p2, p3 ]
        self._i._inTable = self._table
        self._i._db = self._db
        self._i._tpA = tpA
        self._i._verbose = 0
        lPaths = copy.deepcopy( [ p1, p2 ] )
        self._i.joinTEpathsPerQuerySubject( "qry1", "sbj1", lPaths, [] )
        lObs = tpA.getListOfAllPaths()
        lSortedExp = PathUtils.getPathListSortedByIncreasingMinQueryThenMaxQuery( lExp )
        lSortedObs = PathUtils.getPathListSortedByIncreasingMinQueryThenMaxQuery( lObs )
        self.assertEqual( lSortedObs, lSortedExp )
        
        
    def test_joinTEpathsPerQuerySubject_dontconnect_identityNestedTEs( self ):
        p1 = Path()
        p1.setFromTuple( ("1", "qry1", "1", "100", "sbj1", "1", "100", "0.0", "239", "92.0" ) )
        p2 = Path()
        p2.setFromTuple( ("2", "qry1", "5121", "5350", "sbj1", "101", "200", "0.0", "176", "91.2" ) )
        p3 = Path()
        p3.setFromTuple( ("3", "qry1", "390", "5100", "sbj1", "1", "4765", "0.0", "1762", "87.8" ) )
        self._db.createTable( self._table, "path", overwrite = True )
        tpA = TablePathAdaptator( self._db, self._table )
        for p in [ p1, p2, p3 ]: tpA.insert( p )
        lExp = [ p1, p2, p3 ]
        self._i._inTable = self._table
        self._i._db = self._db
        self._i._tpA = tpA
        self._i._verbose = 0
        lPaths = copy.deepcopy( [ p1, p2 ] )
        self._i.joinTEpathsPerQuerySubject( "qry1", "sbj1", lPaths, [] )
        lObs = tpA.getListOfAllPaths()
        lSortedExp = PathUtils.getPathListSortedByIncreasingMinQueryThenMaxQuery( lExp )
        lSortedObs = PathUtils.getPathListSortedByIncreasingMinQueryThenMaxQuery( lObs )
        self.assertEqual( lSortedObs, lSortedExp )
        
        
    def test_joinTEpathsPerQuerySubject_connect_identityNestedTEs( self ):
        p1 = Path()
        p1.setFromTuple( ("1", "qry1", "1", "100", "sbj1", "1", "100", "0.0", "239", "92.0" ) )
        p2 = Path()
        p2.setFromTuple( ("2", "qry1", "5121", "5350", "sbj1", "101", "200", "0.0", "176", "91.2" ) )
        p3 = Path()
        p3.setFromTuple( ("3", "qry1", "200", "310", "sbj1", "1", "120", "0.0", "116", "82.8" ) )  # low identity but short
        p4 = Path()
        p4.setFromTuple( ("4", "qry1", "320", "5100", "sbj1", "1", "4365", "0.0", "4762", "99.8" ) )  # high identity and long
        self._db.createTable( self._table, "path", overwrite = True )
        tpA = TablePathAdaptator( self._db, self._table )
        for p in [ p1, p2, p3, p4 ]: tpA.insert( p )
        p5 = Path()
        p5.setFromTuple( ("1", "qry1", "5121", "5350", "sbj1", "101", "200", "0.0", "176", "91.2" ) )  # connect p2 with p1
        lExp = [ p1, p3, p4, p5 ]
        self._i._inTable = self._table
        self._i._db = self._db
        self._i._tpA = tpA
        self._i._verbose = 0
        lPaths = copy.deepcopy( [ p1, p2 ] )
        self._i.joinTEpathsPerQuerySubject( "qry1", "sbj1", lPaths, [] )
        lObs = tpA.getListOfAllPaths()
        lSortedExp = PathUtils.getPathListSortedByIncreasingMinQueryThenMaxQuery( lExp )
        lSortedObs = PathUtils.getPathListSortedByIncreasingMinQueryThenMaxQuery( lObs )
        self.assertEqual( lSortedObs, lSortedExp )
        
        
    def test_joinTEpathsPerQuerySubject_connect_identityNestedTEs_identity_upper100( self ):
        p1 = Path()
        p1.setFromTuple( ("1", "qry1", "1", "100", "sbj1", "1", "100", "0.0", "239", "92.0" ) )
        p2 = Path()
        p2.setFromTuple( ("2", "qry1", "5121", "5350", "sbj1", "101", "200", "0.0", "176", "105.10" ) )
        p3 = Path()
        p3.setFromTuple( ("3", "qry1", "200", "310", "sbj1", "1", "120", "0.0", "116", "82.8" ) )
        p4 = Path()
        p4.setFromTuple( ("4", "qry1", "320", "5100", "sbj1", "1", "4365", "0.0", "4762", "102.05" ) )
        self._db.createTable( self._table, "path", overwrite = True )
        tpA = TablePathAdaptator( self._db, self._table )
        for p in [ p1, p2, p3, p4, ]: tpA.insert( p )
        lExp = [ p1, p3, p4, p2 ]
        self._i._inTable = self._table
        self._i._db = self._db
        self._i._tpA = tpA
        self._i._verbose = 0
        lPaths = copy.deepcopy( [ p1, p2 ] )
        self._i.joinTEpathsPerQuerySubject( "qry1", "sbj1", lPaths, [] )
        lObs = tpA.getListOfAllPaths()
        lSortedExp = PathUtils.getPathListSortedByIncreasingMinQueryThenMaxQuery( lExp )
        lSortedObs = PathUtils.getPathListSortedByIncreasingMinQueryThenMaxQuery( lObs )
        self.assertEqual( lSortedObs, lSortedExp )
        
        
    def test_joinTEpathsPerQuerySubject_connect_identityNestedTEs_identity_under0( self ):
        p1 = Path()
        p1.setFromTuple( ("1", "qry1", "1", "100", "sbj1", "1", "100", "0.0", "239", "-90.02" ) )
        p2 = Path()
        p2.setFromTuple( ("2", "qry1", "5121", "5350", "sbj1", "101", "200", "0.0", "176", "5.10" ) )
        p3 = Path()
        p3.setFromTuple( ("3", "qry1", "200", "310", "sbj1", "1", "120", "0.0", "116", "2.8" ) )
        p4 = Path()
        p4.setFromTuple( ("4", "qry1", "320", "5100", "sbj1", "1", "4365", "0.0", "4762", "2.05" ) )
        self._db.createTable( self._table, "path", overwrite = True )
        tpA = TablePathAdaptator( self._db, self._table )
        for p in [ p1, p2, p3, p4, ]: tpA.insert( p )
        self._i._inTable = self._table
        self._i._db = self._db
        self._i._tpA = tpA
        self._i._verbose = 0
        lPaths = copy.deepcopy( [ p1, p2 ] )
        isExceptionRaised = False
        try:
            self._i.joinTEpathsPerQuerySubject( "qry1", "sbj1", lPaths, [] )
        except:
            isExceptionRaised = True
        self.assertTrue(isExceptionRaised)
        
        
    def test_join_connect_youngerNestedTEs( self ):
        p1 = Path()
        p1.setFromTuple( ( "29", "qry14", "1", "100", "sbj1", "1", "100", "0.0", "162", "95.2" ) )
        p2 = Path()
        p2.setFromTuple( ( "30", "qry14", "151", "5700", "sbj2", "1", "5600", "0.0", "5135", "97.3" ) )
        p3 = Path()
        p3.setFromTuple( ( "31", "qry14", "5751", "5800", "sbj1", "101", "150", "0.0", "64", "95.6" ) )
        self._db.createTable( self._table, "path", overwrite = True )
        tpA = TablePathAdaptator( self._db, self._table )
        for p in [ p1, p2, p3 ]: tpA.insert( p )
        p4 = Path()
        p4.setFromTuple( ( "29", "qry14", "5751", "5800", "sbj1", "101", "150", "0.0", "64", "95.6" ) )  # connect p3 with p1
        lExp = [ p1, p2, p4 ]
        self._i._inTable = self._table
        self._i._db = self._db
        self._i._tpA = tpA
        self._i._verbose = 0
        self._i.join()
        lObs = tpA.getListOfAllPaths()
        lSortedExp = PathUtils.getPathListSortedByIncreasingMinQueryThenMaxQuery( lExp )
        lSortedObs = PathUtils.getPathListSortedByIncreasingMinQueryThenMaxQuery( lObs )
        self._db.dropTable("{}_range".format(self._table))
        self.assertEqual( lSortedObs, lSortedExp )
        
        
    def test_joinAndSplit_connect_youngerNestedTEs( self ):
        p1 = Path()
        p1.setFromTuple( ( "29", "qry14", "1", "100", "sbj1", "1", "100", "0.0", "162", "95.2" ) )
        p2 = Path()
        p2.setFromTuple( ( "30", "qry14", "151", "5700", "sbj2", "1", "5600", "0.0", "5135", "97.3" ) )
        p3 = Path()
        p3.setFromTuple( ( "31", "qry14", "5751", "5800", "sbj1", "101", "150", "0.0", "64", "95.6" ) )
        self._db.createTable( self._table, "path", overwrite = True )
        tpA = TablePathAdaptator( self._db, self._table )
        for p in [ p1, p2, p3 ]: tpA.insert( p )
        p4 = Path()
        p4.setFromTuple( ( "29", "qry14", "5751", "5800", "sbj1", "101", "150", "0.0", "64", "95.6" ) )  # connect p3 with p1
        lExp = [ p1, p2, p4 ]
        
        self._i._inTable = self._table
        self._i._outTable = self._table + "_out"
        self._i._db = self._db
        self._i._tpA = tpA
        self._i._verbose = 0
        self._i.join()
        self._i.split()
        tpA._table = self._i._outTable
        lObs = tpA.getListOfAllPaths()
        self._db.dropTable(self._i._outTable)
        
        lSortedExp = PathUtils.getPathListSortedByIncreasingMinQueryThenMaxQuery( lExp )
        lSortedObs = PathUtils.getPathListSortedByIncreasingMinQueryThenMaxQuery( lObs )
        self.assertEqual( lSortedObs, lSortedExp )
        
        
    def test_join_connect_2connectedHitsJoinedWithAThird( self ):
        p1 = Path()
        p1.setFromTuple( ( "32", "qry15", "1", "100", "sbj1", "1", "100", "0.0", "239", "90.0" ) )
        p2 = Path()
        p2.setFromTuple( ( "32", "qry15", "121", "200", "sbj1", "101", "180", "0.0", "176", "91.2" ) )
        p3 = Path()
        p3.setFromTuple( ( "33", "qry15", "231", "500", "sbj1", "201", "470", "0.0", "384", "91.4" ) )
        self._db.createTable( self._table, "path", overwrite = True )
        tpA = TablePathAdaptator( self._db, self._table )
        for p in [ p1, p2, p3 ]: tpA.insert( p )
        p4 = Path()
        p4.setFromTuple( ( "32", "qry15", "231", "500", "sbj1", "201", "470", "0.0", "384", "91.4" ) )  # connect p3 with p1-p2
        lExp = [ p1, p2, p4 ]
        self._i._inTable = self._table
        self._i._db = self._db
        self._i._tpA = tpA
        self._i._verbose = 0
        self._i.join()
        lObs = tpA.getListOfAllPaths()
        lSortedExp = PathUtils.getPathListSortedByIncreasingMinQueryThenMaxQuery( lExp )
        lSortedObs = PathUtils.getPathListSortedByIncreasingMinQueryThenMaxQuery( lObs )
        self._db.dropTable("{}_range".format(self._table))
        self.assertEqual( lSortedObs, lSortedExp )
        
        
    def test_joinAndSplit_connect_2connectedHitsJoinedWithAThird( self ):
        p1 = Path()
        p1.setFromTuple( ( "32", "qry15", "1", "100", "sbj1", "1", "100", "0.0", "239", "90.0" ) )
        p2 = Path()
        p2.setFromTuple( ( "32", "qry15", "121", "200", "sbj1", "101", "180", "0.0", "176", "91.2" ) )
        p3 = Path()
        p3.setFromTuple( ( "33", "qry15", "231", "500", "sbj1", "201", "470", "0.0", "384", "91.4" ) )
        self._db.createTable( self._table, "path", overwrite = True )
        tpA = TablePathAdaptator( self._db, self._table )
        for p in [ p1, p2, p3 ]: tpA.insert( p )
        p4 = Path()
        p4.setFromTuple( ( "32", "qry15", "231", "500", "sbj1", "201", "470", "0.0", "384", "91.4" ) )  # connect p3 with p1-p2
        lExp = [ p1, p2, p4 ]
        
        self._i._inTable = self._table
        self._i._outTable = self._table + "_out"
        self._i._db = self._db
        self._i._tpA = tpA
        self._i._verbose = 0
        self._i.join()
        self._i.split()
        tpA._table = self._i._outTable
        lObs = tpA.getListOfAllPaths()
        self._db.dropTable(self._i._outTable)
        
        lSortedExp = PathUtils.getPathListSortedByIncreasingMinQueryThenMaxQuery( lExp )
        lSortedObs = PathUtils.getPathListSortedByIncreasingMinQueryThenMaxQuery( lObs )
        self.assertEqual( lSortedObs, lSortedExp )
        
    def test_split_sameStrand( self ):
        self._db.createTable( self._table, "path", overwrite = True )
        outTable = "dummyOutTable1_{}".format(self._uniqId)
        self._db.createTable( outTable, "path", overwrite = True )
        p1a = Path()
        p1a.setFromTuple( ( "1", "qry1", "11", "500", "sbj1", "1", "490", "0.0", "132", "98.0" ) )  # young
        p2a = Path()
        p2a.setFromTuple( ( "2", "qry1", "505", "700", "sbj2", "1", "200", "0.0", "36", "96.2" ) )  # old
        p1b = Path()
        p1b.setFromTuple( ( "1", "qry1", "721", "1000", "sbj1", "491", "800", "0.0", "117", "96.5" ) )  # young
        p2b = Path()
        p2b.setFromTuple( ( "2", "qry1", "1201", "1800", "sbj2", "201", "820", "0.0", "856", "93.2" ) )  # old
        tpA = TablePathAdaptator( self._db, self._table )
        for p in [ p1a, p2a, p1b, p2b ]: tpA.insert( p )
        p3 = Path()
        p3.setFromTuple( ( "3", "qry1", "721", "1000", "sbj1", "491", "800", "0.0", "117", "96.5" ) )  # split p1a and p1b because their connection overlaps with an older hit
        lExp = [ p1a, p2a, p2b, p3 ]
        iLJFT = LongJoinsForTEs(inTable = self._table, outTable = outTable, verbose = 5)
        iLJFT._db = self._db
        iLJFT._tpA = tpA
        iLJFT.split()
        # self._i._inTable = self._table
        # self._i._db = self._db
        # self._i._tpA = tpA
        # self._i._verbose = 0
        # self._i._outTable = outTable
        # self._i.split()
        tpA._table = outTable
        lObs = tpA.getListOfAllPaths()
        
        lSortedExp = PathUtils.getPathListSortedByIncreasingMinQueryThenMaxQuery( lExp )
        lSortedObs = PathUtils.getPathListSortedByIncreasingMinQueryThenMaxQuery( lObs )
        self.assertEqual( lSortedObs, lSortedExp )
        self._db.dropTable( outTable )

    def test_split_differentStrands( self ):
        self._db.createTable( self._table, "path", overwrite = True )
        p1a = Path()  # subject on reverse strand
        p1a.setFromTuple( ( "1", "qry1", "11", "500", "sbj1", "490", "1", "0.0", "132", "98.0" ) )  # young
        p2a = Path()
        p2a.setFromTuple( ( "2", "qry1", "505", "700", "sbj2", "200", "1", "0.0", "36", "96.2" ) )  # old
        p1b = Path()  # subject on reverse strand
        p1b.setFromTuple( ( "1", "qry1", "721", "1000", "sbj1", "800", "491", "0.0", "117", "96.5" ) )  # young
        p2b = Path()
        p2b.setFromTuple( ( "2", "qry1", "1201", "1800", "sbj2", "820", "201", "0.0", "856", "93.2" ) )  # old
        tpA = TablePathAdaptator( self._db, self._table )
        for p in [ p1a, p2a, p1b, p2b ]: tpA.insert( p )
        p3 = Path()
        p3.setFromTuple( ( "3", "qry1", "721", "1000", "sbj1", "800", "491", "0.0", "117", "96.5" ) )  # split p1a and p1b because their connection overlaps with an older hit
        lExp = [ p1a, p2a, p2b, p3 ]
        
        self._i._inTable = self._table
        self._i._db = self._db
        self._i._tpA = tpA
        self._i._verbose = 0
        outTable = "dummyOutTable3_{}".format(self._uniqId)
        self._db.createTable( outTable, "path", overwrite = True )
        self._i._outTable = outTable
        self._i.split()
        tpA._table = outTable
        lObs = tpA.getListOfAllPaths()
        
        lSortedExp = PathUtils.getPathListSortedByIncreasingMinQueryThenMaxQuery( lExp )
        lSortedObs = PathUtils.getPathListSortedByIncreasingMinQueryThenMaxQuery( lObs )
        self.assertEqual( lSortedObs, lSortedExp )
        self._db.dropTable( outTable )

    def test_split_DontSplitBecauseTooShort( self ):
        self._db.createTable( self._table, "path", overwrite = True )
        p1a = Path()
        p1a.setFromTuple( ( "1", "qry1", "11", "500", "sbj1", "1", "490", "0.0", "132", "98.0" ) )  # young
        p2a = Path()
        p2a.setFromTuple( ( "2", "qry1", "505", "600", "sbj2", "1", "200", "0.0", "36", "96.2" ) )  # old but too short
        p1b = Path()
        p1b.setFromTuple( ( "1", "qry1", "721", "1000", "sbj1", "491", "800", "0.0", "117", "96.5" ) )  # young
        p2b = Path()
        p2b.setFromTuple( ( "2", "qry1", "1201", "1800", "sbj2", "201", "820", "0.0", "856", "93.2" ) )  # old
        tpA = TablePathAdaptator( self._db, self._table )
        for p in [ p1a, p2a, p1b, p2b ]: tpA.insert( p )
        lExp = [ p1a, p1b, p2a, p2b ]
        
        self._i._inTable = self._table
        self._i._db = self._db
        self._i._tpA = tpA
        self._i._verbose = 5
        self._i.setLogger("logFile.txt")
        outTable = "dummyOutTable4_{}".format(self._uniqId)
        self._db.createTable( outTable, "path", overwrite = True )
        self._i._outTable = outTable
        self._i.split()
        tpA._table = outTable
        lObs = tpA.getListOfAllPaths()
        
        lSortedExp = PathUtils.getPathListSortedByIncreasingMinQueryThenMaxQuery( lExp )
        lSortedObs = PathUtils.getPathListSortedByIncreasingMinQueryThenMaxQuery( lObs )
        self.assertEqual( lSortedObs, lSortedExp )
        self._db.dropTable( outTable )
        
    def test_split_overlappingNotIncluded( self ):
        self._db.createTable( self._table, "path", overwrite = True )
        p1a = Path()
        p1a.setFromTuple( ( "1", "qry1", "11", "500", "sbj1", "1", "490", "0.0", "132", "98.0" ) )  # young
        p2 = Path()
        p2.setFromTuple( ( "2", "qry1", "480", "700", "sbj2", "1", "200", "0.0", "36", "96.2" ) )  # overlapping not included
        p1b = Path()
        p1b.setFromTuple( ( "1", "qry1", "721", "1000", "sbj1", "491", "800", "0.0", "117", "96.5" ) )  # young
        tpA = TablePathAdaptator( self._db, self._table )
        for p in [ p1a, p1b, p2 ]: tpA.insert( p )
        lExp = [ p1a, p1b, p2 ]
        
        self._i._inTable = self._table
        self._i._db = self._db
        self._i._tpA = tpA
        outTable = "dummyOutTable2_{}".format(self._uniqId)
        self._db.createTable( outTable, "path", overwrite = True )
        self._i._outTable = outTable
        self._i.split()
        tpA._table = outTable
        lObs = tpA.getListOfAllPaths()
        
        lSortedExp = PathUtils.getPathListSortedByIncreasingMinQueryThenMaxQuery( lExp )
        lSortedObs = PathUtils.getPathListSortedByIncreasingMinQueryThenMaxQuery( lObs )
        self.assertEqual( lSortedObs, lSortedExp )
        self._db.dropTable( outTable )

    def test_split_DontKeepBecauseTooShort( self ):
        self._db.createTable( self._table, "path", overwrite = True )
        p1a = Path()
        p1a.setFromTuple( ( "1", "qry1", "11", "20", "sbj1", "1", "10", "0.0", "132", "98.0" ) )  # too short after split
        p2 = Path()
        p2.setFromTuple( ( "2", "qry1", "30", "150", "sbj2", "1", "30", "0.0", "36", "86.0" ) )  # long-enough older included
        p1b = Path()
        p1b.setFromTuple( ( "1", "qry1", "152", "190", "sbj1", "11", "50", "0.0", "117", "98.0" ) )
        tpA = TablePathAdaptator( self._db, self._table )
        for p in [ p1a, p1b, p2 ]: tpA.insert( p )
        
        p3 = Path()
        p3.setFromTuple( ( "3", "qry1", "152", "190", "sbj1", "11", "50", "0.0", "117", "98.0" ) )  # same as p1b except pathnum
        lExp = [ p2, p3 ]
        
        self._i._inTable = self._table
        self._i._db = self._db
        self._i._tpA = tpA
        self._i._verbose = 0
        outTable = "dummyOutTable_{}".format(self._uniqId)
        self._db.createTable( outTable, "path", overwrite = True )
        self._i._outTable = outTable
        self._i.split()
        tpA._table = outTable
        lObs = tpA.getListOfAllPaths()
        
        lSortedExp = PathUtils.getPathListSortedByIncreasingMinQueryThenMaxQuery( lExp )
        lSortedObs = PathUtils.getPathListSortedByIncreasingMinQueryThenMaxQuery( lObs )
        self.assertEqual( lSortedExp, lSortedObs )
        self._db.dropTable( outTable )
        
                
if __name__ == "__main__":
        unittest.main()