from commons.core.utils.FileUtils import FileUtils
from annot_pipe.ComputeScoreThresholdFromAlignFiles import ComputeScoreThresholdFromAlignFiles
import shutil
import unittest
import os

class Test_F_ComputeScoreThresholdFromAlignFiles( unittest.TestCase ):
    
    def setUp( self ):
        self.blrData = [ [12,50,25],[17,87,2,45,130], [56, 23, 1], [3, 3, 5, 6, 13] ]
        self.cenData = [ [7,7,2,5,0], [156, 123, 111], [123, 123, 125, 156, 131], [] ]
        self.rmData = [ [17,87,2],[117,8,21,45,131, 123, 123], [23, 23, 25, 56, 131],[] ]
          
        self.expBlrThresh = 87
        self.expCenThresh = 156
        self.expRmThresh = 131
        self.expThresholdFileName = 'expThreshold.tmp'
        self.obsThresholdFileName = 'threshold.tmp'

        self._makeAlignFiles()
        self._makeExpThresholdResultFile()

    def tearDown( self ):
        shutil.rmtree("tmp")
        os.remove(self.obsThresholdFileName)
        os.remove(self.expThresholdFileName)
    
    def _makeAlignFiles (self):
        os.mkdir("tmp")
        os.chdir("tmp")
        os.makedirs('BLR')
        os.chdir('BLR')
        for index, scoreList in enumerate(self.blrData):
            with open('batch_00%d_shuffle.fa.align' %index, 'w') as alignFile:
                for score in scoreList:
                    alignFile.write('a\ta\ta\ta\ta\ta\ta\t{}\ta\n'.format(score))
        os.chdir('..')
        os.makedirs('CEN')
        os.chdir('CEN')
        for index, scoreList in enumerate(self.cenData):
            with open('batch_00%d_shuffle.fa.align' %index, 'w') as alignFile:
                for score in scoreList:
                    alignFile.write('a\ta\ta\ta\ta\ta\ta\t{}\ta\n'.format(score))
        os.chdir('..')
        os.makedirs('RM')
        os.chdir('RM')
        for index, scoreList in enumerate(self.rmData):
            with open('batch_00%d_shuffle.fa.align' %index, 'w') as alignFile:
                for score in scoreList:
                    alignFile.write('a\ta\ta\ta\ta\ta\ta\t{}\ta\n'.format(score))
        os.chdir("../..")

    def _makeExpThresholdResultFile(self):        
        with open( self.expThresholdFileName, 'w') as f:
            f.write('BLR\t{}\n'.format(self.expBlrThresh))
            f.write('CEN\t{}\n'.format(self.expCenThresh))
            f.write('RM\t{}\n'.format(self.expRmThresh))

    def _makeExpThresholdResultFile_BLR0(self):        
        with open( self.expThresholdFileName, 'w') as f:
            f.write('BLR\t0\n')
            f.write('CEN\t{}\n'.format(self.expCenThresh))
            f.write('RM\t{}\n'.format(self.expRmThresh))

    def test_ComputeScoreThresholdFromAlignFiles(self):
        iComputeScoreThresholdFromAlignFiles = ComputeScoreThresholdFromAlignFiles(dataDirName = "tmp", quantile = 0.95)
        iComputeScoreThresholdFromAlignFiles.run()
        self.assertTrue(FileUtils.are2FilesIdentical(self.expThresholdFileName, self.obsThresholdFileName))
        
    def test_ComputeScoreThresholdFromAlignFilesAsScript(self):
        os.system('ComputeScoreThresholdFromAlignFiles.py -d tmp -q 0.95')
        self.assertTrue(FileUtils.are2FilesIdentical(self.expThresholdFileName, self.obsThresholdFileName))
        
    def test_ComputeScoreThresholdFromAlignFilesAsScript_withEmptyAlignFileInBLRDir(self):
        shutil.rmtree("tmp/BLR")
        os.chdir("tmp")
        os.mkdir("BLR")
        os.chdir("BLR")
        os.system("touch batch_1.align")
        os.system("touch batch_2.align")
        os.chdir("../..")
        os.remove("expThreshold.tmp")
        self._makeExpThresholdResultFile_BLR0()
        
        os.system('ComputeScoreThresholdFromAlignFiles.py -d tmp -q 0.95 -v 5')
        self.assertTrue(FileUtils.are2FilesIdentical(self.expThresholdFileName, self.obsThresholdFileName))
        
    