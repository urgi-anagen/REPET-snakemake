import os
import subprocess
import time
import shutil
import operator
import unittest
from annot_pipe.LongJoinsForTEs import LongJoinsForTEs
from commons.core.coord.Path import Path
from commons.core.sql.DbFactory import DbFactory
from commons.core.sql.TablePathAdaptator import TablePathAdaptator


class Test_F_LongJoinsForTEs(unittest.TestCase):

    def setUp(self):
        self._cDir = os.getcwd()
        self._uniqId = "{}_{}".format(time.strftime("%Y%m%d%H%M%S"), os.getpid())
        self._workDir = "TestLJProcedure_{}".format(self._uniqId)
        if not os.path.exists(self._workDir):
            os.makedirs(self._workDir)
            os.chdir(self._workDir)
        self._db = DbFactory.createInstance()
        self._table = "dummyPathTable_{}".format(self._uniqId)
        self._pathFileName = "LongJoinsForTEs_input.path"
        self._expPathFileName = "LongJoinsForTEs_exp.path"

        os.symlink("{}/datas/{}".format(self._cDir, self._pathFileName), self._pathFileName)
        os.symlink("{}/datas/{}".format(self._cDir, self._expPathFileName), self._expPathFileName)
        self._db.createTable(self._table, "path", self._pathFileName, overwrite=True)

    def tearDown(self):

        os.chdir(self._cDir)
        try:
            shutil.rmtree(os.path.join(os.getcwd(), self._workDir))
        except:
            pass
        self._db.dropTable(self._table)
        self._db.dropTable("{}_out".format(self._table))
        self._db.close()

    def test_runAsScript(self):
        cmd = "LongJoinsForTEs.py"
        cmd += " -t {}".format(self._table)
        cmd += " -g 5000"
        cmd += " -m 500"
        cmd += " -i 2"
        cmd += " -c 0.95"
        cmd += " -o 15"
        cmd += " -s 100"
        cmd += " -O {}_out".format(self._table)
        cmd += " -v 0"
        subprocess.call(cmd, shell=True)
        lExp = []
        with open(self._expPathFileName, "r") as expF:
            for line in expF.readlines():
                p = Path()
                p.setFromString(line)
                lExp.append(p)

        tpA = TablePathAdaptator(self._db, self._table + "_out")
        lObs = tpA.getListOfAllPaths()
        self._db.dropTable(self._table + "_out")
        lExp.sort(key=operator.attrgetter("id"))
        lObs.sort(key=operator.attrgetter("id"))
        obsFileName = "LongJoinsForTEs_obs.path"
        with open(obsFileName, "w") as obsF:
            for p in lObs:
                p.write(obsF)

        self.assertEqual(lObs, lExp)

    def test_run(self):
        iLJFP = LongJoinsForTEs(self._table, "{}_out".format(self._table), 5)
        iLJFP.run()
        lExp = []
        with open("LongJoinsForTEs_exp.path", "r") as expF:
            for line in expF.readlines():
                p = Path()
                p.setFromString(line)
                lExp.append(p)

        tpA = TablePathAdaptator(self._db, self._table + "_out")
        lObs = tpA.getListOfAllPaths()
        lExp.sort(key=operator.attrgetter("id"))
        lObs.sort(key=operator.attrgetter("id"))
        obsFileName = "LongJoinsForTEs_obs.path"
        with open(obsFileName, "w") as obsF:
            for p in lObs:
                p.write(obsF)

        self.assertEqual(lObs, lExp)

if __name__ == "__main__":
    unittest.main()
