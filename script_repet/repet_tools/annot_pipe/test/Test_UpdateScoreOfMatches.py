import unittest
import os
import subprocess
from commons.core.coord.Align import Align
from commons.core.utils.FileUtils import FileUtils
from annot_pipe.UpdateScoreOfMatches import UpdateScoreOfMatches


class Test_UpdateScoreOfMatches(unittest.TestCase):
    
    def setUp( self ):
        self._i = UpdateScoreOfMatches()
        
    def tearDown( self ):
        pass
    
    def test_zLaunchAsScript( self ):
        cDir = os.getcwd()
        
        iAlign = Align()
        
        inFile = "dummyInFile.align"
        inHandler = open( inFile, "w" )
        iAlign.setFromString( "query1\t1\t100\tsubject1\t1\t95\t1e-180\t230\t90.2\n" )
        iAlign.write( inHandler )
        inHandler.close()
        
        expFile = "dummyExpFile.align"
        expHandler = open( expFile, "w" )
        iAlign.setFromString("query1\t1\t100\tsubject1\t1\t95\t1e-180\t{}\t90.2\n".format((100 - 1 + 1) * 90.2 / 100.0))
        iAlign.write( expHandler )
        expHandler.close()
        
        obsFile = "dummyObsFile.align"
        
        cmd = "python ../UpdateScoreOfMatches.py"
        cmd += " -i {}".format( inFile )
        cmd += " -f align"
        cmd += " -o {}".format( obsFile )
        cmd += " -v {}".format( 0 )
        subprocess.call(cmd, shell=True)
        
        self.assertTrue( FileUtils.are2FilesIdentical( expFile, obsFile ) )
        
        for f in [ inFile, expFile, obsFile ]:
            os.remove( f )
            
        os.chdir( cDir )
        
        
if __name__ == "__main__":
        unittest.main()
