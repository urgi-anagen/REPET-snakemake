#!/usr/bin/env python

# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.


from commons.core.coord.SetUtils import SetUtils
from commons.core.coord.Set import Set
import os
import sys
import getopt
import pandas
from pathlib import Path
from collections import defaultdict
import multiprocessing as mp
from multiprocessing import Process, Queue, Pool, cpu_count
import time


# TODO: to test !
# => RunFunctionalTests.py -n TEannot -S 5 -c
# In results:
# (AATGTTT)4      dmel_chr4       76023   76339
# (TAA)n#Simple_repeat    dmel_chr4       76279   76107
# # Why keep the second set ?
def getLineToWrite(lSets):
    lMergedSets = SetUtils.mergeSetsInList(lSets)
    lMergedSetsWithoutDuplicated = SetUtils.getSetListWithoutDuplicates(lMergedSets)
    return lMergedSetsWithoutDuplicated


def srptAutoPromote(filename="", tableType="", outTable="", verbose=0):
    if filename == "" or outTable == "":
        print("*** Error: missing compulsory options")
        sys.exit(1)

    if verbose > 0:
        print("\nbeginning of srptAutoPromote")
        sys.stdout.flush()

    df = pandas.read_csv(filename, sep='\t', delimiter=None, header=None, index_col=None)

    data = defaultdict(list)
    vect_df = df.values
    if tableType == "set":
        for row in vect_df:
            data[row[2]].append(Set(id=row[0], name=row[1], seqname=row[2], start=row[3], end=row[4]))

    elif tableType == "path":
        for row in vect_df:
            data[row[1]].append(Set(id=row[0], name=row[4], seqname=row[1], start=row[2], end=row[3]))
    else:
        print("*** Error: table type {} not implemented".format(tableType))
        sys.exit(1)

    # premiere creation de la base
    if not os.path.exists(outTable):
        lQueryNames = list(data.keys())
        nb = mp.cpu_count()
        pool = Pool(nb)
        # start_time_for = time.time()
        results = pool.map(getLineToWrite, [data[queryName] for queryName in lQueryNames])
        # print("for_if--- %s seconds ---" % (time.time() - start_time_for))

        # start_time_for = time.time()
        for i in results:
            SetUtils.writeListInFile(i, outTable, "a")

    else:
        tmp = pandas.read_csv(outTable, sep='\t', delimiter=None, header=None, index_col=None)
        os.remove(outTable)
        tmp_file_data = defaultdict(list)

        for row in tmp.values:
            tmp_file_data[row[2]].append(Set(id=row[0], name=row[1], seqname=row[2], start=row[3], end=row[4]))

        new_id = (tmp[0].max()) + 1
        lInQueryNames = list(data.keys())
        lOutQueryNames = list(tmp_file_data.keys())

        for inQueryName in lInQueryNames:
            lInSets = data[inQueryName]
            lOutSets = tmp_file_data[inQueryName]

            if len(lOutSets) > 0:
                lOutQueryNames.remove(inQueryName)

            lInSets = SetUtils.mergeSetsInList(lInSets)
            lMergedSets, newid = SetUtils.getListOfMergedSetsAndNextId(lOutSets, lInSets, new_id)

            lMergedSetsWithoutDuplicated = SetUtils.getSetListWithoutDuplicates(lMergedSets)
            SetUtils.writeListInFile(lMergedSetsWithoutDuplicated, outTable, "a")

        # print("for1--- %s seconds ---" % (time.time() - start_time_for))

        # print("lOutQueryNames",len(lOutQueryNames))
        for outQueryName in lOutQueryNames:
            if verbose > 1:
                print("remain sequence", outQueryName)

            lOutSets = tmp_file_data[outQueryName]
            dId2SetsList = SetUtils.getDictOfListsWithIdAsKey(lOutSets)
            lMergedSets = []
            for lSets in dId2SetsList.values():
                SetUtils.changeIdInList(lSets, new_id)
                lSetsWithoutDuplicated = SetUtils.getSetListWithoutDuplicates(lSets)
                lMergedSets.extend(lSetsWithoutDuplicated)
                newid += 1
            SetUtils.writeListInFile(lSetsWithoutDuplicated, outTable, "a")

    if verbose > 0:
        print("{} finished successfully\n".format(sys.argv[0].split("/")[-1]))
        sys.stdout.flush()

    return 0


# ----------------------------------------------------------------------------


"""
if __name__ == '__main__':
    import time 
    start_time = time.time()
    #os.remove('/home/centos/REPET-snakemake/test_TRF_MREPS.txt')
    srptAutoPromote('/home/centos/REPET-snakemake/AthaGenome_chk_TRF_set.tsv',"set", "/home/centos/REPET-snakemake/test_TRF_MREPS.txt")
    print("TRF--- %s seconds ---" % (time.time() - start_time))    

    start_time = time.time()
    srptAutoPromote('/home/centos/REPET-snakemake/AthaGenome_chk_Mreps_set.tsv',"set", "/home/centos/REPET-snakemake/test_TRF_MREPS.txt")
    print("Mreps--- %s seconds ---" % (time.time() - start_time))    

    #start_time = time.time()
    #srptAutoPromote('/home/mwan/REPET/REPET-snakemake/TEannot_snakemake/test/AthaGenome_chk_RMSSR_path.tsv',"path", "/home/mwan/REPET/REPET-snakemake/TEannot_snakemake/test/test.txt")
    #print("RMSSR--- %s seconds ---" % (time.time() - start_time))    
"""
"""
import pandas
#obs = "/home/mwan/REPET/REPET-snakemake/TEannot_snakemake/test/test.txt"
#exp="/home/mwan/REPET/REPET-snakemake/TEannot_snakemake/test/test_set_to_chr/AthaGenome_chk_allSSRs_set.tsv"
obs = "/home/centos/REPET-snakemake//test_TRF_MREPS.txt"
exp="/home/centos/REPET-snakemake/AthaGenome_chk_allSSRs_set2.tmp8724-AthaGenome_chk_Mreps_set_old"

# OK lorsque c'est seulement TRF
#obs = "/home/mwan/REPET/REPET-snakemake/TEannot_snakemake/test/test_TRF.txt"
#exp="/home/mwan/REPET/REPET-snakemake/TEannot_snakemake/test/AthaGenome_chk_allSSRs_set_trf.tsv"

df_expected = pandas.read_csv(exp, sep='\t', header=None, index_col=0, low_memory=False)
df_observed = pandas.read_csv(obs, sep='\t', header=None, index_col=0, low_memory=False)

expected = df_expected[[2,3,4]].sort_values(by=[2 ,3]).reset_index(drop=True)
observed = df_observed[[2,3,4]].sort_values(by=[2 ,3]).reset_index(drop=True)
#print(expected.compare(observed))
print(expected==(observed)).all()
"""