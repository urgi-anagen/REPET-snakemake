#!/usr/bin/env python

# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import os
import glob
from commons.core.LoggerFactory import LoggerFactory
from commons.core.coord.AlignUtils import AlignUtils
from commons.core.utils.RepetOptionParser import RepetOptionParser

LOG_DEPTH = "repet.annot_pipe"

class ComputeScoreThresholdFromAlignFiles(object):
    
    def __init__(self, dataDirName="", list_files_cen = list(),list_files_rm = list(),list_files_blr = list(), quantile = 0.95, verbosity = 1, log_name="ComputeScoreThresholdFromAlignFiles.log",output_name=""):
        self._dataDirName = dataDirName
        self._quantile = quantile
        #self._list_files_cen = list_files_cen
        #self._list_files_rm = list_files_rm
        #self._list_files_blr = list_files_blr
        if not list_files_cen and not list_files_rm and not list_files_blr:
            self._bool = False
        elif list_files_cen or list_files_rm or list_files_blr :
            self._bool = True

        self._list_files = {"BLR": list_files_blr, "CEN": list_files_cen, "RM": list_files_rm}
        self._verbosity = verbosity
        self._output_name = output_name
        #self._log = LoggerFactory.createLogger("{}.{}".format(LOG_DEPTH, self.__class__.__name__), self._verbosity)
        self._log = LoggerFactory.createLogger("{}.{}".format((LOG_DEPTH, self.__class__.__name__),  self._verbosity),
                                               log_name)
        
    def setAttributesFromCmdLine(self):
        description = "Launch ComputeScoreThresholdFromAlignFiles.py"
        epilog = "\n"
        parser = RepetOptionParser(description = description, epilog = epilog)
        parser.add_option("-d", "--datadirname",    dest = "datadirname",   action = "store",       type = "string",    help = "input data dir name [compulsory] ", default = ".")
        parser.add_option("-q", "--quantile",       dest = "quantile",      action = "store",       type = "float",     help = "quantile [compulsory] [default: 0.95]", default = 0.95)
        parser.add_option("-v", "--verbosity",      dest = "verbosity",     action = "store",       type = "int",       help = "verbosity [optional] [default: 1]", default = 1)
        options = parser.parse_args()[0]
        self._setAttributesFromOptions(options)
        
    def _setAttributesFromOptions(self, options):
        self._dataDirName = options.datadirname
        self._quantile = options.quantile
        self._verbosity = options.verbosity
        
    def setDatadirname(self, datadirname):
        self._dataDirName = datadirname
        
    def setQuantile(self, quantile):
        self._quantile = quantile
        
    def setVerbosity(self, verbosity):
        self._verbosity = verbosity
        
    def _checkOptions(self):
        if self._dataDirName == "" and self._bool == False:
            self._logAndRaise("ERROR: please provide a correct data directory or list of files")

        if self._dataDirName != "" and self._bool:
            self._logAndRaise("ERROR: please provide a correct data directory or list of files")
               
        if self._quantile >1 or self._quantile <=0:
            self._logAndRaise("ERROR: quantile must be between 0 and 1")

        if self._output_name == "":
            self._output_name = "threshold.tmp"

            
    def _logAndRaise(self, errorMsg):
        self._log.error(errorMsg)
        raise Exception(errorMsg)

    def _computeQuantile( self, lValues, percentage):
        n = len(lValues)
        lValues.sort()
        if n == 0:
            return 0
        elif percentage == 1:
            return max(lValues)
        else:
            index = int(round(n * percentage))-1
            if index < 0:
                return lValues[0] 
            return lValues[index]        

                    
    def run(self):
        LoggerFactory.setLevel(self._log, self._verbosity)
        self._checkOptions()
        self._log.info("START ComputeScoreThresholdFromAlignFiles")
        self._log.debug("Quantile: {}".format(self._quantile))
        
        with open(self._output_name, 'w') as f:
            if self._dataDirName != "":
                for alignProgram in ["BLR", "CEN", "RM"]:
                    folder = "{}/{}" .format (self._dataDirName, alignProgram)
                    if not os.path.exists(folder):
                        self._log.info("WARNING: {} doesn't exist, did you run the previous step ?".format(alignProgram))
                    else:
                        lFiles = glob.glob("{}/*.align".format(folder))
                        self._log.debug("parse {} files from {}..." .format (len(lFiles), folder))
                        lScores = []
                        extend = lScores.extend
                        getScore = AlignUtils.getScoreListFromFile
                        for fileName in lFiles:
                            extend(getScore(fileName))
                    f.write('{}\t{}\n'.format(alignProgram, self._computeQuantile(lScores, self._quantile)))

            else:
                for j in self._list_files.keys():
                    lFiles = self._list_files[j]
                    self._log.debug("parse {} files from {}...".format(len(lFiles), j))
                    lScores = []
                    extend = lScores.extend
                    getScore = AlignUtils.getScoreListFromFile
                    for fileName in lFiles:
                        extend(getScore(fileName))
            
                    f.write('{}\t{}\n'.format(j, self._computeQuantile(lScores, self._quantile)))
        self._log.info("END ComputeScoreThresholdFromAlignFiles")

if __name__ == "__main__":
    iCSTFAF = ComputeScoreThresholdFromAlignFiles()
    iCSTFAF.setAttributesFromCmdLine()
    iCSTFAF.run()