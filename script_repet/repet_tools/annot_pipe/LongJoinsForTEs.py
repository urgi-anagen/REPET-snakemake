#!/usr/bin/env python

# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""
Connect TE paths via a 'long join' procedure.
"""

import os
import sys
import pandas

from commons.core.LoggerFactory import LoggerFactory
from commons.core.checker.RepetException import RepetDataException
from commons.core.utils.RepetOptionParser import RepetOptionParser
from commons.core.coord.PathUtils import PathUtils
from commons.core.coord.SetUtils import SetUtils
from commons.core.coord.MergedPath import MergedPath
from commons.core.coord.Path import Path

LOG_DEPTH = "repet.annot_pipe"


class LongJoinsForTEs:
    """
    Connect TE paths via a long join procedure.
    """

    def __init__(self, input_path_file="", output_file="", log_name="LongJoinsForTEs.log", verbose_var=0, max_overlap_var=15,
                 max_gap_length_var=5000, max_mismatch_length_var=500, identity_tolerance_var=2.0,
                 min_nested_TE_coverage_var=0.95, min_length_to_split_var=100, min_length_to_keep_chain_var=20):
        """
        Constructor.
        """
        self._input_path_file = input_path_file
        self._df_path_chr = None

        self._max_overlap = max_overlap_var
        self._max_gap_length = max_gap_length_var
        self._max_mismatch_length = max_mismatch_length_var
        self._identity_tolerance = identity_tolerance_var
        self._min_nested_TE_coverage = min_nested_TE_coverage_var
        self._min_length_to_split = min_length_to_split_var
        self._min_length_to_keep_chain = min_length_to_keep_chain_var

        self._output_file = output_file
        self._df_path_range = None
        self._df_path_output = pandas.DataFrame()
        self._verbose = verbose_var
        self._name = "LongJoinsForTEs"
        self._log = LoggerFactory.createLogger("{}.{}".format((LOG_DEPTH, self.__class__.__name__), self._verbose),
                                               log_name)

    def check_attributes(self):
        """
        Before running, check the required attributes are properly filled.
        """
        if self._input_path_file == "" or not os.path.exists(self._input_path_file):
            self._log.error("missing input file (path file)")
            sys.exit(1)
        else:
            self._df_path_chr = pandas.read_csv(self._input_path_file, sep='\t', header=None, index_col=0,
                                                low_memory=False)

        if self._output_file == "":
            self._output_file = "{}_join".format(self._input_path_file)

    def get_distance_between_ranges(self, r1, r2):
        """
        Return the distance between both ranges by handling all possible strands.
        """
        r1_is_on_direct_strand = r1.isOnDirectStrand()
        r2_is_on_direct_strand = r2.isOnDirectStrand()
        if r1_is_on_direct_strand != r2_is_on_direct_strand:
            self._log.error("can't compute distance for ranges on different strands")
            sys.exit(1)
        if r1_is_on_direct_strand and r2_is_on_direct_strand:
            if r1.start < r2.start:
                distance = r2.start - r1.end
            else:
                distance = r1.start - r2.end
        else:
            if r1.end < r2.end:
                distance = r2.end - r1.start
            else:
                distance = r1.end - r2.start
        return distance

    @staticmethod
    def get_diagonals_gap_and_mismatch(path1, path2, dist_q, dist_s):
        """
        Return the diagonals, gap and mismatch between two path instances.
        """
        diag1 = path1.range_subject.getMax() - path1.range_query.getMax()
        diag2 = path2.range_subject.getMin() - path2.range_query.getMin()
        gap = diag1 - diag2
        if gap > 0:  # gap on query
            mismatch = dist_s
        else:
            mismatch = dist_q
        return diag1, diag2, gap, mismatch

    @staticmethod
    def join_two_paths(id1, id2):
        if id1 < id2:
            new_id = id1
            old_id = id2
        else:
            new_id = id2
            old_id = id1
        return new_id, old_id

    @staticmethod
    def get_path_list_with_direct_query_direct_subject_from_query_subject(dataframe, qry, sbj):
        list_paths = list()
        tmp_df_query_subject = (dataframe.loc[
            (dataframe[1] == qry) & (dataframe[4] == sbj) & (dataframe[2] < dataframe[3]) & (
                    dataframe[5] < dataframe[6])])
        for i in tmp_df_query_subject.index:
            p = Path()
            p.setFromTuple([i] + list(tmp_df_query_subject.loc[i]))
            list_paths.append(p)
        return list_paths

    @staticmethod
    def path_to_path_range_or_path_to_path_range_query(df_path_chr_copy_range, rev=False):
        df_path_without_duplicated = df_path_chr_copy_range[~df_path_chr_copy_range.index.duplicated(keep=False)]
        df_duplicated_path = df_path_chr_copy_range[df_path_chr_copy_range.index.duplicated(keep=False)]
        df_duplicated_path_tmp = df_duplicated_path.copy()
        df_duplicated_path_tmp.loc[:, 9] = (abs(df_duplicated_path.loc[:, 3] - df_duplicated_path.loc[:,
                                                                               2]) + 1) * df_duplicated_path.loc[:, 9]
        for j in list(set(df_duplicated_path.index)):
            query_start = df_duplicated_path.loc[j][2].min()
            query_end = df_duplicated_path.loc[j][3].max()
            if rev:
                subject_start = df_duplicated_path.loc[j][5].max()
                subject_end = df_duplicated_path.loc[j][6].min()
            else:
                subject_start = df_duplicated_path.loc[j][5].min()
                subject_end = df_duplicated_path.loc[j][6].max()
            identity = (round(df_duplicated_path_tmp.loc[j][9].sum() / (
                    abs(df_duplicated_path.loc[j][3] - df_duplicated_path.loc[j][2]) + 1).sum(), 2))
            new_row = pandas.DataFrame({1: [df_duplicated_path.loc[j][1].unique()[0]],
                                        2: [query_start],
                                        3: [query_end],
                                        4: [df_duplicated_path.loc[j][4].unique()[0]],
                                        5: [subject_start],
                                        6: [subject_end],
                                        7: [df_duplicated_path.loc[j][7].min()],
                                        8: [df_duplicated_path.loc[j][8].sum()],
                                        9: [identity]}, index=[j])
            df_path_without_duplicated = pandas.concat([df_path_without_duplicated, new_row], axis=0)
        df_path_without_duplicated = df_path_without_duplicated.sort_index()
        return df_path_without_duplicated

    @staticmethod
    def get_path_list_with_direct_query_reverse_subject_from_query_subject(dataframe, qry, sbj):
        list_paths = list()
        tmp_df_query_subject = (dataframe.loc[
            (dataframe[1] == qry) & (dataframe[4] == sbj) & (dataframe[2] < dataframe[3]) & (
                    dataframe[5] > dataframe[6])])
        for i in tmp_df_query_subject.index:
            p = Path()
            p.setFromTuple([i] + list(tmp_df_query_subject.loc[i]))
            list_paths.append(p)
        return list_paths


    @staticmethod
    def get_path_list_included_in_query_coord(df_path_chr, query, start, end):
        if start > end:
            start, end = end, start
        tmp = df_path_chr.loc[(df_path_chr[1] == query) & (df_path_chr[2] >= start) & (df_path_chr[3] <= end)]

        list_paths = list()
        vect_df = tmp.values
        list_id = tmp.index
        for i in range(len(list_id)):
            row = vect_df[i]
            index = list_id[i]
        #for index, row in tmp.iterrows():
            p = Path()
            p.setFromTuple([index] + list(row))
            list_paths.append(p)
        return list_paths


    def join_TE_paths_per_query_subject(self, qry, sbj, list_paths, list_joined_paths):
        """
        Try to join TE paths for a given TE family.
        @param qry: query name
        @type qry: string
        @param sbj: subject name
        @type sbj: string
        @param list_paths: list of paths to join, if possible
        @type list_paths: list of {Path<Path>} instances
        @param list_joined_paths: list of {Path<Path>} instances that have already been joined
        @type list_joined_paths: list of {Path<Path>} instances
        """
        if len(list_paths) < 2: 
            return

        self._log.debug("\t\tDans join_TE_paths_per_query_subject")
        self._log.debug("\t\t\tTaille de list_paths: {}".format(len(list_paths)))
        self._log.debug("\t\t\tlist_joined_paths: {}".format(list_joined_paths))
        self._log.debug("\t\t\tTest sur le strand")
        for p in list_paths:
            self._log.debug("Traitement du path: {} ".format(p))
            query_is_on_direct_strand = p.range_query.isOnDirectStrand()
            if not query_is_on_direct_strand:
                self._log.error("query is on reverse strand --> exit")
                sys.exit(1)
            else:
                self._log.debug("\t\t\tquery is on direct strand")

        self._log.debug("\t\t\t\Test sur le join entre path[i] with path[i-1]")
        for i in range(1, len(list_paths)):  # try to join path[i] with path[i-1]
            self._log.debug("\t\t\tpath[i-i]: {}".format(list_paths[i - 1]))
            self._log.debug("\t\t\tpath[i]: {}".format(list_paths[i]))

            if list_paths[i].range_query.start == list_paths[
                i - 1].range_query.start:  # Deny long join: same query start
                self._log.debug("\t\t\tDeny long join: same query start")
                continue
            if list_paths[i].range_query.end == list_paths[i - 1].range_query.end:  # Deny long join: same query end
                self._log.debug("\t\t\tDeny long join: same query end")
                continue

            dist_between_queries = self.get_distance_between_ranges(list_paths[i - 1].range_query,
                                                                    list_paths[i].range_query)
            dist_between_subjects = self.get_distance_between_ranges(list_paths[i - 1].range_subject,
                                                                     list_paths[i].range_subject)
            self._log.debug("dist_between_queries: {}".format(dist_between_queries))
            self._log.debug("dist_between_subjects: {}".format(dist_between_subjects))

            if dist_between_queries > 100000:  # Deny long join: query gap > 100 kb
                self._log.debug("\t\t\tDeny long join: query gap > 100 kb ")
                continue
            if dist_between_subjects > 30000:  # Deny long join: subject gap> 30 kb
                self._log.debug("\t\t\tDeny long join: subject gap> 30 kb ")
                continue

            if dist_between_queries < (self._max_overlap * -1):  # Deny long join: too much overlap on query
                self._log.debug("\t\t\tDeny long join: too much overlap on query")
                continue
            if dist_between_subjects < (self._max_overlap * -1):  # Deny long join: too much overlap on subject
                self._log.debug("\t\t\tDeny long join: too much overlap on subject")
                continue

            diag1, diag2, gap, mismatch = self.get_diagonals_gap_and_mismatch(list_paths[i - 1], list_paths[i],
                                                                              dist_between_queries,
                                                                              dist_between_subjects)
            self._log.debug("\t\t\tdiag1: {}, diag2: {}, gap: {}, mismatch: {}".format(diag1, diag2, gap, mismatch))

            if mismatch > self._max_mismatch_length:  # Deny long join: too much mismatch on query or subject
                self._log.debug("\t\t\tDeny long join: too much mismatch on query or subject")
                continue

            try:
                mean_identity = PathUtils.getIdentityFromPathList(list_paths[i - 1: i + 1], True)
            except RepetDataException:
                mean_identity = 100
            self._log.debug("mean_identity: {}".format(mean_identity))

            if abs(list_paths[
                       i - 1].identity - mean_identity) > self._identity_tolerance:  # Deny long join: break identity tolerance
                self._log.debug("\t\t\tDeny long join: break identity tolerance for list_paths[i-1]")
                continue
            if abs(list_paths[i].identity - mean_identity) > self._identity_tolerance:
                self._log.debug("\t\t\tDeny long join: break identity tolerance for list_paths[i]")
                continue

            else:
                if gap <= self._max_gap_length:
                    self._log.debug(
                        "\t\t\tAllow long join: good identity for path[i-1] and path[i] and gp small enough")
                    # Allow long join: good identity for path[i-1] and path[i] and gp small enough
                    new_id, old_id = self.join_two_paths(list_paths[i - 1].id, list_paths[i].id)
                    self._df_path_chr = self._df_path_chr.rename({old_id: new_id}, axis=0)
                    list_joined_paths.append(
                        (qry, list_paths[i - 1], list_paths[i], dist_between_queries, dist_between_subjects))

                    self._log.debug("\t\t\tlist_joined_paths: {}".format(list_joined_paths))

                    continue
                start = list_paths[i - 1].range_query.end + 1
                end = list_paths[i].range_query.start - 1
                list_nested_paths = self.get_path_list_included_in_query_coord(self._df_path_chr, qry, start, end)

                if list_nested_paths:  # Allow long join: there are nested TEs to justify the gap
                    self._log.debug(
                        "\t\t\tAllow long join: there are nested TEs to justify the gap: {}".format(list_nested_paths))
                    try:
                        identity_nested_paths = PathUtils.getIdentityFromPathList(list_nested_paths, False)
                        self._log.debug("\t\t\tidentity_nested_paths: {}".format(identity_nested_paths))
                    except RepetDataException:
                        identity_nested_paths = 100
                    try:
                        identity_checked_paths = PathUtils.getIdentityFromPathList([list_paths[i - 1], list_paths[i]],
                                                                                   True)
                        self._log.debug("\t\t\tidentity_checked_paths: {}".format(identity_checked_paths))
                    except RepetDataException:
                        identity_checked_paths = 100
                    if identity_nested_paths < identity_checked_paths:  # Deny long join: nested TEs are older
                        self._log.debug("\t\t\tDeny long join: nested TEs are older")
                        continue

                    list_nested_sets = PathUtils.getSetListFromQueries(list_nested_paths)
                    list_merged_nested_sets = SetUtils.mergeSetsInList(list_nested_sets)
                    size = SetUtils.getCumulLength(list_merged_nested_sets)
                    # TODO: check if it's possible ?
                    if dist_between_queries <= 0:
                        nested_TE_coverage = 1
                    else:
                        nested_TE_coverage = float(size) / float(dist_between_queries)

                    if nested_TE_coverage < self._min_nested_TE_coverage:  # Deny long join: too low coverage of younger, nested TEs
                        self._log.debug(
                            "\t\t\tDeny long join: too low coverage of younger, nested TEs: nested_TE_coverage: {}, self._min_nested_TE_coverage: {}".format(
                                nested_TE_coverage, self._min_nested_TE_coverage))
                        continue

                    # Allow long join: nested TEs with high coverage
                    new_id2, old_id2 = self.join_two_paths(list_paths[i - 1].id, list_paths[i].id)
                    self._df_path_chr = self._df_path_chr.rename({old_id2: new_id2}, axis=0)
                    list_joined_paths.append(
                        (qry, list_paths[i - 1], list_paths[i], dist_between_queries, dist_between_subjects))
                    self._log.debug(
                        "\t\t\tAllow long join: nested TEs with high coverage list_joined_paths: {}".format(
                            list_joined_paths))

    def join(self):
        """
        Try to join path ranges from a TE annotation (output from Matcher).
        """
        self._log.info("****Beginning of the 'Long Join' procedure...")
        self._log.info("max overlap length: {}".format(self._max_overlap))
        self._log.info("max gap length: {}".format(self._max_gap_length))
        self._log.info("max mismatch length: {}".format(self._max_mismatch_length))
        self._log.info("identity tolerance: {:.2f}".format(self._identity_tolerance))
        self._log.info("inserted TE coverage: {:.2f}".format(self._min_nested_TE_coverage))

        # path2PathRange
        df_path_chr_copy_range_for = self._df_path_chr.loc[
            (self._df_path_chr[2] < self._df_path_chr[3]) & (self._df_path_chr[5] < self._df_path_chr[6])]
        df_path_chr_copy_range_rev = self._df_path_chr.loc[
            (self._df_path_chr[2] < self._df_path_chr[3]) & (self._df_path_chr[5] > self._df_path_chr[6])]
        df_path_without_duplicated_for = self.path_to_path_range_or_path_to_path_range_query(df_path_chr_copy_range_for)
        df_path_without_duplicated_rev = self.path_to_path_range_or_path_to_path_range_query(df_path_chr_copy_range_rev,
                                                                                             rev=True)
        self._df_path_range = pandas.concat([df_path_without_duplicated_for, df_path_without_duplicated_rev], axis=0)

        list_joined_paths = []
        list_query_names = list(self._df_path_chr.loc[:, 1].unique())
        list_query_names.sort()
        for qry in list_query_names:
            self._log.debug("\tQuery: {} ".format(qry))

            list_subject_names = list(self._df_path_chr.loc[:, 4].unique())
            list_subject_names.sort()
            for sbj in list_subject_names:
                self._log.debug("\t\tSubject: {} ".format(sbj))
                # get data as 'pathranges'
                self._log.debug("\t\tlPath with DirectQuery-DirectSubject-FromQuerySubject:")
                list_paths = self.get_path_list_with_direct_query_direct_subject_from_query_subject(self._df_path_range,
                                                                                                    qry, sbj)
                self._log.debug("\t\tlist_paths : {}".format(list_paths))

                # do the joins on the input
                self.join_TE_paths_per_query_subject(qry, sbj, list_paths, list_joined_paths)
                self._log.debug("\t\tlist_joined_paths : {}".format(list_joined_paths))

                self._log.debug("\t\tlPath with DirectQuery-ReverseSubject-FromQuerySubjectreversed:")
                list_paths = self.get_path_list_with_direct_query_reverse_subject_from_query_subject(
                    self._df_path_range, qry,
                    sbj)
                self._log.debug("\t\tlist_paths : {}".format(list_paths))
                self.join_TE_paths_per_query_subject(qry, sbj, list_paths, list_joined_paths)
                self._log.debug("\t\tlist_joined_paths : {}".format(list_joined_paths))
        self._log.info("nb of long joins: {}".format(len(list_joined_paths)))

    def split(self):
        """
        Try to split the connections made previously by considering all the TE families.
        A connection is cut if an older copy overlaps with a more recent hit (using weighted identity).
        """
        self._log.info("******Beginning of the 'Split' procedure...")
        self._log.info("min length to split: {}".format(self._min_length_to_split))

        nb_splits = 0
        new_id = max(self._df_path_chr.index) + 1

        list_query_names = list(self._df_path_chr.loc[:, 1].unique())
        list_query_names.sort()
        self._log.debug("list_query_names: {} ".format(list_query_names))

        for qry in list_query_names:
            self._log.debug("\tquery : {} ".format(qry))
            tmp_df_query = self._df_path_chr.loc[(self._df_path_chr[1] == qry)]
            list_qry_paths = list()
            vect_df = tmp_df_query.values
            list_id = tmp_df_query.index
            for i in range(len(list_id)):
                row = vect_df[i]
                index = list_id[i]
            #for index, row in tmp_df_query.iterrows():
                p = Path()
                p.setFromTuple([index] + list(row))
                list_qry_paths.append(p)
            self._log.debug("\tlist_qry_paths : {} ".format(list_qry_paths))

            dict_tmp = dict((iPath.getIdentifier(), MergedPath()) for iPath in list_qry_paths)

            for path in list_qry_paths:
                ident_key = path.getIdentifier()
                dict_tmp[ident_key].add(path)
                self._log.debug("\t\tident_key : {} , Value: {}".format(ident_key, dict_tmp[ident_key]))
            list_merged_paths = list(dict_tmp.values())
            list_merged_paths = sorted(list_merged_paths, key=lambda x: x.getWeightedIdentity())

            self._log.debug("query '{}': {} path collection(s)".format(qry, len(list_merged_paths)))
            for iMergedPath in list_merged_paths:
                list_path_fragments = iMergedPath.getPathsList()
                init_id = iMergedPath.getID()

                list_paths_to_insert = []

                if iMergedPath.getNbPaths() == 1:  # no connection to test
                    list_paths_to_insert += list_path_fragments
                else:  # several connected paths, try to split them (cut the connections)
                    list_join_coordinates = PathUtils.getListOfJoinCoordinatesOnQuery(list_path_fragments,
                                                                                      self._min_length_to_split)
                    if not list_join_coordinates:  # join distance too small to split
                        list_paths_to_insert += list_path_fragments
                    else:
                        list_overlap_paths = []
                        for join_coords in list_join_coordinates:
                            # "Nested paths" might be more descriptive than "overlap" ?
                            list_tmp = self.get_path_list_included_in_query_coord(self._df_path_output, qry,
                                                                                  join_coords[0],
                                                                                  join_coords[1])
                            if list_tmp:
                                if PathUtils.getLengthOnQueryFromPathList(list_tmp) > self._min_length_to_split:
                                    list_overlap_paths += list_tmp

                        if list_overlap_paths:  # No overlapped path long enough to trigger a split
                            list_paths_to_insert += list_path_fragments
                        else:
                            list_of_lists = PathUtils.getPathListUnjoinedBasedOnQuery(list_overlap_paths,
                                                                                      list_path_fragments)
                            nb_splits += len(list_of_lists) - 1
                            list_paths_to_insert += list_of_lists[0]
                            for list_paths in list_of_lists[1:]:
                                PathUtils.changeIdInList(list_paths, new_id)
                                new_id += 1
                                list_paths_to_insert += list_paths
                list_filtered_paths_to_insert = PathUtils.filterPathListOnChainLength(list_paths_to_insert,
                                                                                      self._min_length_to_keep_chain)
                for i in list_filtered_paths_to_insert:
                    new_row = pandas.DataFrame({1: [i.getQueryName()],
                                                2: [i.getQueryStart()],
                                                3: [i.getQueryEnd()],
                                                4: [i.getSubjectName()],
                                                5: [i.getSubjectStart()],
                                                6: [i.getSubjectEnd()],
                                                7: [i.getEvalue()],
                                                8: [i.getScore()],
                                                9: [i.getIdentity()]}, index=[i.getIdentifier()])
                    self._df_path_output = pandas.concat([self._df_path_output, new_row], axis=0)
                self._df_path_chr = self._df_path_chr.drop(init_id)  # est-ce vraiment utile ?
        self._log.info("nb of splits: {}".format(nb_splits))

    def start(self):
        """
        Print some stats.
        """
        self.check_attributes()
        self._log.info("START {}.py".format(self._name))
        self._log.info("***Starting part")

        # changePathQueryCoordinatesToDirectStrand
        df_path_chr_copy = self._df_path_chr.copy()
        list_num_path_change_query_coordinates = self._df_path_chr.loc[
            self._df_path_chr[2] > self._df_path_chr[3]].index
        for i in list_num_path_change_query_coordinates:
            self._df_path_chr.loc[i, 2] = df_path_chr_copy.loc[i, 3]
            self._df_path_chr.loc[i, 3] = df_path_chr_copy.loc[i, 2]
            self._df_path_chr.loc[i, 5] = df_path_chr_copy.loc[i, 6]
            self._df_path_chr.loc[i, 6] = df_path_chr_copy.loc[i, 5]

        self._log.info("nb of distinct queries: {}".format(len(self._df_path_chr.loc[:, 1].unique())))
        self._log.info("nb of distinct paths: {}".format(self._df_path_chr.shape[0]))
        self._log.info("nb of distinct identifiers: {}".format(len(self._df_path_chr.index.unique())))

    def finish(self):
        """
        Show some stats
        """
        self._log.info("***Finishing part")
        self._log.info("Joined annotations are in {} (path format)".format(self._output_file))
        self._df_path_output.to_csv(self._output_file, sep="\t", index=True, header=None)
        self._log.info("nb of distinct queries: {}".format(len(self._df_path_output.loc[:, 1].unique())))
        self._log.info("nb of distinct paths: {}".format(self._df_path_output.shape[0]))
        self._log.info("nb of distinct identifiers: {}".format(len(self._df_path_output.index.unique())))
        self._log.info("END {}.py".format(self._name))

    def run(self):
        """
        Run the whole program.
        """
        LoggerFactory.setLevel(self._log, self._verbose)
        self.start()
        self.join()
        self.split()
        self.finish()


if __name__ == "__main__":
    usage = "LongJoinForTEs.py [options]"
    description = "Connect TE paths via a long join procedure."
    parser = RepetOptionParser(description=description, epilog="", usage=usage)
    parser.add_option("-p", "--path_file", dest="input_path_file", action="store", type="string",
                      help="name of the input file [format='path'] [compulsory]", default="")
    parser.add_option("-i", "--identity", dest="identity_tolerance", action="store", type="float",
                      help="identity tolerance, in % [default=2]", default=2.0)
    parser.add_option("-o", "--overlap", dest="max_overlap", action="store", type="int",
                      help="max overlap size, in bp [default=15]", default=15)
    parser.add_option("-g", "--gap", dest="max_gap_length", action="store", type="int",
                      help="max gap size, in bp [default=5000]", default=5000)
    parser.add_option("-m", "--mismatch", dest="max_mismatch_length", action="store", type="int",
                      help="max mismatch size, in bp [default=500]", default=500)
    parser.add_option("-c", "--coverage", dest="min_nested_TE_coverage", action="store", type="float",
                      help="TE insertion coverage, [default=0.95]", default=0.95)
    parser.add_option("-s", "--split", dest="min_length_to_split", action="store", type="int",
                      help="min length to split a connection, in bp [default=100]", default=100)
    parser.add_option("-l", "--keep", dest="min_length_to_keep_chain", action="store", type="int",
                      help="min length to keep a chain [default=20]", default=20)
    parser.add_option("-O", "--output", dest="output_file", action="store", type="string",
                      help="name of the output table [default=input_path_file+'_join']", default="")
    parser.add_option("-v", "--verbosity", dest="verbosity", action="store", type="int",
                      help="verbosity [default: 0, from 0 to 2]", default=0)
    options = parser.parse_args()[0]
    input_path_file = options.input_path_file
    output = options.output_file
    verbose = options.verbosity
    max_overlap = options.max_overlap
    max_gap_length = options.max_gap_length
    max_mismatch_length = options.max_mismatch_length
    identity_tolerance = options.identity_tolerance
    min_nested_TE_coverage = options.min_nested_TE_coverage
    min_length_to_split = options.min_length_to_split
    min_length_to_keep_chain = options.min_length_to_keep_chain
    iLongJoinForTEs = LongJoinsForTEs(input_path_file=input_path_file, output_file=output, verbose=verbose,
                                      max_overlap=max_overlap,
                                      max_gap_length=max_gap_length, max_mismatch_length=max_mismatch_length,
                                      identity_tolerance=identity_tolerance,
                                      min_nested_TE_coverage=min_nested_TE_coverage,
                                      min_length_to_split=min_length_to_split,
                                      min_length_to_keep_chain=min_length_to_keep_chain)
    iLongJoinForTEs.run()
