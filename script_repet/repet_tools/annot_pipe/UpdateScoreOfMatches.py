#!/usr/bin/env python

import sys
import getopt
import os

from commons.core.coord.AlignUtils import AlignUtils
from commons.core.utils.RepetOptionParser import RepetOptionParser

class UpdateScoreOfMatches( object ):
    
    def __init__( self, inputData = "", formatData = "",outputData = "",verbose = 0 ,typeInData = "file"):
        self._inputData = inputData
        self._formatData = formatData
        self._outputData = outputData
        self._verbose = verbose
        self._typeInData = typeInData

    def setAttributesFromCmdLine(self):
        description = "Launch UpdateScoreOfMatches.py. The new score is the length on the query times the percentage of identity"
        epilog = "\n"
        parser = RepetOptionParser(description=description, epilog=epilog)
        parser.add_option("-i", "--input", dest="input", action="store", type="string",
                          help="input data (can be file or table) ", default="")
        parser.add_option("-f", "--format", dest="format", action="store", type="str",
                          help="format of the data (align/path)", default="")
        parser.add_option("-o", "--output", dest="output", action="store", type="str",
                          help="output data (default=input+'.newScores')", default="")
        parser.add_option("-v", "--verbosity", dest="verbosity", action="store", type="int",
                          help="verbosity [optional] [default: 1]", default=1)
        options = parser.parse_args()[0]
        self._setAttributesFromOptions(options)

    def _setAttributesFromOptions(self, options):
        self._verbose = options.verbosity
        self._inputData = options.input
        self._formatData = options.format
        self._outputData = options.output
                
    def checkAttributes( self ):
        if self._inputData == "":
            msg = "ERROR: missing input data (-i)"
            sys.stderr.write( "%s\n" % msg )
            sys.exit(1)
        if not os.path.exists( self._inputData ):
            msg = "ERROR: can't find input file '%s'" % ( self._inputData )
            sys.stderr.write( "%s\n" % msg )
            sys.exit(1)
        if self._formatData == "":
            msg = "ERROR: need to precise format (-f)"
            sys.stderr.write( "%s\n" % msg )
            sys.exit(1)
        if self._formatData not in [ "align", "path" ]:
            msg = "ERROR: format '%s' not yet supported" % ( self._formatData )
            sys.stderr.write( "%s\n" % msg )
            sys.exit(1)
        if self._outputData == "":
            self._outputData = "%s.newScores" % ( self._inputData )
            
            
    def start( self ):
        if self._verbose > 0:
            print("START UpdateScoreOfMatches.py")
            sys.stdout.flush()
        self.checkAttributes()
        
        
    def end( self ):
        if self._verbose > 0:
            print("END UpdateScoreOfMatches.py")
            sys.stdout.flush()
            
            
    def run( self ):
        self.start()
        
        if self._typeInData == "file":
            if self._formatData == "align":
                AlignUtils.updateScoresInFile( self._inputData,
                                               self._outputData )
            elif self._formatData == "path":
                pass
            
        self.end()
        
        
if __name__ == "__main__":
    i = UpdateScoreOfMatches()
    i.setAttributesFromCmdLine()
    i.run()
