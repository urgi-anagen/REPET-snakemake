#!/usr/bin/env python

import os
import subprocess
import sys
import getopt
from commons.core.sql.DbFactory import DbFactory
from commons.core.sql.TablePathAdaptator import TablePathAdaptator
from commons.core.sql.TableSetAdaptator import TableSetAdaptator
from commons.core.coord.PathUtils import PathUtils
from commons.core.coord.SetUtils import SetUtils
from commons.core.coord.Path import Path
from commons.core.coord.Match import Match
from commons.core.coord.Map import Map
from commons.core.coord.Align import Align
from commons.core.coord.Set import Set


# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------

def main(map_name="", coordTable="", coordFileName="", dataFormat="", connect=False, outTable="", configFileName="",
         verbose=0):
    if verbose > 0:
        print("START {}".format(sys.argv[0].split("/")[-1]))
        sys.stdout.flush()

    dChunk2Link = {}
    with open(map_name, 'r') as f:
        for line in f.readlines():
            split_line = line.split("\t")
            dChunk2Link[split_line[0]] = (split_line[1], int(split_line[2]), int(split_line[3]))

    if outTable == "":
        outTable = coordTable + "_onchr"

    if coordFileName != "" and coordTable == "":
        if verbose > 0:
            print("convert file '{}'...".format(coordFileName))
            sys.stdout.flush()
        if dataFormat == "path":
            conv_path_file(dChunk2Link, coordFileName)
        elif dataFormat == "tab":
            conv_match_file(dChunk2Link, coordFileName)
        elif dataFormat == "map":
            conv_map_file(dChunk2Link, coordFileName)
        else:
            print("*** Error: unknown file format: {}".format(dataFormat))
            sys.exit(1)

    elif coordFileName == "" and coordTable == "":
        print("*** Error: both table and file names are given")
        sys.exit(1)

    if verbose > 0:
        print("END {}".format(sys.argv[0].split("/")[-1]))
        sys.stdout.flush()

    return 0
 

# ----------------------------------------------------------------------------

def conv_path_file(chunk, filename):
    """
    Convert a 'path' file format.
    """

    fout = open(filename + ".on_chr", "w")
    fin = open(filename, "r")
    p = Path()
    while True:
        if not p.read(fin):
            break
        #p.range_query.show()
        i = chunk.get(p.range_query.seqname.split()[0], None)
        if i == None:
            ("*** Error: chunk: '{}' not found".format(p.range_query.seqname))
            sys.exit(1)
        else:
            p.range_query.seqname = i[0]
            if i[1] < i[2]:
                p.range_query.start = p.range_query.start + i[1] - 1
                p.range_query.end = p.range_query.end + i[1] - 1
            else:
                p.range_query.start = i[1] - p.range_query.start + 1
                p.range_query.end = i[1] - p.range_query.end + 1
            p.write(fout)
    fout.close()
    fin.close()


# ----------------------------------------------------------------------------

def conv_match_file(chunk, filename):
    """
    Convert a 'match' file format.
    """

    fout = open(filename + ".on_chr", "w")
    fin = open(filename, "r")
    fin.readline()
    m = Match()
    while True:
        if not m.read(fin):
            break
        #m.range_query.show()
        i = chunk.get(m.range_query.seqname.split()[0], None)
        if i == None:
            ("*** Error: chunk '{}' not found".format(m.range_query.seqname))
            sys.exit(1)
        else:
            m.range_query.seqname = i[0]
            if i[1] < i[2]:
                m.range_query.start = m.range_query.start + i[1] - 1
                m.range_query.end = m.range_query.end + i[1] - 1
            else:
                m.range_query.start = i[1] - m.range_query.start + 1
                m.range_query.end = i[1] - m.range_query.end + 1
            m.write(fout)
    fout.close()
    fin.close()


# ----------------------------------------------------------------------------

def conv_map_file(chunk, filename):
    """
    Convert a 'map' file format.
    """

    fout = open(filename + ".on_chr", "w")
    fin = open(filename, "r")
    m = Map()
    while True:
        if not m.read(fin):
            break
        #m.show()
        i = chunk.get(m.seqname.split()[0], None)
        if i == None:
            ("*** Error: chunk '{}' not found".format(m.seqname))
            sys.exit(1)
        else:
            m.seqname = i[0]
            if i[1] < i[2]:
                m.start = m.start + i[1] - 1
                m.end = m.end + i[1] - 1
            else:
                m.start = i[1] - m.start + 1
                m.end = i[1] - m.end + 1
            m.write(fout)
    fout.close()
    fin.close()


# ----------------------------------------------------------------------------

def conv_path_table(dChunk2Link, coordTable, outTable):
    """
    Convert the coordinates recorded in a 'path' table.

    @param dChunk2Link: dictionary whose keys are the chunk names and values a list with chromosome name, start and end
    @type dChunk2Link: dictionary

    @param coordTable: name of the table recording the coordinates you want to convert
    @type coordTable: string

    @param outTable: name of the output table
    @type outTable: string
    """

    tmpFileName = "{}.on_chr".format(os.getpid())
    tmpFile = open(tmpFileName, "w")
    p = Path()

    # for each chunk
    lChunks = dChunk2Link.keys()
    #    lChunks.sort()
    for chunkName in sorted(lChunks):
        if verbose > 1:
            ("processing {}...".format(chunkName));
            sys.stdout.flush()

        # retrieve its matches
        sql_cmd = "SELECT * FROM {} WHERE query_name LIKE \"{} %%\" or query_name=\"{}\";" \
            .format(coordTable, chunkName, chunkName)
        db.execute(sql_cmd)
        lPaths = db.fetchall()

        # for each match
        for path in lPaths:
            p.setFromTuple(path)

            # convert the coordinates on the query
            link = dChunk2Link[chunkName]
            p.range_query.seqname = link[0]
            if (link[1] < link[2]):
                p.range_query.start = p.range_query.start + link[1] - 1
                p.range_query.end = p.range_query.end + link[1] - 1
            else:
                p.range_query.start = link[1] - p.range_query.start + 1
                p.range_query.end = link[1] - p.range_query.end + 1

            # convert the coordinates on the subject (if necessary)
            link = dChunk2Link.get(p.range_subject.seqname)
            if link != None:
                if verbose > 1:
                    ("convert subject: {}".format(p.range_subject.seqname))
                p.range_subject.seqname = link[0]
                p.range_subject.start = p.range_subject.start + link[1] - 1
                p.range_subject.end = p.range_subject.end + link[1] - 1
            p.write(tmpFile)
    tmpFile.close()

    db.createTable(outTable, "path", tmpFileName, True)

    os.system("rm -f " + tmpFileName)


# ----------------------------------------------------------------------------

# convert a match table format        

def conv_match_table(chunk, table, outTable):
    temp_file = str(os.getpid()) + ".on_chr"
    fout = open(temp_file, 'w')
    fout.write("dummy line \n")
    m = Match()
    for chunkName in chunk.keys():
        sql_cmd = 'select * from {} where query_name like "{} %%" or query_name="{}" ;' \
            .format(table, chunkName, chunkName)
        db.execute(sql_cmd)
        res = db.fetchall()
        for t in res:
            m.setFromTuple(t)
            i = chunk[chunkName]
            m.range_query.seqname = i[0]
            if (i[1] < i[2]):
                m.range_query.start = m.range_query.start + i[1] - 1
                m.range_query.end = m.range_query.end + i[1] - 1
            else:
                m.range_query.start = i[1] - m.range_query.start + 1
                m.range_query.end = i[1] - m.range_query.end + 1
            m.write(fout)
    fout.close()

    db.createTable(outTable, "path", temp_file, True)

    subprocess.call(("rm -f " + temp_file), shell=True)


# ----------------------------------------------------------------------------

# convert a map table format        

def conv_map_table(chunk, table, outTable):
    temp_file = str(os.getpid()) + ".on_chr"
    fout = open(temp_file, 'w')
    m = Map()
    for chunkName in chunk.keys():
        sql_cmd = 'select * from {} where chr like "{} %%" or chr="{}";' \
            .format(table, chunkName, chunkName)
        db.execute(sql_cmd)
        res = db.fetchall()
        for t in res:
            m.set_from_tuple(t)
            i = chunk[chunkName]
            m.seqname = i[0]
            if (i[1] < i[2]):
                m.start = m.start + i[1] - 1
                m.end = m.end + i[1] - 1
            else:
                m.start = i[1] - m.start + 1
                m.end = i[1] - m.end + 1
            m.write(fout)
    fout.close()

    db.createTable(outTable, "path", temp_file, True)

    subprocess.call(("rm -f " + temp_file), shell=True)


# ----------------------------------------------------------------------------

def conv_set_table(dChunk2Link, table, outTable):
    """
    Convert the coordinates recorded in a 'set' table.

    @param dChunk2Link: dictionary whose keys are the chunk names and values a list with chromosome name, start and end
    @type dChunk2Link: dictionary

    @param coordTable: name of the table recording the coordinates you want to convert
    @type coordTable: string

    @param outTable: name of the output table
    @type outTable: string
    """

    tmpFileName = "{}.on_chr".format(os.getpid())
    tmpFile = open(tmpFileName, "w")
    s = Set()

    for chunkName in dChunk2Link.keys():
        if verbose > 1:
            ("processing {}...".format(chunkName));
            sys.stdout.flush()
        sql_cmd = 'select * from {} where chr like "{} %%" or chr="{}";' \
            .format(table, chunkName, chunkName)
        db.execute(sql_cmd)
        res = db.fetchall()
        for t in res:
            s.setFromTuple(t)
            i = dChunk2Link[chunkName]
            s.seqname = i[0]
            if (i[1] < i[2]):
                s.start = s.start + i[1] - 1
                s.end = s.end + i[1] - 1
            else:
                s.start = i[1] - s.start + 1
                s.end = i[1] - s.end + 1
            s.write(tmpFile)
    tmpFile.close()

    db.createTable(outTable, "set", tmpFileName, True)

    os.remove(tmpFileName)


# ----------------------------------------------------------------------------

def conv_align_table(dChunk2Link, coordTable, outTable):
    """
    Convert the coordinates recorded in an 'align' table.

    @param dChunk2Link: dictionary whose keys are the chunk names and values a list with chromosome name, start and end
    @type dChunk2Link: dictionary

    @param coordTable: name of the table recording the coordinates you want to convert
    @type coordTable: string

    @param outTable: name of the output table
    @type outTable: string
    """

    tmpFileName = "{}.on_chr".format(os.getpid())
    tmpFile = open(tmpFileName, "w")
    p = Align()

    # for each chunk
    lChunks = dChunk2Link.keys()
    #    lChunks.sort()
    for chunkName in sorted(lChunks):
        if verbose > 1:
            ("processing {}...".format(chunkName));
            sys.stdout.flush()

        # retrieve its matches
        sql_cmd = "SELECT * FROM {} WHERE query_name LIKE \"{} %%\" or query_name=\"{}\";" \
            .format(coordTable, chunkName, chunkName)
        db.execute(sql_cmd)
        lHSPs = db.fetchall()

        # for each HSP
        for align in lHSPs:
            p.setFromTuple(align)

            # convert the coordinates on the query
            link = dChunk2Link[chunkName]
            p.range_query.seqname = link[0]
            if (link[1] < link[2]):
                p.range_query.start = p.range_query.start + link[1] - 1
                p.range_query.end = p.range_query.end + link[1] - 1
            else:
                p.range_query.start = link[1] - p.range_query.start + 1
                p.range_query.end = link[1] - p.range_query.end + 1

            # convert the coordinates on the subject (if necessary)
            link = dChunk2Link.get(p.range_subject.seqname)
            if link != None:
                if verbose > 1:
                    ("convert subject: {}".format(p.range_subject.seqname))
                p.range_subject.seqname = link[0]
                p.range_subject.start = p.range_subject.start + link[1] - 1
                p.range_subject.end = p.range_subject.end + link[1] - 1
            p.write(tmpFile)
    tmpFile.close()

    db.createTable(outTable, "align", tmpFileName, True)

    subprocess.call(("rm -f " + tmpFileName), shell=True)


# ----------------------------------------------------------------------------

def connect_path_chunks(chunk, table, db, verbose):
    if verbose > 0:
        ("connect chunks...")
        sys.stdout.flush()

    iTPA = TablePathAdaptator(db, table)

    nbChunks = len(chunk.keys())

    for num_chunk in range(1, nbChunks):
        chunkName = "chunk{}".format(str(num_chunk).zfill(len(str(nbChunks))))

        next_chunkName = "chunk{}".format(str(num_chunk + 1).zfill(len(str(nbChunks))))

        if next_chunkName not in chunk.keys():
            break
        if verbose > 1:
            ("try with {} and {}".format(chunkName, next_chunkName))
            sys.stdout.flush()

        start = chunk[chunkName][2]
        end = chunk[next_chunkName][1]
        if chunk[chunkName][0] == chunk[next_chunkName][0]:
            lPaths = iTPA.getChainListOverlappingQueryCoord(chunk[chunkName][0], start, end)
            if verbose > 1:
                print("{} matches on {} ({}->{})".format(len(lPaths), chunk[chunkName][0], start, end))
                sys.stdout.flush()
            lSortedPaths = PathUtils.getPathListSortedByIncreasingMinQueryThenMaxQueryThenMinSubjectThenMaxSubject(
                lPaths)
            chg_path_id = {}
            pathnum_to_ins = []
            pathnum_to_del = []
            dpath = []
            rpath = []
            for i in lSortedPaths:
                if i.range_query.isOnDirectStrand() and i.range_subject.isOnDirectStrand():
                    dpath.append(i)
                else:
                    rpath.append(i)
            x = 0
            while x < len(dpath) - 1:
                x = x + 1
                if verbose > 1:
                    dpath[x - 1].show()
                    dpath[x].show()

                if dpath[x - 1].id != dpath[x].id \
                        and dpath[x - 1].range_query.seqname \
                        == dpath[x].range_query.seqname \
                        and dpath[x - 1].range_subject.seqname \
                        == dpath[x].range_subject.seqname \
                        and dpath[x - 1].range_query.isOnDirectStrand() \
                        == dpath[x].range_query.isOnDirectStrand() \
                        and dpath[x - 1].range_subject.isOnDirectStrand() \
                        == dpath[x].range_subject.isOnDirectStrand() \
                        and dpath[x - 1].range_query.isOverlapping(dpath[x].range_query) \
                        and dpath[x - 1].range_subject.isOverlapping(dpath[x].range_subject):
                    chg_path_id[dpath[x].id] = dpath[x - 1].id
                    if dpath[x - 1].id not in pathnum_to_ins:
                        pathnum_to_ins.append(dpath[x - 1].id)
                    if dpath[x].id not in pathnum_to_del:
                        pathnum_to_del.append(dpath[x].id)
                    dpath[x - 1].merge(dpath[x])
                    del dpath[x]
                    x = x - 1
                    if verbose > 1:
                        ("--> merged")
            x = 0
            while x < len(rpath) - 1:
                x = x + 1
                if verbose > 1:
                    rpath[x - 1].show()
                    rpath[x].show()

                if rpath[x - 1].id != rpath[x].id \
                        and rpath[x - 1].range_query.seqname \
                        == rpath[x].range_query.seqname \
                        and rpath[x - 1].range_subject.seqname \
                        == rpath[x].range_subject.seqname \
                        and rpath[x - 1].range_query.isOnDirectStrand() \
                        == rpath[x].range_query.isOnDirectStrand() \
                        and rpath[x - 1].range_subject.isOnDirectStrand() \
                        == rpath[x].range_subject.isOnDirectStrand() \
                        and rpath[x - 1].range_query.isOverlapping(rpath[x].range_query) \
                        and rpath[x - 1].range_subject.isOverlapping(rpath[x].range_subject):
                    chg_path_id[rpath[x].id] = rpath[x - 1].id
                    if rpath[x - 1].id not in pathnum_to_ins:
                        pathnum_to_ins.append(rpath[x - 1].id)
                    if rpath[x].id not in pathnum_to_del:
                        pathnum_to_del.append(rpath[x].id)
                    rpath[x - 1].merge(rpath[x])
                    del rpath[x]
                    x = x - 1
                    if verbose > 1:
                        ("--> merged")
            if verbose > 1:
                ("pathnum to delete", pathnum_to_del)
            iTPA.deleteFromIdList(pathnum_to_del)
            iTPA.deleteFromIdList(pathnum_to_ins)
            for i in dpath:
                #                if chg_path_id.has_key(i.id):
                if (i.id) in chg_path_id:
                    i.id = chg_path_id[i.id]
                if verbose > 1:
                    i.show()
                if i.id in pathnum_to_ins:
                    iTPA.insert(i)
                    if verbose > 1:
                        ("--> inserted!")
            for i in rpath:
                #                if chg_path_id.has_key(i.id):
                if (i.id) in chg_path_id:
                    i.id = chg_path_id[i.id]
                if verbose > 1:
                    i.show()
                if i.id in pathnum_to_ins:
                    iTPA.insert(i)
                    if verbose > 1:
                        ("--> inserted!")
            sys.stdout.flush()


# ----------------------------------------------------------------------------

def connect_set_chunks(chunk, table):
    if verbose > 0:
        ("connect chunks...")
        sys.stdout.flush()

    iTSA = TableSetAdaptator(db, table)

    nbChunks = len(chunk.keys())

    for num_chunk in range(1, nbChunks):
        chunkName = "chunk{}".format(str(num_chunk).zfill(len(str(nbChunks))))
        if verbose > 1:
            (chunkName)
        next_chunkName = "chunk{}".format(str(num_chunk + 1).zfill(len(str(nbChunks))))

        if next_chunkName not in chunk.keys():
            break
        start = chunk[chunkName][2]
        end = chunk[next_chunkName][1]
        if chunk[chunkName][0] == chunk[next_chunkName][0]:
            lSets = iTSA.getChainListOverlappingCoord(chunk[chunkName][0], start, end)
            if verbose > 1:
                ("----------")
            lSortedSets = SetUtils.getSetListSortedByIncreasingMinThenMax(lSets)
            chg_set_id = {}
            setnum_to_ins = []
            setnum_to_del = []
            dset = []
            rset = []
            for i in lSortedSets:
                if i.isOnDirectStrand():
                    dset.append(i)
                else:
                    rset.append(i)
            x = 0
            while x < len(dset) - 1:
                x = x + 1
                if verbose > 1:
                    ("++++")
                    dset[x - 1].show()
                    dset[x].show()

                if dset[x - 1].id != dset[x].id \
                        and dset[x - 1].seqname == dset[x].seqname \
                        and dset[x - 1].name == dset[x].name \
                        and dset[x - 1].isOnDirectStrand() \
                        == dset[x].isOnDirectStrand() \
                        and dset[x - 1].isOverlapping(dset[x]):
                    chg_set_id[dset[x].id] = dset[x - 1].id
                    if dset[x - 1].id not in setnum_to_ins:
                        setnum_to_ins.append(dset[x - 1].id)
                    if dset[x].id not in setnum_to_del:
                        setnum_to_del.append(dset[x].id)
                    dset[x - 1].merge(dset[x])
                    del dset[x]
                    x = x - 1
                    if verbose > 1:
                        ("--> merged")
            if verbose > 1:
                ("...........")
            x = 0
            while x < len(rset) - 1:
                x = x + 1
                if verbose > 1:
                    ("++++")
                    rset[x - 1].show()
                    rset[x].show()

                if rset[x - 1].id != rset[x].id \
                        and rset[x - 1].seqname == rset[x].seqname \
                        and rset[x - 1].name == rset[x].name \
                        and rset[x - 1].isOnDirectStrand() \
                        == rset[x].isOnDirectStrand() \
                        and rset[x - 1].isOverlapping(rset[x]):
                    chg_set_id[rset[x].id] = rset[x - 1].id
                    if rset[x - 1].id not in setnum_to_ins:
                        setnum_to_ins.append(rset[x - 1].id)
                    if rset[x].id not in setnum_to_del:
                        setnum_to_del.append(rset[x].id)
                    rset[x - 1].merge(rset[x])
                    del rset[x]
                    x = x - 1
                    if verbose > 1:
                        ("--> merged")
            if verbose > 1:
                ("...........")
                (setnum_to_del)
            iTSA.deleteFromIdList(setnum_to_del)
            if verbose > 1:
                (setnum_to_ins)
            iTSA.deleteFromIdList(setnum_to_ins)
            for i in dset:
                #                if chg_set_id.has_key(i.id):
                if (i.id) in chg_set_id:
                    i.id = chg_set_id[i.id]
                if verbose > 1:
                    i.show()
                if i.id in setnum_to_ins:
                    iTSA.insert(i)
                    if verbose > 1:
                        print("--> inserted!")
                if verbose > 1:
                    print("==========")
            for i in rset:
                #                if chg_set_id.has_key(i.id):
                if (i.id) in chg_set_id:
                    i.id = chg_set_id[i.id]
                if verbose > 1:
                    i.show()
                if i.id in setnum_to_ins:
                    iTSA.insert(i)
                    if verbose > 1:
                        print("--> inserted!")
                if verbose > 1:
                    print("==========")


# -----------------------------------------------------------------------------


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description='usage: srptConvChunk2Chr.py [ options ]\n',
        formatter_class=argparse.RawTextHelpFormatter, add_help=True)
    parser.add_argument('-m', '--map_file', required=True,
                        help="name of the file recording the coordinates of the chunks on the chromosomes ('map' format)")
    parser.add_argument('-f', '--file', required=True, default="",
                        help="name of the file recording the coordinates you want to convert ('path'/'tab'/'map' format)")
    parser.add_argument('-t', '--format_target', required=True, default="",
                        help="format of data (match/path/map/set/align) for the target (-f)")
    parser.add_argument('-o', '--output', default="", help="name of the output file (default=inFileName+'_onchr')")
    parser.add_argument('-v', '--verbose', default=0, type=int, help="verbose (default=0/1/2)")

    args = parser.parse_args()
    map_file = args.map_file
    file_to_convert = args.file
    format = args.format_target
    output = args.output
    verbose = args.verbose

    main(map_name=map_file, coordFileName=file_to_convert, dataFormat=format, connect=False, outTable=output, 
         verbose=0)


