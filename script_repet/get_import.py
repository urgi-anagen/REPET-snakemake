
import re
import os

file = "./list_import.txt"
with open(file, 'r') as f :
    content = f.readlines()

dico = dict()
for i in content:

    line_split = (i.rstrip().split(':'))
    if os.path.splitext(line_split[0])[1] == ".py" and 'test' not in line_split[0]:
        if line_split[0] not in dico.keys():
            dico.setdefault(line_split[0],list())

        if "from" in line_split[1] :
            pass
            modules = re.findall("from\s+([\w,.]+)\s+import",line_split[1])
            dico[line_split[0]].extend(modules)
        else :
            if len(line_split[1].split(',')) > 1 :
                modules = line_split[1].split('import')[1].split(',')
                #print(modules)
                dico[line_split[0]].extend(modules)
            else :

                module = (re.findall("(?<!from)import (\w+)",line_split[1]))
                dico[line_split[0]].extend(module)
        dico[line_split[0]] = sorted(list(set(dico[line_split[0]])))


with open("modules.txt",'w') as fout :
    for j in sorted(dico.keys()) :
        fout.write("* {}:\t{} \n".format(j, "\t".join(dico[j])))
