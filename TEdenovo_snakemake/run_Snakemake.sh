#!/bin/bash

# TODO, make a system that autolauchesth right version of the workflow depending on the configs dependency options presence

VAR=$(env | grep "CONDA_DEFAULT_ENV" | grep "base")
RESULT=$?
if [ $RESULT -eq 0 ]; then
  source ~/anaconda3/etc/profile.d/conda.sh
  conda activate snakemake
  snakemake --use-conda --use-singularity -p --configfile config/config.yaml -c 30 -s Snakefile -R all --max-jobs-per-second 5 --max-status-checks-per-second 5 --latency-wait 5
else
  snakemake --use-conda --use-singularity -p --configfile config/config.yaml -c 30 -s Snakefile -R all --max-jobs-per-second 5 --max-status-checks-per-second 5 --latency-wait 5
fi
