
import sys
from os.path import join
import os
import contextlib

from commons.tools.PreProcess import PreProcess

PreProcess(fastaFileName=snakemake.input[0],
            outputFasta=snakemake.output[0],
            step=13,
            substep3=3,
            InfoForStep3=snakemake.params.subset_length,
            verbosity=3,
            singularity_img = snakemake.params.sif,
            log_name =snakemake.log[0]
            ).run(dirStep = os.path.join(os.getcwd(),snakemake.params.dir))
