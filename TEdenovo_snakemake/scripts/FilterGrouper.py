from denovo_pipe.filterOutGrouper_mod import main

main(inFaFileName=snakemake.input[0],
    minSeq=snakemake.params[0],
    maxSeq=snakemake.params[1],
    maxHspLength=snakemake.params[2],
    maxJoinLength=snakemake.params[3],
    chunkOverlap=snakemake.params[4],
    verboseParam=snakemake.params.verbose,
    logFileName=snakemake.log[0],
    legacy_grouper=False)
