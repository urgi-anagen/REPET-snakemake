import subprocess
import time
import os

from denovo_pipe.ConvertFastaHeadersFromChkToChr import ConvertFastaHeadersFromChkToChr
from commons.tools.ShortenConsensusHeader import ShortenConsensusHeader
from commons.tools.ChangeSequenceHeaders import ChangeSequenceHeaders
from denovo_pipe.buildConsLib import buildconsLib

# input_file = "/home/mwan/REPET/REPET-snakemake/TEdenovo_snakemake/scripts/seqCluster1.fa"
# mapChunkFile = "/home/mwan/REPET/REPET-snakemake/TEdenovo_snakemake/results_DmelChr4/DmelChr4_chunks.map"
# output_map = os.path.join(os.path.split(input_file)[0],"{}.fa_aln".format(os.path.basename(input_file)))
# singularity_img = "/home/mwan/REPET/REPET-snakemake/Singularity/te_finder_2.30.2.sif"
# log="test.log"

def launch_map(input_file, mapChunkFile, output_map, singularity_img, log):

    output_file = os.path.join(os.path.split(input_file)[0],"{}.onChr".format(os.path.basename(input_file)))
    iConvertFastaHeadersFromChkToChr = ConvertFastaHeadersFromChkToChr(inData=input_file,
                                        mapFile=mapChunkFile,
                                        outFile=output_file)
    iConvertFastaHeadersFromChkToChr.run()

    iChangeSequenceHeaders = ChangeSequenceHeaders(inFile=output_file,format="fasta",step=1,prefix="seq")
    iChangeSequenceHeaders.run()

    linkfiles=os.path.join(os.path.split(input_file)[0],"{}.onChr.newHlink".format(os.path.basename(input_file)))
    outfiles=os.path.join(os.path.split(input_file)[0],"{}.onChr.newH".format(os.path.basename(input_file)))

    param_map="50 -8 16 4"

    if singularity_img != "":
        cmd = "singularity exec {} rpt_map {} {} > {}".format(singularity_img,outfiles,param_map,output_map )
    else:
        cmd = "rpt_map {} {} > {}".format(outfiles,param_map,output_map)
#check if runing on a sif or not, we assume blank param is absence

    if os.path.exists(linkfiles) and os.path.exists(outfiles):
        out_dir = os.path.split(input_file)[0]
        subprocess.call(cmd, shell=True)

        if os.path.exists(output_map):
            iChangeSequenceHeaders2 = ChangeSequenceHeaders(inFile=output_map,format="fasta",step=2,linkFile=linkfiles)
            iChangeSequenceHeaders2.run()
            outfiles_ChangeSequenceHeaders2 =os.path.join(os.path.split(output_map)[0],"{}.initH".format(os.path.basename(output_map)))
            if os.path.exists(outfiles_ChangeSequenceHeaders2):
                buildconsLib(file=outfiles_ChangeSequenceHeaders2, logFileName=log)


launch_map(input_file=snakemake.input[0],
            mapChunkFile =snakemake.params.mapChunkFile,
            output_map = snakemake.output.output_map,
            singularity_img = snakemake.params.singularity_img,
            log = snakemake.log[0])
