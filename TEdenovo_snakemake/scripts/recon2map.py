import os
import shutil

def recon2map(inFileName, outFileName):
    with open(inFileName, "r") as inFile, open(outFileName, "w") as outFile:
        content = inFile.readlines()
        for line in content:
            if not line.startswith("#"):
                data = line.split()
                clusterID = data[0]
                memberID = data[1]
                strand = data[2]
                seqName = data[3]
                seqStart = data[4]
                seqEnd = data[5]
                if strand == "-1":
                    tmp = seqStart
                    seqStart = seqEnd
                    seqEnd = tmp
                string = "ReconCluster{}Mb{}\t{}\t{}\t{}\n".format(clusterID, memberID, seqName, seqStart, seqEnd)
                outFile.write(string)


recon2map(snakemake.input[0], snakemake.output[0])
shutil.rmtree((os.path.split(snakemake.input[0])[0]))

