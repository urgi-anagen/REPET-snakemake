from commons.tools.MergeMatchsFiles import MergeMatchsFiles
from commons.tools.RmvPairAlignInChunkOverlaps import RmvPairAlignInChunkOverlaps
import os


base_path = os.getcwd()
MergeMatchsFiles(path = snakemake.params.path_dir,
                fileType="align",
                outFileBaseName=snakemake.params.outFileBaseName,
                allByAll=snakemake.params.allByAll,
                clean=snakemake.params.clean).run()

os.chdir(base_path)
RmvPairAlignInChunkOverlaps(snakemake.output.merge_file,
                            snakemake.params.chunkLength,
                            snakemake.params.chunkOverlap,
                            snakemake.params.margin,
                            snakemake.output.rmvPairAlignInChunkOverlaps_file,
                            snakemake.params.verbose,
                            snakemake.log[0]).run()
