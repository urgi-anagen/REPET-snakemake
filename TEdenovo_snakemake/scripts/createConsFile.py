from commons.tools.ShortenConsensusHeader import ShortenConsensusHeader

def create_consensus_file(list_file, output_name,output_name_short,clustering,log):
    with open(output_name, "a") as outFileHandler:
        isFirstFile = True
        for singleFile in list_file:
            if not isFirstFile:
                outFileHandler.write("")
            isFirstFile = False
            with open(singleFile, "r") as singleFileHandler:
                line = singleFileHandler.readline()
                while line:
                    outFileHandler.write(line)
                    line = singleFileHandler.readline()
    iSCH = ShortenConsensusHeader(fastaFileName=output_name,
                                  consensusFileName=output_name_short,
                                  projectName="Blaster",
                                  clustering=clustering,
                                  multAlign="Map", verbosity=3,
                                  log_name=log)
    iSCH.run()


create_consensus_file(snakemake.input,
                      snakemake.output.output_file,
                      snakemake.output.ouput_shortname,
                      snakemake.params.clustering_method,
                      snakemake.log[0])