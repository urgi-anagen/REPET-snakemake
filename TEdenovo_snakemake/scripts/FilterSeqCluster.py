from denovo_pipe.FilterSeqClusters import FilterSeqClusters

FilterSeqClusters(inFileName=snakemake.input[0],
                    outFileName=snakemake.output[0],
                    HSPLength = 20000,
                    minSeqPerGroup = 3,
                    maxSeqPerGroup = 20,
                    verbose = 3,
                    log_name=snakemake.log[0]
                    ).run()
