import os
import contextlib
from commons.tools.PrepareBatches import PrepareBatches
from os.path import join



base = os.getcwd()
with open(snakemake.log[0], "w") as o:
    with contextlib.redirect_stdout(o):
        PrepareBatches(genomeFastaFileName=snakemake.input[0],
                        singularity_img = snakemake.params.sif,
                        projectDir=snakemake.params.outdir,
                        chunk_length=snakemake.params.chunk_length,
                        chunk_overlap=snakemake.params.chunk_overlap,
                        min_nb_seq_per_batch=snakemake.params.min_nb_seq_per_batch
                        ).run()

os.chdir(base)
