import denovo_pipe.splitSeqPerCluster as splitSeqPerCluster
import os

base_path=os.getcwd()

if not os.path.isdir(snakemake.output[0]):
    os.makedirs(snakemake.output[0],exist_ok=True)

p = (os.path.join(base_path,snakemake.input[0]))

splitSeqPerCluster.main(path=snakemake.output[0],
                        inFileName=p,
                        clusteringMethod=snakemake.params.method,
                        verbose=0)
os.chdir(base_path)
