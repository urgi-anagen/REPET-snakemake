from denovo_pipe.LaunchRecon import LaunchRecon
import os

base=os.getcwd()
os.chdir(snakemake.params.out_dir)

LaunchRecon(Infile = "../../{}".format(snakemake.input.seq_name), #corrects the paths, hacky
            msp = "../../{}".format(snakemake.input.msp),
            verbose = 1).run()

#os.remove(sankemake.params.rm_directories)
os.chdir(base)

#WARNING: input file need to be in the cwd
#if you change the output dir of recon in any way, recon is bound to break


#TODO substract the snakemake.params.out_dir path from the infile path, to get the relative path
#to the infile if in the snakemake.params.out_dir
