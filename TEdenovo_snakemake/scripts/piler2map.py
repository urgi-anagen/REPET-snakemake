def piler2map(inFileName, outFileName):
    with open(inFileName, "r") as inFile, open(outFileName, "w") as outFile:
        content = inFile.readlines()
        for line in content:
            if not line.startswith("#"):
                data = line.split()
                seqName = data[0]
                seqStart = data[3]
                seqEnd = data[4]
                strand = data[6]
                clusterID = data[9]
                memberID = data[12]
                if strand == "-":
                    tmp = seqStart
                    seqStart = seqEnd
                    seqEnd = tmp
                string = "PilerCluster{}Mb{}\t{}\t{}\t{}\n".format(clusterID, memberID, seqName, seqStart, seqEnd)
                outFile.write(string)

piler2map(snakemake.input[0], snakemake.output[0])