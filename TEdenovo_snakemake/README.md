# TEdenovo-snakemake
TEdenovo Pipeline in snakemake based on REPET v3.1.
This pipeline detects repeats in eukaryotic genomes.

## Directory tree
```
├── config
│   └── config.yaml
├── README.md
├── run_Snakemake.sh
├── scripts
│   ├── CreateBatch.py
│   ├── createConsFile.py
│   ├── DenovoPreprocess.py
│   ├── FilterGrouper.py
│   ├── FilterSeqCluster.py
│   ├── launch_rpt_map.py
│   ├── MergeFiles.py
│   ├── piler2map.py
│   ├── recon2map.py
│   ├── recon.pl
│   ├── ReconSnakemake.py
│   └── SplitSeqPerCluster.py
├── Snakefile
├── Snakefile_conda
└── Snakefile_singularity
```

### Scripts
Python scripts used in the Snakefile

### config
The directory contains snakemake config file

```yaml
# config/config.yaml

#Input files
# Path to genome file, must be a fasta
genome_file: /home/etbardet/REPET/REPET-snakemake/data/DmelChr4.fa
# Path to TE librairy, must be a fasta
consensus_lib: /home/etbardet/REPET/REPET-snakemake/data/all_consensus_repet.fa

# Path to Apptainer images
container :
  TE_finder: /home/etbardet/REPET/REPET-snakemake/Singularity/REPET.sif

conda_envs:
  TE_finder: "TE_finder"
  RepeatMasker: "RepeatMasker"
  Recon: "Recon"
  Easel: "Hmmer"
  MREP: "MREPS"
  TRF: "TRF"
  Piler: "Piler"
  MCL: "MCL"


params_batch :
  chunk_length : 200000
  chunk_overlap : 10000
  min_nb_seq_per_batch :  5

params_blaster:
  sensitivity : 0
  e_value : 1e-300
  fragment_cutter_length : 2000000

params_run_RmvPairAlignInChunkOverlaps :
  chunkLength: 200000
  chunkOverlap: 10000
  margin: 10
```

### run_Snakemake.sh
bash script to execute the snakemake.  The script will activate the snakemake environment and run the pipeline, it is assumed that there is a pre-existing snakemake environment (called snakemake).

If the environment doesn't exist, you can create one, using this command line (it is assumed that conda already exists) :

```
conda create -c bioconda -c conda-forge --name snakemake snakemake snakedeploy python=3.9
```

You can also install the environment snakemake in other way, take a look to this documentation :
https://snakemake.readthedocs.io/en/stable/getting_started/installation.html

If you don't want to use the bash script to launch the snakemake pipeline, you can execute it with the command line below :
```
> conda activate snakemake
## if scheduler slurm
> snakemake --use-conda --use-singularity -p --configfile config/config.yaml -j 64 -s Snakefile -R all --cluster sbatch --max-jobs-per-second 5 --max-status-checks-per-second 5
## if no scheduler
> snakemake --use-conda --use-singularity -p --configfile config/config.yaml -c 30 -s Snakefile -R all --max-jobs-per-second 5 --max-status-checks-per-second 5

```

### Snakefile
For the moments, only the step 1 to 4 of TEdenovo ahev been implemented in the snakefile

TODO: details the steps

**![](dag_TEdenovo.svg)**

To obtain the graph, launch the command below :
```
snakemake --use-conda --use-singularity -p --configfile config/config.yaml -c 30 -s Snakefile -R all --max-jobs-per-second 5 --max-status-checks-per-second 5 --latency-wait 3600 --rulegraph | dot -Tsvg > dag_TEdenovo.svg
```
## Launch TEdenovo snakemake
1. Fill the config file in the directory config/

2. Then launch [run_Snakemake.sh](run_Snakemake.sh):
```
./run_Snakemake.sh
```
or you can launch directly the command line :
```
snakemake --use-conda --use-singularity -p --configfile config/config.yaml -c 30 -s Snakefile -R all --max-jobs-per-second 5 --max-status-checks-per-second 5 --latency-wait 3600
```

3. The results will be in the directory :
```
REPET-snakemake/TEdenovo_snakemake/results_TEdenovo_<fasta_name>
```

## Results
The main files in the results directory :
- Consensus fasta file, library created using the selected clustering methods : **all_consensus.fa**
- Consensus fasta file for each selected clustering methods
  - **map/consensus_grouper.fa**
  - **map/consensus_recon.fa**
  - **map/consensus_piler.fa**

The consensus library may have redundant sequences. To remove this redundancy, you can use [PASTEC standalone](https://urgi.versailles.inrae.fr/Tools/PASTEClassifier).
